#ifndef WAVE_WRAPPER_H
#define WAVE_WRAPPER_H

#include "wavelib.h"
#include "waux.h"

#ifdef __cplusplus
extern "C" {
#endif

int wtoutputlength(int N,char *method,int lp, int J, char *ext);

void wave_transform(double *inp, int N, char *wname, char *method, int J, char *ext, double *out, int *length, double *filters);

void inv_wave_transform(double *inp, int N, char *wname, char *method, int J, char *ext, double *out, int outlength, int *length, int lenlength);

void cwave_transform(char* wave, double param, double *inp, int N,double dt, int J,double s0, double dj, char *type, int power,
  int npflag, double *oupmag, double *scale, double *period, double *coi);

void wdenoise(double *signal,int N,int J,char *denoise_method,char *wname,char *method,char *ext,char *thresh,double *denoised);  

int wreccoeff(char *method,double *coeff,int *length,char *ctype,char *ext, int level, int J,double *lpr,
		double *hpr,int lf,int siglength,double *reccoeff);

#ifdef __cplusplus
}
#endif

#endif // End WAVE_WRAPPER_H
