// The Module object: Our interface to the outside world. We import
// and export values on it, and do the work to get that through
// closure compiler if necessary. There are various ways Module can be used:
// 1. Not defined. We create it here
// 2. A function parameter, function(Module) { ..generated code.. }
// 3. pre-run appended it, var Module = {}; ..generated code..
// 4. External script tag defines var Module.
// We need to do an eval in order to handle the closure compiler
// case, where this code here is minified but Module was defined
// elsewhere (e.g. case 4 above). We also need to check if Module
// already exists (e.g. case 3 above).
// Note that if you want to run closure, and also to use Module
// after the generated code, you will need to define   var Module = {};
// before the code. Then that object will be used in the code, and you
// can continue to use Module afterwards as well.
var Module;
if (!Module) Module = (typeof Module !== 'undefined' ? Module : null) || {};

// Sometimes an existing Module object exists with properties
// meant to overwrite the default module functionality. Here
// we collect those properties and reapply _after_ we configure
// the current environment's defaults to avoid having to be so
// defensive during initialization.
var moduleOverrides = {};
for (var key in Module) {
  if (Module.hasOwnProperty(key)) {
    moduleOverrides[key] = Module[key];
  }
}

// The environment setup code below is customized to use Module.
// *** Environment setup code ***
var ENVIRONMENT_IS_WEB = typeof window === 'object';
var ENVIRONMENT_IS_NODE = typeof process === 'object' && typeof require === 'function' && !ENVIRONMENT_IS_WEB;
// Three configurations we can be running in:
// 1) We could be the application main() thread running in the main JS UI thread. (ENVIRONMENT_IS_WORKER == false and ENVIRONMENT_IS_PTHREAD == false)
// 2) We could be the application main() thread proxied to worker. (with Emscripten -s PROXY_TO_WORKER=1) (ENVIRONMENT_IS_WORKER == true, ENVIRONMENT_IS_PTHREAD == false)
// 3) We could be an application pthread running in a worker. (ENVIRONMENT_IS_WORKER == true and ENVIRONMENT_IS_PTHREAD == true)
var ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
var ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;

if (ENVIRONMENT_IS_NODE) {
  // Expose functionality in the same simple way that the shells work
  // Note that we pollute the global namespace here, otherwise we break in node
  if (!Module['print']) Module['print'] = function print(x) {
    process['stdout'].write(x + '\n');
  };
  if (!Module['printErr']) Module['printErr'] = function printErr(x) {
    process['stderr'].write(x + '\n');
  };

  var nodeFS = require('fs');
  var nodePath = require('path');

  Module['read'] = function read(filename, binary) {
    filename = nodePath['normalize'](filename);
    var ret = nodeFS['readFileSync'](filename);
    // The path is absolute if the normalized version is the same as the resolved.
    if (!ret && filename != nodePath['resolve'](filename)) {
      filename = path.join(__dirname, '..', 'src', filename);
      ret = nodeFS['readFileSync'](filename);
    }
    if (ret && !binary) ret = ret.toString();
    return ret;
  };

  Module['readBinary'] = function readBinary(filename) { return Module['read'](filename, true) };

  Module['load'] = function load(f) {
    globalEval(read(f));
  };

  if (!Module['thisProgram']) {
    if (process['argv'].length > 1) {
      Module['thisProgram'] = process['argv'][1].replace(/\\/g, '/');
    } else {
      Module['thisProgram'] = 'unknown-program';
    }
  }

  Module['arguments'] = process['argv'].slice(2);

  if (typeof module !== 'undefined') {
    module['exports'] = Module;
  }

  process['on']('uncaughtException', function(ex) {
    // suppress ExitStatus exceptions from showing an error
    if (!(ex instanceof ExitStatus)) {
      throw ex;
    }
  });

  Module['inspect'] = function () { return '[Emscripten Module object]'; };
}
else if (ENVIRONMENT_IS_SHELL) {
  if (!Module['print']) Module['print'] = print;
  if (typeof printErr != 'undefined') Module['printErr'] = printErr; // not present in v8 or older sm

  if (typeof read != 'undefined') {
    Module['read'] = read;
  } else {
    Module['read'] = function read() { throw 'no read() available (jsc?)' };
  }

  Module['readBinary'] = function readBinary(f) {
    if (typeof readbuffer === 'function') {
      return new Uint8Array(readbuffer(f));
    }
    var data = read(f, 'binary');
    assert(typeof data === 'object');
    return data;
  };

  if (typeof scriptArgs != 'undefined') {
    Module['arguments'] = scriptArgs;
  } else if (typeof arguments != 'undefined') {
    Module['arguments'] = arguments;
  }

}
else if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
  Module['read'] = function read(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.send(null);
    return xhr.responseText;
  };

  if (typeof arguments != 'undefined') {
    Module['arguments'] = arguments;
  }

  if (typeof console !== 'undefined') {
    if (!Module['print']) Module['print'] = function print(x) {
      console.log(x);
    };
    if (!Module['printErr']) Module['printErr'] = function printErr(x) {
      console.log(x);
    };
  } else {
    // Probably a worker, and without console.log. We can do very little here...
    var TRY_USE_DUMP = false;
    if (!Module['print']) Module['print'] = (TRY_USE_DUMP && (typeof(dump) !== "undefined") ? (function(x) {
      dump(x);
    }) : (function(x) {
      // self.postMessage(x); // enable this if you want stdout to be sent as messages
    }));
  }

  if (ENVIRONMENT_IS_WORKER) {
    Module['load'] = importScripts;
  }

  if (typeof Module['setWindowTitle'] === 'undefined') {
    Module['setWindowTitle'] = function(title) { document.title = title };
  }
}
else {
  // Unreachable because SHELL is dependant on the others
  throw 'Unknown runtime environment. Where are we?';
}

function globalEval(x) {
  eval.call(null, x);
}
if (!Module['load'] && Module['read']) {
  Module['load'] = function load(f) {
    globalEval(Module['read'](f));
  };
}
if (!Module['print']) {
  Module['print'] = function(){};
}
if (!Module['printErr']) {
  Module['printErr'] = Module['print'];
}
if (!Module['arguments']) {
  Module['arguments'] = [];
}
if (!Module['thisProgram']) {
  Module['thisProgram'] = './this.program';
}

// *** Environment setup code ***

// Closure helpers
Module.print = Module['print'];
Module.printErr = Module['printErr'];

// Callbacks
Module['preRun'] = [];
Module['postRun'] = [];

// Merge back in the overrides
for (var key in moduleOverrides) {
  if (moduleOverrides.hasOwnProperty(key)) {
    Module[key] = moduleOverrides[key];
  }
}



// === Preamble library stuff ===

// Documentation for the public APIs defined in this file must be updated in: 
//    site/source/docs/api_reference/preamble.js.rst
// A prebuilt local version of the documentation is available at: 
//    site/build/text/docs/api_reference/preamble.js.txt
// You can also build docs locally as HTML or other formats in site/
// An online HTML version (which may be of a different version of Emscripten)
//    is up at http://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html

//========================================
// Runtime code shared with compiler
//========================================

var Runtime = {
  setTempRet0: function (value) {
    tempRet0 = value;
  },
  getTempRet0: function () {
    return tempRet0;
  },
  stackSave: function () {
    return STACKTOP;
  },
  stackRestore: function (stackTop) {
    STACKTOP = stackTop;
  },
  getNativeTypeSize: function (type) {
    switch (type) {
      case 'i1': case 'i8': return 1;
      case 'i16': return 2;
      case 'i32': return 4;
      case 'i64': return 8;
      case 'float': return 4;
      case 'double': return 8;
      default: {
        if (type[type.length-1] === '*') {
          return Runtime.QUANTUM_SIZE; // A pointer
        } else if (type[0] === 'i') {
          var bits = parseInt(type.substr(1));
          assert(bits % 8 === 0);
          return bits/8;
        } else {
          return 0;
        }
      }
    }
  },
  getNativeFieldSize: function (type) {
    return Math.max(Runtime.getNativeTypeSize(type), Runtime.QUANTUM_SIZE);
  },
  STACK_ALIGN: 16,
  prepVararg: function (ptr, type) {
    if (type === 'double' || type === 'i64') {
      // move so the load is aligned
      if (ptr & 7) {
        assert((ptr & 7) === 4);
        ptr += 4;
      }
    } else {
      assert((ptr & 3) === 0);
    }
    return ptr;
  },
  getAlignSize: function (type, size, vararg) {
    // we align i64s and doubles on 64-bit boundaries, unlike x86
    if (!vararg && (type == 'i64' || type == 'double')) return 8;
    if (!type) return Math.min(size, 8); // align structures internally to 64 bits
    return Math.min(size || (type ? Runtime.getNativeFieldSize(type) : 0), Runtime.QUANTUM_SIZE);
  },
  dynCall: function (sig, ptr, args) {
    if (args && args.length) {
      if (!args.splice) args = Array.prototype.slice.call(args);
      args.splice(0, 0, ptr);
      return Module['dynCall_' + sig].apply(null, args);
    } else {
      return Module['dynCall_' + sig].call(null, ptr);
    }
  },
  functionPointers: [],
  addFunction: function (func) {
    for (var i = 0; i < Runtime.functionPointers.length; i++) {
      if (!Runtime.functionPointers[i]) {
        Runtime.functionPointers[i] = func;
        return 2*(1 + i);
      }
    }
    throw 'Finished up all reserved function pointers. Use a higher value for RESERVED_FUNCTION_POINTERS.';
  },
  removeFunction: function (index) {
    Runtime.functionPointers[(index-2)/2] = null;
  },
  warnOnce: function (text) {
    if (!Runtime.warnOnce.shown) Runtime.warnOnce.shown = {};
    if (!Runtime.warnOnce.shown[text]) {
      Runtime.warnOnce.shown[text] = 1;
      Module.printErr(text);
    }
  },
  funcWrappers: {},
  getFuncWrapper: function (func, sig) {
    assert(sig);
    if (!Runtime.funcWrappers[sig]) {
      Runtime.funcWrappers[sig] = {};
    }
    var sigCache = Runtime.funcWrappers[sig];
    if (!sigCache[func]) {
      sigCache[func] = function dynCall_wrapper() {
        return Runtime.dynCall(sig, func, arguments);
      };
    }
    return sigCache[func];
  },
  getCompilerSetting: function (name) {
    throw 'You must build with -s RETAIN_COMPILER_SETTINGS=1 for Runtime.getCompilerSetting or emscripten_get_compiler_setting to work';
  },
  stackAlloc: function (size) { var ret = STACKTOP;STACKTOP = (STACKTOP + size)|0;STACKTOP = (((STACKTOP)+15)&-16); return ret; },
  staticAlloc: function (size) { var ret = STATICTOP;STATICTOP = (STATICTOP + size)|0;STATICTOP = (((STATICTOP)+15)&-16); return ret; },
  dynamicAlloc: function (size) { var ret = DYNAMICTOP;DYNAMICTOP = (DYNAMICTOP + size)|0;DYNAMICTOP = (((DYNAMICTOP)+15)&-16); if (DYNAMICTOP >= TOTAL_MEMORY) { var success = enlargeMemory(); if (!success) { DYNAMICTOP = ret; return 0; } }; return ret; },
  alignMemory: function (size,quantum) { var ret = size = Math.ceil((size)/(quantum ? quantum : 16))*(quantum ? quantum : 16); return ret; },
  makeBigInt: function (low,high,unsigned) { var ret = (unsigned ? ((+((low>>>0)))+((+((high>>>0)))*4294967296.0)) : ((+((low>>>0)))+((+((high|0)))*4294967296.0))); return ret; },
  GLOBAL_BASE: 8,
  QUANTUM_SIZE: 4,
  __dummy__: 0
}


Module['Runtime'] = Runtime;



//========================================
// Runtime essentials
//========================================

var __THREW__ = 0; // Used in checking for thrown exceptions.

var ABORT = false; // whether we are quitting the application. no code should run after this. set in exit() and abort()
var EXITSTATUS = 0;

var undef = 0;
// tempInt is used for 32-bit signed values or smaller. tempBigInt is used
// for 32-bit unsigned values or more than 32 bits. TODO: audit all uses of tempInt
var tempValue, tempInt, tempBigInt, tempInt2, tempBigInt2, tempPair, tempBigIntI, tempBigIntR, tempBigIntS, tempBigIntP, tempBigIntD, tempDouble, tempFloat;
var tempI64, tempI64b;
var tempRet0, tempRet1, tempRet2, tempRet3, tempRet4, tempRet5, tempRet6, tempRet7, tempRet8, tempRet9;

function assert(condition, text) {
  if (!condition) {
    abort('Assertion failed: ' + text);
  }
}

var globalScope = this;

// Returns the C function with a specified identifier (for C++, you need to do manual name mangling)
function getCFunc(ident) {
  var func = Module['_' + ident]; // closure exported function
  if (!func) {
    try {
      func = eval('_' + ident); // explicit lookup
    } catch(e) {}
  }
  assert(func, 'Cannot call unknown function ' + ident + ' (perhaps LLVM optimizations or closure removed it?)');
  return func;
}

var cwrap, ccall;
(function(){
  var JSfuncs = {
    // Helpers for cwrap -- it can't refer to Runtime directly because it might
    // be renamed by closure, instead it calls JSfuncs['stackSave'].body to find
    // out what the minified function name is.
    'stackSave': function() {
      Runtime.stackSave()
    },
    'stackRestore': function() {
      Runtime.stackRestore()
    },
    // type conversion from js to c
    'arrayToC' : function(arr) {
      var ret = Runtime.stackAlloc(arr.length);
      writeArrayToMemory(arr, ret);
      return ret;
    },
    'stringToC' : function(str) {
      var ret = 0;
      if (str !== null && str !== undefined && str !== 0) { // null string
        // at most 4 bytes per UTF-8 code point, +1 for the trailing '\0'
        ret = Runtime.stackAlloc((str.length << 2) + 1);
        writeStringToMemory(str, ret);
      }
      return ret;
    }
  };
  // For fast lookup of conversion functions
  var toC = {'string' : JSfuncs['stringToC'], 'array' : JSfuncs['arrayToC']};

  // C calling interface. 
  ccall = function ccallFunc(ident, returnType, argTypes, args, opts) {
    var func = getCFunc(ident);
    var cArgs = [];
    var stack = 0;
    if (args) {
      for (var i = 0; i < args.length; i++) {
        var converter = toC[argTypes[i]];
        if (converter) {
          if (stack === 0) stack = Runtime.stackSave();
          cArgs[i] = converter(args[i]);
        } else {
          cArgs[i] = args[i];
        }
      }
    }
    var ret = func.apply(null, cArgs);
    if (returnType === 'string') ret = Pointer_stringify(ret);
    if (stack !== 0) {
      if (opts && opts.async) {
        EmterpreterAsync.asyncFinalizers.push(function() {
          Runtime.stackRestore(stack);
        });
        return;
      }
      Runtime.stackRestore(stack);
    }
    return ret;
  }

  var sourceRegex = /^function\s*\(([^)]*)\)\s*{\s*([^*]*?)[\s;]*(?:return\s*(.*?)[;\s]*)?}$/;
  function parseJSFunc(jsfunc) {
    // Match the body and the return value of a javascript function source
    var parsed = jsfunc.toString().match(sourceRegex).slice(1);
    return {arguments : parsed[0], body : parsed[1], returnValue: parsed[2]}
  }
  var JSsource = {};
  for (var fun in JSfuncs) {
    if (JSfuncs.hasOwnProperty(fun)) {
      // Elements of toCsource are arrays of three items:
      // the code, and the return value
      JSsource[fun] = parseJSFunc(JSfuncs[fun]);
    }
  }

  
  cwrap = function cwrap(ident, returnType, argTypes) {
    argTypes = argTypes || [];
    var cfunc = getCFunc(ident);
    // When the function takes numbers and returns a number, we can just return
    // the original function
    var numericArgs = argTypes.every(function(type){ return type === 'number'});
    var numericRet = (returnType !== 'string');
    if ( numericRet && numericArgs) {
      return cfunc;
    }
    // Creation of the arguments list (["$1","$2",...,"$nargs"])
    var argNames = argTypes.map(function(x,i){return '$'+i});
    var funcstr = "(function(" + argNames.join(',') + ") {";
    var nargs = argTypes.length;
    if (!numericArgs) {
      // Generate the code needed to convert the arguments from javascript
      // values to pointers
      funcstr += 'var stack = ' + JSsource['stackSave'].body + ';';
      for (var i = 0; i < nargs; i++) {
        var arg = argNames[i], type = argTypes[i];
        if (type === 'number') continue;
        var convertCode = JSsource[type + 'ToC']; // [code, return]
        funcstr += 'var ' + convertCode.arguments + ' = ' + arg + ';';
        funcstr += convertCode.body + ';';
        funcstr += arg + '=' + convertCode.returnValue + ';';
      }
    }

    // When the code is compressed, the name of cfunc is not literally 'cfunc' anymore
    var cfuncname = parseJSFunc(function(){return cfunc}).returnValue;
    // Call the function
    funcstr += 'var ret = ' + cfuncname + '(' + argNames.join(',') + ');';
    if (!numericRet) { // Return type can only by 'string' or 'number'
      // Convert the result to a string
      var strgfy = parseJSFunc(function(){return Pointer_stringify}).returnValue;
      funcstr += 'ret = ' + strgfy + '(ret);';
    }
    if (!numericArgs) {
      // If we had a stack, restore it
      funcstr += JSsource['stackRestore'].body.replace('()', '(stack)') + ';';
    }
    funcstr += 'return ret})';
    return eval(funcstr);
  };
})();
Module["cwrap"] = cwrap;
Module["ccall"] = ccall;


function setValue(ptr, value, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': HEAP8[((ptr)>>0)]=value; break;
      case 'i8': HEAP8[((ptr)>>0)]=value; break;
      case 'i16': HEAP16[((ptr)>>1)]=value; break;
      case 'i32': HEAP32[((ptr)>>2)]=value; break;
      case 'i64': (tempI64 = [value>>>0,(tempDouble=value,(+(Math_abs(tempDouble))) >= 1.0 ? (tempDouble > 0.0 ? ((Math_min((+(Math_floor((tempDouble)/4294967296.0))), 4294967295.0))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/4294967296.0)))))>>>0) : 0)],HEAP32[((ptr)>>2)]=tempI64[0],HEAP32[(((ptr)+(4))>>2)]=tempI64[1]); break;
      case 'float': HEAPF32[((ptr)>>2)]=value; break;
      case 'double': HEAPF64[((ptr)>>3)]=value; break;
      default: abort('invalid type for setValue: ' + type);
    }
}
Module['setValue'] = setValue;


function getValue(ptr, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': return HEAP8[((ptr)>>0)];
      case 'i8': return HEAP8[((ptr)>>0)];
      case 'i16': return HEAP16[((ptr)>>1)];
      case 'i32': return HEAP32[((ptr)>>2)];
      case 'i64': return HEAP32[((ptr)>>2)];
      case 'float': return HEAPF32[((ptr)>>2)];
      case 'double': return HEAPF64[((ptr)>>3)];
      default: abort('invalid type for setValue: ' + type);
    }
  return null;
}
Module['getValue'] = getValue;

var ALLOC_NORMAL = 0; // Tries to use _malloc()
var ALLOC_STACK = 1; // Lives for the duration of the current function call
var ALLOC_STATIC = 2; // Cannot be freed
var ALLOC_DYNAMIC = 3; // Cannot be freed except through sbrk
var ALLOC_NONE = 4; // Do not allocate
Module['ALLOC_NORMAL'] = ALLOC_NORMAL;
Module['ALLOC_STACK'] = ALLOC_STACK;
Module['ALLOC_STATIC'] = ALLOC_STATIC;
Module['ALLOC_DYNAMIC'] = ALLOC_DYNAMIC;
Module['ALLOC_NONE'] = ALLOC_NONE;

// allocate(): This is for internal use. You can use it yourself as well, but the interface
//             is a little tricky (see docs right below). The reason is that it is optimized
//             for multiple syntaxes to save space in generated code. So you should
//             normally not use allocate(), and instead allocate memory using _malloc(),
//             initialize it with setValue(), and so forth.
// @slab: An array of data, or a number. If a number, then the size of the block to allocate,
//        in *bytes* (note that this is sometimes confusing: the next parameter does not
//        affect this!)
// @types: Either an array of types, one for each byte (or 0 if no type at that position),
//         or a single type which is used for the entire block. This only matters if there
//         is initial data - if @slab is a number, then this does not matter at all and is
//         ignored.
// @allocator: How to allocate memory, see ALLOC_*
function allocate(slab, types, allocator, ptr) {
  var zeroinit, size;
  if (typeof slab === 'number') {
    zeroinit = true;
    size = slab;
  } else {
    zeroinit = false;
    size = slab.length;
  }

  var singleType = typeof types === 'string' ? types : null;

  var ret;
  if (allocator == ALLOC_NONE) {
    ret = ptr;
  } else {
    ret = [_malloc, Runtime.stackAlloc, Runtime.staticAlloc, Runtime.dynamicAlloc][allocator === undefined ? ALLOC_STATIC : allocator](Math.max(size, singleType ? 1 : types.length));
  }

  if (zeroinit) {
    var ptr = ret, stop;
    assert((ret & 3) == 0);
    stop = ret + (size & ~3);
    for (; ptr < stop; ptr += 4) {
      HEAP32[((ptr)>>2)]=0;
    }
    stop = ret + size;
    while (ptr < stop) {
      HEAP8[((ptr++)>>0)]=0;
    }
    return ret;
  }

  if (singleType === 'i8') {
    if (slab.subarray || slab.slice) {
      HEAPU8.set(slab, ret);
    } else {
      HEAPU8.set(new Uint8Array(slab), ret);
    }
    return ret;
  }

  var i = 0, type, typeSize, previousType;
  while (i < size) {
    var curr = slab[i];

    if (typeof curr === 'function') {
      curr = Runtime.getFunctionIndex(curr);
    }

    type = singleType || types[i];
    if (type === 0) {
      i++;
      continue;
    }

    if (type == 'i64') type = 'i32'; // special case: we have one i32 here, and one i32 later

    setValue(ret+i, curr, type);

    // no need to look up size unless type changes, so cache it
    if (previousType !== type) {
      typeSize = Runtime.getNativeTypeSize(type);
      previousType = type;
    }
    i += typeSize;
  }

  return ret;
}
Module['allocate'] = allocate;

// Allocate memory during any stage of startup - static memory early on, dynamic memory later, malloc when ready
function getMemory(size) {
  if (!staticSealed) return Runtime.staticAlloc(size);
  if ((typeof _sbrk !== 'undefined' && !_sbrk.called) || !runtimeInitialized) return Runtime.dynamicAlloc(size);
  return _malloc(size);
}
Module['getMemory'] = getMemory;

function Pointer_stringify(ptr, /* optional */ length) {
  if (length === 0 || !ptr) return '';
  // TODO: use TextDecoder
  // Find the length, and check for UTF while doing so
  var hasUtf = 0;
  var t;
  var i = 0;
  while (1) {
    t = HEAPU8[(((ptr)+(i))>>0)];
    hasUtf |= t;
    if (t == 0 && !length) break;
    i++;
    if (length && i == length) break;
  }
  if (!length) length = i;

  var ret = '';

  if (hasUtf < 128) {
    var MAX_CHUNK = 1024; // split up into chunks, because .apply on a huge string can overflow the stack
    var curr;
    while (length > 0) {
      curr = String.fromCharCode.apply(String, HEAPU8.subarray(ptr, ptr + Math.min(length, MAX_CHUNK)));
      ret = ret ? ret + curr : curr;
      ptr += MAX_CHUNK;
      length -= MAX_CHUNK;
    }
    return ret;
  }
  return Module['UTF8ToString'](ptr);
}
Module['Pointer_stringify'] = Pointer_stringify;

// Given a pointer 'ptr' to a null-terminated ASCII-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function AsciiToString(ptr) {
  var str = '';
  while (1) {
    var ch = HEAP8[((ptr++)>>0)];
    if (!ch) return str;
    str += String.fromCharCode(ch);
  }
}
Module['AsciiToString'] = AsciiToString;

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in ASCII form. The copy will require at most str.length+1 bytes of space in the HEAP.

function stringToAscii(str, outPtr) {
  return writeAsciiToMemory(str, outPtr, false);
}
Module['stringToAscii'] = stringToAscii;

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the given array that contains uint8 values, returns
// a copy of that string as a Javascript String object.

function UTF8ArrayToString(u8Array, idx) {
  var u0, u1, u2, u3, u4, u5;

  var str = '';
  while (1) {
    // For UTF8 byte structure, see http://en.wikipedia.org/wiki/UTF-8#Description and https://www.ietf.org/rfc/rfc2279.txt and https://tools.ietf.org/html/rfc3629
    u0 = u8Array[idx++];
    if (!u0) return str;
    if (!(u0 & 0x80)) { str += String.fromCharCode(u0); continue; }
    u1 = u8Array[idx++] & 63;
    if ((u0 & 0xE0) == 0xC0) { str += String.fromCharCode(((u0 & 31) << 6) | u1); continue; }
    u2 = u8Array[idx++] & 63;
    if ((u0 & 0xF0) == 0xE0) {
      u0 = ((u0 & 15) << 12) | (u1 << 6) | u2;
    } else {
      u3 = u8Array[idx++] & 63;
      if ((u0 & 0xF8) == 0xF0) {
        u0 = ((u0 & 7) << 18) | (u1 << 12) | (u2 << 6) | u3;
      } else {
        u4 = u8Array[idx++] & 63;
        if ((u0 & 0xFC) == 0xF8) {
          u0 = ((u0 & 3) << 24) | (u1 << 18) | (u2 << 12) | (u3 << 6) | u4;
        } else {
          u5 = u8Array[idx++] & 63;
          u0 = ((u0 & 1) << 30) | (u1 << 24) | (u2 << 18) | (u3 << 12) | (u4 << 6) | u5;
        }
      }
    }
    if (u0 < 0x10000) {
      str += String.fromCharCode(u0);
    } else {
      var ch = u0 - 0x10000;
      str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
    }
  }
}
Module['UTF8ArrayToString'] = UTF8ArrayToString;

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function UTF8ToString(ptr) {
  return UTF8ArrayToString(HEAPU8, ptr);
}
Module['UTF8ToString'] = UTF8ToString;

// Copies the given Javascript String object 'str' to the given byte array at address 'outIdx',
// encoded in UTF8 form and null-terminated. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outU8Array: the array to copy to. Each index in this array is assumed to be one 8-byte element.
//   outIdx: The starting offset in the array to begin the copying.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null 
//                    terminator, i.e. if maxBytesToWrite=1, only the null terminator will be written and nothing else.
//                    maxBytesToWrite=0 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8Array(str, outU8Array, outIdx, maxBytesToWrite) {
  if (!(maxBytesToWrite > 0)) // Parameter maxBytesToWrite is not optional. Negative values, 0, null, undefined and false each don't write out any bytes.
    return 0;

  var startIdx = outIdx;
  var endIdx = outIdx + maxBytesToWrite - 1; // -1 for string null terminator.
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    // For UTF8 byte structure, see http://en.wikipedia.org/wiki/UTF-8#Description and https://www.ietf.org/rfc/rfc2279.txt and https://tools.ietf.org/html/rfc3629
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) u = 0x10000 + ((u & 0x3FF) << 10) | (str.charCodeAt(++i) & 0x3FF);
    if (u <= 0x7F) {
      if (outIdx >= endIdx) break;
      outU8Array[outIdx++] = u;
    } else if (u <= 0x7FF) {
      if (outIdx + 1 >= endIdx) break;
      outU8Array[outIdx++] = 0xC0 | (u >> 6);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0xFFFF) {
      if (outIdx + 2 >= endIdx) break;
      outU8Array[outIdx++] = 0xE0 | (u >> 12);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0x1FFFFF) {
      if (outIdx + 3 >= endIdx) break;
      outU8Array[outIdx++] = 0xF0 | (u >> 18);
      outU8Array[outIdx++] = 0x80 | ((u >> 12) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0x3FFFFFF) {
      if (outIdx + 4 >= endIdx) break;
      outU8Array[outIdx++] = 0xF8 | (u >> 24);
      outU8Array[outIdx++] = 0x80 | ((u >> 18) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 12) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else {
      if (outIdx + 5 >= endIdx) break;
      outU8Array[outIdx++] = 0xFC | (u >> 30);
      outU8Array[outIdx++] = 0x80 | ((u >> 24) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 18) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 12) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    }
  }
  // Null-terminate the pointer to the buffer.
  outU8Array[outIdx] = 0;
  return outIdx - startIdx;
}
Module['stringToUTF8Array'] = stringToUTF8Array;

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF8 form. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8(str, outPtr, maxBytesToWrite) {
  return stringToUTF8Array(str, HEAPU8, outPtr, maxBytesToWrite);
}
Module['stringToUTF8'] = stringToUTF8;

// Returns the number of bytes the given Javascript string takes if encoded as a UTF8 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF8(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) u = 0x10000 + ((u & 0x3FF) << 10) | (str.charCodeAt(++i) & 0x3FF);
    if (u <= 0x7F) {
      ++len;
    } else if (u <= 0x7FF) {
      len += 2;
    } else if (u <= 0xFFFF) {
      len += 3;
    } else if (u <= 0x1FFFFF) {
      len += 4;
    } else if (u <= 0x3FFFFFF) {
      len += 5;
    } else {
      len += 6;
    }
  }
  return len;
}
Module['lengthBytesUTF8'] = lengthBytesUTF8;

// Given a pointer 'ptr' to a null-terminated UTF16LE-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function UTF16ToString(ptr) {
  var i = 0;

  var str = '';
  while (1) {
    var codeUnit = HEAP16[(((ptr)+(i*2))>>1)];
    if (codeUnit == 0)
      return str;
    ++i;
    // fromCharCode constructs a character from a UTF-16 code unit, so we can pass the UTF16 string right through.
    str += String.fromCharCode(codeUnit);
  }
}
Module['UTF16ToString'] = UTF16ToString;

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF16 form. The copy will require at most str.length*4+2 bytes of space in the HEAP.
// Use the function lengthBytesUTF16() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null 
//                    terminator, i.e. if maxBytesToWrite=2, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<2 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF16(str, outPtr, maxBytesToWrite) {
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 2) return 0;
  maxBytesToWrite -= 2; // Null terminator.
  var startPtr = outPtr;
  var numCharsToWrite = (maxBytesToWrite < str.length*2) ? (maxBytesToWrite / 2) : str.length;
  for (var i = 0; i < numCharsToWrite; ++i) {
    // charCodeAt returns a UTF-16 encoded code unit, so it can be directly written to the HEAP.
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    HEAP16[((outPtr)>>1)]=codeUnit;
    outPtr += 2;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP16[((outPtr)>>1)]=0;
  return outPtr - startPtr;
}
Module['stringToUTF16'] = stringToUTF16;

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF16(str) {
  return str.length*2;
}
Module['lengthBytesUTF16'] = lengthBytesUTF16;

function UTF32ToString(ptr) {
  var i = 0;

  var str = '';
  while (1) {
    var utf32 = HEAP32[(((ptr)+(i*4))>>2)];
    if (utf32 == 0)
      return str;
    ++i;
    // Gotcha: fromCharCode constructs a character from a UTF-16 encoded code (pair), not from a Unicode code point! So encode the code point to UTF-16 for constructing.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    if (utf32 >= 0x10000) {
      var ch = utf32 - 0x10000;
      str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
    } else {
      str += String.fromCharCode(utf32);
    }
  }
}
Module['UTF32ToString'] = UTF32ToString;

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF32 form. The copy will require at most str.length*4+4 bytes of space in the HEAP.
// Use the function lengthBytesUTF32() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null 
//                    terminator, i.e. if maxBytesToWrite=4, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<4 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF32(str, outPtr, maxBytesToWrite) {
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 4) return 0;
  var startPtr = outPtr;
  var endPtr = startPtr + maxBytesToWrite - 4;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) {
      var trailSurrogate = str.charCodeAt(++i);
      codeUnit = 0x10000 + ((codeUnit & 0x3FF) << 10) | (trailSurrogate & 0x3FF);
    }
    HEAP32[((outPtr)>>2)]=codeUnit;
    outPtr += 4;
    if (outPtr + 4 > endPtr) break;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP32[((outPtr)>>2)]=0;
  return outPtr - startPtr;
}
Module['stringToUTF32'] = stringToUTF32;

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF32(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i);
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) ++i; // possibly a lead surrogate, so skip over the tail surrogate.
    len += 4;
  }

  return len;
}
Module['lengthBytesUTF32'] = lengthBytesUTF32;

function demangle(func) {
  var hasLibcxxabi = !!Module['___cxa_demangle'];
  if (hasLibcxxabi) {
    try {
      var buf = _malloc(func.length);
      writeStringToMemory(func.substr(1), buf);
      var status = _malloc(4);
      var ret = Module['___cxa_demangle'](buf, 0, 0, status);
      if (getValue(status, 'i32') === 0 && ret) {
        return Pointer_stringify(ret);
      }
      // otherwise, libcxxabi failed, we can try ours which may return a partial result
    } catch(e) {
      // failure when using libcxxabi, we can try ours which may return a partial result
    } finally {
      if (buf) _free(buf);
      if (status) _free(status);
      if (ret) _free(ret);
    }
  }
  var i = 3;
  // params, etc.
  var basicTypes = {
    'v': 'void',
    'b': 'bool',
    'c': 'char',
    's': 'short',
    'i': 'int',
    'l': 'long',
    'f': 'float',
    'd': 'double',
    'w': 'wchar_t',
    'a': 'signed char',
    'h': 'unsigned char',
    't': 'unsigned short',
    'j': 'unsigned int',
    'm': 'unsigned long',
    'x': 'long long',
    'y': 'unsigned long long',
    'z': '...'
  };
  var subs = [];
  var first = true;
  function dump(x) {
    //return;
    if (x) Module.print(x);
    Module.print(func);
    var pre = '';
    for (var a = 0; a < i; a++) pre += ' ';
    Module.print (pre + '^');
  }
  function parseNested() {
    i++;
    if (func[i] === 'K') i++; // ignore const
    var parts = [];
    while (func[i] !== 'E') {
      if (func[i] === 'S') { // substitution
        i++;
        var next = func.indexOf('_', i);
        var num = func.substring(i, next) || 0;
        parts.push(subs[num] || '?');
        i = next+1;
        continue;
      }
      if (func[i] === 'C') { // constructor
        parts.push(parts[parts.length-1]);
        i += 2;
        continue;
      }
      var size = parseInt(func.substr(i));
      var pre = size.toString().length;
      if (!size || !pre) { i--; break; } // counter i++ below us
      var curr = func.substr(i + pre, size);
      parts.push(curr);
      subs.push(curr);
      i += pre + size;
    }
    i++; // skip E
    return parts;
  }
  function parse(rawList, limit, allowVoid) { // main parser
    limit = limit || Infinity;
    var ret = '', list = [];
    function flushList() {
      return '(' + list.join(', ') + ')';
    }
    var name;
    if (func[i] === 'N') {
      // namespaced N-E
      name = parseNested().join('::');
      limit--;
      if (limit === 0) return rawList ? [name] : name;
    } else {
      // not namespaced
      if (func[i] === 'K' || (first && func[i] === 'L')) i++; // ignore const and first 'L'
      var size = parseInt(func.substr(i));
      if (size) {
        var pre = size.toString().length;
        name = func.substr(i + pre, size);
        i += pre + size;
      }
    }
    first = false;
    if (func[i] === 'I') {
      i++;
      var iList = parse(true);
      var iRet = parse(true, 1, true);
      ret += iRet[0] + ' ' + name + '<' + iList.join(', ') + '>';
    } else {
      ret = name;
    }
    paramLoop: while (i < func.length && limit-- > 0) {
      //dump('paramLoop');
      var c = func[i++];
      if (c in basicTypes) {
        list.push(basicTypes[c]);
      } else {
        switch (c) {
          case 'P': list.push(parse(true, 1, true)[0] + '*'); break; // pointer
          case 'R': list.push(parse(true, 1, true)[0] + '&'); break; // reference
          case 'L': { // literal
            i++; // skip basic type
            var end = func.indexOf('E', i);
            var size = end - i;
            list.push(func.substr(i, size));
            i += size + 2; // size + 'EE'
            break;
          }
          case 'A': { // array
            var size = parseInt(func.substr(i));
            i += size.toString().length;
            if (func[i] !== '_') throw '?';
            i++; // skip _
            list.push(parse(true, 1, true)[0] + ' [' + size + ']');
            break;
          }
          case 'E': break paramLoop;
          default: ret += '?' + c; break paramLoop;
        }
      }
    }
    if (!allowVoid && list.length === 1 && list[0] === 'void') list = []; // avoid (void)
    if (rawList) {
      if (ret) {
        list.push(ret + '?');
      }
      return list;
    } else {
      return ret + flushList();
    }
  }
  var parsed = func;
  try {
    // Special-case the entry point, since its name differs from other name mangling.
    if (func == 'Object._main' || func == '_main') {
      return 'main()';
    }
    if (typeof func === 'number') func = Pointer_stringify(func);
    if (func[0] !== '_') return func;
    if (func[1] !== '_') return func; // C function
    if (func[2] !== 'Z') return func;
    switch (func[3]) {
      case 'n': return 'operator new()';
      case 'd': return 'operator delete()';
    }
    parsed = parse();
  } catch(e) {
    parsed += '?';
  }
  if (parsed.indexOf('?') >= 0 && !hasLibcxxabi) {
    Runtime.warnOnce('warning: a problem occurred in builtin C++ name demangling; build with  -s DEMANGLE_SUPPORT=1  to link in libcxxabi demangling');
  }
  return parsed;
}

function demangleAll(text) {
  return text.replace(/__Z[\w\d_]+/g, function(x) { var y = demangle(x); return x === y ? x : (x + ' [' + y + ']') });
}

function jsStackTrace() {
  var err = new Error();
  if (!err.stack) {
    // IE10+ special cases: It does have callstack info, but it is only populated if an Error object is thrown,
    // so try that as a special-case.
    try {
      throw new Error(0);
    } catch(e) {
      err = e;
    }
    if (!err.stack) {
      return '(no stack trace available)';
    }
  }
  return err.stack.toString();
}

function stackTrace() {
  return demangleAll(jsStackTrace());
}
Module['stackTrace'] = stackTrace;

// Memory management

var PAGE_SIZE = 4096;

function alignMemoryPage(x) {
  if (x % 4096 > 0) {
    x += (4096 - (x % 4096));
  }
  return x;
}

var HEAP;
var HEAP8, HEAPU8, HEAP16, HEAPU16, HEAP32, HEAPU32, HEAPF32, HEAPF64;

var STATIC_BASE = 0, STATICTOP = 0, staticSealed = false; // static area
var STACK_BASE = 0, STACKTOP = 0, STACK_MAX = 0; // stack area
var DYNAMIC_BASE = 0, DYNAMICTOP = 0; // dynamic area handled by sbrk


function enlargeMemory() {
  abort('Cannot enlarge memory arrays. Either (1) compile with -s TOTAL_MEMORY=X with X higher than the current value ' + TOTAL_MEMORY + ', (2) compile with ALLOW_MEMORY_GROWTH which adjusts the size at runtime but prevents some optimizations, or (3) set Module.TOTAL_MEMORY before the program runs.');
}


var TOTAL_STACK = Module['TOTAL_STACK'] || 5242880;
var TOTAL_MEMORY = Module['TOTAL_MEMORY'] || 16777216;

var totalMemory = 64*1024;
while (totalMemory < TOTAL_MEMORY || totalMemory < 2*TOTAL_STACK) {
  if (totalMemory < 16*1024*1024) {
    totalMemory *= 2;
  } else {
    totalMemory += 16*1024*1024
  }
}
if (totalMemory !== TOTAL_MEMORY) {
  Module.printErr('increasing TOTAL_MEMORY to ' + totalMemory + ' to be compliant with the asm.js spec (and given that TOTAL_STACK=' + TOTAL_STACK + ')');
  TOTAL_MEMORY = totalMemory;
}

// Initialize the runtime's memory
// check for full engine support (use string 'subarray' to avoid closure compiler confusion)
assert(typeof Int32Array !== 'undefined' && typeof Float64Array !== 'undefined' && !!(new Int32Array(1)['subarray']) && !!(new Int32Array(1)['set']),
       'JS engine does not provide full typed array support');

var buffer;
buffer = new ArrayBuffer(TOTAL_MEMORY);
HEAP8 = new Int8Array(buffer);
HEAP16 = new Int16Array(buffer);
HEAP32 = new Int32Array(buffer);
HEAPU8 = new Uint8Array(buffer);
HEAPU16 = new Uint16Array(buffer);
HEAPU32 = new Uint32Array(buffer);
HEAPF32 = new Float32Array(buffer);
HEAPF64 = new Float64Array(buffer);

// Endianness check (note: assumes compiler arch was little-endian)
HEAP32[0] = 255;
assert(HEAPU8[0] === 255 && HEAPU8[3] === 0, 'Typed arrays 2 must be run on a little-endian system');

Module['HEAP'] = HEAP;
Module['buffer'] = buffer;
Module['HEAP8'] = HEAP8;
Module['HEAP16'] = HEAP16;
Module['HEAP32'] = HEAP32;
Module['HEAPU8'] = HEAPU8;
Module['HEAPU16'] = HEAPU16;
Module['HEAPU32'] = HEAPU32;
Module['HEAPF32'] = HEAPF32;
Module['HEAPF64'] = HEAPF64;

function callRuntimeCallbacks(callbacks) {
  while(callbacks.length > 0) {
    var callback = callbacks.shift();
    if (typeof callback == 'function') {
      callback();
      continue;
    }
    var func = callback.func;
    if (typeof func === 'number') {
      if (callback.arg === undefined) {
        Runtime.dynCall('v', func);
      } else {
        Runtime.dynCall('vi', func, [callback.arg]);
      }
    } else {
      func(callback.arg === undefined ? null : callback.arg);
    }
  }
}

var __ATPRERUN__  = []; // functions called before the runtime is initialized
var __ATINIT__    = []; // functions called during startup
var __ATMAIN__    = []; // functions called when main() is to be run
var __ATEXIT__    = []; // functions called during shutdown
var __ATPOSTRUN__ = []; // functions called after the runtime has exited

var runtimeInitialized = false;
var runtimeExited = false;


function preRun() {
  // compatibility - merge in anything from Module['preRun'] at this time
  if (Module['preRun']) {
    if (typeof Module['preRun'] == 'function') Module['preRun'] = [Module['preRun']];
    while (Module['preRun'].length) {
      addOnPreRun(Module['preRun'].shift());
    }
  }
  callRuntimeCallbacks(__ATPRERUN__);
}

function ensureInitRuntime() {
  if (runtimeInitialized) return;
  runtimeInitialized = true;
  callRuntimeCallbacks(__ATINIT__);
}

function preMain() {
  callRuntimeCallbacks(__ATMAIN__);
}

function exitRuntime() {
  callRuntimeCallbacks(__ATEXIT__);
  runtimeExited = true;
}

function postRun() {
  // compatibility - merge in anything from Module['postRun'] at this time
  if (Module['postRun']) {
    if (typeof Module['postRun'] == 'function') Module['postRun'] = [Module['postRun']];
    while (Module['postRun'].length) {
      addOnPostRun(Module['postRun'].shift());
    }
  }
  callRuntimeCallbacks(__ATPOSTRUN__);
}

function addOnPreRun(cb) {
  __ATPRERUN__.unshift(cb);
}
Module['addOnPreRun'] = Module.addOnPreRun = addOnPreRun;

function addOnInit(cb) {
  __ATINIT__.unshift(cb);
}
Module['addOnInit'] = Module.addOnInit = addOnInit;

function addOnPreMain(cb) {
  __ATMAIN__.unshift(cb);
}
Module['addOnPreMain'] = Module.addOnPreMain = addOnPreMain;

function addOnExit(cb) {
  __ATEXIT__.unshift(cb);
}
Module['addOnExit'] = Module.addOnExit = addOnExit;

function addOnPostRun(cb) {
  __ATPOSTRUN__.unshift(cb);
}
Module['addOnPostRun'] = Module.addOnPostRun = addOnPostRun;

// Tools


function intArrayFromString(stringy, dontAddNull, length /* optional */) {
  var len = length > 0 ? length : lengthBytesUTF8(stringy)+1;
  var u8array = new Array(len);
  var numBytesWritten = stringToUTF8Array(stringy, u8array, 0, u8array.length);
  if (dontAddNull) u8array.length = numBytesWritten;
  return u8array;
}
Module['intArrayFromString'] = intArrayFromString;

function intArrayToString(array) {
  var ret = [];
  for (var i = 0; i < array.length; i++) {
    var chr = array[i];
    if (chr > 0xFF) {
      chr &= 0xFF;
    }
    ret.push(String.fromCharCode(chr));
  }
  return ret.join('');
}
Module['intArrayToString'] = intArrayToString;

function writeStringToMemory(string, buffer, dontAddNull) {
  var array = intArrayFromString(string, dontAddNull);
  var i = 0;
  while (i < array.length) {
    var chr = array[i];
    HEAP8[(((buffer)+(i))>>0)]=chr;
    i = i + 1;
  }
}
Module['writeStringToMemory'] = writeStringToMemory;

function writeArrayToMemory(array, buffer) {
  for (var i = 0; i < array.length; i++) {
    HEAP8[((buffer++)>>0)]=array[i];
  }
}
Module['writeArrayToMemory'] = writeArrayToMemory;

function writeAsciiToMemory(str, buffer, dontAddNull) {
  for (var i = 0; i < str.length; ++i) {
    HEAP8[((buffer++)>>0)]=str.charCodeAt(i);
  }
  // Null-terminate the pointer to the HEAP.
  if (!dontAddNull) HEAP8[((buffer)>>0)]=0;
}
Module['writeAsciiToMemory'] = writeAsciiToMemory;

function unSign(value, bits, ignore) {
  if (value >= 0) {
    return value;
  }
  return bits <= 32 ? 2*Math.abs(1 << (bits-1)) + value // Need some trickery, since if bits == 32, we are right at the limit of the bits JS uses in bitshifts
                    : Math.pow(2, bits)         + value;
}
function reSign(value, bits, ignore) {
  if (value <= 0) {
    return value;
  }
  var half = bits <= 32 ? Math.abs(1 << (bits-1)) // abs is needed if bits == 32
                        : Math.pow(2, bits-1);
  if (value >= half && (bits <= 32 || value > half)) { // for huge values, we can hit the precision limit and always get true here. so don't do that
                                                       // but, in general there is no perfect solution here. With 64-bit ints, we get rounding and errors
                                                       // TODO: In i64 mode 1, resign the two parts separately and safely
    value = -2*half + value; // Cannot bitshift half, as it may be at the limit of the bits JS uses in bitshifts
  }
  return value;
}


// check for imul support, and also for correctness ( https://bugs.webkit.org/show_bug.cgi?id=126345 )
if (!Math['imul'] || Math['imul'](0xffffffff, 5) !== -5) Math['imul'] = function imul(a, b) {
  var ah  = a >>> 16;
  var al = a & 0xffff;
  var bh  = b >>> 16;
  var bl = b & 0xffff;
  return (al*bl + ((ah*bl + al*bh) << 16))|0;
};
Math.imul = Math['imul'];


if (!Math['clz32']) Math['clz32'] = function(x) {
  x = x >>> 0;
  for (var i = 0; i < 32; i++) {
    if (x & (1 << (31 - i))) return i;
  }
  return 32;
};
Math.clz32 = Math['clz32']

var Math_abs = Math.abs;
var Math_cos = Math.cos;
var Math_sin = Math.sin;
var Math_tan = Math.tan;
var Math_acos = Math.acos;
var Math_asin = Math.asin;
var Math_atan = Math.atan;
var Math_atan2 = Math.atan2;
var Math_exp = Math.exp;
var Math_log = Math.log;
var Math_sqrt = Math.sqrt;
var Math_ceil = Math.ceil;
var Math_floor = Math.floor;
var Math_pow = Math.pow;
var Math_imul = Math.imul;
var Math_fround = Math.fround;
var Math_min = Math.min;
var Math_clz32 = Math.clz32;

// A counter of dependencies for calling run(). If we need to
// do asynchronous work before running, increment this and
// decrement it. Incrementing must happen in a place like
// PRE_RUN_ADDITIONS (used by emcc to add file preloading).
// Note that you can add dependencies in preRun, even though
// it happens right before run - run will be postponed until
// the dependencies are met.
var runDependencies = 0;
var runDependencyWatcher = null;
var dependenciesFulfilled = null; // overridden to take different actions when all run dependencies are fulfilled

function getUniqueRunDependency(id) {
  return id;
}

function addRunDependency(id) {
  runDependencies++;
  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }
}
Module['addRunDependency'] = addRunDependency;
function removeRunDependency(id) {
  runDependencies--;
  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }
  if (runDependencies == 0) {
    if (runDependencyWatcher !== null) {
      clearInterval(runDependencyWatcher);
      runDependencyWatcher = null;
    }
    if (dependenciesFulfilled) {
      var callback = dependenciesFulfilled;
      dependenciesFulfilled = null;
      callback(); // can add another dependenciesFulfilled
    }
  }
}
Module['removeRunDependency'] = removeRunDependency;

Module["preloadedImages"] = {}; // maps url to image data
Module["preloadedAudios"] = {}; // maps url to audio data



var memoryInitializer = null;



// === Body ===

var ASM_CONSTS = [];




STATIC_BASE = 8;

STATICTOP = STATIC_BASE + 21072;
  /* global initializers */  __ATINIT__.push();
  

/* memory initializer */ allocate([104,97,97,114,0,0,0,0,100,98,49,0,0,0,0,0,100,98,50,0,0,0,0,0,100,98,51,0,0,0,0,0,100,98,52,0,0,0,0,0,100,98,53,0,0,0,0,0,100,98,54,0,0,0,0,0,100,98,55,0,0,0,0,0,100,98,56,0,0,0,0,0,100,98,57,0,0,0,0,0,100,98,49,48,0,0,0,0,100,98,49,50,0,0,0,0,100,98,49,51,0,0,0,0,100,98,49,49,0,0,0,0,100,98,49,52,0,0,0,0,100,98,49,53,0,0,0,0,98,105,111,114,49,46,49,0,98,105,111,114,49,46,51,0,98,105,111,114,49,46,53,0,98,105,111,114,50,46,50,0,98,105,111,114,50,46,52,0,98,105,111,114,50,46,54,0,98,105,111,114,50,46,56,0,98,105,111,114,51,46,49,0,98,105,111,114,51,46,51,0,98,105,111,114,51,46,53,0,98,105,111,114,51,46,55,0,98,105,111,114,51,46,57,0,98,105,111,114,52,46,52,0,98,105,111,114,53,46,53,0,98,105,111,114,54,46,56,0,99,111,105,102,49,0,0,0,99,111,105,102,50,0,0,0,99,111,105,102,51,0,0,0,99,111,105,102,52,0,0,0,99,111,105,102,53,0,0,0,115,121,109,50,0,0,0,0,115,121,109,51,0,0,0,0,115,121,109,52,0,0,0,0,115,121,109,53,0,0,0,0,115,121,109,54,0,0,0,0,115,121,109,55,0,0,0,0,115,121,109,56,0,0,0,0,115,121,109,57,0,0,0,0,115,121,109,49,48,0,0,0,163,158,61,224,133,83,107,63,121,171,40,243,239,195,137,191,29,180,125,69,173,144,121,191,34,27,37,181,185,219,179,63,166,184,169,72,102,130,160,191,115,249,247,211,132,3,207,191,48,222,190,115,3,184,193,63,40,113,59,20,137,45,231,63,3,186,196,194,145,82,227,63,130,175,167,65,60,126,196,63,130,175,167,65,60,126,196,191,3,186,196,194,145,82,227,63,40,113,59,20,137,45,231,191,48,222,190,115,3,184,193,63,115,249,247,211,132,3,207,63,166,184,169,72,102,130,160,191,34,27,37,181,185,219,179,191,29,180,125,69,173,144,121,191,121,171,40,243,239,195,137,63,163,158,61,224,133,83,107,63,130,175,167,65,60,126,196,63,3,186,196,194,145,82,227,63,40,113,59,20,137,45,231,63,48,222,190,115,3,184,193,63,115,249,247,211,132,3,207,191,166,184,169,72,102,130,160,191,34,27,37,181,185,219,179,63,29,180,125,69,173,144,121,191,121,171,40,243,239,195,137,191,163,158,61,224,133,83,107,63,163,158,61,224,133,83,107,63,121,171,40,243,239,195,137,63,29,180,125,69,173,144,121,191,34,27,37,181,185,219,179,191,166,184,169,72,102,130,160,191,115,249,247,211,132,3,207,63,48,222,190,115,3,184,193,63,40,113,59,20,137,45,231,191,3,186,196,194,145,82,227,63,130,175,167,65,60,126,196,191,179,95,100,59,135,166,81,191,121,200,99,76,81,145,115,63,236,13,249,17,247,37,66,63,175,27,6,100,133,43,160,191,104,223,95,61,244,46,156,63,187,210,77,127,221,245,184,63,131,11,39,98,51,156,192,191,216,240,104,210,61,246,204,191,86,50,169,207,15,45,212,63,178,209,59,250,73,9,232,63,230,52,69,246,234,167,223,63,19,49,220,36,239,141,188,63,19,49,220,36,239,141,188,191,230,52,69,246,234,167,223,63,178,209,59,250,73,9,232,191,86,50,169,207,15,45,212,63,216,240,104,210,61,246,204,63,131,11,39,98,51,156,192,191,187,210,77,127,221,245,184,191,104,223,95,61,244,46,156,63,175,27,6,100,133,43,160,63,236,13,249,17,247,37,66,63,121,200,99,76,81,145,115,191,179,95,100,59,135,166,81,191,19,49,220,36,239,141,188,63,230,52,69,246,234,167,223,63,178,209,59,250,73,9,232,63,86,50,169,207,15,45,212,63,216,240,104,210,61,246,204,191,131,11,39,98,51,156,192,191,187,210,77,127,221,245,184,63,104,223,95,61,244,46,156,63,175,27,6,100,133,43,160,191,236,13,249,17,247,37,66,63,121,200,99,76,81,145,115,63,179,95,100,59,135,166,81,191,179,95,100,59,135,166,81,191,121,200,99,76,81,145,115,191,236,13,249,17,247,37,66,63,175,27,6,100,133,43,160,63,104,223,95,61,244,46,156,63,187,210,77,127,221,245,184,191,131,11,39,98,51,156,192,191,216,240,104,210,61,246,204,63,86,50,169,207,15,45,212,63,178,209,59,250,73,9,232,191,230,52,69,246,234,167,223,63,19,49,220,36,239,141,188,191,196,135,1,52,85,46,55,63,169,212,199,249,160,132,93,191,214,232,90,88,31,39,60,63,168,75,164,130,86,180,137,63,174,111,1,169,234,248,144,191,46,81,207,238,168,120,163,191,125,245,242,39,7,163,180,63,249,191,167,44,82,65,178,63,130,212,213,187,55,173,204,191,97,106,73,14,131,107,194,191,129,89,41,186,233,16,222,63,64,205,148,210,12,85,231,63,89,75,48,116,230,96,217,63,169,197,56,186,28,238,179,63,169,197,56,186,28,238,179,191,89,75,48,116,230,96,217,63,64,205,148,210,12,85,231,191,129,89,41,186,233,16,222,63,97,106,73,14,131,107,194,63,130,212,213,187,55,173,204,191,249,191,167,44,82,65,178,191,125,245,242,39,7,163,180,63,46,81,207,238,168,120,163,63,174,111,1,169,234,248,144,191,168,75,164,130,86,180,137,191,213,232,90,88,31,39,60,63,169,212,199,249,160,132,93,63,196,135,1,52,85,46,55,63,169,197,56,186,28,238,179,63,89,75,48,116,230,96,217,63,64,205,148,210,12,85,231,63,129,89,41,186,233,16,222,63,97,106,73,14,131,107,194,191,130,212,213,187,55,173,204,191,249,191,167,44,82,65,178,63,125,245,242,39,7,163,180,63,46,81,207,238,168,120,163,191,174,111,1,169,234,248,144,191,168,75,164,130,86,180,137,63,214,232,90,88,31,39,60,63,169,212,199,249,160,132,93,191,196,135,1,52,85,46,55,63,196,135,1,52,85,46,55,63,169,212,199,249,160,132,93,63,214,232,90,88,31,39,60,63,166,75,164,130,86,180,137,191,174,111,1,169,234,248,144,191,46,81,207,238,168,120,163,63,125,245,242,39,7,163,180,63,249,191,167,44,82,65,178,191,130,212,213,187,55,173,204,191,97,106,73,14,131,107,194,63,129,89,41,186,233,16,222,63,64,205,148,210,12,85,231,191,89,75,48,116,230,96,217,63,169,197,56,186,28,238,179,191,28,87,4,200,187,203,30,191,201,85,239,141,20,34,70,63,150,170,45,23,80,172,57,191,151,5,51,109,239,242,115,191,130,135,50,223,120,233,129,63,202,39,88,205,21,162,140,63,220,23,139,81,188,146,166,191,48,66,8,15,66,201,145,191,224,144,99,177,203,122,192,63,58,57,14,203,249,246,62,63,113,19,37,135,79,45,210,191,100,39,153,69,129,53,144,191,118,117,219,190,57,187,226,63,209,80,146,89,196,158,229,63,204,206,180,144,22,6,212,63,65,252,162,173,100,220,171,63,65,252,162,173,100,220,171,191,204,206,180,144,22,6,212,63,209,80,146,89,196,158,229,191,118,117,219,190,57,187,226,63,100,39,153,69,129,53,144,63,113,19,37,135,79,45,210,191,58,57,14,203,249,246,62,191,224,144,99,177,203,122,192,63,48,66,8,15,66,201,145,63,220,23,139,81,188,146,166,191,202,39,88,205,21,162,140,191,130,135,50,223,120,233,129,63,151,5,51,109,239,242,115,63,150,170,45,23,80,172,57,191,201,85,239,141,20,34,70,191,28,87,4,200,187,203,30,191,65,252,162,173,100,220,171,63,204,206,180,144,22,6,212,63,209,80,146,89,196,158,229,63,118,117,219,190,57,187,226,63,100,39,153,69,129,53,144,191,113,19,37,135,79,45,210,191,58,57,14,203,249,246,62,63,224,144,99,177,203,122,192,63,48,66,8,15,66,201,145,191,220,23,139,81,188,146,166,191,202,39,88,205,21,162,140,63,130,135,50,223,120,233,129,63,151,5,51,109,239,242,115,191,150,170,45,23,80,172,57,191,201,85,239,141,20,34,70,63,28,87,4,200,187,203,30,191,28,87,4,200,187,203,30,191,201,85,239,141,20,34,70,191,150,170,45,23,80,172,57,191,151,5,51,109,239,242,115,63,130,135,50,223,120,233,129,63,202,39,88,205,21,162,140,191,220,23,139,81,188,146,166,191,48,66,8,15,66,201,145,63,224,144,99,177,203,122,192,63,58,57,14,203,249,246,62,191,113,19,37,135,79,45,210,191,100,39,153,69,129,53,144,63,118,117,219,190,57,187,226,63,209,80,146,89,196,158,229,191,204,206,180,144,22,6,212,63,65,252,162,173,100,220,171,191,76,208,217,158,27,161,4,63,124,52,19,168,61,131,48,191,172,38,198,164,118,50,46,63,245,191,245,187,151,69,94,63,132,235,169,100,123,137,113,191,183,193,122,159,163,88,115,191,12,208,4,190,249,229,150,63,82,246,227,166,49,114,48,63,75,85,242,148,98,80,177,191,218,7,99,148,141,118,159,63,126,16,66,30,98,3,195,63,157,74,218,188,142,202,184,191,87,132,251,102,255,196,210,191,96,96,2,168,156,12,193,63,89,250,230,1,129,8,229,63,111,205,1,6,182,90,227,63,228,139,153,128,249,53,207,63,49,74,63,229,243,126,163,63,49,74,63,229,243,126,163,191,228,139,153,128,249,53,207,63,111,205,1,6,182,90,227,191,89,250,230,1,129,8,229,63,96,96,2,168,156,12,193,191,87,132,251,102,255,196,210,191,157,74,218,188,142,202,184,63,126,16,66,30,98,3,195,63,218,7,99,148,141,118,159,191,75,85,242,148,98,80,177,191,82,246,227,166,49,114,48,191,12,208,4,190,249,229,150,63,183,193,122,159,163,88,115,63,132,235,169,100,123,137,113,191,245,191,245,187,151,69,94,191,172,38,198,164,118,50,46,63,124,52,19,168,61,131,48,63,76,208,217,158,27,161,4,63,49,74,63,229,243,126,163,63,228,139,153,128,249,53,207,63,111,205,1,6,182,90,227,63,89,250,230,1,129,8,229,63,96,96,2,168,156,12,193,63,87,132,251,102,255,196,210,191,157,74,218,188,142,202,184,191,126,16,66,30,98,3,195,63,218,7,99,148,141,118,159,63,75,85,242,148,98,80,177,191,82,246,227,166,49,114,48,63,12,208,4,190,249,229,150,63,183,193,122,159,163,88,115,191,132,235,169,100,123,137,113,191,245,191,245,187,151,69,94,63,172,38,198,164,118,50,46,63,124,52,19,168,61,131,48,191,76,208,217,158,27,161,4,63,76,208,217,158,27,161,4,63,124,52,19,168,61,131,48,63,172,38,198,164,118,50,46,63,245,191,245,187,151,69,94,191,132,235,169,100,123,137,113,191,183,193,122,159,163,88,115,63,12,208,4,190,249,229,150,63,82,246,227,166,49,114,48,191,75,85,242,148,98,80,177,191,218,7,99,148,141,118,159,191,126,16,66,30,98,3,195,63,157,74,218,188,142,202,184,63,87,132,251,102,255,196,210,191,96,96,2,168,156,12,193,191,89,250,230,1,129,8,229,63,111,205,1,6,182,90,227,191,228,139,153,128,249,53,207,63,49,74,63,229,243,126,163,191,68,139,229,45,42,209,235,190,140,192,147,27,161,136,24,63,205,110,68,85,245,135,30,191,47,165,146,9,98,121,70,191,151,183,162,114,96,82,96,63,60,21,170,135,135,220,86,63,139,248,115,109,70,251,133,191,85,57,227,179,125,139,109,63,175,169,202,105,64,1,161,63,204,121,21,213,29,42,158,191,207,3,52,7,227,70,178,191,124,19,160,129,155,210,183,63,100,167,160,119,163,77,192,63,45,58,28,124,196,20,201,191,137,157,192,182,247,250,207,191,199,234,35,73,186,254,209,63,223,179,98,64,219,7,230,63,26,220,27,7,213,222,224,63,30,131,25,105,45,22,200,63,234,55,222,73,101,79,155,63,234,55,222,73,101,79,155,191,30,131,25,105,45,22,200,63,26,220,27,7,213,222,224,191,223,179,98,64,219,7,230,63,199,234,35,73,186,254,209,191,137,157,192,182,247,250,207,191,45,58,28,124,196,20,201,63,100,167,160,119,163,77,192,63,124,19,160,129,155,210,183,191,207,3,52,7,227,70,178,191,204,121,21,213,29,42,158,63,175,169,202,105,64,1,161,63,85,57,227,179,125,139,109,191,139,248,115,109,70,251,133,191,60,21,170,135,135,220,86,191,151,183,162,114,96,82,96,63,47,165,146,9,98,121,70,63,205,110,68,85,245,135,30,191,140,192,147,27,161,136,24,191,68,139,229,45,42,209,235,190,234,55,222,73,101,79,155,63,30,131,25,105,45,22,200,63,26,220,27,7,213,222,224,63,223,179,98,64,219,7,230,63,199,234,35,73,186,254,209,63,137,157,192,182,247,250,207,191,45,58,28,124,196,20,201,191,100,167,160,119,163,77,192,63,124,19,160,129,155,210,183,63,207,3,52,7,227,70,178,191,204,121,21,213,29,42,158,191,175,169,202,105,64,1,161,63,85,57,227,179,125,139,109,63,139,248,115,109,70,251,133,191,60,21,170,135,135,220,86,63,151,183,162,114,96,82,96,63,47,165,146,9,98,121,70,191,205,110,68,85,245,135,30,191,140,192,147,27,161,136,24,63,68,139,229,45,42,209,235,190,68,139,229,45,42,209,235,190,140,192,147,27,161,136,24,191,205,110,68,85,245,135,30,191,47,165,146,9,98,121,70,63,151,183,162,114,96,82,96,63,60,21,170,135,135,220,86,191,139,248,115,109,70,251,133,191,85,57,227,179,125,139,109,191,175,169,202,105,64,1,161,63,204,121,21,213,29,42,158,63,207,3,52,7,227,70,178,191,124,19,160,129,155,210,183,191,100,167,160,119,163,77,192,63,45,58,28,124,196,20,201,63,137,157,192,182,247,250,207,191,199,234,35,73,186,254,209,191,223,179,98,64,219,7,230,63,26,220,27,7,213,222,224,191,30,131,25,105,45,22,200,63,234,55,222,73,101,79,155,191,155,194,125,45,80,167,185,190,203,65,12,241,146,203,234,62,31,63,246,86,74,107,249,190,148,180,184,4,105,51,23,191,164,11,190,49,132,120,57,63,15,4,204,36,199,115,219,62,193,192,110,24,192,218,97,191,108,209,248,209,171,107,98,63,22,165,255,75,132,125,123,63,131,40,62,98,74,76,138,191,107,52,63,81,23,6,137,191,125,80,57,190,141,69,165,63,167,82,28,157,17,56,134,63,152,48,25,130,198,175,184,191,228,89,141,135,234,243,117,63,170,221,86,126,117,91,199,63,57,174,243,240,150,89,152,191,175,90,164,141,68,60,212,191,12,135,217,193,74,235,166,191,241,120,41,92,36,130,224,63,219,110,142,157,197,7,229,63,35,64,46,39,150,38,216,63,205,247,130,6,137,12,188,63,182,249,210,120,153,218,138,63,182,249,210,120,153,218,138,191,205,247,130,6,137,12,188,63,35,64,46,39,150,38,216,191,219,110,142,157,197,7,229,63,241,120,41,92,36,130,224,191,12,135,217,193,74,235,166,191,175,90,164,141,68,60,212,63,57,174,243,240,150,89,152,191,170,221,86,126,117,91,199,191,228,89,141,135,234,243,117,63,152,48,25,130,198,175,184,63,167,82,28,157,17,56,134,63,125,80,57,190,141,69,165,191,107,52,63,81,23,6,137,191,131,40,62,98,74,76,138,63,22,165,255,75,132,125,123,63,108,209,248,209,171,107,98,191,193,192,110,24,192,218,97,191,15,4,204,36,199,115,219,190,164,11,190,49,132,120,57,63,148,180,184,4,105,51,23,63,31,63,246,86,74,107,249,190,203,65,12,241,146,203,234,190,155,194,125,45,80,167,185,190,182,249,210,120,153,218,138,63,205,247,130,6,137,12,188,63,35,64,46,39,150,38,216,63,219,110,142,157,197,7,229,63,241,120,41,92,36,130,224,63,12,135,217,193,74,235,166,191,175,90,164,141,68,60,212,191,57,174,243,240,150,89,152,191,170,221,86,126,117,91,199,63,228,89,141,135,234,243,117,63,152,48,25,130,198,175,184,191,167,82,28,157,17,56,134,63,125,80,57,190,141,69,165,63,107,52,63,81,23,6,137,191,131,40,62,98,74,76,138,191,22,165,255,75,132,125,123,63,108,209,248,209,171,107,98,63,193,192,110,24,192,218,97,191,15,4,204,36,199,115,219,62,164,11,190,49,132,120,57,63,148,180,184,4,105,51,23,191,31,63,246,86,74,107,249,190,203,65,12,241,146,203,234,62,155,194,125,45,80,167,185,190,155,194,125,45,80,167,185,190,203,65,12,241,146,203,234,190,31,63,246,86,74,107,249,190,148,180,184,4,105,51,23,63,164,11,190,49,132,120,57,63,15,4,204,36,199,115,219,190,193,192,110,24,192,218,97,191,108,209,248,209,171,107,98,191,22,165,255,75,132,125,123,63,131,40,62,98,74,76,138,63,107,52,63,81,23,6,137,191,125,80,57,190,141,69,165,191,167,82,28,157,17,56,134,63,152,48,25,130,198,175,184,63,228,89,141,135,234,243,117,63,170,221,86,126,117,91,199,191,57,174,243,240,150,89,152,191,175,90,164,141,68,60,212,63,12,135,217,193,74,235,166,191,241,120,41,92,36,130,224,191,219,110,142,157,197,7,229,63,35,64,46,39,150,38,216,191,205,247,130,6,137,12,188,63,182,249,210,120,153,218,138,191,99,163,141,219,249,131,161,62,237,189,195,164,8,183,211,190,193,215,140,2,248,229,229,62,43,66,85,134,154,21,0,63,172,100,0,54,207,164,37,191,155,21,127,132,109,210,9,63,39,76,226,30,235,140,78,63,143,44,31,27,86,142,85,191,200,125,172,205,37,160,102,191,45,36,23,115,9,184,125,63,104,155,26,3,141,18,112,63,118,181,69,146,67,103,152,191,117,171,227,223,41,127,99,63,70,9,82,68,80,190,172,63,60,153,35,222,198,31,155,191,232,232,200,67,53,22,187,191,205,127,85,4,200,172,178,63,48,28,200,121,18,249,198,63,247,212,106,184,66,228,191,191,93,197,111,32,132,40,212,191,20,15,41,81,178,68,182,63,51,164,190,240,46,216,226,63,128,52,24,0,197,141,227,63,175,175,9,108,191,247,211,63,16,61,138,252,100,54,181,63,8,126,211,116,145,216,130,63,8,126,211,116,145,216,130,191,16,61,138,252,100,54,181,63,175,175,9,108,191,247,211,191,128,52,24,0,197,141,227,63,51,164,190,240,46,216,226,191,20,15,41,81,178,68,182,63,93,197,111,32,132,40,212,63,247,212,106,184,66,228,191,191,48,28,200,121,18,249,198,191,205,127,85,4,200,172,178,63,232,232,200,67,53,22,187,63,60,153,35,222,198,31,155,191,70,9,82,68,80,190,172,191,117,171,227,223,41,127,99,63,118,181,69,146,67,103,152,63,104,155,26,3,141,18,112,63,45,36,23,115,9,184,125,191,200,125,172,205,37,160,102,191,143,44,31,27,86,142,85,63,39,76,226,30,235,140,78,63,155,21,127,132,109,210,9,191,172,100,0,54,207,164,37,191,43,66,85,134,154,21,0,191,193,215,140,2,248,229,229,62,237,189,195,164,8,183,211,62,99,163,141,219,249,131,161,62,8,126,211,116,145,216,130,63,16,61,138,252,100,54,181,63,175,175,9,108,191,247,211,63,128,52,24,0,197,141,227,63,51,164,190,240,46,216,226,63,20,15,41,81,178,68,182,63,93,197,111,32,132,40,212,191,247,212,106,184,66,228,191,191,48,28,200,121,18,249,198,63,205,127,85,4,200,172,178,63,232,232,200,67,53,22,187,191,60,153,35,222,198,31,155,191,70,9,82,68,80,190,172,63,117,171,227,223,41,127,99,63,118,181,69,146,67,103,152,191,104,155,26,3,141,18,112,63,45,36,23,115,9,184,125,63,200,125,172,205,37,160,102,191,143,44,31,27,86,142,85,191,39,76,226,30,235,140,78,63,155,21,127,132,109,210,9,63,172,100,0,54,207,164,37,191,43,66,85,134,154,21,0,63,193,215,140,2,248,229,229,62,237,189,195,164,8,183,211,190,99,163,141,219,249,131,161,62,99,163,141,219,249,131,161,62,237,189,195,164,8,183,211,62,193,215,140,2,248,229,229,62,43,66,85,134,154,21,0,191,172,100,0,54,207,164,37,191,155,21,127,132,109,210,9,191,39,76,226,30,235,140,78,63,143,44,31,27,86,142,85,63,200,125,172,205,37,160,102,191,45,36,23,115,9,184,125,191,104,155,26,3,141,18,112,63,118,181,69,146,67,103,152,63,117,171,227,223,41,127,99,63,70,9,82,68,80,190,172,191,60,153,35,222,198,31,155,191,232,232,200,67,53,22,187,63,205,127,85,4,200,172,178,63,48,28,200,121,18,249,198,191,247,212,106,184,66,228,191,191,93,197,111,32,132,40,212,63,20,15,41,81,178,68,182,63,51,164,190,240,46,216,226,191,128,52,24,0,197,141,227,63,175,175,9,108,191,247,211,191,16,61,138,252,100,54,181,63,8,126,211,116,145,216,130,191,189,12,225,180,176,217,210,62,218,61,187,254,160,40,2,191,3,59,25,93,176,138,12,63,248,109,188,240,21,84,48,63,56,173,253,211,56,67,77,191,91,18,132,60,84,57,52,191,124,248,117,10,210,47,116,63,13,107,52,243,73,94,107,191,88,131,104,90,151,119,143,191,190,138,149,106,81,87,149,63,43,128,106,39,39,11,160,63,21,254,190,219,33,2,177,191,128,149,74,126,56,204,167,191,201,167,11,63,10,45,195,63,185,79,106,139,59,232,176,63,254,202,168,137,255,140,209,191,185,244,43,107,111,197,196,191,89,231,239,204,159,93,218,63,5,18,50,100,37,241,229,63,205,200,152,97,40,203,220,63,30,20,35,196,201,112,194,63,216,95,7,22,153,36,147,63,216,95,7,22,153,36,147,191,30,20,35,196,201,112,194,63,205,200,152,97,40,203,220,191,5,18,50,100,37,241,229,63,89,231,239,204,159,93,218,191,185,244,43,107,111,197,196,191,254,202,168,137,255,140,209,63,185,79,106,139,59,232,176,63,201,167,11,63,10,45,195,191,128,149,74,126,56,204,167,191,21,254,190,219,33,2,177,63,43,128,106,39,39,11,160,63,190,138,149,106,81,87,149,191,88,131,104,90,151,119,143,191,13,107,52,243,73,94,107,63,124,248,117,10,210,47,116,63,91,18,132,60,84,57,52,63,56,173,253,211,56,67,77,191,248,109,188,240,21,84,48,191,3,59,25,93,176,138,12,63,218,61,187,254,160,40,2,63,189,12,225,180,176,217,210,62,216,95,7,22,153,36,147,63,30,20,35,196,201,112,194,63,205,200,152,97,40,203,220,63,5,18,50,100,37,241,229,63,89,231,239,204,159,93,218,63,185,244,43,107,111,197,196,191,254,202,168,137,255,140,209,191,185,79,106,139,59,232,176,63,201,167,11,63,10,45,195,63,128,149,74,126,56,204,167,191,21,254,190,219,33,2,177,191,43,128,106,39,39,11,160,63,190,138,149,106,81,87,149,63,88,131,104,90,151,119,143,191,13,107,52,243,73,94,107,191,124,248,117,10,210,47,116,63,91,18,132,60,84,57,52,191,56,173,253,211,56,67,77,191,248,109,188,240,21,84,48,63,3,59,25,93,176,138,12,63,218,61,187,254,160,40,2,191,189,12,225,180,176,217,210,62,189,12,225,180,176,217,210,62,218,61,187,254,160,40,2,63,3,59,25,93,176,138,12,63,248,109,188,240,21,84,48,191,56,173,253,211,56,67,77,191,91,18,132,60,84,57,52,63,124,248,117,10,210,47,116,63,13,107,52,243,73,94,107,63,88,131,104,90,151,119,143,191,190,138,149,106,81,87,149,191,43,128,106,39,39,11,160,63,21,254,190,219,33,2,177,63,128,149,74,126,56,204,167,191,201,167,11,63,10,45,195,191,185,79,106,139,59,232,176,63,254,202,168,137,255,140,209,63,185,244,43,107,111,197,196,191,89,231,239,204,159,93,218,191,5,18,50,100,37,241,229,63,205,200,152,97,40,203,220,191,30,20,35,196,201,112,194,63,216,95,7,22,153,36,147,191,47,103,196,240,144,252,135,190,36,54,19,180,203,240,188,62,236,178,63,229,104,105,210,190,219,88,79,54,191,173,229,190,218,15,232,162,18,6,18,63,229,0,2,60,63,231,5,191,73,212,16,144,246,89,57,191,251,135,229,217,79,51,71,63,200,198,110,31,14,101,81,63,200,9,68,33,71,137,111,191,158,12,200,19,189,115,72,191,78,161,125,186,96,49,138,63,47,79,146,97,206,255,118,191,102,128,188,166,232,232,158,191,108,113,33,169,3,161,155,63,97,244,157,101,10,72,172,63,133,219,243,71,8,81,178,191,85,60,170,214,36,53,182,191,212,196,3,252,40,235,193,63,58,113,188,50,239,182,193,63,94,75,96,208,133,232,203,191,30,2,175,97,88,99,209,191,246,82,66,174,102,253,203,63,2,50,49,220,176,50,228,63,9,110,161,34,223,188,225,63,32,240,98,127,119,79,208,63,146,176,92,15,70,238,175,63,28,93,227,172,2,119,122,63,28,93,227,172,2,119,122,191,146,176,92,15,70,238,175,63,32,240,98,127,119,79,208,191,9,110,161,34,223,188,225,63,2,50,49,220,176,50,228,191,246,82,66,174,102,253,203,63,30,2,175,97,88,99,209,63,94,75,96,208,133,232,203,191,58,113,188,50,239,182,193,191,212,196,3,252,40,235,193,63,85,60,170,214,36,53,182,63,133,219,243,71,8,81,178,191,97,244,157,101,10,72,172,191,108,113,33,169,3,161,155,63,102,128,188,166,232,232,158,63,47,79,146,97,206,255,118,191,78,161,125,186,96,49,138,191,158,12,200,19,189,115,72,191,200,9,68,33,71,137,111,63,200,198,110,31,14,101,81,63,251,135,229,217,79,51,71,191,73,212,16,144,246,89,57,191,229,0,2,60,63,231,5,63,218,15,232,162,18,6,18,63,219,88,79,54,191,173,229,62,236,178,63,229,104,105,210,190,36,54,19,180,203,240,188,190,47,103,196,240,144,252,135,190,28,93,227,172,2,119,122,63,146,176,92,15,70,238,175,63,32,240,98,127,119,79,208,63,9,110,161,34,223,188,225,63,2,50,49,220,176,50,228,63,246,82,66,174,102,253,203,63,30,2,175,97,88,99,209,191,94,75,96,208,133,232,203,191,58,113,188,50,239,182,193,63,212,196,3,252,40,235,193,63,85,60,170,214,36,53,182,191,133,219,243,71,8,81,178,191,97,244,157,101,10,72,172,63,108,113,33,169,3,161,155,63,102,128,188,166,232,232,158,191,47,79,146,97,206,255,118,191,78,161,125,186,96,49,138,63,158,12,200,19,189,115,72,191,200,9,68,33,71,137,111,191,200,198,110,31,14,101,81,63,251,135,229,217,79,51,71,63,73,212,16,144,246,89,57,191,229,0,2,60,63,231,5,191,218,15,232,162,18,6,18,63,219,88,79,54,191,173,229,190,236,178,63,229,104,105,210,190,36,54,19,180,203,240,188,62,47,103,196,240,144,252,135,190,47,103,196,240,144,252,135,190,36,54,19,180,203,240,188,190,236,178,63,229,104,105,210,190,219,88,79,54,191,173,229,62,218,15,232,162,18,6,18,63,229,0,2,60,63,231,5,63,73,212,16,144,246,89,57,191,251,135,229,217,79,51,71,191,200,198,110,31,14,101,81,63,200,9,68,33,71,137,111,63,158,12,200,19,189,115,72,191,78,161,125,186,96,49,138,191,47,79,146,97,206,255,118,191,102,128,188,166,232,232,158,63,108,113,33,169,3,161,155,63,97,244,157,101,10,72,172,191,133,219,243,71,8,81,178,191,85,60,170,214,36,53,182,63,212,196,3,252,40,235,193,63,58,113,188,50,239,182,193,191,94,75,96,208,133,232,203,191,30,2,175,97,88,99,209,63,246,82,66,174,102,253,203,63,2,50,49,220,176,50,228,191,9,110,161,34,223,188,225,63,32,240,98,127,119,79,208,191,146,176,92,15,70,238,175,63,28,93,227,172,2,119,122,191,21,106,51,22,208,118,112,62,121,183,28,29,41,50,165,190,149,63,228,223,88,99,190,62,102,40,84,193,245,53,204,62,54,110,244,101,249,127,253,190,123,191,32,202,172,11,251,62,225,82,203,166,4,111,36,63,131,214,30,247,128,144,55,191,168,160,36,127,254,121,56,191,84,216,61,60,228,214,95,63,85,12,92,113,0,176,47,191,114,177,47,252,225,146,122,191,55,49,153,91,201,228,116,63,132,112,0,53,81,228,142,63,105,120,160,211,58,79,149,191,140,29,42,151,170,98,154,191,224,99,111,66,50,12,172,63,253,152,119,82,88,88,161,63,149,47,46,242,107,114,188,191,197,227,32,6,32,79,164,191,116,24,13,63,186,86,200,63,45,197,120,51,98,182,176,63,215,216,40,110,13,125,210,191,136,81,137,202,233,186,200,191,190,121,194,176,55,178,213,63,44,41,165,81,128,170,228,63,227,51,29,105,71,135,223,63,205,123,2,111,253,94,202,63,244,155,188,16,192,238,167,63,242,102,162,88,0,151,114,63,242,102,162,88,0,151,114,191,244,155,188,16,192,238,167,63,205,123,2,111,253,94,202,191,227,51,29,105,71,135,223,63,44,41,165,81,128,170,228,191,190,121,194,176,55,178,213,63,136,81,137,202,233,186,200,63,215,216,40,110,13,125,210,191,45,197,120,51,98,182,176,191,116,24,13,63,186,86,200,63,197,227,32,6,32,79,164,63,149,47,46,242,107,114,188,191,253,152,119,82,88,88,161,191,224,99,111,66,50,12,172,63,140,29,42,151,170,98,154,63,105,120,160,211,58,79,149,191,132,112,0,53,81,228,142,191,55,49,153,91,201,228,116,63,114,177,47,252,225,146,122,63,85,12,92,113,0,176,47,191,84,216,61,60,228,214,95,191,168,160,36,127,254,121,56,191,131,214,30,247,128,144,55,63,225,82,203,166,4,111,36,63,123,191,32,202,172,11,251,190,54,110,244,101,249,127,253,190,102,40,84,193,245,53,204,190,149,63,228,223,88,99,190,62,121,183,28,29,41,50,165,62,21,106,51,22,208,118,112,62,242,102,162,88,0,151,114,63,244,155,188,16,192,238,167,63,205,123,2,111,253,94,202,63,227,51,29,105,71,135,223,63,44,41,165,81,128,170,228,63,190,121,194,176,55,178,213,63,136,81,137,202,233,186,200,191,215,216,40,110,13,125,210,191,45,197,120,51,98,182,176,63,116,24,13,63,186,86,200,63,197,227,32,6,32,79,164,191,149,47,46,242,107,114,188,191,253,152,119,82,88,88,161,63,224,99,111,66,50,12,172,63,140,29,42,151,170,98,154,191,105,120,160,211,58,79,149,191,132,112,0,53,81,228,142,63,55,49,153,91,201,228,116,63,114,177,47,252,225,146,122,191,85,12,92,113,0,176,47,191,84,216,61,60,228,214,95,63,168,160,36,127,254,121,56,191,131,214,30,247,128,144,55,191,225,82,203,166,4,111,36,63,123,191,32,202,172,11,251,62,54,110,244,101,249,127,253,190,102,40,84,193,245,53,204,62,149,63,228,223,88,99,190,62,121,183,28,29,41,50,165,190,21,106,51,22,208,118,112,62,21,106,51,22,208,118,112,62,121,183,28,29,41,50,165,62,149,63,228,223,88,99,190,62,102,40,84,193,245,53,204,190,54,110,244,101,249,127,253,190,123,191,32,202,172,11,251,190,225,82,203,166,4,111,36,63,131,214,30,247,128,144,55,63,168,160,36,127,254,121,56,191,84,216,61,60,228,214,95,191,85,12,92,113,0,176,47,191,114,177,47,252,225,146,122,63,55,49,153,91,201,228,116,63,132,112,0,53,81,228,142,191,105,120,160,211,58,79,149,191,140,29,42,151,170,98,154,63,224,99,111,66,50,12,172,63,253,152,119,82,88,88,161,191,149,47,46,242,107,114,188,191,197,227,32,6,32,79,164,63,116,24,13,63,186,86,200,63,45,197,120,51,98,182,176,191,215,216,40,110,13,125,210,191,136,81,137,202,233,186,200,63,190,121,194,176,55,178,213,63,44,41,165,81,128,170,228,191,227,51,29,105,71,135,223,63,205,123,2,111,253,94,202,191,244,155,188,16,192,238,167,63,242,102,162,88,0,151,114,191,218,108,223,204,118,248,144,63,218,108,223,204,118,248,144,191,58,242,238,204,217,28,191,191,58,242,238,204,217,28,191,63,205,59,127,102,158,160,230,63,205,59,127,102,158,160,230,63,58,242,238,204,217,28,191,63,58,242,238,204,217,28,191,191,218,108,223,204,118,248,144,191,218,108,223,204,118,248,144,63,218,108,223,204,118,248,144,63,218,108,223,204,118,248,144,63,58,242,238,204,217,28,191,191,58,242,238,204,217,28,191,191,205,59,127,102,158,160,230,63,205,59,127,102,158,160,230,191,58,242,238,204,217,28,191,63,58,242,238,204,217,28,191,63,218,108,223,204,118,248,144,191,218,108,223,204,118,248,144,191,0,0,0,0,0,0,0,0,218,108,223,204,118,248,160,63,218,108,223,204,118,248,176,191,205,59,127,102,158,160,198,191,3,23,183,25,188,222,218,63,24,236,34,192,222,209,239,63,3,23,183,25,188,222,218,63,205,59,127,102,158,160,198,191,218,108,223,204,118,248,176,191,218,108,223,204,118,248,160,63,0,0,0,0,0,0,0,0,218,108,223,204,118,248,160,191,218,108,223,204,118,248,176,191,205,59,127,102,158,160,198,63,3,23,183,25,188,222,218,63,24,236,34,192,222,209,239,191,3,23,183,25,188,222,218,63,205,59,127,102,158,160,198,63,218,108,223,204,118,248,176,191,218,108,223,204,118,248,160,191,0,0,0,0,0,0,0,0,192,10,31,0,198,72,124,191,192,10,31,0,198,72,140,63,138,47,231,76,168,10,168,63,226,16,235,12,193,147,187,191,119,67,126,54,88,190,197,191,175,7,185,121,72,163,220,63,194,243,33,144,152,239,238,63,175,7,185,121,72,163,220,63,119,67,126,54,88,190,197,191,226,16,235,12,193,147,187,191,138,47,231,76,168,10,168,63,192,10,31,0,198,72,140,63,192,10,31,0,198,72,124,191,0,0,0,0,0,0,0,0,192,10,31,0,198,72,124,63,192,10,31,0,198,72,140,63,138,47,231,76,168,10,168,191,226,16,235,12,193,147,187,191,119,67,126,54,88,190,197,63,175,7,185,121,72,163,220,63,194,243,33,144,152,239,238,191,175,7,185,121,72,163,220,63,119,67,126,54,88,190,197,63,226,16,235,12,193,147,187,191,138,47,231,76,168,10,168,191,192,10,31,0,198,72,140,63,192,10,31,0,198,72,124,63,0,0,0,0,0,0,0,0,104,41,27,64,173,191,88,63,104,41,27,64,173,191,104,191,20,26,29,160,57,132,138,191,65,127,32,72,47,156,157,63,183,148,234,244,157,34,171,63,68,90,121,227,212,68,193,191,44,106,125,204,90,248,196,191,78,23,58,62,197,154,221,63,243,107,225,45,218,115,238,63,78,23,58,62,197,154,221,63,44,106,125,204,90,248,196,191,68,90,121,227,212,68,193,191,183,148,234,244,157,34,171,63,65,127,32,72,47,156,157,63,20,26,29,160,57,132,138,191,104,41,27,64,173,191,104,191,104,41,27,64,173,191,88,63,0,0,0,0,0,0,0,0,104,41,27,64,173,191,88,191,104,41,27,64,173,191,104,191,20,26,29,160,57,132,138,63,65,127,32,72,47,156,157,63,183,148,234,244,157,34,171,191,68,90,121,227,212,68,193,191,44,106,125,204,90,248,196,63,78,23,58,62,197,154,221,63,243,107,225,45,218,115,238,191,78,23,58,62,197,154,221,63,44,106,125,204,90,248,196,63,68,90,121,227,212,68,193,191,183,148,234,244,157,34,171,191,65,127,32,72,47,156,157,63,20,26,29,160,57,132,138,63,104,41,27,64,173,191,104,191,104,41,27,64,173,191,88,191,192,10,31,0,198,72,140,191,16,72,23,128,148,54,165,63,3,23,183,25,188,222,170,63,81,107,172,9,184,37,209,191,151,96,71,179,128,98,178,191,194,243,33,144,152,239,238,63,194,243,33,144,152,239,238,63,151,96,71,179,128,98,178,191,81,107,172,9,184,37,209,191,3,23,183,25,188,222,170,63,16,72,23,128,148,54,165,63,192,10,31,0,198,72,140,191,192,10,31,0,198,72,140,191,16,72,23,128,148,54,165,191,3,23,183,25,188,222,170,63,81,107,172,9,184,37,209,63,151,96,71,179,128,98,178,191,194,243,33,144,152,239,238,191,194,243,33,144,152,239,238,63,151,96,71,179,128,98,178,63,81,107,172,9,184,37,209,191,3,23,183,25,188,222,170,191,16,72,23,128,148,54,165,63,192,10,31,0,198,72,140,63,104,41,27,64,173,191,104,63,14,95,20,240,193,143,130,191,141,234,18,168,88,60,145,191,68,250,20,206,45,29,179,63,230,52,171,77,224,10,160,63,224,192,46,237,48,70,211,191,183,148,234,244,157,34,155,191,243,107,225,45,218,115,238,63,243,107,225,45,218,115,238,63,183,148,234,244,157,34,155,191,224,192,46,237,48,70,211,191,230,52,171,77,224,10,160,63,68,250,20,206,45,29,179,63,141,234,18,168,88,60,145,191,14,95,20,240,193,143,130,191,104,41,27,64,173,191,104,63,104,41,27,64,173,191,104,63,14,95,20,240,193,143,130,63,141,234,18,168,88,60,145,191,68,250,20,206,45,29,179,191,230,52,171,77,224,10,160,63,224,192,46,237,48,70,211,63,183,148,234,244,157,34,155,191,243,107,225,45,218,115,238,191,243,107,225,45,218,115,238,63,183,148,234,244,157,34,155,63,224,192,46,237,48,70,211,191,230,52,171,77,224,10,160,191,68,250,20,206,45,29,179,63,141,234,18,168,88,60,145,63,14,95,20,240,193,143,130,191,104,41,27,64,173,191,104,191,222,62,229,236,27,70,70,191,38,239,171,241,148,180,96,63,71,140,227,24,33,186,116,63,237,248,227,205,31,29,149,191,99,133,236,84,42,231,140,191,242,115,181,168,229,96,185,63,147,165,27,88,208,48,137,63,80,74,35,116,6,126,212,191,98,110,18,144,53,203,96,63,151,73,36,204,228,37,238,63,151,73,36,204,228,37,238,63,98,110,18,144,53,203,96,63,80,74,35,116,6,126,212,191,147,165,27,88,208,48,137,63,242,115,181,168,229,96,185,63,99,133,236,84,42,231,140,191,237,248,227,205,31,29,149,191,71,140,227,24,33,186,116,63,38,239,171,241,148,180,96,63,222,62,229,236,27,70,70,191,222,62,229,236,27,70,70,191,38,239,171,241,148,180,96,191,71,140,227,24,33,186,116,63,237,248,227,205,31,29,149,63,99,133,236,84,42,231,140,191,242,115,181,168,229,96,185,191,147,165,27,88,208,48,137,63,80,74,35,116,6,126,212,63,98,110,18,144,53,203,96,63,151,73,36,204,228,37,238,191,151,73,36,204,228,37,238,63,98,110,18,144,53,203,96,191,80,74,35,116,6,126,212,191,147,165,27,88,208,48,137,191,242,115,181,168,229,96,185,63,99,133,236,84,42,231,140,63,237,248,227,205,31,29,149,191,71,140,227,24,33,186,116,191,38,239,171,241,148,180,96,63,222,62,229,236,27,70,70,63,0,0,0,0,0,0,0,0,119,22,134,86,64,94,163,63,120,245,36,129,254,107,152,191,204,221,29,135,225,81,188,191,37,139,145,78,94,39,216,63,113,240,117,189,78,73,235,63,37,139,145,78,94,39,216,63,204,221,29,135,225,81,188,191,120,245,36,129,254,107,152,191,119,22,134,86,64,94,163,63,0,0,0,0,0,0,0,0,68,236,53,198,158,133,176,191,56,107,217,75,62,213,164,63,252,157,12,24,6,194,218,63,22,130,250,47,70,59,233,191,252,157,12,24,6,194,218,63,56,107,217,75,62,213,164,63,68,236,53,198,158,133,176,191,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,68,236,53,198,158,133,176,191,56,107,217,75,62,213,164,191,252,157,12,24,6,194,218,63,22,130,250,47,70,59,233,63,252,157,12,24,6,194,218,63,56,107,217,75,62,213,164,191,68,236,53,198,158,133,176,191,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,119,22,134,86,64,94,163,191,120,245,36,129,254,107,152,191,204,221,29,135,225,81,188,63,37,139,145,78,94,39,216,63,113,240,117,189,78,73,235,191,37,139,145,78,94,39,216,63,204,221,29,135,225,81,188,63,120,245,36,129,254,107,152,191,119,22,134,86,64,94,163,191,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,99,23,13,181,221,81,164,63,222,199,75,21,25,71,128,63,40,30,147,124,173,226,171,191,142,221,212,157,101,30,214,63,166,252,111,95,184,146,231,63,142,221,212,157,101,30,214,63,40,30,147,124,173,226,171,191,222,199,75,21,25,71,128,63,99,23,13,181,221,81,164,63,0,0,0,0,0,0,0,0,10,60,79,248,48,143,139,191,6,236,164,83,193,19,102,191,175,252,35,243,153,127,193,63,52,2,92,129,236,239,183,191,69,192,78,216,241,131,222,191,51,134,125,9,193,200,236,63,69,192,78,216,241,131,222,191,52,2,92,129,236,239,183,191,175,252,35,243,153,127,193,63,6,236,164,83,193,19,102,191,10,60,79,248,48,143,139,191,0,0,0,0,0,0,0,0,10,60,79,248,48,143,139,63,6,236,164,83,193,19,102,191,175,252,35,243,153,127,193,191,52,2,92,129,236,239,183,191,69,192,78,216,241,131,222,63,51,134,125,9,193,200,236,63,69,192,78,216,241,131,222,63,52,2,92,129,236,239,183,191,175,252,35,243,153,127,193,191,6,236,164,83,193,19,102,191,10,60,79,248,48,143,139,63,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,99,23,13,181,221,81,164,63,222,199,75,21,25,71,128,191,40,30,147,124,173,226,171,191,142,221,212,157,101,30,214,191,166,252,111,95,184,146,231,63,142,221,212,157,101,30,214,191,40,30,147,124,173,226,171,191,222,199,75,21,25,71,128,191,99,23,13,181,221,81,164,63,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,120,168,110,120,56,70,95,63,183,178,116,20,25,93,95,191,13,34,71,138,254,101,145,191,199,28,140,60,38,113,136,63,185,179,14,84,151,118,169,63,43,54,240,241,132,199,179,191,70,233,230,146,67,20,184,191,101,92,43,138,83,238,218,63,223,35,227,16,246,109,234,63,101,92,43,138,83,238,218,63,70,233,230,146,67,20,184,191,43,54,240,241,132,199,179,191,185,179,14,84,151,118,169,63,199,28,140,60,38,113,136,63,13,34,71,138,254,101,145,191,183,178,116,20,25,93,95,191,120,168,110,120,56,70,95,63,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,219,31,132,220,134,139,141,63,120,106,23,163,35,161,141,191,175,39,10,4,32,39,180,191,92,90,123,155,28,171,164,63,121,164,157,48,10,190,218,63,4,236,213,220,248,72,232,191,121,164,157,48,10,190,218,63,92,90,123,155,28,171,164,63,175,39,10,4,32,39,180,191,120,106,23,163,35,161,141,191,219,31,132,220,134,139,141,63,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,219,31,132,220,134,139,141,63,120,106,23,163,35,161,141,63,175,39,10,4,32,39,180,191,92,90,123,155,28,171,164,191,121,164,157,48,10,190,218,63,4,236,213,220,248,72,232,63,121,164,157,48,10,190,218,63,92,90,123,155,28,171,164,191,175,39,10,4,32,39,180,191,120,106,23,163,35,161,141,63,219,31,132,220,134,139,141,63,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,120,168,110,120,56,70,95,191,183,178,116,20,25,93,95,191,13,34,71,138,254,101,145,63,199,28,140,60,38,113,136,63,185,179,14,84,151,118,169,191,43,54,240,241,132,199,179,191,70,233,230,146,67,20,184,63,101,92,43,138,83,238,218,63,223,35,227,16,246,109,234,191,101,92,43,138,83,238,218,63,70,233,230,146,67,20,184,63,43,54,240,241,132,199,179,191], "i8", ALLOC_NONE, Runtime.GLOBAL_BASE);
/* memory initializer */ allocate([185,179,14,84,151,118,169,191,199,28,140,60,38,113,136,63,13,34,71,138,254,101,145,63,183,178,116,20,25,93,95,191,120,168,110,120,56,70,95,191,20,109,200,38,104,156,71,191,85,242,76,158,23,223,93,191,229,230,134,16,4,252,118,63,221,234,74,112,157,63,152,63,171,197,175,40,48,110,174,191,255,228,142,191,193,148,179,191,174,161,86,131,54,176,218,63,54,152,93,255,212,1,234,63,30,207,105,255,6,182,216,63,240,197,45,231,83,63,177,191,246,20,209,102,228,58,165,191,72,174,183,136,215,199,144,63,72,174,183,136,215,199,144,191,246,20,209,102,228,58,165,191,240,197,45,231,83,63,177,63,30,207,105,255,6,182,216,63,54,152,93,255,212,1,234,191,174,161,86,131,54,176,218,63,255,228,142,191,193,148,179,63,171,197,175,40,48,110,174,191,221,234,74,112,157,63,152,191,229,230,134,16,4,252,118,63,85,242,76,158,23,223,93,63,20,109,200,38,104,156,71,191,72,174,183,136,215,199,144,63,246,20,209,102,228,58,165,191,240,197,45,231,83,63,177,191,30,207,105,255,6,182,216,63,54,152,93,255,212,1,234,63,174,161,86,131,54,176,218,63,255,228,142,191,193,148,179,191,171,197,175,40,48,110,174,191,221,234,74,112,157,63,152,63,229,230,134,16,4,252,118,63,85,242,76,158,23,223,93,191,20,109,200,38,104,156,71,191,20,109,200,38,104,156,71,191,85,242,76,158,23,223,93,63,229,230,134,16,4,252,118,63,221,234,74,112,157,63,152,191,171,197,175,40,48,110,174,191,255,228,142,191,193,148,179,63,174,161,86,131,54,176,218,63,54,152,93,255,212,1,234,191,30,207,105,255,6,182,216,63,240,197,45,231,83,63,177,63,246,20,209,102,228,58,165,191,72,174,183,136,215,199,144,191,166,34,106,36,231,35,2,191,111,157,182,220,155,155,18,191,52,227,181,152,210,141,62,63,203,137,182,164,54,79,82,63,132,68,214,168,39,23,101,191,181,231,71,23,198,114,130,191,66,219,83,83,253,66,144,63,50,103,173,82,50,177,161,63,215,100,82,53,189,17,181,191,27,173,182,29,121,97,178,191,202,196,122,245,69,108,219,63,120,147,111,125,159,102,233,63,170,104,60,26,107,238,217,63,97,13,42,162,144,75,175,191,156,115,243,143,109,214,176,191,210,149,186,203,251,3,152,63,181,29,34,210,164,224,127,63,212,5,20,182,146,19,111,191,212,5,20,182,146,19,111,63,181,29,34,210,164,224,127,63,210,149,186,203,251,3,152,191,156,115,243,143,109,214,176,191,97,13,42,162,144,75,175,63,170,104,60,26,107,238,217,63,120,147,111,125,159,102,233,191,202,196,122,245,69,108,219,63,27,173,182,29,121,97,178,63,215,100,82,53,189,17,181,191,50,103,173,82,50,177,161,191,66,219,83,83,253,66,144,63,181,231,71,23,198,114,130,63,132,68,214,168,39,23,101,191,203,137,182,164,54,79,82,191,52,227,181,152,210,141,62,63,111,157,182,220,155,155,18,63,166,34,106,36,231,35,2,191,212,5,20,182,146,19,111,191,181,29,34,210,164,224,127,63,210,149,186,203,251,3,152,63,156,115,243,143,109,214,176,191,97,13,42,162,144,75,175,191,170,104,60,26,107,238,217,63,120,147,111,125,159,102,233,63,202,196,122,245,69,108,219,63,27,173,182,29,121,97,178,191,215,100,82,53,189,17,181,191,50,103,173,82,50,177,161,63,66,219,83,83,253,66,144,63,181,231,71,23,198,114,130,191,132,68,214,168,39,23,101,191,203,137,182,164,54,79,82,63,52,227,181,152,210,141,62,63,111,157,182,220,155,155,18,191,166,34,106,36,231,35,2,191,166,34,106,36,231,35,2,191,111,157,182,220,155,155,18,63,52,227,181,152,210,141,62,63,203,137,182,164,54,79,82,191,132,68,214,168,39,23,101,191,181,231,71,23,198,114,130,63,66,219,83,83,253,66,144,63,50,103,173,82,50,177,161,191,215,100,82,53,189,17,181,191,27,173,182,29,121,97,178,63,202,196,122,245,69,108,219,63,120,147,111,125,159,102,233,191,170,104,60,26,107,238,217,63,97,13,42,162,144,75,175,63,156,115,243,143,109,214,176,191,210,149,186,203,251,3,152,191,181,29,34,210,164,224,127,63,212,5,20,182,146,19,111,63,208,82,44,196,115,242,189,190,52,102,97,41,28,88,203,190,174,255,46,93,154,95,0,63,155,78,4,117,128,87,16,63,161,215,98,51,166,9,49,191,235,251,62,113,16,77,67,191,136,1,36,132,88,192,84,63,238,46,121,243,84,187,110,63,122,127,83,196,36,45,119,191,102,171,108,11,84,39,143,191,202,102,85,25,42,175,153,63,213,8,52,92,164,35,164,63,203,29,190,35,231,161,184,191,101,251,249,134,127,14,177,191,115,8,158,44,251,204,219,63,253,196,62,240,25,8,233,63,128,197,124,182,105,148,218,63,112,107,124,101,42,182,172,191,57,27,98,249,228,205,180,191,22,143,53,218,154,82,155,63,9,105,61,147,96,116,144,63,158,26,13,130,3,23,126,191,115,81,206,185,149,178,90,191,76,103,29,3,69,61,77,63,76,103,29,3,69,61,77,191,115,81,206,185,149,178,90,191,158,26,13,130,3,23,126,63,9,105,61,147,96,116,144,63,22,143,53,218,154,82,155,191,57,27,98,249,228,205,180,191,112,107,124,101,42,182,172,63,128,197,124,182,105,148,218,63,253,196,62,240,25,8,233,191,115,8,158,44,251,204,219,63,101,251,249,134,127,14,177,63,203,29,190,35,231,161,184,191,213,8,52,92,164,35,164,191,202,102,85,25,42,175,153,63,102,171,108,11,84,39,143,63,122,127,83,196,36,45,119,191,238,46,121,243,84,187,110,191,136,1,36,132,88,192,84,63,235,251,62,113,16,77,67,63,161,215,98,51,166,9,49,191,155,78,4,117,128,87,16,191,174,255,46,93,154,95,0,63,52,102,97,41,28,88,203,62,208,82,44,196,115,242,189,190,76,103,29,3,69,61,77,63,115,81,206,185,149,178,90,191,158,26,13,130,3,23,126,191,9,105,61,147,96,116,144,63,22,143,53,218,154,82,155,63,57,27,98,249,228,205,180,191,112,107,124,101,42,182,172,191,128,197,124,182,105,148,218,63,253,196,62,240,25,8,233,63,115,8,158,44,251,204,219,63,101,251,249,134,127,14,177,191,203,29,190,35,231,161,184,191,213,8,52,92,164,35,164,63,202,102,85,25,42,175,153,63,102,171,108,11,84,39,143,191,122,127,83,196,36,45,119,191,238,46,121,243,84,187,110,63,136,1,36,132,88,192,84,63,235,251,62,113,16,77,67,191,161,215,98,51,166,9,49,191,155,78,4,117,128,87,16,63,174,255,46,93,154,95,0,63,52,102,97,41,28,88,203,190,208,82,44,196,115,242,189,190,208,82,44,196,115,242,189,190,52,102,97,41,28,88,203,62,174,255,46,93,154,95,0,63,155,78,4,117,128,87,16,191,161,215,98,51,166,9,49,191,235,251,62,113,16,77,67,63,136,1,36,132,88,192,84,63,238,46,121,243,84,187,110,191,122,127,83,196,36,45,119,191,102,171,108,11,84,39,143,63,202,102,85,25,42,175,153,63,213,8,52,92,164,35,164,191,203,29,190,35,231,161,184,191,101,251,249,134,127,14,177,63,115,8,158,44,251,204,219,63,253,196,62,240,25,8,233,191,128,197,124,182,105,148,218,63,112,107,124,101,42,182,172,63,57,27,98,249,228,205,180,191,22,143,53,218,154,82,155,191,9,105,61,147,96,116,144,63,158,26,13,130,3,23,126,63,115,81,206,185,149,178,90,191,76,103,29,3,69,61,77,191,145,163,132,249,123,140,121,190,103,207,249,50,75,121,134,190,165,76,134,19,229,79,193,62,53,254,56,101,28,84,207,62,130,181,46,125,181,89,246,190,74,233,216,111,158,172,5,191,179,137,142,72,199,107,34,63,146,103,207,4,19,207,51,63,47,251,220,159,8,233,68,191,49,72,241,77,142,62,91,191,225,36,193,75,39,239,99,63,204,20,137,52,195,180,123,63,42,53,22,73,178,196,130,191,16,151,19,137,110,60,148,191,153,240,47,197,230,187,160,63,158,253,132,242,219,35,165,63,67,146,174,77,233,6,187,191,193,2,203,136,45,195,175,191,13,29,149,7,14,8,220,63,221,79,170,253,250,198,232,63,193,128,181,211,240,250,218,63,44,12,165,199,102,165,170,191,193,173,51,223,17,136,183,191,146,77,228,108,20,216,156,63,227,85,107,207,78,248,151,63,27,133,91,147,159,191,132,191,104,158,3,95,103,9,113,191,71,33,121,189,23,216,97,63,92,34,113,251,34,128,55,63,217,25,201,138,64,204,43,191,217,25,201,138,64,204,43,63,92,34,113,251,34,128,55,63,71,33,121,189,23,216,97,191,104,158,3,95,103,9,113,191,27,133,91,147,159,191,132,63,227,85,107,207,78,248,151,63,146,77,228,108,20,216,156,191,193,173,51,223,17,136,183,191,44,12,165,199,102,165,170,63,193,128,181,211,240,250,218,63,221,79,170,253,250,198,232,191,13,29,149,7,14,8,220,63,193,2,203,136,45,195,175,63,67,146,174,77,233,6,187,191,158,253,132,242,219,35,165,191,153,240,47,197,230,187,160,63,16,151,19,137,110,60,148,63,42,53,22,73,178,196,130,191,204,20,137,52,195,180,123,191,225,36,193,75,39,239,99,63,49,72,241,77,142,62,91,63,47,251,220,159,8,233,68,191,146,103,207,4,19,207,51,191,179,137,142,72,199,107,34,63,74,233,216,111,158,172,5,63,130,181,46,125,181,89,246,190,53,254,56,101,28,84,207,190,165,76,134,19,229,79,193,62,103,207,249,50,75,121,134,62,145,163,132,249,123,140,121,190,217,25,201,138,64,204,43,191,92,34,113,251,34,128,55,63,71,33,121,189,23,216,97,63,104,158,3,95,103,9,113,191,27,133,91,147,159,191,132,191,227,85,107,207,78,248,151,63,146,77,228,108,20,216,156,63,193,173,51,223,17,136,183,191,44,12,165,199,102,165,170,191,193,128,181,211,240,250,218,63,221,79,170,253,250,198,232,63,13,29,149,7,14,8,220,63,193,2,203,136,45,195,175,191,67,146,174,77,233,6,187,191,158,253,132,242,219,35,165,63,153,240,47,197,230,187,160,63,16,151,19,137,110,60,148,191,42,53,22,73,178,196,130,191,204,20,137,52,195,180,123,63,225,36,193,75,39,239,99,63,49,72,241,77,142,62,91,191,47,251,220,159,8,233,68,191,146,103,207,4,19,207,51,63,179,137,142,72,199,107,34,63,74,233,216,111,158,172,5,191,130,181,46,125,181,89,246,190,53,254,56,101,28,84,207,62,165,76,134,19,229,79,193,62,103,207,249,50,75,121,134,190,145,163,132,249,123,140,121,190,145,163,132,249,123,140,121,190,103,207,249,50,75,121,134,62,165,76,134,19,229,79,193,62,53,254,56,101,28,84,207,190,130,181,46,125,181,89,246,190,74,233,216,111,158,172,5,63,179,137,142,72,199,107,34,63,146,103,207,4,19,207,51,191,47,251,220,159,8,233,68,191,49,72,241,77,142,62,91,63,225,36,193,75,39,239,99,63,204,20,137,52,195,180,123,191,42,53,22,73,178,196,130,191,16,151,19,137,110,60,148,63,153,240,47,197,230,187,160,63,158,253,132,242,219,35,165,191,67,146,174,77,233,6,187,191,193,2,203,136,45,195,175,63,13,29,149,7,14,8,220,63,221,79,170,253,250,198,232,191,193,128,181,211,240,250,218,63,44,12,165,199,102,165,170,63,193,173,51,223,17,136,183,191,146,77,228,108,20,216,156,191,227,85,107,207,78,248,151,63,27,133,91,147,159,191,132,63,104,158,3,95,103,9,113,191,71,33,121,189,23,216,97,191,92,34,113,251,34,128,55,63,217,25,201,138,64,204,43,63,96,214,144,42,51,253,155,63,91,171,123,128,91,58,158,63,147,130,208,129,103,9,164,191,141,0,171,191,219,133,201,63,89,129,126,226,39,38,231,63,193,245,76,64,142,73,228,63,18,163,54,117,36,0,145,63,115,12,29,158,38,113,198,191,130,124,22,28,184,155,149,191,72,204,100,57,0,2,148,63,72,204,100,57,0,2,148,191,130,124,22,28,184,155,149,191,115,12,29,158,38,113,198,63,18,163,54,117,36,0,145,63,193,245,76,64,142,73,228,191,89,129,126,226,39,38,231,63,141,0,171,191,219,133,201,191,147,130,208,129,103,9,164,191,91,171,123,128,91,58,158,191,96,214,144,42,51,253,155,63,72,204,100,57,0,2,148,63,130,124,22,28,184,155,149,191,115,12,29,158,38,113,198,191,18,163,54,117,36,0,145,63,193,245,76,64,142,73,228,63,89,129,126,226,39,38,231,63,141,0,171,191,219,133,201,63,147,130,208,129,103,9,164,191,91,171,123,128,91,58,158,63,96,214,144,42,51,253,155,63,96,214,144,42,51,253,155,63,91,171,123,128,91,58,158,191,147,130,208,129,103,9,164,191,141,0,171,191,219,133,201,191,89,129,126,226,39,38,231,63,193,245,76,64,142,73,228,191,18,163,54,117,36,0,145,63,115,12,29,158,38,113,198,63,130,124,22,28,184,155,149,191,72,204,100,57,0,2,148,191,110,203,68,142,48,140,143,63,249,22,186,199,141,152,108,63,167,4,162,148,153,52,190,191,131,12,214,20,81,188,168,191,16,61,197,230,117,109,223,63,140,174,182,49,91,52,233,63,243,72,171,185,162,160,213,63,72,143,212,104,95,152,178,191,158,141,86,78,212,144,149,191,14,249,215,172,46,230,166,63,46,185,82,41,82,246,92,63,183,57,68,178,162,243,127,191,183,57,68,178,162,243,127,63,46,185,82,41,82,246,92,63,14,249,215,172,46,230,166,191,158,141,86,78,212,144,149,191,72,143,212,104,95,152,178,63,243,72,171,185,162,160,213,63,140,174,182,49,91,52,233,191,16,61,197,230,117,109,223,63,131,12,214,20,81,188,168,63,167,4,162,148,153,52,190,191,249,22,186,199,141,152,108,191,110,203,68,142,48,140,143,63,183,57,68,178,162,243,127,191,46,185,82,41,82,246,92,63,14,249,215,172,46,230,166,63,158,141,86,78,212,144,149,191,72,143,212,104,95,152,178,191,243,72,171,185,162,160,213,63,140,174,182,49,91,52,233,63,16,61,197,230,117,109,223,63,131,12,214,20,81,188,168,191,167,4,162,148,153,52,190,191,249,22,186,199,141,152,108,63,110,203,68,142,48,140,143,63,110,203,68,142,48,140,143,63,249,22,186,199,141,152,108,191,167,4,162,148,153,52,190,191,131,12,214,20,81,188,168,63,16,61,197,230,117,109,223,63,140,174,182,49,91,52,233,191,243,72,171,185,162,160,213,63,72,143,212,104,95,152,178,63,158,141,86,78,212,144,149,191,14,249,215,172,46,230,166,191,46,185,82,41,82,246,92,63,183,57,68,178,162,243,127,63,229,213,170,59,44,248,101,63,123,71,247,245,12,41,81,191,246,216,223,238,15,225,137,191,64,29,70,108,117,63,159,63,150,202,57,98,106,97,177,63,192,69,245,58,253,94,169,191,107,5,204,208,30,220,145,63,191,108,39,53,191,39,225,63,74,15,18,121,134,145,232,63,179,223,65,107,232,120,210,63,30,125,204,102,17,237,193,191,247,98,45,23,82,153,187,191,115,175,39,233,11,109,112,63,217,136,56,89,123,7,133,63,217,136,56,89,123,7,133,191,115,175,39,233,11,109,112,63,247,98,45,23,82,153,187,63,30,125,204,102,17,237,193,191,179,223,65,107,232,120,210,191,74,15,18,121,134,145,232,63,191,108,39,53,191,39,225,191,107,5,204,208,30,220,145,63,192,69,245,58,253,94,169,63,150,202,57,98,106,97,177,63,64,29,70,108,117,63,159,191,246,216,223,238,15,225,137,191,123,71,247,245,12,41,81,63,229,213,170,59,44,248,101,63,217,136,56,89,123,7,133,63,115,175,39,233,11,109,112,63,247,98,45,23,82,153,187,191,30,125,204,102,17,237,193,191,179,223,65,107,232,120,210,63,74,15,18,121,134,145,232,63,191,108,39,53,191,39,225,63,107,5,204,208,30,220,145,63,192,69,245,58,253,94,169,191,150,202,57,98,106,97,177,63,64,29,70,108,117,63,159,63,246,216,223,238,15,225,137,191,123,71,247,245,12,41,81,191,229,213,170,59,44,248,101,63,229,213,170,59,44,248,101,63,123,71,247,245,12,41,81,63,246,216,223,238,15,225,137,191,64,29,70,108,117,63,159,191,150,202,57,98,106,97,177,63,192,69,245,58,253,94,169,63,107,5,204,208,30,220,145,63,191,108,39,53,191,39,225,191,74,15,18,121,134,145,232,63,179,223,65,107,232,120,210,191,30,125,204,102,17,237,193,191,247,98,45,23,82,153,187,63,115,175,39,233,11,109,112,63,217,136,56,89,123,7,133,191,107,102,131,188,112,181,107,191,231,128,76,81,188,195,65,191,94,60,48,171,86,58,160,63,179,124,62,84,7,41,127,63,173,2,182,49,119,87,194,191,177,145,159,194,56,95,175,191,244,49,241,181,152,206,222,63,209,17,79,167,180,222,232,63,144,10,231,24,4,83,215,63,209,253,171,23,165,152,170,191,243,87,74,45,78,223,155,191,252,127,110,39,130,40,169,63,205,208,210,42,136,51,111,63,231,194,120,36,74,159,142,191,108,96,58,180,41,218,51,191,3,113,178,183,6,247,94,63,3,113,178,183,6,247,94,191,108,96,58,180,41,218,51,191,231,194,120,36,74,159,142,63,205,208,210,42,136,51,111,63,252,127,110,39,130,40,169,191,243,87,74,45,78,223,155,191,209,253,171,23,165,152,170,63,144,10,231,24,4,83,215,63,209,17,79,167,180,222,232,191,244,49,241,181,152,206,222,63,177,145,159,194,56,95,175,63,173,2,182,49,119,87,194,191,179,124,62,84,7,41,127,191,94,60,48,171,86,58,160,63,231,128,76,81,188,195,65,63,107,102,131,188,112,181,107,191,3,113,178,183,6,247,94,63,108,96,58,180,41,218,51,191,231,194,120,36,74,159,142,191,205,208,210,42,136,51,111,63,252,127,110,39,130,40,169,63,243,87,74,45,78,223,155,191,209,253,171,23,165,152,170,191,144,10,231,24,4,83,215,63,209,17,79,167,180,222,232,63,244,49,241,181,152,206,222,63,177,145,159,194,56,95,175,191,173,2,182,49,119,87,194,191,179,124,62,84,7,41,127,63,94,60,48,171,86,58,160,63,231,128,76,81,188,195,65,191,107,102,131,188,112,181,107,191,107,102,131,188,112,181,107,191,231,128,76,81,188,195,65,63,94,60,48,171,86,58,160,63,179,124,62,84,7,41,127,191,173,2,182,49,119,87,194,191,177,145,159,194,56,95,175,63,244,49,241,181,152,206,222,63,209,17,79,167,180,222,232,191,144,10,231,24,4,83,215,63,209,253,171,23,165,152,170,63,243,87,74,45,78,223,155,191,252,127,110,39,130,40,169,191,205,208,210,42,136,51,111,63,231,194,120,36,74,159,142,63,108,96,58,180,41,218,51,191,3,113,178,183,6,247,94,191,152,155,145,151,221,243,86,63,191,118,14,83,25,79,68,63,103,156,156,92,85,46,139,191,119,80,150,46,26,156,135,191,47,46,227,72,69,243,158,63,0,166,222,180,112,30,67,63,141,167,172,103,118,240,171,191,197,148,230,183,183,143,206,63,24,190,139,77,3,249,230,63,45,245,51,144,60,193,227,63,25,190,30,77,60,15,162,63,16,195,248,213,188,132,200,191,179,151,184,164,223,171,146,191,182,77,186,245,168,200,175,63,252,146,52,193,206,36,130,63,196,245,148,90,83,5,133,191,189,132,8,25,55,2,63,191,158,78,190,45,196,133,81,63,158,78,190,45,196,133,81,191,189,132,8,25,55,2,63,191,196,245,148,90,83,5,133,63,252,146,52,193,206,36,130,63,182,77,186,245,168,200,175,191,179,151,184,164,223,171,146,191,16,195,248,213,188,132,200,63,25,190,30,77,60,15,162,63,45,245,51,144,60,193,227,191,24,190,139,77,3,249,230,63,197,148,230,183,183,143,206,191,141,167,172,103,118,240,171,191,0,166,222,180,112,30,67,191,47,46,227,72,69,243,158,63,119,80,150,46,26,156,135,63,103,156,156,92,85,46,139,191,191,118,14,83,25,79,68,191,152,155,145,151,221,243,86,63,158,78,190,45,196,133,81,63,189,132,8,25,55,2,63,191,196,245,148,90,83,5,133,191,252,146,52,193,206,36,130,63,182,77,186,245,168,200,175,63,179,151,184,164,223,171,146,191,16,195,248,213,188,132,200,191,25,190,30,77,60,15,162,63,45,245,51,144,60,193,227,63,24,190,139,77,3,249,230,63,197,148,230,183,183,143,206,63,141,167,172,103,118,240,171,191,0,166,222,180,112,30,67,63,47,46,227,72,69,243,158,63,119,80,150,46,26,156,135,191,103,156,156,92,85,46,139,191,191,118,14,83,25,79,68,63,152,155,145,151,221,243,86,63,152,155,145,151,221,243,86,63,191,118,14,83,25,79,68,191,103,156,156,92,85,46,139,191,119,80,150,46,26,156,135,63,47,46,227,72,69,243,158,63,0,166,222,180,112,30,67,191,141,167,172,103,118,240,171,191,197,148,230,183,183,143,206,191,24,190,139,77,3,249,230,63,45,245,51,144,60,193,227,191,25,190,30,77,60,15,162,63,16,195,248,213,188,132,200,63,179,151,184,164,223,171,146,191,182,77,186,245,168,200,175,191,252,146,52,193,206,36,130,63,196,245,148,90,83,5,133,63,189,132,8,25,55,2,63,191,158,78,190,45,196,133,81,191,0,254,174,152,145,60,73,63,191,4,210,197,204,17,25,63,143,109,81,142,135,178,129,191,148,66,210,144,66,2,88,191,207,120,252,108,198,131,167,63,201,2,3,149,237,198,135,63,134,219,218,251,78,106,196,191,98,171,28,15,58,37,178,191,191,14,21,13,46,48,222,63,133,97,94,131,211,159,232,63,26,55,135,30,158,144,216,63,216,112,72,36,223,49,162,191,218,227,54,48,0,97,160,191,0,211,18,228,240,152,169,63,44,119,96,202,242,156,119,63,128,68,57,227,236,215,148,191,188,46,84,167,115,91,74,191,91,26,87,165,74,208,114,63,47,118,33,233,64,231,13,63,49,206,95,215,68,26,62,191,49,206,95,215,68,26,62,63,47,118,33,233,64,231,13,63,91,26,87,165,74,208,114,191,188,46,84,167,115,91,74,191,128,68,57,227,236,215,148,63,44,119,96,202,242,156,119,63,0,211,18,228,240,152,169,191,218,227,54,48,0,97,160,191,216,112,72,36,223,49,162,63,26,55,135,30,158,144,216,63,133,97,94,131,211,159,232,191,191,14,21,13,46,48,222,63,98,171,28,15,58,37,178,63,134,219,218,251,78,106,196,191,201,2,3,149,237,198,135,191,207,120,252,108,198,131,167,63,148,66,210,144,66,2,88,63,143,109,81,142,135,178,129,191,191,4,210,197,204,17,25,191,0,254,174,152,145,60,73,63,49,206,95,215,68,26,62,191,47,118,33,233,64,231,13,63,91,26,87,165,74,208,114,63,188,46,84,167,115,91,74,191,128,68,57,227,236,215,148,191,44,119,96,202,242,156,119,63,0,211,18,228,240,152,169,63,218,227,54,48,0,97,160,191,216,112,72,36,223,49,162,191,26,55,135,30,158,144,216,63,133,97,94,131,211,159,232,63,191,14,21,13,46,48,222,63,98,171,28,15,58,37,178,191,134,219,218,251,78,106,196,191,201,2,3,149,237,198,135,63,207,120,252,108,198,131,167,63,148,66,210,144,66,2,88,191,143,109,81,142,135,178,129,191,191,4,210,197,204,17,25,63,0,254,174,152,145,60,73,63,0,254,174,152,145,60,73,63,191,4,210,197,204,17,25,191,143,109,81,142,135,178,129,191,148,66,210,144,66,2,88,63,207,120,252,108,198,131,167,63,201,2,3,149,237,198,135,191,134,219,218,251,78,106,196,191,98,171,28,15,58,37,178,63,191,14,21,13,46,48,222,63,133,97,94,131,211,159,232,191,26,55,135,30,158,144,216,63,216,112,72,36,223,49,162,63,218,227,54,48,0,97,160,191,0,211,18,228,240,152,169,191,44,119,96,202,242,156,119,63,128,68,57,227,236,215,148,63,188,46,84,167,115,91,74,191,91,26,87,165,74,208,114,191,47,118,33,233,64,231,13,63,49,206,95,215,68,26,62,63,10,32,70,105,108,116,101,114,32,78,111,116,32,105,110,32,68,97,116,97,98,97,115,101,32,0,0,0,0,0,0,0,10,32,84,104,101,32,68,101,99,111,109,112,111,115,105,116,105,111,110,32,73,116,101,114,97,116,105,111,110,115,32,67,97,110,110,111,116,32,69,120,99,101,101,100,32,49,48,48,46,32,69,120,105,116,105,110,103,32,0,0,0,0,0,0,10,32,69,114,114,111,114,32,45,32,84,104,101,32,83,105,103,110,97,108,32,67,97,110,32,111,110,108,121,32,98,101,32,105,116,101,114,97,116,101,100,32,37,100,32,116,105,109,101,115,32,117,115,105,110,103,32,116,104,105,115,32,119,97,118,101,108,101,116,46,32,69,120,105,116,105,110,103,10,0,10,32,70,111,114,32,83,87,84,32,116,104,101,32,115,105,103,110,97,108,32,108,101,110,103,116,104,32,109,117,115,116,32,98,101,32,97,32,109,117,108,116,105,112,108,101,32,111,102,32,50,94,74,46,32,0,100,98,0,0,0,0,0,0,99,111,105,102,0,0,0,0,10,32,77,79,68,87,84,32,105,115,32,111,110,108,121,32,105,109,112,108,101,109,101,110,116,101,100,32,102,111,114,32,111,114,116,104,111,103,111,110,97,108,32,119,97,118,101,108,101,116,32,102,97,109,105,108,105,101,115,32,45,32,100,98,44,32,115,121,109,32,97,110,100,32,99,111,105,102,32,0,100,105,114,101,99,116,0,0,102,102,116,0,0,0,0,0,70,70,84,0,0,0,0,0,83,105,103,110,97,108,32,101,120,116,101,110,115,105,111,110,32,99,97,110,32,98,101,32,101,105,116,104,101,114,32,112,101,114,32,111,114,32,115,121,109,0,0,0,0,0,0,0,68,101,99,111,109,112,111,115,105,116,105,111,110,32,70,105,108,116,101,114,115,32,109,117,115,116,32,104,97,118,101,32,116,104,101,32,115,97,109,101,32,108,101,110,103,116,104,46,0,0,0,0,0,0,0,0,83,87,84,32,79,110,108,121,32,97,99,99,101,112,116,115,32,116,119,111,32,109,101,116,104,111,100,115,32,45,32,100,105,114,101,99,116,32,97,110,100,32,102,102,116,0,0,0,67,111,110,118,111,108,117,116,105,111,110,32,79,110,108,121,32,97,99,99,101,112,116,115,32,116,119,111,32,109,101,116,104,111,100,115,32,45,32,100,105,114,101,99,116,32,97,110,100,32,102,102,116,0,0,0,100,119,116,0,0,0,0,0,68,87,84,0,0,0,0,0,112,101,114,0,0,0,0,0,115,121,109,0,0,0,0,0,115,119,116,0,0,0,0,0,83,87,84,0,0,0,0,0,109,111,100,119,116,0,0,0,77,79,68,87,84,0,0,0,93,61,127,102,158,160,230,63,0,0,0,0,0,136,57,61,68,23,117,250,82,176,230,63,0,0,0,0,0,0,216,60,254,217,11,117,18,192,230,63,0,0,0,0,0,120,40,189,191,118,212,221,220,207,230,63,0,0,0,0,0,192,30,61,41,26,101,60,178,223,230,63,0,0,0,0,0,0,216,188,227,58,89,152,146,239,230,63,0,0,0,0,0,0,188,188,134,147,81,249,125,255,230,63,0,0,0,0,0,216,47,189,163,45,244,102,116,15,231,63,0,0,0,0,0,136,44,189,195,95,236,232,117,31,231,63,0,0,0,0,0,192,19,61,5,207,234,134,130,47,231,63,0,0,0,0,0,48,56,189,82,129,165,72,154,63,231,63,0,0,0,0,0,192,0,189,252,204,215,53,189,79,231,63,0,0,0,0,0,136,47,61,241,103,66,86,235,95,231,63,0,0,0,0,0,224,3,61,72,109,171,177,36,112,231,63,0,0,0,0,0,208,39,189,56,93,222,79,105,128,231,63,0,0,0,0,0,0,221,188,0,29,172,56,185,144,231,63,0,0,0,0,0,0,227,60,120,1,235,115,20,161,231,63,0,0,0,0,0,0,237,188,96,208,118,9,123,177,231,63,0,0,0,0,0,64,32,61,51,193,48,1,237,193,231,63,0,0,0,0,0,0,160,60,54,134,255,98,106,210,231,63,0,0,0,0,0,144,38,189,59,78,207,54,243,226,231,63,0,0,0,0,0,224,2,189,232,195,145,132,135,243,231,63,0,0,0,0,0,88,36,189,78,27,62,84,39,4,232,63,0,0,0,0,0,0,51,61,26,7,209,173,210,20,232,63,0,0,0,0,0,0,15,61,126,205,76,153,137,37,232,63,0,0,0,0,0,192,33,189,208,66,185,30,76,54,232,63,0,0,0,0,0,208,41,61,181,202,35,70,26,71,232,63,0,0,0,0,0,16,71,61,188,91,159,23,244,87,232,63,0,0,0,0,0,96,34,61,175,145,68,155,217,104,232,63,0,0,0,0,0,196,50,189,149,163,49,217,202,121,232,63,0,0,0,0,0,0,35,189,184,101,138,217,199,138,232,63,0,0,0,0,0,128,42,189,0,88,120,164,208,155,232,63,0,0,0,0,0,0,237,188,35,162,42,66,229,172,232,63,0,0,0,0,0,40,51,61,250,25,214,186,5,190,232,63,0,0,0,0,0,180,66,61,131,67,181,22,50,207,232,63,0,0,0,0,0,208,46,189,76,102,8,94,106,224,232,63,0,0,0,0,0,80,32,189,7,120,21,153,174,241,232,63,0,0,0,0,0,40,40,61,14,44,40,208,254,2,233,63,0,0,0,0,0,176,28,189,150,255,145,11,91,20,233,63,0,0,0,0,0,224,5,189,249,47,170,83,195,37,233,63,0,0,0,0,0,64,245,60,74,198,205,176,55,55,233,63,0,0,0,0,0,32,23,61,174,152,95,43,184,72,233,63,0,0,0,0,0,0,9,189,203,82,200,203,68,90,233,63,0,0,0,0,0,104,37,61,33,111,118,154,221,107,233,63,0,0,0,0,0,208,54,189,42,78,222,159,130,125,233,63,0,0,0,0,0,0,1,189,163,35,122,228,51,143,233,63,0,0,0,0,0,0,45,61,4,6,202,112,241,160,233,63,0,0,0,0,0,164,56,189,137,255,83,77,187,178,233,63,0,0,0,0,0,92,53,61,91,241,163,130,145,196,233,63,0,0,0,0,0,184,38,61,197,184,75,25,116,214,233,63,0,0,0,0,0,0,236,188,142,35,227,25,99,232,233,63,0,0,0,0,0,208,23,61,2,243,7,141,94,250,233,63,0,0,0,0,0,64,22,61,77,229,93,123,102,12,234,63,0,0,0,0,0,0,245,188,246,184,142,237,122,30,234,63,0,0,0,0,0,224,9,61,39,46,74,236,155,48,234,63,0,0,0,0,0,216,42,61,93,10,70,128,201,66,234,63,0,0,0,0,0,240,26,189,155,37,62,178,3,85,234,63,0,0,0,0,0,96,11,61,19,98,244,138,74,103,234,63,0,0,0,0,0,136,56,61,167,179,48,19,158,121,234,63,0,0,0,0,0,32,17,61,141,46,193,83,254,139,234,63,0,0,0,0,0,192,6,61,210,252,121,85,107,158,234,63,0,0,0,0,0,184,41,189,184,111,53,33,229,176,234,63,0,0,0,0,0,112,43,61,129,243,211,191,107,195,234,63,0,0,0,0,0,0,217,60,128,39,60,58,255,213,234,63,0,0,0,0,0,0,228,60,163,210,90,153,159,232,234,63,0,0,0,0,0,144,44,189,103,243,34,230,76,251,234,63,0,0,0,0,0,80,22,61,144,183,141,41,7,14,235,63,0,0,0,0,0,212,47,61,169,137,154,108,206,32,235,63,0,0,0,0,0,112,18,61,75,26,79,184,162,51,235,63,0,0,0,0,0,71,77,61,231,71,183,21,132,70,235,63,0,0,0,0,0,56,56,189,58,89,229,141,114,89,235,63,0,0,0,0,0,0,152,60,106,197,241,41,110,108,235,63,0,0,0,0,0,208,10,61,80,94,251,242,118,127,235,63,0,0,0,0,0,128,222,60,178,73,39,242,140,146,235,63,0,0,0,0,0,192,4,189,3,6,161,48,176,165,235,63,0,0,0,0,0,112,13,189,102,111,154,183,224,184,235,63,0,0,0,0,0,144,13,61,255,193,75,144,30,204,235,63,0,0,0,0,0,160,2,61,111,161,243,195,105,223,235,63,0,0,0,0,0,120,31,189,184,29,215,91,194,242,235,63,0,0,0,0,0,160,16,189,233,178,65,97,40,6,236,63,0,0,0,0,0,64,17,189,224,82,133,221,155,25,236,63,0,0,0,0,0,224,11,61,238,100,250,217,28,45,236,63,0,0,0,0,0,64,9,189,47,208,255,95,171,64,236,63,0,0,0,0,0,208,14,189,21,253,250,120,71,84,236,63,0,0,0,0,0,102,57,61,203,208,87,46,241,103,236,63,0,0,0,0,0,16,26,189,182,193,136,137,168,123,236,63,0,0,0,0,128,69,88,189,51,231,6,148,109,143,236,63,0,0,0,0,0,72,26,189,223,196,81,87,64,163,236,63,0,0,0,0,0,0,203,60,148,144,239,220,32,183,236,63,0,0,0,0,0,64,1,61,137,22,109,46,15,203,236,63,0,0,0,0,0,32,240,60,18,196,93,85,11,223,236,63,0,0,0,0,0,96,243,60,59,171,91,91,21,243,236,63,0,0,0,0,0,144,6,189,188,137,7,74,45,7,237,63,0,0,0,0,0,160,9,61,250,200,8,43,83,27,237,63,0,0,0,0,0,224,21,189,133,138,13,8,135,47,237,63,0,0,0,0,0,40,29,61,3,162,202,234,200,67,237,63,0,0,0,0,0,160,1,61,145,164,251,220,24,88,237,63,0,0,0,0,0,0,223,60,161,230,98,232,118,108,237,63,0,0,0,0,0,160,3,189,78,131,201,22,227,128,237,63,0,0,0,0,0,216,12,189,144,96,255,113,93,149,237,63,0,0,0,0,0,192,244,60,174,50,219,3,230,169,237,63,0,0,0,0,0,144,255,60,37,131,58,214,124,190,237,63,0,0,0,0,0,128,233,60,69,180,1,243,33,211,237,63,0,0,0,0,0,32,245,188,191,5,28,100,213,231,237,63,0,0,0,0,0,112,29,189,236,154,123,51,151,252,237,63,0,0,0,0,0,20,22,189,94,125,25,107,103,17,238,63,0,0,0,0,0,72,11,61,231,163,245,20,70,38,238,63,0,0,0,0,0,206,64,61,92,238,22,59,51,59,238,63,0,0,0,0,0,104,12,61,180,63,139,231,46,80,238,63,0,0,0,0,0,48,9,189,104,109,103,36,57,101,238,63,0,0,0,0,0,0,229,188,68,76,199,251,81,122,238,63,0,0,0,0,0,248,7,189,38,183,205,119,121,143,238,63,0,0,0,0,0,112,243,188,232,144,164,162,175,164,238,63,0,0,0,0,0,208,229,60,228,202,124,134,244,185,238,63,0,0,0,0,0,26,22,61,13,104,142,45,72,207,238,63,0,0,0,0,0,80,245,60,20,133,24,162,170,228,238,63,0,0,0,0,0,64,198,60,19,90,97,238,27,250,238,63,0,0,0,0,0,128,238,188,6,65,182,28,156,15,239,63,0,0,0,0,0,136,250,188,99,185,107,55,43,37,239,63,0,0,0,0,0,144,44,189,117,114,221,72,201,58,239,63,0,0,0,0,0,0,170,60,36,69,110,91,118,80,239,63,0,0,0,0,0,240,244,188,253,68,136,121,50,102,239,63,0,0,0,0,0,128,202,60,56,190,156,173,253,123,239,63,0,0,0,0,0,188,250,60,130,60,36,2,216,145,239,63,0,0,0,0,0,96,212,188,142,144,158,129,193,167,239,63,0,0,0,0,0,12,11,189,17,213,146,54,186,189,239,63,0,0,0,0,0,224,192,188,148,113,143,43,194,211,239,63,0,0,0,0,128,222,16,189,238,35,42,107,217,233,239,63,0,0,0,0,0,67,238,60,0,0,0,0,0,0,240,63,0,0,0,0,0,0,0,0,190,188,90,250,26,11,240,63,0,0,0,0,0,64,179,188,3,51,251,169,61,22,240,63,0,0,0,0,0,23,18,189,130,2,59,20,104,33,240,63,0,0,0,0,0,64,186,60,108,128,119,62,154,44,240,63,0,0,0,0,0,152,239,60,202,187,17,46,212,55,240,63,0,0,0,0,0,64,199,188,137,127,110,232,21,67,240,63,0,0,0,0,0,48,216,60,103,84,246,114,95,78,240,63,0,0,0,0,0,63,26,189,90,133,21,211,176,89,240,63,0,0,0,0,0,132,2,189,149,31,60,14,10,101,240,63,0,0,0,0,0,96,241,60,26,247,221,41,107,112,240,63,0,0,0,0,0,36,21,61,45,168,114,43,212,123,240,63,0,0,0,0,0,160,233,188,208,155,117,24,69,135,240,63,0,0,0,0,0,64,230,60,200,7,102,246,189,146,240,63,0,0,0,0,0,120,0,189,131,243,198,202,62,158,240,63,0,0,0,0,0,0,152,188,48,57,31,155,199,169,240,63,0,0,0,0,0,160,255,60,252,136,249,108,88,181,240,63,0,0,0,0,0,200,250,188,138,108,228,69,241,192,240,63,0,0,0,0,0,192,217,60,22,72,114,43,146,204,240,63,0,0,0,0,0,32,5,61,216,93,57,35,59,216,240,63,0,0,0,0,0,208,250,188,243,209,211,50,236,227,240,63,0,0,0,0,0,172,27,61,166,169,223,95,165,239,240,63,0,0,0,0,0,232,4,189,240,210,254,175,102,251,240,63,0,0,0,0,0,48,13,189,75,35,215,40,48,7,241,63,0,0,0,0,0,80,241,60,91,91,18,208,1,19,241,63,0,0,0,0,0,0,236,60,249,42,94,171,219,30,241,63,0,0,0,0,0,188,22,61,213,49,108,192,189,42,241,63,0,0,0,0,0,64,232,60,125,4,242,20,168,54,241,63,0,0,0,0,0,208,14,189,233,45,169,174,154,66,241,63,0,0,0,0,0,224,232,60,56,49,79,147,149,78,241,63,0,0,0,0,0,64,235,60,113,142,165,200,152,90,241,63,0,0,0,0,0,48,5,61,223,195,113,84,164,102,241,63,0,0,0,0,0,56,3,61,17,82,125,60,184,114,241,63,0,0,0,0,0,212,40,61,159,187,149,134,212,126,241,63,0,0,0,0,0,208,5,189,147,141,140,56,249,138,241,63,0,0,0,0,0,136,28,189,102,93,55,88,38,151,241,63,0,0,0,0,0,240,17,61,167,203,111,235,91,163,241,63,0,0,0,0,0,72,16,61,227,135,19,248,153,175,241,63,0,0,0,0,0,57,71,189,84,93,4,132,224,187,241,63,0,0,0,0,0,228,36,61,67,28,40,149,47,200,241,63,0,0,0,0,0,32,10,189,178,185,104,49,135,212,241,63,0,0,0,0,0,128,227,60,49,64,180,94,231,224,241,63,0,0,0,0,0,192,234,60,56,217,252,34,80,237,241,63,0,0,0,0,0,144,1,61,247,205,56,132,193,249,241,63,0,0,0,0,0,120,27,189,143,141,98,136,59,6,242,63,0,0,0,0,0,148,45,61,30,168,120,53,190,18,242,63,0,0,0,0,0,0,216,60,65,221,125,145,73,31,242,63,0,0,0,0,0,52,43,61,35,19,121,162,221,43,242,63,0,0,0,0,0,248,25,61,231,97,117,110,122,56,242,63,0,0,0,0,0,200,25,189,39,20,130,251,31,69,242,63,0,0,0,0,0,48,2,61,2,166,178,79,206,81,242,63,0,0,0,0,0,72,19,189,176,206,30,113,133,94,242,63,0,0,0,0,0,112,18,61,22,125,226,101,69,107,242,63,0,0,0,0,0,208,17,61,15,224,29,52,14,120,242,63,0,0,0,0,0,238,49,61,62,99,245,225,223,132,242,63,0,0,0,0,0,192,20,189,48,187,145,117,186,145,242,63,0,0,0,0,0,216,19,189,9,223,31,245,157,158,242,63,0,0,0,0,0,176,8,61,155,14,209,102,138,171,242,63,0,0,0,0,0,124,34,189,58,218,218,208,127,184,242,63,0,0,0,0,0,52,42,61,249,26,119,57,126,197,242,63,0,0,0,0,0,128,16,189,217,2,228,166,133,210,242,63,0,0,0,0,0,208,14,189,121,21,100,31,150,223,242,63,0,0,0,0,0,32,244,188,207,46,62,169,175,236,242,63,0,0,0,0,0,152,36,189,34,136,189,74,210,249,242,63,0,0,0,0,0,48,22,189,37,182,49,10,254,6,243,63,0,0,0,0,0,54,50,189,11,165,238,237,50,20,243,63,0,0,0,0,128,223,112,189,184,215,76,252,112,33,243,63,0,0,0,0,0,72,34,189,162,233,168,59,184,46,243,63,0,0,0,0,0,152,37,189,102,23,100,178,8,60,243,63,0,0,0,0,0,208,30,61,39,250,227,102,98,73,243,63,0,0,0,0,0,0,220,188,15,159,146,95,197,86,243,63,0,0,0,0,0,216,48,189,185,136,222,162,49,100,243,63,0,0,0,0,0,200,34,61,57,170,58,55,167,113,243,63,0,0,0,0,0,96,32,61,254,116,30,35,38,127,243,63,0,0,0,0,0,96,22,189,56,216,5,109,174,140,243,63,0,0,0,0,0,224,10,189,195,62,113,27,64,154,243,63,0,0,0,0,0,114,68,189,32,160,229,52,219,167,243,63,0,0,0,0,0,32,8,61,149,110,236,191,127,181,243,63,0,0,0,0,0,128,62,61,242,168,19,195,45,195,243,63,0,0,0,0,0,128,239,60,34,225,237,68,229,208,243,63,0,0,0,0,0,160,23,189,187,52,18,76,166,222,243,63,0,0,0,0,0,48,38,61,204,78,28,223,112,236,243,63,0,0,0,0,0,166,72,189,140,126,172,4,69,250,243,63,0,0,0,0,0,220,60,189,187,160,103,195,34,8,244,63,0,0,0,0,0,184,37,61,149,46,247,33,10,22,244,63,0,0,0,0,0,192,30,61,70,70,9,39,251,35,244,63,0,0,0,0,0,96,19,189,32,169,80,217,245,49,244,63,0,0,0,0,0,152,35,61,235,185,132,63,250,63,244,63,0,0,0,0,0,0,250,60,25,137,97,96,8,78,244,63,0,0,0,0,0,192,246,188,1,210,167,66,32,92,244,63,0,0,0,0,0,192,11,189,22,0,29,237,65,106,244,63,0,0,0,0,0,128,18,189,38,51,139,102,109,120,244,63,0,0,0,0,0,224,48,61,0,60,193,181,162,134,244,63,0,0,0,0,0,64,45,189,4,175,146,225,225,148,244,63,0,0,0,0,0,32,12,61,114,211,215,240,42,163,244,63,0,0,0,0,0,80,30,189,1,184,109,234,125,177,244,63,0,0,0,0,0,128,7,61,225,41,54,213,218,191,244,63,0,0,0,0,0,128,19,189,50,193,23,184,65,206,244,63,0,0,0,0,0,128,0,61,219,221,253,153,178,220,244,63,0,0,0,0,0,112,44,61,150,171,216,129,45,235,244,63,0,0,0,0,0,224,28,189,2,45,157,118,178,249,244,63,0,0,0,0,0,32,25,61,193,49,69,127,65,8,245,63,0,0,0,0,0,192,8,189,42,102,207,162,218,22,245,63,0,0,0,0,0,0,250,188,234,81,63,232,125,37,245,63,0,0,0,0,0,8,74,61,218,78,157,86,43,52,245,63,0,0,0,0,0,216,38,189,26,172,246,244,226,66,245,63,0,0,0,0,0,68,50,189,219,148,93,202,164,81,245,63,0,0,0,0,0,60,72,61,107,17,233,221,112,96,245,63,0,0,0,0,0,176,36,61,222,41,181,54,71,111,245,63,0,0,0,0,0,90,65,61,14,196,226,219,39,126,245,63,0,0,0,0,0,224,41,189,111,199,151,212,18,141,245,63,0,0,0,0,0,8,35,189,76,11,255,39,8,156,245,63,0,0,0,0,0,236,77,61,39,84,72,221,7,171,245,63,0,0,0,0,0,0,196,188,244,122,168,251,17,186,245,63,0,0,0,0,0,8,48,61,11,70,89,138,38,201,245,63,0,0,0,0,0,200,38,189,63,142,153,144,69,216,245,63,0,0,0,0,0,154,70,61,225,32,173,21,111,231,245,63,0,0,0,0,0,64,27,189,202,235,220,32,163,246,245,63,0,0,0,0,0,112,23,61,184,220,118,185,225,5,246,63,0,0,0,0,0,248,38,61,21,247,205,230,42,21,246,63,0,0,0,0,0,0,1,61,49,85,58,176,126,36,246,63,0,0,0,0,0,208,21,189,181,41,25,29,221,51,246,63,0,0,0,0,0,208,18,189,19,195,204,52,70,67,246,63], "i8", ALLOC_NONE, Runtime.GLOBAL_BASE+10240);
/* memory initializer */ allocate([0,0,0,0,0,128,234,188,250,142,188,254,185,82,246,63,0,0,0,0,0,96,40,189,151,51,85,130,56,98,246,63,0,0,0,0,0,254,113,61,142,50,8,199,193,113,246,63,0,0,0,0,0,32,55,189,126,169,76,212,85,129,246,63,0,0,0,0,0,128,230,60,113,148,158,177,244,144,246,63,0,0,0,0,0,120,41,189,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], "i8", ALLOC_NONE, Runtime.GLOBAL_BASE+20480);





/* no memory initializer */
var tempDoublePtr = Runtime.alignMemory(allocate(12, "i8", ALLOC_STATIC), 8);

assert(tempDoublePtr % 8 == 0);

function copyTempFloat(ptr) { // functions, because inlining this code increases code size too much

  HEAP8[tempDoublePtr] = HEAP8[ptr];

  HEAP8[tempDoublePtr+1] = HEAP8[ptr+1];

  HEAP8[tempDoublePtr+2] = HEAP8[ptr+2];

  HEAP8[tempDoublePtr+3] = HEAP8[ptr+3];

}

function copyTempDouble(ptr) {

  HEAP8[tempDoublePtr] = HEAP8[ptr];

  HEAP8[tempDoublePtr+1] = HEAP8[ptr+1];

  HEAP8[tempDoublePtr+2] = HEAP8[ptr+2];

  HEAP8[tempDoublePtr+3] = HEAP8[ptr+3];

  HEAP8[tempDoublePtr+4] = HEAP8[ptr+4];

  HEAP8[tempDoublePtr+5] = HEAP8[ptr+5];

  HEAP8[tempDoublePtr+6] = HEAP8[ptr+6];

  HEAP8[tempDoublePtr+7] = HEAP8[ptr+7];

}

// {{PRE_LIBRARY}}


  var _BDtoIHigh=true;

  
  
  var ___errno_state=0;function ___setErrNo(value) {
      // For convenient setting and returning of errno.
      HEAP32[((___errno_state)>>2)]=value;
      return value;
    }
  
  var ERRNO_CODES={EPERM:1,ENOENT:2,ESRCH:3,EINTR:4,EIO:5,ENXIO:6,E2BIG:7,ENOEXEC:8,EBADF:9,ECHILD:10,EAGAIN:11,EWOULDBLOCK:11,ENOMEM:12,EACCES:13,EFAULT:14,ENOTBLK:15,EBUSY:16,EEXIST:17,EXDEV:18,ENODEV:19,ENOTDIR:20,EISDIR:21,EINVAL:22,ENFILE:23,EMFILE:24,ENOTTY:25,ETXTBSY:26,EFBIG:27,ENOSPC:28,ESPIPE:29,EROFS:30,EMLINK:31,EPIPE:32,EDOM:33,ERANGE:34,ENOMSG:42,EIDRM:43,ECHRNG:44,EL2NSYNC:45,EL3HLT:46,EL3RST:47,ELNRNG:48,EUNATCH:49,ENOCSI:50,EL2HLT:51,EDEADLK:35,ENOLCK:37,EBADE:52,EBADR:53,EXFULL:54,ENOANO:55,EBADRQC:56,EBADSLT:57,EDEADLOCK:35,EBFONT:59,ENOSTR:60,ENODATA:61,ETIME:62,ENOSR:63,ENONET:64,ENOPKG:65,EREMOTE:66,ENOLINK:67,EADV:68,ESRMNT:69,ECOMM:70,EPROTO:71,EMULTIHOP:72,EDOTDOT:73,EBADMSG:74,ENOTUNIQ:76,EBADFD:77,EREMCHG:78,ELIBACC:79,ELIBBAD:80,ELIBSCN:81,ELIBMAX:82,ELIBEXEC:83,ENOSYS:38,ENOTEMPTY:39,ENAMETOOLONG:36,ELOOP:40,EOPNOTSUPP:95,EPFNOSUPPORT:96,ECONNRESET:104,ENOBUFS:105,EAFNOSUPPORT:97,EPROTOTYPE:91,ENOTSOCK:88,ENOPROTOOPT:92,ESHUTDOWN:108,ECONNREFUSED:111,EADDRINUSE:98,ECONNABORTED:103,ENETUNREACH:101,ENETDOWN:100,ETIMEDOUT:110,EHOSTDOWN:112,EHOSTUNREACH:113,EINPROGRESS:115,EALREADY:114,EDESTADDRREQ:89,EMSGSIZE:90,EPROTONOSUPPORT:93,ESOCKTNOSUPPORT:94,EADDRNOTAVAIL:99,ENETRESET:102,EISCONN:106,ENOTCONN:107,ETOOMANYREFS:109,EUSERS:87,EDQUOT:122,ESTALE:116,ENOTSUP:95,ENOMEDIUM:123,EILSEQ:84,EOVERFLOW:75,ECANCELED:125,ENOTRECOVERABLE:131,EOWNERDEAD:130,ESTRPIPE:86};function _sysconf(name) {
      // long sysconf(int name);
      // http://pubs.opengroup.org/onlinepubs/009695399/functions/sysconf.html
      switch(name) {
        case 30: return PAGE_SIZE;
        case 85: return totalMemory / PAGE_SIZE;
        case 132:
        case 133:
        case 12:
        case 137:
        case 138:
        case 15:
        case 235:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
        case 149:
        case 13:
        case 10:
        case 236:
        case 153:
        case 9:
        case 21:
        case 22:
        case 159:
        case 154:
        case 14:
        case 77:
        case 78:
        case 139:
        case 80:
        case 81:
        case 82:
        case 68:
        case 67:
        case 164:
        case 11:
        case 29:
        case 47:
        case 48:
        case 95:
        case 52:
        case 51:
        case 46:
          return 200809;
        case 79:
          return 0;
        case 27:
        case 246:
        case 127:
        case 128:
        case 23:
        case 24:
        case 160:
        case 161:
        case 181:
        case 182:
        case 242:
        case 183:
        case 184:
        case 243:
        case 244:
        case 245:
        case 165:
        case 178:
        case 179:
        case 49:
        case 50:
        case 168:
        case 169:
        case 175:
        case 170:
        case 171:
        case 172:
        case 97:
        case 76:
        case 32:
        case 173:
        case 35:
          return -1;
        case 176:
        case 177:
        case 7:
        case 155:
        case 8:
        case 157:
        case 125:
        case 126:
        case 92:
        case 93:
        case 129:
        case 130:
        case 131:
        case 94:
        case 91:
          return 1;
        case 74:
        case 60:
        case 69:
        case 70:
        case 4:
          return 1024;
        case 31:
        case 42:
        case 72:
          return 32;
        case 87:
        case 26:
        case 33:
          return 2147483647;
        case 34:
        case 1:
          return 47839;
        case 38:
        case 36:
          return 99;
        case 43:
        case 37:
          return 2048;
        case 0: return 2097152;
        case 3: return 65536;
        case 28: return 32768;
        case 44: return 32767;
        case 75: return 16384;
        case 39: return 1000;
        case 89: return 700;
        case 71: return 256;
        case 40: return 255;
        case 2: return 100;
        case 180: return 64;
        case 25: return 20;
        case 5: return 16;
        case 6: return 6;
        case 73: return 4;
        case 84: {
          if (typeof navigator === 'object') return navigator['hardwareConcurrency'] || 1;
          return 1;
        }
      }
      ___setErrNo(ERRNO_CODES.EINVAL);
      return -1;
    }

   
  Module["_memset"] = _memset;

  var _BDtoILow=true;

  var _ceil=Math_ceil;

   
  Module["_bitshift64Shl"] = _bitshift64Shl;

  function _abort() {
      Module['abort']();
    }

  
  
  
  
  var ERRNO_MESSAGES={0:"Success",1:"Not super-user",2:"No such file or directory",3:"No such process",4:"Interrupted system call",5:"I/O error",6:"No such device or address",7:"Arg list too long",8:"Exec format error",9:"Bad file number",10:"No children",11:"No more processes",12:"Not enough core",13:"Permission denied",14:"Bad address",15:"Block device required",16:"Mount device busy",17:"File exists",18:"Cross-device link",19:"No such device",20:"Not a directory",21:"Is a directory",22:"Invalid argument",23:"Too many open files in system",24:"Too many open files",25:"Not a typewriter",26:"Text file busy",27:"File too large",28:"No space left on device",29:"Illegal seek",30:"Read only file system",31:"Too many links",32:"Broken pipe",33:"Math arg out of domain of func",34:"Math result not representable",35:"File locking deadlock error",36:"File or path name too long",37:"No record locks available",38:"Function not implemented",39:"Directory not empty",40:"Too many symbolic links",42:"No message of desired type",43:"Identifier removed",44:"Channel number out of range",45:"Level 2 not synchronized",46:"Level 3 halted",47:"Level 3 reset",48:"Link number out of range",49:"Protocol driver not attached",50:"No CSI structure available",51:"Level 2 halted",52:"Invalid exchange",53:"Invalid request descriptor",54:"Exchange full",55:"No anode",56:"Invalid request code",57:"Invalid slot",59:"Bad font file fmt",60:"Device not a stream",61:"No data (for no delay io)",62:"Timer expired",63:"Out of streams resources",64:"Machine is not on the network",65:"Package not installed",66:"The object is remote",67:"The link has been severed",68:"Advertise error",69:"Srmount error",70:"Communication error on send",71:"Protocol error",72:"Multihop attempted",73:"Cross mount point (not really error)",74:"Trying to read unreadable message",75:"Value too large for defined data type",76:"Given log. name not unique",77:"f.d. invalid for this operation",78:"Remote address changed",79:"Can   access a needed shared lib",80:"Accessing a corrupted shared lib",81:".lib section in a.out corrupted",82:"Attempting to link in too many libs",83:"Attempting to exec a shared library",84:"Illegal byte sequence",86:"Streams pipe error",87:"Too many users",88:"Socket operation on non-socket",89:"Destination address required",90:"Message too long",91:"Protocol wrong type for socket",92:"Protocol not available",93:"Unknown protocol",94:"Socket type not supported",95:"Not supported",96:"Protocol family not supported",97:"Address family not supported by protocol family",98:"Address already in use",99:"Address not available",100:"Network interface is not configured",101:"Network is unreachable",102:"Connection reset by network",103:"Connection aborted",104:"Connection reset by peer",105:"No buffer space available",106:"Socket is already connected",107:"Socket is not connected",108:"Can't send after socket shutdown",109:"Too many references",110:"Connection timed out",111:"Connection refused",112:"Host is down",113:"Host is unreachable",114:"Socket already connected",115:"Connection already in progress",116:"Stale file handle",122:"Quota exceeded",123:"No medium (in tape drive)",125:"Operation canceled",130:"Previous owner died",131:"State not recoverable"};
  
  var PATH={splitPath:function (filename) {
        var splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
        return splitPathRe.exec(filename).slice(1);
      },normalizeArray:function (parts, allowAboveRoot) {
        // if the path tries to go above the root, `up` ends up > 0
        var up = 0;
        for (var i = parts.length - 1; i >= 0; i--) {
          var last = parts[i];
          if (last === '.') {
            parts.splice(i, 1);
          } else if (last === '..') {
            parts.splice(i, 1);
            up++;
          } else if (up) {
            parts.splice(i, 1);
            up--;
          }
        }
        // if the path is allowed to go above the root, restore leading ..s
        if (allowAboveRoot) {
          for (; up--; up) {
            parts.unshift('..');
          }
        }
        return parts;
      },normalize:function (path) {
        var isAbsolute = path.charAt(0) === '/',
            trailingSlash = path.substr(-1) === '/';
        // Normalize the path
        path = PATH.normalizeArray(path.split('/').filter(function(p) {
          return !!p;
        }), !isAbsolute).join('/');
        if (!path && !isAbsolute) {
          path = '.';
        }
        if (path && trailingSlash) {
          path += '/';
        }
        return (isAbsolute ? '/' : '') + path;
      },dirname:function (path) {
        var result = PATH.splitPath(path),
            root = result[0],
            dir = result[1];
        if (!root && !dir) {
          // No dirname whatsoever
          return '.';
        }
        if (dir) {
          // It has a dirname, strip trailing slash
          dir = dir.substr(0, dir.length - 1);
        }
        return root + dir;
      },basename:function (path) {
        // EMSCRIPTEN return '/'' for '/', not an empty string
        if (path === '/') return '/';
        var lastSlash = path.lastIndexOf('/');
        if (lastSlash === -1) return path;
        return path.substr(lastSlash+1);
      },extname:function (path) {
        return PATH.splitPath(path)[3];
      },join:function () {
        var paths = Array.prototype.slice.call(arguments, 0);
        return PATH.normalize(paths.join('/'));
      },join2:function (l, r) {
        return PATH.normalize(l + '/' + r);
      },resolve:function () {
        var resolvedPath = '',
          resolvedAbsolute = false;
        for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
          var path = (i >= 0) ? arguments[i] : FS.cwd();
          // Skip empty and invalid entries
          if (typeof path !== 'string') {
            throw new TypeError('Arguments to path.resolve must be strings');
          } else if (!path) {
            return ''; // an invalid portion invalidates the whole thing
          }
          resolvedPath = path + '/' + resolvedPath;
          resolvedAbsolute = path.charAt(0) === '/';
        }
        // At this point the path should be resolved to a full absolute path, but
        // handle relative paths to be safe (might happen when process.cwd() fails)
        resolvedPath = PATH.normalizeArray(resolvedPath.split('/').filter(function(p) {
          return !!p;
        }), !resolvedAbsolute).join('/');
        return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
      },relative:function (from, to) {
        from = PATH.resolve(from).substr(1);
        to = PATH.resolve(to).substr(1);
        function trim(arr) {
          var start = 0;
          for (; start < arr.length; start++) {
            if (arr[start] !== '') break;
          }
          var end = arr.length - 1;
          for (; end >= 0; end--) {
            if (arr[end] !== '') break;
          }
          if (start > end) return [];
          return arr.slice(start, end - start + 1);
        }
        var fromParts = trim(from.split('/'));
        var toParts = trim(to.split('/'));
        var length = Math.min(fromParts.length, toParts.length);
        var samePartsLength = length;
        for (var i = 0; i < length; i++) {
          if (fromParts[i] !== toParts[i]) {
            samePartsLength = i;
            break;
          }
        }
        var outputParts = [];
        for (var i = samePartsLength; i < fromParts.length; i++) {
          outputParts.push('..');
        }
        outputParts = outputParts.concat(toParts.slice(samePartsLength));
        return outputParts.join('/');
      }};
  
  var TTY={ttys:[],init:function () {
        // https://github.com/kripken/emscripten/pull/1555
        // if (ENVIRONMENT_IS_NODE) {
        //   // currently, FS.init does not distinguish if process.stdin is a file or TTY
        //   // device, it always assumes it's a TTY device. because of this, we're forcing
        //   // process.stdin to UTF8 encoding to at least make stdin reading compatible
        //   // with text files until FS.init can be refactored.
        //   process['stdin']['setEncoding']('utf8');
        // }
      },shutdown:function () {
        // https://github.com/kripken/emscripten/pull/1555
        // if (ENVIRONMENT_IS_NODE) {
        //   // inolen: any idea as to why node -e 'process.stdin.read()' wouldn't exit immediately (with process.stdin being a tty)?
        //   // isaacs: because now it's reading from the stream, you've expressed interest in it, so that read() kicks off a _read() which creates a ReadReq operation
        //   // inolen: I thought read() in that case was a synchronous operation that just grabbed some amount of buffered data if it exists?
        //   // isaacs: it is. but it also triggers a _read() call, which calls readStart() on the handle
        //   // isaacs: do process.stdin.pause() and i'd think it'd probably close the pending call
        //   process['stdin']['pause']();
        // }
      },register:function (dev, ops) {
        TTY.ttys[dev] = { input: [], output: [], ops: ops };
        FS.registerDevice(dev, TTY.stream_ops);
      },stream_ops:{open:function (stream) {
          var tty = TTY.ttys[stream.node.rdev];
          if (!tty) {
            throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
          }
          stream.tty = tty;
          stream.seekable = false;
        },close:function (stream) {
          // flush any pending line data
          stream.tty.ops.flush(stream.tty);
        },flush:function (stream) {
          stream.tty.ops.flush(stream.tty);
        },read:function (stream, buffer, offset, length, pos /* ignored */) {
          if (!stream.tty || !stream.tty.ops.get_char) {
            throw new FS.ErrnoError(ERRNO_CODES.ENXIO);
          }
          var bytesRead = 0;
          for (var i = 0; i < length; i++) {
            var result;
            try {
              result = stream.tty.ops.get_char(stream.tty);
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
            if (result === undefined && bytesRead === 0) {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
            if (result === null || result === undefined) break;
            bytesRead++;
            buffer[offset+i] = result;
          }
          if (bytesRead) {
            stream.node.timestamp = Date.now();
          }
          return bytesRead;
        },write:function (stream, buffer, offset, length, pos) {
          if (!stream.tty || !stream.tty.ops.put_char) {
            throw new FS.ErrnoError(ERRNO_CODES.ENXIO);
          }
          for (var i = 0; i < length; i++) {
            try {
              stream.tty.ops.put_char(stream.tty, buffer[offset+i]);
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
          }
          if (length) {
            stream.node.timestamp = Date.now();
          }
          return i;
        }},default_tty_ops:{get_char:function (tty) {
          if (!tty.input.length) {
            var result = null;
            if (ENVIRONMENT_IS_NODE) {
              // we will read data by chunks of BUFSIZE
              var BUFSIZE = 256;
              var buf = new Buffer(BUFSIZE);
              var bytesRead = 0;
  
              var fd = process.stdin.fd;
              // Linux and Mac cannot use process.stdin.fd (which isn't set up as sync)
              var usingDevice = false;
              try {
                fd = fs.openSync('/dev/stdin', 'r');
                usingDevice = true;
              } catch (e) {}
  
              bytesRead = fs.readSync(fd, buf, 0, BUFSIZE, null);
  
              if (usingDevice) { fs.closeSync(fd); }
              if (bytesRead > 0) {
                result = buf.slice(0, bytesRead).toString('utf-8');
              } else {
                result = null;
              }
  
            } else if (typeof window != 'undefined' &&
              typeof window.prompt == 'function') {
              // Browser.
              result = window.prompt('Input: ');  // returns null on cancel
              if (result !== null) {
                result += '\n';
              }
            } else if (typeof readline == 'function') {
              // Command line.
              result = readline();
              if (result !== null) {
                result += '\n';
              }
            }
            if (!result) {
              return null;
            }
            tty.input = intArrayFromString(result, true);
          }
          return tty.input.shift();
        },put_char:function (tty, val) {
          if (val === null || val === 10) {
            Module['print'](UTF8ArrayToString(tty.output, 0));
            tty.output = [];
          } else {
            if (val != 0) tty.output.push(val); // val == 0 would cut text output off in the middle.
          }
        },flush:function (tty) {
          if (tty.output && tty.output.length > 0) {
            Module['print'](UTF8ArrayToString(tty.output, 0));
            tty.output = [];
          }
        }},default_tty1_ops:{put_char:function (tty, val) {
          if (val === null || val === 10) {
            Module['printErr'](UTF8ArrayToString(tty.output, 0));
            tty.output = [];
          } else {
            if (val != 0) tty.output.push(val);
          }
        },flush:function (tty) {
          if (tty.output && tty.output.length > 0) {
            Module['printErr'](UTF8ArrayToString(tty.output, 0));
            tty.output = [];
          }
        }}};
  
  var MEMFS={ops_table:null,mount:function (mount) {
        return MEMFS.createNode(null, '/', 16384 | 511 /* 0777 */, 0);
      },createNode:function (parent, name, mode, dev) {
        if (FS.isBlkdev(mode) || FS.isFIFO(mode)) {
          // no supported
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (!MEMFS.ops_table) {
          MEMFS.ops_table = {
            dir: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr,
                lookup: MEMFS.node_ops.lookup,
                mknod: MEMFS.node_ops.mknod,
                rename: MEMFS.node_ops.rename,
                unlink: MEMFS.node_ops.unlink,
                rmdir: MEMFS.node_ops.rmdir,
                readdir: MEMFS.node_ops.readdir,
                symlink: MEMFS.node_ops.symlink
              },
              stream: {
                llseek: MEMFS.stream_ops.llseek
              }
            },
            file: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr
              },
              stream: {
                llseek: MEMFS.stream_ops.llseek,
                read: MEMFS.stream_ops.read,
                write: MEMFS.stream_ops.write,
                allocate: MEMFS.stream_ops.allocate,
                mmap: MEMFS.stream_ops.mmap,
                msync: MEMFS.stream_ops.msync
              }
            },
            link: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr,
                readlink: MEMFS.node_ops.readlink
              },
              stream: {}
            },
            chrdev: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr
              },
              stream: FS.chrdev_stream_ops
            }
          };
        }
        var node = FS.createNode(parent, name, mode, dev);
        if (FS.isDir(node.mode)) {
          node.node_ops = MEMFS.ops_table.dir.node;
          node.stream_ops = MEMFS.ops_table.dir.stream;
          node.contents = {};
        } else if (FS.isFile(node.mode)) {
          node.node_ops = MEMFS.ops_table.file.node;
          node.stream_ops = MEMFS.ops_table.file.stream;
          node.usedBytes = 0; // The actual number of bytes used in the typed array, as opposed to contents.buffer.byteLength which gives the whole capacity.
          // When the byte data of the file is populated, this will point to either a typed array, or a normal JS array. Typed arrays are preferred
          // for performance, and used by default. However, typed arrays are not resizable like normal JS arrays are, so there is a small disk size
          // penalty involved for appending file writes that continuously grow a file similar to std::vector capacity vs used -scheme.
          node.contents = null; 
        } else if (FS.isLink(node.mode)) {
          node.node_ops = MEMFS.ops_table.link.node;
          node.stream_ops = MEMFS.ops_table.link.stream;
        } else if (FS.isChrdev(node.mode)) {
          node.node_ops = MEMFS.ops_table.chrdev.node;
          node.stream_ops = MEMFS.ops_table.chrdev.stream;
        }
        node.timestamp = Date.now();
        // add the new node to the parent
        if (parent) {
          parent.contents[name] = node;
        }
        return node;
      },getFileDataAsRegularArray:function (node) {
        if (node.contents && node.contents.subarray) {
          var arr = [];
          for (var i = 0; i < node.usedBytes; ++i) arr.push(node.contents[i]);
          return arr; // Returns a copy of the original data.
        }
        return node.contents; // No-op, the file contents are already in a JS array. Return as-is.
      },getFileDataAsTypedArray:function (node) {
        if (!node.contents) return new Uint8Array;
        if (node.contents.subarray) return node.contents.subarray(0, node.usedBytes); // Make sure to not return excess unused bytes.
        return new Uint8Array(node.contents);
      },expandFileStorage:function (node, newCapacity) {
        // If we are asked to expand the size of a file that already exists, revert to using a standard JS array to store the file
        // instead of a typed array. This makes resizing the array more flexible because we can just .push() elements at the back to
        // increase the size.
        if (node.contents && node.contents.subarray && newCapacity > node.contents.length) {
          node.contents = MEMFS.getFileDataAsRegularArray(node);
          node.usedBytes = node.contents.length; // We might be writing to a lazy-loaded file which had overridden this property, so force-reset it.
        }
  
        if (!node.contents || node.contents.subarray) { // Keep using a typed array if creating a new storage, or if old one was a typed array as well.
          var prevCapacity = node.contents ? node.contents.buffer.byteLength : 0;
          if (prevCapacity >= newCapacity) return; // No need to expand, the storage was already large enough.
          // Don't expand strictly to the given requested limit if it's only a very small increase, but instead geometrically grow capacity.
          // For small filesizes (<1MB), perform size*2 geometric increase, but for large sizes, do a much more conservative size*1.125 increase to
          // avoid overshooting the allocation cap by a very large margin.
          var CAPACITY_DOUBLING_MAX = 1024 * 1024;
          newCapacity = Math.max(newCapacity, (prevCapacity * (prevCapacity < CAPACITY_DOUBLING_MAX ? 2.0 : 1.125)) | 0);
          if (prevCapacity != 0) newCapacity = Math.max(newCapacity, 256); // At minimum allocate 256b for each file when expanding.
          var oldContents = node.contents;
          node.contents = new Uint8Array(newCapacity); // Allocate new storage.
          if (node.usedBytes > 0) node.contents.set(oldContents.subarray(0, node.usedBytes), 0); // Copy old data over to the new storage.
          return;
        }
        // Not using a typed array to back the file storage. Use a standard JS array instead.
        if (!node.contents && newCapacity > 0) node.contents = [];
        while (node.contents.length < newCapacity) node.contents.push(0);
      },resizeFileStorage:function (node, newSize) {
        if (node.usedBytes == newSize) return;
        if (newSize == 0) {
          node.contents = null; // Fully decommit when requesting a resize to zero.
          node.usedBytes = 0;
          return;
        }
        if (!node.contents || node.contents.subarray) { // Resize a typed array if that is being used as the backing store.
          var oldContents = node.contents;
          node.contents = new Uint8Array(new ArrayBuffer(newSize)); // Allocate new storage.
          if (oldContents) {
            node.contents.set(oldContents.subarray(0, Math.min(newSize, node.usedBytes))); // Copy old data over to the new storage.
          }
          node.usedBytes = newSize;
          return;
        }
        // Backing with a JS array.
        if (!node.contents) node.contents = [];
        if (node.contents.length > newSize) node.contents.length = newSize;
        else while (node.contents.length < newSize) node.contents.push(0);
        node.usedBytes = newSize;
      },node_ops:{getattr:function (node) {
          var attr = {};
          // device numbers reuse inode numbers.
          attr.dev = FS.isChrdev(node.mode) ? node.id : 1;
          attr.ino = node.id;
          attr.mode = node.mode;
          attr.nlink = 1;
          attr.uid = 0;
          attr.gid = 0;
          attr.rdev = node.rdev;
          if (FS.isDir(node.mode)) {
            attr.size = 4096;
          } else if (FS.isFile(node.mode)) {
            attr.size = node.usedBytes;
          } else if (FS.isLink(node.mode)) {
            attr.size = node.link.length;
          } else {
            attr.size = 0;
          }
          attr.atime = new Date(node.timestamp);
          attr.mtime = new Date(node.timestamp);
          attr.ctime = new Date(node.timestamp);
          // NOTE: In our implementation, st_blocks = Math.ceil(st_size/st_blksize),
          //       but this is not required by the standard.
          attr.blksize = 4096;
          attr.blocks = Math.ceil(attr.size / attr.blksize);
          return attr;
        },setattr:function (node, attr) {
          if (attr.mode !== undefined) {
            node.mode = attr.mode;
          }
          if (attr.timestamp !== undefined) {
            node.timestamp = attr.timestamp;
          }
          if (attr.size !== undefined) {
            MEMFS.resizeFileStorage(node, attr.size);
          }
        },lookup:function (parent, name) {
          throw FS.genericErrors[ERRNO_CODES.ENOENT];
        },mknod:function (parent, name, mode, dev) {
          return MEMFS.createNode(parent, name, mode, dev);
        },rename:function (old_node, new_dir, new_name) {
          // if we're overwriting a directory at new_name, make sure it's empty.
          if (FS.isDir(old_node.mode)) {
            var new_node;
            try {
              new_node = FS.lookupNode(new_dir, new_name);
            } catch (e) {
            }
            if (new_node) {
              for (var i in new_node.contents) {
                throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
              }
            }
          }
          // do the internal rewiring
          delete old_node.parent.contents[old_node.name];
          old_node.name = new_name;
          new_dir.contents[new_name] = old_node;
          old_node.parent = new_dir;
        },unlink:function (parent, name) {
          delete parent.contents[name];
        },rmdir:function (parent, name) {
          var node = FS.lookupNode(parent, name);
          for (var i in node.contents) {
            throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
          }
          delete parent.contents[name];
        },readdir:function (node) {
          var entries = ['.', '..']
          for (var key in node.contents) {
            if (!node.contents.hasOwnProperty(key)) {
              continue;
            }
            entries.push(key);
          }
          return entries;
        },symlink:function (parent, newname, oldpath) {
          var node = MEMFS.createNode(parent, newname, 511 /* 0777 */ | 40960, 0);
          node.link = oldpath;
          return node;
        },readlink:function (node) {
          if (!FS.isLink(node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          return node.link;
        }},stream_ops:{read:function (stream, buffer, offset, length, position) {
          var contents = stream.node.contents;
          if (position >= stream.node.usedBytes) return 0;
          var size = Math.min(stream.node.usedBytes - position, length);
          assert(size >= 0);
          if (size > 8 && contents.subarray) { // non-trivial, and typed array
            buffer.set(contents.subarray(position, position + size), offset);
          } else {
            for (var i = 0; i < size; i++) buffer[offset + i] = contents[position + i];
          }
          return size;
        },write:function (stream, buffer, offset, length, position, canOwn) {
          if (!length) return 0;
          var node = stream.node;
          node.timestamp = Date.now();
  
          if (buffer.subarray && (!node.contents || node.contents.subarray)) { // This write is from a typed array to a typed array?
            if (canOwn) { // Can we just reuse the buffer we are given?
              node.contents = buffer.subarray(offset, offset + length);
              node.usedBytes = length;
              return length;
            } else if (node.usedBytes === 0 && position === 0) { // If this is a simple first write to an empty file, do a fast set since we don't need to care about old data.
              node.contents = new Uint8Array(buffer.subarray(offset, offset + length));
              node.usedBytes = length;
              return length;
            } else if (position + length <= node.usedBytes) { // Writing to an already allocated and used subrange of the file?
              node.contents.set(buffer.subarray(offset, offset + length), position);
              return length;
            }
          }
  
          // Appending to an existing file and we need to reallocate, or source data did not come as a typed array.
          MEMFS.expandFileStorage(node, position+length);
          if (node.contents.subarray && buffer.subarray) node.contents.set(buffer.subarray(offset, offset + length), position); // Use typed array write if available.
          else {
            for (var i = 0; i < length; i++) {
             node.contents[position + i] = buffer[offset + i]; // Or fall back to manual write if not.
            }
          }
          node.usedBytes = Math.max(node.usedBytes, position+length);
          return length;
        },llseek:function (stream, offset, whence) {
          var position = offset;
          if (whence === 1) {  // SEEK_CUR.
            position += stream.position;
          } else if (whence === 2) {  // SEEK_END.
            if (FS.isFile(stream.node.mode)) {
              position += stream.node.usedBytes;
            }
          }
          if (position < 0) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          return position;
        },allocate:function (stream, offset, length) {
          MEMFS.expandFileStorage(stream.node, offset + length);
          stream.node.usedBytes = Math.max(stream.node.usedBytes, offset + length);
        },mmap:function (stream, buffer, offset, length, position, prot, flags) {
          if (!FS.isFile(stream.node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
          }
          var ptr;
          var allocated;
          var contents = stream.node.contents;
          // Only make a new copy when MAP_PRIVATE is specified.
          if ( !(flags & 2) &&
                (contents.buffer === buffer || contents.buffer === buffer.buffer) ) {
            // We can't emulate MAP_SHARED when the file is not backed by the buffer
            // we're mapping to (e.g. the HEAP buffer).
            allocated = false;
            ptr = contents.byteOffset;
          } else {
            // Try to avoid unnecessary slices.
            if (position > 0 || position + length < stream.node.usedBytes) {
              if (contents.subarray) {
                contents = contents.subarray(position, position + length);
              } else {
                contents = Array.prototype.slice.call(contents, position, position + length);
              }
            }
            allocated = true;
            ptr = _malloc(length);
            if (!ptr) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOMEM);
            }
            buffer.set(contents, ptr);
          }
          return { ptr: ptr, allocated: allocated };
        },msync:function (stream, buffer, offset, length, mmapFlags) {
          if (!FS.isFile(stream.node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
          }
          if (mmapFlags & 2) {
            // MAP_PRIVATE calls need not to be synced back to underlying fs
            return 0;
          }
  
          var bytesWritten = MEMFS.stream_ops.write(stream, buffer, 0, length, offset, false);
          // should we check if bytesWritten and length are the same?
          return 0;
        }}};
  
  var IDBFS={dbs:{},indexedDB:function () {
        if (typeof indexedDB !== 'undefined') return indexedDB;
        var ret = null;
        if (typeof window === 'object') ret = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        assert(ret, 'IDBFS used, but indexedDB not supported');
        return ret;
      },DB_VERSION:21,DB_STORE_NAME:"FILE_DATA",mount:function (mount) {
        // reuse all of the core MEMFS functionality
        return MEMFS.mount.apply(null, arguments);
      },syncfs:function (mount, populate, callback) {
        IDBFS.getLocalSet(mount, function(err, local) {
          if (err) return callback(err);
  
          IDBFS.getRemoteSet(mount, function(err, remote) {
            if (err) return callback(err);
  
            var src = populate ? remote : local;
            var dst = populate ? local : remote;
  
            IDBFS.reconcile(src, dst, callback);
          });
        });
      },getDB:function (name, callback) {
        // check the cache first
        var db = IDBFS.dbs[name];
        if (db) {
          return callback(null, db);
        }
  
        var req;
        try {
          req = IDBFS.indexedDB().open(name, IDBFS.DB_VERSION);
        } catch (e) {
          return callback(e);
        }
        req.onupgradeneeded = function(e) {
          var db = e.target.result;
          var transaction = e.target.transaction;
  
          var fileStore;
  
          if (db.objectStoreNames.contains(IDBFS.DB_STORE_NAME)) {
            fileStore = transaction.objectStore(IDBFS.DB_STORE_NAME);
          } else {
            fileStore = db.createObjectStore(IDBFS.DB_STORE_NAME);
          }
  
          if (!fileStore.indexNames.contains('timestamp')) {
            fileStore.createIndex('timestamp', 'timestamp', { unique: false });
          }
        };
        req.onsuccess = function() {
          db = req.result;
  
          // add to the cache
          IDBFS.dbs[name] = db;
          callback(null, db);
        };
        req.onerror = function(e) {
          callback(this.error);
          e.preventDefault();
        };
      },getLocalSet:function (mount, callback) {
        var entries = {};
  
        function isRealDir(p) {
          return p !== '.' && p !== '..';
        };
        function toAbsolute(root) {
          return function(p) {
            return PATH.join2(root, p);
          }
        };
  
        var check = FS.readdir(mount.mountpoint).filter(isRealDir).map(toAbsolute(mount.mountpoint));
  
        while (check.length) {
          var path = check.pop();
          var stat;
  
          try {
            stat = FS.stat(path);
          } catch (e) {
            return callback(e);
          }
  
          if (FS.isDir(stat.mode)) {
            check.push.apply(check, FS.readdir(path).filter(isRealDir).map(toAbsolute(path)));
          }
  
          entries[path] = { timestamp: stat.mtime };
        }
  
        return callback(null, { type: 'local', entries: entries });
      },getRemoteSet:function (mount, callback) {
        var entries = {};
  
        IDBFS.getDB(mount.mountpoint, function(err, db) {
          if (err) return callback(err);
  
          var transaction = db.transaction([IDBFS.DB_STORE_NAME], 'readonly');
          transaction.onerror = function(e) {
            callback(this.error);
            e.preventDefault();
          };
  
          var store = transaction.objectStore(IDBFS.DB_STORE_NAME);
          var index = store.index('timestamp');
  
          index.openKeyCursor().onsuccess = function(event) {
            var cursor = event.target.result;
  
            if (!cursor) {
              return callback(null, { type: 'remote', db: db, entries: entries });
            }
  
            entries[cursor.primaryKey] = { timestamp: cursor.key };
  
            cursor.continue();
          };
        });
      },loadLocalEntry:function (path, callback) {
        var stat, node;
  
        try {
          var lookup = FS.lookupPath(path);
          node = lookup.node;
          stat = FS.stat(path);
        } catch (e) {
          return callback(e);
        }
  
        if (FS.isDir(stat.mode)) {
          return callback(null, { timestamp: stat.mtime, mode: stat.mode });
        } else if (FS.isFile(stat.mode)) {
          // Performance consideration: storing a normal JavaScript array to a IndexedDB is much slower than storing a typed array.
          // Therefore always convert the file contents to a typed array first before writing the data to IndexedDB.
          node.contents = MEMFS.getFileDataAsTypedArray(node);
          return callback(null, { timestamp: stat.mtime, mode: stat.mode, contents: node.contents });
        } else {
          return callback(new Error('node type not supported'));
        }
      },storeLocalEntry:function (path, entry, callback) {
        try {
          if (FS.isDir(entry.mode)) {
            FS.mkdir(path, entry.mode);
          } else if (FS.isFile(entry.mode)) {
            FS.writeFile(path, entry.contents, { encoding: 'binary', canOwn: true });
          } else {
            return callback(new Error('node type not supported'));
          }
  
          FS.chmod(path, entry.mode);
          FS.utime(path, entry.timestamp, entry.timestamp);
        } catch (e) {
          return callback(e);
        }
  
        callback(null);
      },removeLocalEntry:function (path, callback) {
        try {
          var lookup = FS.lookupPath(path);
          var stat = FS.stat(path);
  
          if (FS.isDir(stat.mode)) {
            FS.rmdir(path);
          } else if (FS.isFile(stat.mode)) {
            FS.unlink(path);
          }
        } catch (e) {
          return callback(e);
        }
  
        callback(null);
      },loadRemoteEntry:function (store, path, callback) {
        var req = store.get(path);
        req.onsuccess = function(event) { callback(null, event.target.result); };
        req.onerror = function(e) {
          callback(this.error);
          e.preventDefault();
        };
      },storeRemoteEntry:function (store, path, entry, callback) {
        var req = store.put(entry, path);
        req.onsuccess = function() { callback(null); };
        req.onerror = function(e) {
          callback(this.error);
          e.preventDefault();
        };
      },removeRemoteEntry:function (store, path, callback) {
        var req = store.delete(path);
        req.onsuccess = function() { callback(null); };
        req.onerror = function(e) {
          callback(this.error);
          e.preventDefault();
        };
      },reconcile:function (src, dst, callback) {
        var total = 0;
  
        var create = [];
        Object.keys(src.entries).forEach(function (key) {
          var e = src.entries[key];
          var e2 = dst.entries[key];
          if (!e2 || e.timestamp > e2.timestamp) {
            create.push(key);
            total++;
          }
        });
  
        var remove = [];
        Object.keys(dst.entries).forEach(function (key) {
          var e = dst.entries[key];
          var e2 = src.entries[key];
          if (!e2) {
            remove.push(key);
            total++;
          }
        });
  
        if (!total) {
          return callback(null);
        }
  
        var errored = false;
        var completed = 0;
        var db = src.type === 'remote' ? src.db : dst.db;
        var transaction = db.transaction([IDBFS.DB_STORE_NAME], 'readwrite');
        var store = transaction.objectStore(IDBFS.DB_STORE_NAME);
  
        function done(err) {
          if (err) {
            if (!done.errored) {
              done.errored = true;
              return callback(err);
            }
            return;
          }
          if (++completed >= total) {
            return callback(null);
          }
        };
  
        transaction.onerror = function(e) {
          done(this.error);
          e.preventDefault();
        };
  
        // sort paths in ascending order so directory entries are created
        // before the files inside them
        create.sort().forEach(function (path) {
          if (dst.type === 'local') {
            IDBFS.loadRemoteEntry(store, path, function (err, entry) {
              if (err) return done(err);
              IDBFS.storeLocalEntry(path, entry, done);
            });
          } else {
            IDBFS.loadLocalEntry(path, function (err, entry) {
              if (err) return done(err);
              IDBFS.storeRemoteEntry(store, path, entry, done);
            });
          }
        });
  
        // sort paths in descending order so files are deleted before their
        // parent directories
        remove.sort().reverse().forEach(function(path) {
          if (dst.type === 'local') {
            IDBFS.removeLocalEntry(path, done);
          } else {
            IDBFS.removeRemoteEntry(store, path, done);
          }
        });
      }};
  
  var NODEFS={isWindows:false,staticInit:function () {
        NODEFS.isWindows = !!process.platform.match(/^win/);
      },mount:function (mount) {
        assert(ENVIRONMENT_IS_NODE);
        return NODEFS.createNode(null, '/', NODEFS.getMode(mount.opts.root), 0);
      },createNode:function (parent, name, mode, dev) {
        if (!FS.isDir(mode) && !FS.isFile(mode) && !FS.isLink(mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var node = FS.createNode(parent, name, mode);
        node.node_ops = NODEFS.node_ops;
        node.stream_ops = NODEFS.stream_ops;
        return node;
      },getMode:function (path) {
        var stat;
        try {
          stat = fs.lstatSync(path);
          if (NODEFS.isWindows) {
            // On Windows, directories return permission bits 'rw-rw-rw-', even though they have 'rwxrwxrwx', so
            // propagate write bits to execute bits.
            stat.mode = stat.mode | ((stat.mode & 146) >> 1);
          }
        } catch (e) {
          if (!e.code) throw e;
          throw new FS.ErrnoError(ERRNO_CODES[e.code]);
        }
        return stat.mode;
      },realPath:function (node) {
        var parts = [];
        while (node.parent !== node) {
          parts.push(node.name);
          node = node.parent;
        }
        parts.push(node.mount.opts.root);
        parts.reverse();
        return PATH.join.apply(null, parts);
      },flagsToPermissionStringMap:{0:"r",1:"r+",2:"r+",64:"r",65:"r+",66:"r+",129:"rx+",193:"rx+",514:"w+",577:"w",578:"w+",705:"wx",706:"wx+",1024:"a",1025:"a",1026:"a+",1089:"a",1090:"a+",1153:"ax",1154:"ax+",1217:"ax",1218:"ax+",4096:"rs",4098:"rs+"},flagsToPermissionString:function (flags) {
        if (flags in NODEFS.flagsToPermissionStringMap) {
          return NODEFS.flagsToPermissionStringMap[flags];
        } else {
          return flags;
        }
      },node_ops:{getattr:function (node) {
          var path = NODEFS.realPath(node);
          var stat;
          try {
            stat = fs.lstatSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          // node.js v0.10.20 doesn't report blksize and blocks on Windows. Fake them with default blksize of 4096.
          // See http://support.microsoft.com/kb/140365
          if (NODEFS.isWindows && !stat.blksize) {
            stat.blksize = 4096;
          }
          if (NODEFS.isWindows && !stat.blocks) {
            stat.blocks = (stat.size+stat.blksize-1)/stat.blksize|0;
          }
          return {
            dev: stat.dev,
            ino: stat.ino,
            mode: stat.mode,
            nlink: stat.nlink,
            uid: stat.uid,
            gid: stat.gid,
            rdev: stat.rdev,
            size: stat.size,
            atime: stat.atime,
            mtime: stat.mtime,
            ctime: stat.ctime,
            blksize: stat.blksize,
            blocks: stat.blocks
          };
        },setattr:function (node, attr) {
          var path = NODEFS.realPath(node);
          try {
            if (attr.mode !== undefined) {
              fs.chmodSync(path, attr.mode);
              // update the common node structure mode as well
              node.mode = attr.mode;
            }
            if (attr.timestamp !== undefined) {
              var date = new Date(attr.timestamp);
              fs.utimesSync(path, date, date);
            }
            if (attr.size !== undefined) {
              fs.truncateSync(path, attr.size);
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },lookup:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          var mode = NODEFS.getMode(path);
          return NODEFS.createNode(parent, name, mode);
        },mknod:function (parent, name, mode, dev) {
          var node = NODEFS.createNode(parent, name, mode, dev);
          // create the backing node for this in the fs root as well
          var path = NODEFS.realPath(node);
          try {
            if (FS.isDir(node.mode)) {
              fs.mkdirSync(path, node.mode);
            } else {
              fs.writeFileSync(path, '', { mode: node.mode });
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          return node;
        },rename:function (oldNode, newDir, newName) {
          var oldPath = NODEFS.realPath(oldNode);
          var newPath = PATH.join2(NODEFS.realPath(newDir), newName);
          try {
            fs.renameSync(oldPath, newPath);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },unlink:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          try {
            fs.unlinkSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },rmdir:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          try {
            fs.rmdirSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },readdir:function (node) {
          var path = NODEFS.realPath(node);
          try {
            return fs.readdirSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },symlink:function (parent, newName, oldPath) {
          var newPath = PATH.join2(NODEFS.realPath(parent), newName);
          try {
            fs.symlinkSync(oldPath, newPath);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },readlink:function (node) {
          var path = NODEFS.realPath(node);
          try {
            path = fs.readlinkSync(path);
            path = NODEJS_PATH.relative(NODEJS_PATH.resolve(node.mount.opts.root), path);
            return path;
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        }},stream_ops:{open:function (stream) {
          var path = NODEFS.realPath(stream.node);
          try {
            if (FS.isFile(stream.node.mode)) {
              stream.nfd = fs.openSync(path, NODEFS.flagsToPermissionString(stream.flags));
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },close:function (stream) {
          try {
            if (FS.isFile(stream.node.mode) && stream.nfd) {
              fs.closeSync(stream.nfd);
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },read:function (stream, buffer, offset, length, position) {
          if (length === 0) return 0; // node errors on 0 length reads
          // FIXME this is terrible.
          var nbuffer = new Buffer(length);
          var res;
          try {
            res = fs.readSync(stream.nfd, nbuffer, 0, length, position);
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          if (res > 0) {
            for (var i = 0; i < res; i++) {
              buffer[offset + i] = nbuffer[i];
            }
          }
          return res;
        },write:function (stream, buffer, offset, length, position) {
          // FIXME this is terrible.
          var nbuffer = new Buffer(buffer.subarray(offset, offset + length));
          var res;
          try {
            res = fs.writeSync(stream.nfd, nbuffer, 0, length, position);
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          return res;
        },llseek:function (stream, offset, whence) {
          var position = offset;
          if (whence === 1) {  // SEEK_CUR.
            position += stream.position;
          } else if (whence === 2) {  // SEEK_END.
            if (FS.isFile(stream.node.mode)) {
              try {
                var stat = fs.fstatSync(stream.nfd);
                position += stat.size;
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES[e.code]);
              }
            }
          }
  
          if (position < 0) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
  
          return position;
        }}};
  
  var _stdin=allocate(1, "i32*", ALLOC_STATIC);
  
  var _stdout=allocate(1, "i32*", ALLOC_STATIC);
  
  var _stderr=allocate(1, "i32*", ALLOC_STATIC);
  
  function _fflush(stream) {
      // int fflush(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fflush.html
  
      /*
      // Disabled, see https://github.com/kripken/emscripten/issues/2770
      stream = FS.getStreamFromPtr(stream);
      if (stream.stream_ops.flush) {
        stream.stream_ops.flush(stream);
      }
      */
    }var FS={root:null,mounts:[],devices:[null],streams:[],nextInode:1,nameTable:null,currentPath:"/",initialized:false,ignorePermissions:true,trackingDelegate:{},tracking:{openFlags:{READ:1,WRITE:2}},ErrnoError:null,genericErrors:{},handleFSError:function (e) {
        if (!(e instanceof FS.ErrnoError)) throw e + ' : ' + stackTrace();
        return ___setErrNo(e.errno);
      },lookupPath:function (path, opts) {
        path = PATH.resolve(FS.cwd(), path);
        opts = opts || {};
  
        if (!path) return { path: '', node: null };
  
        var defaults = {
          follow_mount: true,
          recurse_count: 0
        };
        for (var key in defaults) {
          if (opts[key] === undefined) {
            opts[key] = defaults[key];
          }
        }
  
        if (opts.recurse_count > 8) {  // max recursive lookup of 8
          throw new FS.ErrnoError(ERRNO_CODES.ELOOP);
        }
  
        // split the path
        var parts = PATH.normalizeArray(path.split('/').filter(function(p) {
          return !!p;
        }), false);
  
        // start at the root
        var current = FS.root;
        var current_path = '/';
  
        for (var i = 0; i < parts.length; i++) {
          var islast = (i === parts.length-1);
          if (islast && opts.parent) {
            // stop resolving
            break;
          }
  
          current = FS.lookupNode(current, parts[i]);
          current_path = PATH.join2(current_path, parts[i]);
  
          // jump to the mount's root node if this is a mountpoint
          if (FS.isMountpoint(current)) {
            if (!islast || (islast && opts.follow_mount)) {
              current = current.mounted.root;
            }
          }
  
          // by default, lookupPath will not follow a symlink if it is the final path component.
          // setting opts.follow = true will override this behavior.
          if (!islast || opts.follow) {
            var count = 0;
            while (FS.isLink(current.mode)) {
              var link = FS.readlink(current_path);
              current_path = PATH.resolve(PATH.dirname(current_path), link);
  
              var lookup = FS.lookupPath(current_path, { recurse_count: opts.recurse_count });
              current = lookup.node;
  
              if (count++ > 40) {  // limit max consecutive symlinks to 40 (SYMLOOP_MAX).
                throw new FS.ErrnoError(ERRNO_CODES.ELOOP);
              }
            }
          }
        }
  
        return { path: current_path, node: current };
      },getPath:function (node) {
        var path;
        while (true) {
          if (FS.isRoot(node)) {
            var mount = node.mount.mountpoint;
            if (!path) return mount;
            return mount[mount.length-1] !== '/' ? mount + '/' + path : mount + path;
          }
          path = path ? node.name + '/' + path : node.name;
          node = node.parent;
        }
      },hashName:function (parentid, name) {
        var hash = 0;
  
  
        for (var i = 0; i < name.length; i++) {
          hash = ((hash << 5) - hash + name.charCodeAt(i)) | 0;
        }
        return ((parentid + hash) >>> 0) % FS.nameTable.length;
      },hashAddNode:function (node) {
        var hash = FS.hashName(node.parent.id, node.name);
        node.name_next = FS.nameTable[hash];
        FS.nameTable[hash] = node;
      },hashRemoveNode:function (node) {
        var hash = FS.hashName(node.parent.id, node.name);
        if (FS.nameTable[hash] === node) {
          FS.nameTable[hash] = node.name_next;
        } else {
          var current = FS.nameTable[hash];
          while (current) {
            if (current.name_next === node) {
              current.name_next = node.name_next;
              break;
            }
            current = current.name_next;
          }
        }
      },lookupNode:function (parent, name) {
        var err = FS.mayLookup(parent);
        if (err) {
          throw new FS.ErrnoError(err, parent);
        }
        var hash = FS.hashName(parent.id, name);
        for (var node = FS.nameTable[hash]; node; node = node.name_next) {
          var nodeName = node.name;
          if (node.parent.id === parent.id && nodeName === name) {
            return node;
          }
        }
        // if we failed to find it in the cache, call into the VFS
        return FS.lookup(parent, name);
      },createNode:function (parent, name, mode, rdev) {
        if (!FS.FSNode) {
          FS.FSNode = function(parent, name, mode, rdev) {
            if (!parent) {
              parent = this;  // root node sets parent to itself
            }
            this.parent = parent;
            this.mount = parent.mount;
            this.mounted = null;
            this.id = FS.nextInode++;
            this.name = name;
            this.mode = mode;
            this.node_ops = {};
            this.stream_ops = {};
            this.rdev = rdev;
          };
  
          FS.FSNode.prototype = {};
  
          // compatibility
          var readMode = 292 | 73;
          var writeMode = 146;
  
          // NOTE we must use Object.defineProperties instead of individual calls to
          // Object.defineProperty in order to make closure compiler happy
          Object.defineProperties(FS.FSNode.prototype, {
            read: {
              get: function() { return (this.mode & readMode) === readMode; },
              set: function(val) { val ? this.mode |= readMode : this.mode &= ~readMode; }
            },
            write: {
              get: function() { return (this.mode & writeMode) === writeMode; },
              set: function(val) { val ? this.mode |= writeMode : this.mode &= ~writeMode; }
            },
            isFolder: {
              get: function() { return FS.isDir(this.mode); }
            },
            isDevice: {
              get: function() { return FS.isChrdev(this.mode); }
            }
          });
        }
  
        var node = new FS.FSNode(parent, name, mode, rdev);
  
        FS.hashAddNode(node);
  
        return node;
      },destroyNode:function (node) {
        FS.hashRemoveNode(node);
      },isRoot:function (node) {
        return node === node.parent;
      },isMountpoint:function (node) {
        return !!node.mounted;
      },isFile:function (mode) {
        return (mode & 61440) === 32768;
      },isDir:function (mode) {
        return (mode & 61440) === 16384;
      },isLink:function (mode) {
        return (mode & 61440) === 40960;
      },isChrdev:function (mode) {
        return (mode & 61440) === 8192;
      },isBlkdev:function (mode) {
        return (mode & 61440) === 24576;
      },isFIFO:function (mode) {
        return (mode & 61440) === 4096;
      },isSocket:function (mode) {
        return (mode & 49152) === 49152;
      },flagModes:{"r":0,"rs":1052672,"r+":2,"w":577,"wx":705,"xw":705,"w+":578,"wx+":706,"xw+":706,"a":1089,"ax":1217,"xa":1217,"a+":1090,"ax+":1218,"xa+":1218},modeStringToFlags:function (str) {
        var flags = FS.flagModes[str];
        if (typeof flags === 'undefined') {
          throw new Error('Unknown file open mode: ' + str);
        }
        return flags;
      },flagsToPermissionString:function (flag) {
        var accmode = flag & 2097155;
        var perms = ['r', 'w', 'rw'][accmode];
        if ((flag & 512)) {
          perms += 'w';
        }
        return perms;
      },nodePermissions:function (node, perms) {
        if (FS.ignorePermissions) {
          return 0;
        }
        // return 0 if any user, group or owner bits are set.
        if (perms.indexOf('r') !== -1 && !(node.mode & 292)) {
          return ERRNO_CODES.EACCES;
        } else if (perms.indexOf('w') !== -1 && !(node.mode & 146)) {
          return ERRNO_CODES.EACCES;
        } else if (perms.indexOf('x') !== -1 && !(node.mode & 73)) {
          return ERRNO_CODES.EACCES;
        }
        return 0;
      },mayLookup:function (dir) {
        var err = FS.nodePermissions(dir, 'x');
        if (err) return err;
        if (!dir.node_ops.lookup) return ERRNO_CODES.EACCES;
        return 0;
      },mayCreate:function (dir, name) {
        try {
          var node = FS.lookupNode(dir, name);
          return ERRNO_CODES.EEXIST;
        } catch (e) {
        }
        return FS.nodePermissions(dir, 'wx');
      },mayDelete:function (dir, name, isdir) {
        var node;
        try {
          node = FS.lookupNode(dir, name);
        } catch (e) {
          return e.errno;
        }
        var err = FS.nodePermissions(dir, 'wx');
        if (err) {
          return err;
        }
        if (isdir) {
          if (!FS.isDir(node.mode)) {
            return ERRNO_CODES.ENOTDIR;
          }
          if (FS.isRoot(node) || FS.getPath(node) === FS.cwd()) {
            return ERRNO_CODES.EBUSY;
          }
        } else {
          if (FS.isDir(node.mode)) {
            return ERRNO_CODES.EISDIR;
          }
        }
        return 0;
      },mayOpen:function (node, flags) {
        if (!node) {
          return ERRNO_CODES.ENOENT;
        }
        if (FS.isLink(node.mode)) {
          return ERRNO_CODES.ELOOP;
        } else if (FS.isDir(node.mode)) {
          if ((flags & 2097155) !== 0 ||  // opening for write
              (flags & 512)) {
            return ERRNO_CODES.EISDIR;
          }
        }
        return FS.nodePermissions(node, FS.flagsToPermissionString(flags));
      },MAX_OPEN_FDS:4096,nextfd:function (fd_start, fd_end) {
        fd_start = fd_start || 0;
        fd_end = fd_end || FS.MAX_OPEN_FDS;
        for (var fd = fd_start; fd <= fd_end; fd++) {
          if (!FS.streams[fd]) {
            return fd;
          }
        }
        throw new FS.ErrnoError(ERRNO_CODES.EMFILE);
      },getStream:function (fd) {
        return FS.streams[fd];
      },createStream:function (stream, fd_start, fd_end) {
        if (!FS.FSStream) {
          FS.FSStream = function(){};
          FS.FSStream.prototype = {};
          // compatibility
          Object.defineProperties(FS.FSStream.prototype, {
            object: {
              get: function() { return this.node; },
              set: function(val) { this.node = val; }
            },
            isRead: {
              get: function() { return (this.flags & 2097155) !== 1; }
            },
            isWrite: {
              get: function() { return (this.flags & 2097155) !== 0; }
            },
            isAppend: {
              get: function() { return (this.flags & 1024); }
            }
          });
        }
        // clone it, so we can return an instance of FSStream
        var newStream = new FS.FSStream();
        for (var p in stream) {
          newStream[p] = stream[p];
        }
        stream = newStream;
        var fd = FS.nextfd(fd_start, fd_end);
        stream.fd = fd;
        FS.streams[fd] = stream;
        return stream;
      },closeStream:function (fd) {
        FS.streams[fd] = null;
      },getStreamFromPtr:function (ptr) {
        return FS.streams[ptr - 1];
      },getPtrForStream:function (stream) {
        return stream ? stream.fd + 1 : 0;
      },chrdev_stream_ops:{open:function (stream) {
          var device = FS.getDevice(stream.node.rdev);
          // override node's stream ops with the device's
          stream.stream_ops = device.stream_ops;
          // forward the open call
          if (stream.stream_ops.open) {
            stream.stream_ops.open(stream);
          }
        },llseek:function () {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }},major:function (dev) {
        return ((dev) >> 8);
      },minor:function (dev) {
        return ((dev) & 0xff);
      },makedev:function (ma, mi) {
        return ((ma) << 8 | (mi));
      },registerDevice:function (dev, ops) {
        FS.devices[dev] = { stream_ops: ops };
      },getDevice:function (dev) {
        return FS.devices[dev];
      },getMounts:function (mount) {
        var mounts = [];
        var check = [mount];
  
        while (check.length) {
          var m = check.pop();
  
          mounts.push(m);
  
          check.push.apply(check, m.mounts);
        }
  
        return mounts;
      },syncfs:function (populate, callback) {
        if (typeof(populate) === 'function') {
          callback = populate;
          populate = false;
        }
  
        var mounts = FS.getMounts(FS.root.mount);
        var completed = 0;
  
        function done(err) {
          if (err) {
            if (!done.errored) {
              done.errored = true;
              return callback(err);
            }
            return;
          }
          if (++completed >= mounts.length) {
            callback(null);
          }
        };
  
        // sync all mounts
        mounts.forEach(function (mount) {
          if (!mount.type.syncfs) {
            return done(null);
          }
          mount.type.syncfs(mount, populate, done);
        });
      },mount:function (type, opts, mountpoint) {
        var root = mountpoint === '/';
        var pseudo = !mountpoint;
        var node;
  
        if (root && FS.root) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        } else if (!root && !pseudo) {
          var lookup = FS.lookupPath(mountpoint, { follow_mount: false });
  
          mountpoint = lookup.path;  // use the absolute path
          node = lookup.node;
  
          if (FS.isMountpoint(node)) {
            throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
          }
  
          if (!FS.isDir(node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
          }
        }
  
        var mount = {
          type: type,
          opts: opts,
          mountpoint: mountpoint,
          mounts: []
        };
  
        // create a root node for the fs
        var mountRoot = type.mount(mount);
        mountRoot.mount = mount;
        mount.root = mountRoot;
  
        if (root) {
          FS.root = mountRoot;
        } else if (node) {
          // set as a mountpoint
          node.mounted = mount;
  
          // add the new mount to the current mount's children
          if (node.mount) {
            node.mount.mounts.push(mount);
          }
        }
  
        return mountRoot;
      },unmount:function (mountpoint) {
        var lookup = FS.lookupPath(mountpoint, { follow_mount: false });
  
        if (!FS.isMountpoint(lookup.node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
  
        // destroy the nodes for this mount, and all its child mounts
        var node = lookup.node;
        var mount = node.mounted;
        var mounts = FS.getMounts(mount);
  
        Object.keys(FS.nameTable).forEach(function (hash) {
          var current = FS.nameTable[hash];
  
          while (current) {
            var next = current.name_next;
  
            if (mounts.indexOf(current.mount) !== -1) {
              FS.destroyNode(current);
            }
  
            current = next;
          }
        });
  
        // no longer a mountpoint
        node.mounted = null;
  
        // remove this mount from the child mounts
        var idx = node.mount.mounts.indexOf(mount);
        assert(idx !== -1);
        node.mount.mounts.splice(idx, 1);
      },lookup:function (parent, name) {
        return parent.node_ops.lookup(parent, name);
      },mknod:function (path, mode, dev) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        if (!name || name === '.' || name === '..') {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var err = FS.mayCreate(parent, name);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.mknod) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return parent.node_ops.mknod(parent, name, mode, dev);
      },create:function (path, mode) {
        mode = mode !== undefined ? mode : 438 /* 0666 */;
        mode &= 4095;
        mode |= 32768;
        return FS.mknod(path, mode, 0);
      },mkdir:function (path, mode) {
        mode = mode !== undefined ? mode : 511 /* 0777 */;
        mode &= 511 | 512;
        mode |= 16384;
        return FS.mknod(path, mode, 0);
      },mkdev:function (path, mode, dev) {
        if (typeof(dev) === 'undefined') {
          dev = mode;
          mode = 438 /* 0666 */;
        }
        mode |= 8192;
        return FS.mknod(path, mode, dev);
      },symlink:function (oldpath, newpath) {
        if (!PATH.resolve(oldpath)) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        var lookup = FS.lookupPath(newpath, { parent: true });
        var parent = lookup.node;
        if (!parent) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        var newname = PATH.basename(newpath);
        var err = FS.mayCreate(parent, newname);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.symlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return parent.node_ops.symlink(parent, newname, oldpath);
      },rename:function (old_path, new_path) {
        var old_dirname = PATH.dirname(old_path);
        var new_dirname = PATH.dirname(new_path);
        var old_name = PATH.basename(old_path);
        var new_name = PATH.basename(new_path);
        // parents must exist
        var lookup, old_dir, new_dir;
        try {
          lookup = FS.lookupPath(old_path, { parent: true });
          old_dir = lookup.node;
          lookup = FS.lookupPath(new_path, { parent: true });
          new_dir = lookup.node;
        } catch (e) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        if (!old_dir || !new_dir) throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        // need to be part of the same mount
        if (old_dir.mount !== new_dir.mount) {
          throw new FS.ErrnoError(ERRNO_CODES.EXDEV);
        }
        // source must exist
        var old_node = FS.lookupNode(old_dir, old_name);
        // old path should not be an ancestor of the new path
        var relative = PATH.relative(old_path, new_dirname);
        if (relative.charAt(0) !== '.') {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        // new path should not be an ancestor of the old path
        relative = PATH.relative(new_path, old_dirname);
        if (relative.charAt(0) !== '.') {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
        }
        // see if the new path already exists
        var new_node;
        try {
          new_node = FS.lookupNode(new_dir, new_name);
        } catch (e) {
          // not fatal
        }
        // early out if nothing needs to change
        if (old_node === new_node) {
          return;
        }
        // we'll need to delete the old entry
        var isdir = FS.isDir(old_node.mode);
        var err = FS.mayDelete(old_dir, old_name, isdir);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        // need delete permissions if we'll be overwriting.
        // need create permissions if new doesn't already exist.
        err = new_node ?
          FS.mayDelete(new_dir, new_name, isdir) :
          FS.mayCreate(new_dir, new_name);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!old_dir.node_ops.rename) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(old_node) || (new_node && FS.isMountpoint(new_node))) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        // if we are going to change the parent, check write permissions
        if (new_dir !== old_dir) {
          err = FS.nodePermissions(old_dir, 'w');
          if (err) {
            throw new FS.ErrnoError(err);
          }
        }
        try {
          if (FS.trackingDelegate['willMovePath']) {
            FS.trackingDelegate['willMovePath'](old_path, new_path);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['willMovePath']('"+old_path+"', '"+new_path+"') threw an exception: " + e.message);
        }
        // remove the node from the lookup hash
        FS.hashRemoveNode(old_node);
        // do the underlying fs rename
        try {
          old_dir.node_ops.rename(old_node, new_dir, new_name);
        } catch (e) {
          throw e;
        } finally {
          // add the node back to the hash (in case node_ops.rename
          // changed its name)
          FS.hashAddNode(old_node);
        }
        try {
          if (FS.trackingDelegate['onMovePath']) FS.trackingDelegate['onMovePath'](old_path, new_path);
        } catch(e) {
          console.log("FS.trackingDelegate['onMovePath']('"+old_path+"', '"+new_path+"') threw an exception: " + e.message);
        }
      },rmdir:function (path) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        var node = FS.lookupNode(parent, name);
        var err = FS.mayDelete(parent, name, true);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.rmdir) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        try {
          if (FS.trackingDelegate['willDeletePath']) {
            FS.trackingDelegate['willDeletePath'](path);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['willDeletePath']('"+path+"') threw an exception: " + e.message);
        }
        parent.node_ops.rmdir(parent, name);
        FS.destroyNode(node);
        try {
          if (FS.trackingDelegate['onDeletePath']) FS.trackingDelegate['onDeletePath'](path);
        } catch(e) {
          console.log("FS.trackingDelegate['onDeletePath']('"+path+"') threw an exception: " + e.message);
        }
      },readdir:function (path) {
        var lookup = FS.lookupPath(path, { follow: true });
        var node = lookup.node;
        if (!node.node_ops.readdir) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
        }
        return node.node_ops.readdir(node);
      },unlink:function (path) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        var node = FS.lookupNode(parent, name);
        var err = FS.mayDelete(parent, name, false);
        if (err) {
          // POSIX says unlink should set EPERM, not EISDIR
          if (err === ERRNO_CODES.EISDIR) err = ERRNO_CODES.EPERM;
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.unlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        try {
          if (FS.trackingDelegate['willDeletePath']) {
            FS.trackingDelegate['willDeletePath'](path);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['willDeletePath']('"+path+"') threw an exception: " + e.message);
        }
        parent.node_ops.unlink(parent, name);
        FS.destroyNode(node);
        try {
          if (FS.trackingDelegate['onDeletePath']) FS.trackingDelegate['onDeletePath'](path);
        } catch(e) {
          console.log("FS.trackingDelegate['onDeletePath']('"+path+"') threw an exception: " + e.message);
        }
      },readlink:function (path) {
        var lookup = FS.lookupPath(path);
        var link = lookup.node;
        if (!link) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        if (!link.node_ops.readlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        return PATH.resolve(FS.getPath(lookup.node.parent), link.node_ops.readlink(link));
      },stat:function (path, dontFollow) {
        var lookup = FS.lookupPath(path, { follow: !dontFollow });
        var node = lookup.node;
        if (!node) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        if (!node.node_ops.getattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return node.node_ops.getattr(node);
      },lstat:function (path) {
        return FS.stat(path, true);
      },chmod:function (path, mode, dontFollow) {
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: !dontFollow });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        node.node_ops.setattr(node, {
          mode: (mode & 4095) | (node.mode & ~4095),
          timestamp: Date.now()
        });
      },lchmod:function (path, mode) {
        FS.chmod(path, mode, true);
      },fchmod:function (fd, mode) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        FS.chmod(stream.node, mode);
      },chown:function (path, uid, gid, dontFollow) {
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: !dontFollow });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        node.node_ops.setattr(node, {
          timestamp: Date.now()
          // we ignore the uid / gid for now
        });
      },lchown:function (path, uid, gid) {
        FS.chown(path, uid, gid, true);
      },fchown:function (fd, uid, gid) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        FS.chown(stream.node, uid, gid);
      },truncate:function (path, len) {
        if (len < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: true });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isDir(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!FS.isFile(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var err = FS.nodePermissions(node, 'w');
        if (err) {
          throw new FS.ErrnoError(err);
        }
        node.node_ops.setattr(node, {
          size: len,
          timestamp: Date.now()
        });
      },ftruncate:function (fd, len) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        FS.truncate(stream.node, len);
      },utime:function (path, atime, mtime) {
        var lookup = FS.lookupPath(path, { follow: true });
        var node = lookup.node;
        node.node_ops.setattr(node, {
          timestamp: Math.max(atime, mtime)
        });
      },open:function (path, flags, mode, fd_start, fd_end) {
        if (path === "") {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        flags = typeof flags === 'string' ? FS.modeStringToFlags(flags) : flags;
        mode = typeof mode === 'undefined' ? 438 /* 0666 */ : mode;
        if ((flags & 64)) {
          mode = (mode & 4095) | 32768;
        } else {
          mode = 0;
        }
        var node;
        if (typeof path === 'object') {
          node = path;
        } else {
          path = PATH.normalize(path);
          try {
            var lookup = FS.lookupPath(path, {
              follow: !(flags & 131072)
            });
            node = lookup.node;
          } catch (e) {
            // ignore
          }
        }
        // perhaps we need to create the node
        var created = false;
        if ((flags & 64)) {
          if (node) {
            // if O_CREAT and O_EXCL are set, error out if the node already exists
            if ((flags & 128)) {
              throw new FS.ErrnoError(ERRNO_CODES.EEXIST);
            }
          } else {
            // node doesn't exist, try to create it
            node = FS.mknod(path, mode, 0);
            created = true;
          }
        }
        if (!node) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        // can't truncate a device
        if (FS.isChrdev(node.mode)) {
          flags &= ~512;
        }
        // check permissions, if this is not a file we just created now (it is ok to
        // create and write to a file with read-only permissions; it is read-only
        // for later use)
        if (!created) {
          var err = FS.mayOpen(node, flags);
          if (err) {
            throw new FS.ErrnoError(err);
          }
        }
        // do truncation if necessary
        if ((flags & 512)) {
          FS.truncate(node, 0);
        }
        // we've already handled these, don't pass down to the underlying vfs
        flags &= ~(128 | 512);
  
        // register the stream with the filesystem
        var stream = FS.createStream({
          node: node,
          path: FS.getPath(node),  // we want the absolute path to the node
          flags: flags,
          seekable: true,
          position: 0,
          stream_ops: node.stream_ops,
          // used by the file family libc calls (fopen, fwrite, ferror, etc.)
          ungotten: [],
          error: false
        }, fd_start, fd_end);
        // call the new stream's open function
        if (stream.stream_ops.open) {
          stream.stream_ops.open(stream);
        }
        if (Module['logReadFiles'] && !(flags & 1)) {
          if (!FS.readFiles) FS.readFiles = {};
          if (!(path in FS.readFiles)) {
            FS.readFiles[path] = 1;
            Module['printErr']('read file: ' + path);
          }
        }
        try {
          if (FS.trackingDelegate['onOpenFile']) {
            var trackingFlags = 0;
            if ((flags & 2097155) !== 1) {
              trackingFlags |= FS.tracking.openFlags.READ;
            }
            if ((flags & 2097155) !== 0) {
              trackingFlags |= FS.tracking.openFlags.WRITE;
            }
            FS.trackingDelegate['onOpenFile'](path, trackingFlags);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['onOpenFile']('"+path+"', flags) threw an exception: " + e.message);
        }
        return stream;
      },close:function (stream) {
        try {
          if (stream.stream_ops.close) {
            stream.stream_ops.close(stream);
          }
        } catch (e) {
          throw e;
        } finally {
          FS.closeStream(stream.fd);
        }
      },llseek:function (stream, offset, whence) {
        if (!stream.seekable || !stream.stream_ops.llseek) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        stream.position = stream.stream_ops.llseek(stream, offset, whence);
        stream.ungotten = [];
        return stream.position;
      },read:function (stream, buffer, offset, length, position) {
        if (length < 0 || position < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 1) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (FS.isDir(stream.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!stream.stream_ops.read) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var seeking = true;
        if (typeof position === 'undefined') {
          position = stream.position;
          seeking = false;
        } else if (!stream.seekable) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        var bytesRead = stream.stream_ops.read(stream, buffer, offset, length, position);
        if (!seeking) stream.position += bytesRead;
        return bytesRead;
      },write:function (stream, buffer, offset, length, position, canOwn) {
        if (length < 0 || position < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (FS.isDir(stream.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!stream.stream_ops.write) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if (stream.flags & 1024) {
          // seek to the end before writing in append mode
          FS.llseek(stream, 0, 2);
        }
        var seeking = true;
        if (typeof position === 'undefined') {
          position = stream.position;
          seeking = false;
        } else if (!stream.seekable) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        var bytesWritten = stream.stream_ops.write(stream, buffer, offset, length, position, canOwn);
        if (!seeking) stream.position += bytesWritten;
        try {
          if (stream.path && FS.trackingDelegate['onWriteToFile']) FS.trackingDelegate['onWriteToFile'](stream.path);
        } catch(e) {
          console.log("FS.trackingDelegate['onWriteToFile']('"+path+"') threw an exception: " + e.message);
        }
        return bytesWritten;
      },allocate:function (stream, offset, length) {
        if (offset < 0 || length <= 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (!FS.isFile(stream.node.mode) && !FS.isDir(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
        }
        if (!stream.stream_ops.allocate) {
          throw new FS.ErrnoError(ERRNO_CODES.EOPNOTSUPP);
        }
        stream.stream_ops.allocate(stream, offset, length);
      },mmap:function (stream, buffer, offset, length, position, prot, flags) {
        // TODO if PROT is PROT_WRITE, make sure we have write access
        if ((stream.flags & 2097155) === 1) {
          throw new FS.ErrnoError(ERRNO_CODES.EACCES);
        }
        if (!stream.stream_ops.mmap) {
          throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
        }
        return stream.stream_ops.mmap(stream, buffer, offset, length, position, prot, flags);
      },msync:function (stream, buffer, offset, length, mmapFlags) {
        if (!stream || !stream.stream_ops.msync) {
          return 0;
        }
        return stream.stream_ops.msync(stream, buffer, offset, length, mmapFlags);
      },munmap:function (stream) {
        return 0;
      },ioctl:function (stream, cmd, arg) {
        if (!stream.stream_ops.ioctl) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTTY);
        }
        return stream.stream_ops.ioctl(stream, cmd, arg);
      },readFile:function (path, opts) {
        opts = opts || {};
        opts.flags = opts.flags || 'r';
        opts.encoding = opts.encoding || 'binary';
        if (opts.encoding !== 'utf8' && opts.encoding !== 'binary') {
          throw new Error('Invalid encoding type "' + opts.encoding + '"');
        }
        var ret;
        var stream = FS.open(path, opts.flags);
        var stat = FS.stat(path);
        var length = stat.size;
        var buf = new Uint8Array(length);
        FS.read(stream, buf, 0, length, 0);
        if (opts.encoding === 'utf8') {
          ret = UTF8ArrayToString(buf, 0);
        } else if (opts.encoding === 'binary') {
          ret = buf;
        }
        FS.close(stream);
        return ret;
      },writeFile:function (path, data, opts) {
        opts = opts || {};
        opts.flags = opts.flags || 'w';
        opts.encoding = opts.encoding || 'utf8';
        if (opts.encoding !== 'utf8' && opts.encoding !== 'binary') {
          throw new Error('Invalid encoding type "' + opts.encoding + '"');
        }
        var stream = FS.open(path, opts.flags, opts.mode);
        if (opts.encoding === 'utf8') {
          var buf = new Uint8Array(lengthBytesUTF8(data)+1);
          var actualNumBytes = stringToUTF8Array(data, buf, 0, buf.length);
          FS.write(stream, buf, 0, actualNumBytes, 0, opts.canOwn);
        } else if (opts.encoding === 'binary') {
          FS.write(stream, data, 0, data.length, 0, opts.canOwn);
        }
        FS.close(stream);
      },cwd:function () {
        return FS.currentPath;
      },chdir:function (path) {
        var lookup = FS.lookupPath(path, { follow: true });
        if (!FS.isDir(lookup.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
        }
        var err = FS.nodePermissions(lookup.node, 'x');
        if (err) {
          throw new FS.ErrnoError(err);
        }
        FS.currentPath = lookup.path;
      },createDefaultDirectories:function () {
        FS.mkdir('/tmp');
        FS.mkdir('/home');
        FS.mkdir('/home/web_user');
      },createDefaultDevices:function () {
        // create /dev
        FS.mkdir('/dev');
        // setup /dev/null
        FS.registerDevice(FS.makedev(1, 3), {
          read: function() { return 0; },
          write: function(stream, buffer, offset, length, pos) { return length; }
        });
        FS.mkdev('/dev/null', FS.makedev(1, 3));
        // setup /dev/tty and /dev/tty1
        // stderr needs to print output using Module['printErr']
        // so we register a second tty just for it.
        TTY.register(FS.makedev(5, 0), TTY.default_tty_ops);
        TTY.register(FS.makedev(6, 0), TTY.default_tty1_ops);
        FS.mkdev('/dev/tty', FS.makedev(5, 0));
        FS.mkdev('/dev/tty1', FS.makedev(6, 0));
        // setup /dev/[u]random
        var random_device;
        if (typeof crypto !== 'undefined') {
          // for modern web browsers
          var randomBuffer = new Uint8Array(1);
          random_device = function() { crypto.getRandomValues(randomBuffer); return randomBuffer[0]; };
        } else if (ENVIRONMENT_IS_NODE) {
          // for nodejs
          random_device = function() { return require('crypto').randomBytes(1)[0]; };
        } else {
          // default for ES5 platforms
          random_device = function() { return (Math.random()*256)|0; };
        }
        FS.createDevice('/dev', 'random', random_device);
        FS.createDevice('/dev', 'urandom', random_device);
        // we're not going to emulate the actual shm device,
        // just create the tmp dirs that reside in it commonly
        FS.mkdir('/dev/shm');
        FS.mkdir('/dev/shm/tmp');
      },createStandardStreams:function () {
        // TODO deprecate the old functionality of a single
        // input / output callback and that utilizes FS.createDevice
        // and instead require a unique set of stream ops
  
        // by default, we symlink the standard streams to the
        // default tty devices. however, if the standard streams
        // have been overwritten we create a unique device for
        // them instead.
        if (Module['stdin']) {
          FS.createDevice('/dev', 'stdin', Module['stdin']);
        } else {
          FS.symlink('/dev/tty', '/dev/stdin');
        }
        if (Module['stdout']) {
          FS.createDevice('/dev', 'stdout', null, Module['stdout']);
        } else {
          FS.symlink('/dev/tty', '/dev/stdout');
        }
        if (Module['stderr']) {
          FS.createDevice('/dev', 'stderr', null, Module['stderr']);
        } else {
          FS.symlink('/dev/tty1', '/dev/stderr');
        }
  
        // open default streams for the stdin, stdout and stderr devices
        var stdin = FS.open('/dev/stdin', 'r');
        HEAP32[((_stdin)>>2)]=FS.getPtrForStream(stdin);
        assert(stdin.fd === 0, 'invalid handle for stdin (' + stdin.fd + ')');
  
        var stdout = FS.open('/dev/stdout', 'w');
        HEAP32[((_stdout)>>2)]=FS.getPtrForStream(stdout);
        assert(stdout.fd === 1, 'invalid handle for stdout (' + stdout.fd + ')');
  
        var stderr = FS.open('/dev/stderr', 'w');
        HEAP32[((_stderr)>>2)]=FS.getPtrForStream(stderr);
        assert(stderr.fd === 2, 'invalid handle for stderr (' + stderr.fd + ')');
      },ensureErrnoError:function () {
        if (FS.ErrnoError) return;
        FS.ErrnoError = function ErrnoError(errno, node) {
          this.node = node;
          this.setErrno = function(errno) {
            this.errno = errno;
            for (var key in ERRNO_CODES) {
              if (ERRNO_CODES[key] === errno) {
                this.code = key;
                break;
              }
            }
          };
          this.setErrno(errno);
          this.message = ERRNO_MESSAGES[errno];
        };
        FS.ErrnoError.prototype = new Error();
        FS.ErrnoError.prototype.constructor = FS.ErrnoError;
        // Some errors may happen quite a bit, to avoid overhead we reuse them (and suffer a lack of stack info)
        [ERRNO_CODES.ENOENT].forEach(function(code) {
          FS.genericErrors[code] = new FS.ErrnoError(code);
          FS.genericErrors[code].stack = '<generic error, no stack>';
        });
      },staticInit:function () {
        FS.ensureErrnoError();
  
        FS.nameTable = new Array(4096);
  
        FS.mount(MEMFS, {}, '/');
  
        FS.createDefaultDirectories();
        FS.createDefaultDevices();
      },init:function (input, output, error) {
        assert(!FS.init.initialized, 'FS.init was previously called. If you want to initialize later with custom parameters, remove any earlier calls (note that one is automatically added to the generated code)');
        FS.init.initialized = true;
  
        FS.ensureErrnoError();
  
        // Allow Module.stdin etc. to provide defaults, if none explicitly passed to us here
        Module['stdin'] = input || Module['stdin'];
        Module['stdout'] = output || Module['stdout'];
        Module['stderr'] = error || Module['stderr'];
  
        FS.createStandardStreams();
      },quit:function () {
        FS.init.initialized = false;
        for (var i = 0; i < FS.streams.length; i++) {
          var stream = FS.streams[i];
          if (!stream) {
            continue;
          }
          FS.close(stream);
        }
      },getMode:function (canRead, canWrite) {
        var mode = 0;
        if (canRead) mode |= 292 | 73;
        if (canWrite) mode |= 146;
        return mode;
      },joinPath:function (parts, forceRelative) {
        var path = PATH.join.apply(null, parts);
        if (forceRelative && path[0] == '/') path = path.substr(1);
        return path;
      },absolutePath:function (relative, base) {
        return PATH.resolve(base, relative);
      },standardizePath:function (path) {
        return PATH.normalize(path);
      },findObject:function (path, dontResolveLastLink) {
        var ret = FS.analyzePath(path, dontResolveLastLink);
        if (ret.exists) {
          return ret.object;
        } else {
          ___setErrNo(ret.error);
          return null;
        }
      },analyzePath:function (path, dontResolveLastLink) {
        // operate from within the context of the symlink's target
        try {
          var lookup = FS.lookupPath(path, { follow: !dontResolveLastLink });
          path = lookup.path;
        } catch (e) {
        }
        var ret = {
          isRoot: false, exists: false, error: 0, name: null, path: null, object: null,
          parentExists: false, parentPath: null, parentObject: null
        };
        try {
          var lookup = FS.lookupPath(path, { parent: true });
          ret.parentExists = true;
          ret.parentPath = lookup.path;
          ret.parentObject = lookup.node;
          ret.name = PATH.basename(path);
          lookup = FS.lookupPath(path, { follow: !dontResolveLastLink });
          ret.exists = true;
          ret.path = lookup.path;
          ret.object = lookup.node;
          ret.name = lookup.node.name;
          ret.isRoot = lookup.path === '/';
        } catch (e) {
          ret.error = e.errno;
        };
        return ret;
      },createFolder:function (parent, name, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(canRead, canWrite);
        return FS.mkdir(path, mode);
      },createPath:function (parent, path, canRead, canWrite) {
        parent = typeof parent === 'string' ? parent : FS.getPath(parent);
        var parts = path.split('/').reverse();
        while (parts.length) {
          var part = parts.pop();
          if (!part) continue;
          var current = PATH.join2(parent, part);
          try {
            FS.mkdir(current);
          } catch (e) {
            // ignore EEXIST
          }
          parent = current;
        }
        return current;
      },createFile:function (parent, name, properties, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(canRead, canWrite);
        return FS.create(path, mode);
      },createDataFile:function (parent, name, data, canRead, canWrite, canOwn) {
        var path = name ? PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name) : parent;
        var mode = FS.getMode(canRead, canWrite);
        var node = FS.create(path, mode);
        if (data) {
          if (typeof data === 'string') {
            var arr = new Array(data.length);
            for (var i = 0, len = data.length; i < len; ++i) arr[i] = data.charCodeAt(i);
            data = arr;
          }
          // make sure we can write to the file
          FS.chmod(node, mode | 146);
          var stream = FS.open(node, 'w');
          FS.write(stream, data, 0, data.length, 0, canOwn);
          FS.close(stream);
          FS.chmod(node, mode);
        }
        return node;
      },createDevice:function (parent, name, input, output) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(!!input, !!output);
        if (!FS.createDevice.major) FS.createDevice.major = 64;
        var dev = FS.makedev(FS.createDevice.major++, 0);
        // Create a fake device that a set of stream ops to emulate
        // the old behavior.
        FS.registerDevice(dev, {
          open: function(stream) {
            stream.seekable = false;
          },
          close: function(stream) {
            // flush any pending line data
            if (output && output.buffer && output.buffer.length) {
              output(10);
            }
          },
          read: function(stream, buffer, offset, length, pos /* ignored */) {
            var bytesRead = 0;
            for (var i = 0; i < length; i++) {
              var result;
              try {
                result = input();
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES.EIO);
              }
              if (result === undefined && bytesRead === 0) {
                throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
              }
              if (result === null || result === undefined) break;
              bytesRead++;
              buffer[offset+i] = result;
            }
            if (bytesRead) {
              stream.node.timestamp = Date.now();
            }
            return bytesRead;
          },
          write: function(stream, buffer, offset, length, pos) {
            for (var i = 0; i < length; i++) {
              try {
                output(buffer[offset+i]);
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES.EIO);
              }
            }
            if (length) {
              stream.node.timestamp = Date.now();
            }
            return i;
          }
        });
        return FS.mkdev(path, mode, dev);
      },createLink:function (parent, name, target, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        return FS.symlink(target, path);
      },forceLoadFile:function (obj) {
        if (obj.isDevice || obj.isFolder || obj.link || obj.contents) return true;
        var success = true;
        if (typeof XMLHttpRequest !== 'undefined') {
          throw new Error("Lazy loading should have been performed (contents set) in createLazyFile, but it was not. Lazy loading only works in web workers. Use --embed-file or --preload-file in emcc on the main thread.");
        } else if (Module['read']) {
          // Command-line.
          try {
            // WARNING: Can't read binary files in V8's d8 or tracemonkey's js, as
            //          read() will try to parse UTF8.
            obj.contents = intArrayFromString(Module['read'](obj.url), true);
            obj.usedBytes = obj.contents.length;
          } catch (e) {
            success = false;
          }
        } else {
          throw new Error('Cannot load without read() or XMLHttpRequest.');
        }
        if (!success) ___setErrNo(ERRNO_CODES.EIO);
        return success;
      },createLazyFile:function (parent, name, url, canRead, canWrite) {
        // Lazy chunked Uint8Array (implements get and length from Uint8Array). Actual getting is abstracted away for eventual reuse.
        function LazyUint8Array() {
          this.lengthKnown = false;
          this.chunks = []; // Loaded chunks. Index is the chunk number
        }
        LazyUint8Array.prototype.get = function LazyUint8Array_get(idx) {
          if (idx > this.length-1 || idx < 0) {
            return undefined;
          }
          var chunkOffset = idx % this.chunkSize;
          var chunkNum = (idx / this.chunkSize)|0;
          return this.getter(chunkNum)[chunkOffset];
        }
        LazyUint8Array.prototype.setDataGetter = function LazyUint8Array_setDataGetter(getter) {
          this.getter = getter;
        }
        LazyUint8Array.prototype.cacheLength = function LazyUint8Array_cacheLength() {
          // Find length
          var xhr = new XMLHttpRequest();
          xhr.open('HEAD', url, false);
          xhr.send(null);
          if (!(xhr.status >= 200 && xhr.status < 300 || xhr.status === 304)) throw new Error("Couldn't load " + url + ". Status: " + xhr.status);
          var datalength = Number(xhr.getResponseHeader("Content-length"));
          var header;
          var hasByteServing = (header = xhr.getResponseHeader("Accept-Ranges")) && header === "bytes";
          var chunkSize = 1024*1024; // Chunk size in bytes
  
          if (!hasByteServing) chunkSize = datalength;
  
          // Function to get a range from the remote URL.
          var doXHR = (function(from, to) {
            if (from > to) throw new Error("invalid range (" + from + ", " + to + ") or no bytes requested!");
            if (to > datalength-1) throw new Error("only " + datalength + " bytes available! programmer error!");
  
            // TODO: Use mozResponseArrayBuffer, responseStream, etc. if available.
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, false);
            if (datalength !== chunkSize) xhr.setRequestHeader("Range", "bytes=" + from + "-" + to);
  
            // Some hints to the browser that we want binary data.
            if (typeof Uint8Array != 'undefined') xhr.responseType = 'arraybuffer';
            if (xhr.overrideMimeType) {
              xhr.overrideMimeType('text/plain; charset=x-user-defined');
            }
  
            xhr.send(null);
            if (!(xhr.status >= 200 && xhr.status < 300 || xhr.status === 304)) throw new Error("Couldn't load " + url + ". Status: " + xhr.status);
            if (xhr.response !== undefined) {
              return new Uint8Array(xhr.response || []);
            } else {
              return intArrayFromString(xhr.responseText || '', true);
            }
          });
          var lazyArray = this;
          lazyArray.setDataGetter(function(chunkNum) {
            var start = chunkNum * chunkSize;
            var end = (chunkNum+1) * chunkSize - 1; // including this byte
            end = Math.min(end, datalength-1); // if datalength-1 is selected, this is the last block
            if (typeof(lazyArray.chunks[chunkNum]) === "undefined") {
              lazyArray.chunks[chunkNum] = doXHR(start, end);
            }
            if (typeof(lazyArray.chunks[chunkNum]) === "undefined") throw new Error("doXHR failed!");
            return lazyArray.chunks[chunkNum];
          });
  
          this._length = datalength;
          this._chunkSize = chunkSize;
          this.lengthKnown = true;
        }
        if (typeof XMLHttpRequest !== 'undefined') {
          if (!ENVIRONMENT_IS_WORKER) throw 'Cannot do synchronous binary XHRs outside webworkers in modern browsers. Use --embed-file or --preload-file in emcc';
          var lazyArray = new LazyUint8Array();
          Object.defineProperty(lazyArray, "length", {
              get: function() {
                  if(!this.lengthKnown) {
                      this.cacheLength();
                  }
                  return this._length;
              }
          });
          Object.defineProperty(lazyArray, "chunkSize", {
              get: function() {
                  if(!this.lengthKnown) {
                      this.cacheLength();
                  }
                  return this._chunkSize;
              }
          });
  
          var properties = { isDevice: false, contents: lazyArray };
        } else {
          var properties = { isDevice: false, url: url };
        }
  
        var node = FS.createFile(parent, name, properties, canRead, canWrite);
        // This is a total hack, but I want to get this lazy file code out of the
        // core of MEMFS. If we want to keep this lazy file concept I feel it should
        // be its own thin LAZYFS proxying calls to MEMFS.
        if (properties.contents) {
          node.contents = properties.contents;
        } else if (properties.url) {
          node.contents = null;
          node.url = properties.url;
        }
        // Add a function that defers querying the file size until it is asked the first time.
        Object.defineProperty(node, "usedBytes", {
            get: function() { return this.contents.length; }
        });
        // override each stream op with one that tries to force load the lazy file first
        var stream_ops = {};
        var keys = Object.keys(node.stream_ops);
        keys.forEach(function(key) {
          var fn = node.stream_ops[key];
          stream_ops[key] = function forceLoadLazyFile() {
            if (!FS.forceLoadFile(node)) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
            return fn.apply(null, arguments);
          };
        });
        // use a custom read function
        stream_ops.read = function stream_ops_read(stream, buffer, offset, length, position) {
          if (!FS.forceLoadFile(node)) {
            throw new FS.ErrnoError(ERRNO_CODES.EIO);
          }
          var contents = stream.node.contents;
          if (position >= contents.length)
            return 0;
          var size = Math.min(contents.length - position, length);
          assert(size >= 0);
          if (contents.slice) { // normal array
            for (var i = 0; i < size; i++) {
              buffer[offset + i] = contents[position + i];
            }
          } else {
            for (var i = 0; i < size; i++) { // LazyUint8Array from sync binary XHR
              buffer[offset + i] = contents.get(position + i);
            }
          }
          return size;
        };
        node.stream_ops = stream_ops;
        return node;
      },createPreloadedFile:function (parent, name, url, canRead, canWrite, onload, onerror, dontCreateFile, canOwn, preFinish) {
        Browser.init();
        // TODO we should allow people to just pass in a complete filename instead
        // of parent and name being that we just join them anyways
        var fullname = name ? PATH.resolve(PATH.join2(parent, name)) : parent;
        var dep = getUniqueRunDependency('cp ' + fullname); // might have several active requests for the same fullname
        function processData(byteArray) {
          function finish(byteArray) {
            if (preFinish) preFinish();
            if (!dontCreateFile) {
              FS.createDataFile(parent, name, byteArray, canRead, canWrite, canOwn);
            }
            if (onload) onload();
            removeRunDependency(dep);
          }
          var handled = false;
          Module['preloadPlugins'].forEach(function(plugin) {
            if (handled) return;
            if (plugin['canHandle'](fullname)) {
              plugin['handle'](byteArray, fullname, finish, function() {
                if (onerror) onerror();
                removeRunDependency(dep);
              });
              handled = true;
            }
          });
          if (!handled) finish(byteArray);
        }
        addRunDependency(dep);
        if (typeof url == 'string') {
          Browser.asyncLoad(url, function(byteArray) {
            processData(byteArray);
          }, onerror);
        } else {
          processData(url);
        }
      },indexedDB:function () {
        return window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
      },DB_NAME:function () {
        return 'EM_FS_' + window.location.pathname;
      },DB_VERSION:20,DB_STORE_NAME:"FILE_DATA",saveFilesToDB:function (paths, onload, onerror) {
        onload = onload || function(){};
        onerror = onerror || function(){};
        var indexedDB = FS.indexedDB();
        try {
          var openRequest = indexedDB.open(FS.DB_NAME(), FS.DB_VERSION);
        } catch (e) {
          return onerror(e);
        }
        openRequest.onupgradeneeded = function openRequest_onupgradeneeded() {
          console.log('creating db');
          var db = openRequest.result;
          db.createObjectStore(FS.DB_STORE_NAME);
        };
        openRequest.onsuccess = function openRequest_onsuccess() {
          var db = openRequest.result;
          var transaction = db.transaction([FS.DB_STORE_NAME], 'readwrite');
          var files = transaction.objectStore(FS.DB_STORE_NAME);
          var ok = 0, fail = 0, total = paths.length;
          function finish() {
            if (fail == 0) onload(); else onerror();
          }
          paths.forEach(function(path) {
            var putRequest = files.put(FS.analyzePath(path).object.contents, path);
            putRequest.onsuccess = function putRequest_onsuccess() { ok++; if (ok + fail == total) finish() };
            putRequest.onerror = function putRequest_onerror() { fail++; if (ok + fail == total) finish() };
          });
          transaction.onerror = onerror;
        };
        openRequest.onerror = onerror;
      },loadFilesFromDB:function (paths, onload, onerror) {
        onload = onload || function(){};
        onerror = onerror || function(){};
        var indexedDB = FS.indexedDB();
        try {
          var openRequest = indexedDB.open(FS.DB_NAME(), FS.DB_VERSION);
        } catch (e) {
          return onerror(e);
        }
        openRequest.onupgradeneeded = onerror; // no database to load from
        openRequest.onsuccess = function openRequest_onsuccess() {
          var db = openRequest.result;
          try {
            var transaction = db.transaction([FS.DB_STORE_NAME], 'readonly');
          } catch(e) {
            onerror(e);
            return;
          }
          var files = transaction.objectStore(FS.DB_STORE_NAME);
          var ok = 0, fail = 0, total = paths.length;
          function finish() {
            if (fail == 0) onload(); else onerror();
          }
          paths.forEach(function(path) {
            var getRequest = files.get(path);
            getRequest.onsuccess = function getRequest_onsuccess() {
              if (FS.analyzePath(path).exists) {
                FS.unlink(path);
              }
              FS.createDataFile(PATH.dirname(path), PATH.basename(path), getRequest.result, true, true, true);
              ok++;
              if (ok + fail == total) finish();
            };
            getRequest.onerror = function getRequest_onerror() { fail++; if (ok + fail == total) finish() };
          });
          transaction.onerror = onerror;
        };
        openRequest.onerror = onerror;
      }};
  
  
  
  
  function _mkport() { throw 'TODO' }var SOCKFS={mount:function (mount) {
        // If Module['websocket'] has already been defined (e.g. for configuring
        // the subprotocol/url) use that, if not initialise it to a new object.
        Module['websocket'] = (Module['websocket'] && 
                               ('object' === typeof Module['websocket'])) ? Module['websocket'] : {};
  
        // Add the Event registration mechanism to the exported websocket configuration
        // object so we can register network callbacks from native JavaScript too.
        // For more documentation see system/include/emscripten/emscripten.h
        Module['websocket']._callbacks = {};
        Module['websocket']['on'] = function(event, callback) {
  	    if ('function' === typeof callback) {
  		  this._callbacks[event] = callback;
          }
  	    return this;
        };
  
        Module['websocket'].emit = function(event, param) {
  	    if ('function' === typeof this._callbacks[event]) {
  		  this._callbacks[event].call(this, param);
          }
        };
  
        // If debug is enabled register simple default logging callbacks for each Event.
  
        return FS.createNode(null, '/', 16384 | 511 /* 0777 */, 0);
      },createSocket:function (family, type, protocol) {
        var streaming = type == 1;
        if (protocol) {
          assert(streaming == (protocol == 6)); // if SOCK_STREAM, must be tcp
        }
  
        // create our internal socket structure
        var sock = {
          family: family,
          type: type,
          protocol: protocol,
          server: null,
          error: null, // Used in getsockopt for SOL_SOCKET/SO_ERROR test
          peers: {},
          pending: [],
          recv_queue: [],
          sock_ops: SOCKFS.websocket_sock_ops
        };
  
        // create the filesystem node to store the socket structure
        var name = SOCKFS.nextname();
        var node = FS.createNode(SOCKFS.root, name, 49152, 0);
        node.sock = sock;
  
        // and the wrapping stream that enables library functions such
        // as read and write to indirectly interact with the socket
        var stream = FS.createStream({
          path: name,
          node: node,
          flags: FS.modeStringToFlags('r+'),
          seekable: false,
          stream_ops: SOCKFS.stream_ops
        });
  
        // map the new stream to the socket structure (sockets have a 1:1
        // relationship with a stream)
        sock.stream = stream;
  
        return sock;
      },getSocket:function (fd) {
        var stream = FS.getStream(fd);
        if (!stream || !FS.isSocket(stream.node.mode)) {
          return null;
        }
        return stream.node.sock;
      },stream_ops:{poll:function (stream) {
          var sock = stream.node.sock;
          return sock.sock_ops.poll(sock);
        },ioctl:function (stream, request, varargs) {
          var sock = stream.node.sock;
          return sock.sock_ops.ioctl(sock, request, varargs);
        },read:function (stream, buffer, offset, length, position /* ignored */) {
          var sock = stream.node.sock;
          var msg = sock.sock_ops.recvmsg(sock, length);
          if (!msg) {
            // socket is closed
            return 0;
          }
          buffer.set(msg.buffer, offset);
          return msg.buffer.length;
        },write:function (stream, buffer, offset, length, position /* ignored */) {
          var sock = stream.node.sock;
          return sock.sock_ops.sendmsg(sock, buffer, offset, length);
        },close:function (stream) {
          var sock = stream.node.sock;
          sock.sock_ops.close(sock);
        }},nextname:function () {
        if (!SOCKFS.nextname.current) {
          SOCKFS.nextname.current = 0;
        }
        return 'socket[' + (SOCKFS.nextname.current++) + ']';
      },websocket_sock_ops:{createPeer:function (sock, addr, port) {
          var ws;
  
          if (typeof addr === 'object') {
            ws = addr;
            addr = null;
            port = null;
          }
  
          if (ws) {
            // for sockets that've already connected (e.g. we're the server)
            // we can inspect the _socket property for the address
            if (ws._socket) {
              addr = ws._socket.remoteAddress;
              port = ws._socket.remotePort;
            }
            // if we're just now initializing a connection to the remote,
            // inspect the url property
            else {
              var result = /ws[s]?:\/\/([^:]+):(\d+)/.exec(ws.url);
              if (!result) {
                throw new Error('WebSocket URL must be in the format ws(s)://address:port');
              }
              addr = result[1];
              port = parseInt(result[2], 10);
            }
          } else {
            // create the actual websocket object and connect
            try {
              // runtimeConfig gets set to true if WebSocket runtime configuration is available.
              var runtimeConfig = (Module['websocket'] && ('object' === typeof Module['websocket']));
  
              // The default value is 'ws://' the replace is needed because the compiler replaces '//' comments with '#'
              // comments without checking context, so we'd end up with ws:#, the replace swaps the '#' for '//' again.
              var url = 'ws:#'.replace('#', '//');
  
              if (runtimeConfig) {
                if ('string' === typeof Module['websocket']['url']) {
                  url = Module['websocket']['url']; // Fetch runtime WebSocket URL config.
                }
              }
  
              if (url === 'ws://' || url === 'wss://') { // Is the supplied URL config just a prefix, if so complete it.
                var parts = addr.split('/');
                url = url + parts[0] + ":" + port + "/" + parts.slice(1).join('/');
              }
  
              // Make the WebSocket subprotocol (Sec-WebSocket-Protocol) default to binary if no configuration is set.
              var subProtocols = 'binary'; // The default value is 'binary'
  
              if (runtimeConfig) {
                if ('string' === typeof Module['websocket']['subprotocol']) {
                  subProtocols = Module['websocket']['subprotocol']; // Fetch runtime WebSocket subprotocol config.
                }
              }
  
              // The regex trims the string (removes spaces at the beginning and end, then splits the string by
              // <any space>,<any space> into an Array. Whitespace removal is important for Websockify and ws.
              subProtocols = subProtocols.replace(/^ +| +$/g,"").split(/ *, */);
  
              // The node ws library API for specifying optional subprotocol is slightly different than the browser's.
              var opts = ENVIRONMENT_IS_NODE ? {'protocol': subProtocols.toString()} : subProtocols;
  
              // If node we use the ws library.
              var WebSocket = ENVIRONMENT_IS_NODE ? require('ws') : window['WebSocket'];
              ws = new WebSocket(url, opts);
              ws.binaryType = 'arraybuffer';
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EHOSTUNREACH);
            }
          }
  
  
          var peer = {
            addr: addr,
            port: port,
            socket: ws,
            dgram_send_queue: []
          };
  
          SOCKFS.websocket_sock_ops.addPeer(sock, peer);
          SOCKFS.websocket_sock_ops.handlePeerEvents(sock, peer);
  
          // if this is a bound dgram socket, send the port number first to allow
          // us to override the ephemeral port reported to us by remotePort on the
          // remote end.
          if (sock.type === 2 && typeof sock.sport !== 'undefined') {
            peer.dgram_send_queue.push(new Uint8Array([
                255, 255, 255, 255,
                'p'.charCodeAt(0), 'o'.charCodeAt(0), 'r'.charCodeAt(0), 't'.charCodeAt(0),
                ((sock.sport & 0xff00) >> 8) , (sock.sport & 0xff)
            ]));
          }
  
          return peer;
        },getPeer:function (sock, addr, port) {
          return sock.peers[addr + ':' + port];
        },addPeer:function (sock, peer) {
          sock.peers[peer.addr + ':' + peer.port] = peer;
        },removePeer:function (sock, peer) {
          delete sock.peers[peer.addr + ':' + peer.port];
        },handlePeerEvents:function (sock, peer) {
          var first = true;
  
          var handleOpen = function () {
  
            Module['websocket'].emit('open', sock.stream.fd);
  
            try {
              var queued = peer.dgram_send_queue.shift();
              while (queued) {
                peer.socket.send(queued);
                queued = peer.dgram_send_queue.shift();
              }
            } catch (e) {
              // not much we can do here in the way of proper error handling as we've already
              // lied and said this data was sent. shut it down.
              peer.socket.close();
            }
          };
  
          function handleMessage(data) {
            assert(typeof data !== 'string' && data.byteLength !== undefined);  // must receive an ArrayBuffer
            data = new Uint8Array(data);  // make a typed array view on the array buffer
  
  
            // if this is the port message, override the peer's port with it
            var wasfirst = first;
            first = false;
            if (wasfirst &&
                data.length === 10 &&
                data[0] === 255 && data[1] === 255 && data[2] === 255 && data[3] === 255 &&
                data[4] === 'p'.charCodeAt(0) && data[5] === 'o'.charCodeAt(0) && data[6] === 'r'.charCodeAt(0) && data[7] === 't'.charCodeAt(0)) {
              // update the peer's port and it's key in the peer map
              var newport = ((data[8] << 8) | data[9]);
              SOCKFS.websocket_sock_ops.removePeer(sock, peer);
              peer.port = newport;
              SOCKFS.websocket_sock_ops.addPeer(sock, peer);
              return;
            }
  
            sock.recv_queue.push({ addr: peer.addr, port: peer.port, data: data });
            Module['websocket'].emit('message', sock.stream.fd);
          };
  
          if (ENVIRONMENT_IS_NODE) {
            peer.socket.on('open', handleOpen);
            peer.socket.on('message', function(data, flags) {
              if (!flags.binary) {
                return;
              }
              handleMessage((new Uint8Array(data)).buffer);  // copy from node Buffer -> ArrayBuffer
            });
            peer.socket.on('close', function() {
              Module['websocket'].emit('close', sock.stream.fd);
            });
            peer.socket.on('error', function(error) {
              // Although the ws library may pass errors that may be more descriptive than
              // ECONNREFUSED they are not necessarily the expected error code e.g. 
              // ENOTFOUND on getaddrinfo seems to be node.js specific, so using ECONNREFUSED
              // is still probably the most useful thing to do.
              sock.error = ERRNO_CODES.ECONNREFUSED; // Used in getsockopt for SOL_SOCKET/SO_ERROR test.
              Module['websocket'].emit('error', [sock.stream.fd, sock.error, 'ECONNREFUSED: Connection refused']);
              // don't throw
            });
          } else {
            peer.socket.onopen = handleOpen;
            peer.socket.onclose = function() {
              Module['websocket'].emit('close', sock.stream.fd);
            };
            peer.socket.onmessage = function peer_socket_onmessage(event) {
              handleMessage(event.data);
            };
            peer.socket.onerror = function(error) {
              // The WebSocket spec only allows a 'simple event' to be thrown on error,
              // so we only really know as much as ECONNREFUSED.
              sock.error = ERRNO_CODES.ECONNREFUSED; // Used in getsockopt for SOL_SOCKET/SO_ERROR test.
              Module['websocket'].emit('error', [sock.stream.fd, sock.error, 'ECONNREFUSED: Connection refused']);
            };
          }
        },poll:function (sock) {
          if (sock.type === 1 && sock.server) {
            // listen sockets should only say they're available for reading
            // if there are pending clients.
            return sock.pending.length ? (64 | 1) : 0;
          }
  
          var mask = 0;
          var dest = sock.type === 1 ?  // we only care about the socket state for connection-based sockets
            SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport) :
            null;
  
          if (sock.recv_queue.length ||
              !dest ||  // connection-less sockets are always ready to read
              (dest && dest.socket.readyState === dest.socket.CLOSING) ||
              (dest && dest.socket.readyState === dest.socket.CLOSED)) {  // let recv return 0 once closed
            mask |= (64 | 1);
          }
  
          if (!dest ||  // connection-less sockets are always ready to write
              (dest && dest.socket.readyState === dest.socket.OPEN)) {
            mask |= 4;
          }
  
          if ((dest && dest.socket.readyState === dest.socket.CLOSING) ||
              (dest && dest.socket.readyState === dest.socket.CLOSED)) {
            mask |= 16;
          }
  
          return mask;
        },ioctl:function (sock, request, arg) {
          switch (request) {
            case 21531:
              var bytes = 0;
              if (sock.recv_queue.length) {
                bytes = sock.recv_queue[0].data.length;
              }
              HEAP32[((arg)>>2)]=bytes;
              return 0;
            default:
              return ERRNO_CODES.EINVAL;
          }
        },close:function (sock) {
          // if we've spawned a listen server, close it
          if (sock.server) {
            try {
              sock.server.close();
            } catch (e) {
            }
            sock.server = null;
          }
          // close any peer connections
          var peers = Object.keys(sock.peers);
          for (var i = 0; i < peers.length; i++) {
            var peer = sock.peers[peers[i]];
            try {
              peer.socket.close();
            } catch (e) {
            }
            SOCKFS.websocket_sock_ops.removePeer(sock, peer);
          }
          return 0;
        },bind:function (sock, addr, port) {
          if (typeof sock.saddr !== 'undefined' || typeof sock.sport !== 'undefined') {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);  // already bound
          }
          sock.saddr = addr;
          sock.sport = port || _mkport();
          // in order to emulate dgram sockets, we need to launch a listen server when
          // binding on a connection-less socket
          // note: this is only required on the server side
          if (sock.type === 2) {
            // close the existing server if it exists
            if (sock.server) {
              sock.server.close();
              sock.server = null;
            }
            // swallow error operation not supported error that occurs when binding in the
            // browser where this isn't supported
            try {
              sock.sock_ops.listen(sock, 0);
            } catch (e) {
              if (!(e instanceof FS.ErrnoError)) throw e;
              if (e.errno !== ERRNO_CODES.EOPNOTSUPP) throw e;
            }
          }
        },connect:function (sock, addr, port) {
          if (sock.server) {
            throw new FS.ErrnoError(ERRNO_CODES.EOPNOTSUPP);
          }
  
          // TODO autobind
          // if (!sock.addr && sock.type == 2) {
          // }
  
          // early out if we're already connected / in the middle of connecting
          if (typeof sock.daddr !== 'undefined' && typeof sock.dport !== 'undefined') {
            var dest = SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport);
            if (dest) {
              if (dest.socket.readyState === dest.socket.CONNECTING) {
                throw new FS.ErrnoError(ERRNO_CODES.EALREADY);
              } else {
                throw new FS.ErrnoError(ERRNO_CODES.EISCONN);
              }
            }
          }
  
          // add the socket to our peer list and set our
          // destination address / port to match
          var peer = SOCKFS.websocket_sock_ops.createPeer(sock, addr, port);
          sock.daddr = peer.addr;
          sock.dport = peer.port;
  
          // always "fail" in non-blocking mode
          throw new FS.ErrnoError(ERRNO_CODES.EINPROGRESS);
        },listen:function (sock, backlog) {
          if (!ENVIRONMENT_IS_NODE) {
            throw new FS.ErrnoError(ERRNO_CODES.EOPNOTSUPP);
          }
          if (sock.server) {
             throw new FS.ErrnoError(ERRNO_CODES.EINVAL);  // already listening
          }
          var WebSocketServer = require('ws').Server;
          var host = sock.saddr;
          sock.server = new WebSocketServer({
            host: host,
            port: sock.sport
            // TODO support backlog
          });
          Module['websocket'].emit('listen', sock.stream.fd); // Send Event with listen fd.
  
          sock.server.on('connection', function(ws) {
            if (sock.type === 1) {
              var newsock = SOCKFS.createSocket(sock.family, sock.type, sock.protocol);
  
              // create a peer on the new socket
              var peer = SOCKFS.websocket_sock_ops.createPeer(newsock, ws);
              newsock.daddr = peer.addr;
              newsock.dport = peer.port;
  
              // push to queue for accept to pick up
              sock.pending.push(newsock);
              Module['websocket'].emit('connection', newsock.stream.fd);
            } else {
              // create a peer on the listen socket so calling sendto
              // with the listen socket and an address will resolve
              // to the correct client
              SOCKFS.websocket_sock_ops.createPeer(sock, ws);
              Module['websocket'].emit('connection', sock.stream.fd);
            }
          });
          sock.server.on('closed', function() {
            Module['websocket'].emit('close', sock.stream.fd);
            sock.server = null;
          });
          sock.server.on('error', function(error) {
            // Although the ws library may pass errors that may be more descriptive than
            // ECONNREFUSED they are not necessarily the expected error code e.g. 
            // ENOTFOUND on getaddrinfo seems to be node.js specific, so using EHOSTUNREACH
            // is still probably the most useful thing to do. This error shouldn't
            // occur in a well written app as errors should get trapped in the compiled
            // app's own getaddrinfo call.
            sock.error = ERRNO_CODES.EHOSTUNREACH; // Used in getsockopt for SOL_SOCKET/SO_ERROR test.
            Module['websocket'].emit('error', [sock.stream.fd, sock.error, 'EHOSTUNREACH: Host is unreachable']);
            // don't throw
          });
        },accept:function (listensock) {
          if (!listensock.server) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          var newsock = listensock.pending.shift();
          newsock.stream.flags = listensock.stream.flags;
          return newsock;
        },getname:function (sock, peer) {
          var addr, port;
          if (peer) {
            if (sock.daddr === undefined || sock.dport === undefined) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
            }
            addr = sock.daddr;
            port = sock.dport;
          } else {
            // TODO saddr and sport will be set for bind()'d UDP sockets, but what
            // should we be returning for TCP sockets that've been connect()'d?
            addr = sock.saddr || 0;
            port = sock.sport || 0;
          }
          return { addr: addr, port: port };
        },sendmsg:function (sock, buffer, offset, length, addr, port) {
          if (sock.type === 2) {
            // connection-less sockets will honor the message address,
            // and otherwise fall back to the bound destination address
            if (addr === undefined || port === undefined) {
              addr = sock.daddr;
              port = sock.dport;
            }
            // if there was no address to fall back to, error out
            if (addr === undefined || port === undefined) {
              throw new FS.ErrnoError(ERRNO_CODES.EDESTADDRREQ);
            }
          } else {
            // connection-based sockets will only use the bound
            addr = sock.daddr;
            port = sock.dport;
          }
  
          // find the peer for the destination address
          var dest = SOCKFS.websocket_sock_ops.getPeer(sock, addr, port);
  
          // early out if not connected with a connection-based socket
          if (sock.type === 1) {
            if (!dest || dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
            } else if (dest.socket.readyState === dest.socket.CONNECTING) {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
          }
  
          // create a copy of the incoming data to send, as the WebSocket API
          // doesn't work entirely with an ArrayBufferView, it'll just send
          // the entire underlying buffer
          var data;
          if (buffer instanceof Array || buffer instanceof ArrayBuffer) {
            data = buffer.slice(offset, offset + length);
          } else {  // ArrayBufferView
            data = buffer.buffer.slice(buffer.byteOffset + offset, buffer.byteOffset + offset + length);
          }
  
          // if we're emulating a connection-less dgram socket and don't have
          // a cached connection, queue the buffer to send upon connect and
          // lie, saying the data was sent now.
          if (sock.type === 2) {
            if (!dest || dest.socket.readyState !== dest.socket.OPEN) {
              // if we're not connected, open a new connection
              if (!dest || dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
                dest = SOCKFS.websocket_sock_ops.createPeer(sock, addr, port);
              }
              dest.dgram_send_queue.push(data);
              return length;
            }
          }
  
          try {
            // send the actual data
            dest.socket.send(data);
            return length;
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
        },recvmsg:function (sock, length) {
          // http://pubs.opengroup.org/onlinepubs/7908799/xns/recvmsg.html
          if (sock.type === 1 && sock.server) {
            // tcp servers should not be recv()'ing on the listen socket
            throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
          }
  
          var queued = sock.recv_queue.shift();
          if (!queued) {
            if (sock.type === 1) {
              var dest = SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport);
  
              if (!dest) {
                // if we have a destination address but are not connected, error out
                throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
              }
              else if (dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
                // return null if the socket has closed
                return null;
              }
              else {
                // else, our socket is in a valid state but truly has nothing available
                throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
              }
            } else {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
          }
  
          // queued.data will be an ArrayBuffer if it's unadulterated, but if it's
          // requeued TCP data it'll be an ArrayBufferView
          var queuedLength = queued.data.byteLength || queued.data.length;
          var queuedOffset = queued.data.byteOffset || 0;
          var queuedBuffer = queued.data.buffer || queued.data;
          var bytesRead = Math.min(length, queuedLength);
          var res = {
            buffer: new Uint8Array(queuedBuffer, queuedOffset, bytesRead),
            addr: queued.addr,
            port: queued.port
          };
  
  
          // push back any unread data for TCP connections
          if (sock.type === 1 && bytesRead < queuedLength) {
            var bytesRemaining = queuedLength - bytesRead;
            queued.data = new Uint8Array(queuedBuffer, queuedOffset + bytesRead, bytesRemaining);
            sock.recv_queue.unshift(queued);
          }
  
          return res;
        }}};function _send(fd, buf, len, flags) {
      var sock = SOCKFS.getSocket(fd);
      if (!sock) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      // TODO honor flags
      return _write(fd, buf, len);
    }
  
  function _pwrite(fildes, buf, nbyte, offset) {
      // ssize_t pwrite(int fildes, const void *buf, size_t nbyte, off_t offset);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/write.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        var slab = HEAP8;
        return FS.write(stream, slab, buf, nbyte, offset);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _write(fildes, buf, nbyte) {
      // ssize_t write(int fildes, const void *buf, size_t nbyte);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/write.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
  
  
      try {
        var slab = HEAP8;
        return FS.write(stream, slab, buf, nbyte);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }
  
  function _fileno(stream) {
      // int fileno(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fileno.html
      stream = FS.getStreamFromPtr(stream);
      if (!stream) return -1;
      return stream.fd;
    }function _fwrite(ptr, size, nitems, stream) {
      // size_t fwrite(const void *restrict ptr, size_t size, size_t nitems, FILE *restrict stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fwrite.html
      var bytesToWrite = nitems * size;
      if (bytesToWrite == 0) return 0;
      var fd = _fileno(stream);
      var bytesWritten = _write(fd, ptr, bytesToWrite);
      if (bytesWritten == -1) {
        var streamObj = FS.getStreamFromPtr(stream);
        if (streamObj) streamObj.error = true;
        return 0;
      } else {
        return (bytesWritten / size)|0;
      }
    }
  
  
   
  Module["_strlen"] = _strlen;
  
  function __reallyNegative(x) {
      return x < 0 || (x === 0 && (1/x) === -Infinity);
    }function __formatString(format, varargs) {
      assert((varargs & 3) === 0);
      var textIndex = format;
      var argIndex = 0;
      function getNextArg(type) {
        // NOTE: Explicitly ignoring type safety. Otherwise this fails:
        //       int x = 4; printf("%c\n", (char)x);
        var ret;
        argIndex = Runtime.prepVararg(argIndex, type);
        if (type === 'double') {
          ret = (HEAP32[((tempDoublePtr)>>2)]=HEAP32[(((varargs)+(argIndex))>>2)],HEAP32[(((tempDoublePtr)+(4))>>2)]=HEAP32[(((varargs)+((argIndex)+(4)))>>2)],(+(HEAPF64[(tempDoublePtr)>>3])));
          argIndex += 8;
        } else if (type == 'i64') {
          ret = [HEAP32[(((varargs)+(argIndex))>>2)],
                 HEAP32[(((varargs)+(argIndex+4))>>2)]];
  
          argIndex += 8;
        } else {
          assert((argIndex & 3) === 0);
          type = 'i32'; // varargs are always i32, i64, or double
          ret = HEAP32[(((varargs)+(argIndex))>>2)];
          argIndex += 4;
        }
        return ret;
      }
  
      var ret = [];
      var curr, next, currArg;
      while(1) {
        var startTextIndex = textIndex;
        curr = HEAP8[((textIndex)>>0)];
        if (curr === 0) break;
        next = HEAP8[((textIndex+1)>>0)];
        if (curr == 37) {
          // Handle flags.
          var flagAlwaysSigned = false;
          var flagLeftAlign = false;
          var flagAlternative = false;
          var flagZeroPad = false;
          var flagPadSign = false;
          flagsLoop: while (1) {
            switch (next) {
              case 43:
                flagAlwaysSigned = true;
                break;
              case 45:
                flagLeftAlign = true;
                break;
              case 35:
                flagAlternative = true;
                break;
              case 48:
                if (flagZeroPad) {
                  break flagsLoop;
                } else {
                  flagZeroPad = true;
                  break;
                }
              case 32:
                flagPadSign = true;
                break;
              default:
                break flagsLoop;
            }
            textIndex++;
            next = HEAP8[((textIndex+1)>>0)];
          }
  
          // Handle width.
          var width = 0;
          if (next == 42) {
            width = getNextArg('i32');
            textIndex++;
            next = HEAP8[((textIndex+1)>>0)];
          } else {
            while (next >= 48 && next <= 57) {
              width = width * 10 + (next - 48);
              textIndex++;
              next = HEAP8[((textIndex+1)>>0)];
            }
          }
  
          // Handle precision.
          var precisionSet = false, precision = -1;
          if (next == 46) {
            precision = 0;
            precisionSet = true;
            textIndex++;
            next = HEAP8[((textIndex+1)>>0)];
            if (next == 42) {
              precision = getNextArg('i32');
              textIndex++;
            } else {
              while(1) {
                var precisionChr = HEAP8[((textIndex+1)>>0)];
                if (precisionChr < 48 ||
                    precisionChr > 57) break;
                precision = precision * 10 + (precisionChr - 48);
                textIndex++;
              }
            }
            next = HEAP8[((textIndex+1)>>0)];
          }
          if (precision < 0) {
            precision = 6; // Standard default.
            precisionSet = false;
          }
  
          // Handle integer sizes. WARNING: These assume a 32-bit architecture!
          var argSize;
          switch (String.fromCharCode(next)) {
            case 'h':
              var nextNext = HEAP8[((textIndex+2)>>0)];
              if (nextNext == 104) {
                textIndex++;
                argSize = 1; // char (actually i32 in varargs)
              } else {
                argSize = 2; // short (actually i32 in varargs)
              }
              break;
            case 'l':
              var nextNext = HEAP8[((textIndex+2)>>0)];
              if (nextNext == 108) {
                textIndex++;
                argSize = 8; // long long
              } else {
                argSize = 4; // long
              }
              break;
            case 'L': // long long
            case 'q': // int64_t
            case 'j': // intmax_t
              argSize = 8;
              break;
            case 'z': // size_t
            case 't': // ptrdiff_t
            case 'I': // signed ptrdiff_t or unsigned size_t
              argSize = 4;
              break;
            default:
              argSize = null;
          }
          if (argSize) textIndex++;
          next = HEAP8[((textIndex+1)>>0)];
  
          // Handle type specifier.
          switch (String.fromCharCode(next)) {
            case 'd': case 'i': case 'u': case 'o': case 'x': case 'X': case 'p': {
              // Integer.
              var signed = next == 100 || next == 105;
              argSize = argSize || 4;
              var currArg = getNextArg('i' + (argSize * 8));
              var origArg = currArg;
              var argText;
              // Flatten i64-1 [low, high] into a (slightly rounded) double
              if (argSize == 8) {
                currArg = Runtime.makeBigInt(currArg[0], currArg[1], next == 117);
              }
              // Truncate to requested size.
              if (argSize <= 4) {
                var limit = Math.pow(256, argSize) - 1;
                currArg = (signed ? reSign : unSign)(currArg & limit, argSize * 8);
              }
              // Format the number.
              var currAbsArg = Math.abs(currArg);
              var prefix = '';
              if (next == 100 || next == 105) {
                if (argSize == 8 && i64Math) argText = i64Math.stringify(origArg[0], origArg[1], null); else
                argText = reSign(currArg, 8 * argSize, 1).toString(10);
              } else if (next == 117) {
                if (argSize == 8 && i64Math) argText = i64Math.stringify(origArg[0], origArg[1], true); else
                argText = unSign(currArg, 8 * argSize, 1).toString(10);
                currArg = Math.abs(currArg);
              } else if (next == 111) {
                argText = (flagAlternative ? '0' : '') + currAbsArg.toString(8);
              } else if (next == 120 || next == 88) {
                prefix = (flagAlternative && currArg != 0) ? '0x' : '';
                if (argSize == 8 && i64Math) {
                  if (origArg[1]) {
                    argText = (origArg[1]>>>0).toString(16);
                    var lower = (origArg[0]>>>0).toString(16);
                    while (lower.length < 8) lower = '0' + lower;
                    argText += lower;
                  } else {
                    argText = (origArg[0]>>>0).toString(16);
                  }
                } else
                if (currArg < 0) {
                  // Represent negative numbers in hex as 2's complement.
                  currArg = -currArg;
                  argText = (currAbsArg - 1).toString(16);
                  var buffer = [];
                  for (var i = 0; i < argText.length; i++) {
                    buffer.push((0xF - parseInt(argText[i], 16)).toString(16));
                  }
                  argText = buffer.join('');
                  while (argText.length < argSize * 2) argText = 'f' + argText;
                } else {
                  argText = currAbsArg.toString(16);
                }
                if (next == 88) {
                  prefix = prefix.toUpperCase();
                  argText = argText.toUpperCase();
                }
              } else if (next == 112) {
                if (currAbsArg === 0) {
                  argText = '(nil)';
                } else {
                  prefix = '0x';
                  argText = currAbsArg.toString(16);
                }
              }
              if (precisionSet) {
                while (argText.length < precision) {
                  argText = '0' + argText;
                }
              }
  
              // Add sign if needed
              if (currArg >= 0) {
                if (flagAlwaysSigned) {
                  prefix = '+' + prefix;
                } else if (flagPadSign) {
                  prefix = ' ' + prefix;
                }
              }
  
              // Move sign to prefix so we zero-pad after the sign
              if (argText.charAt(0) == '-') {
                prefix = '-' + prefix;
                argText = argText.substr(1);
              }
  
              // Add padding.
              while (prefix.length + argText.length < width) {
                if (flagLeftAlign) {
                  argText += ' ';
                } else {
                  if (flagZeroPad) {
                    argText = '0' + argText;
                  } else {
                    prefix = ' ' + prefix;
                  }
                }
              }
  
              // Insert the result into the buffer.
              argText = prefix + argText;
              argText.split('').forEach(function(chr) {
                ret.push(chr.charCodeAt(0));
              });
              break;
            }
            case 'f': case 'F': case 'e': case 'E': case 'g': case 'G': {
              // Float.
              var currArg = getNextArg('double');
              var argText;
              if (isNaN(currArg)) {
                argText = 'nan';
                flagZeroPad = false;
              } else if (!isFinite(currArg)) {
                argText = (currArg < 0 ? '-' : '') + 'inf';
                flagZeroPad = false;
              } else {
                var isGeneral = false;
                var effectivePrecision = Math.min(precision, 20);
  
                // Convert g/G to f/F or e/E, as per:
                // http://pubs.opengroup.org/onlinepubs/9699919799/functions/printf.html
                if (next == 103 || next == 71) {
                  isGeneral = true;
                  precision = precision || 1;
                  var exponent = parseInt(currArg.toExponential(effectivePrecision).split('e')[1], 10);
                  if (precision > exponent && exponent >= -4) {
                    next = ((next == 103) ? 'f' : 'F').charCodeAt(0);
                    precision -= exponent + 1;
                  } else {
                    next = ((next == 103) ? 'e' : 'E').charCodeAt(0);
                    precision--;
                  }
                  effectivePrecision = Math.min(precision, 20);
                }
  
                if (next == 101 || next == 69) {
                  argText = currArg.toExponential(effectivePrecision);
                  // Make sure the exponent has at least 2 digits.
                  if (/[eE][-+]\d$/.test(argText)) {
                    argText = argText.slice(0, -1) + '0' + argText.slice(-1);
                  }
                } else if (next == 102 || next == 70) {
                  argText = currArg.toFixed(effectivePrecision);
                  if (currArg === 0 && __reallyNegative(currArg)) {
                    argText = '-' + argText;
                  }
                }
  
                var parts = argText.split('e');
                if (isGeneral && !flagAlternative) {
                  // Discard trailing zeros and periods.
                  while (parts[0].length > 1 && parts[0].indexOf('.') != -1 &&
                         (parts[0].slice(-1) == '0' || parts[0].slice(-1) == '.')) {
                    parts[0] = parts[0].slice(0, -1);
                  }
                } else {
                  // Make sure we have a period in alternative mode.
                  if (flagAlternative && argText.indexOf('.') == -1) parts[0] += '.';
                  // Zero pad until required precision.
                  while (precision > effectivePrecision++) parts[0] += '0';
                }
                argText = parts[0] + (parts.length > 1 ? 'e' + parts[1] : '');
  
                // Capitalize 'E' if needed.
                if (next == 69) argText = argText.toUpperCase();
  
                // Add sign.
                if (currArg >= 0) {
                  if (flagAlwaysSigned) {
                    argText = '+' + argText;
                  } else if (flagPadSign) {
                    argText = ' ' + argText;
                  }
                }
              }
  
              // Add padding.
              while (argText.length < width) {
                if (flagLeftAlign) {
                  argText += ' ';
                } else {
                  if (flagZeroPad && (argText[0] == '-' || argText[0] == '+')) {
                    argText = argText[0] + '0' + argText.slice(1);
                  } else {
                    argText = (flagZeroPad ? '0' : ' ') + argText;
                  }
                }
              }
  
              // Adjust case.
              if (next < 97) argText = argText.toUpperCase();
  
              // Insert the result into the buffer.
              argText.split('').forEach(function(chr) {
                ret.push(chr.charCodeAt(0));
              });
              break;
            }
            case 's': {
              // String.
              var arg = getNextArg('i8*');
              var argLength = arg ? _strlen(arg) : '(null)'.length;
              if (precisionSet) argLength = Math.min(argLength, precision);
              if (!flagLeftAlign) {
                while (argLength < width--) {
                  ret.push(32);
                }
              }
              if (arg) {
                for (var i = 0; i < argLength; i++) {
                  ret.push(HEAPU8[((arg++)>>0)]);
                }
              } else {
                ret = ret.concat(intArrayFromString('(null)'.substr(0, argLength), true));
              }
              if (flagLeftAlign) {
                while (argLength < width--) {
                  ret.push(32);
                }
              }
              break;
            }
            case 'c': {
              // Character.
              if (flagLeftAlign) ret.push(getNextArg('i8'));
              while (--width > 0) {
                ret.push(32);
              }
              if (!flagLeftAlign) ret.push(getNextArg('i8'));
              break;
            }
            case 'n': {
              // Write the length written so far to the next parameter.
              var ptr = getNextArg('i32*');
              HEAP32[((ptr)>>2)]=ret.length;
              break;
            }
            case '%': {
              // Literal percent sign.
              ret.push(curr);
              break;
            }
            default: {
              // Unknown specifiers remain untouched.
              for (var i = startTextIndex; i < textIndex + 2; i++) {
                ret.push(HEAP8[((i)>>0)]);
              }
            }
          }
          textIndex += 2;
          // TODO: Support a/A (hex float) and m (last error) specifiers.
          // TODO: Support %1${specifier} for arg selection.
        } else {
          ret.push(curr);
          textIndex += 1;
        }
      }
      return ret;
    }function _fprintf(stream, format, varargs) {
      // int fprintf(FILE *restrict stream, const char *restrict format, ...);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/printf.html
      var result = __formatString(format, varargs);
      var stack = Runtime.stackSave();
      var ret = _fwrite(allocate(result, 'i8', ALLOC_STACK), 1, result.length, stream);
      Runtime.stackRestore(stack);
      return ret;
    }function _printf(format, varargs) {
      // int printf(const char *restrict format, ...);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/printf.html
      var stdout = HEAP32[((_stdout)>>2)];
      return _fprintf(stdout, format, varargs);
    }


  
  function _emscripten_memcpy_big(dest, src, num) {
      HEAPU8.set(HEAPU8.subarray(src, src+num), dest);
      return dest;
    } 
  Module["_memcpy"] = _memcpy;

  var _cos=Math_cos;

  var _log=Math_log;

  
  function _fputs(s, stream) {
      // int fputs(const char *restrict s, FILE *restrict stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fputs.html
      var fd = _fileno(stream);
      return _write(fd, s, _strlen(s));
    }
  
  function _fputc(c, stream) {
      // int fputc(int c, FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fputc.html
      var chr = unSign(c & 0xFF);
      HEAP8[((_fputc.ret)>>0)]=chr;
      var fd = _fileno(stream);
      var ret = _write(fd, _fputc.ret, 1);
      if (ret == -1) {
        var streamObj = FS.getStreamFromPtr(stream);
        if (streamObj) streamObj.error = true;
        return -1;
      } else {
        return chr;
      }
    }function _puts(s) {
      // int puts(const char *s);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/puts.html
      // NOTE: puts() always writes an extra newline.
      var stdout = HEAP32[((_stdout)>>2)];
      var ret = _fputs(s, stdout);
      if (ret < 0) {
        return ret;
      } else {
        var newlineRet = _fputc(10, stdout);
        return (newlineRet < 0) ? -1 : ret + 1;
      }
    }

  function ___errno_location() {
      return ___errno_state;
    }

  var _BItoD=true;

   
  Module["_strcpy"] = _strcpy;

  
  function __exit(status) {
      // void _exit(int status);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/exit.html
      Module['exit'](status);
    }function _exit(status) {
      __exit(status);
    }

  function _sbrk(bytes) {
      // Implement a Linux-like 'memory area' for our 'process'.
      // Changes the size of the memory area by |bytes|; returns the
      // address of the previous top ('break') of the memory area
      // We control the "dynamic" memory - DYNAMIC_BASE to DYNAMICTOP
      var self = _sbrk;
      if (!self.called) {
        DYNAMICTOP = alignMemoryPage(DYNAMICTOP); // make sure we start out aligned
        self.called = true;
        assert(Runtime.dynamicAlloc);
        self.alloc = Runtime.dynamicAlloc;
        Runtime.dynamicAlloc = function() { abort('cannot dynamically allocate, sbrk now has control') };
      }
      var ret = DYNAMICTOP;
      if (bytes != 0) {
        var success = self.alloc(bytes);
        if (!success) return -1 >>> 0; // sbrk failure code
      }
      return ret;  // Previous break location.
    }

  function _time(ptr) {
      var ret = (Date.now()/1000)|0;
      if (ptr) {
        HEAP32[((ptr)>>2)]=ret;
      }
      return ret;
    }

  
  
  function _emscripten_set_main_loop_timing(mode, value) {
      Browser.mainLoop.timingMode = mode;
      Browser.mainLoop.timingValue = value;
  
      if (!Browser.mainLoop.func) {
        return 1; // Return non-zero on failure, can't set timing mode when there is no main loop.
      }
  
      if (mode == 0 /*EM_TIMING_SETTIMEOUT*/) {
        Browser.mainLoop.scheduler = function Browser_mainLoop_scheduler() {
          setTimeout(Browser.mainLoop.runner, value); // doing this each time means that on exception, we stop
        };
        Browser.mainLoop.method = 'timeout';
      } else if (mode == 1 /*EM_TIMING_RAF*/) {
        Browser.mainLoop.scheduler = function Browser_mainLoop_scheduler() {
          Browser.requestAnimationFrame(Browser.mainLoop.runner);
        };
        Browser.mainLoop.method = 'rAF';
      }
      return 0;
    }function _emscripten_set_main_loop(func, fps, simulateInfiniteLoop, arg, noSetTiming) {
      Module['noExitRuntime'] = true;
  
      assert(!Browser.mainLoop.func, 'emscripten_set_main_loop: there can only be one main loop function at once: call emscripten_cancel_main_loop to cancel the previous one before setting a new one with different parameters.');
  
      Browser.mainLoop.func = func;
      Browser.mainLoop.arg = arg;
  
      var thisMainLoopId = Browser.mainLoop.currentlyRunningMainloop;
  
      Browser.mainLoop.runner = function Browser_mainLoop_runner() {
        if (ABORT) return;
        if (Browser.mainLoop.queue.length > 0) {
          var start = Date.now();
          var blocker = Browser.mainLoop.queue.shift();
          blocker.func(blocker.arg);
          if (Browser.mainLoop.remainingBlockers) {
            var remaining = Browser.mainLoop.remainingBlockers;
            var next = remaining%1 == 0 ? remaining-1 : Math.floor(remaining);
            if (blocker.counted) {
              Browser.mainLoop.remainingBlockers = next;
            } else {
              // not counted, but move the progress along a tiny bit
              next = next + 0.5; // do not steal all the next one's progress
              Browser.mainLoop.remainingBlockers = (8*remaining + next)/9;
            }
          }
          console.log('main loop blocker "' + blocker.name + '" took ' + (Date.now() - start) + ' ms'); //, left: ' + Browser.mainLoop.remainingBlockers);
          Browser.mainLoop.updateStatus();
          setTimeout(Browser.mainLoop.runner, 0);
          return;
        }
  
        // catch pauses from non-main loop sources
        if (thisMainLoopId < Browser.mainLoop.currentlyRunningMainloop) return;
  
        // Implement very basic swap interval control
        Browser.mainLoop.currentFrameNumber = Browser.mainLoop.currentFrameNumber + 1 | 0;
        if (Browser.mainLoop.timingMode == 1/*EM_TIMING_RAF*/ && Browser.mainLoop.timingValue > 1 && Browser.mainLoop.currentFrameNumber % Browser.mainLoop.timingValue != 0) {
          // Not the scheduled time to render this frame - skip.
          Browser.mainLoop.scheduler();
          return;
        }
  
        // Signal GL rendering layer that processing of a new frame is about to start. This helps it optimize
        // VBO double-buffering and reduce GPU stalls.
  
        if (Browser.mainLoop.method === 'timeout' && Module.ctx) {
          Module.printErr('Looks like you are rendering without using requestAnimationFrame for the main loop. You should use 0 for the frame rate in emscripten_set_main_loop in order to use requestAnimationFrame, as that can greatly improve your frame rates!');
          Browser.mainLoop.method = ''; // just warn once per call to set main loop
        }
  
        Browser.mainLoop.runIter(function() {
          if (typeof arg !== 'undefined') {
            Runtime.dynCall('vi', func, [arg]);
          } else {
            Runtime.dynCall('v', func);
          }
        });
  
        // catch pauses from the main loop itself
        if (thisMainLoopId < Browser.mainLoop.currentlyRunningMainloop) return;
  
        // Queue new audio data. This is important to be right after the main loop invocation, so that we will immediately be able
        // to queue the newest produced audio samples.
        // TODO: Consider adding pre- and post- rAF callbacks so that GL.newRenderingFrameStarted() and SDL.audio.queueNewAudioData()
        //       do not need to be hardcoded into this function, but can be more generic.
        if (typeof SDL === 'object' && SDL.audio && SDL.audio.queueNewAudioData) SDL.audio.queueNewAudioData();
  
        Browser.mainLoop.scheduler();
      }
  
      if (!noSetTiming) {
        if (fps && fps > 0) _emscripten_set_main_loop_timing(0/*EM_TIMING_SETTIMEOUT*/, 1000.0 / fps);
        else _emscripten_set_main_loop_timing(1/*EM_TIMING_RAF*/, 1); // Do rAF by rendering each frame (no decimating)
  
        Browser.mainLoop.scheduler();
      }
  
      if (simulateInfiniteLoop) {
        throw 'SimulateInfiniteLoop';
      }
    }var Browser={mainLoop:{scheduler:null,method:"",currentlyRunningMainloop:0,func:null,arg:0,timingMode:0,timingValue:0,currentFrameNumber:0,queue:[],pause:function () {
          Browser.mainLoop.scheduler = null;
          Browser.mainLoop.currentlyRunningMainloop++; // Incrementing this signals the previous main loop that it's now become old, and it must return.
        },resume:function () {
          Browser.mainLoop.currentlyRunningMainloop++;
          var timingMode = Browser.mainLoop.timingMode;
          var timingValue = Browser.mainLoop.timingValue;
          var func = Browser.mainLoop.func;
          Browser.mainLoop.func = null;
          _emscripten_set_main_loop(func, 0, false, Browser.mainLoop.arg, true /* do not set timing and call scheduler, we will do it on the next lines */);
          _emscripten_set_main_loop_timing(timingMode, timingValue);
          Browser.mainLoop.scheduler();
        },updateStatus:function () {
          if (Module['setStatus']) {
            var message = Module['statusMessage'] || 'Please wait...';
            var remaining = Browser.mainLoop.remainingBlockers;
            var expected = Browser.mainLoop.expectedBlockers;
            if (remaining) {
              if (remaining < expected) {
                Module['setStatus'](message + ' (' + (expected - remaining) + '/' + expected + ')');
              } else {
                Module['setStatus'](message);
              }
            } else {
              Module['setStatus']('');
            }
          }
        },runIter:function (func) {
          if (ABORT) return;
          if (Module['preMainLoop']) {
            var preRet = Module['preMainLoop']();
            if (preRet === false) {
              return; // |return false| skips a frame
            }
          }
          try {
            func();
          } catch (e) {
            if (e instanceof ExitStatus) {
              return;
            } else {
              if (e && typeof e === 'object' && e.stack) Module.printErr('exception thrown: ' + [e, e.stack]);
              throw e;
            }
          }
          if (Module['postMainLoop']) Module['postMainLoop']();
        }},isFullScreen:false,pointerLock:false,moduleContextCreatedCallbacks:[],workers:[],init:function () {
        if (!Module["preloadPlugins"]) Module["preloadPlugins"] = []; // needs to exist even in workers
  
        if (Browser.initted) return;
        Browser.initted = true;
  
        try {
          new Blob();
          Browser.hasBlobConstructor = true;
        } catch(e) {
          Browser.hasBlobConstructor = false;
          console.log("warning: no blob constructor, cannot create blobs with mimetypes");
        }
        Browser.BlobBuilder = typeof MozBlobBuilder != "undefined" ? MozBlobBuilder : (typeof WebKitBlobBuilder != "undefined" ? WebKitBlobBuilder : (!Browser.hasBlobConstructor ? console.log("warning: no BlobBuilder") : null));
        Browser.URLObject = typeof window != "undefined" ? (window.URL ? window.URL : window.webkitURL) : undefined;
        if (!Module.noImageDecoding && typeof Browser.URLObject === 'undefined') {
          console.log("warning: Browser does not support creating object URLs. Built-in browser image decoding will not be available.");
          Module.noImageDecoding = true;
        }
  
        // Support for plugins that can process preloaded files. You can add more of these to
        // your app by creating and appending to Module.preloadPlugins.
        //
        // Each plugin is asked if it can handle a file based on the file's name. If it can,
        // it is given the file's raw data. When it is done, it calls a callback with the file's
        // (possibly modified) data. For example, a plugin might decompress a file, or it
        // might create some side data structure for use later (like an Image element, etc.).
  
        var imagePlugin = {};
        imagePlugin['canHandle'] = function imagePlugin_canHandle(name) {
          return !Module.noImageDecoding && /\.(jpg|jpeg|png|bmp)$/i.test(name);
        };
        imagePlugin['handle'] = function imagePlugin_handle(byteArray, name, onload, onerror) {
          var b = null;
          if (Browser.hasBlobConstructor) {
            try {
              b = new Blob([byteArray], { type: Browser.getMimetype(name) });
              if (b.size !== byteArray.length) { // Safari bug #118630
                // Safari's Blob can only take an ArrayBuffer
                b = new Blob([(new Uint8Array(byteArray)).buffer], { type: Browser.getMimetype(name) });
              }
            } catch(e) {
              Runtime.warnOnce('Blob constructor present but fails: ' + e + '; falling back to blob builder');
            }
          }
          if (!b) {
            var bb = new Browser.BlobBuilder();
            bb.append((new Uint8Array(byteArray)).buffer); // we need to pass a buffer, and must copy the array to get the right data range
            b = bb.getBlob();
          }
          var url = Browser.URLObject.createObjectURL(b);
          var img = new Image();
          img.onload = function img_onload() {
            assert(img.complete, 'Image ' + name + ' could not be decoded');
            var canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0);
            Module["preloadedImages"][name] = canvas;
            Browser.URLObject.revokeObjectURL(url);
            if (onload) onload(byteArray);
          };
          img.onerror = function img_onerror(event) {
            console.log('Image ' + url + ' could not be decoded');
            if (onerror) onerror();
          };
          img.src = url;
        };
        Module['preloadPlugins'].push(imagePlugin);
  
        var audioPlugin = {};
        audioPlugin['canHandle'] = function audioPlugin_canHandle(name) {
          return !Module.noAudioDecoding && name.substr(-4) in { '.ogg': 1, '.wav': 1, '.mp3': 1 };
        };
        audioPlugin['handle'] = function audioPlugin_handle(byteArray, name, onload, onerror) {
          var done = false;
          function finish(audio) {
            if (done) return;
            done = true;
            Module["preloadedAudios"][name] = audio;
            if (onload) onload(byteArray);
          }
          function fail() {
            if (done) return;
            done = true;
            Module["preloadedAudios"][name] = new Audio(); // empty shim
            if (onerror) onerror();
          }
          if (Browser.hasBlobConstructor) {
            try {
              var b = new Blob([byteArray], { type: Browser.getMimetype(name) });
            } catch(e) {
              return fail();
            }
            var url = Browser.URLObject.createObjectURL(b); // XXX we never revoke this!
            var audio = new Audio();
            audio.addEventListener('canplaythrough', function() { finish(audio) }, false); // use addEventListener due to chromium bug 124926
            audio.onerror = function audio_onerror(event) {
              if (done) return;
              console.log('warning: browser could not fully decode audio ' + name + ', trying slower base64 approach');
              function encode64(data) {
                var BASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
                var PAD = '=';
                var ret = '';
                var leftchar = 0;
                var leftbits = 0;
                for (var i = 0; i < data.length; i++) {
                  leftchar = (leftchar << 8) | data[i];
                  leftbits += 8;
                  while (leftbits >= 6) {
                    var curr = (leftchar >> (leftbits-6)) & 0x3f;
                    leftbits -= 6;
                    ret += BASE[curr];
                  }
                }
                if (leftbits == 2) {
                  ret += BASE[(leftchar&3) << 4];
                  ret += PAD + PAD;
                } else if (leftbits == 4) {
                  ret += BASE[(leftchar&0xf) << 2];
                  ret += PAD;
                }
                return ret;
              }
              audio.src = 'data:audio/x-' + name.substr(-3) + ';base64,' + encode64(byteArray);
              finish(audio); // we don't wait for confirmation this worked - but it's worth trying
            };
            audio.src = url;
            // workaround for chrome bug 124926 - we do not always get oncanplaythrough or onerror
            Browser.safeSetTimeout(function() {
              finish(audio); // try to use it even though it is not necessarily ready to play
            }, 10000);
          } else {
            return fail();
          }
        };
        Module['preloadPlugins'].push(audioPlugin);
  
        // Canvas event setup
  
        var canvas = Module['canvas'];
        function pointerLockChange() {
          Browser.pointerLock = document['pointerLockElement'] === canvas ||
                                document['mozPointerLockElement'] === canvas ||
                                document['webkitPointerLockElement'] === canvas ||
                                document['msPointerLockElement'] === canvas;
        }
        if (canvas) {
          // forced aspect ratio can be enabled by defining 'forcedAspectRatio' on Module
          // Module['forcedAspectRatio'] = 4 / 3;
          
          canvas.requestPointerLock = canvas['requestPointerLock'] ||
                                      canvas['mozRequestPointerLock'] ||
                                      canvas['webkitRequestPointerLock'] ||
                                      canvas['msRequestPointerLock'] ||
                                      function(){};
          canvas.exitPointerLock = document['exitPointerLock'] ||
                                   document['mozExitPointerLock'] ||
                                   document['webkitExitPointerLock'] ||
                                   document['msExitPointerLock'] ||
                                   function(){}; // no-op if function does not exist
          canvas.exitPointerLock = canvas.exitPointerLock.bind(document);
  
  
          document.addEventListener('pointerlockchange', pointerLockChange, false);
          document.addEventListener('mozpointerlockchange', pointerLockChange, false);
          document.addEventListener('webkitpointerlockchange', pointerLockChange, false);
          document.addEventListener('mspointerlockchange', pointerLockChange, false);
  
          if (Module['elementPointerLock']) {
            canvas.addEventListener("click", function(ev) {
              if (!Browser.pointerLock && canvas.requestPointerLock) {
                canvas.requestPointerLock();
                ev.preventDefault();
              }
            }, false);
          }
        }
      },createContext:function (canvas, useWebGL, setInModule, webGLContextAttributes) {
        if (useWebGL && Module.ctx && canvas == Module.canvas) return Module.ctx; // no need to recreate GL context if it's already been created for this canvas.
  
        var ctx;
        var contextHandle;
        if (useWebGL) {
          // For GLES2/desktop GL compatibility, adjust a few defaults to be different to WebGL defaults, so that they align better with the desktop defaults.
          var contextAttributes = {
            antialias: false,
            alpha: false
          };
  
          if (webGLContextAttributes) {
            for (var attribute in webGLContextAttributes) {
              contextAttributes[attribute] = webGLContextAttributes[attribute];
            }
          }
  
          contextHandle = GL.createContext(canvas, contextAttributes);
          if (contextHandle) {
            ctx = GL.getContext(contextHandle).GLctx;
          }
          // Set the background of the WebGL canvas to black
          canvas.style.backgroundColor = "black";
        } else {
          ctx = canvas.getContext('2d');
        }
  
        if (!ctx) return null;
  
        if (setInModule) {
          if (!useWebGL) assert(typeof GLctx === 'undefined', 'cannot set in module if GLctx is used, but we are a non-GL context that would replace it');
  
          Module.ctx = ctx;
          if (useWebGL) GL.makeContextCurrent(contextHandle);
          Module.useWebGL = useWebGL;
          Browser.moduleContextCreatedCallbacks.forEach(function(callback) { callback() });
          Browser.init();
        }
        return ctx;
      },destroyContext:function (canvas, useWebGL, setInModule) {},fullScreenHandlersInstalled:false,lockPointer:undefined,resizeCanvas:undefined,requestFullScreen:function (lockPointer, resizeCanvas, vrDevice) {
        Browser.lockPointer = lockPointer;
        Browser.resizeCanvas = resizeCanvas;
        Browser.vrDevice = vrDevice;
        if (typeof Browser.lockPointer === 'undefined') Browser.lockPointer = true;
        if (typeof Browser.resizeCanvas === 'undefined') Browser.resizeCanvas = false;
        if (typeof Browser.vrDevice === 'undefined') Browser.vrDevice = null;
  
        var canvas = Module['canvas'];
        function fullScreenChange() {
          Browser.isFullScreen = false;
          var canvasContainer = canvas.parentNode;
          if ((document['webkitFullScreenElement'] || document['webkitFullscreenElement'] ||
               document['mozFullScreenElement'] || document['mozFullscreenElement'] ||
               document['fullScreenElement'] || document['fullscreenElement'] ||
               document['msFullScreenElement'] || document['msFullscreenElement'] ||
               document['webkitCurrentFullScreenElement']) === canvasContainer) {
            canvas.cancelFullScreen = document['cancelFullScreen'] ||
                                      document['mozCancelFullScreen'] ||
                                      document['webkitCancelFullScreen'] ||
                                      document['msExitFullscreen'] ||
                                      document['exitFullscreen'] ||
                                      function() {};
            canvas.cancelFullScreen = canvas.cancelFullScreen.bind(document);
            if (Browser.lockPointer) canvas.requestPointerLock();
            Browser.isFullScreen = true;
            if (Browser.resizeCanvas) Browser.setFullScreenCanvasSize();
          } else {
            
            // remove the full screen specific parent of the canvas again to restore the HTML structure from before going full screen
            canvasContainer.parentNode.insertBefore(canvas, canvasContainer);
            canvasContainer.parentNode.removeChild(canvasContainer);
            
            if (Browser.resizeCanvas) Browser.setWindowedCanvasSize();
          }
          if (Module['onFullScreen']) Module['onFullScreen'](Browser.isFullScreen);
          Browser.updateCanvasDimensions(canvas);
        }
  
        if (!Browser.fullScreenHandlersInstalled) {
          Browser.fullScreenHandlersInstalled = true;
          document.addEventListener('fullscreenchange', fullScreenChange, false);
          document.addEventListener('mozfullscreenchange', fullScreenChange, false);
          document.addEventListener('webkitfullscreenchange', fullScreenChange, false);
          document.addEventListener('MSFullscreenChange', fullScreenChange, false);
        }
  
        // create a new parent to ensure the canvas has no siblings. this allows browsers to optimize full screen performance when its parent is the full screen root
        var canvasContainer = document.createElement("div");
        canvas.parentNode.insertBefore(canvasContainer, canvas);
        canvasContainer.appendChild(canvas);
  
        // use parent of canvas as full screen root to allow aspect ratio correction (Firefox stretches the root to screen size)
        canvasContainer.requestFullScreen = canvasContainer['requestFullScreen'] ||
                                            canvasContainer['mozRequestFullScreen'] ||
                                            canvasContainer['msRequestFullscreen'] ||
                                           (canvasContainer['webkitRequestFullScreen'] ? function() { canvasContainer['webkitRequestFullScreen'](Element['ALLOW_KEYBOARD_INPUT']) } : null);
  
        if (vrDevice) {
          canvasContainer.requestFullScreen({ vrDisplay: vrDevice });
        } else {
          canvasContainer.requestFullScreen();
        }
      },nextRAF:0,fakeRequestAnimationFrame:function (func) {
        // try to keep 60fps between calls to here
        var now = Date.now();
        if (Browser.nextRAF === 0) {
          Browser.nextRAF = now + 1000/60;
        } else {
          while (now + 2 >= Browser.nextRAF) { // fudge a little, to avoid timer jitter causing us to do lots of delay:0
            Browser.nextRAF += 1000/60;
          }
        }
        var delay = Math.max(Browser.nextRAF - now, 0);
        setTimeout(func, delay);
      },requestAnimationFrame:function requestAnimationFrame(func) {
        if (typeof window === 'undefined') { // Provide fallback to setTimeout if window is undefined (e.g. in Node.js)
          Browser.fakeRequestAnimationFrame(func);
        } else {
          if (!window.requestAnimationFrame) {
            window.requestAnimationFrame = window['requestAnimationFrame'] ||
                                           window['mozRequestAnimationFrame'] ||
                                           window['webkitRequestAnimationFrame'] ||
                                           window['msRequestAnimationFrame'] ||
                                           window['oRequestAnimationFrame'] ||
                                           Browser.fakeRequestAnimationFrame;
          }
          window.requestAnimationFrame(func);
        }
      },safeCallback:function (func) {
        return function() {
          if (!ABORT) return func.apply(null, arguments);
        };
      },allowAsyncCallbacks:true,queuedAsyncCallbacks:[],pauseAsyncCallbacks:function () {
        Browser.allowAsyncCallbacks = false;
      },resumeAsyncCallbacks:function () { // marks future callbacks as ok to execute, and synchronously runs any remaining ones right now
        Browser.allowAsyncCallbacks = true;
        if (Browser.queuedAsyncCallbacks.length > 0) {
          var callbacks = Browser.queuedAsyncCallbacks;
          Browser.queuedAsyncCallbacks = [];
          callbacks.forEach(function(func) {
            func();
          });
        }
      },safeRequestAnimationFrame:function (func) {
        return Browser.requestAnimationFrame(function() {
          if (ABORT) return;
          if (Browser.allowAsyncCallbacks) {
            func();
          } else {
            Browser.queuedAsyncCallbacks.push(func);
          }
        });
      },safeSetTimeout:function (func, timeout) {
        Module['noExitRuntime'] = true;
        return setTimeout(function() {
          if (ABORT) return;
          if (Browser.allowAsyncCallbacks) {
            func();
          } else {
            Browser.queuedAsyncCallbacks.push(func);
          }
        }, timeout);
      },safeSetInterval:function (func, timeout) {
        Module['noExitRuntime'] = true;
        return setInterval(function() {
          if (ABORT) return;
          if (Browser.allowAsyncCallbacks) {
            func();
          } // drop it on the floor otherwise, next interval will kick in
        }, timeout);
      },getMimetype:function (name) {
        return {
          'jpg': 'image/jpeg',
          'jpeg': 'image/jpeg',
          'png': 'image/png',
          'bmp': 'image/bmp',
          'ogg': 'audio/ogg',
          'wav': 'audio/wav',
          'mp3': 'audio/mpeg'
        }[name.substr(name.lastIndexOf('.')+1)];
      },getUserMedia:function (func) {
        if(!window.getUserMedia) {
          window.getUserMedia = navigator['getUserMedia'] ||
                                navigator['mozGetUserMedia'];
        }
        window.getUserMedia(func);
      },getMovementX:function (event) {
        return event['movementX'] ||
               event['mozMovementX'] ||
               event['webkitMovementX'] ||
               0;
      },getMovementY:function (event) {
        return event['movementY'] ||
               event['mozMovementY'] ||
               event['webkitMovementY'] ||
               0;
      },getMouseWheelDelta:function (event) {
        var delta = 0;
        switch (event.type) {
          case 'DOMMouseScroll': 
            delta = event.detail;
            break;
          case 'mousewheel': 
            delta = event.wheelDelta;
            break;
          case 'wheel': 
            delta = event['deltaY'];
            break;
          default:
            throw 'unrecognized mouse wheel event: ' + event.type;
        }
        return delta;
      },mouseX:0,mouseY:0,mouseMovementX:0,mouseMovementY:0,touches:{},lastTouches:{},calculateMouseEvent:function (event) { // event should be mousemove, mousedown or mouseup
        if (Browser.pointerLock) {
          // When the pointer is locked, calculate the coordinates
          // based on the movement of the mouse.
          // Workaround for Firefox bug 764498
          if (event.type != 'mousemove' &&
              ('mozMovementX' in event)) {
            Browser.mouseMovementX = Browser.mouseMovementY = 0;
          } else {
            Browser.mouseMovementX = Browser.getMovementX(event);
            Browser.mouseMovementY = Browser.getMovementY(event);
          }
          
          // check if SDL is available
          if (typeof SDL != "undefined") {
          	Browser.mouseX = SDL.mouseX + Browser.mouseMovementX;
          	Browser.mouseY = SDL.mouseY + Browser.mouseMovementY;
          } else {
          	// just add the mouse delta to the current absolut mouse position
          	// FIXME: ideally this should be clamped against the canvas size and zero
          	Browser.mouseX += Browser.mouseMovementX;
          	Browser.mouseY += Browser.mouseMovementY;
          }        
        } else {
          // Otherwise, calculate the movement based on the changes
          // in the coordinates.
          var rect = Module["canvas"].getBoundingClientRect();
          var cw = Module["canvas"].width;
          var ch = Module["canvas"].height;
  
          // Neither .scrollX or .pageXOffset are defined in a spec, but
          // we prefer .scrollX because it is currently in a spec draft.
          // (see: http://www.w3.org/TR/2013/WD-cssom-view-20131217/)
          var scrollX = ((typeof window.scrollX !== 'undefined') ? window.scrollX : window.pageXOffset);
          var scrollY = ((typeof window.scrollY !== 'undefined') ? window.scrollY : window.pageYOffset);
  
          if (event.type === 'touchstart' || event.type === 'touchend' || event.type === 'touchmove') {
            var touch = event.touch;
            if (touch === undefined) {
              return; // the "touch" property is only defined in SDL
  
            }
            var adjustedX = touch.pageX - (scrollX + rect.left);
            var adjustedY = touch.pageY - (scrollY + rect.top);
  
            adjustedX = adjustedX * (cw / rect.width);
            adjustedY = adjustedY * (ch / rect.height);
  
            var coords = { x: adjustedX, y: adjustedY };
            
            if (event.type === 'touchstart') {
              Browser.lastTouches[touch.identifier] = coords;
              Browser.touches[touch.identifier] = coords;
            } else if (event.type === 'touchend' || event.type === 'touchmove') {
              var last = Browser.touches[touch.identifier];
              if (!last) last = coords;
              Browser.lastTouches[touch.identifier] = last;
              Browser.touches[touch.identifier] = coords;
            } 
            return;
          }
  
          var x = event.pageX - (scrollX + rect.left);
          var y = event.pageY - (scrollY + rect.top);
  
          // the canvas might be CSS-scaled compared to its backbuffer;
          // SDL-using content will want mouse coordinates in terms
          // of backbuffer units.
          x = x * (cw / rect.width);
          y = y * (ch / rect.height);
  
          Browser.mouseMovementX = x - Browser.mouseX;
          Browser.mouseMovementY = y - Browser.mouseY;
          Browser.mouseX = x;
          Browser.mouseY = y;
        }
      },xhrLoad:function (url, onload, onerror) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function xhr_onload() {
          if (xhr.status == 200 || (xhr.status == 0 && xhr.response)) { // file URLs can return 0
            onload(xhr.response);
          } else {
            onerror();
          }
        };
        xhr.onerror = onerror;
        xhr.send(null);
      },asyncLoad:function (url, onload, onerror, noRunDep) {
        Browser.xhrLoad(url, function(arrayBuffer) {
          assert(arrayBuffer, 'Loading data file "' + url + '" failed (no arrayBuffer).');
          onload(new Uint8Array(arrayBuffer));
          if (!noRunDep) removeRunDependency('al ' + url);
        }, function(event) {
          if (onerror) {
            onerror();
          } else {
            throw 'Loading data file "' + url + '" failed.';
          }
        });
        if (!noRunDep) addRunDependency('al ' + url);
      },resizeListeners:[],updateResizeListeners:function () {
        var canvas = Module['canvas'];
        Browser.resizeListeners.forEach(function(listener) {
          listener(canvas.width, canvas.height);
        });
      },setCanvasSize:function (width, height, noUpdates) {
        var canvas = Module['canvas'];
        Browser.updateCanvasDimensions(canvas, width, height);
        if (!noUpdates) Browser.updateResizeListeners();
      },windowedWidth:0,windowedHeight:0,setFullScreenCanvasSize:function () {
        // check if SDL is available   
        if (typeof SDL != "undefined") {
        	var flags = HEAPU32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)];
        	flags = flags | 0x00800000; // set SDL_FULLSCREEN flag
        	HEAP32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)]=flags
        }
        Browser.updateResizeListeners();
      },setWindowedCanvasSize:function () {
        // check if SDL is available       
        if (typeof SDL != "undefined") {
        	var flags = HEAPU32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)];
        	flags = flags & ~0x00800000; // clear SDL_FULLSCREEN flag
        	HEAP32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)]=flags
        }
        Browser.updateResizeListeners();
      },updateCanvasDimensions:function (canvas, wNative, hNative) {
        if (wNative && hNative) {
          canvas.widthNative = wNative;
          canvas.heightNative = hNative;
        } else {
          wNative = canvas.widthNative;
          hNative = canvas.heightNative;
        }
        var w = wNative;
        var h = hNative;
        if (Module['forcedAspectRatio'] && Module['forcedAspectRatio'] > 0) {
          if (w/h < Module['forcedAspectRatio']) {
            w = Math.round(h * Module['forcedAspectRatio']);
          } else {
            h = Math.round(w / Module['forcedAspectRatio']);
          }
        }
        if (((document['webkitFullScreenElement'] || document['webkitFullscreenElement'] ||
             document['mozFullScreenElement'] || document['mozFullscreenElement'] ||
             document['fullScreenElement'] || document['fullscreenElement'] ||
             document['msFullScreenElement'] || document['msFullscreenElement'] ||
             document['webkitCurrentFullScreenElement']) === canvas.parentNode) && (typeof screen != 'undefined')) {
           var factor = Math.min(screen.width / w, screen.height / h);
           w = Math.round(w * factor);
           h = Math.round(h * factor);
        }
        if (Browser.resizeCanvas) {
          if (canvas.width  != w) canvas.width  = w;
          if (canvas.height != h) canvas.height = h;
          if (typeof canvas.style != 'undefined') {
            canvas.style.removeProperty( "width");
            canvas.style.removeProperty("height");
          }
        } else {
          if (canvas.width  != wNative) canvas.width  = wNative;
          if (canvas.height != hNative) canvas.height = hNative;
          if (typeof canvas.style != 'undefined') {
            if (w != wNative || h != hNative) {
              canvas.style.setProperty( "width", w + "px", "important");
              canvas.style.setProperty("height", h + "px", "important");
            } else {
              canvas.style.removeProperty( "width");
              canvas.style.removeProperty("height");
            }
          }
        }
      },wgetRequests:{},nextWgetRequestHandle:0,getNextWgetRequestHandle:function () {
        var handle = Browser.nextWgetRequestHandle;
        Browser.nextWgetRequestHandle++;
        return handle;
      }};

  var _sin=Math_sin;
___errno_state = Runtime.staticAlloc(4); HEAP32[((___errno_state)>>2)]=0;
FS.staticInit();__ATINIT__.unshift(function() { if (!Module["noFSInit"] && !FS.init.initialized) FS.init() });__ATMAIN__.push(function() { FS.ignorePermissions = false });__ATEXIT__.push(function() { FS.quit() });Module["FS_createFolder"] = FS.createFolder;Module["FS_createPath"] = FS.createPath;Module["FS_createDataFile"] = FS.createDataFile;Module["FS_createPreloadedFile"] = FS.createPreloadedFile;Module["FS_createLazyFile"] = FS.createLazyFile;Module["FS_createLink"] = FS.createLink;Module["FS_createDevice"] = FS.createDevice;
__ATINIT__.unshift(function() { TTY.init() });__ATEXIT__.push(function() { TTY.shutdown() });
if (ENVIRONMENT_IS_NODE) { var fs = require("fs"); var NODEJS_PATH = require("path"); NODEFS.staticInit(); }
__ATINIT__.push(function() { SOCKFS.root = FS.mount(SOCKFS, {}, null); });
_fputc.ret = allocate([0], "i8", ALLOC_STATIC);
Module["requestFullScreen"] = function Module_requestFullScreen(lockPointer, resizeCanvas, vrDevice) { Browser.requestFullScreen(lockPointer, resizeCanvas, vrDevice) };
  Module["requestAnimationFrame"] = function Module_requestAnimationFrame(func) { Browser.requestAnimationFrame(func) };
  Module["setCanvasSize"] = function Module_setCanvasSize(width, height, noUpdates) { Browser.setCanvasSize(width, height, noUpdates) };
  Module["pauseMainLoop"] = function Module_pauseMainLoop() { Browser.mainLoop.pause() };
  Module["resumeMainLoop"] = function Module_resumeMainLoop() { Browser.mainLoop.resume() };
  Module["getUserMedia"] = function Module_getUserMedia() { Browser.getUserMedia() }
  Module["createContext"] = function Module_createContext(canvas, useWebGL, setInModule, webGLContextAttributes) { return Browser.createContext(canvas, useWebGL, setInModule, webGLContextAttributes) }
STACK_BASE = STACKTOP = Runtime.alignMemory(STATICTOP);

staticSealed = true; // seal the static portion of memory

STACK_MAX = STACK_BASE + TOTAL_STACK;

DYNAMIC_BASE = DYNAMICTOP = Runtime.alignMemory(STACK_MAX);

assert(DYNAMIC_BASE < TOTAL_MEMORY, "TOTAL_MEMORY not big enough for stack");



Module.asmGlobalArg = { "Math": Math, "Int8Array": Int8Array, "Int16Array": Int16Array, "Int32Array": Int32Array, "Uint8Array": Uint8Array, "Uint16Array": Uint16Array, "Uint32Array": Uint32Array, "Float32Array": Float32Array, "Float64Array": Float64Array, "NaN": NaN, "Infinity": Infinity };
Module.asmLibraryArg = { "abort": abort, "assert": assert, "_sin": _sin, "_send": _send, "___setErrNo": ___setErrNo, "_fflush": _fflush, "_pwrite": _pwrite, "__reallyNegative": __reallyNegative, "_emscripten_set_main_loop_timing": _emscripten_set_main_loop_timing, "_sbrk": _sbrk, "_emscripten_memcpy_big": _emscripten_memcpy_big, "_fileno": _fileno, "_sysconf": _sysconf, "_cos": _cos, "_puts": _puts, "_printf": _printf, "_log": _log, "_write": _write, "_emscripten_set_main_loop": _emscripten_set_main_loop, "___errno_location": ___errno_location, "_fputc": _fputc, "_mkport": _mkport, "__exit": __exit, "_abort": _abort, "_fwrite": _fwrite, "_time": _time, "_fprintf": _fprintf, "_ceil": _ceil, "__formatString": __formatString, "_fputs": _fputs, "_exit": _exit, "STACKTOP": STACKTOP, "STACK_MAX": STACK_MAX, "tempDoublePtr": tempDoublePtr, "ABORT": ABORT };
// EMSCRIPTEN_START_ASM
var asm = (function(global, env, buffer) {
  'use asm';
  
  var HEAP8 = new global.Int8Array(buffer);
  var HEAP16 = new global.Int16Array(buffer);
  var HEAP32 = new global.Int32Array(buffer);
  var HEAPU8 = new global.Uint8Array(buffer);
  var HEAPU16 = new global.Uint16Array(buffer);
  var HEAPU32 = new global.Uint32Array(buffer);
  var HEAPF32 = new global.Float32Array(buffer);
  var HEAPF64 = new global.Float64Array(buffer);


  var STACKTOP=env.STACKTOP|0;
  var STACK_MAX=env.STACK_MAX|0;
  var tempDoublePtr=env.tempDoublePtr|0;
  var ABORT=env.ABORT|0;

  var __THREW__ = 0;
  var threwValue = 0;
  var setjmpId = 0;
  var undef = 0;
  var nan = global.NaN, inf = global.Infinity;
  var tempInt = 0, tempBigInt = 0, tempBigIntP = 0, tempBigIntS = 0, tempBigIntR = 0.0, tempBigIntI = 0, tempBigIntD = 0, tempValue = 0, tempDouble = 0.0;

  var tempRet0 = 0;
  var tempRet1 = 0;
  var tempRet2 = 0;
  var tempRet3 = 0;
  var tempRet4 = 0;
  var tempRet5 = 0;
  var tempRet6 = 0;
  var tempRet7 = 0;
  var tempRet8 = 0;
  var tempRet9 = 0;
  var Math_floor=global.Math.floor;
  var Math_abs=global.Math.abs;
  var Math_sqrt=global.Math.sqrt;
  var Math_pow=global.Math.pow;
  var Math_cos=global.Math.cos;
  var Math_sin=global.Math.sin;
  var Math_tan=global.Math.tan;
  var Math_acos=global.Math.acos;
  var Math_asin=global.Math.asin;
  var Math_atan=global.Math.atan;
  var Math_atan2=global.Math.atan2;
  var Math_exp=global.Math.exp;
  var Math_log=global.Math.log;
  var Math_ceil=global.Math.ceil;
  var Math_imul=global.Math.imul;
  var Math_min=global.Math.min;
  var Math_clz32=global.Math.clz32;
  var abort=env.abort;
  var assert=env.assert;
  var _sin=env._sin;
  var _send=env._send;
  var ___setErrNo=env.___setErrNo;
  var _fflush=env._fflush;
  var _pwrite=env._pwrite;
  var __reallyNegative=env.__reallyNegative;
  var _emscripten_set_main_loop_timing=env._emscripten_set_main_loop_timing;
  var _sbrk=env._sbrk;
  var _emscripten_memcpy_big=env._emscripten_memcpy_big;
  var _fileno=env._fileno;
  var _sysconf=env._sysconf;
  var _cos=env._cos;
  var _puts=env._puts;
  var _printf=env._printf;
  var _log=env._log;
  var _write=env._write;
  var _emscripten_set_main_loop=env._emscripten_set_main_loop;
  var ___errno_location=env.___errno_location;
  var _fputc=env._fputc;
  var _mkport=env._mkport;
  var __exit=env.__exit;
  var _abort=env._abort;
  var _fwrite=env._fwrite;
  var _time=env._time;
  var _fprintf=env._fprintf;
  var _ceil=env._ceil;
  var __formatString=env.__formatString;
  var _fputs=env._fputs;
  var _exit=env._exit;
  var tempFloat = 0.0;

// EMSCRIPTEN_START_FUNCS
function stackAlloc(size) {
  size = size|0;
  var ret = 0;
  ret = STACKTOP;
  STACKTOP = (STACKTOP + size)|0;
  STACKTOP = (STACKTOP + 15)&-16;

  return ret|0;
}
function stackSave() {
  return STACKTOP|0;
}
function stackRestore(top) {
  top = top|0;
  STACKTOP = top;
}
function establishStackSpace(stackBase, stackMax) {
  stackBase = stackBase|0;
  stackMax = stackMax|0;
  STACKTOP = stackBase;
  STACK_MAX = stackMax;
}

function setThrew(threw, value) {
  threw = threw|0;
  value = value|0;
  if ((__THREW__|0) == 0) {
    __THREW__ = threw;
    threwValue = value;
  }
}
function copyTempFloat(ptr) {
  ptr = ptr|0;
  HEAP8[tempDoublePtr>>0] = HEAP8[ptr>>0];
  HEAP8[tempDoublePtr+1>>0] = HEAP8[ptr+1>>0];
  HEAP8[tempDoublePtr+2>>0] = HEAP8[ptr+2>>0];
  HEAP8[tempDoublePtr+3>>0] = HEAP8[ptr+3>>0];
}
function copyTempDouble(ptr) {
  ptr = ptr|0;
  HEAP8[tempDoublePtr>>0] = HEAP8[ptr>>0];
  HEAP8[tempDoublePtr+1>>0] = HEAP8[ptr+1>>0];
  HEAP8[tempDoublePtr+2>>0] = HEAP8[ptr+2>>0];
  HEAP8[tempDoublePtr+3>>0] = HEAP8[ptr+3>>0];
  HEAP8[tempDoublePtr+4>>0] = HEAP8[ptr+4>>0];
  HEAP8[tempDoublePtr+5>>0] = HEAP8[ptr+5>>0];
  HEAP8[tempDoublePtr+6>>0] = HEAP8[ptr+6>>0];
  HEAP8[tempDoublePtr+7>>0] = HEAP8[ptr+7>>0];
}

function setTempRet0(value) {
  value = value|0;
  tempRet0 = value;
}
function getTempRet0() {
  return tempRet0|0;
}

function _fft_init($N,$sgn) {
 $N = $N|0;
 $sgn = $sgn|0;
 var $0 = 0, $1 = 0, $10 = 0.0, $11 = 0.0, $12 = 0.0, $13 = 0.0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0.0, $32 = 0.0, $33 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $M$0 = 0, $ct$01 = 0, $exitcond = 0, $exp2 = 0.0, $obj$0$in = 0, $or$cond = 0;
 var $twi_len$0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (_dividebyN($N)|0);
 $1 = ($0|0)==(1);
 if ($1) {
  $2 = $N << 4;
  $3 = (($2) + 272)|0;
  $4 = (_malloc($3)|0);
  $5 = ((($4)) + 8|0);
  $6 = (_factors($N,$5)|0);
  $7 = ((($4)) + 264|0);
  HEAP32[$7>>2] = $6;
  $8 = ((($4)) + 272|0);
  _longvectorN($8,0,$5,$6);
  $9 = ((($4)) + 268|0);
  HEAP32[$9>>2] = 0;
  $obj$0$in = $4;$twi_len$0 = $N;
 } else {
  $10 = (+($N|0));
  $11 = (+_log10($10));
  $12 = $11 / 0.3010299956639812;
  $13 = (+Math_ceil((+$12)));
  $exp2 = (+_exp2($13));
  $14 = (~~(($exp2)));
  $15 = $N << 1;
  $16 = (($15) + -2)|0;
  $17 = ($14|0)<($16|0);
  $18 = $17&1;
  $M$0 = $14 << $18;
  $19 = $M$0 << 4;
  $20 = (($19) + 272)|0;
  $21 = (_malloc($20)|0);
  $22 = ((($21)) + 8|0);
  $23 = (_factors($M$0,$22)|0);
  $24 = ((($21)) + 264|0);
  HEAP32[$24>>2] = $23;
  $25 = ((($21)) + 272|0);
  _longvectorN($25,0,$22,$23);
  $26 = ((($21)) + 268|0);
  HEAP32[$26>>2] = 1;
  $obj$0$in = $21;$twi_len$0 = $M$0;
 }
 HEAP32[$obj$0$in>>2] = $N;
 $27 = ((($obj$0$in)) + 4|0);
 HEAP32[$27>>2] = $sgn;
 $28 = ($sgn|0)==(-1);
 $29 = ($twi_len$0|0)>(0);
 $or$cond = $28 & $29;
 if ($or$cond) {
  $ct$01 = 0;
 } else {
  return ($obj$0$in|0);
 }
 while(1) {
  $30 = (((((($obj$0$in)) + 272|0) + ($ct$01<<4)|0)) + 8|0);
  $31 = +HEAPF64[$30>>3];
  $32 = -$31;
  HEAPF64[$30>>3] = $32;
  $33 = (($ct$01) + 1)|0;
  $exitcond = ($33|0)==($twi_len$0|0);
  if ($exitcond) {
   break;
  } else {
   $ct$01 = $33;
  }
 }
 return ($obj$0$in|0);
}
function _dividebyN($N) {
 $N = $N|0;
 var $$ = 0, $$0$lcssa = 0, $$063 = 0, $$1$lcssa = 0, $$10$lcssa = 0, $$1033 = 0, $$11$lcssa = 0, $$1130 = 0, $$12$lcssa = 0, $$1227 = 0, $$13$lcssa = 0, $$1324 = 0, $$14$lcssa = 0, $$1421 = 0, $$15$lcssa = 0, $$1518 = 0, $$16$lcssa = 0, $$160 = 0, $$1617 = 0, $$2$lcssa = 0;
 var $$257 = 0, $$3$lcssa = 0, $$354 = 0, $$4$lcssa = 0, $$451 = 0, $$5$lcssa = 0, $$548 = 0, $$6$lcssa = 0, $$645 = 0, $$7$lcssa = 0, $$742 = 0, $$8$lcssa = 0, $$839 = 0, $$9$lcssa = 0, $$936 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0;
 var $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0;
 var $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0;
 var $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0;
 var $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0;
 var $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (($N|0) % 53)&-1;
 $1 = ($0|0)==(0);
 if ($1) {
  $$063 = $N;
  while(1) {
   $4 = (($$063|0) / 53)&-1;
   $5 = (($4|0) % 53)&-1;
   $6 = ($5|0)==(0);
   if ($6) {
    $$063 = $4;
   } else {
    $$0$lcssa = $4;
    break;
   }
  }
 } else {
  $$0$lcssa = $N;
 }
 $2 = (($$0$lcssa|0) % 47)&-1;
 $3 = ($2|0)==(0);
 if ($3) {
  $$160 = $$0$lcssa;
  while(1) {
   $9 = (($$160|0) / 47)&-1;
   $10 = (($9|0) % 47)&-1;
   $11 = ($10|0)==(0);
   if ($11) {
    $$160 = $9;
   } else {
    $$1$lcssa = $9;
    break;
   }
  }
 } else {
  $$1$lcssa = $$0$lcssa;
 }
 $7 = (($$1$lcssa|0) % 43)&-1;
 $8 = ($7|0)==(0);
 if ($8) {
  $$257 = $$1$lcssa;
  while(1) {
   $14 = (($$257|0) / 43)&-1;
   $15 = (($14|0) % 43)&-1;
   $16 = ($15|0)==(0);
   if ($16) {
    $$257 = $14;
   } else {
    $$2$lcssa = $14;
    break;
   }
  }
 } else {
  $$2$lcssa = $$1$lcssa;
 }
 $12 = (($$2$lcssa|0) % 41)&-1;
 $13 = ($12|0)==(0);
 if ($13) {
  $$354 = $$2$lcssa;
  while(1) {
   $19 = (($$354|0) / 41)&-1;
   $20 = (($19|0) % 41)&-1;
   $21 = ($20|0)==(0);
   if ($21) {
    $$354 = $19;
   } else {
    $$3$lcssa = $19;
    break;
   }
  }
 } else {
  $$3$lcssa = $$2$lcssa;
 }
 $17 = (($$3$lcssa|0) % 37)&-1;
 $18 = ($17|0)==(0);
 if ($18) {
  $$451 = $$3$lcssa;
  while(1) {
   $24 = (($$451|0) / 37)&-1;
   $25 = (($24|0) % 37)&-1;
   $26 = ($25|0)==(0);
   if ($26) {
    $$451 = $24;
   } else {
    $$4$lcssa = $24;
    break;
   }
  }
 } else {
  $$4$lcssa = $$3$lcssa;
 }
 $22 = (($$4$lcssa|0) % 31)&-1;
 $23 = ($22|0)==(0);
 if ($23) {
  $$548 = $$4$lcssa;
  while(1) {
   $29 = (($$548|0) / 31)&-1;
   $30 = (($29|0) % 31)&-1;
   $31 = ($30|0)==(0);
   if ($31) {
    $$548 = $29;
   } else {
    $$5$lcssa = $29;
    break;
   }
  }
 } else {
  $$5$lcssa = $$4$lcssa;
 }
 $27 = (($$5$lcssa|0) % 29)&-1;
 $28 = ($27|0)==(0);
 if ($28) {
  $$645 = $$5$lcssa;
  while(1) {
   $34 = (($$645|0) / 29)&-1;
   $35 = (($34|0) % 29)&-1;
   $36 = ($35|0)==(0);
   if ($36) {
    $$645 = $34;
   } else {
    $$6$lcssa = $34;
    break;
   }
  }
 } else {
  $$6$lcssa = $$5$lcssa;
 }
 $32 = (($$6$lcssa|0) % 23)&-1;
 $33 = ($32|0)==(0);
 if ($33) {
  $$742 = $$6$lcssa;
  while(1) {
   $39 = (($$742|0) / 23)&-1;
   $40 = (($39|0) % 23)&-1;
   $41 = ($40|0)==(0);
   if ($41) {
    $$742 = $39;
   } else {
    $$7$lcssa = $39;
    break;
   }
  }
 } else {
  $$7$lcssa = $$6$lcssa;
 }
 $37 = (($$7$lcssa|0) % 17)&-1;
 $38 = ($37|0)==(0);
 if ($38) {
  $$839 = $$7$lcssa;
  while(1) {
   $44 = (($$839|0) / 17)&-1;
   $45 = (($44|0) % 17)&-1;
   $46 = ($45|0)==(0);
   if ($46) {
    $$839 = $44;
   } else {
    $$8$lcssa = $44;
    break;
   }
  }
 } else {
  $$8$lcssa = $$7$lcssa;
 }
 $42 = (($$8$lcssa|0) % 13)&-1;
 $43 = ($42|0)==(0);
 if ($43) {
  $$936 = $$8$lcssa;
  while(1) {
   $49 = (($$936|0) / 13)&-1;
   $50 = (($49|0) % 13)&-1;
   $51 = ($50|0)==(0);
   if ($51) {
    $$936 = $49;
   } else {
    $$9$lcssa = $49;
    break;
   }
  }
 } else {
  $$9$lcssa = $$8$lcssa;
 }
 $47 = (($$9$lcssa|0) % 11)&-1;
 $48 = ($47|0)==(0);
 if ($48) {
  $$1033 = $$9$lcssa;
  while(1) {
   $54 = (($$1033|0) / 11)&-1;
   $55 = (($54|0) % 11)&-1;
   $56 = ($55|0)==(0);
   if ($56) {
    $$1033 = $54;
   } else {
    $$10$lcssa = $54;
    break;
   }
  }
 } else {
  $$10$lcssa = $$9$lcssa;
 }
 $52 = $$10$lcssa & 7;
 $53 = ($52|0)==(0);
 if ($53) {
  $$1130 = $$10$lcssa;
  while(1) {
   $59 = (($$1130|0) / 8)&-1;
   $60 = $59 & 7;
   $61 = ($60|0)==(0);
   if ($61) {
    $$1130 = $59;
   } else {
    $$11$lcssa = $59;
    break;
   }
  }
 } else {
  $$11$lcssa = $$10$lcssa;
 }
 $57 = (($$11$lcssa|0) % 7)&-1;
 $58 = ($57|0)==(0);
 if ($58) {
  $$1227 = $$11$lcssa;
  while(1) {
   $64 = (($$1227|0) / 7)&-1;
   $65 = (($64|0) % 7)&-1;
   $66 = ($65|0)==(0);
   if ($66) {
    $$1227 = $64;
   } else {
    $$12$lcssa = $64;
    break;
   }
  }
 } else {
  $$12$lcssa = $$11$lcssa;
 }
 $62 = (($$12$lcssa|0) % 5)&-1;
 $63 = ($62|0)==(0);
 if ($63) {
  $$1324 = $$12$lcssa;
  while(1) {
   $69 = (($$1324|0) / 5)&-1;
   $70 = (($69|0) % 5)&-1;
   $71 = ($70|0)==(0);
   if ($71) {
    $$1324 = $69;
   } else {
    $$13$lcssa = $69;
    break;
   }
  }
 } else {
  $$13$lcssa = $$12$lcssa;
 }
 $67 = $$13$lcssa & 3;
 $68 = ($67|0)==(0);
 if ($68) {
  $$1421 = $$13$lcssa;
  while(1) {
   $74 = (($$1421|0) / 4)&-1;
   $75 = $74 & 3;
   $76 = ($75|0)==(0);
   if ($76) {
    $$1421 = $74;
   } else {
    $$14$lcssa = $74;
    break;
   }
  }
 } else {
  $$14$lcssa = $$13$lcssa;
 }
 $72 = (($$14$lcssa|0) % 3)&-1;
 $73 = ($72|0)==(0);
 if ($73) {
  $$1518 = $$14$lcssa;
  while(1) {
   $79 = (($$1518|0) / 3)&-1;
   $80 = (($79|0) % 3)&-1;
   $81 = ($80|0)==(0);
   if ($81) {
    $$1518 = $79;
   } else {
    $$15$lcssa = $79;
    break;
   }
  }
 } else {
  $$15$lcssa = $$14$lcssa;
 }
 $77 = $$15$lcssa & 1;
 $78 = ($77|0)==(0);
 if ($78) {
  $$1617 = $$15$lcssa;
 } else {
  $$16$lcssa = $$15$lcssa;
  $85 = ($$16$lcssa|0)==(1);
  $$ = $85&1;
  return ($$|0);
 }
 while(1) {
  $82 = (($$1617|0) / 2)&-1;
  $83 = $82 & 1;
  $84 = ($83|0)==(0);
  if ($84) {
   $$1617 = $82;
  } else {
   $$16$lcssa = $82;
   break;
  }
 }
 $85 = ($$16$lcssa|0)==(1);
 $$ = $85&1;
 return ($$|0);
}
function _factors($M,$arr) {
 $M = $M|0;
 $arr = $arr|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0;
 var $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0;
 var $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0;
 var $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0;
 var $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0;
 var $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $N$0$lcssa = 0, $N$0118 = 0, $N$1$lcssa = 0, $N$10$lcssa = 0, $N$1068 = 0, $N$11$lcssa = 0, $N$1113 = 0, $N$1163 = 0, $N$12$lcssa = 0, $N$1258 = 0, $N$13$lcssa = 0, $N$1353 = 0, $N$14$lcssa = 0, $N$1448 = 0;
 var $N$15$lcssa = 0, $N$1543 = 0, $N$16$lcssa = 0, $N$1638 = 0, $N$17$lcssa = 0, $N$1732 = 0, $N$1828 = 0, $N$19$lcssa = 0, $N$1919 = 0, $N$2$lcssa = 0, $N$20$lcssa = 0, $N$2022 = 0, $N$2108 = 0, $N$3$lcssa = 0, $N$3103 = 0, $N$4$lcssa = 0, $N$498 = 0, $N$5$lcssa = 0, $N$593 = 0, $N$6$lcssa = 0;
 var $N$688 = 0, $N$7$lcssa = 0, $N$783 = 0, $N$8$lcssa = 0, $N$878 = 0, $N$9$lcssa = 0, $N$973 = 0, $i$0$lcssa = 0, $i$0119 = 0, $i$1$lcssa = 0, $i$10$lcssa = 0, $i$1069 = 0, $i$11$lcssa = 0, $i$1114 = 0, $i$1164 = 0, $i$12$lcssa = 0, $i$1259 = 0, $i$13$lcssa = 0, $i$1354 = 0, $i$14$lcssa = 0;
 var $i$1449 = 0, $i$15$lcssa = 0, $i$1544 = 0, $i$16$lcssa = 0, $i$1639 = 0, $i$17$lcssa = 0, $i$1733 = 0, $i$1829 = 0, $i$19$lcssa = 0, $i$1920 = 0, $i$2$lcssa = 0, $i$20$lcssa = 0, $i$2023 = 0, $i$21 = 0, $i$2109 = 0, $i$3$lcssa = 0, $i$3104 = 0, $i$4$lcssa = 0, $i$499 = 0, $i$5$lcssa = 0;
 var $i$594 = 0, $i$6$lcssa = 0, $i$689 = 0, $i$7$lcssa = 0, $i$784 = 0, $i$8$lcssa = 0, $i$879 = 0, $i$9$lcssa = 0, $i$974 = 0, $num$027 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (($M|0) % 53)&-1;
 $1 = ($0|0)==(0);
 if ($1) {
  $N$0118 = $M;$i$0119 = 0;
  while(1) {
   $4 = (($N$0118|0) / 53)&-1;
   $5 = (($arr) + ($i$0119<<2)|0);
   HEAP32[$5>>2] = 53;
   $6 = (($i$0119) + 1)|0;
   $7 = (($4|0) % 53)&-1;
   $8 = ($7|0)==(0);
   if ($8) {
    $N$0118 = $4;$i$0119 = $6;
   } else {
    $N$0$lcssa = $4;$i$0$lcssa = $6;
    break;
   }
  }
 } else {
  $N$0$lcssa = $M;$i$0$lcssa = 0;
 }
 $2 = (($N$0$lcssa|0) % 47)&-1;
 $3 = ($2|0)==(0);
 if ($3) {
  $N$1113 = $N$0$lcssa;$i$1114 = $i$0$lcssa;
  while(1) {
   $11 = (($N$1113|0) / 47)&-1;
   $12 = (($arr) + ($i$1114<<2)|0);
   HEAP32[$12>>2] = 47;
   $13 = (($i$1114) + 1)|0;
   $14 = (($11|0) % 47)&-1;
   $15 = ($14|0)==(0);
   if ($15) {
    $N$1113 = $11;$i$1114 = $13;
   } else {
    $N$1$lcssa = $11;$i$1$lcssa = $13;
    break;
   }
  }
 } else {
  $N$1$lcssa = $N$0$lcssa;$i$1$lcssa = $i$0$lcssa;
 }
 $9 = (($N$1$lcssa|0) % 43)&-1;
 $10 = ($9|0)==(0);
 if ($10) {
  $N$2108 = $N$1$lcssa;$i$2109 = $i$1$lcssa;
  while(1) {
   $18 = (($N$2108|0) / 43)&-1;
   $19 = (($arr) + ($i$2109<<2)|0);
   HEAP32[$19>>2] = 43;
   $20 = (($i$2109) + 1)|0;
   $21 = (($18|0) % 43)&-1;
   $22 = ($21|0)==(0);
   if ($22) {
    $N$2108 = $18;$i$2109 = $20;
   } else {
    $N$2$lcssa = $18;$i$2$lcssa = $20;
    break;
   }
  }
 } else {
  $N$2$lcssa = $N$1$lcssa;$i$2$lcssa = $i$1$lcssa;
 }
 $16 = (($N$2$lcssa|0) % 41)&-1;
 $17 = ($16|0)==(0);
 if ($17) {
  $N$3103 = $N$2$lcssa;$i$3104 = $i$2$lcssa;
  while(1) {
   $25 = (($N$3103|0) / 41)&-1;
   $26 = (($arr) + ($i$3104<<2)|0);
   HEAP32[$26>>2] = 41;
   $27 = (($i$3104) + 1)|0;
   $28 = (($25|0) % 41)&-1;
   $29 = ($28|0)==(0);
   if ($29) {
    $N$3103 = $25;$i$3104 = $27;
   } else {
    $N$3$lcssa = $25;$i$3$lcssa = $27;
    break;
   }
  }
 } else {
  $N$3$lcssa = $N$2$lcssa;$i$3$lcssa = $i$2$lcssa;
 }
 $23 = (($N$3$lcssa|0) % 37)&-1;
 $24 = ($23|0)==(0);
 if ($24) {
  $N$498 = $N$3$lcssa;$i$499 = $i$3$lcssa;
  while(1) {
   $32 = (($N$498|0) / 37)&-1;
   $33 = (($arr) + ($i$499<<2)|0);
   HEAP32[$33>>2] = 37;
   $34 = (($i$499) + 1)|0;
   $35 = (($32|0) % 37)&-1;
   $36 = ($35|0)==(0);
   if ($36) {
    $N$498 = $32;$i$499 = $34;
   } else {
    $N$4$lcssa = $32;$i$4$lcssa = $34;
    break;
   }
  }
 } else {
  $N$4$lcssa = $N$3$lcssa;$i$4$lcssa = $i$3$lcssa;
 }
 $30 = (($N$4$lcssa|0) % 31)&-1;
 $31 = ($30|0)==(0);
 if ($31) {
  $N$593 = $N$4$lcssa;$i$594 = $i$4$lcssa;
  while(1) {
   $39 = (($N$593|0) / 31)&-1;
   $40 = (($arr) + ($i$594<<2)|0);
   HEAP32[$40>>2] = 31;
   $41 = (($i$594) + 1)|0;
   $42 = (($39|0) % 31)&-1;
   $43 = ($42|0)==(0);
   if ($43) {
    $N$593 = $39;$i$594 = $41;
   } else {
    $N$5$lcssa = $39;$i$5$lcssa = $41;
    break;
   }
  }
 } else {
  $N$5$lcssa = $N$4$lcssa;$i$5$lcssa = $i$4$lcssa;
 }
 $37 = (($N$5$lcssa|0) % 29)&-1;
 $38 = ($37|0)==(0);
 if ($38) {
  $N$688 = $N$5$lcssa;$i$689 = $i$5$lcssa;
  while(1) {
   $46 = (($N$688|0) / 29)&-1;
   $47 = (($arr) + ($i$689<<2)|0);
   HEAP32[$47>>2] = 29;
   $48 = (($i$689) + 1)|0;
   $49 = (($46|0) % 29)&-1;
   $50 = ($49|0)==(0);
   if ($50) {
    $N$688 = $46;$i$689 = $48;
   } else {
    $N$6$lcssa = $46;$i$6$lcssa = $48;
    break;
   }
  }
 } else {
  $N$6$lcssa = $N$5$lcssa;$i$6$lcssa = $i$5$lcssa;
 }
 $44 = (($N$6$lcssa|0) % 23)&-1;
 $45 = ($44|0)==(0);
 if ($45) {
  $N$783 = $N$6$lcssa;$i$784 = $i$6$lcssa;
  while(1) {
   $53 = (($N$783|0) / 23)&-1;
   $54 = (($arr) + ($i$784<<2)|0);
   HEAP32[$54>>2] = 23;
   $55 = (($i$784) + 1)|0;
   $56 = (($53|0) % 23)&-1;
   $57 = ($56|0)==(0);
   if ($57) {
    $N$783 = $53;$i$784 = $55;
   } else {
    $N$7$lcssa = $53;$i$7$lcssa = $55;
    break;
   }
  }
 } else {
  $N$7$lcssa = $N$6$lcssa;$i$7$lcssa = $i$6$lcssa;
 }
 $51 = (($N$7$lcssa|0) % 19)&-1;
 $52 = ($51|0)==(0);
 if ($52) {
  $N$878 = $N$7$lcssa;$i$879 = $i$7$lcssa;
  while(1) {
   $60 = (($N$878|0) / 19)&-1;
   $61 = (($arr) + ($i$879<<2)|0);
   HEAP32[$61>>2] = 19;
   $62 = (($i$879) + 1)|0;
   $63 = (($60|0) % 19)&-1;
   $64 = ($63|0)==(0);
   if ($64) {
    $N$878 = $60;$i$879 = $62;
   } else {
    $N$8$lcssa = $60;$i$8$lcssa = $62;
    break;
   }
  }
 } else {
  $N$8$lcssa = $N$7$lcssa;$i$8$lcssa = $i$7$lcssa;
 }
 $58 = (($N$8$lcssa|0) % 17)&-1;
 $59 = ($58|0)==(0);
 if ($59) {
  $N$973 = $N$8$lcssa;$i$974 = $i$8$lcssa;
  while(1) {
   $67 = (($N$973|0) / 17)&-1;
   $68 = (($arr) + ($i$974<<2)|0);
   HEAP32[$68>>2] = 17;
   $69 = (($i$974) + 1)|0;
   $70 = (($67|0) % 17)&-1;
   $71 = ($70|0)==(0);
   if ($71) {
    $N$973 = $67;$i$974 = $69;
   } else {
    $N$9$lcssa = $67;$i$9$lcssa = $69;
    break;
   }
  }
 } else {
  $N$9$lcssa = $N$8$lcssa;$i$9$lcssa = $i$8$lcssa;
 }
 $65 = (($N$9$lcssa|0) % 13)&-1;
 $66 = ($65|0)==(0);
 if ($66) {
  $N$1068 = $N$9$lcssa;$i$1069 = $i$9$lcssa;
  while(1) {
   $74 = (($N$1068|0) / 13)&-1;
   $75 = (($arr) + ($i$1069<<2)|0);
   HEAP32[$75>>2] = 13;
   $76 = (($i$1069) + 1)|0;
   $77 = (($74|0) % 13)&-1;
   $78 = ($77|0)==(0);
   if ($78) {
    $N$1068 = $74;$i$1069 = $76;
   } else {
    $N$10$lcssa = $74;$i$10$lcssa = $76;
    break;
   }
  }
 } else {
  $N$10$lcssa = $N$9$lcssa;$i$10$lcssa = $i$9$lcssa;
 }
 $72 = (($N$10$lcssa|0) % 11)&-1;
 $73 = ($72|0)==(0);
 if ($73) {
  $N$1163 = $N$10$lcssa;$i$1164 = $i$10$lcssa;
  while(1) {
   $81 = (($N$1163|0) / 11)&-1;
   $82 = (($arr) + ($i$1164<<2)|0);
   HEAP32[$82>>2] = 11;
   $83 = (($i$1164) + 1)|0;
   $84 = (($81|0) % 11)&-1;
   $85 = ($84|0)==(0);
   if ($85) {
    $N$1163 = $81;$i$1164 = $83;
   } else {
    $N$11$lcssa = $81;$i$11$lcssa = $83;
    break;
   }
  }
 } else {
  $N$11$lcssa = $N$10$lcssa;$i$11$lcssa = $i$10$lcssa;
 }
 $79 = $N$11$lcssa & 7;
 $80 = ($79|0)==(0);
 if ($80) {
  $N$1258 = $N$11$lcssa;$i$1259 = $i$11$lcssa;
  while(1) {
   $88 = (($N$1258|0) / 8)&-1;
   $89 = (($arr) + ($i$1259<<2)|0);
   HEAP32[$89>>2] = 8;
   $90 = (($i$1259) + 1)|0;
   $91 = $88 & 7;
   $92 = ($91|0)==(0);
   if ($92) {
    $N$1258 = $88;$i$1259 = $90;
   } else {
    $N$12$lcssa = $88;$i$12$lcssa = $90;
    break;
   }
  }
 } else {
  $N$12$lcssa = $N$11$lcssa;$i$12$lcssa = $i$11$lcssa;
 }
 $86 = (($N$12$lcssa|0) % 7)&-1;
 $87 = ($86|0)==(0);
 if ($87) {
  $N$1353 = $N$12$lcssa;$i$1354 = $i$12$lcssa;
  while(1) {
   $95 = (($N$1353|0) / 7)&-1;
   $96 = (($arr) + ($i$1354<<2)|0);
   HEAP32[$96>>2] = 7;
   $97 = (($i$1354) + 1)|0;
   $98 = (($95|0) % 7)&-1;
   $99 = ($98|0)==(0);
   if ($99) {
    $N$1353 = $95;$i$1354 = $97;
   } else {
    $N$13$lcssa = $95;$i$13$lcssa = $97;
    break;
   }
  }
 } else {
  $N$13$lcssa = $N$12$lcssa;$i$13$lcssa = $i$12$lcssa;
 }
 $93 = (($N$13$lcssa|0) % 5)&-1;
 $94 = ($93|0)==(0);
 if ($94) {
  $N$1448 = $N$13$lcssa;$i$1449 = $i$13$lcssa;
  while(1) {
   $102 = (($N$1448|0) / 5)&-1;
   $103 = (($arr) + ($i$1449<<2)|0);
   HEAP32[$103>>2] = 5;
   $104 = (($i$1449) + 1)|0;
   $105 = (($102|0) % 5)&-1;
   $106 = ($105|0)==(0);
   if ($106) {
    $N$1448 = $102;$i$1449 = $104;
   } else {
    $N$14$lcssa = $102;$i$14$lcssa = $104;
    break;
   }
  }
 } else {
  $N$14$lcssa = $N$13$lcssa;$i$14$lcssa = $i$13$lcssa;
 }
 $100 = $N$14$lcssa & 3;
 $101 = ($100|0)==(0);
 if ($101) {
  $N$1543 = $N$14$lcssa;$i$1544 = $i$14$lcssa;
  while(1) {
   $109 = (($N$1543|0) / 4)&-1;
   $110 = (($arr) + ($i$1544<<2)|0);
   HEAP32[$110>>2] = 4;
   $111 = (($i$1544) + 1)|0;
   $112 = $109 & 3;
   $113 = ($112|0)==(0);
   if ($113) {
    $N$1543 = $109;$i$1544 = $111;
   } else {
    $N$15$lcssa = $109;$i$15$lcssa = $111;
    break;
   }
  }
 } else {
  $N$15$lcssa = $N$14$lcssa;$i$15$lcssa = $i$14$lcssa;
 }
 $107 = (($N$15$lcssa|0) % 3)&-1;
 $108 = ($107|0)==(0);
 if ($108) {
  $N$1638 = $N$15$lcssa;$i$1639 = $i$15$lcssa;
  while(1) {
   $116 = (($N$1638|0) / 3)&-1;
   $117 = (($arr) + ($i$1639<<2)|0);
   HEAP32[$117>>2] = 3;
   $118 = (($i$1639) + 1)|0;
   $119 = (($116|0) % 3)&-1;
   $120 = ($119|0)==(0);
   if ($120) {
    $N$1638 = $116;$i$1639 = $118;
   } else {
    $N$16$lcssa = $116;$i$16$lcssa = $118;
    break;
   }
  }
 } else {
  $N$16$lcssa = $N$15$lcssa;$i$16$lcssa = $i$15$lcssa;
 }
 $114 = $N$16$lcssa & 1;
 $115 = ($114|0)==(0);
 if ($115) {
  $N$1732 = $N$16$lcssa;$i$1733 = $i$16$lcssa;
  while(1) {
   $121 = (($N$1732|0) / 2)&-1;
   $122 = (($arr) + ($i$1733<<2)|0);
   HEAP32[$122>>2] = 2;
   $123 = (($i$1733) + 1)|0;
   $124 = $121 & 1;
   $125 = ($124|0)==(0);
   if ($125) {
    $N$1732 = $121;$i$1733 = $123;
   } else {
    $N$17$lcssa = $121;$i$17$lcssa = $123;
    break;
   }
  }
 } else {
  $N$17$lcssa = $N$16$lcssa;$i$17$lcssa = $i$16$lcssa;
 }
 $126 = ($N$17$lcssa|0)>(31);
 if ($126) {
  $N$1828 = $N$17$lcssa;$i$1829 = $i$17$lcssa;$num$027 = 2;
 } else {
  $i$21 = $i$17$lcssa;
  return ($i$21|0);
 }
 while(1) {
  $127 = ($num$027*6)|0;
  $128 = (($127) + -1)|0;
  $129 = $127 | 1;
  $130 = (($N$1828|0) % ($128|0))&-1;
  $131 = ($130|0)==(0);
  if ($131) {
   $N$1919 = $N$1828;$i$1920 = $i$1829;
   while(1) {
    $134 = (($arr) + ($i$1920<<2)|0);
    HEAP32[$134>>2] = $128;
    $135 = (($i$1920) + 1)|0;
    $136 = (($N$1919|0) / ($128|0))&-1;
    $137 = (($136|0) % ($128|0))&-1;
    $138 = ($137|0)==(0);
    if ($138) {
     $N$1919 = $136;$i$1920 = $135;
    } else {
     $N$19$lcssa = $136;$i$19$lcssa = $135;
     break;
    }
   }
  } else {
   $N$19$lcssa = $N$1828;$i$19$lcssa = $i$1829;
  }
  $132 = (($N$19$lcssa|0) % ($129|0))&-1;
  $133 = ($132|0)==(0);
  if ($133) {
   $N$2022 = $N$19$lcssa;$i$2023 = $i$19$lcssa;
   while(1) {
    $139 = (($arr) + ($i$2023<<2)|0);
    HEAP32[$139>>2] = $129;
    $140 = (($i$2023) + 1)|0;
    $141 = (($N$2022|0) / ($129|0))&-1;
    $142 = (($141|0) % ($129|0))&-1;
    $143 = ($142|0)==(0);
    if ($143) {
     $N$2022 = $141;$i$2023 = $140;
    } else {
     $N$20$lcssa = $141;$i$20$lcssa = $140;
     break;
    }
   }
  } else {
   $N$20$lcssa = $N$19$lcssa;$i$20$lcssa = $i$19$lcssa;
  }
  $144 = (($num$027) + 1)|0;
  $145 = ($N$20$lcssa|0)>(1);
  if ($145) {
   $N$1828 = $N$20$lcssa;$i$1829 = $i$20$lcssa;$num$027 = $144;
  } else {
   $i$21 = $i$20$lcssa;
   break;
  }
 }
 return ($i$21|0);
}
function _longvectorN($sig,$N,$array,$tx) {
 $sig = $sig|0;
 $N = $N|0;
 $array = $array|0;
 $tx = $tx|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0.0, $17 = 0.0, $18 = 0.0, $19 = 0, $2 = 0, $20 = 0.0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $3 = 0, $4 = 0, $5 = 0, $6 = 0.0, $7 = 0.0, $8 = 0, $9 = 0, $L$010 = 0, $ct$09 = 0, $ct$1$lcssa = 0, $ct$13 = 0, $ct$2$lcssa = 0, $ct$21 = 0, $exitcond = 0, $exitcond14 = 0, $i$08 = 0, $j$04 = 0, $k$02 = 0, $smax = 0, label = 0;
 var sp = 0;
 sp = STACKTOP;
 $0 = ($tx|0)>(0);
 if (!($0)) {
  return;
 }
 $1 = (($tx) + -1)|0;
 $L$010 = 1;$ct$09 = 0;$i$08 = 0;
 while(1) {
  $2 = (($1) - ($i$08))|0;
  $3 = (($array) + ($2<<2)|0);
  $4 = HEAP32[$3>>2]|0;
  $5 = Math_imul($4, $L$010)|0;
  $6 = (+($5|0));
  $7 = -6.2831853071795862 / $6;
  $8 = ($L$010|0)>(0);
  if ($8) {
   $9 = HEAP32[$3>>2]|0;
   $10 = ($9|0)>(1);
   $ct$13 = $ct$09;$j$04 = 0;
   while(1) {
    if ($10) {
     $11 = HEAP32[$3>>2]|0;
     $12 = (($11) + -1)|0;
     $13 = ($12|0)>(1);
     $smax = $13 ? $12 : 1;
     $ct$21 = $ct$13;$k$02 = 0;
     while(1) {
      $14 = (($k$02) + 1)|0;
      $15 = Math_imul($14, $j$04)|0;
      $16 = (+($15|0));
      $17 = $7 * $16;
      $18 = (+Math_cos((+$17)));
      $19 = (($sig) + ($ct$21<<4)|0);
      HEAPF64[$19>>3] = $18;
      $20 = (+Math_sin((+$17)));
      $21 = (((($sig) + ($ct$21<<4)|0)) + 8|0);
      HEAPF64[$21>>3] = $20;
      $22 = (($ct$21) + 1)|0;
      $23 = ($14|0)<($12|0);
      if ($23) {
       $ct$21 = $22;$k$02 = $14;
      } else {
       break;
      }
     }
     $24 = (($ct$13) + ($smax))|0;
     $ct$2$lcssa = $24;
    } else {
     $ct$2$lcssa = $ct$13;
    }
    $25 = (($j$04) + 1)|0;
    $exitcond = ($25|0)==($L$010|0);
    if ($exitcond) {
     $ct$1$lcssa = $ct$2$lcssa;
     break;
    } else {
     $ct$13 = $ct$2$lcssa;$j$04 = $25;
    }
   }
  } else {
   $ct$1$lcssa = $ct$09;
  }
  $26 = (($i$08) + 1)|0;
  $exitcond14 = ($26|0)==($tx|0);
  if ($exitcond14) {
   break;
  } else {
   $L$010 = $5;$ct$09 = $ct$1$lcssa;$i$08 = $26;
  }
 }
 return;
}
function _fft_exec($obj,$inp,$oup) {
 $obj = $obj|0;
 $inp = $inp|0;
 $oup = $oup|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ((($obj)) + 268|0);
 $1 = HEAP32[$0>>2]|0;
 if ((($1|0) == 1)) {
  $5 = HEAP32[$obj>>2]|0;
  $6 = ((($obj)) + 4|0);
  $7 = HEAP32[$6>>2]|0;
  _bluestein_fft($inp,$oup,$obj,$7,$5);
  return;
 } else if ((($1|0) == 0)) {
  $2 = HEAP32[$obj>>2]|0;
  $3 = ((($obj)) + 4|0);
  $4 = HEAP32[$3>>2]|0;
  _mixed_radix_dit_rec($oup,$inp,$obj,$4,$2,1,0);
  return;
 } else {
  return;
 }
}
function _mixed_radix_dit_rec($op,$ip,$obj,$sgn,$N,$l,$inc) {
 $op = $op|0;
 $ip = $ip|0;
 $obj = $obj|0;
 $sgn = $sgn|0;
 $N = $N|0;
 $l = $l|0;
 $inc = $inc|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0.0, $1000 = 0, $1001 = 0, $1002 = 0, $1003 = 0, $1004 = 0, $1005 = 0, $1006 = 0, $1007 = 0, $1008 = 0, $1009 = 0, $101 = 0.0, $1010 = 0, $1011 = 0, $1012 = 0, $1013 = 0.0, $1014 = 0;
 var $1015 = 0.0, $1016 = 0.0, $1017 = 0, $1018 = 0.0, $1019 = 0, $102 = 0.0, $1020 = 0.0, $1021 = 0, $1022 = 0.0, $1023 = 0, $1024 = 0.0, $1025 = 0, $1026 = 0.0, $1027 = 0, $1028 = 0.0, $1029 = 0, $103 = 0.0, $1030 = 0.0, $1031 = 0, $1032 = 0.0;
 var $1033 = 0, $1034 = 0.0, $1035 = 0, $1036 = 0.0, $1037 = 0, $1038 = 0.0, $1039 = 0.0, $104 = 0.0, $1040 = 0.0, $1041 = 0.0, $1042 = 0.0, $1043 = 0.0, $1044 = 0.0, $1045 = 0.0, $1046 = 0.0, $1047 = 0.0, $1048 = 0.0, $1049 = 0.0, $105 = 0.0, $1050 = 0.0;
 var $1051 = 0.0, $1052 = 0.0, $1053 = 0.0, $1054 = 0.0, $1055 = 0.0, $1056 = 0.0, $1057 = 0.0, $1058 = 0.0, $1059 = 0.0, $106 = 0.0, $1060 = 0.0, $1061 = 0.0, $1062 = 0.0, $1063 = 0.0, $1064 = 0.0, $1065 = 0.0, $1066 = 0.0, $1067 = 0.0, $1068 = 0.0, $1069 = 0;
 var $107 = 0.0, $1070 = 0.0, $1071 = 0.0, $1072 = 0.0, $1073 = 0.0, $1074 = 0.0, $1075 = 0.0, $1076 = 0.0, $1077 = 0.0, $1078 = 0.0, $1079 = 0.0, $108 = 0.0, $1080 = 0.0, $1081 = 0.0, $1082 = 0.0, $1083 = 0.0, $1084 = 0.0, $1085 = 0.0, $1086 = 0.0, $1087 = 0.0;
 var $1088 = 0.0, $1089 = 0.0, $109 = 0.0, $1090 = 0.0, $1091 = 0.0, $1092 = 0.0, $1093 = 0.0, $1094 = 0.0, $1095 = 0.0, $1096 = 0.0, $1097 = 0.0, $1098 = 0.0, $1099 = 0.0, $11 = 0, $110 = 0.0, $1100 = 0.0, $1101 = 0.0, $1102 = 0.0, $1103 = 0.0, $1104 = 0.0;
 var $1105 = 0.0, $1106 = 0.0, $1107 = 0.0, $1108 = 0.0, $1109 = 0.0, $111 = 0.0, $1110 = 0.0, $1111 = 0.0, $1112 = 0.0, $1113 = 0.0, $1114 = 0.0, $1115 = 0.0, $1116 = 0.0, $1117 = 0.0, $1118 = 0.0, $1119 = 0.0, $112 = 0.0, $1120 = 0.0, $1121 = 0.0, $1122 = 0.0;
 var $1123 = 0.0, $1124 = 0.0, $1125 = 0.0, $1126 = 0.0, $1127 = 0.0, $1128 = 0.0, $1129 = 0.0, $113 = 0.0, $1130 = 0.0, $1131 = 0.0, $1132 = 0.0, $1133 = 0.0, $1134 = 0.0, $1135 = 0.0, $1136 = 0.0, $1137 = 0.0, $1138 = 0.0, $1139 = 0.0, $114 = 0.0, $1140 = 0.0;
 var $1141 = 0.0, $1142 = 0.0, $1143 = 0.0, $1144 = 0.0, $1145 = 0.0, $1146 = 0.0, $1147 = 0.0, $1148 = 0.0, $1149 = 0.0, $115 = 0.0, $1150 = 0.0, $1151 = 0.0, $1152 = 0.0, $1153 = 0.0, $1154 = 0.0, $1155 = 0.0, $1156 = 0.0, $1157 = 0.0, $1158 = 0.0, $1159 = 0.0;
 var $116 = 0.0, $1160 = 0.0, $1161 = 0.0, $1162 = 0, $1163 = 0, $1164 = 0, $1165 = 0, $1166 = 0, $1167 = 0.0, $1168 = 0, $1169 = 0.0, $117 = 0.0, $1170 = 0, $1171 = 0, $1172 = 0.0, $1173 = 0, $1174 = 0.0, $1175 = 0, $1176 = 0, $1177 = 0.0;
 var $1178 = 0, $1179 = 0.0, $118 = 0, $1180 = 0, $1181 = 0, $1182 = 0.0, $1183 = 0, $1184 = 0.0, $1185 = 0, $1186 = 0, $1187 = 0.0, $1188 = 0, $1189 = 0.0, $119 = 0.0, $1190 = 0, $1191 = 0, $1192 = 0.0, $1193 = 0, $1194 = 0.0, $1195 = 0;
 var $1196 = 0, $1197 = 0, $1198 = 0, $1199 = 0, $12 = 0.0, $120 = 0, $1200 = 0, $1201 = 0, $1202 = 0.0, $1203 = 0, $1204 = 0.0, $1205 = 0, $1206 = 0.0, $1207 = 0.0, $1208 = 0, $1209 = 0.0, $121 = 0, $1210 = 0.0, $1211 = 0.0, $1212 = 0.0;
 var $1213 = 0.0, $1214 = 0.0, $1215 = 0, $1216 = 0.0, $1217 = 0.0, $1218 = 0, $1219 = 0.0, $122 = 0.0, $1220 = 0.0, $1221 = 0.0, $1222 = 0.0, $1223 = 0.0, $1224 = 0.0, $1225 = 0, $1226 = 0.0, $1227 = 0.0, $1228 = 0, $1229 = 0.0, $123 = 0, $1230 = 0.0;
 var $1231 = 0.0, $1232 = 0.0, $1233 = 0.0, $1234 = 0.0, $1235 = 0, $1236 = 0.0, $1237 = 0.0, $1238 = 0, $1239 = 0.0, $124 = 0, $1240 = 0.0, $1241 = 0.0, $1242 = 0.0, $1243 = 0.0, $1244 = 0.0, $1245 = 0, $1246 = 0.0, $1247 = 0.0, $1248 = 0, $1249 = 0.0;
 var $125 = 0.0, $1250 = 0.0, $1251 = 0.0, $1252 = 0.0, $1253 = 0.0, $1254 = 0.0, $1255 = 0, $1256 = 0.0, $1257 = 0.0, $1258 = 0, $1259 = 0.0, $126 = 0, $1260 = 0.0, $1261 = 0.0, $1262 = 0.0, $1263 = 0.0, $1264 = 0.0, $1265 = 0.0, $1266 = 0.0, $1267 = 0.0;
 var $1268 = 0.0, $1269 = 0.0, $127 = 0, $1270 = 0.0, $1271 = 0.0, $1272 = 0.0, $1273 = 0.0, $1274 = 0.0, $1275 = 0.0, $1276 = 0.0, $1277 = 0.0, $1278 = 0.0, $1279 = 0.0, $128 = 0, $1280 = 0.0, $1281 = 0.0, $1282 = 0.0, $1283 = 0.0, $1284 = 0.0, $1285 = 0.0;
 var $1286 = 0.0, $1287 = 0.0, $1288 = 0.0, $1289 = 0.0, $129 = 0.0, $1290 = 0.0, $1291 = 0.0, $1292 = 0.0, $1293 = 0.0, $1294 = 0.0, $1295 = 0.0, $1296 = 0.0, $1297 = 0.0, $1298 = 0.0, $1299 = 0.0, $13 = 0, $130 = 0, $1300 = 0.0, $1301 = 0.0, $1302 = 0.0;
 var $1303 = 0.0, $1304 = 0.0, $1305 = 0.0, $1306 = 0.0, $1307 = 0.0, $1308 = 0.0, $1309 = 0.0, $131 = 0, $1310 = 0.0, $1311 = 0.0, $1312 = 0.0, $1313 = 0.0, $1314 = 0.0, $1315 = 0.0, $1316 = 0.0, $1317 = 0.0, $1318 = 0.0, $1319 = 0.0, $132 = 0.0, $1320 = 0.0;
 var $1321 = 0.0, $1322 = 0.0, $1323 = 0.0, $1324 = 0.0, $1325 = 0.0, $1326 = 0.0, $1327 = 0.0, $1328 = 0.0, $1329 = 0.0, $133 = 0, $1330 = 0.0, $1331 = 0.0, $1332 = 0.0, $1333 = 0.0, $1334 = 0.0, $1335 = 0.0, $1336 = 0.0, $1337 = 0.0, $1338 = 0.0, $1339 = 0.0;
 var $134 = 0, $1340 = 0.0, $1341 = 0.0, $1342 = 0.0, $1343 = 0.0, $1344 = 0.0, $1345 = 0.0, $1346 = 0.0, $1347 = 0.0, $1348 = 0.0, $1349 = 0.0, $135 = 0, $1350 = 0.0, $1351 = 0.0, $1352 = 0.0, $1353 = 0.0, $1354 = 0.0, $1355 = 0.0, $1356 = 0.0, $1357 = 0.0;
 var $1358 = 0.0, $1359 = 0.0, $136 = 0.0, $1360 = 0.0, $1361 = 0.0, $1362 = 0.0, $1363 = 0.0, $1364 = 0.0, $1365 = 0.0, $1366 = 0.0, $1367 = 0.0, $1368 = 0.0, $1369 = 0.0, $137 = 0, $1370 = 0.0, $1371 = 0.0, $1372 = 0.0, $1373 = 0.0, $1374 = 0.0, $1375 = 0.0;
 var $1376 = 0.0, $1377 = 0.0, $1378 = 0.0, $1379 = 0.0, $138 = 0, $1380 = 0.0, $1381 = 0.0, $1382 = 0.0, $1383 = 0.0, $1384 = 0.0, $1385 = 0.0, $1386 = 0.0, $1387 = 0, $1388 = 0, $1389 = 0, $139 = 0.0, $1390 = 0, $1391 = 0, $1392 = 0, $1393 = 0;
 var $1394 = 0, $1395 = 0, $1396 = 0, $1397 = 0, $1398 = 0, $1399 = 0, $14 = 0, $140 = 0, $1400 = 0, $1401 = 0, $1402 = 0, $1403 = 0, $1404 = 0, $1405 = 0, $1406 = 0, $1407 = 0, $1408 = 0, $1409 = 0, $141 = 0, $1410 = 0;
 var $1411 = 0, $1412 = 0, $1413 = 0, $1414 = 0, $1415 = 0, $1416 = 0, $1417 = 0, $1418 = 0, $1419 = 0, $142 = 0, $1420 = 0, $1421 = 0, $1422 = 0, $1423 = 0, $1424 = 0.0, $1425 = 0, $1426 = 0.0, $1427 = 0, $1428 = 0, $1429 = 0.0;
 var $143 = 0.0, $1430 = 0, $1431 = 0.0, $1432 = 0, $1433 = 0, $1434 = 0.0, $1435 = 0, $1436 = 0.0, $1437 = 0, $1438 = 0, $1439 = 0.0, $144 = 0, $1440 = 0, $1441 = 0.0, $1442 = 0, $1443 = 0, $1444 = 0.0, $1445 = 0, $1446 = 0.0, $1447 = 0;
 var $1448 = 0, $1449 = 0.0, $145 = 0, $1450 = 0, $1451 = 0.0, $1452 = 0, $1453 = 0, $1454 = 0.0, $1455 = 0, $1456 = 0.0, $1457 = 0, $1458 = 0, $1459 = 0, $146 = 0.0, $1460 = 0, $1461 = 0, $1462 = 0, $1463 = 0, $1464 = 0, $1465 = 0.0;
 var $1466 = 0, $1467 = 0.0, $1468 = 0, $1469 = 0.0, $147 = 0, $1470 = 0.0, $1471 = 0, $1472 = 0.0, $1473 = 0.0, $1474 = 0.0, $1475 = 0.0, $1476 = 0.0, $1477 = 0.0, $1478 = 0, $1479 = 0.0, $148 = 0.0, $1480 = 0.0, $1481 = 0, $1482 = 0.0, $1483 = 0.0;
 var $1484 = 0.0, $1485 = 0.0, $1486 = 0.0, $1487 = 0.0, $1488 = 0, $1489 = 0.0, $149 = 0.0, $1490 = 0.0, $1491 = 0, $1492 = 0.0, $1493 = 0.0, $1494 = 0.0, $1495 = 0.0, $1496 = 0.0, $1497 = 0.0, $1498 = 0, $1499 = 0.0, $15 = 0.0, $150 = 0.0, $1500 = 0.0;
 var $1501 = 0, $1502 = 0.0, $1503 = 0.0, $1504 = 0.0, $1505 = 0.0, $1506 = 0.0, $1507 = 0.0, $1508 = 0, $1509 = 0.0, $151 = 0.0, $1510 = 0.0, $1511 = 0, $1512 = 0.0, $1513 = 0.0, $1514 = 0.0, $1515 = 0.0, $1516 = 0.0, $1517 = 0.0, $1518 = 0, $1519 = 0.0;
 var $152 = 0.0, $1520 = 0.0, $1521 = 0, $1522 = 0.0, $1523 = 0.0, $1524 = 0.0, $1525 = 0.0, $1526 = 0.0, $1527 = 0.0, $1528 = 0, $1529 = 0.0, $153 = 0.0, $1530 = 0.0, $1531 = 0, $1532 = 0.0, $1533 = 0.0, $1534 = 0.0, $1535 = 0.0, $1536 = 0.0, $1537 = 0.0;
 var $1538 = 0.0, $1539 = 0.0, $154 = 0.0, $1540 = 0.0, $1541 = 0.0, $1542 = 0.0, $1543 = 0.0, $1544 = 0.0, $1545 = 0.0, $1546 = 0.0, $1547 = 0.0, $1548 = 0.0, $1549 = 0.0, $155 = 0.0, $1550 = 0.0, $1551 = 0.0, $1552 = 0.0, $1553 = 0.0, $1554 = 0.0, $1555 = 0.0;
 var $1556 = 0.0, $1557 = 0.0, $1558 = 0.0, $1559 = 0.0, $156 = 0.0, $1560 = 0.0, $1561 = 0.0, $1562 = 0.0, $1563 = 0.0, $1564 = 0.0, $1565 = 0.0, $1566 = 0.0, $1567 = 0.0, $1568 = 0.0, $1569 = 0.0, $157 = 0.0, $1570 = 0.0, $1571 = 0.0, $1572 = 0.0, $1573 = 0.0;
 var $1574 = 0.0, $1575 = 0.0, $1576 = 0.0, $1577 = 0.0, $1578 = 0.0, $1579 = 0.0, $158 = 0.0, $1580 = 0.0, $1581 = 0.0, $1582 = 0.0, $1583 = 0.0, $1584 = 0.0, $1585 = 0.0, $1586 = 0.0, $1587 = 0.0, $1588 = 0.0, $1589 = 0.0, $159 = 0.0, $1590 = 0.0, $1591 = 0.0;
 var $1592 = 0.0, $1593 = 0.0, $1594 = 0.0, $1595 = 0.0, $1596 = 0.0, $1597 = 0.0, $1598 = 0.0, $1599 = 0.0, $16 = 0, $160 = 0.0, $1600 = 0.0, $1601 = 0.0, $1602 = 0.0, $1603 = 0.0, $1604 = 0.0, $1605 = 0.0, $1606 = 0.0, $1607 = 0.0, $1608 = 0.0, $1609 = 0;
 var $161 = 0.0, $1610 = 0, $1611 = 0, $1612 = 0, $1613 = 0, $1614 = 0, $1615 = 0, $1616 = 0, $1617 = 0, $1618 = 0, $1619 = 0, $162 = 0.0, $1620 = 0, $1621 = 0, $1622 = 0, $1623 = 0, $1624 = 0, $1625 = 0, $1626 = 0, $1627 = 0;
 var $1628 = 0, $1629 = 0, $163 = 0.0, $1630 = 0, $1631 = 0, $1632 = 0, $1633 = 0.0, $1634 = 0, $1635 = 0, $1636 = 0.0, $1637 = 0.0, $1638 = 0.0, $1639 = 0.0, $164 = 0.0, $1640 = 0, $1641 = 0, $1642 = 0.0, $1643 = 0, $1644 = 0, $1645 = 0;
 var $1646 = 0, $1647 = 0, $1648 = 0, $1649 = 0, $165 = 0.0, $1650 = 0, $1651 = 0, $1652 = 0, $1653 = 0.0, $1654 = 0, $1655 = 0, $1656 = 0, $1657 = 0.0, $1658 = 0.0, $1659 = 0, $166 = 0.0, $1660 = 0, $1661 = 0, $1662 = 0.0, $1663 = 0;
 var $1664 = 0, $1665 = 0, $1666 = 0, $1667 = 0.0, $1668 = 0, $1669 = 0.0, $167 = 0.0, $1670 = 0, $1671 = 0, $1672 = 0, $1673 = 0.0, $1674 = 0, $1675 = 0, $1676 = 0.0, $1677 = 0, $1678 = 0, $1679 = 0, $168 = 0.0, $1680 = 0, $1681 = 0;
 var $1682 = 0.0, $1683 = 0.0, $1684 = 0.0, $1685 = 0, $1686 = 0.0, $1687 = 0.0, $1688 = 0.0, $1689 = 0, $169 = 0, $1690 = 0.0, $1691 = 0.0, $1692 = 0.0, $1693 = 0.0, $1694 = 0.0, $1695 = 0.0, $1696 = 0.0, $1697 = 0, $1698 = 0, $1699 = 0, $17 = 0.0;
 var $170 = 0.0, $1700 = 0, $1701 = 0.0, $1702 = 0, $1703 = 0, $1704 = 0.0, $1705 = 0.0, $1706 = 0, $1707 = 0, $1708 = 0.0, $1709 = 0, $171 = 0.0, $1710 = 0.0, $1711 = 0.0, $1712 = 0, $1713 = 0, $1714 = 0.0, $1715 = 0.0, $1716 = 0.0, $1717 = 0;
 var $1718 = 0.0, $1719 = 0.0, $172 = 0.0, $1720 = 0.0, $1721 = 0, $1722 = 0, $1723 = 0.0, $1724 = 0.0, $1725 = 0, $1726 = 0.0, $1727 = 0.0, $1728 = 0, $1729 = 0.0, $173 = 0.0, $1730 = 0.0, $1731 = 0, $1732 = 0, $1733 = 0.0, $1734 = 0.0, $1735 = 0;
 var $1736 = 0, $1737 = 0, $1738 = 0, $1739 = 0, $174 = 0.0, $1740 = 0, $1741 = 0, $1742 = 0.0, $1743 = 0, $1744 = 0.0, $1745 = 0.0, $1746 = 0.0, $1747 = 0, $1748 = 0.0, $1749 = 0.0, $175 = 0.0, $1750 = 0.0, $1751 = 0, $1752 = 0.0, $1753 = 0;
 var $1754 = 0, $1755 = 0.0, $1756 = 0.0, $1757 = 0.0, $1758 = 0, $1759 = 0.0, $176 = 0.0, $1760 = 0.0, $1761 = 0.0, $1762 = 0, $1763 = 0.0, $1764 = 0.0, $1765 = 0.0, $1766 = 0, $1767 = 0, $1768 = 0, $1769 = 0, $177 = 0.0, $1770 = 0.0, $1771 = 0;
 var $1772 = 0.0, $1773 = 0, $1774 = 0, $1775 = 0, $1776 = 0, $1777 = 0.0, $1778 = 0, $1779 = 0, $178 = 0.0, $1780 = 0, $179 = 0.0, $18 = 0.0, $180 = 0.0, $181 = 0.0, $182 = 0.0, $183 = 0.0, $184 = 0.0, $185 = 0.0, $186 = 0.0, $187 = 0.0;
 var $188 = 0.0, $189 = 0.0, $19 = 0.0, $190 = 0.0, $191 = 0.0, $192 = 0.0, $193 = 0.0, $194 = 0.0, $195 = 0.0, $196 = 0.0, $197 = 0.0, $198 = 0.0, $199 = 0.0, $2 = 0, $20 = 0.0, $200 = 0.0, $201 = 0.0, $202 = 0.0, $203 = 0.0, $204 = 0.0;
 var $205 = 0.0, $206 = 0.0, $207 = 0.0, $208 = 0.0, $209 = 0.0, $21 = 0.0, $210 = 0.0, $211 = 0.0, $212 = 0.0, $213 = 0.0, $214 = 0.0, $215 = 0.0, $216 = 0.0, $217 = 0.0, $218 = 0.0, $219 = 0.0, $22 = 0.0, $220 = 0.0, $221 = 0, $222 = 0.0;
 var $223 = 0, $224 = 0, $225 = 0.0, $226 = 0, $227 = 0, $228 = 0.0, $229 = 0, $23 = 0.0, $230 = 0, $231 = 0, $232 = 0.0, $233 = 0, $234 = 0, $235 = 0.0, $236 = 0, $237 = 0, $238 = 0, $239 = 0.0, $24 = 0.0, $240 = 0;
 var $241 = 0, $242 = 0.0, $243 = 0, $244 = 0, $245 = 0, $246 = 0.0, $247 = 0, $248 = 0, $249 = 0.0, $25 = 0.0, $250 = 0, $251 = 0, $252 = 0, $253 = 0.0, $254 = 0, $255 = 0, $256 = 0.0, $257 = 0, $258 = 0, $259 = 0;
 var $26 = 0.0, $260 = 0.0, $261 = 0, $262 = 0, $263 = 0.0, $264 = 0, $265 = 0.0, $266 = 0.0, $267 = 0.0, $268 = 0.0, $269 = 0.0, $27 = 0.0, $270 = 0.0, $271 = 0.0, $272 = 0.0, $273 = 0.0, $274 = 0.0, $275 = 0.0, $276 = 0.0, $277 = 0.0;
 var $278 = 0.0, $279 = 0.0, $28 = 0, $280 = 0.0, $281 = 0.0, $282 = 0.0, $283 = 0.0, $284 = 0.0, $285 = 0.0, $286 = 0.0, $287 = 0.0, $288 = 0.0, $289 = 0.0, $29 = 0.0, $290 = 0.0, $291 = 0.0, $292 = 0.0, $293 = 0.0, $294 = 0.0, $295 = 0.0;
 var $296 = 0.0, $297 = 0.0, $298 = 0.0, $299 = 0.0, $3 = 0.0, $30 = 0, $300 = 0.0, $301 = 0.0, $302 = 0, $303 = 0.0, $304 = 0.0, $305 = 0.0, $306 = 0.0, $307 = 0.0, $308 = 0.0, $309 = 0.0, $31 = 0, $310 = 0.0, $311 = 0.0, $312 = 0.0;
 var $313 = 0.0, $314 = 0.0, $315 = 0.0, $316 = 0.0, $317 = 0.0, $318 = 0.0, $319 = 0.0, $32 = 0.0, $320 = 0.0, $321 = 0.0, $322 = 0.0, $323 = 0.0, $324 = 0.0, $325 = 0.0, $326 = 0.0, $327 = 0.0, $328 = 0.0, $329 = 0.0, $33 = 0, $330 = 0.0;
 var $331 = 0.0, $332 = 0.0, $333 = 0.0, $334 = 0.0, $335 = 0.0, $336 = 0.0, $337 = 0.0, $338 = 0.0, $339 = 0.0, $34 = 0, $340 = 0.0, $341 = 0.0, $342 = 0.0, $343 = 0.0, $344 = 0.0, $345 = 0.0, $346 = 0.0, $347 = 0.0, $348 = 0.0, $349 = 0.0;
 var $35 = 0.0, $350 = 0.0, $351 = 0.0, $352 = 0.0, $353 = 0.0, $354 = 0.0, $355 = 0.0, $356 = 0.0, $357 = 0.0, $358 = 0.0, $359 = 0.0, $36 = 0, $360 = 0.0, $361 = 0.0, $362 = 0.0, $363 = 0.0, $364 = 0.0, $365 = 0.0, $366 = 0.0, $367 = 0.0;
 var $368 = 0.0, $369 = 0.0, $37 = 0, $370 = 0.0, $371 = 0.0, $372 = 0.0, $373 = 0.0, $374 = 0.0, $375 = 0.0, $376 = 0.0, $377 = 0.0, $378 = 0.0, $379 = 0.0, $38 = 0, $380 = 0.0, $381 = 0.0, $382 = 0.0, $383 = 0.0, $384 = 0.0, $385 = 0.0;
 var $386 = 0.0, $387 = 0.0, $388 = 0.0, $389 = 0.0, $39 = 0.0, $390 = 0.0, $391 = 0.0, $392 = 0.0, $393 = 0.0, $394 = 0.0, $395 = 0.0, $396 = 0.0, $397 = 0.0, $398 = 0.0, $399 = 0.0, $4 = 0, $40 = 0, $400 = 0.0, $401 = 0.0, $402 = 0.0;
 var $403 = 0.0, $404 = 0.0, $405 = 0.0, $406 = 0.0, $407 = 0.0, $408 = 0, $409 = 0.0, $41 = 0, $410 = 0, $411 = 0, $412 = 0.0, $413 = 0, $414 = 0, $415 = 0.0, $416 = 0, $417 = 0, $418 = 0, $419 = 0.0, $42 = 0.0, $420 = 0;
 var $421 = 0, $422 = 0.0, $423 = 0, $424 = 0, $425 = 0, $426 = 0.0, $427 = 0, $428 = 0, $429 = 0.0, $43 = 0, $430 = 0, $431 = 0, $432 = 0, $433 = 0.0, $434 = 0, $435 = 0, $436 = 0.0, $437 = 0, $438 = 0, $439 = 0;
 var $44 = 0.0, $440 = 0.0, $441 = 0, $442 = 0, $443 = 0.0, $444 = 0, $445 = 0, $446 = 0, $447 = 0.0, $448 = 0, $449 = 0, $45 = 0.0, $450 = 0.0, $451 = 0, $452 = 0, $453 = 0, $454 = 0.0, $455 = 0, $456 = 0, $457 = 0.0;
 var $458 = 0, $459 = 0.0, $46 = 0.0, $460 = 0.0, $461 = 0.0, $462 = 0.0, $463 = 0.0, $464 = 0.0, $465 = 0.0, $466 = 0.0, $467 = 0.0, $468 = 0.0, $469 = 0.0, $47 = 0.0, $470 = 0.0, $471 = 0.0, $472 = 0.0, $473 = 0.0, $474 = 0.0, $475 = 0.0;
 var $476 = 0.0, $477 = 0.0, $478 = 0.0, $479 = 0.0, $48 = 0.0, $480 = 0.0, $481 = 0.0, $482 = 0.0, $483 = 0.0, $484 = 0.0, $485 = 0.0, $486 = 0.0, $487 = 0.0, $488 = 0.0, $489 = 0.0, $49 = 0.0, $490 = 0.0, $491 = 0.0, $492 = 0.0, $493 = 0.0;
 var $494 = 0.0, $495 = 0.0, $496 = 0.0, $497 = 0.0, $498 = 0.0, $499 = 0.0, $5 = 0.0, $50 = 0.0, $500 = 0.0, $501 = 0.0, $502 = 0.0, $503 = 0.0, $504 = 0.0, $505 = 0.0, $506 = 0.0, $507 = 0.0, $508 = 0.0, $509 = 0.0, $51 = 0.0, $510 = 0;
 var $511 = 0.0, $512 = 0.0, $513 = 0.0, $514 = 0.0, $515 = 0.0, $516 = 0.0, $517 = 0.0, $518 = 0.0, $519 = 0.0, $52 = 0.0, $520 = 0.0, $521 = 0.0, $522 = 0.0, $523 = 0.0, $524 = 0.0, $525 = 0.0, $526 = 0.0, $527 = 0.0, $528 = 0.0, $529 = 0.0;
 var $53 = 0.0, $530 = 0.0, $531 = 0.0, $532 = 0.0, $533 = 0.0, $534 = 0.0, $535 = 0.0, $536 = 0.0, $537 = 0.0, $538 = 0.0, $539 = 0.0, $54 = 0.0, $540 = 0.0, $541 = 0.0, $542 = 0.0, $543 = 0.0, $544 = 0.0, $545 = 0.0, $546 = 0, $547 = 0;
 var $548 = 0, $549 = 0, $55 = 0.0, $550 = 0, $551 = 0, $552 = 0, $553 = 0, $554 = 0, $555 = 0.0, $556 = 0, $557 = 0.0, $558 = 0, $559 = 0, $56 = 0.0, $560 = 0.0, $561 = 0, $562 = 0.0, $563 = 0, $564 = 0.0, $565 = 0.0;
 var $566 = 0, $567 = 0.0, $568 = 0.0, $569 = 0.0, $57 = 0.0, $570 = 0.0, $571 = 0.0, $572 = 0.0, $573 = 0.0, $574 = 0.0, $575 = 0.0, $576 = 0.0, $577 = 0, $578 = 0, $579 = 0, $58 = 0.0, $580 = 0, $581 = 0, $582 = 0, $583 = 0;
 var $584 = 0, $585 = 0, $586 = 0, $587 = 0, $588 = 0, $589 = 0, $59 = 0.0, $590 = 0.0, $591 = 0.0, $592 = 0, $593 = 0, $594 = 0, $595 = 0.0, $596 = 0, $597 = 0.0, $598 = 0, $599 = 0, $6 = 0, $60 = 0.0, $600 = 0.0;
 var $601 = 0, $602 = 0.0, $603 = 0, $604 = 0, $605 = 0, $606 = 0.0, $607 = 0, $608 = 0.0, $609 = 0, $61 = 0.0, $610 = 0.0, $611 = 0.0, $612 = 0, $613 = 0.0, $614 = 0.0, $615 = 0.0, $616 = 0.0, $617 = 0.0, $618 = 0.0, $619 = 0;
 var $62 = 0.0, $620 = 0.0, $621 = 0.0, $622 = 0, $623 = 0.0, $624 = 0.0, $625 = 0.0, $626 = 0.0, $627 = 0.0, $628 = 0.0, $629 = 0.0, $63 = 0.0, $630 = 0.0, $631 = 0.0, $632 = 0.0, $633 = 0.0, $634 = 0.0, $635 = 0.0, $636 = 0.0, $637 = 0.0;
 var $638 = 0.0, $639 = 0.0, $64 = 0.0, $640 = 0.0, $641 = 0.0, $642 = 0.0, $643 = 0.0, $644 = 0.0, $645 = 0, $646 = 0, $647 = 0, $648 = 0, $649 = 0, $65 = 0.0, $650 = 0, $651 = 0, $652 = 0, $653 = 0, $654 = 0, $655 = 0;
 var $656 = 0, $657 = 0, $658 = 0, $659 = 0, $66 = 0.0, $660 = 0.0, $661 = 0, $662 = 0.0, $663 = 0.0, $664 = 0, $665 = 0.0, $666 = 0, $667 = 0.0, $668 = 0, $669 = 0.0, $67 = 0.0, $670 = 0, $671 = 0.0, $672 = 0, $673 = 0.0;
 var $674 = 0.0, $675 = 0.0, $676 = 0.0, $677 = 0.0, $678 = 0.0, $679 = 0.0, $68 = 0, $680 = 0.0, $681 = 0.0, $682 = 0.0, $683 = 0.0, $684 = 0.0, $685 = 0.0, $686 = 0.0, $687 = 0.0, $688 = 0.0, $689 = 0.0, $69 = 0.0, $690 = 0.0, $691 = 0.0;
 var $692 = 0.0, $693 = 0, $694 = 0, $695 = 0, $696 = 0, $697 = 0, $698 = 0.0, $699 = 0, $7 = 0.0, $70 = 0, $700 = 0.0, $701 = 0, $702 = 0, $703 = 0.0, $704 = 0, $705 = 0.0, $706 = 0, $707 = 0, $708 = 0.0, $709 = 0;
 var $71 = 0, $710 = 0.0, $711 = 0, $712 = 0, $713 = 0, $714 = 0, $715 = 0.0, $716 = 0, $717 = 0.0, $718 = 0, $719 = 0.0, $72 = 0.0, $720 = 0.0, $721 = 0, $722 = 0.0, $723 = 0.0, $724 = 0.0, $725 = 0.0, $726 = 0.0, $727 = 0.0;
 var $728 = 0, $729 = 0.0, $73 = 0, $730 = 0.0, $731 = 0, $732 = 0.0, $733 = 0.0, $734 = 0.0, $735 = 0.0, $736 = 0.0, $737 = 0.0, $738 = 0, $739 = 0.0, $74 = 0, $740 = 0.0, $741 = 0, $742 = 0.0, $743 = 0.0, $744 = 0.0, $745 = 0.0;
 var $746 = 0.0, $747 = 0.0, $748 = 0.0, $749 = 0.0, $75 = 0.0, $750 = 0.0, $751 = 0.0, $752 = 0.0, $753 = 0.0, $754 = 0.0, $755 = 0.0, $756 = 0.0, $757 = 0.0, $758 = 0.0, $759 = 0.0, $76 = 0, $760 = 0.0, $761 = 0.0, $762 = 0.0, $763 = 0.0;
 var $764 = 0.0, $765 = 0.0, $766 = 0, $767 = 0, $768 = 0, $769 = 0, $77 = 0, $770 = 0, $771 = 0, $772 = 0, $773 = 0, $774 = 0, $775 = 0, $776 = 0, $777 = 0, $778 = 0, $779 = 0, $78 = 0, $780 = 0, $781 = 0;
 var $782 = 0, $783 = 0, $784 = 0, $785 = 0.0, $786 = 0, $787 = 0.0, $788 = 0.0, $789 = 0, $79 = 0.0, $790 = 0.0, $791 = 0, $792 = 0.0, $793 = 0, $794 = 0.0, $795 = 0, $796 = 0.0, $797 = 0, $798 = 0.0, $799 = 0, $8 = 0;
 var $80 = 0, $800 = 0.0, $801 = 0, $802 = 0.0, $803 = 0.0, $804 = 0.0, $805 = 0.0, $806 = 0.0, $807 = 0.0, $808 = 0.0, $809 = 0.0, $81 = 0, $810 = 0.0, $811 = 0.0, $812 = 0.0, $813 = 0.0, $814 = 0.0, $815 = 0.0, $816 = 0.0, $817 = 0.0;
 var $818 = 0.0, $819 = 0.0, $82 = 0.0, $820 = 0.0, $821 = 0.0, $822 = 0.0, $823 = 0.0, $824 = 0.0, $825 = 0.0, $826 = 0.0, $827 = 0.0, $828 = 0.0, $829 = 0.0, $83 = 0, $830 = 0.0, $831 = 0.0, $832 = 0.0, $833 = 0.0, $834 = 0.0, $835 = 0.0;
 var $836 = 0.0, $837 = 0.0, $838 = 0.0, $839 = 0.0, $84 = 0, $840 = 0.0, $841 = 0.0, $842 = 0.0, $843 = 0.0, $844 = 0.0, $845 = 0.0, $846 = 0.0, $847 = 0.0, $848 = 0.0, $849 = 0.0, $85 = 0, $850 = 0.0, $851 = 0.0, $852 = 0.0, $853 = 0.0;
 var $854 = 0.0, $855 = 0.0, $856 = 0, $857 = 0, $858 = 0, $859 = 0, $86 = 0.0, $860 = 0, $861 = 0, $862 = 0.0, $863 = 0, $864 = 0.0, $865 = 0, $866 = 0, $867 = 0.0, $868 = 0, $869 = 0.0, $87 = 0, $870 = 0, $871 = 0;
 var $872 = 0.0, $873 = 0, $874 = 0.0, $875 = 0, $876 = 0, $877 = 0.0, $878 = 0, $879 = 0.0, $88 = 0, $880 = 0, $881 = 0, $882 = 0, $883 = 0, $884 = 0, $885 = 0.0, $886 = 0, $887 = 0.0, $888 = 0, $889 = 0.0, $89 = 0.0;
 var $890 = 0.0, $891 = 0, $892 = 0.0, $893 = 0.0, $894 = 0.0, $895 = 0.0, $896 = 0.0, $897 = 0.0, $898 = 0, $899 = 0.0, $9 = 0.0, $90 = 0, $900 = 0.0, $901 = 0, $902 = 0.0, $903 = 0.0, $904 = 0.0, $905 = 0.0, $906 = 0.0, $907 = 0.0;
 var $908 = 0, $909 = 0.0, $91 = 0.0, $910 = 0.0, $911 = 0, $912 = 0.0, $913 = 0.0, $914 = 0.0, $915 = 0.0, $916 = 0.0, $917 = 0.0, $918 = 0, $919 = 0.0, $92 = 0.0, $920 = 0.0, $921 = 0, $922 = 0.0, $923 = 0.0, $924 = 0.0, $925 = 0.0;
 var $926 = 0.0, $927 = 0.0, $928 = 0.0, $929 = 0.0, $93 = 0.0, $930 = 0.0, $931 = 0.0, $932 = 0.0, $933 = 0.0, $934 = 0.0, $935 = 0.0, $936 = 0.0, $937 = 0.0, $938 = 0.0, $939 = 0.0, $94 = 0.0, $940 = 0.0, $941 = 0.0, $942 = 0.0, $943 = 0.0;
 var $944 = 0.0, $945 = 0.0, $946 = 0.0, $947 = 0.0, $948 = 0.0, $949 = 0.0, $95 = 0.0, $950 = 0.0, $951 = 0.0, $952 = 0.0, $953 = 0.0, $954 = 0.0, $955 = 0.0, $956 = 0.0, $957 = 0.0, $958 = 0.0, $959 = 0.0, $96 = 0.0, $960 = 0.0, $961 = 0.0;
 var $962 = 0.0, $963 = 0.0, $964 = 0.0, $965 = 0.0, $966 = 0.0, $967 = 0.0, $968 = 0.0, $969 = 0.0, $97 = 0.0, $970 = 0.0, $971 = 0.0, $972 = 0.0, $973 = 0.0, $974 = 0.0, $975 = 0.0, $976 = 0.0, $977 = 0.0, $978 = 0.0, $979 = 0.0, $98 = 0.0;
 var $980 = 0.0, $981 = 0.0, $982 = 0.0, $983 = 0.0, $984 = 0.0, $985 = 0.0, $986 = 0, $987 = 0, $988 = 0, $989 = 0, $99 = 0.0, $990 = 0, $991 = 0, $992 = 0, $993 = 0, $994 = 0, $995 = 0, $996 = 0, $997 = 0, $998 = 0;
 var $999 = 0, $exitcond = 0, $exitcond71 = 0, $exitcond72 = 0, $i$044 = 0, $i$141 = 0, $i$239 = 0, $i$38 = 0, $i$410 = 0, $i$512 = 0, $ind234$09 = 0, $k$048 = 0, $k127$060 = 0, $k173$063 = 0, $k233$035 = 0, $k57$051 = 0, $k68$054 = 0, $k90$057 = 0, $radix$0 = 0, $t$0 = 0;
 var $t$0$lcssa = 0, $tau5i$0 = 0.0, $tau5i$1 = 0.0, $tau5i120$0 = 0.0, $tau5i120$1 = 0.0, $tau5r$0 = 0.0, $tau5r$1 = 0.0, $tau5r119$0 = 0.0, $tau5r119$1 = 0.0, $tau7i$0 = 0.0, $tau7i$1 = 0.0, $tau7i$2 = 0.0, $tau7i166$0 = 0.0, $tau7i166$1 = 0.0, $tau7i166$2 = 0.0, $tau7i166$3 = 0.0, $tau7i166$4 = 0.0, $tau7i166$5 = 0.0, $tau7r$0 = 0.0, $tau7r$1 = 0.0;
 var $tau7r$2 = 0.0, $tau7r165$0 = 0.0, $tau7r165$1 = 0.0, $tau7r165$2 = 0.0, $tau7r165$3 = 0.0, $tau7r165$4 = 0.0, $tau7r165$5 = 0.0, $tau9i$0 = 0.0, $tau9i$1 = 0.0, $tau9i$2 = 0.0, $tau9i226$0 = 0.0, $tau9i226$1 = 0.0, $tau9i226$2 = 0.0, $tau9r$0 = 0.0, $tau9r$1 = 0.0, $tau9r$2 = 0.0, $tau9r225$0 = 0.0, $tau9r225$1 = 0.0, $tau9r225$2 = 0.0, $temp1i236$0$lcssa = 0.0;
 var $temp1i236$014 = 0.0, $temp1i236$1$lcssa = 0.0, $temp1i236$121 = 0.0, $temp1r235$0$lcssa = 0.0, $temp1r235$013 = 0.0, $temp1r235$1$lcssa = 0.0, $temp1r235$120 = 0.0, $temp2i238$0$lcssa = 0.0, $temp2i238$023 = 0.0, $temp2r237$0$lcssa = 0.0, $temp2r237$022 = 0.0, $u$031 = 0, $v$019 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($N|0)>(1);
 if ($0) {
  $1 = (((($obj)) + 8|0) + ($inc<<2)|0);
  $2 = HEAP32[$1>>2]|0;
  $radix$0 = $2;
 } else {
  $radix$0 = 0;
 }
 switch ($N|0) {
 case 8:  {
  $407 = +HEAPF64[$ip>>3];
  HEAPF64[$op>>3] = $407;
  $408 = ((($ip)) + 8|0);
  $409 = +HEAPF64[$408>>3];
  $410 = ((($op)) + 8|0);
  HEAPF64[$410>>3] = $409;
  $411 = (($ip) + ($l<<4)|0);
  $412 = +HEAPF64[$411>>3];
  $413 = ((($op)) + 16|0);
  HEAPF64[$413>>3] = $412;
  $414 = (((($ip) + ($l<<4)|0)) + 8|0);
  $415 = +HEAPF64[$414>>3];
  $416 = ((($op)) + 24|0);
  HEAPF64[$416>>3] = $415;
  $417 = $l << 1;
  $418 = (($ip) + ($417<<4)|0);
  $419 = +HEAPF64[$418>>3];
  $420 = ((($op)) + 32|0);
  HEAPF64[$420>>3] = $419;
  $421 = (((($ip) + ($417<<4)|0)) + 8|0);
  $422 = +HEAPF64[$421>>3];
  $423 = ((($op)) + 40|0);
  HEAPF64[$423>>3] = $422;
  $424 = ($l*3)|0;
  $425 = (($ip) + ($424<<4)|0);
  $426 = +HEAPF64[$425>>3];
  $427 = ((($op)) + 48|0);
  HEAPF64[$427>>3] = $426;
  $428 = (((($ip) + ($424<<4)|0)) + 8|0);
  $429 = +HEAPF64[$428>>3];
  $430 = ((($op)) + 56|0);
  HEAPF64[$430>>3] = $429;
  $431 = $l << 2;
  $432 = (($ip) + ($431<<4)|0);
  $433 = +HEAPF64[$432>>3];
  $434 = ((($op)) + 64|0);
  HEAPF64[$434>>3] = $433;
  $435 = (((($ip) + ($431<<4)|0)) + 8|0);
  $436 = +HEAPF64[$435>>3];
  $437 = ((($op)) + 72|0);
  HEAPF64[$437>>3] = $436;
  $438 = ($l*5)|0;
  $439 = (($ip) + ($438<<4)|0);
  $440 = +HEAPF64[$439>>3];
  $441 = ((($op)) + 80|0);
  HEAPF64[$441>>3] = $440;
  $442 = (((($ip) + ($438<<4)|0)) + 8|0);
  $443 = +HEAPF64[$442>>3];
  $444 = ((($op)) + 88|0);
  HEAPF64[$444>>3] = $443;
  $445 = ($l*6)|0;
  $446 = (($ip) + ($445<<4)|0);
  $447 = +HEAPF64[$446>>3];
  $448 = ((($op)) + 96|0);
  HEAPF64[$448>>3] = $447;
  $449 = (((($ip) + ($445<<4)|0)) + 8|0);
  $450 = +HEAPF64[$449>>3];
  $451 = ((($op)) + 104|0);
  HEAPF64[$451>>3] = $450;
  $452 = ($l*7)|0;
  $453 = (($ip) + ($452<<4)|0);
  $454 = +HEAPF64[$453>>3];
  $455 = ((($op)) + 112|0);
  HEAPF64[$455>>3] = $454;
  $456 = (((($ip) + ($452<<4)|0)) + 8|0);
  $457 = +HEAPF64[$456>>3];
  $458 = ((($op)) + 120|0);
  HEAPF64[$458>>3] = $457;
  $459 = +HEAPF64[$op>>3];
  $460 = +HEAPF64[$434>>3];
  $461 = $459 + $460;
  $462 = $459 - $460;
  $463 = +HEAPF64[$410>>3];
  $464 = +HEAPF64[$437>>3];
  $465 = $463 + $464;
  $466 = $463 - $464;
  $467 = +HEAPF64[$413>>3];
  $468 = +HEAPF64[$455>>3];
  $469 = $467 + $468;
  $470 = $467 - $468;
  $471 = +HEAPF64[$416>>3];
  $472 = $457 + $471;
  $473 = $471 - $457;
  $474 = +HEAPF64[$427>>3];
  $475 = +HEAPF64[$441>>3];
  $476 = $474 + $475;
  $477 = $474 - $475;
  $478 = +HEAPF64[$430>>3];
  $479 = +HEAPF64[$444>>3];
  $480 = $478 + $479;
  $481 = $478 - $479;
  $482 = +HEAPF64[$420>>3];
  $483 = +HEAPF64[$448>>3];
  $484 = $482 + $483;
  $485 = $482 - $483;
  $486 = +HEAPF64[$423>>3];
  $487 = +HEAPF64[$451>>3];
  $488 = $486 + $487;
  $489 = $486 - $487;
  $490 = $461 + $469;
  $491 = $490 + $476;
  $492 = $491 + $484;
  HEAPF64[$op>>3] = $492;
  $493 = $465 + $472;
  $494 = $493 + $480;
  $495 = $494 + $488;
  HEAPF64[$410>>3] = $495;
  $496 = $461 - $469;
  $497 = $496 - $476;
  $498 = $497 + $484;
  HEAPF64[$434>>3] = $498;
  $499 = $465 - $472;
  $500 = $499 - $480;
  $501 = $500 + $488;
  HEAPF64[$437>>3] = $501;
  $502 = $469 - $476;
  $503 = $472 - $480;
  $504 = $470 + $477;
  $505 = $473 + $481;
  $506 = $502 * 0.70710678118654757;
  $507 = $462 + $506;
  $508 = $503 * 0.70710678118654757;
  $509 = $466 + $508;
  $510 = ($sgn|0)==(1);
  if ($510) {
   $511 = $504 * -0.70710678118654757;
   $512 = $511 - $485;
   $513 = $505 * -0.70710678118654757;
   $514 = $513 - $489;
   $tau9i$0 = $514;$tau9r$0 = $512;
  } else {
   $515 = $504 * 0.70710678118654757;
   $516 = $515 + $485;
   $517 = $505 * 0.70710678118654757;
   $518 = $517 + $489;
   $tau9i$0 = $518;$tau9r$0 = $516;
  }
  $519 = $507 - $tau9i$0;
  HEAPF64[$413>>3] = $519;
  $520 = $509 + $tau9r$0;
  HEAPF64[$416>>3] = $520;
  $521 = $507 + $tau9i$0;
  HEAPF64[$455>>3] = $521;
  $522 = $509 - $tau9r$0;
  HEAPF64[$458>>3] = $522;
  $523 = $461 - $484;
  $524 = $465 - $488;
  $525 = $477 - $470;
  $526 = $481 - $473;
  $527 = $470 - $477;
  $528 = $473 - $481;
  $tau9i$1 = $510 ? $526 : $528;
  $tau9r$1 = $510 ? $525 : $527;
  $529 = $523 - $tau9i$1;
  HEAPF64[$420>>3] = $529;
  $530 = $tau9r$1 + $524;
  HEAPF64[$423>>3] = $530;
  $531 = $tau9i$1 + $523;
  HEAPF64[$448>>3] = $531;
  $532 = $524 - $tau9r$1;
  HEAPF64[$451>>3] = $532;
  $533 = $462 - $506;
  $534 = $466 - $508;
  $535 = $504 * 0.70710678118654757;
  if ($510) {
   $536 = $485 - $535;
   $537 = $505 * 0.70710678118654757;
   $538 = $489 - $537;
   $tau9i$2 = $538;$tau9r$2 = $536;
  } else {
   $539 = $535 - $485;
   $540 = $505 * 0.70710678118654757;
   $541 = $540 - $489;
   $tau9i$2 = $541;$tau9r$2 = $539;
  }
  $542 = $533 - $tau9i$2;
  HEAPF64[$427>>3] = $542;
  $543 = $534 + $tau9r$2;
  HEAPF64[$430>>3] = $543;
  $544 = $533 + $tau9i$2;
  HEAPF64[$441>>3] = $544;
  $545 = $534 - $tau9r$2;
  HEAPF64[$444>>3] = $545;
  return;
  break;
 }
 case 5:  {
  $117 = +HEAPF64[$ip>>3];
  HEAPF64[$op>>3] = $117;
  $118 = ((($ip)) + 8|0);
  $119 = +HEAPF64[$118>>3];
  $120 = ((($op)) + 8|0);
  HEAPF64[$120>>3] = $119;
  $121 = (($ip) + ($l<<4)|0);
  $122 = +HEAPF64[$121>>3];
  $123 = ((($op)) + 16|0);
  HEAPF64[$123>>3] = $122;
  $124 = (((($ip) + ($l<<4)|0)) + 8|0);
  $125 = +HEAPF64[$124>>3];
  $126 = ((($op)) + 24|0);
  HEAPF64[$126>>3] = $125;
  $127 = $l << 1;
  $128 = (($ip) + ($127<<4)|0);
  $129 = +HEAPF64[$128>>3];
  $130 = ((($op)) + 32|0);
  HEAPF64[$130>>3] = $129;
  $131 = (((($ip) + ($127<<4)|0)) + 8|0);
  $132 = +HEAPF64[$131>>3];
  $133 = ((($op)) + 40|0);
  HEAPF64[$133>>3] = $132;
  $134 = ($l*3)|0;
  $135 = (($ip) + ($134<<4)|0);
  $136 = +HEAPF64[$135>>3];
  $137 = ((($op)) + 48|0);
  HEAPF64[$137>>3] = $136;
  $138 = (((($ip) + ($134<<4)|0)) + 8|0);
  $139 = +HEAPF64[$138>>3];
  $140 = ((($op)) + 56|0);
  HEAPF64[$140>>3] = $139;
  $141 = $l << 2;
  $142 = (($ip) + ($141<<4)|0);
  $143 = +HEAPF64[$142>>3];
  $144 = ((($op)) + 64|0);
  HEAPF64[$144>>3] = $143;
  $145 = (((($ip) + ($141<<4)|0)) + 8|0);
  $146 = +HEAPF64[$145>>3];
  $147 = ((($op)) + 72|0);
  HEAPF64[$147>>3] = $146;
  $148 = +HEAPF64[$123>>3];
  $149 = +HEAPF64[$144>>3];
  $150 = $148 + $149;
  $151 = $148 - $149;
  $152 = +HEAPF64[$126>>3];
  $153 = $146 + $152;
  $154 = $152 - $146;
  $155 = +HEAPF64[$130>>3];
  $156 = +HEAPF64[$137>>3];
  $157 = $155 + $156;
  $158 = $155 - $156;
  $159 = +HEAPF64[$133>>3];
  $160 = +HEAPF64[$140>>3];
  $161 = $159 + $160;
  $162 = $159 - $160;
  $163 = $150 * 0.30901699437000002;
  $164 = $157 * 0.80901699436999996;
  $165 = $163 - $164;
  $166 = $153 * 0.30901699437000002;
  $167 = $161 * 0.80901699436999996;
  $168 = $166 - $167;
  $169 = ($sgn|0)==(1);
  if ($169) {
   $170 = $151 * 0.95105651628999998;
   $171 = $158 * 0.58778525229;
   $172 = $170 + $171;
   $173 = $154 * 0.95105651628999998;
   $174 = $162 * 0.58778525229;
   $175 = $173 + $174;
   $tau5i$0 = $175;$tau5r$0 = $172;
  } else {
   $176 = $151 * -0.95105651628999998;
   $177 = $158 * 0.58778525229;
   $178 = $176 - $177;
   $179 = $154 * -0.95105651628999998;
   $180 = $162 * 0.58778525229;
   $181 = $179 - $180;
   $tau5i$0 = $181;$tau5r$0 = $178;
  }
  $182 = +HEAPF64[$op>>3];
  $183 = $165 + $182;
  $184 = +HEAPF64[$120>>3];
  $185 = $168 + $184;
  $186 = $tau5i$0 + $183;
  HEAPF64[$123>>3] = $186;
  $187 = $185 - $tau5r$0;
  HEAPF64[$126>>3] = $187;
  $188 = $183 - $tau5i$0;
  HEAPF64[$144>>3] = $188;
  $189 = $tau5r$0 + $185;
  HEAPF64[$147>>3] = $189;
  $190 = $150 * 0.80901699436999996;
  $191 = $157 * 0.30901699437000002;
  $192 = $191 - $190;
  $193 = $153 * 0.80901699436999996;
  $194 = $161 * 0.30901699437000002;
  $195 = $194 - $193;
  $196 = $151 * 0.58778525229;
  $197 = $158 * 0.95105651628999998;
  if ($169) {
   $198 = $196 - $197;
   $199 = $154 * 0.58778525229;
   $200 = $162 * 0.95105651628999998;
   $201 = $199 - $200;
   $tau5i$1 = $201;$tau5r$1 = $198;
  } else {
   $202 = $197 - $196;
   $203 = $154 * 0.58778525229;
   $204 = $162 * 0.95105651628999998;
   $205 = $204 - $203;
   $tau5i$1 = $205;$tau5r$1 = $202;
  }
  $206 = +HEAPF64[$op>>3];
  $207 = $192 + $206;
  $208 = +HEAPF64[$120>>3];
  $209 = $195 + $208;
  $210 = $tau5i$1 + $207;
  HEAPF64[$130>>3] = $210;
  $211 = $209 - $tau5r$1;
  HEAPF64[$133>>3] = $211;
  $212 = $207 - $tau5i$1;
  HEAPF64[$137>>3] = $212;
  $213 = $tau5r$1 + $209;
  HEAPF64[$140>>3] = $213;
  $214 = $150 + $157;
  $215 = +HEAPF64[$op>>3];
  $216 = $214 + $215;
  HEAPF64[$op>>3] = $216;
  $217 = $153 + $161;
  $218 = +HEAPF64[$120>>3];
  $219 = $217 + $218;
  HEAPF64[$120>>3] = $219;
  return;
  break;
 }
 case 1:  {
  $3 = +HEAPF64[$ip>>3];
  HEAPF64[$op>>3] = $3;
  $4 = ((($ip)) + 8|0);
  $5 = +HEAPF64[$4>>3];
  $6 = ((($op)) + 8|0);
  HEAPF64[$6>>3] = $5;
  return;
  break;
 }
 case 2:  {
  $7 = +HEAPF64[$ip>>3];
  HEAPF64[$op>>3] = $7;
  $8 = ((($ip)) + 8|0);
  $9 = +HEAPF64[$8>>3];
  $10 = ((($op)) + 8|0);
  HEAPF64[$10>>3] = $9;
  $11 = (($ip) + ($l<<4)|0);
  $12 = +HEAPF64[$11>>3];
  $13 = ((($op)) + 16|0);
  HEAPF64[$13>>3] = $12;
  $14 = (((($ip) + ($l<<4)|0)) + 8|0);
  $15 = +HEAPF64[$14>>3];
  $16 = ((($op)) + 24|0);
  HEAPF64[$16>>3] = $15;
  $17 = +HEAPF64[$op>>3];
  $18 = +HEAPF64[$10>>3];
  $19 = +HEAPF64[$13>>3];
  $20 = $17 + $19;
  HEAPF64[$op>>3] = $20;
  $21 = +HEAPF64[$16>>3];
  $22 = $18 + $21;
  HEAPF64[$10>>3] = $22;
  $23 = +HEAPF64[$13>>3];
  $24 = $17 - $23;
  HEAPF64[$13>>3] = $24;
  $25 = +HEAPF64[$16>>3];
  $26 = $18 - $25;
  HEAPF64[$16>>3] = $26;
  return;
  break;
 }
 case 3:  {
  $27 = +HEAPF64[$ip>>3];
  HEAPF64[$op>>3] = $27;
  $28 = ((($ip)) + 8|0);
  $29 = +HEAPF64[$28>>3];
  $30 = ((($op)) + 8|0);
  HEAPF64[$30>>3] = $29;
  $31 = (($ip) + ($l<<4)|0);
  $32 = +HEAPF64[$31>>3];
  $33 = ((($op)) + 16|0);
  HEAPF64[$33>>3] = $32;
  $34 = (((($ip) + ($l<<4)|0)) + 8|0);
  $35 = +HEAPF64[$34>>3];
  $36 = ((($op)) + 24|0);
  HEAPF64[$36>>3] = $35;
  $37 = $l << 1;
  $38 = (($ip) + ($37<<4)|0);
  $39 = +HEAPF64[$38>>3];
  $40 = ((($op)) + 32|0);
  HEAPF64[$40>>3] = $39;
  $41 = (((($ip) + ($37<<4)|0)) + 8|0);
  $42 = +HEAPF64[$41>>3];
  $43 = ((($op)) + 40|0);
  $44 = +HEAPF64[$33>>3];
  $45 = $44 + $39;
  $46 = +HEAPF64[$36>>3];
  $47 = $42 + $46;
  $48 = (+($sgn|0));
  $49 = $48 * 0.86602540378000004;
  $50 = $44 - $39;
  $51 = $49 * $50;
  $52 = $46 - $42;
  $53 = $49 * $52;
  $54 = +HEAPF64[$op>>3];
  $55 = $45 * 0.5;
  $56 = $54 - $55;
  $57 = +HEAPF64[$30>>3];
  $58 = $47 * 0.5;
  $59 = $57 - $58;
  $60 = $45 + $54;
  HEAPF64[$op>>3] = $60;
  $61 = +HEAPF64[$30>>3];
  $62 = $47 + $61;
  HEAPF64[$30>>3] = $62;
  $63 = $56 + $53;
  HEAPF64[$33>>3] = $63;
  $64 = $59 - $51;
  HEAPF64[$36>>3] = $64;
  $65 = $56 - $53;
  HEAPF64[$40>>3] = $65;
  $66 = $51 + $59;
  HEAPF64[$43>>3] = $66;
  return;
  break;
 }
 case 4:  {
  $67 = +HEAPF64[$ip>>3];
  HEAPF64[$op>>3] = $67;
  $68 = ((($ip)) + 8|0);
  $69 = +HEAPF64[$68>>3];
  $70 = ((($op)) + 8|0);
  HEAPF64[$70>>3] = $69;
  $71 = (($ip) + ($l<<4)|0);
  $72 = +HEAPF64[$71>>3];
  $73 = ((($op)) + 16|0);
  HEAPF64[$73>>3] = $72;
  $74 = (((($ip) + ($l<<4)|0)) + 8|0);
  $75 = +HEAPF64[$74>>3];
  $76 = ((($op)) + 24|0);
  HEAPF64[$76>>3] = $75;
  $77 = $l << 1;
  $78 = (($ip) + ($77<<4)|0);
  $79 = +HEAPF64[$78>>3];
  $80 = ((($op)) + 32|0);
  HEAPF64[$80>>3] = $79;
  $81 = (((($ip) + ($77<<4)|0)) + 8|0);
  $82 = +HEAPF64[$81>>3];
  $83 = ((($op)) + 40|0);
  HEAPF64[$83>>3] = $82;
  $84 = ($l*3)|0;
  $85 = (($ip) + ($84<<4)|0);
  $86 = +HEAPF64[$85>>3];
  $87 = ((($op)) + 48|0);
  HEAPF64[$87>>3] = $86;
  $88 = (((($ip) + ($84<<4)|0)) + 8|0);
  $89 = +HEAPF64[$88>>3];
  $90 = ((($op)) + 56|0);
  $91 = +HEAPF64[$op>>3];
  $92 = +HEAPF64[$80>>3];
  $93 = $91 + $92;
  $94 = +HEAPF64[$70>>3];
  $95 = +HEAPF64[$83>>3];
  $96 = $94 + $95;
  $97 = $91 - $92;
  $98 = $94 - $95;
  $99 = +HEAPF64[$73>>3];
  $100 = +HEAPF64[$87>>3];
  $101 = $99 + $100;
  $102 = +HEAPF64[$76>>3];
  $103 = $89 + $102;
  $104 = (+($sgn|0));
  $105 = $99 - $100;
  $106 = $104 * $105;
  $107 = $102 - $89;
  $108 = $104 * $107;
  $109 = $93 + $101;
  HEAPF64[$op>>3] = $109;
  $110 = $96 + $103;
  HEAPF64[$70>>3] = $110;
  $111 = $97 + $108;
  HEAPF64[$73>>3] = $111;
  $112 = $98 - $106;
  HEAPF64[$76>>3] = $112;
  $113 = $93 - $101;
  HEAPF64[$80>>3] = $113;
  $114 = $96 - $103;
  HEAPF64[$83>>3] = $114;
  $115 = $97 - $108;
  HEAPF64[$87>>3] = $115;
  $116 = $98 + $106;
  HEAPF64[$90>>3] = $116;
  return;
  break;
 }
 case 7:  {
  $220 = +HEAPF64[$ip>>3];
  HEAPF64[$op>>3] = $220;
  $221 = ((($ip)) + 8|0);
  $222 = +HEAPF64[$221>>3];
  $223 = ((($op)) + 8|0);
  HEAPF64[$223>>3] = $222;
  $224 = (($ip) + ($l<<4)|0);
  $225 = +HEAPF64[$224>>3];
  $226 = ((($op)) + 16|0);
  HEAPF64[$226>>3] = $225;
  $227 = (((($ip) + ($l<<4)|0)) + 8|0);
  $228 = +HEAPF64[$227>>3];
  $229 = ((($op)) + 24|0);
  HEAPF64[$229>>3] = $228;
  $230 = $l << 1;
  $231 = (($ip) + ($230<<4)|0);
  $232 = +HEAPF64[$231>>3];
  $233 = ((($op)) + 32|0);
  HEAPF64[$233>>3] = $232;
  $234 = (((($ip) + ($230<<4)|0)) + 8|0);
  $235 = +HEAPF64[$234>>3];
  $236 = ((($op)) + 40|0);
  HEAPF64[$236>>3] = $235;
  $237 = ($l*3)|0;
  $238 = (($ip) + ($237<<4)|0);
  $239 = +HEAPF64[$238>>3];
  $240 = ((($op)) + 48|0);
  HEAPF64[$240>>3] = $239;
  $241 = (((($ip) + ($237<<4)|0)) + 8|0);
  $242 = +HEAPF64[$241>>3];
  $243 = ((($op)) + 56|0);
  HEAPF64[$243>>3] = $242;
  $244 = $l << 2;
  $245 = (($ip) + ($244<<4)|0);
  $246 = +HEAPF64[$245>>3];
  $247 = ((($op)) + 64|0);
  HEAPF64[$247>>3] = $246;
  $248 = (((($ip) + ($244<<4)|0)) + 8|0);
  $249 = +HEAPF64[$248>>3];
  $250 = ((($op)) + 72|0);
  HEAPF64[$250>>3] = $249;
  $251 = ($l*5)|0;
  $252 = (($ip) + ($251<<4)|0);
  $253 = +HEAPF64[$252>>3];
  $254 = ((($op)) + 80|0);
  HEAPF64[$254>>3] = $253;
  $255 = (((($ip) + ($251<<4)|0)) + 8|0);
  $256 = +HEAPF64[$255>>3];
  $257 = ((($op)) + 88|0);
  HEAPF64[$257>>3] = $256;
  $258 = ($l*6)|0;
  $259 = (($ip) + ($258<<4)|0);
  $260 = +HEAPF64[$259>>3];
  $261 = ((($op)) + 96|0);
  HEAPF64[$261>>3] = $260;
  $262 = (((($ip) + ($258<<4)|0)) + 8|0);
  $263 = +HEAPF64[$262>>3];
  $264 = ((($op)) + 104|0);
  HEAPF64[$264>>3] = $263;
  $265 = +HEAPF64[$226>>3];
  $266 = +HEAPF64[$261>>3];
  $267 = $265 + $266;
  $268 = $265 - $266;
  $269 = +HEAPF64[$229>>3];
  $270 = $263 + $269;
  $271 = $269 - $263;
  $272 = +HEAPF64[$233>>3];
  $273 = +HEAPF64[$254>>3];
  $274 = $272 + $273;
  $275 = $272 - $273;
  $276 = +HEAPF64[$236>>3];
  $277 = +HEAPF64[$257>>3];
  $278 = $276 + $277;
  $279 = $276 - $277;
  $280 = +HEAPF64[$240>>3];
  $281 = +HEAPF64[$247>>3];
  $282 = $280 + $281;
  $283 = $280 - $281;
  $284 = +HEAPF64[$243>>3];
  $285 = +HEAPF64[$250>>3];
  $286 = $284 + $285;
  $287 = $284 - $285;
  $288 = +HEAPF64[$op>>3];
  $289 = $267 * 0.62348980185000002;
  $290 = $289 + $288;
  $291 = $274 * 0.22252093395;
  $292 = $290 - $291;
  $293 = $282 * 0.90096886789999997;
  $294 = $292 - $293;
  $295 = +HEAPF64[$223>>3];
  $296 = $270 * 0.62348980185000002;
  $297 = $296 + $295;
  $298 = $278 * 0.22252093395;
  $299 = $297 - $298;
  $300 = $286 * 0.90096886789999997;
  $301 = $299 - $300;
  $302 = ($sgn|0)==(1);
  if ($302) {
   $303 = $268 * -0.78183148246;
   $304 = $275 * 0.97492791217999996;
   $305 = $303 - $304;
   $306 = $283 * 0.43388373911;
   $307 = $305 - $306;
   $308 = $271 * -0.78183148246;
   $309 = $279 * 0.97492791217999996;
   $310 = $308 - $309;
   $311 = $287 * 0.43388373911;
   $312 = $310 - $311;
   $tau7i$0 = $312;$tau7r$0 = $307;
  } else {
   $313 = $268 * 0.78183148246;
   $314 = $275 * 0.97492791217999996;
   $315 = $313 + $314;
   $316 = $283 * 0.43388373911;
   $317 = $315 + $316;
   $318 = $271 * 0.78183148246;
   $319 = $279 * 0.97492791217999996;
   $320 = $318 + $319;
   $321 = $287 * 0.43388373911;
   $322 = $320 + $321;
   $tau7i$0 = $322;$tau7r$0 = $317;
  }
  $323 = $294 - $tau7i$0;
  HEAPF64[$226>>3] = $323;
  $324 = $294 + $tau7i$0;
  HEAPF64[$261>>3] = $324;
  $325 = $301 + $tau7r$0;
  HEAPF64[$229>>3] = $325;
  $326 = $301 - $tau7r$0;
  HEAPF64[$264>>3] = $326;
  $327 = +HEAPF64[$op>>3];
  $328 = $267 * 0.22252093395;
  $329 = $327 - $328;
  $330 = $274 * 0.90096886789999997;
  $331 = $329 - $330;
  $332 = $282 * 0.62348980185000002;
  $333 = $332 + $331;
  $334 = +HEAPF64[$223>>3];
  $335 = $270 * 0.22252093395;
  $336 = $334 - $335;
  $337 = $278 * 0.90096886789999997;
  $338 = $336 - $337;
  $339 = $286 * 0.62348980185000002;
  $340 = $339 + $338;
  $341 = $268 * 0.97492791217999996;
  $342 = $275 * 0.43388373911;
  if ($302) {
   $343 = $342 - $341;
   $344 = $283 * 0.78183148246;
   $345 = $343 + $344;
   $346 = $271 * 0.97492791217999996;
   $347 = $279 * 0.43388373911;
   $348 = $347 - $346;
   $349 = $287 * 0.78183148246;
   $350 = $348 + $349;
   $tau7i$1 = $350;$tau7r$1 = $345;
  } else {
   $351 = $341 - $342;
   $352 = $283 * 0.78183148246;
   $353 = $351 - $352;
   $354 = $271 * 0.97492791217999996;
   $355 = $279 * 0.43388373911;
   $356 = $354 - $355;
   $357 = $287 * 0.78183148246;
   $358 = $356 - $357;
   $tau7i$1 = $358;$tau7r$1 = $353;
  }
  $359 = $333 - $tau7i$1;
  HEAPF64[$233>>3] = $359;
  $360 = $333 + $tau7i$1;
  HEAPF64[$254>>3] = $360;
  $361 = $340 + $tau7r$1;
  HEAPF64[$236>>3] = $361;
  $362 = $340 - $tau7r$1;
  HEAPF64[$257>>3] = $362;
  $363 = +HEAPF64[$op>>3];
  $364 = $267 * 0.90096886789999997;
  $365 = $363 - $364;
  $366 = $274 * 0.62348980185000002;
  $367 = $366 + $365;
  $368 = $282 * 0.22252093395;
  $369 = $367 - $368;
  $370 = +HEAPF64[$223>>3];
  $371 = $270 * 0.90096886789999997;
  $372 = $370 - $371;
  $373 = $278 * 0.62348980185000002;
  $374 = $373 + $372;
  $375 = $286 * 0.22252093395;
  $376 = $374 - $375;
  $377 = $268 * 0.43388373911;
  $378 = $275 * 0.78183148246;
  if ($302) {
   $379 = $378 - $377;
   $380 = $283 * 0.97492791217999996;
   $381 = $379 - $380;
   $382 = $271 * 0.43388373911;
   $383 = $279 * 0.78183148246;
   $384 = $383 - $382;
   $385 = $287 * 0.97492791217999996;
   $386 = $384 - $385;
   $tau7i$2 = $386;$tau7r$2 = $381;
  } else {
   $387 = $377 - $378;
   $388 = $283 * 0.97492791217999996;
   $389 = $387 + $388;
   $390 = $271 * 0.43388373911;
   $391 = $279 * 0.78183148246;
   $392 = $390 - $391;
   $393 = $287 * 0.97492791217999996;
   $394 = $392 + $393;
   $tau7i$2 = $394;$tau7r$2 = $389;
  }
  $395 = $369 - $tau7i$2;
  HEAPF64[$240>>3] = $395;
  $396 = $369 + $tau7i$2;
  HEAPF64[$247>>3] = $396;
  $397 = $376 + $tau7r$2;
  HEAPF64[$243>>3] = $397;
  $398 = $376 - $tau7r$2;
  HEAPF64[$250>>3] = $398;
  $399 = $267 + $274;
  $400 = $399 + $282;
  $401 = +HEAPF64[$op>>3];
  $402 = $400 + $401;
  HEAPF64[$op>>3] = $402;
  $403 = $270 + $278;
  $404 = $403 + $286;
  $405 = +HEAPF64[$223>>3];
  $406 = $404 + $405;
  HEAPF64[$223>>3] = $406;
  return;
  break;
 }
 default: {
  switch ($radix$0|0) {
  case 2:  {
   $546 = (($N|0) / 2)&-1;
   $547 = $l << 1;
   $548 = (($inc) + 1)|0;
   _mixed_radix_dit_rec($op,$ip,$obj,$sgn,$546,$547,$548);
   $549 = (($op) + ($546<<4)|0);
   $550 = (($ip) + ($l<<4)|0);
   _mixed_radix_dit_rec($549,$550,$obj,$sgn,$546,$547,$548);
   $551 = ($N|0)>(1);
   if (!($551)) {
    return;
   }
   $552 = (($546) + -1)|0;
   $k$048 = 0;
   while(1) {
    $553 = (($552) + ($k$048))|0;
    $554 = (((($obj)) + 272|0) + ($553<<4)|0);
    $555 = +HEAPF64[$554>>3];
    $556 = (((((($obj)) + 272|0) + ($553<<4)|0)) + 8|0);
    $557 = +HEAPF64[$556>>3];
    $558 = (($k$048) + ($546))|0;
    $559 = (($op) + ($k$048<<4)|0);
    $560 = +HEAPF64[$559>>3];
    $561 = (((($op) + ($k$048<<4)|0)) + 8|0);
    $562 = +HEAPF64[$561>>3];
    $563 = (($op) + ($558<<4)|0);
    $564 = +HEAPF64[$563>>3];
    $565 = $555 * $564;
    $566 = (((($op) + ($558<<4)|0)) + 8|0);
    $567 = +HEAPF64[$566>>3];
    $568 = $557 * $567;
    $569 = $565 - $568;
    $570 = $555 * $567;
    $571 = $557 * $564;
    $572 = $571 + $570;
    $573 = $560 + $569;
    HEAPF64[$559>>3] = $573;
    $574 = $562 + $572;
    HEAPF64[$561>>3] = $574;
    $575 = $560 - $569;
    HEAPF64[$563>>3] = $575;
    $576 = $562 - $572;
    HEAPF64[$566>>3] = $576;
    $577 = (($k$048) + 1)|0;
    $578 = ($577|0)<($546|0);
    if ($578) {
     $k$048 = $577;
    } else {
     break;
    }
   }
   return;
   break;
  }
  case 3:  {
   $579 = (($N|0) / 3)&-1;
   $580 = ($l*3)|0;
   $581 = (($inc) + 1)|0;
   _mixed_radix_dit_rec($op,$ip,$obj,$sgn,$579,$580,$581);
   $582 = (($op) + ($579<<4)|0);
   $583 = (($ip) + ($l<<4)|0);
   _mixed_radix_dit_rec($582,$583,$obj,$sgn,$579,$580,$581);
   $584 = $579 << 1;
   $585 = (($op) + ($584<<4)|0);
   $586 = $l << 1;
   $587 = (($ip) + ($586<<4)|0);
   _mixed_radix_dit_rec($585,$587,$obj,$sgn,$579,$580,$581);
   $588 = ($N|0)>(2);
   if (!($588)) {
    return;
   }
   $589 = (($579) + -1)|0;
   $590 = (+($sgn|0));
   $591 = $590 * 0.86602540378000004;
   $k57$051 = 0;
   while(1) {
    $592 = $k57$051 << 1;
    $593 = (($589) + ($592))|0;
    $594 = (((($obj)) + 272|0) + ($593<<4)|0);
    $595 = +HEAPF64[$594>>3];
    $596 = (((((($obj)) + 272|0) + ($593<<4)|0)) + 8|0);
    $597 = +HEAPF64[$596>>3];
    $598 = (($592) + ($579))|0;
    $599 = (((($obj)) + 272|0) + ($598<<4)|0);
    $600 = +HEAPF64[$599>>3];
    $601 = (((((($obj)) + 272|0) + ($598<<4)|0)) + 8|0);
    $602 = +HEAPF64[$601>>3];
    $603 = (($k57$051) + ($579))|0;
    $604 = (($603) + ($579))|0;
    $605 = (($op) + ($k57$051<<4)|0);
    $606 = +HEAPF64[$605>>3];
    $607 = (((($op) + ($k57$051<<4)|0)) + 8|0);
    $608 = +HEAPF64[$607>>3];
    $609 = (($op) + ($603<<4)|0);
    $610 = +HEAPF64[$609>>3];
    $611 = $595 * $610;
    $612 = (((($op) + ($603<<4)|0)) + 8|0);
    $613 = +HEAPF64[$612>>3];
    $614 = $597 * $613;
    $615 = $611 - $614;
    $616 = $595 * $613;
    $617 = $597 * $610;
    $618 = $617 + $616;
    $619 = (($op) + ($604<<4)|0);
    $620 = +HEAPF64[$619>>3];
    $621 = $600 * $620;
    $622 = (((($op) + ($604<<4)|0)) + 8|0);
    $623 = +HEAPF64[$622>>3];
    $624 = $602 * $623;
    $625 = $621 - $624;
    $626 = $600 * $623;
    $627 = $602 * $620;
    $628 = $627 + $626;
    $629 = $615 + $625;
    $630 = $618 + $628;
    $631 = $615 - $625;
    $632 = $591 * $631;
    $633 = $618 - $628;
    $634 = $591 * $633;
    $635 = $629 * 0.5;
    $636 = $606 - $635;
    $637 = $630 * 0.5;
    $638 = $608 - $637;
    $639 = $606 + $629;
    HEAPF64[$605>>3] = $639;
    $640 = $608 + $630;
    HEAPF64[$607>>3] = $640;
    $641 = $634 + $636;
    HEAPF64[$609>>3] = $641;
    $642 = $638 - $632;
    HEAPF64[$612>>3] = $642;
    $643 = $636 - $634;
    HEAPF64[$619>>3] = $643;
    $644 = $632 + $638;
    HEAPF64[$622>>3] = $644;
    $645 = (($k57$051) + 1)|0;
    $646 = ($645|0)<($579|0);
    if ($646) {
     $k57$051 = $645;
    } else {
     break;
    }
   }
   return;
   break;
  }
  case 4:  {
   $647 = (($N|0) / 4)&-1;
   $648 = $l << 2;
   $649 = (($inc) + 1)|0;
   _mixed_radix_dit_rec($op,$ip,$obj,$sgn,$647,$648,$649);
   $650 = (($op) + ($647<<4)|0);
   $651 = (($ip) + ($l<<4)|0);
   _mixed_radix_dit_rec($650,$651,$obj,$sgn,$647,$648,$649);
   $652 = $647 << 1;
   $653 = (($op) + ($652<<4)|0);
   $654 = $l << 1;
   $655 = (($ip) + ($654<<4)|0);
   _mixed_radix_dit_rec($653,$655,$obj,$sgn,$647,$648,$649);
   $656 = ($647*3)|0;
   $657 = (($op) + ($656<<4)|0);
   $658 = ($l*3)|0;
   $659 = (($ip) + ($658<<4)|0);
   _mixed_radix_dit_rec($657,$659,$obj,$sgn,$647,$648,$649);
   $660 = +HEAPF64[$op>>3];
   $661 = ((($op)) + 8|0);
   $662 = +HEAPF64[$661>>3];
   $663 = +HEAPF64[$650>>3];
   $664 = (((($op) + ($647<<4)|0)) + 8|0);
   $665 = +HEAPF64[$664>>3];
   $666 = (($op) + ($652<<4)|0);
   $667 = +HEAPF64[$666>>3];
   $668 = (((($op) + ($652<<4)|0)) + 8|0);
   $669 = +HEAPF64[$668>>3];
   $670 = (($op) + ($656<<4)|0);
   $671 = +HEAPF64[$670>>3];
   $672 = (((($op) + ($656<<4)|0)) + 8|0);
   $673 = +HEAPF64[$672>>3];
   $674 = $660 + $667;
   $675 = $662 + $669;
   $676 = $660 - $667;
   $677 = $662 - $669;
   $678 = $663 + $671;
   $679 = $665 + $673;
   $680 = (+($sgn|0));
   $681 = $663 - $671;
   $682 = $680 * $681;
   $683 = $665 - $673;
   $684 = $680 * $683;
   $685 = $674 + $678;
   HEAPF64[$op>>3] = $685;
   $686 = $675 + $679;
   HEAPF64[$661>>3] = $686;
   $687 = $676 + $684;
   HEAPF64[$650>>3] = $687;
   $688 = $677 - $682;
   HEAPF64[$664>>3] = $688;
   $689 = $674 - $678;
   HEAPF64[$666>>3] = $689;
   $690 = $675 - $679;
   HEAPF64[$668>>3] = $690;
   $691 = $676 - $684;
   HEAPF64[$670>>3] = $691;
   $692 = $677 + $682;
   HEAPF64[$672>>3] = $692;
   $693 = ($N|0)>(7);
   if (!($693)) {
    return;
   }
   $694 = (($647) + -1)|0;
   $k68$054 = 1;
   while(1) {
    $695 = ($k68$054*3)|0;
    $696 = (($694) + ($695))|0;
    $697 = (((($obj)) + 272|0) + ($696<<4)|0);
    $698 = +HEAPF64[$697>>3];
    $699 = (((((($obj)) + 272|0) + ($696<<4)|0)) + 8|0);
    $700 = +HEAPF64[$699>>3];
    $701 = (($695) + ($647))|0;
    $702 = (((($obj)) + 272|0) + ($701<<4)|0);
    $703 = +HEAPF64[$702>>3];
    $704 = (((((($obj)) + 272|0) + ($701<<4)|0)) + 8|0);
    $705 = +HEAPF64[$704>>3];
    $706 = (($701) + 1)|0;
    $707 = (((($obj)) + 272|0) + ($706<<4)|0);
    $708 = +HEAPF64[$707>>3];
    $709 = (((((($obj)) + 272|0) + ($706<<4)|0)) + 8|0);
    $710 = +HEAPF64[$709>>3];
    $711 = (($k68$054) + ($647))|0;
    $712 = (($711) + ($647))|0;
    $713 = (($712) + ($647))|0;
    $714 = (($op) + ($k68$054<<4)|0);
    $715 = +HEAPF64[$714>>3];
    $716 = (((($op) + ($k68$054<<4)|0)) + 8|0);
    $717 = +HEAPF64[$716>>3];
    $718 = (($op) + ($711<<4)|0);
    $719 = +HEAPF64[$718>>3];
    $720 = $698 * $719;
    $721 = (((($op) + ($711<<4)|0)) + 8|0);
    $722 = +HEAPF64[$721>>3];
    $723 = $700 * $722;
    $724 = $720 - $723;
    $725 = $698 * $722;
    $726 = $700 * $719;
    $727 = $726 + $725;
    $728 = (($op) + ($712<<4)|0);
    $729 = +HEAPF64[$728>>3];
    $730 = $703 * $729;
    $731 = (((($op) + ($712<<4)|0)) + 8|0);
    $732 = +HEAPF64[$731>>3];
    $733 = $705 * $732;
    $734 = $730 - $733;
    $735 = $703 * $732;
    $736 = $705 * $729;
    $737 = $736 + $735;
    $738 = (($op) + ($713<<4)|0);
    $739 = +HEAPF64[$738>>3];
    $740 = $708 * $739;
    $741 = (((($op) + ($713<<4)|0)) + 8|0);
    $742 = +HEAPF64[$741>>3];
    $743 = $710 * $742;
    $744 = $740 - $743;
    $745 = $708 * $742;
    $746 = $710 * $739;
    $747 = $746 + $745;
    $748 = $715 + $734;
    $749 = $717 + $737;
    $750 = $715 - $734;
    $751 = $717 - $737;
    $752 = $724 + $744;
    $753 = $727 + $747;
    $754 = $724 - $744;
    $755 = $680 * $754;
    $756 = $727 - $747;
    $757 = $680 * $756;
    $758 = $748 + $752;
    HEAPF64[$714>>3] = $758;
    $759 = $749 + $753;
    HEAPF64[$716>>3] = $759;
    $760 = $750 + $757;
    HEAPF64[$718>>3] = $760;
    $761 = $751 - $755;
    HEAPF64[$721>>3] = $761;
    $762 = $748 - $752;
    HEAPF64[$728>>3] = $762;
    $763 = $749 - $753;
    HEAPF64[$731>>3] = $763;
    $764 = $750 - $757;
    HEAPF64[$738>>3] = $764;
    $765 = $751 + $755;
    HEAPF64[$741>>3] = $765;
    $766 = (($k68$054) + 1)|0;
    $767 = ($766|0)<($647|0);
    if ($767) {
     $k68$054 = $766;
    } else {
     break;
    }
   }
   return;
   break;
  }
  case 5:  {
   $768 = (($N|0) / 5)&-1;
   $769 = ($l*5)|0;
   $770 = (($inc) + 1)|0;
   _mixed_radix_dit_rec($op,$ip,$obj,$sgn,$768,$769,$770);
   $771 = (($op) + ($768<<4)|0);
   $772 = (($ip) + ($l<<4)|0);
   _mixed_radix_dit_rec($771,$772,$obj,$sgn,$768,$769,$770);
   $773 = $768 << 1;
   $774 = (($op) + ($773<<4)|0);
   $775 = $l << 1;
   $776 = (($ip) + ($775<<4)|0);
   _mixed_radix_dit_rec($774,$776,$obj,$sgn,$768,$769,$770);
   $777 = ($768*3)|0;
   $778 = (($op) + ($777<<4)|0);
   $779 = ($l*3)|0;
   $780 = (($ip) + ($779<<4)|0);
   _mixed_radix_dit_rec($778,$780,$obj,$sgn,$768,$769,$770);
   $781 = $768 << 2;
   $782 = (($op) + ($781<<4)|0);
   $783 = $l << 2;
   $784 = (($ip) + ($783<<4)|0);
   _mixed_radix_dit_rec($782,$784,$obj,$sgn,$768,$769,$770);
   $785 = +HEAPF64[$op>>3];
   $786 = ((($op)) + 8|0);
   $787 = +HEAPF64[$786>>3];
   $788 = +HEAPF64[$771>>3];
   $789 = (((($op) + ($768<<4)|0)) + 8|0);
   $790 = +HEAPF64[$789>>3];
   $791 = (($op) + ($773<<4)|0);
   $792 = +HEAPF64[$791>>3];
   $793 = (((($op) + ($773<<4)|0)) + 8|0);
   $794 = +HEAPF64[$793>>3];
   $795 = (($op) + ($777<<4)|0);
   $796 = +HEAPF64[$795>>3];
   $797 = (((($op) + ($777<<4)|0)) + 8|0);
   $798 = +HEAPF64[$797>>3];
   $799 = (($op) + ($781<<4)|0);
   $800 = +HEAPF64[$799>>3];
   $801 = (((($op) + ($781<<4)|0)) + 8|0);
   $802 = +HEAPF64[$801>>3];
   $803 = $788 + $800;
   $804 = $790 + $802;
   $805 = $792 + $796;
   $806 = $794 + $798;
   $807 = $788 - $800;
   $808 = $790 - $802;
   $809 = $792 - $796;
   $810 = $794 - $798;
   $811 = $785 + $803;
   $812 = $805 + $811;
   HEAPF64[$op>>3] = $812;
   $813 = $787 + $804;
   $814 = $806 + $813;
   HEAPF64[$786>>3] = $814;
   $815 = $803 * 0.30901699437000002;
   $816 = $805 * 0.80901699436999996;
   $817 = $815 - $816;
   $818 = $804 * 0.30901699437000002;
   $819 = $806 * 0.80901699436999996;
   $820 = $818 - $819;
   $821 = (+($sgn|0));
   $822 = $807 * 0.95105651628999998;
   $823 = $809 * 0.58778525229;
   $824 = $823 + $822;
   $825 = $821 * $824;
   $826 = $808 * 0.95105651628999998;
   $827 = $810 * 0.58778525229;
   $828 = $827 + $826;
   $829 = $821 * $828;
   $830 = $785 + $817;
   $831 = $787 + $820;
   $832 = $830 + $829;
   HEAPF64[$771>>3] = $832;
   $833 = $831 - $825;
   HEAPF64[$789>>3] = $833;
   $834 = $830 - $829;
   HEAPF64[$799>>3] = $834;
   $835 = $825 + $831;
   HEAPF64[$801>>3] = $835;
   $836 = $803 * 0.80901699436999996;
   $837 = $805 * 0.30901699437000002;
   $838 = $837 - $836;
   $839 = $804 * 0.80901699436999996;
   $840 = $806 * 0.30901699437000002;
   $841 = $840 - $839;
   $842 = $807 * 0.58778525229;
   $843 = $809 * 0.95105651628999998;
   $844 = $842 - $843;
   $845 = $821 * $844;
   $846 = $808 * 0.58778525229;
   $847 = $810 * 0.95105651628999998;
   $848 = $846 - $847;
   $849 = $821 * $848;
   $850 = $785 + $838;
   $851 = $787 + $841;
   $852 = $850 + $849;
   HEAPF64[$791>>3] = $852;
   $853 = $851 - $845;
   HEAPF64[$793>>3] = $853;
   $854 = $850 - $849;
   HEAPF64[$795>>3] = $854;
   $855 = $845 + $851;
   HEAPF64[$797>>3] = $855;
   $856 = ($N|0)>(9);
   if (!($856)) {
    return;
   }
   $857 = (($768) + -1)|0;
   $858 = ($sgn|0)==(1);
   $k90$057 = 1;
   while(1) {
    $859 = $k90$057 << 2;
    $860 = (($857) + ($859))|0;
    $861 = (((($obj)) + 272|0) + ($860<<4)|0);
    $862 = +HEAPF64[$861>>3];
    $863 = (((((($obj)) + 272|0) + ($860<<4)|0)) + 8|0);
    $864 = +HEAPF64[$863>>3];
    $865 = (($859) + ($768))|0;
    $866 = (((($obj)) + 272|0) + ($865<<4)|0);
    $867 = +HEAPF64[$866>>3];
    $868 = (((((($obj)) + 272|0) + ($865<<4)|0)) + 8|0);
    $869 = +HEAPF64[$868>>3];
    $870 = (($865) + 1)|0;
    $871 = (((($obj)) + 272|0) + ($870<<4)|0);
    $872 = +HEAPF64[$871>>3];
    $873 = (((((($obj)) + 272|0) + ($870<<4)|0)) + 8|0);
    $874 = +HEAPF64[$873>>3];
    $875 = (($865) + 2)|0;
    $876 = (((($obj)) + 272|0) + ($875<<4)|0);
    $877 = +HEAPF64[$876>>3];
    $878 = (((((($obj)) + 272|0) + ($875<<4)|0)) + 8|0);
    $879 = +HEAPF64[$878>>3];
    $880 = (($k90$057) + ($768))|0;
    $881 = (($880) + ($768))|0;
    $882 = (($881) + ($768))|0;
    $883 = (($882) + ($768))|0;
    $884 = (($op) + ($k90$057<<4)|0);
    $885 = +HEAPF64[$884>>3];
    $886 = (((($op) + ($k90$057<<4)|0)) + 8|0);
    $887 = +HEAPF64[$886>>3];
    $888 = (($op) + ($880<<4)|0);
    $889 = +HEAPF64[$888>>3];
    $890 = $862 * $889;
    $891 = (((($op) + ($880<<4)|0)) + 8|0);
    $892 = +HEAPF64[$891>>3];
    $893 = $864 * $892;
    $894 = $890 - $893;
    $895 = $862 * $892;
    $896 = $864 * $889;
    $897 = $896 + $895;
    $898 = (($op) + ($881<<4)|0);
    $899 = +HEAPF64[$898>>3];
    $900 = $867 * $899;
    $901 = (((($op) + ($881<<4)|0)) + 8|0);
    $902 = +HEAPF64[$901>>3];
    $903 = $869 * $902;
    $904 = $900 - $903;
    $905 = $867 * $902;
    $906 = $869 * $899;
    $907 = $906 + $905;
    $908 = (($op) + ($882<<4)|0);
    $909 = +HEAPF64[$908>>3];
    $910 = $872 * $909;
    $911 = (((($op) + ($882<<4)|0)) + 8|0);
    $912 = +HEAPF64[$911>>3];
    $913 = $874 * $912;
    $914 = $910 - $913;
    $915 = $872 * $912;
    $916 = $874 * $909;
    $917 = $916 + $915;
    $918 = (($op) + ($883<<4)|0);
    $919 = +HEAPF64[$918>>3];
    $920 = $877 * $919;
    $921 = (((($op) + ($883<<4)|0)) + 8|0);
    $922 = +HEAPF64[$921>>3];
    $923 = $879 * $922;
    $924 = $920 - $923;
    $925 = $877 * $922;
    $926 = $879 * $919;
    $927 = $926 + $925;
    $928 = $894 + $924;
    $929 = $897 + $927;
    $930 = $904 + $914;
    $931 = $907 + $917;
    $932 = $894 - $924;
    $933 = $897 - $927;
    $934 = $904 - $914;
    $935 = $907 - $917;
    $936 = $885 + $928;
    $937 = $930 + $936;
    HEAPF64[$884>>3] = $937;
    $938 = $887 + $929;
    $939 = $931 + $938;
    HEAPF64[$886>>3] = $939;
    $940 = $928 * 0.30901699437000002;
    $941 = $930 * 0.80901699436999996;
    $942 = $940 - $941;
    $943 = $929 * 0.30901699437000002;
    $944 = $931 * 0.80901699436999996;
    $945 = $943 - $944;
    if ($858) {
     $946 = $932 * 0.95105651628999998;
     $947 = $934 * 0.58778525229;
     $948 = $947 + $946;
     $949 = $933 * 0.95105651628999998;
     $950 = $935 * 0.58778525229;
     $951 = $950 + $949;
     $tau5i120$0 = $951;$tau5r119$0 = $948;
    } else {
     $952 = $932 * -0.95105651628999998;
     $953 = $934 * 0.58778525229;
     $954 = $952 - $953;
     $955 = $933 * -0.95105651628999998;
     $956 = $935 * 0.58778525229;
     $957 = $955 - $956;
     $tau5i120$0 = $957;$tau5r119$0 = $954;
    }
    $958 = $885 + $942;
    $959 = $887 + $945;
    $960 = $958 + $tau5i120$0;
    HEAPF64[$888>>3] = $960;
    $961 = $959 - $tau5r119$0;
    HEAPF64[$891>>3] = $961;
    $962 = $958 - $tau5i120$0;
    HEAPF64[$918>>3] = $962;
    $963 = $959 + $tau5r119$0;
    HEAPF64[$921>>3] = $963;
    $964 = $928 * 0.80901699436999996;
    $965 = $930 * 0.30901699437000002;
    $966 = $965 - $964;
    $967 = $929 * 0.80901699436999996;
    $968 = $931 * 0.30901699437000002;
    $969 = $968 - $967;
    $970 = $932 * 0.58778525229;
    $971 = $934 * 0.95105651628999998;
    if ($858) {
     $972 = $970 - $971;
     $973 = $933 * 0.58778525229;
     $974 = $935 * 0.95105651628999998;
     $975 = $973 - $974;
     $tau5i120$1 = $975;$tau5r119$1 = $972;
    } else {
     $976 = $971 - $970;
     $977 = $933 * 0.58778525229;
     $978 = $935 * 0.95105651628999998;
     $979 = $978 - $977;
     $tau5i120$1 = $979;$tau5r119$1 = $976;
    }
    $980 = $885 + $966;
    $981 = $887 + $969;
    $982 = $980 + $tau5i120$1;
    HEAPF64[$898>>3] = $982;
    $983 = $981 - $tau5r119$1;
    HEAPF64[$901>>3] = $983;
    $984 = $980 - $tau5i120$1;
    HEAPF64[$908>>3] = $984;
    $985 = $981 + $tau5r119$1;
    HEAPF64[$911>>3] = $985;
    $986 = (($k90$057) + 1)|0;
    $987 = ($986|0)<($768|0);
    if ($987) {
     $k90$057 = $986;
    } else {
     break;
    }
   }
   return;
   break;
  }
  case 7:  {
   $988 = (($N|0) / 7)&-1;
   $989 = ($l*7)|0;
   $990 = (($inc) + 1)|0;
   _mixed_radix_dit_rec($op,$ip,$obj,$sgn,$988,$989,$990);
   $991 = (($op) + ($988<<4)|0);
   $992 = (($ip) + ($l<<4)|0);
   _mixed_radix_dit_rec($991,$992,$obj,$sgn,$988,$989,$990);
   $993 = $988 << 1;
   $994 = (($op) + ($993<<4)|0);
   $995 = $l << 1;
   $996 = (($ip) + ($995<<4)|0);
   _mixed_radix_dit_rec($994,$996,$obj,$sgn,$988,$989,$990);
   $997 = ($988*3)|0;
   $998 = (($op) + ($997<<4)|0);
   $999 = ($l*3)|0;
   $1000 = (($ip) + ($999<<4)|0);
   _mixed_radix_dit_rec($998,$1000,$obj,$sgn,$988,$989,$990);
   $1001 = $988 << 2;
   $1002 = (($op) + ($1001<<4)|0);
   $1003 = $l << 2;
   $1004 = (($ip) + ($1003<<4)|0);
   _mixed_radix_dit_rec($1002,$1004,$obj,$sgn,$988,$989,$990);
   $1005 = ($988*5)|0;
   $1006 = (($op) + ($1005<<4)|0);
   $1007 = ($l*5)|0;
   $1008 = (($ip) + ($1007<<4)|0);
   _mixed_radix_dit_rec($1006,$1008,$obj,$sgn,$988,$989,$990);
   $1009 = ($988*6)|0;
   $1010 = (($op) + ($1009<<4)|0);
   $1011 = ($l*6)|0;
   $1012 = (($ip) + ($1011<<4)|0);
   _mixed_radix_dit_rec($1010,$1012,$obj,$sgn,$988,$989,$990);
   $1013 = +HEAPF64[$op>>3];
   $1014 = ((($op)) + 8|0);
   $1015 = +HEAPF64[$1014>>3];
   $1016 = +HEAPF64[$991>>3];
   $1017 = (((($op) + ($988<<4)|0)) + 8|0);
   $1018 = +HEAPF64[$1017>>3];
   $1019 = (($op) + ($993<<4)|0);
   $1020 = +HEAPF64[$1019>>3];
   $1021 = (((($op) + ($993<<4)|0)) + 8|0);
   $1022 = +HEAPF64[$1021>>3];
   $1023 = (($op) + ($997<<4)|0);
   $1024 = +HEAPF64[$1023>>3];
   $1025 = (((($op) + ($997<<4)|0)) + 8|0);
   $1026 = +HEAPF64[$1025>>3];
   $1027 = (($op) + ($1001<<4)|0);
   $1028 = +HEAPF64[$1027>>3];
   $1029 = (((($op) + ($1001<<4)|0)) + 8|0);
   $1030 = +HEAPF64[$1029>>3];
   $1031 = (($op) + ($1005<<4)|0);
   $1032 = +HEAPF64[$1031>>3];
   $1033 = (((($op) + ($1005<<4)|0)) + 8|0);
   $1034 = +HEAPF64[$1033>>3];
   $1035 = (($op) + ($1009<<4)|0);
   $1036 = +HEAPF64[$1035>>3];
   $1037 = (((($op) + ($1009<<4)|0)) + 8|0);
   $1038 = +HEAPF64[$1037>>3];
   $1039 = $1016 + $1036;
   $1040 = $1016 - $1036;
   $1041 = $1018 + $1038;
   $1042 = $1018 - $1038;
   $1043 = $1020 + $1032;
   $1044 = $1020 - $1032;
   $1045 = $1022 + $1034;
   $1046 = $1022 - $1034;
   $1047 = $1024 + $1028;
   $1048 = $1024 - $1028;
   $1049 = $1026 + $1030;
   $1050 = $1026 - $1030;
   $1051 = $1013 + $1039;
   $1052 = $1043 + $1051;
   $1053 = $1047 + $1052;
   HEAPF64[$op>>3] = $1053;
   $1054 = $1015 + $1041;
   $1055 = $1045 + $1054;
   $1056 = $1049 + $1055;
   HEAPF64[$1014>>3] = $1056;
   $1057 = $1039 * 0.62348980185000002;
   $1058 = $1013 + $1057;
   $1059 = $1043 * 0.22252093395;
   $1060 = $1058 - $1059;
   $1061 = $1047 * 0.90096886789999997;
   $1062 = $1060 - $1061;
   $1063 = $1041 * 0.62348980185000002;
   $1064 = $1015 + $1063;
   $1065 = $1045 * 0.22252093395;
   $1066 = $1064 - $1065;
   $1067 = $1049 * 0.90096886789999997;
   $1068 = $1066 - $1067;
   $1069 = ($sgn|0)==(1);
   if ($1069) {
    $1070 = $1040 * -0.78183148246;
    $1071 = $1044 * 0.97492791217999996;
    $1072 = $1070 - $1071;
    $1073 = $1048 * 0.43388373911;
    $1074 = $1072 - $1073;
    $1075 = $1042 * -0.78183148246;
    $1076 = $1046 * 0.97492791217999996;
    $1077 = $1075 - $1076;
    $1078 = $1050 * 0.43388373911;
    $1079 = $1077 - $1078;
    $tau7i166$0 = $1079;$tau7r165$0 = $1074;
   } else {
    $1080 = $1040 * 0.78183148246;
    $1081 = $1044 * 0.97492791217999996;
    $1082 = $1081 + $1080;
    $1083 = $1048 * 0.43388373911;
    $1084 = $1083 + $1082;
    $1085 = $1042 * 0.78183148246;
    $1086 = $1046 * 0.97492791217999996;
    $1087 = $1086 + $1085;
    $1088 = $1050 * 0.43388373911;
    $1089 = $1088 + $1087;
    $tau7i166$0 = $1089;$tau7r165$0 = $1084;
   }
   $1090 = $1062 - $tau7i166$0;
   HEAPF64[$991>>3] = $1090;
   $1091 = $1068 + $tau7r165$0;
   HEAPF64[$1017>>3] = $1091;
   $1092 = $1062 + $tau7i166$0;
   HEAPF64[$1035>>3] = $1092;
   $1093 = $1068 - $tau7r165$0;
   HEAPF64[$1037>>3] = $1093;
   $1094 = $1039 * 0.22252093395;
   $1095 = $1013 - $1094;
   $1096 = $1043 * 0.90096886789999997;
   $1097 = $1095 - $1096;
   $1098 = $1047 * 0.62348980185000002;
   $1099 = $1098 + $1097;
   $1100 = $1041 * 0.22252093395;
   $1101 = $1015 - $1100;
   $1102 = $1045 * 0.90096886789999997;
   $1103 = $1101 - $1102;
   $1104 = $1049 * 0.62348980185000002;
   $1105 = $1104 + $1103;
   $1106 = $1040 * 0.97492791217999996;
   $1107 = $1044 * 0.43388373911;
   if ($1069) {
    $1108 = $1107 - $1106;
    $1109 = $1048 * 0.78183148246;
    $1110 = $1109 + $1108;
    $1111 = $1042 * 0.97492791217999996;
    $1112 = $1046 * 0.43388373911;
    $1113 = $1112 - $1111;
    $1114 = $1050 * 0.78183148246;
    $1115 = $1114 + $1113;
    $tau7i166$1 = $1115;$tau7r165$1 = $1110;
   } else {
    $1116 = $1106 - $1107;
    $1117 = $1048 * 0.78183148246;
    $1118 = $1116 - $1117;
    $1119 = $1042 * 0.97492791217999996;
    $1120 = $1046 * 0.43388373911;
    $1121 = $1119 - $1120;
    $1122 = $1050 * 0.78183148246;
    $1123 = $1121 - $1122;
    $tau7i166$1 = $1123;$tau7r165$1 = $1118;
   }
   $1124 = $1099 - $tau7i166$1;
   HEAPF64[$1019>>3] = $1124;
   $1125 = $1105 + $tau7r165$1;
   HEAPF64[$1021>>3] = $1125;
   $1126 = $1099 + $tau7i166$1;
   HEAPF64[$1031>>3] = $1126;
   $1127 = $1105 - $tau7r165$1;
   HEAPF64[$1033>>3] = $1127;
   $1128 = $1039 * 0.90096886789999997;
   $1129 = $1013 - $1128;
   $1130 = $1043 * 0.62348980185000002;
   $1131 = $1130 + $1129;
   $1132 = $1047 * 0.22252093395;
   $1133 = $1131 - $1132;
   $1134 = $1041 * 0.90096886789999997;
   $1135 = $1015 - $1134;
   $1136 = $1045 * 0.62348980185000002;
   $1137 = $1136 + $1135;
   $1138 = $1049 * 0.22252093395;
   $1139 = $1137 - $1138;
   $1140 = $1040 * 0.43388373911;
   $1141 = $1044 * 0.78183148246;
   if ($1069) {
    $1142 = $1141 - $1140;
    $1143 = $1048 * 0.97492791217999996;
    $1144 = $1142 - $1143;
    $1145 = $1042 * 0.43388373911;
    $1146 = $1046 * 0.78183148246;
    $1147 = $1146 - $1145;
    $1148 = $1050 * 0.97492791217999996;
    $1149 = $1147 - $1148;
    $tau7i166$2 = $1149;$tau7r165$2 = $1144;
   } else {
    $1150 = $1140 - $1141;
    $1151 = $1048 * 0.97492791217999996;
    $1152 = $1151 + $1150;
    $1153 = $1042 * 0.43388373911;
    $1154 = $1046 * 0.78183148246;
    $1155 = $1153 - $1154;
    $1156 = $1050 * 0.97492791217999996;
    $1157 = $1156 + $1155;
    $tau7i166$2 = $1157;$tau7r165$2 = $1152;
   }
   $1158 = $1133 - $tau7i166$2;
   HEAPF64[$1023>>3] = $1158;
   $1159 = $1139 + $tau7r165$2;
   HEAPF64[$1025>>3] = $1159;
   $1160 = $1133 + $tau7i166$2;
   HEAPF64[$1027>>3] = $1160;
   $1161 = $1139 - $tau7r165$2;
   HEAPF64[$1029>>3] = $1161;
   $1162 = ($N|0)>(13);
   if (!($1162)) {
    return;
   }
   $1163 = (($988) + -1)|0;
   $k127$060 = 1;
   while(1) {
    $1164 = ($k127$060*6)|0;
    $1165 = (($1163) + ($1164))|0;
    $1166 = (((($obj)) + 272|0) + ($1165<<4)|0);
    $1167 = +HEAPF64[$1166>>3];
    $1168 = (((((($obj)) + 272|0) + ($1165<<4)|0)) + 8|0);
    $1169 = +HEAPF64[$1168>>3];
    $1170 = (($1164) + ($988))|0;
    $1171 = (((($obj)) + 272|0) + ($1170<<4)|0);
    $1172 = +HEAPF64[$1171>>3];
    $1173 = (((((($obj)) + 272|0) + ($1170<<4)|0)) + 8|0);
    $1174 = +HEAPF64[$1173>>3];
    $1175 = (($1170) + 1)|0;
    $1176 = (((($obj)) + 272|0) + ($1175<<4)|0);
    $1177 = +HEAPF64[$1176>>3];
    $1178 = (((((($obj)) + 272|0) + ($1175<<4)|0)) + 8|0);
    $1179 = +HEAPF64[$1178>>3];
    $1180 = (($1170) + 2)|0;
    $1181 = (((($obj)) + 272|0) + ($1180<<4)|0);
    $1182 = +HEAPF64[$1181>>3];
    $1183 = (((((($obj)) + 272|0) + ($1180<<4)|0)) + 8|0);
    $1184 = +HEAPF64[$1183>>3];
    $1185 = (($1170) + 3)|0;
    $1186 = (((($obj)) + 272|0) + ($1185<<4)|0);
    $1187 = +HEAPF64[$1186>>3];
    $1188 = (((((($obj)) + 272|0) + ($1185<<4)|0)) + 8|0);
    $1189 = +HEAPF64[$1188>>3];
    $1190 = (($1170) + 4)|0;
    $1191 = (((($obj)) + 272|0) + ($1190<<4)|0);
    $1192 = +HEAPF64[$1191>>3];
    $1193 = (((((($obj)) + 272|0) + ($1190<<4)|0)) + 8|0);
    $1194 = +HEAPF64[$1193>>3];
    $1195 = (($k127$060) + ($988))|0;
    $1196 = (($1195) + ($988))|0;
    $1197 = (($1196) + ($988))|0;
    $1198 = (($1197) + ($988))|0;
    $1199 = (($1198) + ($988))|0;
    $1200 = (($1199) + ($988))|0;
    $1201 = (($op) + ($k127$060<<4)|0);
    $1202 = +HEAPF64[$1201>>3];
    $1203 = (((($op) + ($k127$060<<4)|0)) + 8|0);
    $1204 = +HEAPF64[$1203>>3];
    $1205 = (($op) + ($1195<<4)|0);
    $1206 = +HEAPF64[$1205>>3];
    $1207 = $1167 * $1206;
    $1208 = (((($op) + ($1195<<4)|0)) + 8|0);
    $1209 = +HEAPF64[$1208>>3];
    $1210 = $1169 * $1209;
    $1211 = $1207 - $1210;
    $1212 = $1167 * $1209;
    $1213 = $1169 * $1206;
    $1214 = $1213 + $1212;
    $1215 = (($op) + ($1196<<4)|0);
    $1216 = +HEAPF64[$1215>>3];
    $1217 = $1172 * $1216;
    $1218 = (((($op) + ($1196<<4)|0)) + 8|0);
    $1219 = +HEAPF64[$1218>>3];
    $1220 = $1174 * $1219;
    $1221 = $1217 - $1220;
    $1222 = $1172 * $1219;
    $1223 = $1174 * $1216;
    $1224 = $1223 + $1222;
    $1225 = (($op) + ($1197<<4)|0);
    $1226 = +HEAPF64[$1225>>3];
    $1227 = $1177 * $1226;
    $1228 = (((($op) + ($1197<<4)|0)) + 8|0);
    $1229 = +HEAPF64[$1228>>3];
    $1230 = $1179 * $1229;
    $1231 = $1227 - $1230;
    $1232 = $1177 * $1229;
    $1233 = $1179 * $1226;
    $1234 = $1233 + $1232;
    $1235 = (($op) + ($1198<<4)|0);
    $1236 = +HEAPF64[$1235>>3];
    $1237 = $1182 * $1236;
    $1238 = (((($op) + ($1198<<4)|0)) + 8|0);
    $1239 = +HEAPF64[$1238>>3];
    $1240 = $1184 * $1239;
    $1241 = $1237 - $1240;
    $1242 = $1182 * $1239;
    $1243 = $1184 * $1236;
    $1244 = $1243 + $1242;
    $1245 = (($op) + ($1199<<4)|0);
    $1246 = +HEAPF64[$1245>>3];
    $1247 = $1187 * $1246;
    $1248 = (((($op) + ($1199<<4)|0)) + 8|0);
    $1249 = +HEAPF64[$1248>>3];
    $1250 = $1189 * $1249;
    $1251 = $1247 - $1250;
    $1252 = $1187 * $1249;
    $1253 = $1189 * $1246;
    $1254 = $1253 + $1252;
    $1255 = (($op) + ($1200<<4)|0);
    $1256 = +HEAPF64[$1255>>3];
    $1257 = $1192 * $1256;
    $1258 = (((($op) + ($1200<<4)|0)) + 8|0);
    $1259 = +HEAPF64[$1258>>3];
    $1260 = $1194 * $1259;
    $1261 = $1257 - $1260;
    $1262 = $1192 * $1259;
    $1263 = $1194 * $1256;
    $1264 = $1263 + $1262;
    $1265 = $1211 + $1261;
    $1266 = $1211 - $1261;
    $1267 = $1214 + $1264;
    $1268 = $1214 - $1264;
    $1269 = $1221 + $1251;
    $1270 = $1221 - $1251;
    $1271 = $1224 + $1254;
    $1272 = $1224 - $1254;
    $1273 = $1231 + $1241;
    $1274 = $1231 - $1241;
    $1275 = $1234 + $1244;
    $1276 = $1234 - $1244;
    $1277 = $1202 + $1265;
    $1278 = $1269 + $1277;
    $1279 = $1273 + $1278;
    HEAPF64[$1201>>3] = $1279;
    $1280 = $1204 + $1267;
    $1281 = $1271 + $1280;
    $1282 = $1275 + $1281;
    HEAPF64[$1203>>3] = $1282;
    $1283 = $1265 * 0.62348980185000002;
    $1284 = $1202 + $1283;
    $1285 = $1269 * 0.22252093395;
    $1286 = $1284 - $1285;
    $1287 = $1273 * 0.90096886789999997;
    $1288 = $1286 - $1287;
    $1289 = $1267 * 0.62348980185000002;
    $1290 = $1204 + $1289;
    $1291 = $1271 * 0.22252093395;
    $1292 = $1290 - $1291;
    $1293 = $1275 * 0.90096886789999997;
    $1294 = $1292 - $1293;
    if ($1069) {
     $1295 = $1266 * -0.78183148246;
     $1296 = $1270 * 0.97492791217999996;
     $1297 = $1295 - $1296;
     $1298 = $1274 * 0.43388373911;
     $1299 = $1297 - $1298;
     $1300 = $1268 * -0.78183148246;
     $1301 = $1272 * 0.97492791217999996;
     $1302 = $1300 - $1301;
     $1303 = $1276 * 0.43388373911;
     $1304 = $1302 - $1303;
     $tau7i166$3 = $1304;$tau7r165$3 = $1299;
    } else {
     $1305 = $1266 * 0.78183148246;
     $1306 = $1270 * 0.97492791217999996;
     $1307 = $1306 + $1305;
     $1308 = $1274 * 0.43388373911;
     $1309 = $1308 + $1307;
     $1310 = $1268 * 0.78183148246;
     $1311 = $1272 * 0.97492791217999996;
     $1312 = $1311 + $1310;
     $1313 = $1276 * 0.43388373911;
     $1314 = $1313 + $1312;
     $tau7i166$3 = $1314;$tau7r165$3 = $1309;
    }
    $1315 = $1288 - $tau7i166$3;
    HEAPF64[$1205>>3] = $1315;
    $1316 = $1294 + $tau7r165$3;
    HEAPF64[$1208>>3] = $1316;
    $1317 = $1288 + $tau7i166$3;
    HEAPF64[$1255>>3] = $1317;
    $1318 = $1294 - $tau7r165$3;
    HEAPF64[$1258>>3] = $1318;
    $1319 = $1265 * 0.22252093395;
    $1320 = $1202 - $1319;
    $1321 = $1269 * 0.90096886789999997;
    $1322 = $1320 - $1321;
    $1323 = $1273 * 0.62348980185000002;
    $1324 = $1323 + $1322;
    $1325 = $1267 * 0.22252093395;
    $1326 = $1204 - $1325;
    $1327 = $1271 * 0.90096886789999997;
    $1328 = $1326 - $1327;
    $1329 = $1275 * 0.62348980185000002;
    $1330 = $1329 + $1328;
    $1331 = $1266 * 0.97492791217999996;
    $1332 = $1270 * 0.43388373911;
    if ($1069) {
     $1333 = $1332 - $1331;
     $1334 = $1274 * 0.78183148246;
     $1335 = $1334 + $1333;
     $1336 = $1268 * 0.97492791217999996;
     $1337 = $1272 * 0.43388373911;
     $1338 = $1337 - $1336;
     $1339 = $1276 * 0.78183148246;
     $1340 = $1339 + $1338;
     $tau7i166$4 = $1340;$tau7r165$4 = $1335;
    } else {
     $1341 = $1331 - $1332;
     $1342 = $1274 * 0.78183148246;
     $1343 = $1341 - $1342;
     $1344 = $1268 * 0.97492791217999996;
     $1345 = $1272 * 0.43388373911;
     $1346 = $1344 - $1345;
     $1347 = $1276 * 0.78183148246;
     $1348 = $1346 - $1347;
     $tau7i166$4 = $1348;$tau7r165$4 = $1343;
    }
    $1349 = $1324 - $tau7i166$4;
    HEAPF64[$1215>>3] = $1349;
    $1350 = $1330 + $tau7r165$4;
    HEAPF64[$1218>>3] = $1350;
    $1351 = $1324 + $tau7i166$4;
    HEAPF64[$1245>>3] = $1351;
    $1352 = $1330 - $tau7r165$4;
    HEAPF64[$1248>>3] = $1352;
    $1353 = $1265 * 0.90096886789999997;
    $1354 = $1202 - $1353;
    $1355 = $1269 * 0.62348980185000002;
    $1356 = $1355 + $1354;
    $1357 = $1273 * 0.22252093395;
    $1358 = $1356 - $1357;
    $1359 = $1267 * 0.90096886789999997;
    $1360 = $1204 - $1359;
    $1361 = $1271 * 0.62348980185000002;
    $1362 = $1361 + $1360;
    $1363 = $1275 * 0.22252093395;
    $1364 = $1362 - $1363;
    $1365 = $1266 * 0.43388373911;
    $1366 = $1270 * 0.78183148246;
    if ($1069) {
     $1367 = $1366 - $1365;
     $1368 = $1274 * 0.97492791217999996;
     $1369 = $1367 - $1368;
     $1370 = $1268 * 0.43388373911;
     $1371 = $1272 * 0.78183148246;
     $1372 = $1371 - $1370;
     $1373 = $1276 * 0.97492791217999996;
     $1374 = $1372 - $1373;
     $tau7i166$5 = $1374;$tau7r165$5 = $1369;
    } else {
     $1375 = $1365 - $1366;
     $1376 = $1274 * 0.97492791217999996;
     $1377 = $1376 + $1375;
     $1378 = $1268 * 0.43388373911;
     $1379 = $1272 * 0.78183148246;
     $1380 = $1378 - $1379;
     $1381 = $1276 * 0.97492791217999996;
     $1382 = $1381 + $1380;
     $tau7i166$5 = $1382;$tau7r165$5 = $1377;
    }
    $1383 = $1358 - $tau7i166$5;
    HEAPF64[$1225>>3] = $1383;
    $1384 = $1364 + $tau7r165$5;
    HEAPF64[$1228>>3] = $1384;
    $1385 = $1358 + $tau7i166$5;
    HEAPF64[$1235>>3] = $1385;
    $1386 = $1364 - $tau7r165$5;
    HEAPF64[$1238>>3] = $1386;
    $1387 = (($k127$060) + 1)|0;
    $1388 = ($1387|0)<($988|0);
    if ($1388) {
     $k127$060 = $1387;
    } else {
     break;
    }
   }
   return;
   break;
  }
  case 8:  {
   $1389 = (($N|0) / 8)&-1;
   $1390 = $l << 3;
   $1391 = (($inc) + 1)|0;
   _mixed_radix_dit_rec($op,$ip,$obj,$sgn,$1389,$1390,$1391);
   $1392 = (($op) + ($1389<<4)|0);
   $1393 = (($ip) + ($l<<4)|0);
   _mixed_radix_dit_rec($1392,$1393,$obj,$sgn,$1389,$1390,$1391);
   $1394 = $1389 << 1;
   $1395 = (($op) + ($1394<<4)|0);
   $1396 = $l << 1;
   $1397 = (($ip) + ($1396<<4)|0);
   _mixed_radix_dit_rec($1395,$1397,$obj,$sgn,$1389,$1390,$1391);
   $1398 = ($1389*3)|0;
   $1399 = (($op) + ($1398<<4)|0);
   $1400 = ($l*3)|0;
   $1401 = (($ip) + ($1400<<4)|0);
   _mixed_radix_dit_rec($1399,$1401,$obj,$sgn,$1389,$1390,$1391);
   $1402 = $1389 << 2;
   $1403 = (($op) + ($1402<<4)|0);
   $1404 = $l << 2;
   $1405 = (($ip) + ($1404<<4)|0);
   _mixed_radix_dit_rec($1403,$1405,$obj,$sgn,$1389,$1390,$1391);
   $1406 = ($1389*5)|0;
   $1407 = (($op) + ($1406<<4)|0);
   $1408 = ($l*5)|0;
   $1409 = (($ip) + ($1408<<4)|0);
   _mixed_radix_dit_rec($1407,$1409,$obj,$sgn,$1389,$1390,$1391);
   $1410 = ($1389*6)|0;
   $1411 = (($op) + ($1410<<4)|0);
   $1412 = ($l*6)|0;
   $1413 = (($ip) + ($1412<<4)|0);
   _mixed_radix_dit_rec($1411,$1413,$obj,$sgn,$1389,$1390,$1391);
   $1414 = ($1389*7)|0;
   $1415 = (($op) + ($1414<<4)|0);
   $1416 = ($l*7)|0;
   $1417 = (($ip) + ($1416<<4)|0);
   _mixed_radix_dit_rec($1415,$1417,$obj,$sgn,$1389,$1390,$1391);
   $1418 = ($N|0)>(7);
   if (!($1418)) {
    return;
   }
   $1419 = (($1389) + -1)|0;
   $1420 = ($sgn|0)==(1);
   $k173$063 = 0;
   while(1) {
    $1421 = ($k173$063*7)|0;
    $1422 = (($1419) + ($1421))|0;
    $1423 = (((($obj)) + 272|0) + ($1422<<4)|0);
    $1424 = +HEAPF64[$1423>>3];
    $1425 = (((((($obj)) + 272|0) + ($1422<<4)|0)) + 8|0);
    $1426 = +HEAPF64[$1425>>3];
    $1427 = (($1421) + ($1389))|0;
    $1428 = (((($obj)) + 272|0) + ($1427<<4)|0);
    $1429 = +HEAPF64[$1428>>3];
    $1430 = (((((($obj)) + 272|0) + ($1427<<4)|0)) + 8|0);
    $1431 = +HEAPF64[$1430>>3];
    $1432 = (($1427) + 1)|0;
    $1433 = (((($obj)) + 272|0) + ($1432<<4)|0);
    $1434 = +HEAPF64[$1433>>3];
    $1435 = (((((($obj)) + 272|0) + ($1432<<4)|0)) + 8|0);
    $1436 = +HEAPF64[$1435>>3];
    $1437 = (($1427) + 2)|0;
    $1438 = (((($obj)) + 272|0) + ($1437<<4)|0);
    $1439 = +HEAPF64[$1438>>3];
    $1440 = (((((($obj)) + 272|0) + ($1437<<4)|0)) + 8|0);
    $1441 = +HEAPF64[$1440>>3];
    $1442 = (($1427) + 3)|0;
    $1443 = (((($obj)) + 272|0) + ($1442<<4)|0);
    $1444 = +HEAPF64[$1443>>3];
    $1445 = (((((($obj)) + 272|0) + ($1442<<4)|0)) + 8|0);
    $1446 = +HEAPF64[$1445>>3];
    $1447 = (($1427) + 4)|0;
    $1448 = (((($obj)) + 272|0) + ($1447<<4)|0);
    $1449 = +HEAPF64[$1448>>3];
    $1450 = (((((($obj)) + 272|0) + ($1447<<4)|0)) + 8|0);
    $1451 = +HEAPF64[$1450>>3];
    $1452 = (($1427) + 5)|0;
    $1453 = (((($obj)) + 272|0) + ($1452<<4)|0);
    $1454 = +HEAPF64[$1453>>3];
    $1455 = (((((($obj)) + 272|0) + ($1452<<4)|0)) + 8|0);
    $1456 = +HEAPF64[$1455>>3];
    $1457 = (($k173$063) + ($1389))|0;
    $1458 = (($1457) + ($1389))|0;
    $1459 = (($1458) + ($1389))|0;
    $1460 = (($1459) + ($1389))|0;
    $1461 = (($1460) + ($1389))|0;
    $1462 = (($1461) + ($1389))|0;
    $1463 = (($1462) + ($1389))|0;
    $1464 = (($op) + ($k173$063<<4)|0);
    $1465 = +HEAPF64[$1464>>3];
    $1466 = (((($op) + ($k173$063<<4)|0)) + 8|0);
    $1467 = +HEAPF64[$1466>>3];
    $1468 = (($op) + ($1457<<4)|0);
    $1469 = +HEAPF64[$1468>>3];
    $1470 = $1424 * $1469;
    $1471 = (((($op) + ($1457<<4)|0)) + 8|0);
    $1472 = +HEAPF64[$1471>>3];
    $1473 = $1426 * $1472;
    $1474 = $1470 - $1473;
    $1475 = $1424 * $1472;
    $1476 = $1426 * $1469;
    $1477 = $1476 + $1475;
    $1478 = (($op) + ($1458<<4)|0);
    $1479 = +HEAPF64[$1478>>3];
    $1480 = $1429 * $1479;
    $1481 = (((($op) + ($1458<<4)|0)) + 8|0);
    $1482 = +HEAPF64[$1481>>3];
    $1483 = $1431 * $1482;
    $1484 = $1480 - $1483;
    $1485 = $1429 * $1482;
    $1486 = $1431 * $1479;
    $1487 = $1486 + $1485;
    $1488 = (($op) + ($1459<<4)|0);
    $1489 = +HEAPF64[$1488>>3];
    $1490 = $1434 * $1489;
    $1491 = (((($op) + ($1459<<4)|0)) + 8|0);
    $1492 = +HEAPF64[$1491>>3];
    $1493 = $1436 * $1492;
    $1494 = $1490 - $1493;
    $1495 = $1434 * $1492;
    $1496 = $1436 * $1489;
    $1497 = $1496 + $1495;
    $1498 = (($op) + ($1460<<4)|0);
    $1499 = +HEAPF64[$1498>>3];
    $1500 = $1439 * $1499;
    $1501 = (((($op) + ($1460<<4)|0)) + 8|0);
    $1502 = +HEAPF64[$1501>>3];
    $1503 = $1441 * $1502;
    $1504 = $1500 - $1503;
    $1505 = $1439 * $1502;
    $1506 = $1441 * $1499;
    $1507 = $1506 + $1505;
    $1508 = (($op) + ($1461<<4)|0);
    $1509 = +HEAPF64[$1508>>3];
    $1510 = $1444 * $1509;
    $1511 = (((($op) + ($1461<<4)|0)) + 8|0);
    $1512 = +HEAPF64[$1511>>3];
    $1513 = $1446 * $1512;
    $1514 = $1510 - $1513;
    $1515 = $1444 * $1512;
    $1516 = $1446 * $1509;
    $1517 = $1516 + $1515;
    $1518 = (($op) + ($1462<<4)|0);
    $1519 = +HEAPF64[$1518>>3];
    $1520 = $1449 * $1519;
    $1521 = (((($op) + ($1462<<4)|0)) + 8|0);
    $1522 = +HEAPF64[$1521>>3];
    $1523 = $1451 * $1522;
    $1524 = $1520 - $1523;
    $1525 = $1449 * $1522;
    $1526 = $1451 * $1519;
    $1527 = $1526 + $1525;
    $1528 = (($op) + ($1463<<4)|0);
    $1529 = +HEAPF64[$1528>>3];
    $1530 = $1454 * $1529;
    $1531 = (((($op) + ($1463<<4)|0)) + 8|0);
    $1532 = +HEAPF64[$1531>>3];
    $1533 = $1456 * $1532;
    $1534 = $1530 - $1533;
    $1535 = $1454 * $1532;
    $1536 = $1456 * $1529;
    $1537 = $1536 + $1535;
    $1538 = $1465 + $1504;
    $1539 = $1465 - $1504;
    $1540 = $1467 + $1507;
    $1541 = $1467 - $1507;
    $1542 = $1474 + $1534;
    $1543 = $1474 - $1534;
    $1544 = $1477 + $1537;
    $1545 = $1477 - $1537;
    $1546 = $1494 + $1514;
    $1547 = $1494 - $1514;
    $1548 = $1497 - $1517;
    $1549 = $1497 + $1517;
    $1550 = $1484 + $1524;
    $1551 = $1484 - $1524;
    $1552 = $1487 - $1527;
    $1553 = $1487 + $1527;
    $1554 = $1538 + $1542;
    $1555 = $1546 + $1554;
    $1556 = $1550 + $1555;
    HEAPF64[$1464>>3] = $1556;
    $1557 = $1540 + $1544;
    $1558 = $1549 + $1557;
    $1559 = $1553 + $1558;
    HEAPF64[$1466>>3] = $1559;
    $1560 = $1538 - $1542;
    $1561 = $1560 - $1546;
    $1562 = $1550 + $1561;
    HEAPF64[$1498>>3] = $1562;
    $1563 = $1540 - $1544;
    $1564 = $1563 - $1549;
    $1565 = $1553 + $1564;
    HEAPF64[$1501>>3] = $1565;
    $1566 = $1542 - $1546;
    $1567 = $1544 - $1549;
    $1568 = $1547 + $1543;
    $1569 = $1548 + $1545;
    $1570 = $1566 * 0.70710678118654757;
    $1571 = $1539 + $1570;
    $1572 = $1567 * 0.70710678118654757;
    $1573 = $1541 + $1572;
    if ($1420) {
     $1574 = $1568 * -0.70710678118654757;
     $1575 = $1574 - $1551;
     $1576 = $1569 * -0.70710678118654757;
     $1577 = $1576 - $1552;
     $tau9i226$0 = $1577;$tau9r225$0 = $1575;
    } else {
     $1578 = $1568 * 0.70710678118654757;
     $1579 = $1551 + $1578;
     $1580 = $1569 * 0.70710678118654757;
     $1581 = $1552 + $1580;
     $tau9i226$0 = $1581;$tau9r225$0 = $1579;
    }
    $1582 = $1571 - $tau9i226$0;
    HEAPF64[$1468>>3] = $1582;
    $1583 = $1573 + $tau9r225$0;
    HEAPF64[$1471>>3] = $1583;
    $1584 = $1571 + $tau9i226$0;
    HEAPF64[$1528>>3] = $1584;
    $1585 = $1573 - $tau9r225$0;
    HEAPF64[$1531>>3] = $1585;
    $1586 = $1538 - $1550;
    $1587 = $1540 - $1553;
    $1588 = $1547 - $1543;
    $1589 = $1548 - $1545;
    $1590 = $1543 - $1547;
    $1591 = $1545 - $1548;
    $tau9r225$1 = $1420 ? $1588 : $1590;
    $tau9i226$1 = $1420 ? $1589 : $1591;
    $1592 = $1586 - $tau9i226$1;
    HEAPF64[$1478>>3] = $1592;
    $1593 = $1587 + $tau9r225$1;
    HEAPF64[$1481>>3] = $1593;
    $1594 = $1586 + $tau9i226$1;
    HEAPF64[$1518>>3] = $1594;
    $1595 = $1587 - $tau9r225$1;
    HEAPF64[$1521>>3] = $1595;
    $1596 = $1539 - $1570;
    $1597 = $1541 - $1572;
    $1598 = $1568 * 0.70710678118654757;
    if ($1420) {
     $1599 = $1551 - $1598;
     $1600 = $1569 * 0.70710678118654757;
     $1601 = $1552 - $1600;
     $tau9i226$2 = $1601;$tau9r225$2 = $1599;
    } else {
     $1602 = $1598 - $1551;
     $1603 = $1569 * 0.70710678118654757;
     $1604 = $1603 - $1552;
     $tau9i226$2 = $1604;$tau9r225$2 = $1602;
    }
    $1605 = $1596 - $tau9i226$2;
    HEAPF64[$1488>>3] = $1605;
    $1606 = $1597 + $tau9r225$2;
    HEAPF64[$1491>>3] = $1606;
    $1607 = $1596 + $tau9i226$2;
    HEAPF64[$1508>>3] = $1607;
    $1608 = $1597 - $tau9r225$2;
    HEAPF64[$1511>>3] = $1608;
    $1609 = (($k173$063) + 1)|0;
    $1610 = ($1609|0)<($1389|0);
    if ($1610) {
     $k173$063 = $1609;
    } else {
     break;
    }
   }
   return;
   break;
  }
  default: {
   $1611 = (($radix$0) + -1)|0;
   $1612 = $1611 << 3;
   $1613 = (_malloc($1612)|0);
   $1614 = (_malloc($1612)|0);
   $1615 = (_malloc($1612)|0);
   $1616 = (_malloc($1612)|0);
   $1617 = (_malloc($1612)|0);
   $1618 = (_malloc($1612)|0);
   $1619 = $radix$0 << 3;
   $1620 = (_malloc($1619)|0);
   $1621 = (_malloc($1619)|0);
   $1622 = (($N|0) / ($radix$0|0))&-1;
   $1623 = Math_imul($radix$0, $l)|0;
   $1624 = ($radix$0|0)>(0);
   if ($1624) {
    $1625 = (($inc) + 1)|0;
    $i$044 = 0;
    while(1) {
     $1626 = Math_imul($i$044, $1622)|0;
     $1627 = (($op) + ($1626<<4)|0);
     $1628 = Math_imul($i$044, $l)|0;
     $1629 = (($ip) + ($1628<<4)|0);
     _mixed_radix_dit_rec($1627,$1629,$obj,$sgn,$1622,$1623,$1625);
     $1630 = (($i$044) + 1)|0;
     $exitcond72 = ($1630|0)==($radix$0|0);
     if ($exitcond72) {
      break;
     } else {
      $i$044 = $1630;
     }
    }
   }
   $1631 = (($1611|0) / 2)&-1;
   $1632 = ($1611|0)>(1);
   if ($1632) {
    $1633 = (+($radix$0|0));
    $i$141 = 1;
    while(1) {
     $1636 = (+($i$141|0));
     $1637 = $1636 * 6.2831853071795862;
     $1638 = $1637 / $1633;
     $1639 = (+Math_cos((+$1638)));
     $1640 = (($i$141) + -1)|0;
     $1641 = (($1617) + ($1640<<3)|0);
     HEAPF64[$1641>>3] = $1639;
     $1642 = (+Math_sin((+$1638)));
     $1643 = (($1618) + ($1640<<3)|0);
     HEAPF64[$1643>>3] = $1642;
     $1644 = (($i$141) + 1)|0;
     $1645 = ($i$141|0)<($1631|0);
     if ($1645) {
      $i$141 = $1644;
     } else {
      break;
     }
    }
    $1634 = ($1611|0)>(1);
    if ($1634) {
     $1635 = (($1631) + -1)|0;
     $i$239 = 0;
     while(1) {
      $1655 = (($1635) - ($i$239))|0;
      $1656 = (($1618) + ($1655<<3)|0);
      $1657 = +HEAPF64[$1656>>3];
      $1658 = -$1657;
      $1659 = (($i$239) + ($1631))|0;
      $1660 = (($1618) + ($1659<<3)|0);
      HEAPF64[$1660>>3] = $1658;
      $1661 = (($1617) + ($1655<<3)|0);
      $1662 = +HEAPF64[$1661>>3];
      $1663 = (($1617) + ($1659<<3)|0);
      HEAPF64[$1663>>3] = $1662;
      $1664 = (($i$239) + 1)|0;
      $1665 = ($1664|0)<($1631|0);
      if ($1665) {
       $i$239 = $1664;
      } else {
       break;
      }
     }
    }
   }
   $1646 = ($1622|0)>(0);
   if ($1646) {
    $1647 = (($1622) + -1)|0;
    $1648 = ($radix$0|0)>(1);
    $1649 = ($1611|0)>(1);
    $1650 = ($1611|0)>(1);
    $1651 = ($1611|0)>(1);
    $1652 = ($1611|0)>(1);
    $1653 = (+($sgn|0));
    $1654 = (($radix$0) + -1)|0;
    $k233$035 = 0;
    while(1) {
     $1666 = (($op) + ($k233$035<<4)|0);
     $1667 = +HEAPF64[$1666>>3];
     HEAPF64[$1620>>3] = $1667;
     $1668 = (((($op) + ($k233$035<<4)|0)) + 8|0);
     $1669 = +HEAPF64[$1668>>3];
     HEAPF64[$1621>>3] = $1669;
     if ($1648) {
      $1670 = Math_imul($k233$035, $1611)|0;
      $1671 = (($1647) + ($1670))|0;
      $i$38 = 0;$ind234$09 = $1671;
      while(1) {
       $1672 = (((($obj)) + 272|0) + ($ind234$09<<4)|0);
       $1673 = +HEAPF64[$1672>>3];
       $1674 = (($1613) + ($i$38<<3)|0);
       HEAPF64[$1674>>3] = $1673;
       $1675 = (((((($obj)) + 272|0) + ($ind234$09<<4)|0)) + 8|0);
       $1676 = +HEAPF64[$1675>>3];
       $1677 = (($1614) + ($i$38<<3)|0);
       HEAPF64[$1677>>3] = $1676;
       $1678 = (($i$38) + 1)|0;
       $1679 = Math_imul($1678, $1622)|0;
       $1680 = (($1679) + ($k233$035))|0;
       $1681 = (($op) + ($1680<<4)|0);
       $1682 = +HEAPF64[$1681>>3];
       $1683 = +HEAPF64[$1674>>3];
       $1684 = $1682 * $1683;
       $1685 = (((($op) + ($1680<<4)|0)) + 8|0);
       $1686 = +HEAPF64[$1685>>3];
       $1687 = $1676 * $1686;
       $1688 = $1684 - $1687;
       $1689 = (($1620) + ($1678<<3)|0);
       HEAPF64[$1689>>3] = $1688;
       $1690 = +HEAPF64[$1685>>3];
       $1691 = +HEAPF64[$1674>>3];
       $1692 = $1690 * $1691;
       $1693 = +HEAPF64[$1681>>3];
       $1694 = +HEAPF64[$1677>>3];
       $1695 = $1693 * $1694;
       $1696 = $1692 + $1695;
       $1697 = (($1621) + ($1678<<3)|0);
       HEAPF64[$1697>>3] = $1696;
       $1698 = (($ind234$09) + 1)|0;
       $exitcond = ($1678|0)==($1654|0);
       if ($exitcond) {
        break;
       } else {
        $i$38 = $1678;$ind234$09 = $1698;
       }
      }
     }
     if ($1649) {
      $i$410 = 0;
      while(1) {
       $1699 = (($i$410) + 1)|0;
       $1700 = (($1620) + ($1699<<3)|0);
       $1701 = +HEAPF64[$1700>>3];
       $1702 = (($1611) - ($i$410))|0;
       $1703 = (($1620) + ($1702<<3)|0);
       $1704 = +HEAPF64[$1703>>3];
       $1705 = $1701 + $1704;
       $1706 = (($1615) + ($i$410<<3)|0);
       HEAPF64[$1706>>3] = $1705;
       $1707 = (($1621) + ($1699<<3)|0);
       $1708 = +HEAPF64[$1707>>3];
       $1709 = (($1621) + ($1702<<3)|0);
       $1710 = +HEAPF64[$1709>>3];
       $1711 = $1708 - $1710;
       $1712 = (($i$410) + ($1631))|0;
       $1713 = (($1616) + ($1712<<3)|0);
       HEAPF64[$1713>>3] = $1711;
       $1714 = +HEAPF64[$1707>>3];
       $1715 = +HEAPF64[$1709>>3];
       $1716 = $1714 + $1715;
       $1717 = (($1616) + ($i$410<<3)|0);
       HEAPF64[$1717>>3] = $1716;
       $1718 = +HEAPF64[$1700>>3];
       $1719 = +HEAPF64[$1703>>3];
       $1720 = $1718 - $1719;
       $1721 = (($1615) + ($1712<<3)|0);
       HEAPF64[$1721>>3] = $1720;
       $1722 = ($1699|0)<($1631|0);
       if ($1722) {
        $i$410 = $1699;
       } else {
        break;
       }
      }
     }
     $1723 = +HEAPF64[$1620>>3];
     $1724 = +HEAPF64[$1621>>3];
     if ($1650) {
      $i$512 = 0;$temp1i236$014 = $1724;$temp1r235$013 = $1723;
      while(1) {
       $1725 = (($1615) + ($i$512<<3)|0);
       $1726 = +HEAPF64[$1725>>3];
       $1727 = $temp1r235$013 + $1726;
       $1728 = (($1616) + ($i$512<<3)|0);
       $1729 = +HEAPF64[$1728>>3];
       $1730 = $temp1i236$014 + $1729;
       $1731 = (($i$512) + 1)|0;
       $1732 = ($1731|0)<($1631|0);
       if ($1732) {
        $i$512 = $1731;$temp1i236$014 = $1730;$temp1r235$013 = $1727;
       } else {
        $temp1i236$0$lcssa = $1730;$temp1r235$0$lcssa = $1727;
        break;
       }
      }
     } else {
      $temp1i236$0$lcssa = $1724;$temp1r235$0$lcssa = $1723;
     }
     HEAPF64[$1666>>3] = $temp1r235$0$lcssa;
     HEAPF64[$1668>>3] = $temp1i236$0$lcssa;
     if ($1651) {
      $1733 = +HEAPF64[$1620>>3];
      $1734 = +HEAPF64[$1621>>3];
      $u$031 = 0;
      while(1) {
       if ($1652) {
        $1735 = (($u$031) + 1)|0;
        $temp1i236$121 = $1734;$temp1r235$120 = $1733;$temp2i238$023 = 0.0;$temp2r237$022 = 0.0;$v$019 = 0;
        while(1) {
         $1736 = (($v$019) + 1)|0;
         $1737 = Math_imul($1736, $1735)|0;
         $t$0 = $1737;
         while(1) {
          $1738 = ($t$0|0)<($radix$0|0);
          $1739 = (($t$0) - ($radix$0))|0;
          if ($1738) {
           $t$0$lcssa = $t$0;
           break;
          } else {
           $t$0 = $1739;
          }
         }
         $1740 = (($t$0$lcssa) + -1)|0;
         $1741 = (($1617) + ($1740<<3)|0);
         $1742 = +HEAPF64[$1741>>3];
         $1743 = (($1615) + ($v$019<<3)|0);
         $1744 = +HEAPF64[$1743>>3];
         $1745 = $1742 * $1744;
         $1746 = $temp1r235$120 + $1745;
         $1747 = (($1616) + ($v$019<<3)|0);
         $1748 = +HEAPF64[$1747>>3];
         $1749 = $1742 * $1748;
         $1750 = $temp1i236$121 + $1749;
         $1751 = (($1618) + ($1740<<3)|0);
         $1752 = +HEAPF64[$1751>>3];
         $1753 = (($v$019) + ($1631))|0;
         $1754 = (($1615) + ($1753<<3)|0);
         $1755 = +HEAPF64[$1754>>3];
         $1756 = $1752 * $1755;
         $1757 = $temp2r237$022 - $1756;
         $1758 = (($1616) + ($1753<<3)|0);
         $1759 = +HEAPF64[$1758>>3];
         $1760 = $1752 * $1759;
         $1761 = $temp2i238$023 - $1760;
         $1762 = ($1736|0)<($1631|0);
         if ($1762) {
          $temp1i236$121 = $1750;$temp1r235$120 = $1746;$temp2i238$023 = $1761;$temp2r237$022 = $1757;$v$019 = $1736;
         } else {
          $temp1i236$1$lcssa = $1750;$temp1r235$1$lcssa = $1746;$temp2i238$0$lcssa = $1761;$temp2r237$0$lcssa = $1757;
          break;
         }
        }
       } else {
        $temp1i236$1$lcssa = $1734;$temp1r235$1$lcssa = $1733;$temp2i238$0$lcssa = 0.0;$temp2r237$0$lcssa = 0.0;
       }
       $1763 = $1653 * $temp2r237$0$lcssa;
       $1764 = $1653 * $temp2i238$0$lcssa;
       $1765 = $temp1r235$1$lcssa - $1764;
       $1766 = (($u$031) + 1)|0;
       $1767 = Math_imul($1766, $1622)|0;
       $1768 = (($1767) + ($k233$035))|0;
       $1769 = (($op) + ($1768<<4)|0);
       HEAPF64[$1769>>3] = $1765;
       $1770 = $temp1i236$1$lcssa + $1763;
       $1771 = (((($op) + ($1768<<4)|0)) + 8|0);
       HEAPF64[$1771>>3] = $1770;
       $1772 = $temp1r235$1$lcssa + $1764;
       $1773 = (($1654) - ($u$031))|0;
       $1774 = Math_imul($1773, $1622)|0;
       $1775 = (($1774) + ($k233$035))|0;
       $1776 = (($op) + ($1775<<4)|0);
       HEAPF64[$1776>>3] = $1772;
       $1777 = $temp1i236$1$lcssa - $1763;
       $1778 = (((($op) + ($1775<<4)|0)) + 8|0);
       HEAPF64[$1778>>3] = $1777;
       $1779 = ($1766|0)<($1631|0);
       if ($1779) {
        $u$031 = $1766;
       } else {
        break;
       }
      }
     }
     $1780 = (($k233$035) + 1)|0;
     $exitcond71 = ($1780|0)==($1622|0);
     if ($exitcond71) {
      break;
     } else {
      $k233$035 = $1780;
     }
    }
   }
   _free($1613);
   _free($1614);
   _free($1615);
   _free($1616);
   _free($1617);
   _free($1618);
   _free($1620);
   _free($1621);
   return;
  }
  }
 }
 }
}
function _bluestein_fft($data,$oup,$obj,$sgn,$N) {
 $data = $data|0;
 $oup = $oup|0;
 $obj = $obj|0;
 $sgn = $sgn|0;
 $N = $N|0;
 var $0 = 0, $1 = 0.0, $10 = 0, $100 = 0.0, $101 = 0, $102 = 0.0, $103 = 0, $104 = 0.0, $105 = 0.0, $106 = 0.0, $107 = 0.0, $108 = 0.0, $109 = 0.0, $11 = 0, $110 = 0, $111 = 0, $112 = 0.0, $113 = 0.0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0.0, $12 = 0, $120 = 0, $121 = 0.0, $122 = 0.0, $123 = 0, $124 = 0.0, $125 = 0, $126 = 0.0, $127 = 0.0, $128 = 0.0, $129 = 0, $13 = 0, $130 = 0.0, $131 = 0.0, $132 = 0.0, $133 = 0.0;
 var $134 = 0.0, $135 = 0.0, $136 = 0.0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0.0, $141 = 0, $142 = 0.0, $143 = 0.0, $144 = 0, $145 = 0.0, $146 = 0, $147 = 0.0, $148 = 0.0, $149 = 0.0, $15 = 0, $150 = 0, $151 = 0.0;
 var $152 = 0.0, $153 = 0.0, $154 = 0.0, $155 = 0.0, $156 = 0.0, $157 = 0.0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0.0, $163 = 0.0, $164 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0.0, $20 = 0.0, $21 = 0.0;
 var $22 = 0, $23 = 0, $24 = 0.0, $25 = 0.0, $26 = 0, $27 = 0.0, $28 = 0.0, $29 = 0, $3 = 0.0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0.0, $35 = 0, $36 = 0.0, $37 = 0.0, $38 = 0, $39 = 0.0, $4 = 0.0;
 var $40 = 0, $41 = 0.0, $42 = 0.0, $43 = 0.0, $44 = 0, $45 = 0.0, $46 = 0.0, $47 = 0.0, $48 = 0.0, $49 = 0.0, $5 = 0, $50 = 0.0, $51 = 0.0, $52 = 0, $53 = 0, $54 = 0, $55 = 0.0, $56 = 0, $57 = 0.0, $58 = 0.0;
 var $59 = 0, $6 = 0, $60 = 0.0, $61 = 0, $62 = 0.0, $63 = 0.0, $64 = 0.0, $65 = 0, $66 = 0.0, $67 = 0.0, $68 = 0.0, $69 = 0.0, $7 = 0, $70 = 0.0, $71 = 0.0, $72 = 0.0, $73 = 0, $74 = 0, $75 = 0, $76 = 0;
 var $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0.0, $83 = 0, $84 = 0.0, $85 = 0.0, $86 = 0, $87 = 0.0, $88 = 0, $89 = 0.0, $9 = 0, $90 = 0.0, $91 = 0.0, $92 = 0.0, $93 = 0.0, $94 = 0.0;
 var $95 = 0, $96 = 0, $97 = 0.0, $98 = 0, $99 = 0.0, $M$0 = 0, $exitcond = 0, $exitcond34 = 0, $exitcond35 = 0, $exitcond36 = 0, $exitcond37 = 0, $exitcond38 = 0, $exitcond39 = 0, $exitcond40 = 0, $exitcond41 = 0, $exitcond42 = 0, $exp2 = 0.0, $i$026 = 0, $i$128 = 0, $i$223 = 0;
 var $i$319 = 0, $i$421 = 0, $i$512 = 0, $i$614 = 0, $ii$030 = 0, $ii$116 = 0, $ii$211 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ((($obj)) + 268|0);
 HEAP32[$0>>2] = 0;
 $1 = (+($N|0));
 $2 = (+_log10($1));
 $3 = $2 / 0.3010299956639812;
 $4 = (+Math_ceil((+$3)));
 $exp2 = (+_exp2($4));
 $5 = (~~(($exp2)));
 $6 = ((($obj)) + 4|0);
 $7 = HEAP32[$6>>2]|0;
 $8 = HEAP32[$obj>>2]|0;
 $9 = $N << 1;
 $10 = (($9) + -2)|0;
 $11 = ($5|0)<($10|0);
 $12 = $11&1;
 $M$0 = $5 << $12;
 HEAP32[$obj>>2] = $M$0;
 $13 = $M$0 << 4;
 $14 = (_malloc($13)|0);
 $15 = (_malloc($13)|0);
 $16 = (_malloc($13)|0);
 $17 = (_malloc($13)|0);
 $18 = $N << 4;
 $19 = (_malloc($18)|0);
 _bluestein_exp($16,$19,$N,$M$0);
 $20 = (+($M$0|0));
 $21 = 1.0 / $20;
 $22 = ($M$0|0)>(0);
 if ($22) {
  $ii$030 = 0;
  while(1) {
   $23 = (((($16) + ($ii$030<<4)|0)) + 8|0);
   $24 = +HEAPF64[$23>>3];
   $25 = $21 * $24;
   HEAPF64[$23>>3] = $25;
   $26 = (($16) + ($ii$030<<4)|0);
   $27 = +HEAPF64[$26>>3];
   $28 = $21 * $27;
   HEAPF64[$26>>3] = $28;
   $29 = (($ii$030) + 1)|0;
   $exitcond42 = ($29|0)==($M$0|0);
   if ($exitcond42) {
    break;
   } else {
    $ii$030 = $29;
   }
  }
 }
 _fft_exec($obj,$16,$15);
 $30 = ($sgn|0)==(1);
 $31 = ($N|0)>(0);
 if ($30) {
  if ($31) {
   $i$026 = 0;
   while(1) {
    $33 = (($data) + ($i$026<<4)|0);
    $34 = +HEAPF64[$33>>3];
    $35 = (($19) + ($i$026<<4)|0);
    $36 = +HEAPF64[$35>>3];
    $37 = $34 * $36;
    $38 = (((($data) + ($i$026<<4)|0)) + 8|0);
    $39 = +HEAPF64[$38>>3];
    $40 = (((($19) + ($i$026<<4)|0)) + 8|0);
    $41 = +HEAPF64[$40>>3];
    $42 = $39 * $41;
    $43 = $37 + $42;
    $44 = (($16) + ($i$026<<4)|0);
    HEAPF64[$44>>3] = $43;
    $45 = +HEAPF64[$33>>3];
    $46 = +HEAPF64[$40>>3];
    $47 = $45 * $46;
    $48 = +HEAPF64[$38>>3];
    $49 = +HEAPF64[$35>>3];
    $50 = $48 * $49;
    $51 = $50 - $47;
    $52 = (((($16) + ($i$026<<4)|0)) + 8|0);
    HEAPF64[$52>>3] = $51;
    $53 = (($i$026) + 1)|0;
    $exitcond40 = ($53|0)==($N|0);
    if ($exitcond40) {
     break;
    } else {
     $i$026 = $53;
    }
   }
  }
 } else {
  if ($31) {
   $i$128 = 0;
   while(1) {
    $54 = (($data) + ($i$128<<4)|0);
    $55 = +HEAPF64[$54>>3];
    $56 = (($19) + ($i$128<<4)|0);
    $57 = +HEAPF64[$56>>3];
    $58 = $55 * $57;
    $59 = (((($data) + ($i$128<<4)|0)) + 8|0);
    $60 = +HEAPF64[$59>>3];
    $61 = (((($19) + ($i$128<<4)|0)) + 8|0);
    $62 = +HEAPF64[$61>>3];
    $63 = $60 * $62;
    $64 = $58 - $63;
    $65 = (($16) + ($i$128<<4)|0);
    HEAPF64[$65>>3] = $64;
    $66 = +HEAPF64[$54>>3];
    $67 = +HEAPF64[$61>>3];
    $68 = $66 * $67;
    $69 = +HEAPF64[$59>>3];
    $70 = +HEAPF64[$56>>3];
    $71 = $69 * $70;
    $72 = $68 + $71;
    $73 = (((($16) + ($i$128<<4)|0)) + 8|0);
    HEAPF64[$73>>3] = $72;
    $74 = (($i$128) + 1)|0;
    $exitcond41 = ($74|0)==($N|0);
    if ($exitcond41) {
     break;
    } else {
     $i$128 = $74;
    }
   }
  }
 }
 $32 = ($M$0|0)>($N|0);
 if ($32) {
  $i$223 = $N;
  while(1) {
   $75 = (($16) + ($i$223<<4)|0);
   $76 = (($i$223) + 1)|0;
   $exitcond39 = ($76|0)==($M$0|0);
   ;HEAP32[$75>>2]=0|0;HEAP32[$75+4>>2]=0|0;HEAP32[$75+8>>2]=0|0;HEAP32[$75+12>>2]=0|0;
   if ($exitcond39) {
    break;
   } else {
    $i$223 = $76;
   }
  }
 }
 _fft_exec($obj,$16,$14);
 $77 = ($M$0|0)>(0);
 do {
  if ($30) {
   if ($77) {
    $i$319 = 0;
    while(1) {
     $81 = (($14) + ($i$319<<4)|0);
     $82 = +HEAPF64[$81>>3];
     $83 = (($15) + ($i$319<<4)|0);
     $84 = +HEAPF64[$83>>3];
     $85 = $82 * $84;
     $86 = (((($14) + ($i$319<<4)|0)) + 8|0);
     $87 = +HEAPF64[$86>>3];
     $88 = (((($15) + ($i$319<<4)|0)) + 8|0);
     $89 = +HEAPF64[$88>>3];
     $90 = $87 * $89;
     $91 = $85 - $90;
     $92 = $82 * $89;
     $93 = $84 * $87;
     $94 = $93 + $92;
     HEAPF64[$86>>3] = $94;
     HEAPF64[$81>>3] = $91;
     $95 = (($i$319) + 1)|0;
     $exitcond37 = ($95|0)==($M$0|0);
     if ($exitcond37) {
      label = 15;
      break;
     } else {
      $i$319 = $95;
     }
    }
   } else {
    $79 = (0 - ($sgn))|0;
    HEAP32[$6>>2] = $79;
    _fft_exec($obj,$14,$17);
    label = 21;
    break;
   }
  } else {
   if ($77) {
    $i$421 = 0;
    while(1) {
     $96 = (($14) + ($i$421<<4)|0);
     $97 = +HEAPF64[$96>>3];
     $98 = (($15) + ($i$421<<4)|0);
     $99 = +HEAPF64[$98>>3];
     $100 = $97 * $99;
     $101 = (((($14) + ($i$421<<4)|0)) + 8|0);
     $102 = +HEAPF64[$101>>3];
     $103 = (((($15) + ($i$421<<4)|0)) + 8|0);
     $104 = +HEAPF64[$103>>3];
     $105 = $102 * $104;
     $106 = $100 + $105;
     $107 = $97 * $104;
     $108 = $99 * $102;
     $109 = $108 - $107;
     HEAPF64[$101>>3] = $109;
     HEAPF64[$96>>3] = $106;
     $110 = (($i$421) + 1)|0;
     $exitcond38 = ($110|0)==($M$0|0);
     if ($exitcond38) {
      label = 15;
      break;
     } else {
      $i$421 = $110;
     }
    }
   } else {
    $78 = (0 - ($sgn))|0;
    HEAP32[$6>>2] = $78;
    _fft_exec($obj,$14,$17);
    label = 20;
    break;
   }
  }
 } while(0);
 if ((label|0) == 15) {
  $80 = ($M$0|0)>(0);
  if ($80) {
   $ii$116 = 0;
   while(1) {
    $111 = (((((($obj)) + 272|0) + ($ii$116<<4)|0)) + 8|0);
    $112 = +HEAPF64[$111>>3];
    $113 = -$112;
    HEAPF64[$111>>3] = $113;
    $114 = (($ii$116) + 1)|0;
    $exitcond36 = ($114|0)==($M$0|0);
    if ($exitcond36) {
     break;
    } else {
     $ii$116 = $114;
    }
   }
  }
  $115 = (0 - ($sgn))|0;
  HEAP32[$6>>2] = $115;
  _fft_exec($obj,$14,$17);
  if ($30) {
   label = 21;
  } else {
   label = 20;
  }
 }
 if ((label|0) == 20) {
  $116 = ($N|0)>(0);
  if ($116) {
   $i$614 = 0;
   while(1) {
    $139 = (($17) + ($i$614<<4)|0);
    $140 = +HEAPF64[$139>>3];
    $141 = (($19) + ($i$614<<4)|0);
    $142 = +HEAPF64[$141>>3];
    $143 = $140 * $142;
    $144 = (((($17) + ($i$614<<4)|0)) + 8|0);
    $145 = +HEAPF64[$144>>3];
    $146 = (((($19) + ($i$614<<4)|0)) + 8|0);
    $147 = +HEAPF64[$146>>3];
    $148 = $145 * $147;
    $149 = $143 - $148;
    $150 = (($oup) + ($i$614<<4)|0);
    HEAPF64[$150>>3] = $149;
    $151 = +HEAPF64[$139>>3];
    $152 = +HEAPF64[$146>>3];
    $153 = $151 * $152;
    $154 = +HEAPF64[$144>>3];
    $155 = +HEAPF64[$141>>3];
    $156 = $154 * $155;
    $157 = $153 + $156;
    $158 = (((($oup) + ($i$614<<4)|0)) + 8|0);
    HEAPF64[$158>>3] = $157;
    $159 = (($i$614) + 1)|0;
    $exitcond35 = ($159|0)==($N|0);
    if ($exitcond35) {
     break;
    } else {
     $i$614 = $159;
    }
   }
  }
 }
 else if ((label|0) == 21) {
  $117 = ($N|0)>(0);
  if ($117) {
   $i$512 = 0;
   while(1) {
    $118 = (($17) + ($i$512<<4)|0);
    $119 = +HEAPF64[$118>>3];
    $120 = (($19) + ($i$512<<4)|0);
    $121 = +HEAPF64[$120>>3];
    $122 = $119 * $121;
    $123 = (((($17) + ($i$512<<4)|0)) + 8|0);
    $124 = +HEAPF64[$123>>3];
    $125 = (((($19) + ($i$512<<4)|0)) + 8|0);
    $126 = +HEAPF64[$125>>3];
    $127 = $124 * $126;
    $128 = $122 + $127;
    $129 = (($oup) + ($i$512<<4)|0);
    HEAPF64[$129>>3] = $128;
    $130 = +HEAPF64[$118>>3];
    $131 = +HEAPF64[$125>>3];
    $132 = $130 * $131;
    $133 = +HEAPF64[$123>>3];
    $134 = +HEAPF64[$120>>3];
    $135 = $133 * $134;
    $136 = $135 - $132;
    $137 = (((($oup) + ($i$512<<4)|0)) + 8|0);
    HEAPF64[$137>>3] = $136;
    $138 = (($i$512) + 1)|0;
    $exitcond34 = ($138|0)==($N|0);
    if ($exitcond34) {
     break;
    } else {
     $i$512 = $138;
    }
   }
  }
 }
 HEAP32[$6>>2] = $7;
 HEAP32[$obj>>2] = $8;
 HEAP32[$0>>2] = 1;
 $160 = ($M$0|0)>(0);
 if ($160) {
  $ii$211 = 0;
 } else {
  _free($14);
  _free($17);
  _free($16);
  _free($15);
  _free($19);
  return;
 }
 while(1) {
  $161 = (((((($obj)) + 272|0) + ($ii$211<<4)|0)) + 8|0);
  $162 = +HEAPF64[$161>>3];
  $163 = -$162;
  HEAPF64[$161>>3] = $163;
  $164 = (($ii$211) + 1)|0;
  $exitcond = ($164|0)==($M$0|0);
  if ($exitcond) {
   break;
  } else {
   $ii$211 = $164;
  }
 }
 _free($14);
 _free($17);
 _free($16);
 _free($15);
 _free($19);
 return;
}
function _bluestein_exp($hl,$hlt,$len,$M) {
 $hl = $hl|0;
 $hlt = $hlt|0;
 $len = $len|0;
 $M = $M|0;
 var $0 = 0.0, $1 = 0.0, $10 = 0, $11 = 0.0, $12 = 0, $13 = 0.0, $14 = 0, $15 = 0.0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0.0, $3 = 0, $30 = 0, $31 = 0, $32 = 0.0, $33 = 0, $34 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0.0, $8 = 0.0, $9 = 0.0, $exitcond = 0, $exitcond9 = 0, $i$06 = 0, $i$13 = 0, $i$22 = 0;
 var $l2$05 = 0, $l2$1 = 0, $l2$1$lcssa = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (+($len|0));
 $1 = 3.1415926535897931 / $0;
 $2 = $len << 1;
 $3 = ($len|0)>(0);
 if ($3) {
  $i$06 = 0;$l2$05 = 0;
  while(1) {
   $7 = (+($l2$05|0));
   $8 = $1 * $7;
   $9 = (+Math_cos((+$8)));
   $10 = (($hlt) + ($i$06<<4)|0);
   HEAPF64[$10>>3] = $9;
   $11 = (+Math_sin((+$8)));
   $12 = (((($hlt) + ($i$06<<4)|0)) + 8|0);
   HEAPF64[$12>>3] = $11;
   $13 = +HEAPF64[$10>>3];
   $14 = (($hl) + ($i$06<<4)|0);
   HEAPF64[$14>>3] = $13;
   $15 = +HEAPF64[$12>>3];
   $16 = (((($hl) + ($i$06<<4)|0)) + 8|0);
   HEAPF64[$16>>3] = $15;
   $17 = $i$06 << 1;
   $18 = $17 | 1;
   $19 = (($18) + ($l2$05))|0;
   $l2$1 = $19;
   while(1) {
    $20 = ($l2$1|0)>($2|0);
    $21 = (($l2$1) - ($2))|0;
    if ($20) {
     $l2$1 = $21;
    } else {
     $l2$1$lcssa = $l2$1;
     break;
    }
   }
   $22 = (($i$06) + 1)|0;
   $exitcond9 = ($22|0)==($len|0);
   if ($exitcond9) {
    break;
   } else {
    $i$06 = $22;$l2$05 = $l2$1$lcssa;
   }
  }
 }
 $4 = (($M) - ($len))|0;
 $5 = (($4) + 1)|0;
 $6 = ($4|0)<($len|0);
 if (!($6)) {
  $i$13 = $len;
  while(1) {
   $24 = (($hl) + ($i$13<<4)|0);
   $25 = (($i$13) + 1)|0;
   $26 = ($25|0)<($5|0);
   ;HEAP32[$24>>2]=0|0;HEAP32[$24+4>>2]=0|0;HEAP32[$24+8>>2]=0|0;HEAP32[$24+12>>2]=0|0;
   if ($26) {
    $i$13 = $25;
   } else {
    break;
   }
  }
 }
 $23 = ($5|0)<($M|0);
 if ($23) {
  $i$22 = $5;
 } else {
  return;
 }
 while(1) {
  $27 = (($M) - ($i$22))|0;
  $28 = (($hlt) + ($27<<4)|0);
  $29 = +HEAPF64[$28>>3];
  $30 = (($hl) + ($i$22<<4)|0);
  HEAPF64[$30>>3] = $29;
  $31 = (((($hlt) + ($27<<4)|0)) + 8|0);
  $32 = +HEAPF64[$31>>3];
  $33 = (((($hl) + ($i$22<<4)|0)) + 8|0);
  HEAPF64[$33>>3] = $32;
  $34 = (($i$22) + 1)|0;
  $exitcond = ($34|0)==($M|0);
  if ($exitcond) {
   break;
  } else {
   $i$22 = $34;
  }
 }
 return;
}
function _fft_real_init($N,$sgn) {
 $N = $N|0;
 $sgn = $sgn|0;
 var $0 = 0, $1 = 0, $10 = 0.0, $11 = 0.0, $12 = 0, $13 = 0.0, $14 = 0, $15 = 0, $16 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0.0, $7 = 0, $8 = 0.0, $9 = 0.0, $k$01 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (($N|0) / 2)&-1;
 $1 = $0 << 4;
 $2 = (($1) + 24)|0;
 $3 = (_malloc($2)|0);
 $4 = (_fft_init($0,$sgn)|0);
 HEAP32[$3>>2] = $4;
 $5 = ($N|0)>(1);
 if (!($5)) {
  return ($3|0);
 }
 $6 = (+($N|0));
 $7 = ((($3)) + 8|0);
 $k$01 = 0;
 while(1) {
  $8 = (+($k$01|0));
  $9 = $8 * 6.2831853071795862;
  $10 = $9 / $6;
  $11 = (+Math_cos((+$10)));
  $12 = (($7) + ($k$01<<4)|0);
  HEAPF64[$12>>3] = $11;
  $13 = (+Math_sin((+$10)));
  $14 = (((($7) + ($k$01<<4)|0)) + 8|0);
  HEAPF64[$14>>3] = $13;
  $15 = (($k$01) + 1)|0;
  $16 = ($15|0)<($0|0);
  if ($16) {
   $k$01 = $15;
  } else {
   break;
  }
 }
 return ($3|0);
}
function _fft_r2c_exec($obj,$inp,$oup) {
 $obj = $obj|0;
 $inp = $inp|0;
 $oup = $oup|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0.0, $14 = 0, $15 = 0, $16 = 0, $17 = 0.0, $18 = 0, $19 = 0.0, $2 = 0, $20 = 0.0, $21 = 0, $22 = 0, $23 = 0, $24 = 0.0, $25 = 0, $26 = 0;
 var $27 = 0.0, $28 = 0.0, $29 = 0, $3 = 0, $30 = 0.0, $31 = 0, $32 = 0.0, $33 = 0.0, $34 = 0.0, $35 = 0, $36 = 0.0, $37 = 0.0, $38 = 0.0, $39 = 0, $4 = 0, $40 = 0.0, $41 = 0.0, $42 = 0.0, $43 = 0.0, $44 = 0;
 var $45 = 0.0, $46 = 0.0, $47 = 0.0, $48 = 0.0, $49 = 0.0, $5 = 0, $50 = 0.0, $51 = 0.0, $52 = 0.0, $53 = 0.0, $54 = 0.0, $55 = 0, $56 = 0, $57 = 0.0, $58 = 0.0, $59 = 0.0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0.0, $65 = 0, $66 = 0, $67 = 0, $68 = 0.0, $69 = 0.0, $7 = 0, $70 = 0, $71 = 0, $8 = 0, $9 = 0.0, $exitcond = 0, $exitcond10 = 0, $exitcond11 = 0, $i$06 = 0, $i$12 = 0, $i$21 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[$obj>>2]|0;
 $1 = HEAP32[$0>>2]|0;
 $2 = $1 << 1;
 $3 = $1 << 4;
 $4 = (_malloc($3)|0);
 $5 = (_malloc($3)|0);
 $6 = ($1|0)>(0);
 if ($6) {
  $i$06 = 0;
  while(1) {
   $7 = $i$06 << 1;
   $8 = (($inp) + ($7<<3)|0);
   $9 = +HEAPF64[$8>>3];
   $10 = (($4) + ($i$06<<4)|0);
   HEAPF64[$10>>3] = $9;
   $11 = $7 | 1;
   $12 = (($inp) + ($11<<3)|0);
   $13 = +HEAPF64[$12>>3];
   $14 = (((($4) + ($i$06<<4)|0)) + 8|0);
   HEAPF64[$14>>3] = $13;
   $15 = (($i$06) + 1)|0;
   $exitcond11 = ($15|0)==($1|0);
   if ($exitcond11) {
    break;
   } else {
    $i$06 = $15;
   }
  }
 }
 $16 = HEAP32[$obj>>2]|0;
 _fft_exec($16,$4,$5);
 $17 = +HEAPF64[$5>>3];
 $18 = ((($5)) + 8|0);
 $19 = +HEAPF64[$18>>3];
 $20 = $17 + $19;
 HEAPF64[$oup>>3] = $20;
 $21 = ((($oup)) + 8|0);
 HEAPF64[$21>>3] = 0.0;
 $22 = ($1|0)>(1);
 if ($22) {
  $i$12 = 1;
  while(1) {
   $23 = (((($5) + ($i$12<<4)|0)) + 8|0);
   $24 = +HEAPF64[$23>>3];
   $25 = (($1) - ($i$12))|0;
   $26 = (((($5) + ($25<<4)|0)) + 8|0);
   $27 = +HEAPF64[$26>>3];
   $28 = $24 + $27;
   $29 = (($5) + ($25<<4)|0);
   $30 = +HEAPF64[$29>>3];
   $31 = (($5) + ($i$12<<4)|0);
   $32 = +HEAPF64[$31>>3];
   $33 = $30 - $32;
   $34 = $30 + $32;
   $35 = (((($obj)) + 8|0) + ($i$12<<4)|0);
   $36 = +HEAPF64[$35>>3];
   $37 = $28 * $36;
   $38 = $34 + $37;
   $39 = (((((($obj)) + 8|0) + ($i$12<<4)|0)) + 8|0);
   $40 = +HEAPF64[$39>>3];
   $41 = $33 * $40;
   $42 = $38 + $41;
   $43 = $42 * 0.5;
   $44 = (($oup) + ($i$12<<4)|0);
   HEAPF64[$44>>3] = $43;
   $45 = +HEAPF64[$23>>3];
   $46 = +HEAPF64[$26>>3];
   $47 = $45 - $46;
   $48 = +HEAPF64[$35>>3];
   $49 = $33 * $48;
   $50 = $47 + $49;
   $51 = +HEAPF64[$39>>3];
   $52 = $28 * $51;
   $53 = $50 - $52;
   $54 = $53 * 0.5;
   $55 = (((($oup) + ($i$12<<4)|0)) + 8|0);
   HEAPF64[$55>>3] = $54;
   $56 = (($i$12) + 1)|0;
   $exitcond10 = ($56|0)==($1|0);
   if ($exitcond10) {
    break;
   } else {
    $i$12 = $56;
   }
  }
 }
 $57 = +HEAPF64[$5>>3];
 $58 = +HEAPF64[$18>>3];
 $59 = $57 - $58;
 $60 = (($oup) + ($1<<4)|0);
 HEAPF64[$60>>3] = $59;
 $61 = (((($oup) + ($1<<4)|0)) + 8|0);
 HEAPF64[$61>>3] = 0.0;
 $62 = ($1|0)>(1);
 if ($62) {
  $i$21 = 1;
 } else {
  _free($4);
  _free($5);
  return;
 }
 while(1) {
  $63 = (($oup) + ($i$21<<4)|0);
  $64 = +HEAPF64[$63>>3];
  $65 = (($2) - ($i$21))|0;
  $66 = (($oup) + ($65<<4)|0);
  HEAPF64[$66>>3] = $64;
  $67 = (((($oup) + ($i$21<<4)|0)) + 8|0);
  $68 = +HEAPF64[$67>>3];
  $69 = -$68;
  $70 = (((($oup) + ($65<<4)|0)) + 8|0);
  HEAPF64[$70>>3] = $69;
  $71 = (($i$21) + 1)|0;
  $exitcond = ($71|0)==($1|0);
  if ($exitcond) {
   break;
  } else {
   $i$21 = $71;
  }
 }
 _free($4);
 _free($5);
 return;
}
function _fft_c2r_exec($obj,$inp,$oup) {
 $obj = $obj|0;
 $inp = $inp|0;
 $oup = $oup|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0.0, $13 = 0.0, $14 = 0, $15 = 0.0, $16 = 0, $17 = 0.0, $18 = 0.0, $19 = 0.0, $2 = 0, $20 = 0, $21 = 0.0, $22 = 0.0, $23 = 0.0, $24 = 0, $25 = 0.0, $26 = 0.0;
 var $27 = 0.0, $28 = 0, $29 = 0.0, $3 = 0, $30 = 0.0, $31 = 0.0, $32 = 0.0, $33 = 0.0, $34 = 0.0, $35 = 0.0, $36 = 0.0, $37 = 0.0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0.0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0.0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $6 = 0, $7 = 0, $8 = 0.0, $9 = 0.0, $exitcond = 0, $exitcond6 = 0, $i$02 = 0, $i$11 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[$obj>>2]|0;
 $1 = HEAP32[$0>>2]|0;
 $2 = $1 << 4;
 $3 = (_malloc($2)|0);
 $4 = (_malloc($2)|0);
 $5 = ($1|0)>(0);
 if ($5) {
  $i$02 = 0;
 } else {
  $6 = HEAP32[$obj>>2]|0;
  _fft_exec($6,$3,$4);
  _free($3);
  _free($4);
  return;
 }
 while(1) {
  $7 = (((($inp) + ($i$02<<4)|0)) + 8|0);
  $8 = +HEAPF64[$7>>3];
  $9 = -$8;
  $10 = (($1) - ($i$02))|0;
  $11 = (((($inp) + ($10<<4)|0)) + 8|0);
  $12 = +HEAPF64[$11>>3];
  $13 = $9 - $12;
  $14 = (($inp) + ($10<<4)|0);
  $15 = +HEAPF64[$14>>3];
  $16 = (($inp) + ($i$02<<4)|0);
  $17 = +HEAPF64[$16>>3];
  $18 = $17 - $15;
  $19 = $15 + $17;
  $20 = (((($obj)) + 8|0) + ($i$02<<4)|0);
  $21 = +HEAPF64[$20>>3];
  $22 = $13 * $21;
  $23 = $19 + $22;
  $24 = (((((($obj)) + 8|0) + ($i$02<<4)|0)) + 8|0);
  $25 = +HEAPF64[$24>>3];
  $26 = $18 * $25;
  $27 = $23 - $26;
  $28 = (($3) + ($i$02<<4)|0);
  HEAPF64[$28>>3] = $27;
  $29 = +HEAPF64[$7>>3];
  $30 = +HEAPF64[$11>>3];
  $31 = $29 - $30;
  $32 = +HEAPF64[$20>>3];
  $33 = $18 * $32;
  $34 = $31 + $33;
  $35 = +HEAPF64[$24>>3];
  $36 = $13 * $35;
  $37 = $34 + $36;
  $38 = (((($3) + ($i$02<<4)|0)) + 8|0);
  HEAPF64[$38>>3] = $37;
  $39 = (($i$02) + 1)|0;
  $exitcond6 = ($39|0)==($1|0);
  if ($exitcond6) {
   break;
  } else {
   $i$02 = $39;
  }
 }
 $40 = HEAP32[$obj>>2]|0;
 _fft_exec($40,$3,$4);
 $41 = ($1|0)>(0);
 if ($41) {
  $i$11 = 0;
 } else {
  _free($3);
  _free($4);
  return;
 }
 while(1) {
  $42 = (($4) + ($i$11<<4)|0);
  $43 = +HEAPF64[$42>>3];
  $44 = $i$11 << 1;
  $45 = (($oup) + ($44<<3)|0);
  HEAPF64[$45>>3] = $43;
  $46 = (((($4) + ($i$11<<4)|0)) + 8|0);
  $47 = +HEAPF64[$46>>3];
  $48 = $44 | 1;
  $49 = (($oup) + ($48<<3)|0);
  HEAPF64[$49>>3] = $47;
  $50 = (($i$11) + 1)|0;
  $exitcond = ($50|0)==($1|0);
  if ($exitcond) {
   break;
  } else {
   $i$11 = $50;
  }
 }
 _free($3);
 _free($4);
 return;
}
function _factorf($M) {
 $M = $M|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 var $N$0$lcssa = 0, $N$010 = 0, $N$1$lcssa = 0, $N$17 = 0, $N$2$lcssa = 0, $N$24 = 0, $N$3$lcssa = 0, $N$33 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (($M|0) % 7)&-1;
 $1 = ($0|0)==(0);
 if ($1) {
  $N$010 = $M;
  while(1) {
   $4 = (($N$010|0) / 7)&-1;
   $5 = (($4|0) % 7)&-1;
   $6 = ($5|0)==(0);
   if ($6) {
    $N$010 = $4;
   } else {
    $N$0$lcssa = $4;
    break;
   }
  }
 } else {
  $N$0$lcssa = $M;
 }
 $2 = (($N$0$lcssa|0) % 3)&-1;
 $3 = ($2|0)==(0);
 if ($3) {
  $N$17 = $N$0$lcssa;
  while(1) {
   $9 = (($N$17|0) / 3)&-1;
   $10 = (($9|0) % 3)&-1;
   $11 = ($10|0)==(0);
   if ($11) {
    $N$17 = $9;
   } else {
    $N$1$lcssa = $9;
    break;
   }
  }
 } else {
  $N$1$lcssa = $N$0$lcssa;
 }
 $7 = (($N$1$lcssa|0) % 5)&-1;
 $8 = ($7|0)==(0);
 if ($8) {
  $N$24 = $N$1$lcssa;
  while(1) {
   $14 = (($N$24|0) / 5)&-1;
   $15 = (($14|0) % 5)&-1;
   $16 = ($15|0)==(0);
   if ($16) {
    $N$24 = $14;
   } else {
    $N$2$lcssa = $14;
    break;
   }
  }
 } else {
  $N$2$lcssa = $N$1$lcssa;
 }
 $12 = $N$2$lcssa & 1;
 $13 = ($12|0)==(0);
 if ($13) {
  $N$33 = $N$2$lcssa;
 } else {
  $N$3$lcssa = $N$2$lcssa;
  return ($N$3$lcssa|0);
 }
 while(1) {
  $17 = (($N$33|0) / 2)&-1;
  $18 = $17 & 1;
  $19 = ($18|0)==(0);
  if ($19) {
   $N$33 = $17;
  } else {
   $N$3$lcssa = $17;
   break;
  }
 }
 return ($N$3$lcssa|0);
}
function _findnexte($M) {
 $M = $M|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $N$0 = 0, $N$0$lcssa = 0, $not$ = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $N$0 = $M;
 while(1) {
  $0 = (_factorf($N$0)|0);
  $1 = $N$0 & 1;
  $2 = ($1|0)!=(0);
  $not$ = ($0|0)!=(1);
  $3 = $2 | $not$;
  $4 = (($N$0) + 1)|0;
  if ($3) {
   $N$0 = $4;
  } else {
   $N$0$lcssa = $N$0;
   break;
  }
 }
 return ($N$0$lcssa|0);
}
function _conv_init($N,$L) {
 $N = $N|0;
 $L = $L|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (($N) + -1)|0;
 $1 = (($0) + ($L))|0;
 $2 = (_malloc(20)|0);
 $3 = (_findnexte($1)|0);
 $4 = ((($2)) + 16|0);
 HEAP32[$4>>2] = $3;
 $5 = ((($2)) + 8|0);
 HEAP32[$5>>2] = $N;
 $6 = ((($2)) + 12|0);
 HEAP32[$6>>2] = $L;
 $7 = HEAP32[$4>>2]|0;
 $8 = (_fft_real_init($7,1)|0);
 HEAP32[$2>>2] = $8;
 $9 = HEAP32[$4>>2]|0;
 $10 = (_fft_real_init($9,-1)|0);
 $11 = ((($2)) + 4|0);
 HEAP32[$11>>2] = $10;
 return ($2|0);
}
function _conv_direct($inp1,$N,$inp2,$L,$oup) {
 $inp1 = $inp1|0;
 $N = $N|0;
 $inp2 = $inp2|0;
 $L = $L|0;
 $oup = $oup|0;
 var $0 = 0, $1 = 0, $10 = 0.0, $11 = 0, $12 = 0, $13 = 0.0, $14 = 0.0, $15 = 0.0, $16 = 0.0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0.0, $23 = 0, $24 = 0.0, $25 = 0.0, $26 = 0;
 var $27 = 0, $28 = 0.0, $29 = 0, $3 = 0, $30 = 0, $31 = 0.0, $32 = 0.0, $33 = 0.0, $34 = 0.0, $35 = 0, $36 = 0.0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0.0, $41 = 0, $42 = 0, $43 = 0, $44 = 0.0;
 var $45 = 0, $46 = 0, $47 = 0.0, $48 = 0.0, $49 = 0.0, $5 = 0, $50 = 0.0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0.0, $57 = 0, $58 = 0.0, $59 = 0.0, $6 = 0.0, $60 = 0, $61 = 0, $62 = 0.0;
 var $63 = 0, $64 = 0, $65 = 0.0, $66 = 0.0, $67 = 0.0, $68 = 0.0, $69 = 0, $7 = 0, $70 = 0.0, $71 = 0, $72 = 0, $8 = 0, $9 = 0, $exitcond = 0, $exitcond31 = 0, $exitcond32 = 0, $exitcond34 = 0, $exitcond38 = 0, $exitcond39 = 0, $i$021 = 0;
 var $i$17 = 0, $indvars$iv = 0, $indvars$iv$next = 0, $indvars$iv$next37 = 0, $indvars$iv36 = 0, $k$027 = 0, $k$120 = 0, $k$213 = 0, $k$36 = 0, $m$023 = 0, $m$116 = 0, $m$29 = 0, $m$35 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (($N) + -1)|0;
 $1 = (($0) + ($L))|0;
 $2 = ($N|0)<($L|0);
 if ($2) {
  $4 = ($N|0)>(0);
  if ($4) {
   $indvars$iv = 1;$k$213 = 0;
   while(1) {
    $42 = (($oup) + ($k$213<<3)|0);
    HEAPF64[$42>>3] = 0.0;
    $m$29 = 0;
    while(1) {
     $43 = (($inp2) + ($m$29<<3)|0);
     $44 = +HEAPF64[$43>>3];
     $45 = (($k$213) - ($m$29))|0;
     $46 = (($inp1) + ($45<<3)|0);
     $47 = +HEAPF64[$46>>3];
     $48 = $44 * $47;
     $49 = +HEAPF64[$42>>3];
     $50 = $49 + $48;
     HEAPF64[$42>>3] = $50;
     $51 = (($m$29) + 1)|0;
     $exitcond31 = ($51|0)==($indvars$iv|0);
     if ($exitcond31) {
      break;
     } else {
      $m$29 = $51;
     }
    }
    $52 = (($k$213) + 1)|0;
    $indvars$iv$next = (($indvars$iv) + 1)|0;
    $exitcond32 = ($52|0)==($N|0);
    if ($exitcond32) {
     break;
    } else {
     $indvars$iv = $indvars$iv$next;$k$213 = $52;
    }
   }
  }
  $39 = ($1|0)>($N|0);
  if (!($39)) {
   return;
  }
  $40 = (+($L|0));
  $41 = (($L) + -1)|0;
  $i$17 = 0;$k$36 = $N;
  while(1) {
   $53 = (($oup) + ($k$36<<3)|0);
   HEAPF64[$53>>3] = 0.0;
   $54 = (($i$17) + 1)|0;
   $55 = (($54) + ($N))|0;
   $56 = (+($55|0));
   $57 = $56 < $40;
   $58 = $57 ? $56 : $40;
   $59 = (+($54|0));
   $60 = $59 < $58;
   if ($60) {
    $m$35 = $54;
    while(1) {
     $61 = (($inp2) + ($m$35<<3)|0);
     $62 = +HEAPF64[$61>>3];
     $63 = (($k$36) - ($m$35))|0;
     $64 = (($inp1) + ($63<<3)|0);
     $65 = +HEAPF64[$64>>3];
     $66 = $62 * $65;
     $67 = +HEAPF64[$53>>3];
     $68 = $67 + $66;
     HEAPF64[$53>>3] = $68;
     $69 = (($m$35) + 1)|0;
     $70 = (+($69|0));
     $71 = $70 < $58;
     if ($71) {
      $m$35 = $69;
     } else {
      break;
     }
    }
   }
   $72 = (($k$36) + 1)|0;
   $exitcond = ($54|0)==($41|0);
   if ($exitcond) {
    break;
   } else {
    $i$17 = $54;$k$36 = $72;
   }
  }
  return;
 } else {
  $3 = ($L|0)>(0);
  if ($3) {
   $indvars$iv36 = 1;$k$027 = 0;
   while(1) {
    $8 = (($oup) + ($k$027<<3)|0);
    HEAPF64[$8>>3] = 0.0;
    $m$023 = 0;
    while(1) {
     $9 = (($inp1) + ($m$023<<3)|0);
     $10 = +HEAPF64[$9>>3];
     $11 = (($k$027) - ($m$023))|0;
     $12 = (($inp2) + ($11<<3)|0);
     $13 = +HEAPF64[$12>>3];
     $14 = $10 * $13;
     $15 = +HEAPF64[$8>>3];
     $16 = $15 + $14;
     HEAPF64[$8>>3] = $16;
     $17 = (($m$023) + 1)|0;
     $exitcond38 = ($17|0)==($indvars$iv36|0);
     if ($exitcond38) {
      break;
     } else {
      $m$023 = $17;
     }
    }
    $18 = (($k$027) + 1)|0;
    $indvars$iv$next37 = (($indvars$iv36) + 1)|0;
    $exitcond39 = ($18|0)==($L|0);
    if ($exitcond39) {
     break;
    } else {
     $indvars$iv36 = $indvars$iv$next37;$k$027 = $18;
    }
   }
  }
  $5 = ($1|0)>($L|0);
  if (!($5)) {
   return;
  }
  $6 = (+($N|0));
  $7 = (($N) + -1)|0;
  $i$021 = 0;$k$120 = $L;
  while(1) {
   $19 = (($oup) + ($k$120<<3)|0);
   HEAPF64[$19>>3] = 0.0;
   $20 = (($i$021) + 1)|0;
   $21 = (($20) + ($L))|0;
   $22 = (+($21|0));
   $23 = $22 < $6;
   $24 = $23 ? $22 : $6;
   $25 = (+($20|0));
   $26 = $25 < $24;
   if ($26) {
    $m$116 = $20;
    while(1) {
     $27 = (($inp1) + ($m$116<<3)|0);
     $28 = +HEAPF64[$27>>3];
     $29 = (($k$120) - ($m$116))|0;
     $30 = (($inp2) + ($29<<3)|0);
     $31 = +HEAPF64[$30>>3];
     $32 = $28 * $31;
     $33 = +HEAPF64[$19>>3];
     $34 = $33 + $32;
     HEAPF64[$19>>3] = $34;
     $35 = (($m$116) + 1)|0;
     $36 = (+($35|0));
     $37 = $36 < $24;
     if ($37) {
      $m$116 = $35;
     } else {
      break;
     }
    }
   }
   $38 = (($k$120) + 1)|0;
   $exitcond34 = ($20|0)==($7|0);
   if ($exitcond34) {
    break;
   } else {
    $i$021 = $20;$k$120 = $38;
   }
  }
  return;
 }
}
function _conv_fft($obj,$inp1,$inp2,$oup) {
 $obj = $obj|0;
 $inp1 = $inp1|0;
 $inp2 = $inp2|0;
 $oup = $oup|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0.0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0.0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0.0, $32 = 0, $33 = 0.0, $34 = 0.0, $35 = 0, $36 = 0.0, $37 = 0, $38 = 0.0, $39 = 0.0, $4 = 0, $40 = 0.0, $41 = 0, $42 = 0.0, $43 = 0.0, $44 = 0.0;
 var $45 = 0.0, $46 = 0.0, $47 = 0.0, $48 = 0.0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0.0, $55 = 0, $56 = 0, $57 = 0, $58 = 0.0, $59 = 0.0, $6 = 0, $60 = 0, $61 = 0, $7 = 0;
 var $8 = 0, $9 = 0, $exitcond = 0, $exitcond10 = 0, $exitcond11 = 0, $i$06 = 0, $i$12 = 0, $i$21 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ((($obj)) + 16|0);
 $1 = HEAP32[$0>>2]|0;
 $2 = ((($obj)) + 8|0);
 $3 = HEAP32[$2>>2]|0;
 $4 = ((($obj)) + 12|0);
 $5 = HEAP32[$4>>2]|0;
 $6 = (($3) + -1)|0;
 $7 = (($6) + ($5))|0;
 $8 = $1 << 4;
 $9 = (_malloc($8)|0);
 $10 = (_malloc($8)|0);
 $11 = (_malloc($8)|0);
 $12 = (_malloc($8)|0);
 $13 = (_malloc($8)|0);
 $14 = (_malloc($8)|0);
 $15 = ($1|0)>(0);
 if ($15) {
  $i$06 = 0;
  while(1) {
   $16 = ($i$06|0)<($3|0);
   if ($16) {
    $17 = (($inp1) + ($i$06<<3)|0);
    $18 = +HEAPF64[$17>>3];
    $19 = (($9) + ($i$06<<3)|0);
    HEAPF64[$19>>3] = $18;
   } else {
    $20 = (($9) + ($i$06<<3)|0);
    HEAPF64[$20>>3] = 0.0;
   }
   $21 = ($i$06|0)<($5|0);
   if ($21) {
    $22 = (($inp2) + ($i$06<<3)|0);
    $23 = +HEAPF64[$22>>3];
    $24 = (($10) + ($i$06<<3)|0);
    HEAPF64[$24>>3] = $23;
   } else {
    $25 = (($10) + ($i$06<<3)|0);
    HEAPF64[$25>>3] = 0.0;
   }
   $26 = (($i$06) + 1)|0;
   $exitcond11 = ($26|0)==($1|0);
   if ($exitcond11) {
    break;
   } else {
    $i$06 = $26;
   }
  }
 }
 $27 = HEAP32[$obj>>2]|0;
 _fft_r2c_exec($27,$9,$12);
 $28 = HEAP32[$obj>>2]|0;
 _fft_r2c_exec($28,$10,$13);
 $29 = ($1|0)>(0);
 if ($29) {
  $i$12 = 0;
  while(1) {
   $30 = (($12) + ($i$12<<4)|0);
   $31 = +HEAPF64[$30>>3];
   $32 = (($13) + ($i$12<<4)|0);
   $33 = +HEAPF64[$32>>3];
   $34 = $31 * $33;
   $35 = (((($12) + ($i$12<<4)|0)) + 8|0);
   $36 = +HEAPF64[$35>>3];
   $37 = (((($13) + ($i$12<<4)|0)) + 8|0);
   $38 = +HEAPF64[$37>>3];
   $39 = $36 * $38;
   $40 = $34 - $39;
   $41 = (($11) + ($i$12<<4)|0);
   HEAPF64[$41>>3] = $40;
   $42 = +HEAPF64[$35>>3];
   $43 = +HEAPF64[$32>>3];
   $44 = $42 * $43;
   $45 = +HEAPF64[$30>>3];
   $46 = +HEAPF64[$37>>3];
   $47 = $45 * $46;
   $48 = $44 + $47;
   $49 = (((($11) + ($i$12<<4)|0)) + 8|0);
   HEAPF64[$49>>3] = $48;
   $50 = (($i$12) + 1)|0;
   $exitcond10 = ($50|0)==($1|0);
   if ($exitcond10) {
    break;
   } else {
    $i$12 = $50;
   }
  }
 }
 $51 = ((($obj)) + 4|0);
 $52 = HEAP32[$51>>2]|0;
 _fft_c2r_exec($52,$11,$14);
 $53 = ($7|0)>(0);
 if (!($53)) {
  _free($9);
  _free($10);
  _free($11);
  _free($12);
  _free($13);
  _free($14);
  return;
 }
 $54 = (+($1|0));
 $55 = (($3) + ($5))|0;
 $56 = (($55) + -1)|0;
 $i$21 = 0;
 while(1) {
  $57 = (($14) + ($i$21<<3)|0);
  $58 = +HEAPF64[$57>>3];
  $59 = $58 / $54;
  $60 = (($oup) + ($i$21<<3)|0);
  HEAPF64[$60>>3] = $59;
  $61 = (($i$21) + 1)|0;
  $exitcond = ($61|0)==($56|0);
  if ($exitcond) {
   break;
  } else {
   $i$21 = $61;
  }
 }
 _free($9);
 _free($10);
 _free($11);
 _free($12);
 _free($13);
 _free($14);
 return;
}
function _free_conv($object) {
 $object = $object|0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 _free($object);
 return;
}
function _filtlength($name) {
 $name = $name|0;
 var $$0 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0;
 var $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0;
 var $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0;
 var $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0;
 var $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (_strcmp($name,8)|0);
 $1 = ($0|0)==(0);
 do {
  if ($1) {
   $$0 = 2;
  } else {
   $2 = (_strcmp($name,16)|0);
   $3 = ($2|0)==(0);
   if ($3) {
    $$0 = 2;
   } else {
    $4 = (_strcmp($name,24)|0);
    $5 = ($4|0)==(0);
    if ($5) {
     $$0 = 4;
    } else {
     $6 = (_strcmp($name,32)|0);
     $7 = ($6|0)==(0);
     if ($7) {
      $$0 = 6;
     } else {
      $8 = (_strcmp($name,40)|0);
      $9 = ($8|0)==(0);
      if ($9) {
       $$0 = 8;
      } else {
       $10 = (_strcmp($name,48)|0);
       $11 = ($10|0)==(0);
       if ($11) {
        $$0 = 10;
       } else {
        $12 = (_strcmp($name,56)|0);
        $13 = ($12|0)==(0);
        if ($13) {
         $$0 = 12;
        } else {
         $14 = (_strcmp($name,64)|0);
         $15 = ($14|0)==(0);
         if ($15) {
          $$0 = 14;
         } else {
          $16 = (_strcmp($name,72)|0);
          $17 = ($16|0)==(0);
          if ($17) {
           $$0 = 16;
          } else {
           $18 = (_strcmp($name,80)|0);
           $19 = ($18|0)==(0);
           if ($19) {
            $$0 = 18;
           } else {
            $20 = (_strcmp($name,88)|0);
            $21 = ($20|0)==(0);
            if ($21) {
             $$0 = 20;
            } else {
             $22 = (_strcmp($name,96)|0);
             $23 = ($22|0)==(0);
             if ($23) {
              $$0 = 24;
             } else {
              $24 = (_strcmp($name,104)|0);
              $25 = ($24|0)==(0);
              if ($25) {
               $$0 = 26;
              } else {
               $26 = (_strcmp($name,112)|0);
               $27 = ($26|0)==(0);
               if ($27) {
                $$0 = 22;
               } else {
                $28 = (_strcmp($name,120)|0);
                $29 = ($28|0)==(0);
                if ($29) {
                 $$0 = 28;
                } else {
                 $30 = (_strcmp($name,128)|0);
                 $31 = ($30|0)==(0);
                 if ($31) {
                  $$0 = 30;
                 } else {
                  $32 = (_strcmp($name,136)|0);
                  $33 = ($32|0)==(0);
                  if ($33) {
                   $$0 = 2;
                  } else {
                   $34 = (_strcmp($name,144)|0);
                   $35 = ($34|0)==(0);
                   if ($35) {
                    $$0 = 6;
                   } else {
                    $36 = (_strcmp($name,152)|0);
                    $37 = ($36|0)==(0);
                    if ($37) {
                     $$0 = 10;
                    } else {
                     $38 = (_strcmp($name,160)|0);
                     $39 = ($38|0)==(0);
                     if ($39) {
                      $$0 = 6;
                      break;
                     }
                     $40 = (_strcmp($name,168)|0);
                     $41 = ($40|0)==(0);
                     if ($41) {
                      $$0 = 10;
                      break;
                     }
                     $42 = (_strcmp($name,176)|0);
                     $43 = ($42|0)==(0);
                     if ($43) {
                      $$0 = 14;
                      break;
                     }
                     $44 = (_strcmp($name,184)|0);
                     $45 = ($44|0)==(0);
                     if ($45) {
                      $$0 = 18;
                      break;
                     }
                     $46 = (_strcmp($name,192)|0);
                     $47 = ($46|0)==(0);
                     if ($47) {
                      $$0 = 4;
                      break;
                     }
                     $48 = (_strcmp($name,200)|0);
                     $49 = ($48|0)==(0);
                     if ($49) {
                      $$0 = 8;
                      break;
                     }
                     $50 = (_strcmp($name,208)|0);
                     $51 = ($50|0)==(0);
                     if ($51) {
                      $$0 = 12;
                      break;
                     }
                     $52 = (_strcmp($name,216)|0);
                     $53 = ($52|0)==(0);
                     if ($53) {
                      $$0 = 16;
                      break;
                     }
                     $54 = (_strcmp($name,224)|0);
                     $55 = ($54|0)==(0);
                     if ($55) {
                      $$0 = 20;
                      break;
                     }
                     $56 = (_strcmp($name,232)|0);
                     $57 = ($56|0)==(0);
                     if ($57) {
                      $$0 = 10;
                      break;
                     }
                     $58 = (_strcmp($name,240)|0);
                     $59 = ($58|0)==(0);
                     if ($59) {
                      $$0 = 12;
                      break;
                     }
                     $60 = (_strcmp($name,248)|0);
                     $61 = ($60|0)==(0);
                     if ($61) {
                      $$0 = 18;
                      break;
                     }
                     $62 = (_strcmp($name,256)|0);
                     $63 = ($62|0)==(0);
                     if ($63) {
                      $$0 = 6;
                      break;
                     }
                     $64 = (_strcmp($name,264)|0);
                     $65 = ($64|0)==(0);
                     if ($65) {
                      $$0 = 12;
                      break;
                     }
                     $66 = (_strcmp($name,272)|0);
                     $67 = ($66|0)==(0);
                     if ($67) {
                      $$0 = 18;
                      break;
                     }
                     $68 = (_strcmp($name,280)|0);
                     $69 = ($68|0)==(0);
                     if ($69) {
                      $$0 = 24;
                      break;
                     }
                     $70 = (_strcmp($name,288)|0);
                     $71 = ($70|0)==(0);
                     if ($71) {
                      $$0 = 30;
                      break;
                     }
                     $72 = (_strcmp($name,296)|0);
                     $73 = ($72|0)==(0);
                     if ($73) {
                      $$0 = 4;
                      break;
                     }
                     $74 = (_strcmp($name,304)|0);
                     $75 = ($74|0)==(0);
                     if ($75) {
                      $$0 = 6;
                      break;
                     }
                     $76 = (_strcmp($name,312)|0);
                     $77 = ($76|0)==(0);
                     if ($77) {
                      $$0 = 8;
                      break;
                     }
                     $78 = (_strcmp($name,320)|0);
                     $79 = ($78|0)==(0);
                     if ($79) {
                      $$0 = 10;
                      break;
                     }
                     $80 = (_strcmp($name,328)|0);
                     $81 = ($80|0)==(0);
                     if ($81) {
                      $$0 = 12;
                      break;
                     }
                     $82 = (_strcmp($name,336)|0);
                     $83 = ($82|0)==(0);
                     if ($83) {
                      $$0 = 14;
                      break;
                     }
                     $84 = (_strcmp($name,344)|0);
                     $85 = ($84|0)==(0);
                     if ($85) {
                      $$0 = 16;
                      break;
                     }
                     $86 = (_strcmp($name,352)|0);
                     $87 = ($86|0)==(0);
                     if ($87) {
                      $$0 = 18;
                      break;
                     }
                     $88 = (_strcmp($name,360)|0);
                     $89 = ($88|0)==(0);
                     if ($89) {
                      $$0 = 20;
                      break;
                     }
                     (_puts((15856|0))|0);
                     $$0 = -1;
                    }
                   }
                  }
                 }
                }
               }
              }
             }
            }
           }
          }
         }
        }
       }
      }
     }
    }
   }
  }
 } while(0);
 return ($$0|0);
}
function _filtcoef($name,$lp1,$hp1,$lp2,$hp2) {
 $name = $name|0;
 $lp1 = $lp1|0;
 $hp1 = $hp1|0;
 $lp2 = $lp2|0;
 $hp2 = $hp2|0;
 var $$0 = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0.0, $1000 = 0, $1001 = 0, $1002 = 0, $1003 = 0, $1004 = 0, $1005 = 0, $1006 = 0, $1007 = 0, $1008 = 0, $1009 = 0, $101 = 0, $1010 = 0, $1011 = 0, $1012 = 0, $1013 = 0;
 var $1014 = 0, $1015 = 0, $1016 = 0, $1017 = 0, $1018 = 0, $1019 = 0, $102 = 0, $1020 = 0, $1021 = 0, $1022 = 0, $1023 = 0, $1024 = 0, $1025 = 0, $1026 = 0, $1027 = 0, $1028 = 0.0, $1029 = 0, $103 = 0.0, $1030 = 0, $1031 = 0.0;
 var $1032 = 0, $1033 = 0, $1034 = 0.0, $1035 = 0, $1036 = 0, $1037 = 0.0, $1038 = 0, $1039 = 0, $104 = 0, $1040 = 0, $1041 = 0, $1042 = 0, $1043 = 0.0, $1044 = 0, $1045 = 0, $1046 = 0.0, $1047 = 0, $1048 = 0, $1049 = 0.0, $105 = 0;
 var $1050 = 0, $1051 = 0, $1052 = 0.0, $1053 = 0, $1054 = 0, $1055 = 0, $1056 = 0, $1057 = 0, $1058 = 0.0, $1059 = 0, $106 = 0.0, $1060 = 0, $1061 = 0.0, $1062 = 0, $1063 = 0, $1064 = 0.0, $1065 = 0, $1066 = 0, $1067 = 0.0, $1068 = 0;
 var $1069 = 0, $107 = 0, $1070 = 0, $1071 = 0, $1072 = 0, $1073 = 0.0, $1074 = 0, $1075 = 0, $1076 = 0.0, $1077 = 0, $1078 = 0, $1079 = 0.0, $108 = 0, $1080 = 0, $1081 = 0, $1082 = 0.0, $1083 = 0, $1084 = 0, $1085 = 0, $1086 = 0;
 var $1087 = 0, $1088 = 0.0, $1089 = 0, $109 = 0, $1090 = 0, $1091 = 0.0, $1092 = 0, $1093 = 0, $1094 = 0.0, $1095 = 0, $1096 = 0, $1097 = 0.0, $1098 = 0, $1099 = 0, $11 = 0, $110 = 0, $1100 = 0, $1101 = 0, $1102 = 0, $1103 = 0.0;
 var $1104 = 0, $1105 = 0, $1106 = 0.0, $1107 = 0, $1108 = 0, $1109 = 0.0, $111 = 0, $1110 = 0, $1111 = 0, $1112 = 0.0, $1113 = 0, $1114 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0, $116 = 0, $117 = 0, $118 = 0, $119 = 0;
 var $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0, $134 = 0, $135 = 0, $136 = 0, $137 = 0;
 var $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0.0, $142 = 0, $143 = 0, $144 = 0.0, $145 = 0, $146 = 0, $147 = 0.0, $148 = 0, $149 = 0, $15 = 0, $150 = 0.0, $151 = 0, $152 = 0, $153 = 0, $154 = 0, $155 = 0;
 var $156 = 0.0, $157 = 0, $158 = 0, $159 = 0.0, $16 = 0, $160 = 0, $161 = 0, $162 = 0.0, $163 = 0, $164 = 0, $165 = 0.0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0, $170 = 0, $171 = 0.0, $172 = 0, $173 = 0;
 var $174 = 0.0, $175 = 0, $176 = 0, $177 = 0.0, $178 = 0, $179 = 0, $18 = 0, $180 = 0.0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0.0, $187 = 0, $188 = 0, $189 = 0.0, $19 = 0, $190 = 0, $191 = 0;
 var $192 = 0.0, $193 = 0, $194 = 0, $195 = 0.0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0.0, $202 = 0, $203 = 0, $204 = 0.0, $205 = 0, $206 = 0, $207 = 0.0, $208 = 0, $209 = 0;
 var $21 = 0, $210 = 0.0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0.0, $217 = 0, $218 = 0, $219 = 0.0, $22 = 0, $220 = 0, $221 = 0, $222 = 0.0, $223 = 0, $224 = 0, $225 = 0.0, $226 = 0, $227 = 0;
 var $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0.0, $232 = 0, $233 = 0, $234 = 0.0, $235 = 0, $236 = 0, $237 = 0.0, $238 = 0, $239 = 0, $24 = 0, $240 = 0.0, $241 = 0, $242 = 0, $243 = 0, $244 = 0, $245 = 0;
 var $246 = 0.0, $247 = 0, $248 = 0, $249 = 0.0, $25 = 0, $250 = 0, $251 = 0, $252 = 0.0, $253 = 0, $254 = 0, $255 = 0.0, $256 = 0, $257 = 0, $258 = 0, $259 = 0, $26 = 0, $260 = 0, $261 = 0.0, $262 = 0, $263 = 0;
 var $264 = 0.0, $265 = 0, $266 = 0, $267 = 0.0, $268 = 0, $269 = 0, $27 = 0, $270 = 0.0, $271 = 0, $272 = 0, $273 = 0, $274 = 0, $275 = 0, $276 = 0.0, $277 = 0, $278 = 0, $279 = 0.0, $28 = 0, $280 = 0, $281 = 0;
 var $282 = 0.0, $283 = 0, $284 = 0, $285 = 0.0, $286 = 0, $287 = 0, $288 = 0, $289 = 0, $29 = 0, $290 = 0, $291 = 0.0, $292 = 0, $293 = 0, $294 = 0.0, $295 = 0, $296 = 0, $297 = 0.0, $298 = 0, $299 = 0, $3 = 0;
 var $30 = 0, $300 = 0.0, $301 = 0, $302 = 0, $303 = 0, $304 = 0, $305 = 0, $306 = 0, $307 = 0, $308 = 0, $309 = 0, $31 = 0, $310 = 0, $311 = 0, $312 = 0, $313 = 0, $314 = 0, $315 = 0, $316 = 0, $317 = 0;
 var $318 = 0, $319 = 0, $32 = 0, $320 = 0, $321 = 0, $322 = 0, $323 = 0, $324 = 0, $325 = 0.0, $326 = 0.0, $327 = 0.0, $328 = 0.0, $329 = 0, $33 = 0, $330 = 0.0, $331 = 0, $332 = 0, $333 = 0.0, $334 = 0, $335 = 0;
 var $336 = 0.0, $337 = 0, $338 = 0, $339 = 0.0, $34 = 0, $340 = 0, $341 = 0, $342 = 0.0, $343 = 0, $344 = 0, $345 = 0.0, $346 = 0, $347 = 0, $348 = 0.0, $349 = 0, $35 = 0, $350 = 0, $351 = 0.0, $352 = 0, $353 = 0;
 var $354 = 0.0, $355 = 0, $356 = 0, $357 = 0.0, $358 = 0, $359 = 0, $36 = 0, $360 = 0.0, $361 = 0, $362 = 0, $363 = 0.0, $364 = 0, $365 = 0, $366 = 0.0, $367 = 0, $368 = 0, $369 = 0.0, $37 = 0, $370 = 0, $371 = 0;
 var $372 = 0.0, $373 = 0, $374 = 0, $375 = 0.0, $376 = 0, $377 = 0, $378 = 0.0, $379 = 0, $38 = 0, $380 = 0, $381 = 0.0, $382 = 0, $383 = 0, $384 = 0.0, $385 = 0, $386 = 0, $387 = 0.0, $388 = 0, $389 = 0, $39 = 0;
 var $390 = 0, $391 = 0, $392 = 0, $393 = 0, $394 = 0, $395 = 0, $396 = 0.0, $397 = 0, $398 = 0, $399 = 0.0, $4 = 0, $40 = 0, $400 = 0, $401 = 0, $402 = 0.0, $403 = 0, $404 = 0, $405 = 0.0, $406 = 0, $407 = 0;
 var $408 = 0, $409 = 0, $41 = 0, $410 = 0, $411 = 0, $412 = 0, $413 = 0, $414 = 0, $415 = 0, $416 = 0, $417 = 0, $418 = 0, $419 = 0, $42 = 0, $420 = 0, $421 = 0, $422 = 0, $423 = 0, $424 = 0, $425 = 0;
 var $426 = 0.0, $427 = 0.0, $428 = 0.0, $429 = 0.0, $43 = 0, $430 = 0, $431 = 0.0, $432 = 0, $433 = 0, $434 = 0.0, $435 = 0, $436 = 0, $437 = 0.0, $438 = 0, $439 = 0, $44 = 0.0, $440 = 0.0, $441 = 0, $442 = 0, $443 = 0.0;
 var $444 = 0, $445 = 0, $446 = 0.0, $447 = 0, $448 = 0, $449 = 0.0, $45 = 0.0, $450 = 0, $451 = 0, $452 = 0.0, $453 = 0, $454 = 0, $455 = 0.0, $456 = 0, $457 = 0, $458 = 0.0, $459 = 0, $46 = 0.0, $460 = 0, $461 = 0.0;
 var $462 = 0, $463 = 0, $464 = 0.0, $465 = 0, $466 = 0, $467 = 0.0, $468 = 0, $469 = 0, $47 = 0.0, $470 = 0.0, $471 = 0, $472 = 0, $473 = 0.0, $474 = 0, $475 = 0, $476 = 0.0, $477 = 0, $478 = 0, $479 = 0.0, $48 = 0;
 var $480 = 0, $481 = 0, $482 = 0.0, $483 = 0, $484 = 0, $485 = 0.0, $486 = 0, $487 = 0, $488 = 0.0, $489 = 0, $49 = 0.0, $490 = 0, $491 = 0, $492 = 0, $493 = 0, $494 = 0, $495 = 0, $496 = 0, $497 = 0, $498 = 0;
 var $499 = 0.0, $5 = 0, $50 = 0, $500 = 0, $501 = 0, $502 = 0.0, $503 = 0, $504 = 0, $505 = 0.0, $506 = 0, $507 = 0, $508 = 0.0, $509 = 0, $51 = 0, $510 = 0, $511 = 0, $512 = 0, $513 = 0, $514 = 0, $515 = 0;
 var $516 = 0, $517 = 0, $518 = 0, $519 = 0, $52 = 0.0, $520 = 0.0, $521 = 0, $522 = 0, $523 = 0.0, $524 = 0, $525 = 0, $526 = 0.0, $527 = 0, $528 = 0, $529 = 0.0, $53 = 0, $530 = 0, $531 = 0, $532 = 0, $533 = 0;
 var $534 = 0, $535 = 0, $536 = 0, $537 = 0, $538 = 0, $539 = 0, $54 = 0, $540 = 0, $541 = 0.0, $542 = 0, $543 = 0, $544 = 0.0, $545 = 0, $546 = 0, $547 = 0.0, $548 = 0, $549 = 0, $55 = 0.0, $550 = 0.0, $551 = 0;
 var $552 = 0, $553 = 0, $554 = 0, $555 = 0, $556 = 0, $557 = 0, $558 = 0, $559 = 0, $56 = 0, $560 = 0, $561 = 0, $562 = 0, $563 = 0, $564 = 0, $565 = 0, $566 = 0, $567 = 0, $568 = 0, $569 = 0, $57 = 0;
 var $570 = 0, $571 = 0, $572 = 0, $573 = 0, $574 = 0, $575 = 0, $576 = 0, $577 = 0.0, $578 = 0.0, $579 = 0, $58 = 0.0, $580 = 0, $581 = 0.0, $582 = 0, $583 = 0, $584 = 0.0, $585 = 0, $586 = 0, $587 = 0, $588 = 0;
 var $589 = 0.0, $59 = 0, $590 = 0, $591 = 0, $592 = 0.0, $593 = 0, $594 = 0, $595 = 0, $596 = 0, $597 = 0.0, $598 = 0, $599 = 0, $6 = 0, $60 = 0, $600 = 0.0, $601 = 0, $602 = 0, $603 = 0, $604 = 0, $605 = 0.0;
 var $606 = 0, $607 = 0, $608 = 0.0, $609 = 0, $61 = 0.0, $610 = 0, $611 = 0, $612 = 0, $613 = 0.0, $614 = 0, $615 = 0, $616 = 0.0, $617 = 0, $618 = 0, $619 = 0, $62 = 0, $620 = 0, $621 = 0.0, $622 = 0, $623 = 0;
 var $624 = 0.0, $625 = 0, $626 = 0, $627 = 0, $628 = 0, $629 = 0.0, $63 = 0, $630 = 0, $631 = 0, $632 = 0.0, $633 = 0, $634 = 0, $635 = 0, $636 = 0, $637 = 0, $638 = 0, $639 = 0, $64 = 0.0, $640 = 0, $641 = 0;
 var $642 = 0, $643 = 0, $644 = 0, $645 = 0, $646 = 0.0, $647 = 0, $648 = 0, $649 = 0.0, $65 = 0, $650 = 0, $651 = 0, $652 = 0.0, $653 = 0, $654 = 0, $655 = 0.0, $656 = 0, $657 = 0, $658 = 0, $659 = 0, $66 = 0;
 var $660 = 0, $661 = 0, $662 = 0, $663 = 0, $664 = 0, $665 = 0, $666 = 0, $667 = 0, $668 = 0, $669 = 0.0, $67 = 0.0, $670 = 0, $671 = 0, $672 = 0.0, $673 = 0, $674 = 0, $675 = 0.0, $676 = 0, $677 = 0, $678 = 0.0;
 var $679 = 0, $68 = 0, $680 = 0, $681 = 0, $682 = 0, $683 = 0, $684 = 0, $685 = 0, $686 = 0, $687 = 0, $688 = 0, $689 = 0, $69 = 0, $690 = 0, $691 = 0, $692 = 0.0, $693 = 0, $694 = 0, $695 = 0.0, $696 = 0;
 var $697 = 0, $698 = 0.0, $699 = 0, $7 = 0, $70 = 0.0, $700 = 0, $701 = 0.0, $702 = 0, $703 = 0, $704 = 0, $705 = 0, $706 = 0, $707 = 0.0, $708 = 0, $709 = 0, $71 = 0, $710 = 0.0, $711 = 0, $712 = 0, $713 = 0.0;
 var $714 = 0, $715 = 0, $716 = 0.0, $717 = 0, $718 = 0, $719 = 0, $72 = 0, $720 = 0, $721 = 0, $722 = 0.0, $723 = 0, $724 = 0, $725 = 0.0, $726 = 0, $727 = 0, $728 = 0.0, $729 = 0, $73 = 0.0, $730 = 0, $731 = 0.0;
 var $732 = 0, $733 = 0, $734 = 0, $735 = 0, $736 = 0, $737 = 0.0, $738 = 0, $739 = 0, $74 = 0, $740 = 0.0, $741 = 0, $742 = 0, $743 = 0.0, $744 = 0, $745 = 0, $746 = 0.0, $747 = 0, $748 = 0, $749 = 0, $75 = 0;
 var $750 = 0, $751 = 0, $752 = 0, $753 = 0, $754 = 0, $755 = 0, $756 = 0, $757 = 0, $758 = 0, $759 = 0, $76 = 0.0, $760 = 0, $761 = 0, $762 = 0, $763 = 0, $764 = 0, $765 = 0, $766 = 0, $767 = 0, $768 = 0;
 var $769 = 0, $77 = 0, $770 = 0, $771 = 0.0, $772 = 0.0, $773 = 0.0, $774 = 0.0, $775 = 0, $776 = 0.0, $777 = 0, $778 = 0, $779 = 0.0, $78 = 0, $780 = 0, $781 = 0, $782 = 0.0, $783 = 0, $784 = 0, $785 = 0.0, $786 = 0;
 var $787 = 0, $788 = 0.0, $789 = 0, $79 = 0.0, $790 = 0, $791 = 0.0, $792 = 0, $793 = 0, $794 = 0.0, $795 = 0, $796 = 0, $797 = 0.0, $798 = 0, $799 = 0, $8 = 0, $80 = 0, $800 = 0.0, $801 = 0, $802 = 0, $803 = 0.0;
 var $804 = 0, $805 = 0, $806 = 0.0, $807 = 0, $808 = 0, $809 = 0.0, $81 = 0, $810 = 0, $811 = 0, $812 = 0.0, $813 = 0, $814 = 0, $815 = 0.0, $816 = 0, $817 = 0, $818 = 0.0, $819 = 0, $82 = 0.0, $820 = 0, $821 = 0.0;
 var $822 = 0, $823 = 0, $824 = 0.0, $825 = 0, $826 = 0, $827 = 0.0, $828 = 0, $829 = 0, $83 = 0, $830 = 0.0, $831 = 0, $832 = 0, $833 = 0.0, $834 = 0, $835 = 0, $836 = 0, $837 = 0, $838 = 0.0, $839 = 0, $84 = 0;
 var $840 = 0, $841 = 0.0, $842 = 0, $843 = 0, $844 = 0.0, $845 = 0, $846 = 0, $847 = 0.0, $848 = 0, $849 = 0, $85 = 0.0, $850 = 0, $851 = 0, $852 = 0, $853 = 0.0, $854 = 0, $855 = 0, $856 = 0.0, $857 = 0, $858 = 0;
 var $859 = 0.0, $86 = 0, $860 = 0, $861 = 0, $862 = 0.0, $863 = 0, $864 = 0, $865 = 0, $866 = 0, $867 = 0, $868 = 0.0, $869 = 0, $87 = 0, $870 = 0, $871 = 0.0, $872 = 0, $873 = 0, $874 = 0.0, $875 = 0, $876 = 0;
 var $877 = 0.0, $878 = 0, $879 = 0, $88 = 0.0, $880 = 0, $881 = 0, $882 = 0, $883 = 0.0, $884 = 0, $885 = 0, $886 = 0.0, $887 = 0, $888 = 0, $889 = 0.0, $89 = 0, $890 = 0, $891 = 0, $892 = 0.0, $893 = 0, $894 = 0;
 var $895 = 0, $896 = 0, $897 = 0, $898 = 0, $899 = 0, $9 = 0, $90 = 0, $900 = 0, $901 = 0, $902 = 0, $903 = 0, $904 = 0, $905 = 0, $906 = 0, $907 = 0, $908 = 0, $909 = 0, $91 = 0.0, $910 = 0, $911 = 0;
 var $912 = 0, $913 = 0, $914 = 0, $915 = 0, $916 = 0, $917 = 0, $918 = 0, $919 = 0, $92 = 0, $920 = 0, $921 = 0, $922 = 0, $923 = 0, $924 = 0, $925 = 0, $926 = 0, $927 = 0, $928 = 0, $929 = 0, $93 = 0;
 var $930 = 0, $931 = 0.0, $932 = 0.0, $933 = 0.0, $934 = 0.0, $935 = 0, $936 = 0.0, $937 = 0, $938 = 0, $939 = 0.0, $94 = 0.0, $940 = 0, $941 = 0, $942 = 0.0, $943 = 0, $944 = 0, $945 = 0.0, $946 = 0, $947 = 0, $948 = 0.0;
 var $949 = 0, $95 = 0, $950 = 0, $951 = 0.0, $952 = 0, $953 = 0, $954 = 0.0, $955 = 0, $956 = 0, $957 = 0.0, $958 = 0, $959 = 0, $96 = 0, $960 = 0.0, $961 = 0, $962 = 0, $963 = 0.0, $964 = 0, $965 = 0, $966 = 0.0;
 var $967 = 0, $968 = 0, $969 = 0.0, $97 = 0.0, $970 = 0, $971 = 0, $972 = 0.0, $973 = 0, $974 = 0, $975 = 0.0, $976 = 0, $977 = 0, $978 = 0.0, $979 = 0, $98 = 0, $980 = 0, $981 = 0.0, $982 = 0, $983 = 0, $984 = 0.0;
 var $985 = 0, $986 = 0, $987 = 0.0, $988 = 0, $989 = 0, $99 = 0, $990 = 0.0, $991 = 0, $992 = 0, $993 = 0.0, $994 = 0, $995 = 0, $996 = 0, $997 = 0, $998 = 0, $999 = 0, $exitcond = 0, $exitcond128 = 0, $exitcond133 = 0, $exitcond138 = 0;
 var $exitcond143 = 0, $exitcond148 = 0, $exitcond153 = 0, $exitcond158 = 0, $exitcond163 = 0, $exitcond168 = 0, $exitcond173 = 0, $exitcond189 = 0, $exitcond199 = 0, $exitcond204 = 0, $exitcond209 = 0, $exitcond225 = 0, $exitcond230 = 0, $exitcond235 = 0, $exitcond240 = 0, $exitcond245 = 0, $exitcond250 = 0, $exitcond260 = 0, $exitcond265 = 0, $exitcond270 = 0;
 var $exitcond275 = 0, $exitcond297 = 0, $exitcond302 = 0, $exitcond307 = 0, $exitcond312 = 0, $exitcond317 = 0, $exitcond322 = 0, $hp1_a6 = 0, $hp2_a8 = 0, $i$1071 = 0, $i$1172 = 0, $i$1273 = 0, $i$1374 = 0, $i$1475 = 0, $i$1778 = 0, $i$1980 = 0, $i$2081 = 0, $i$2182 = 0, $i$2485 = 0, $i$2586 = 0;
 var $i$2687 = 0, $i$2788 = 0, $i$2889 = 0, $i$2990 = 0, $i$3192 = 0, $i$3293 = 0, $i$3394 = 0, $i$3495 = 0, $i$3899 = 0, $i$39100 = 0, $i$40101 = 0, $i$41102 = 0, $i$42103 = 0, $i$43104 = 0, $i$465 = 0, $i$566 = 0, $i$667 = 0, $i$768 = 0, $i$869 = 0, $i$970 = 0;
 var $lp1_a5 = 0, $lp2_a7 = 0, dest = 0, label = 0, sp = 0, stop = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 416|0;
 $lp1_a5 = sp + 256|0;
 $hp1_a6 = sp;
 $lp2_a7 = sp + 160|0;
 $hp2_a8 = sp + 208|0;
 $0 = (_strcmp($name,8)|0);
 $1 = ($0|0)==(0);
 if (!($1)) {
  $2 = (_strcmp($name,16)|0);
  $3 = ($2|0)==(0);
  if (!($3)) {
   $8 = (_strcmp($name,24)|0);
   $9 = ($8|0)==(0);
   if ($9) {
    HEAPF64[$lp1>>3] = -0.12940952255092145;
    HEAPF64[$hp1>>3] = -0.48296291314469025;
    HEAPF64[$lp2>>3] = 0.48296291314469025;
    HEAPF64[$hp2>>3] = -0.12940952255092145;
    $10 = ((($lp1)) + 8|0);
    HEAPF64[$10>>3] = 0.22414386804185735;
    $11 = ((($hp1)) + 8|0);
    HEAPF64[$11>>3] = 0.83651630373746899;
    $12 = ((($lp2)) + 8|0);
    HEAPF64[$12>>3] = 0.83651630373746899;
    $13 = ((($hp2)) + 8|0);
    HEAPF64[$13>>3] = -0.22414386804185735;
    $14 = ((($lp1)) + 16|0);
    HEAPF64[$14>>3] = 0.83651630373746899;
    $15 = ((($hp1)) + 16|0);
    HEAPF64[$15>>3] = -0.22414386804185735;
    $16 = ((($lp2)) + 16|0);
    HEAPF64[$16>>3] = 0.22414386804185735;
    $17 = ((($hp2)) + 16|0);
    HEAPF64[$17>>3] = 0.83651630373746899;
    $18 = ((($lp1)) + 24|0);
    HEAPF64[$18>>3] = 0.48296291314469025;
    $19 = ((($hp1)) + 24|0);
    HEAPF64[$19>>3] = -0.12940952255092145;
    $20 = ((($lp2)) + 24|0);
    HEAPF64[$20>>3] = -0.12940952255092145;
    $21 = ((($hp2)) + 24|0);
    HEAPF64[$21>>3] = -0.48296291314469025;
    $$0 = 4;
    STACKTOP = sp;return ($$0|0);
   }
   $22 = (_strcmp($name,32)|0);
   $23 = ($22|0)==(0);
   if ($23) {
    dest=$lp1_a5; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$lp1_a5>>3] = 0.035226291882100656;
    $24 = ((($lp1_a5)) + 8|0);
    HEAPF64[$24>>3] = -0.085441273882241486;
    $25 = ((($lp1_a5)) + 16|0);
    HEAPF64[$25>>3] = -0.13501102001039084;
    $26 = ((($lp1_a5)) + 24|0);
    HEAPF64[$26>>3] = 0.45987750211933132;
    $27 = ((($lp1_a5)) + 32|0);
    HEAPF64[$27>>3] = 0.80689150931333875;
    $28 = ((($lp1_a5)) + 40|0);
    HEAPF64[$28>>3] = 0.33267055295095688;
    dest=$hp1_a6; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$hp1_a6>>3] = -0.33267055295095688;
    $29 = ((($hp1_a6)) + 8|0);
    HEAPF64[$29>>3] = 0.80689150931333875;
    $30 = ((($hp1_a6)) + 16|0);
    HEAPF64[$30>>3] = -0.45987750211933132;
    $31 = ((($hp1_a6)) + 24|0);
    HEAPF64[$31>>3] = -0.13501102001039084;
    $32 = ((($hp1_a6)) + 32|0);
    HEAPF64[$32>>3] = 0.085441273882241486;
    $33 = ((($hp1_a6)) + 40|0);
    HEAPF64[$33>>3] = 0.035226291882100656;
    dest=$lp2_a7; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$lp2_a7>>3] = 0.33267055295095688;
    $34 = ((($lp2_a7)) + 8|0);
    HEAPF64[$34>>3] = 0.80689150931333875;
    $35 = ((($lp2_a7)) + 16|0);
    HEAPF64[$35>>3] = 0.45987750211933132;
    $36 = ((($lp2_a7)) + 24|0);
    HEAPF64[$36>>3] = -0.13501102001039084;
    $37 = ((($lp2_a7)) + 32|0);
    HEAPF64[$37>>3] = -0.085441273882241486;
    $38 = ((($lp2_a7)) + 40|0);
    HEAPF64[$38>>3] = 0.035226291882100656;
    dest=$hp2_a8; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$hp2_a8>>3] = 0.035226291882100656;
    $39 = ((($hp2_a8)) + 8|0);
    HEAPF64[$39>>3] = 0.085441273882241486;
    $40 = ((($hp2_a8)) + 16|0);
    HEAPF64[$40>>3] = -0.13501102001039084;
    $41 = ((($hp2_a8)) + 24|0);
    HEAPF64[$41>>3] = -0.45987750211933132;
    $42 = ((($hp2_a8)) + 32|0);
    HEAPF64[$42>>3] = 0.80689150931333875;
    $43 = ((($hp2_a8)) + 40|0);
    HEAPF64[$43>>3] = -0.33267055295095688;
    $44 = +HEAPF64[$lp1_a5>>3];
    HEAPF64[$lp1>>3] = $44;
    $45 = +HEAPF64[$hp1_a6>>3];
    HEAPF64[$hp1>>3] = $45;
    $46 = +HEAPF64[$lp2_a7>>3];
    HEAPF64[$lp2>>3] = $46;
    $47 = +HEAPF64[$hp2_a8>>3];
    HEAPF64[$hp2>>3] = $47;
    $48 = ((($lp1_a5)) + 8|0);
    $49 = +HEAPF64[$48>>3];
    $50 = ((($lp1)) + 8|0);
    HEAPF64[$50>>3] = $49;
    $51 = ((($hp1_a6)) + 8|0);
    $52 = +HEAPF64[$51>>3];
    $53 = ((($hp1)) + 8|0);
    HEAPF64[$53>>3] = $52;
    $54 = ((($lp2_a7)) + 8|0);
    $55 = +HEAPF64[$54>>3];
    $56 = ((($lp2)) + 8|0);
    HEAPF64[$56>>3] = $55;
    $57 = ((($hp2_a8)) + 8|0);
    $58 = +HEAPF64[$57>>3];
    $59 = ((($hp2)) + 8|0);
    HEAPF64[$59>>3] = $58;
    $60 = ((($lp1_a5)) + 16|0);
    $61 = +HEAPF64[$60>>3];
    $62 = ((($lp1)) + 16|0);
    HEAPF64[$62>>3] = $61;
    $63 = ((($hp1_a6)) + 16|0);
    $64 = +HEAPF64[$63>>3];
    $65 = ((($hp1)) + 16|0);
    HEAPF64[$65>>3] = $64;
    $66 = ((($lp2_a7)) + 16|0);
    $67 = +HEAPF64[$66>>3];
    $68 = ((($lp2)) + 16|0);
    HEAPF64[$68>>3] = $67;
    $69 = ((($hp2_a8)) + 16|0);
    $70 = +HEAPF64[$69>>3];
    $71 = ((($hp2)) + 16|0);
    HEAPF64[$71>>3] = $70;
    $72 = ((($lp1_a5)) + 24|0);
    $73 = +HEAPF64[$72>>3];
    $74 = ((($lp1)) + 24|0);
    HEAPF64[$74>>3] = $73;
    $75 = ((($hp1_a6)) + 24|0);
    $76 = +HEAPF64[$75>>3];
    $77 = ((($hp1)) + 24|0);
    HEAPF64[$77>>3] = $76;
    $78 = ((($lp2_a7)) + 24|0);
    $79 = +HEAPF64[$78>>3];
    $80 = ((($lp2)) + 24|0);
    HEAPF64[$80>>3] = $79;
    $81 = ((($hp2_a8)) + 24|0);
    $82 = +HEAPF64[$81>>3];
    $83 = ((($hp2)) + 24|0);
    HEAPF64[$83>>3] = $82;
    $84 = ((($lp1_a5)) + 32|0);
    $85 = +HEAPF64[$84>>3];
    $86 = ((($lp1)) + 32|0);
    HEAPF64[$86>>3] = $85;
    $87 = ((($hp1_a6)) + 32|0);
    $88 = +HEAPF64[$87>>3];
    $89 = ((($hp1)) + 32|0);
    HEAPF64[$89>>3] = $88;
    $90 = ((($lp2_a7)) + 32|0);
    $91 = +HEAPF64[$90>>3];
    $92 = ((($lp2)) + 32|0);
    HEAPF64[$92>>3] = $91;
    $93 = ((($hp2_a8)) + 32|0);
    $94 = +HEAPF64[$93>>3];
    $95 = ((($hp2)) + 32|0);
    HEAPF64[$95>>3] = $94;
    $96 = ((($lp1_a5)) + 40|0);
    $97 = +HEAPF64[$96>>3];
    $98 = ((($lp1)) + 40|0);
    HEAPF64[$98>>3] = $97;
    $99 = ((($hp1_a6)) + 40|0);
    $100 = +HEAPF64[$99>>3];
    $101 = ((($hp1)) + 40|0);
    HEAPF64[$101>>3] = $100;
    $102 = ((($lp2_a7)) + 40|0);
    $103 = +HEAPF64[$102>>3];
    $104 = ((($lp2)) + 40|0);
    HEAPF64[$104>>3] = $103;
    $105 = ((($hp2_a8)) + 40|0);
    $106 = +HEAPF64[$105>>3];
    $107 = ((($hp2)) + 40|0);
    HEAPF64[$107>>3] = $106;
    $$0 = 6;
    STACKTOP = sp;return ($$0|0);
   }
   $108 = (_strcmp($name,40)|0);
   $109 = ($108|0)==(0);
   if ($109) {
    HEAPF64[$lp1>>3] = -0.010597401784997278;
    HEAPF64[$hp1>>3] = -0.23037781330885523;
    HEAPF64[$lp2>>3] = 0.23037781330885523;
    HEAPF64[$hp2>>3] = -0.010597401784997278;
    $110 = ((($lp1)) + 8|0);
    HEAPF64[$110>>3] = 0.032883011666982945;
    $111 = ((($hp1)) + 8|0);
    HEAPF64[$111>>3] = 0.71484657055254153;
    $112 = ((($lp2)) + 8|0);
    HEAPF64[$112>>3] = 0.71484657055254153;
    $113 = ((($hp2)) + 8|0);
    HEAPF64[$113>>3] = -0.032883011666982945;
    $114 = ((($lp1)) + 16|0);
    HEAPF64[$114>>3] = 0.030841381835986965;
    $115 = ((($hp1)) + 16|0);
    HEAPF64[$115>>3] = -0.63088076792959036;
    $116 = ((($lp2)) + 16|0);
    HEAPF64[$116>>3] = 0.63088076792959036;
    $117 = ((($hp2)) + 16|0);
    HEAPF64[$117>>3] = 0.030841381835986965;
    $118 = ((($lp1)) + 24|0);
    HEAPF64[$118>>3] = -0.18703481171888114;
    $119 = ((($hp1)) + 24|0);
    HEAPF64[$119>>3] = -0.027983769416983849;
    $120 = ((($lp2)) + 24|0);
    HEAPF64[$120>>3] = -0.027983769416983849;
    $121 = ((($hp2)) + 24|0);
    HEAPF64[$121>>3] = 0.18703481171888114;
    $122 = ((($lp1)) + 32|0);
    HEAPF64[$122>>3] = -0.027983769416983849;
    $123 = ((($hp1)) + 32|0);
    HEAPF64[$123>>3] = 0.18703481171888114;
    $124 = ((($lp2)) + 32|0);
    HEAPF64[$124>>3] = -0.18703481171888114;
    $125 = ((($hp2)) + 32|0);
    HEAPF64[$125>>3] = -0.027983769416983849;
    $126 = ((($lp1)) + 40|0);
    HEAPF64[$126>>3] = 0.63088076792959036;
    $127 = ((($hp1)) + 40|0);
    HEAPF64[$127>>3] = 0.030841381835986965;
    $128 = ((($lp2)) + 40|0);
    HEAPF64[$128>>3] = 0.030841381835986965;
    $129 = ((($hp2)) + 40|0);
    HEAPF64[$129>>3] = -0.63088076792959036;
    $130 = ((($lp1)) + 48|0);
    HEAPF64[$130>>3] = 0.71484657055254153;
    $131 = ((($hp1)) + 48|0);
    HEAPF64[$131>>3] = -0.032883011666982945;
    $132 = ((($lp2)) + 48|0);
    HEAPF64[$132>>3] = 0.032883011666982945;
    $133 = ((($hp2)) + 48|0);
    HEAPF64[$133>>3] = 0.71484657055254153;
    $134 = ((($lp1)) + 56|0);
    HEAPF64[$134>>3] = 0.23037781330885523;
    $135 = ((($hp1)) + 56|0);
    HEAPF64[$135>>3] = -0.010597401784997278;
    $136 = ((($lp2)) + 56|0);
    HEAPF64[$136>>3] = -0.010597401784997278;
    $137 = ((($hp2)) + 56|0);
    HEAPF64[$137>>3] = -0.23037781330885523;
    $$0 = 8;
    STACKTOP = sp;return ($$0|0);
   }
   $138 = (_strcmp($name,48)|0);
   $139 = ($138|0)==(0);
   if ($139) {
    $i$465 = 0;
    while(1) {
     $140 = (368 + ($i$465<<3)|0);
     $141 = +HEAPF64[$140>>3];
     $142 = (($lp1) + ($i$465<<3)|0);
     HEAPF64[$142>>3] = $141;
     $143 = (448 + ($i$465<<3)|0);
     $144 = +HEAPF64[$143>>3];
     $145 = (($hp1) + ($i$465<<3)|0);
     HEAPF64[$145>>3] = $144;
     $146 = (528 + ($i$465<<3)|0);
     $147 = +HEAPF64[$146>>3];
     $148 = (($lp2) + ($i$465<<3)|0);
     HEAPF64[$148>>3] = $147;
     $149 = (608 + ($i$465<<3)|0);
     $150 = +HEAPF64[$149>>3];
     $151 = (($hp2) + ($i$465<<3)|0);
     HEAPF64[$151>>3] = $150;
     $152 = (($i$465) + 1)|0;
     $exitcond = ($152|0)==(10);
     if ($exitcond) {
      $$0 = 10;
      break;
     } else {
      $i$465 = $152;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $153 = (_strcmp($name,56)|0);
   $154 = ($153|0)==(0);
   if ($154) {
    $i$566 = 0;
    while(1) {
     $155 = (688 + ($i$566<<3)|0);
     $156 = +HEAPF64[$155>>3];
     $157 = (($lp1) + ($i$566<<3)|0);
     HEAPF64[$157>>3] = $156;
     $158 = (784 + ($i$566<<3)|0);
     $159 = +HEAPF64[$158>>3];
     $160 = (($hp1) + ($i$566<<3)|0);
     HEAPF64[$160>>3] = $159;
     $161 = (880 + ($i$566<<3)|0);
     $162 = +HEAPF64[$161>>3];
     $163 = (($lp2) + ($i$566<<3)|0);
     HEAPF64[$163>>3] = $162;
     $164 = (976 + ($i$566<<3)|0);
     $165 = +HEAPF64[$164>>3];
     $166 = (($hp2) + ($i$566<<3)|0);
     HEAPF64[$166>>3] = $165;
     $167 = (($i$566) + 1)|0;
     $exitcond128 = ($167|0)==(12);
     if ($exitcond128) {
      $$0 = 12;
      break;
     } else {
      $i$566 = $167;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $168 = (_strcmp($name,64)|0);
   $169 = ($168|0)==(0);
   if ($169) {
    $i$667 = 0;
    while(1) {
     $170 = (1072 + ($i$667<<3)|0);
     $171 = +HEAPF64[$170>>3];
     $172 = (($lp1) + ($i$667<<3)|0);
     HEAPF64[$172>>3] = $171;
     $173 = (1184 + ($i$667<<3)|0);
     $174 = +HEAPF64[$173>>3];
     $175 = (($hp1) + ($i$667<<3)|0);
     HEAPF64[$175>>3] = $174;
     $176 = (1296 + ($i$667<<3)|0);
     $177 = +HEAPF64[$176>>3];
     $178 = (($lp2) + ($i$667<<3)|0);
     HEAPF64[$178>>3] = $177;
     $179 = (1408 + ($i$667<<3)|0);
     $180 = +HEAPF64[$179>>3];
     $181 = (($hp2) + ($i$667<<3)|0);
     HEAPF64[$181>>3] = $180;
     $182 = (($i$667) + 1)|0;
     $exitcond133 = ($182|0)==(14);
     if ($exitcond133) {
      $$0 = 14;
      break;
     } else {
      $i$667 = $182;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $183 = (_strcmp($name,72)|0);
   $184 = ($183|0)==(0);
   if ($184) {
    $i$768 = 0;
    while(1) {
     $185 = (1520 + ($i$768<<3)|0);
     $186 = +HEAPF64[$185>>3];
     $187 = (($lp1) + ($i$768<<3)|0);
     HEAPF64[$187>>3] = $186;
     $188 = (1648 + ($i$768<<3)|0);
     $189 = +HEAPF64[$188>>3];
     $190 = (($hp1) + ($i$768<<3)|0);
     HEAPF64[$190>>3] = $189;
     $191 = (1776 + ($i$768<<3)|0);
     $192 = +HEAPF64[$191>>3];
     $193 = (($lp2) + ($i$768<<3)|0);
     HEAPF64[$193>>3] = $192;
     $194 = (1904 + ($i$768<<3)|0);
     $195 = +HEAPF64[$194>>3];
     $196 = (($hp2) + ($i$768<<3)|0);
     HEAPF64[$196>>3] = $195;
     $197 = (($i$768) + 1)|0;
     $exitcond138 = ($197|0)==(16);
     if ($exitcond138) {
      $$0 = 16;
      break;
     } else {
      $i$768 = $197;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $198 = (_strcmp($name,80)|0);
   $199 = ($198|0)==(0);
   if ($199) {
    $i$869 = 0;
    while(1) {
     $200 = (2032 + ($i$869<<3)|0);
     $201 = +HEAPF64[$200>>3];
     $202 = (($lp1) + ($i$869<<3)|0);
     HEAPF64[$202>>3] = $201;
     $203 = (2176 + ($i$869<<3)|0);
     $204 = +HEAPF64[$203>>3];
     $205 = (($hp1) + ($i$869<<3)|0);
     HEAPF64[$205>>3] = $204;
     $206 = (2320 + ($i$869<<3)|0);
     $207 = +HEAPF64[$206>>3];
     $208 = (($lp2) + ($i$869<<3)|0);
     HEAPF64[$208>>3] = $207;
     $209 = (2464 + ($i$869<<3)|0);
     $210 = +HEAPF64[$209>>3];
     $211 = (($hp2) + ($i$869<<3)|0);
     HEAPF64[$211>>3] = $210;
     $212 = (($i$869) + 1)|0;
     $exitcond143 = ($212|0)==(18);
     if ($exitcond143) {
      $$0 = 18;
      break;
     } else {
      $i$869 = $212;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $213 = (_strcmp($name,88)|0);
   $214 = ($213|0)==(0);
   if ($214) {
    $i$970 = 0;
    while(1) {
     $215 = (2608 + ($i$970<<3)|0);
     $216 = +HEAPF64[$215>>3];
     $217 = (($lp1) + ($i$970<<3)|0);
     HEAPF64[$217>>3] = $216;
     $218 = (2768 + ($i$970<<3)|0);
     $219 = +HEAPF64[$218>>3];
     $220 = (($hp1) + ($i$970<<3)|0);
     HEAPF64[$220>>3] = $219;
     $221 = (2928 + ($i$970<<3)|0);
     $222 = +HEAPF64[$221>>3];
     $223 = (($lp2) + ($i$970<<3)|0);
     HEAPF64[$223>>3] = $222;
     $224 = (3088 + ($i$970<<3)|0);
     $225 = +HEAPF64[$224>>3];
     $226 = (($hp2) + ($i$970<<3)|0);
     HEAPF64[$226>>3] = $225;
     $227 = (($i$970) + 1)|0;
     $exitcond148 = ($227|0)==(20);
     if ($exitcond148) {
      $$0 = 20;
      break;
     } else {
      $i$970 = $227;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $228 = (_strcmp($name,96)|0);
   $229 = ($228|0)==(0);
   if ($229) {
    $i$1071 = 0;
    while(1) {
     $230 = (3248 + ($i$1071<<3)|0);
     $231 = +HEAPF64[$230>>3];
     $232 = (($lp1) + ($i$1071<<3)|0);
     HEAPF64[$232>>3] = $231;
     $233 = (3440 + ($i$1071<<3)|0);
     $234 = +HEAPF64[$233>>3];
     $235 = (($hp1) + ($i$1071<<3)|0);
     HEAPF64[$235>>3] = $234;
     $236 = (3632 + ($i$1071<<3)|0);
     $237 = +HEAPF64[$236>>3];
     $238 = (($lp2) + ($i$1071<<3)|0);
     HEAPF64[$238>>3] = $237;
     $239 = (3824 + ($i$1071<<3)|0);
     $240 = +HEAPF64[$239>>3];
     $241 = (($hp2) + ($i$1071<<3)|0);
     HEAPF64[$241>>3] = $240;
     $242 = (($i$1071) + 1)|0;
     $exitcond153 = ($242|0)==(24);
     if ($exitcond153) {
      $$0 = 24;
      break;
     } else {
      $i$1071 = $242;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $243 = (_strcmp($name,104)|0);
   $244 = ($243|0)==(0);
   if ($244) {
    $i$1172 = 0;
    while(1) {
     $245 = (4016 + ($i$1172<<3)|0);
     $246 = +HEAPF64[$245>>3];
     $247 = (($lp1) + ($i$1172<<3)|0);
     HEAPF64[$247>>3] = $246;
     $248 = (4224 + ($i$1172<<3)|0);
     $249 = +HEAPF64[$248>>3];
     $250 = (($hp1) + ($i$1172<<3)|0);
     HEAPF64[$250>>3] = $249;
     $251 = (4432 + ($i$1172<<3)|0);
     $252 = +HEAPF64[$251>>3];
     $253 = (($lp2) + ($i$1172<<3)|0);
     HEAPF64[$253>>3] = $252;
     $254 = (4640 + ($i$1172<<3)|0);
     $255 = +HEAPF64[$254>>3];
     $256 = (($hp2) + ($i$1172<<3)|0);
     HEAPF64[$256>>3] = $255;
     $257 = (($i$1172) + 1)|0;
     $exitcond158 = ($257|0)==(26);
     if ($exitcond158) {
      $$0 = 26;
      break;
     } else {
      $i$1172 = $257;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $258 = (_strcmp($name,112)|0);
   $259 = ($258|0)==(0);
   if ($259) {
    $i$1273 = 0;
    while(1) {
     $260 = (4848 + ($i$1273<<3)|0);
     $261 = +HEAPF64[$260>>3];
     $262 = (($lp1) + ($i$1273<<3)|0);
     HEAPF64[$262>>3] = $261;
     $263 = (5024 + ($i$1273<<3)|0);
     $264 = +HEAPF64[$263>>3];
     $265 = (($hp1) + ($i$1273<<3)|0);
     HEAPF64[$265>>3] = $264;
     $266 = (5200 + ($i$1273<<3)|0);
     $267 = +HEAPF64[$266>>3];
     $268 = (($lp2) + ($i$1273<<3)|0);
     HEAPF64[$268>>3] = $267;
     $269 = (5376 + ($i$1273<<3)|0);
     $270 = +HEAPF64[$269>>3];
     $271 = (($hp2) + ($i$1273<<3)|0);
     HEAPF64[$271>>3] = $270;
     $272 = (($i$1273) + 1)|0;
     $exitcond163 = ($272|0)==(22);
     if ($exitcond163) {
      $$0 = 22;
      break;
     } else {
      $i$1273 = $272;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $273 = (_strcmp($name,120)|0);
   $274 = ($273|0)==(0);
   if ($274) {
    $i$1374 = 0;
    while(1) {
     $275 = (5552 + ($i$1374<<3)|0);
     $276 = +HEAPF64[$275>>3];
     $277 = (($lp1) + ($i$1374<<3)|0);
     HEAPF64[$277>>3] = $276;
     $278 = (5776 + ($i$1374<<3)|0);
     $279 = +HEAPF64[$278>>3];
     $280 = (($hp1) + ($i$1374<<3)|0);
     HEAPF64[$280>>3] = $279;
     $281 = (6000 + ($i$1374<<3)|0);
     $282 = +HEAPF64[$281>>3];
     $283 = (($lp2) + ($i$1374<<3)|0);
     HEAPF64[$283>>3] = $282;
     $284 = (6224 + ($i$1374<<3)|0);
     $285 = +HEAPF64[$284>>3];
     $286 = (($hp2) + ($i$1374<<3)|0);
     HEAPF64[$286>>3] = $285;
     $287 = (($i$1374) + 1)|0;
     $exitcond168 = ($287|0)==(28);
     if ($exitcond168) {
      $$0 = 28;
      break;
     } else {
      $i$1374 = $287;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $288 = (_strcmp($name,128)|0);
   $289 = ($288|0)==(0);
   if ($289) {
    $i$1475 = 0;
    while(1) {
     $290 = (6448 + ($i$1475<<3)|0);
     $291 = +HEAPF64[$290>>3];
     $292 = (($lp1) + ($i$1475<<3)|0);
     HEAPF64[$292>>3] = $291;
     $293 = (6688 + ($i$1475<<3)|0);
     $294 = +HEAPF64[$293>>3];
     $295 = (($hp1) + ($i$1475<<3)|0);
     HEAPF64[$295>>3] = $294;
     $296 = (6928 + ($i$1475<<3)|0);
     $297 = +HEAPF64[$296>>3];
     $298 = (($lp2) + ($i$1475<<3)|0);
     HEAPF64[$298>>3] = $297;
     $299 = (7168 + ($i$1475<<3)|0);
     $300 = +HEAPF64[$299>>3];
     $301 = (($hp2) + ($i$1475<<3)|0);
     HEAPF64[$301>>3] = $300;
     $302 = (($i$1475) + 1)|0;
     $exitcond173 = ($302|0)==(30);
     if ($exitcond173) {
      $$0 = 30;
      break;
     } else {
      $i$1475 = $302;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $303 = (_strcmp($name,136)|0);
   $304 = ($303|0)==(0);
   if ($304) {
    HEAPF64[$lp1>>3] = 0.70710678118654757;
    HEAPF64[$hp1>>3] = -0.70710678118654757;
    HEAPF64[$lp2>>3] = 0.70710678118654757;
    HEAPF64[$hp2>>3] = 0.70710678118654757;
    $305 = ((($lp1)) + 8|0);
    HEAPF64[$305>>3] = 0.70710678118654757;
    $306 = ((($hp1)) + 8|0);
    HEAPF64[$306>>3] = 0.70710678118654757;
    $307 = ((($lp2)) + 8|0);
    HEAPF64[$307>>3] = 0.70710678118654757;
    $308 = ((($hp2)) + 8|0);
    HEAPF64[$308>>3] = -0.70710678118654757;
    $$0 = 2;
    STACKTOP = sp;return ($$0|0);
   }
   $309 = (_strcmp($name,144)|0);
   $310 = ($309|0)==(0);
   if ($310) {
    dest=$lp1_a5; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$lp1_a5>>3] = -0.088388347648318447;
    $311 = ((($lp1_a5)) + 8|0);
    HEAPF64[$311>>3] = 0.088388347648318447;
    $312 = ((($lp1_a5)) + 16|0);
    HEAPF64[$312>>3] = 0.70710678118654757;
    $313 = ((($lp1_a5)) + 24|0);
    HEAPF64[$313>>3] = 0.70710678118654757;
    $314 = ((($lp1_a5)) + 32|0);
    HEAPF64[$314>>3] = 0.088388347648318447;
    $315 = ((($lp1_a5)) + 40|0);
    HEAPF64[$315>>3] = -0.088388347648318447;
    dest=$hp1_a6; stop=dest+48|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $316 = ((($hp1_a6)) + 16|0);
    HEAPF64[$316>>3] = -0.70710678118654757;
    $317 = ((($hp1_a6)) + 24|0);
    HEAPF64[$317>>3] = 0.70710678118654757;
    dest=$lp2_a7; stop=dest+48|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $318 = ((($lp2_a7)) + 16|0);
    HEAPF64[$318>>3] = 0.70710678118654757;
    $319 = ((($lp2_a7)) + 24|0);
    HEAPF64[$319>>3] = 0.70710678118654757;
    dest=$hp2_a8; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$hp2_a8>>3] = -0.088388347648318447;
    $320 = ((($hp2_a8)) + 8|0);
    HEAPF64[$320>>3] = -0.088388347648318447;
    $321 = ((($hp2_a8)) + 16|0);
    HEAPF64[$321>>3] = 0.70710678118654757;
    $322 = ((($hp2_a8)) + 24|0);
    HEAPF64[$322>>3] = -0.70710678118654757;
    $323 = ((($hp2_a8)) + 32|0);
    HEAPF64[$323>>3] = 0.088388347648318447;
    $324 = ((($hp2_a8)) + 40|0);
    HEAPF64[$324>>3] = 0.088388347648318447;
    $325 = +HEAPF64[$lp1_a5>>3];
    HEAPF64[$lp1>>3] = $325;
    $326 = +HEAPF64[$hp1_a6>>3];
    HEAPF64[$hp1>>3] = $326;
    $327 = +HEAPF64[$lp2_a7>>3];
    HEAPF64[$lp2>>3] = $327;
    $328 = +HEAPF64[$hp2_a8>>3];
    HEAPF64[$hp2>>3] = $328;
    $329 = ((($lp1_a5)) + 8|0);
    $330 = +HEAPF64[$329>>3];
    $331 = ((($lp1)) + 8|0);
    HEAPF64[$331>>3] = $330;
    $332 = ((($hp1_a6)) + 8|0);
    $333 = +HEAPF64[$332>>3];
    $334 = ((($hp1)) + 8|0);
    HEAPF64[$334>>3] = $333;
    $335 = ((($lp2_a7)) + 8|0);
    $336 = +HEAPF64[$335>>3];
    $337 = ((($lp2)) + 8|0);
    HEAPF64[$337>>3] = $336;
    $338 = ((($hp2_a8)) + 8|0);
    $339 = +HEAPF64[$338>>3];
    $340 = ((($hp2)) + 8|0);
    HEAPF64[$340>>3] = $339;
    $341 = ((($lp1_a5)) + 16|0);
    $342 = +HEAPF64[$341>>3];
    $343 = ((($lp1)) + 16|0);
    HEAPF64[$343>>3] = $342;
    $344 = ((($hp1_a6)) + 16|0);
    $345 = +HEAPF64[$344>>3];
    $346 = ((($hp1)) + 16|0);
    HEAPF64[$346>>3] = $345;
    $347 = ((($lp2_a7)) + 16|0);
    $348 = +HEAPF64[$347>>3];
    $349 = ((($lp2)) + 16|0);
    HEAPF64[$349>>3] = $348;
    $350 = ((($hp2_a8)) + 16|0);
    $351 = +HEAPF64[$350>>3];
    $352 = ((($hp2)) + 16|0);
    HEAPF64[$352>>3] = $351;
    $353 = ((($lp1_a5)) + 24|0);
    $354 = +HEAPF64[$353>>3];
    $355 = ((($lp1)) + 24|0);
    HEAPF64[$355>>3] = $354;
    $356 = ((($hp1_a6)) + 24|0);
    $357 = +HEAPF64[$356>>3];
    $358 = ((($hp1)) + 24|0);
    HEAPF64[$358>>3] = $357;
    $359 = ((($lp2_a7)) + 24|0);
    $360 = +HEAPF64[$359>>3];
    $361 = ((($lp2)) + 24|0);
    HEAPF64[$361>>3] = $360;
    $362 = ((($hp2_a8)) + 24|0);
    $363 = +HEAPF64[$362>>3];
    $364 = ((($hp2)) + 24|0);
    HEAPF64[$364>>3] = $363;
    $365 = ((($lp1_a5)) + 32|0);
    $366 = +HEAPF64[$365>>3];
    $367 = ((($lp1)) + 32|0);
    HEAPF64[$367>>3] = $366;
    $368 = ((($hp1_a6)) + 32|0);
    $369 = +HEAPF64[$368>>3];
    $370 = ((($hp1)) + 32|0);
    HEAPF64[$370>>3] = $369;
    $371 = ((($lp2_a7)) + 32|0);
    $372 = +HEAPF64[$371>>3];
    $373 = ((($lp2)) + 32|0);
    HEAPF64[$373>>3] = $372;
    $374 = ((($hp2_a8)) + 32|0);
    $375 = +HEAPF64[$374>>3];
    $376 = ((($hp2)) + 32|0);
    HEAPF64[$376>>3] = $375;
    $377 = ((($lp1_a5)) + 40|0);
    $378 = +HEAPF64[$377>>3];
    $379 = ((($lp1)) + 40|0);
    HEAPF64[$379>>3] = $378;
    $380 = ((($hp1_a6)) + 40|0);
    $381 = +HEAPF64[$380>>3];
    $382 = ((($hp1)) + 40|0);
    HEAPF64[$382>>3] = $381;
    $383 = ((($lp2_a7)) + 40|0);
    $384 = +HEAPF64[$383>>3];
    $385 = ((($lp2)) + 40|0);
    HEAPF64[$385>>3] = $384;
    $386 = ((($hp2_a8)) + 40|0);
    $387 = +HEAPF64[$386>>3];
    $388 = ((($hp2)) + 40|0);
    HEAPF64[$388>>3] = $387;
    $$0 = 6;
    STACKTOP = sp;return ($$0|0);
   }
   $389 = (_strcmp($name,152)|0);
   $390 = ($389|0)==(0);
   if ($390) {
    dest=$lp1_a5; stop=dest+80|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $391 = ((($lp1_a5)) + 32|0);
    HEAPF64[$391>>3] = -0.70710678118654757;
    $392 = ((($lp1_a5)) + 40|0);
    HEAPF64[$392>>3] = 0.70710678118654757;
    dest=$hp1_a6; stop=dest+80|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $393 = ((($hp1_a6)) + 32|0);
    HEAPF64[$393>>3] = 0.70710678118654757;
    $394 = ((($hp1_a6)) + 40|0);
    HEAPF64[$394>>3] = 0.70710678118654757;
    $i$1778 = 0;
    while(1) {
     $395 = (7408 + ($i$1778<<3)|0);
     $396 = +HEAPF64[$395>>3];
     $397 = (($lp1) + ($i$1778<<3)|0);
     HEAPF64[$397>>3] = $396;
     $398 = (($lp1_a5) + ($i$1778<<3)|0);
     $399 = +HEAPF64[$398>>3];
     $400 = (($hp1) + ($i$1778<<3)|0);
     HEAPF64[$400>>3] = $399;
     $401 = (($hp1_a6) + ($i$1778<<3)|0);
     $402 = +HEAPF64[$401>>3];
     $403 = (($lp2) + ($i$1778<<3)|0);
     HEAPF64[$403>>3] = $402;
     $404 = (7488 + ($i$1778<<3)|0);
     $405 = +HEAPF64[$404>>3];
     $406 = (($hp2) + ($i$1778<<3)|0);
     HEAPF64[$406>>3] = $405;
     $407 = (($i$1778) + 1)|0;
     $exitcond189 = ($407|0)==(10);
     if ($exitcond189) {
      break;
     } else {
      $i$1778 = $407;
     }
    }
    $$0 = 10;
    STACKTOP = sp;return ($$0|0);
   }
   $408 = (_strcmp($name,160)|0);
   $409 = ($408|0)==(0);
   if ($409) {
    dest=$lp1_a5; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $410 = ((($lp1_a5)) + 8|0);
    HEAPF64[$410>>3] = -0.17677669529663689;
    $411 = ((($lp1_a5)) + 16|0);
    HEAPF64[$411>>3] = 0.35355339059327379;
    $412 = ((($lp1_a5)) + 24|0);
    HEAPF64[$412>>3] = 1.0606601717798214;
    $413 = ((($lp1_a5)) + 32|0);
    HEAPF64[$413>>3] = 0.35355339059327379;
    $414 = ((($lp1_a5)) + 40|0);
    HEAPF64[$414>>3] = -0.17677669529663689;
    dest=$hp1_a6; stop=dest+48|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $415 = ((($hp1_a6)) + 8|0);
    HEAPF64[$415>>3] = 0.35355339059327379;
    $416 = ((($hp1_a6)) + 16|0);
    HEAPF64[$416>>3] = -0.70710678118654757;
    $417 = ((($hp1_a6)) + 24|0);
    HEAPF64[$417>>3] = 0.35355339059327379;
    dest=$lp2_a7; stop=dest+48|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $418 = ((($lp2_a7)) + 8|0);
    HEAPF64[$418>>3] = 0.35355339059327379;
    $419 = ((($lp2_a7)) + 16|0);
    HEAPF64[$419>>3] = 0.70710678118654757;
    $420 = ((($lp2_a7)) + 24|0);
    HEAPF64[$420>>3] = 0.35355339059327379;
    dest=$hp2_a8; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $421 = ((($hp2_a8)) + 8|0);
    HEAPF64[$421>>3] = 0.17677669529663689;
    $422 = ((($hp2_a8)) + 16|0);
    HEAPF64[$422>>3] = 0.35355339059327379;
    $423 = ((($hp2_a8)) + 24|0);
    HEAPF64[$423>>3] = -1.0606601717798214;
    $424 = ((($hp2_a8)) + 32|0);
    HEAPF64[$424>>3] = 0.35355339059327379;
    $425 = ((($hp2_a8)) + 40|0);
    HEAPF64[$425>>3] = 0.17677669529663689;
    $426 = +HEAPF64[$lp1_a5>>3];
    HEAPF64[$lp1>>3] = $426;
    $427 = +HEAPF64[$hp1_a6>>3];
    HEAPF64[$hp1>>3] = $427;
    $428 = +HEAPF64[$lp2_a7>>3];
    HEAPF64[$lp2>>3] = $428;
    $429 = +HEAPF64[$hp2_a8>>3];
    HEAPF64[$hp2>>3] = $429;
    $430 = ((($lp1_a5)) + 8|0);
    $431 = +HEAPF64[$430>>3];
    $432 = ((($lp1)) + 8|0);
    HEAPF64[$432>>3] = $431;
    $433 = ((($hp1_a6)) + 8|0);
    $434 = +HEAPF64[$433>>3];
    $435 = ((($hp1)) + 8|0);
    HEAPF64[$435>>3] = $434;
    $436 = ((($lp2_a7)) + 8|0);
    $437 = +HEAPF64[$436>>3];
    $438 = ((($lp2)) + 8|0);
    HEAPF64[$438>>3] = $437;
    $439 = ((($hp2_a8)) + 8|0);
    $440 = +HEAPF64[$439>>3];
    $441 = ((($hp2)) + 8|0);
    HEAPF64[$441>>3] = $440;
    $442 = ((($lp1_a5)) + 16|0);
    $443 = +HEAPF64[$442>>3];
    $444 = ((($lp1)) + 16|0);
    HEAPF64[$444>>3] = $443;
    $445 = ((($hp1_a6)) + 16|0);
    $446 = +HEAPF64[$445>>3];
    $447 = ((($hp1)) + 16|0);
    HEAPF64[$447>>3] = $446;
    $448 = ((($lp2_a7)) + 16|0);
    $449 = +HEAPF64[$448>>3];
    $450 = ((($lp2)) + 16|0);
    HEAPF64[$450>>3] = $449;
    $451 = ((($hp2_a8)) + 16|0);
    $452 = +HEAPF64[$451>>3];
    $453 = ((($hp2)) + 16|0);
    HEAPF64[$453>>3] = $452;
    $454 = ((($lp1_a5)) + 24|0);
    $455 = +HEAPF64[$454>>3];
    $456 = ((($lp1)) + 24|0);
    HEAPF64[$456>>3] = $455;
    $457 = ((($hp1_a6)) + 24|0);
    $458 = +HEAPF64[$457>>3];
    $459 = ((($hp1)) + 24|0);
    HEAPF64[$459>>3] = $458;
    $460 = ((($lp2_a7)) + 24|0);
    $461 = +HEAPF64[$460>>3];
    $462 = ((($lp2)) + 24|0);
    HEAPF64[$462>>3] = $461;
    $463 = ((($hp2_a8)) + 24|0);
    $464 = +HEAPF64[$463>>3];
    $465 = ((($hp2)) + 24|0);
    HEAPF64[$465>>3] = $464;
    $466 = ((($lp1_a5)) + 32|0);
    $467 = +HEAPF64[$466>>3];
    $468 = ((($lp1)) + 32|0);
    HEAPF64[$468>>3] = $467;
    $469 = ((($hp1_a6)) + 32|0);
    $470 = +HEAPF64[$469>>3];
    $471 = ((($hp1)) + 32|0);
    HEAPF64[$471>>3] = $470;
    $472 = ((($lp2_a7)) + 32|0);
    $473 = +HEAPF64[$472>>3];
    $474 = ((($lp2)) + 32|0);
    HEAPF64[$474>>3] = $473;
    $475 = ((($hp2_a8)) + 32|0);
    $476 = +HEAPF64[$475>>3];
    $477 = ((($hp2)) + 32|0);
    HEAPF64[$477>>3] = $476;
    $478 = ((($lp1_a5)) + 40|0);
    $479 = +HEAPF64[$478>>3];
    $480 = ((($lp1)) + 40|0);
    HEAPF64[$480>>3] = $479;
    $481 = ((($hp1_a6)) + 40|0);
    $482 = +HEAPF64[$481>>3];
    $483 = ((($hp1)) + 40|0);
    HEAPF64[$483>>3] = $482;
    $484 = ((($lp2_a7)) + 40|0);
    $485 = +HEAPF64[$484>>3];
    $486 = ((($lp2)) + 40|0);
    HEAPF64[$486>>3] = $485;
    $487 = ((($hp2_a8)) + 40|0);
    $488 = +HEAPF64[$487>>3];
    $489 = ((($hp2)) + 40|0);
    HEAPF64[$489>>3] = $488;
    $$0 = 6;
    STACKTOP = sp;return ($$0|0);
   }
   $490 = (_strcmp($name,168)|0);
   $491 = ($490|0)==(0);
   if ($491) {
    dest=$lp1_a5; stop=dest+80|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $492 = ((($lp1_a5)) + 24|0);
    HEAPF64[$492>>3] = 0.35355339059327379;
    $493 = ((($lp1_a5)) + 32|0);
    HEAPF64[$493>>3] = -0.70710678118654757;
    $494 = ((($lp1_a5)) + 40|0);
    HEAPF64[$494>>3] = 0.35355339059327379;
    dest=$hp1_a6; stop=dest+80|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $495 = ((($hp1_a6)) + 24|0);
    HEAPF64[$495>>3] = 0.35355339059327379;
    $496 = ((($hp1_a6)) + 32|0);
    HEAPF64[$496>>3] = 0.70710678118654757;
    $497 = ((($hp1_a6)) + 40|0);
    HEAPF64[$497>>3] = 0.35355339059327379;
    $i$1980 = 0;
    while(1) {
     $498 = (7568 + ($i$1980<<3)|0);
     $499 = +HEAPF64[$498>>3];
     $500 = (($lp1) + ($i$1980<<3)|0);
     HEAPF64[$500>>3] = $499;
     $501 = (($lp1_a5) + ($i$1980<<3)|0);
     $502 = +HEAPF64[$501>>3];
     $503 = (($hp1) + ($i$1980<<3)|0);
     HEAPF64[$503>>3] = $502;
     $504 = (($hp1_a6) + ($i$1980<<3)|0);
     $505 = +HEAPF64[$504>>3];
     $506 = (($lp2) + ($i$1980<<3)|0);
     HEAPF64[$506>>3] = $505;
     $507 = (7648 + ($i$1980<<3)|0);
     $508 = +HEAPF64[$507>>3];
     $509 = (($hp2) + ($i$1980<<3)|0);
     HEAPF64[$509>>3] = $508;
     $510 = (($i$1980) + 1)|0;
     $exitcond199 = ($510|0)==(10);
     if ($exitcond199) {
      break;
     } else {
      $i$1980 = $510;
     }
    }
    $$0 = 10;
    STACKTOP = sp;return ($$0|0);
   }
   $511 = (_strcmp($name,176)|0);
   $512 = ($511|0)==(0);
   if ($512) {
    dest=$lp1_a5; stop=dest+112|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $513 = ((($lp1_a5)) + 40|0);
    HEAPF64[$513>>3] = 0.35355339059327379;
    $514 = ((($lp1_a5)) + 48|0);
    HEAPF64[$514>>3] = -0.70710678118654757;
    $515 = ((($lp1_a5)) + 56|0);
    HEAPF64[$515>>3] = 0.35355339059327379;
    dest=$hp1_a6; stop=dest+112|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $516 = ((($hp1_a6)) + 40|0);
    HEAPF64[$516>>3] = 0.35355339059327379;
    $517 = ((($hp1_a6)) + 48|0);
    HEAPF64[$517>>3] = 0.70710678118654757;
    $518 = ((($hp1_a6)) + 56|0);
    HEAPF64[$518>>3] = 0.35355339059327379;
    $i$2081 = 0;
    while(1) {
     $519 = (7728 + ($i$2081<<3)|0);
     $520 = +HEAPF64[$519>>3];
     $521 = (($lp1) + ($i$2081<<3)|0);
     HEAPF64[$521>>3] = $520;
     $522 = (($lp1_a5) + ($i$2081<<3)|0);
     $523 = +HEAPF64[$522>>3];
     $524 = (($hp1) + ($i$2081<<3)|0);
     HEAPF64[$524>>3] = $523;
     $525 = (($hp1_a6) + ($i$2081<<3)|0);
     $526 = +HEAPF64[$525>>3];
     $527 = (($lp2) + ($i$2081<<3)|0);
     HEAPF64[$527>>3] = $526;
     $528 = (7840 + ($i$2081<<3)|0);
     $529 = +HEAPF64[$528>>3];
     $530 = (($hp2) + ($i$2081<<3)|0);
     HEAPF64[$530>>3] = $529;
     $531 = (($i$2081) + 1)|0;
     $exitcond204 = ($531|0)==(14);
     if ($exitcond204) {
      break;
     } else {
      $i$2081 = $531;
     }
    }
    $$0 = 14;
    STACKTOP = sp;return ($$0|0);
   }
   $532 = (_strcmp($name,184)|0);
   $533 = ($532|0)==(0);
   if ($533) {
    _memset(($lp1_a5|0),0,144)|0;
    $534 = ((($lp1_a5)) + 56|0);
    HEAPF64[$534>>3] = 0.35355339059327379;
    $535 = ((($lp1_a5)) + 64|0);
    HEAPF64[$535>>3] = -0.70710678118654757;
    $536 = ((($lp1_a5)) + 72|0);
    HEAPF64[$536>>3] = 0.35355339059327379;
    _memset(($hp1_a6|0),0,144)|0;
    $537 = ((($hp1_a6)) + 56|0);
    HEAPF64[$537>>3] = 0.35355339059327379;
    $538 = ((($hp1_a6)) + 64|0);
    HEAPF64[$538>>3] = 0.70710678118654757;
    $539 = ((($hp1_a6)) + 72|0);
    HEAPF64[$539>>3] = 0.35355339059327379;
    $i$2182 = 0;
    while(1) {
     $540 = (7952 + ($i$2182<<3)|0);
     $541 = +HEAPF64[$540>>3];
     $542 = (($lp1) + ($i$2182<<3)|0);
     HEAPF64[$542>>3] = $541;
     $543 = (($lp1_a5) + ($i$2182<<3)|0);
     $544 = +HEAPF64[$543>>3];
     $545 = (($hp1) + ($i$2182<<3)|0);
     HEAPF64[$545>>3] = $544;
     $546 = (($hp1_a6) + ($i$2182<<3)|0);
     $547 = +HEAPF64[$546>>3];
     $548 = (($lp2) + ($i$2182<<3)|0);
     HEAPF64[$548>>3] = $547;
     $549 = (8096 + ($i$2182<<3)|0);
     $550 = +HEAPF64[$549>>3];
     $551 = (($hp2) + ($i$2182<<3)|0);
     HEAPF64[$551>>3] = $550;
     $552 = (($i$2182) + 1)|0;
     $exitcond209 = ($552|0)==(18);
     if ($exitcond209) {
      break;
     } else {
      $i$2182 = $552;
     }
    }
    $$0 = 18;
    STACKTOP = sp;return ($$0|0);
   }
   $553 = (_strcmp($name,192)|0);
   $554 = ($553|0)==(0);
   if ($554) {
    HEAPF64[$lp1>>3] = -0.35355339059327379;
    HEAPF64[$hp1>>3] = -0.17677669529663689;
    HEAPF64[$lp2>>3] = 0.17677669529663689;
    HEAPF64[$hp2>>3] = -0.35355339059327379;
    $555 = ((($lp1)) + 8|0);
    HEAPF64[$555>>3] = 1.0606601717798214;
    $556 = ((($hp1)) + 8|0);
    HEAPF64[$556>>3] = 0.53033008588991071;
    $557 = ((($lp2)) + 8|0);
    HEAPF64[$557>>3] = 0.53033008588991071;
    $558 = ((($hp2)) + 8|0);
    HEAPF64[$558>>3] = -1.0606601717798214;
    $559 = ((($lp1)) + 16|0);
    HEAPF64[$559>>3] = 1.0606601717798214;
    $560 = ((($hp1)) + 16|0);
    HEAPF64[$560>>3] = -0.53033008588991071;
    $561 = ((($lp2)) + 16|0);
    HEAPF64[$561>>3] = 0.53033008588991071;
    $562 = ((($hp2)) + 16|0);
    HEAPF64[$562>>3] = 1.0606601717798214;
    $563 = ((($lp1)) + 24|0);
    HEAPF64[$563>>3] = -0.35355339059327379;
    $564 = ((($hp1)) + 24|0);
    HEAPF64[$564>>3] = 0.17677669529663689;
    $565 = ((($lp2)) + 24|0);
    HEAPF64[$565>>3] = 0.17677669529663689;
    $566 = ((($hp2)) + 24|0);
    HEAPF64[$566>>3] = 0.35355339059327379;
    $$0 = 4;
    STACKTOP = sp;return ($$0|0);
   }
   $567 = (_strcmp($name,200)|0);
   $568 = ($567|0)==(0);
   if ($568) {
    dest=$lp1_a5; stop=dest+64|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $569 = ((($lp1_a5)) + 16|0);
    HEAPF64[$569>>3] = -0.17677669529663689;
    $570 = ((($lp1_a5)) + 24|0);
    HEAPF64[$570>>3] = 0.53033008588991071;
    $571 = ((($lp1_a5)) + 32|0);
    HEAPF64[$571>>3] = -0.53033008588991071;
    $572 = ((($lp1_a5)) + 40|0);
    HEAPF64[$572>>3] = 0.17677669529663689;
    dest=$hp1_a6; stop=dest+64|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $573 = ((($hp1_a6)) + 16|0);
    HEAPF64[$573>>3] = 0.17677669529663689;
    $574 = ((($hp1_a6)) + 24|0);
    HEAPF64[$574>>3] = 0.53033008588991071;
    $575 = ((($hp1_a6)) + 32|0);
    HEAPF64[$575>>3] = 0.53033008588991071;
    $576 = ((($hp1_a6)) + 40|0);
    HEAPF64[$576>>3] = 0.17677669529663689;
    HEAPF64[$lp1>>3] = 0.066291260736238838;
    $577 = +HEAPF64[$lp1_a5>>3];
    HEAPF64[$hp1>>3] = $577;
    $578 = +HEAPF64[$hp1_a6>>3];
    HEAPF64[$lp2>>3] = $578;
    HEAPF64[$hp2>>3] = 0.066291260736238838;
    $579 = ((($lp1)) + 8|0);
    HEAPF64[$579>>3] = -0.19887378220871652;
    $580 = ((($lp1_a5)) + 8|0);
    $581 = +HEAPF64[$580>>3];
    $582 = ((($hp1)) + 8|0);
    HEAPF64[$582>>3] = $581;
    $583 = ((($hp1_a6)) + 8|0);
    $584 = +HEAPF64[$583>>3];
    $585 = ((($lp2)) + 8|0);
    HEAPF64[$585>>3] = $584;
    $586 = ((($hp2)) + 8|0);
    HEAPF64[$586>>3] = 0.19887378220871652;
    $587 = ((($lp1)) + 16|0);
    HEAPF64[$587>>3] = -0.15467960838455727;
    $588 = ((($lp1_a5)) + 16|0);
    $589 = +HEAPF64[$588>>3];
    $590 = ((($hp1)) + 16|0);
    HEAPF64[$590>>3] = $589;
    $591 = ((($hp1_a6)) + 16|0);
    $592 = +HEAPF64[$591>>3];
    $593 = ((($lp2)) + 16|0);
    HEAPF64[$593>>3] = $592;
    $594 = ((($hp2)) + 16|0);
    HEAPF64[$594>>3] = -0.15467960838455727;
    $595 = ((($lp1)) + 24|0);
    HEAPF64[$595>>3] = 0.99436891104358249;
    $596 = ((($lp1_a5)) + 24|0);
    $597 = +HEAPF64[$596>>3];
    $598 = ((($hp1)) + 24|0);
    HEAPF64[$598>>3] = $597;
    $599 = ((($hp1_a6)) + 24|0);
    $600 = +HEAPF64[$599>>3];
    $601 = ((($lp2)) + 24|0);
    HEAPF64[$601>>3] = $600;
    $602 = ((($hp2)) + 24|0);
    HEAPF64[$602>>3] = -0.99436891104358249;
    $603 = ((($lp1)) + 32|0);
    HEAPF64[$603>>3] = 0.99436891104358249;
    $604 = ((($lp1_a5)) + 32|0);
    $605 = +HEAPF64[$604>>3];
    $606 = ((($hp1)) + 32|0);
    HEAPF64[$606>>3] = $605;
    $607 = ((($hp1_a6)) + 32|0);
    $608 = +HEAPF64[$607>>3];
    $609 = ((($lp2)) + 32|0);
    HEAPF64[$609>>3] = $608;
    $610 = ((($hp2)) + 32|0);
    HEAPF64[$610>>3] = 0.99436891104358249;
    $611 = ((($lp1)) + 40|0);
    HEAPF64[$611>>3] = -0.15467960838455727;
    $612 = ((($lp1_a5)) + 40|0);
    $613 = +HEAPF64[$612>>3];
    $614 = ((($hp1)) + 40|0);
    HEAPF64[$614>>3] = $613;
    $615 = ((($hp1_a6)) + 40|0);
    $616 = +HEAPF64[$615>>3];
    $617 = ((($lp2)) + 40|0);
    HEAPF64[$617>>3] = $616;
    $618 = ((($hp2)) + 40|0);
    HEAPF64[$618>>3] = 0.15467960838455727;
    $619 = ((($lp1)) + 48|0);
    HEAPF64[$619>>3] = -0.19887378220871652;
    $620 = ((($lp1_a5)) + 48|0);
    $621 = +HEAPF64[$620>>3];
    $622 = ((($hp1)) + 48|0);
    HEAPF64[$622>>3] = $621;
    $623 = ((($hp1_a6)) + 48|0);
    $624 = +HEAPF64[$623>>3];
    $625 = ((($lp2)) + 48|0);
    HEAPF64[$625>>3] = $624;
    $626 = ((($hp2)) + 48|0);
    HEAPF64[$626>>3] = -0.19887378220871652;
    $627 = ((($lp1)) + 56|0);
    HEAPF64[$627>>3] = 0.066291260736238838;
    $628 = ((($lp1_a5)) + 56|0);
    $629 = +HEAPF64[$628>>3];
    $630 = ((($hp1)) + 56|0);
    HEAPF64[$630>>3] = $629;
    $631 = ((($hp1_a6)) + 56|0);
    $632 = +HEAPF64[$631>>3];
    $633 = ((($lp2)) + 56|0);
    HEAPF64[$633>>3] = $632;
    $634 = ((($hp2)) + 56|0);
    HEAPF64[$634>>3] = -0.066291260736238838;
    $$0 = 8;
    STACKTOP = sp;return ($$0|0);
   }
   $635 = (_strcmp($name,208)|0);
   $636 = ($635|0)==(0);
   if ($636) {
    dest=$lp1_a5; stop=dest+96|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $637 = ((($lp1_a5)) + 32|0);
    HEAPF64[$637>>3] = -0.17677669529663689;
    $638 = ((($lp1_a5)) + 40|0);
    HEAPF64[$638>>3] = 0.53033008588991071;
    $639 = ((($lp1_a5)) + 48|0);
    HEAPF64[$639>>3] = -0.53033008588991071;
    $640 = ((($lp1_a5)) + 56|0);
    HEAPF64[$640>>3] = 0.17677669529663689;
    dest=$hp1_a6; stop=dest+96|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $641 = ((($hp1_a6)) + 32|0);
    HEAPF64[$641>>3] = 0.17677669529663689;
    $642 = ((($hp1_a6)) + 40|0);
    HEAPF64[$642>>3] = 0.53033008588991071;
    $643 = ((($hp1_a6)) + 48|0);
    HEAPF64[$643>>3] = 0.53033008588991071;
    $644 = ((($hp1_a6)) + 56|0);
    HEAPF64[$644>>3] = 0.17677669529663689;
    $i$2485 = 0;
    while(1) {
     $645 = (8240 + ($i$2485<<3)|0);
     $646 = +HEAPF64[$645>>3];
     $647 = (($lp1) + ($i$2485<<3)|0);
     HEAPF64[$647>>3] = $646;
     $648 = (($lp1_a5) + ($i$2485<<3)|0);
     $649 = +HEAPF64[$648>>3];
     $650 = (($hp1) + ($i$2485<<3)|0);
     HEAPF64[$650>>3] = $649;
     $651 = (($hp1_a6) + ($i$2485<<3)|0);
     $652 = +HEAPF64[$651>>3];
     $653 = (($lp2) + ($i$2485<<3)|0);
     HEAPF64[$653>>3] = $652;
     $654 = (8336 + ($i$2485<<3)|0);
     $655 = +HEAPF64[$654>>3];
     $656 = (($hp2) + ($i$2485<<3)|0);
     HEAPF64[$656>>3] = $655;
     $657 = (($i$2485) + 1)|0;
     $exitcond225 = ($657|0)==(12);
     if ($exitcond225) {
      break;
     } else {
      $i$2485 = $657;
     }
    }
    $$0 = 12;
    STACKTOP = sp;return ($$0|0);
   }
   $658 = (_strcmp($name,216)|0);
   $659 = ($658|0)==(0);
   if ($659) {
    dest=$lp1_a5; stop=dest+128|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $660 = ((($lp1_a5)) + 48|0);
    HEAPF64[$660>>3] = -0.17677669529663689;
    $661 = ((($lp1_a5)) + 56|0);
    HEAPF64[$661>>3] = 0.53033008588991071;
    $662 = ((($lp1_a5)) + 64|0);
    HEAPF64[$662>>3] = -0.53033008588991071;
    $663 = ((($lp1_a5)) + 72|0);
    HEAPF64[$663>>3] = 0.17677669529663689;
    dest=$hp1_a6; stop=dest+128|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    $664 = ((($hp1_a6)) + 48|0);
    HEAPF64[$664>>3] = 0.17677669529663689;
    $665 = ((($hp1_a6)) + 56|0);
    HEAPF64[$665>>3] = 0.53033008588991071;
    $666 = ((($hp1_a6)) + 64|0);
    HEAPF64[$666>>3] = 0.53033008588991071;
    $667 = ((($hp1_a6)) + 72|0);
    HEAPF64[$667>>3] = 0.17677669529663689;
    $i$2586 = 0;
    while(1) {
     $668 = (8432 + ($i$2586<<3)|0);
     $669 = +HEAPF64[$668>>3];
     $670 = (($lp1) + ($i$2586<<3)|0);
     HEAPF64[$670>>3] = $669;
     $671 = (($lp1_a5) + ($i$2586<<3)|0);
     $672 = +HEAPF64[$671>>3];
     $673 = (($hp1) + ($i$2586<<3)|0);
     HEAPF64[$673>>3] = $672;
     $674 = (($hp1_a6) + ($i$2586<<3)|0);
     $675 = +HEAPF64[$674>>3];
     $676 = (($lp2) + ($i$2586<<3)|0);
     HEAPF64[$676>>3] = $675;
     $677 = (8560 + ($i$2586<<3)|0);
     $678 = +HEAPF64[$677>>3];
     $679 = (($hp2) + ($i$2586<<3)|0);
     HEAPF64[$679>>3] = $678;
     $680 = (($i$2586) + 1)|0;
     $exitcond230 = ($680|0)==(16);
     if ($exitcond230) {
      break;
     } else {
      $i$2586 = $680;
     }
    }
    $$0 = 16;
    STACKTOP = sp;return ($$0|0);
   }
   $681 = (_strcmp($name,224)|0);
   $682 = ($681|0)==(0);
   if ($682) {
    _memset(($lp1_a5|0),0,160)|0;
    $683 = ((($lp1_a5)) + 64|0);
    HEAPF64[$683>>3] = -0.17677669529663689;
    $684 = ((($lp1_a5)) + 72|0);
    HEAPF64[$684>>3] = 0.53033008588991071;
    $685 = ((($lp1_a5)) + 80|0);
    HEAPF64[$685>>3] = -0.53033008588991071;
    $686 = ((($lp1_a5)) + 88|0);
    HEAPF64[$686>>3] = 0.17677669529663689;
    _memset(($hp1_a6|0),0,160)|0;
    $687 = ((($hp1_a6)) + 64|0);
    HEAPF64[$687>>3] = 0.17677669529663689;
    $688 = ((($hp1_a6)) + 72|0);
    HEAPF64[$688>>3] = 0.53033008588991071;
    $689 = ((($hp1_a6)) + 80|0);
    HEAPF64[$689>>3] = 0.53033008588991071;
    $690 = ((($hp1_a6)) + 88|0);
    HEAPF64[$690>>3] = 0.17677669529663689;
    $i$2687 = 0;
    while(1) {
     $691 = (8688 + ($i$2687<<3)|0);
     $692 = +HEAPF64[$691>>3];
     $693 = (($lp1) + ($i$2687<<3)|0);
     HEAPF64[$693>>3] = $692;
     $694 = (($lp1_a5) + ($i$2687<<3)|0);
     $695 = +HEAPF64[$694>>3];
     $696 = (($hp1) + ($i$2687<<3)|0);
     HEAPF64[$696>>3] = $695;
     $697 = (($hp1_a6) + ($i$2687<<3)|0);
     $698 = +HEAPF64[$697>>3];
     $699 = (($lp2) + ($i$2687<<3)|0);
     HEAPF64[$699>>3] = $698;
     $700 = (8848 + ($i$2687<<3)|0);
     $701 = +HEAPF64[$700>>3];
     $702 = (($hp2) + ($i$2687<<3)|0);
     HEAPF64[$702>>3] = $701;
     $703 = (($i$2687) + 1)|0;
     $exitcond235 = ($703|0)==(20);
     if ($exitcond235) {
      break;
     } else {
      $i$2687 = $703;
     }
    }
    $$0 = 20;
    STACKTOP = sp;return ($$0|0);
   }
   $704 = (_strcmp($name,232)|0);
   $705 = ($704|0)==(0);
   if ($705) {
    $i$2788 = 0;
    while(1) {
     $706 = (9008 + ($i$2788<<3)|0);
     $707 = +HEAPF64[$706>>3];
     $708 = (($lp1) + ($i$2788<<3)|0);
     HEAPF64[$708>>3] = $707;
     $709 = (9088 + ($i$2788<<3)|0);
     $710 = +HEAPF64[$709>>3];
     $711 = (($hp1) + ($i$2788<<3)|0);
     HEAPF64[$711>>3] = $710;
     $712 = (9168 + ($i$2788<<3)|0);
     $713 = +HEAPF64[$712>>3];
     $714 = (($lp2) + ($i$2788<<3)|0);
     HEAPF64[$714>>3] = $713;
     $715 = (9248 + ($i$2788<<3)|0);
     $716 = +HEAPF64[$715>>3];
     $717 = (($hp2) + ($i$2788<<3)|0);
     HEAPF64[$717>>3] = $716;
     $718 = (($i$2788) + 1)|0;
     $exitcond240 = ($718|0)==(10);
     if ($exitcond240) {
      $$0 = 10;
      break;
     } else {
      $i$2788 = $718;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $719 = (_strcmp($name,240)|0);
   $720 = ($719|0)==(0);
   if ($720) {
    $i$2889 = 0;
    while(1) {
     $721 = (9328 + ($i$2889<<3)|0);
     $722 = +HEAPF64[$721>>3];
     $723 = (($lp1) + ($i$2889<<3)|0);
     HEAPF64[$723>>3] = $722;
     $724 = (9424 + ($i$2889<<3)|0);
     $725 = +HEAPF64[$724>>3];
     $726 = (($hp1) + ($i$2889<<3)|0);
     HEAPF64[$726>>3] = $725;
     $727 = (9520 + ($i$2889<<3)|0);
     $728 = +HEAPF64[$727>>3];
     $729 = (($lp2) + ($i$2889<<3)|0);
     HEAPF64[$729>>3] = $728;
     $730 = (9616 + ($i$2889<<3)|0);
     $731 = +HEAPF64[$730>>3];
     $732 = (($hp2) + ($i$2889<<3)|0);
     HEAPF64[$732>>3] = $731;
     $733 = (($i$2889) + 1)|0;
     $exitcond245 = ($733|0)==(12);
     if ($exitcond245) {
      $$0 = 12;
      break;
     } else {
      $i$2889 = $733;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $734 = (_strcmp($name,248)|0);
   $735 = ($734|0)==(0);
   if ($735) {
    $i$2990 = 0;
    while(1) {
     $736 = (9712 + ($i$2990<<3)|0);
     $737 = +HEAPF64[$736>>3];
     $738 = (($lp1) + ($i$2990<<3)|0);
     HEAPF64[$738>>3] = $737;
     $739 = (9856 + ($i$2990<<3)|0);
     $740 = +HEAPF64[$739>>3];
     $741 = (($hp1) + ($i$2990<<3)|0);
     HEAPF64[$741>>3] = $740;
     $742 = (10000 + ($i$2990<<3)|0);
     $743 = +HEAPF64[$742>>3];
     $744 = (($lp2) + ($i$2990<<3)|0);
     HEAPF64[$744>>3] = $743;
     $745 = (10144 + ($i$2990<<3)|0);
     $746 = +HEAPF64[$745>>3];
     $747 = (($hp2) + ($i$2990<<3)|0);
     HEAPF64[$747>>3] = $746;
     $748 = (($i$2990) + 1)|0;
     $exitcond250 = ($748|0)==(18);
     if ($exitcond250) {
      $$0 = 18;
      break;
     } else {
      $i$2990 = $748;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $749 = (_strcmp($name,256)|0);
   $750 = ($749|0)==(0);
   if ($750) {
    dest=$lp1_a5; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$lp1_a5>>3] = -0.01565572813546454;
    $751 = ((($lp1_a5)) + 8|0);
    HEAPF64[$751>>3] = -0.072732619512853897;
    $752 = ((($lp1_a5)) + 16|0);
    HEAPF64[$752>>3] = 0.38486484686420286;
    $753 = ((($lp1_a5)) + 24|0);
    HEAPF64[$753>>3] = 0.85257202021225542;
    $754 = ((($lp1_a5)) + 32|0);
    HEAPF64[$754>>3] = 0.33789766245780922;
    $755 = ((($lp1_a5)) + 40|0);
    HEAPF64[$755>>3] = -0.072732619512853897;
    dest=$hp1_a6; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$hp1_a6>>3] = 0.072732619512853897;
    $756 = ((($hp1_a6)) + 8|0);
    HEAPF64[$756>>3] = 0.33789766245780922;
    $757 = ((($hp1_a6)) + 16|0);
    HEAPF64[$757>>3] = -0.85257202021225542;
    $758 = ((($hp1_a6)) + 24|0);
    HEAPF64[$758>>3] = 0.38486484686420286;
    $759 = ((($hp1_a6)) + 32|0);
    HEAPF64[$759>>3] = 0.072732619512853897;
    $760 = ((($hp1_a6)) + 40|0);
    HEAPF64[$760>>3] = -0.01565572813546454;
    dest=$lp2_a7; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$lp2_a7>>3] = -0.072732619512853897;
    $761 = ((($lp2_a7)) + 8|0);
    HEAPF64[$761>>3] = 0.33789766245780922;
    $762 = ((($lp2_a7)) + 16|0);
    HEAPF64[$762>>3] = 0.85257202021225542;
    $763 = ((($lp2_a7)) + 24|0);
    HEAPF64[$763>>3] = 0.38486484686420286;
    $764 = ((($lp2_a7)) + 32|0);
    HEAPF64[$764>>3] = -0.072732619512853897;
    $765 = ((($lp2_a7)) + 40|0);
    HEAPF64[$765>>3] = -0.01565572813546454;
    dest=$hp2_a8; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$hp2_a8>>3] = -0.01565572813546454;
    $766 = ((($hp2_a8)) + 8|0);
    HEAPF64[$766>>3] = 0.072732619512853897;
    $767 = ((($hp2_a8)) + 16|0);
    HEAPF64[$767>>3] = 0.38486484686420286;
    $768 = ((($hp2_a8)) + 24|0);
    HEAPF64[$768>>3] = -0.85257202021225542;
    $769 = ((($hp2_a8)) + 32|0);
    HEAPF64[$769>>3] = 0.33789766245780922;
    $770 = ((($hp2_a8)) + 40|0);
    HEAPF64[$770>>3] = 0.072732619512853897;
    $771 = +HEAPF64[$lp1_a5>>3];
    HEAPF64[$lp1>>3] = $771;
    $772 = +HEAPF64[$hp1_a6>>3];
    HEAPF64[$hp1>>3] = $772;
    $773 = +HEAPF64[$lp2_a7>>3];
    HEAPF64[$lp2>>3] = $773;
    $774 = +HEAPF64[$hp2_a8>>3];
    HEAPF64[$hp2>>3] = $774;
    $775 = ((($lp1_a5)) + 8|0);
    $776 = +HEAPF64[$775>>3];
    $777 = ((($lp1)) + 8|0);
    HEAPF64[$777>>3] = $776;
    $778 = ((($hp1_a6)) + 8|0);
    $779 = +HEAPF64[$778>>3];
    $780 = ((($hp1)) + 8|0);
    HEAPF64[$780>>3] = $779;
    $781 = ((($lp2_a7)) + 8|0);
    $782 = +HEAPF64[$781>>3];
    $783 = ((($lp2)) + 8|0);
    HEAPF64[$783>>3] = $782;
    $784 = ((($hp2_a8)) + 8|0);
    $785 = +HEAPF64[$784>>3];
    $786 = ((($hp2)) + 8|0);
    HEAPF64[$786>>3] = $785;
    $787 = ((($lp1_a5)) + 16|0);
    $788 = +HEAPF64[$787>>3];
    $789 = ((($lp1)) + 16|0);
    HEAPF64[$789>>3] = $788;
    $790 = ((($hp1_a6)) + 16|0);
    $791 = +HEAPF64[$790>>3];
    $792 = ((($hp1)) + 16|0);
    HEAPF64[$792>>3] = $791;
    $793 = ((($lp2_a7)) + 16|0);
    $794 = +HEAPF64[$793>>3];
    $795 = ((($lp2)) + 16|0);
    HEAPF64[$795>>3] = $794;
    $796 = ((($hp2_a8)) + 16|0);
    $797 = +HEAPF64[$796>>3];
    $798 = ((($hp2)) + 16|0);
    HEAPF64[$798>>3] = $797;
    $799 = ((($lp1_a5)) + 24|0);
    $800 = +HEAPF64[$799>>3];
    $801 = ((($lp1)) + 24|0);
    HEAPF64[$801>>3] = $800;
    $802 = ((($hp1_a6)) + 24|0);
    $803 = +HEAPF64[$802>>3];
    $804 = ((($hp1)) + 24|0);
    HEAPF64[$804>>3] = $803;
    $805 = ((($lp2_a7)) + 24|0);
    $806 = +HEAPF64[$805>>3];
    $807 = ((($lp2)) + 24|0);
    HEAPF64[$807>>3] = $806;
    $808 = ((($hp2_a8)) + 24|0);
    $809 = +HEAPF64[$808>>3];
    $810 = ((($hp2)) + 24|0);
    HEAPF64[$810>>3] = $809;
    $811 = ((($lp1_a5)) + 32|0);
    $812 = +HEAPF64[$811>>3];
    $813 = ((($lp1)) + 32|0);
    HEAPF64[$813>>3] = $812;
    $814 = ((($hp1_a6)) + 32|0);
    $815 = +HEAPF64[$814>>3];
    $816 = ((($hp1)) + 32|0);
    HEAPF64[$816>>3] = $815;
    $817 = ((($lp2_a7)) + 32|0);
    $818 = +HEAPF64[$817>>3];
    $819 = ((($lp2)) + 32|0);
    HEAPF64[$819>>3] = $818;
    $820 = ((($hp2_a8)) + 32|0);
    $821 = +HEAPF64[$820>>3];
    $822 = ((($hp2)) + 32|0);
    HEAPF64[$822>>3] = $821;
    $823 = ((($lp1_a5)) + 40|0);
    $824 = +HEAPF64[$823>>3];
    $825 = ((($lp1)) + 40|0);
    HEAPF64[$825>>3] = $824;
    $826 = ((($hp1_a6)) + 40|0);
    $827 = +HEAPF64[$826>>3];
    $828 = ((($hp1)) + 40|0);
    HEAPF64[$828>>3] = $827;
    $829 = ((($lp2_a7)) + 40|0);
    $830 = +HEAPF64[$829>>3];
    $831 = ((($lp2)) + 40|0);
    HEAPF64[$831>>3] = $830;
    $832 = ((($hp2_a8)) + 40|0);
    $833 = +HEAPF64[$832>>3];
    $834 = ((($hp2)) + 40|0);
    HEAPF64[$834>>3] = $833;
    $$0 = 6;
    STACKTOP = sp;return ($$0|0);
   }
   $835 = (_strcmp($name,264)|0);
   $836 = ($835|0)==(0);
   if ($836) {
    $i$3192 = 0;
    while(1) {
     $837 = (10288 + ($i$3192<<3)|0);
     $838 = +HEAPF64[$837>>3];
     $839 = (($lp1) + ($i$3192<<3)|0);
     HEAPF64[$839>>3] = $838;
     $840 = (10384 + ($i$3192<<3)|0);
     $841 = +HEAPF64[$840>>3];
     $842 = (($hp1) + ($i$3192<<3)|0);
     HEAPF64[$842>>3] = $841;
     $843 = (10480 + ($i$3192<<3)|0);
     $844 = +HEAPF64[$843>>3];
     $845 = (($lp2) + ($i$3192<<3)|0);
     HEAPF64[$845>>3] = $844;
     $846 = (10576 + ($i$3192<<3)|0);
     $847 = +HEAPF64[$846>>3];
     $848 = (($hp2) + ($i$3192<<3)|0);
     HEAPF64[$848>>3] = $847;
     $849 = (($i$3192) + 1)|0;
     $exitcond260 = ($849|0)==(12);
     if ($exitcond260) {
      $$0 = 12;
      break;
     } else {
      $i$3192 = $849;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $850 = (_strcmp($name,272)|0);
   $851 = ($850|0)==(0);
   if ($851) {
    $i$3293 = 0;
    while(1) {
     $852 = (10672 + ($i$3293<<3)|0);
     $853 = +HEAPF64[$852>>3];
     $854 = (($lp1) + ($i$3293<<3)|0);
     HEAPF64[$854>>3] = $853;
     $855 = (10816 + ($i$3293<<3)|0);
     $856 = +HEAPF64[$855>>3];
     $857 = (($hp1) + ($i$3293<<3)|0);
     HEAPF64[$857>>3] = $856;
     $858 = (10960 + ($i$3293<<3)|0);
     $859 = +HEAPF64[$858>>3];
     $860 = (($lp2) + ($i$3293<<3)|0);
     HEAPF64[$860>>3] = $859;
     $861 = (11104 + ($i$3293<<3)|0);
     $862 = +HEAPF64[$861>>3];
     $863 = (($hp2) + ($i$3293<<3)|0);
     HEAPF64[$863>>3] = $862;
     $864 = (($i$3293) + 1)|0;
     $exitcond265 = ($864|0)==(18);
     if ($exitcond265) {
      $$0 = 18;
      break;
     } else {
      $i$3293 = $864;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $865 = (_strcmp($name,280)|0);
   $866 = ($865|0)==(0);
   if ($866) {
    $i$3394 = 0;
    while(1) {
     $867 = (11248 + ($i$3394<<3)|0);
     $868 = +HEAPF64[$867>>3];
     $869 = (($lp1) + ($i$3394<<3)|0);
     HEAPF64[$869>>3] = $868;
     $870 = (11440 + ($i$3394<<3)|0);
     $871 = +HEAPF64[$870>>3];
     $872 = (($hp1) + ($i$3394<<3)|0);
     HEAPF64[$872>>3] = $871;
     $873 = (11632 + ($i$3394<<3)|0);
     $874 = +HEAPF64[$873>>3];
     $875 = (($lp2) + ($i$3394<<3)|0);
     HEAPF64[$875>>3] = $874;
     $876 = (11824 + ($i$3394<<3)|0);
     $877 = +HEAPF64[$876>>3];
     $878 = (($hp2) + ($i$3394<<3)|0);
     HEAPF64[$878>>3] = $877;
     $879 = (($i$3394) + 1)|0;
     $exitcond270 = ($879|0)==(24);
     if ($exitcond270) {
      $$0 = 24;
      break;
     } else {
      $i$3394 = $879;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $880 = (_strcmp($name,288)|0);
   $881 = ($880|0)==(0);
   if ($881) {
    $i$3495 = 0;
    while(1) {
     $882 = (12016 + ($i$3495<<3)|0);
     $883 = +HEAPF64[$882>>3];
     $884 = (($lp1) + ($i$3495<<3)|0);
     HEAPF64[$884>>3] = $883;
     $885 = (12256 + ($i$3495<<3)|0);
     $886 = +HEAPF64[$885>>3];
     $887 = (($hp1) + ($i$3495<<3)|0);
     HEAPF64[$887>>3] = $886;
     $888 = (12496 + ($i$3495<<3)|0);
     $889 = +HEAPF64[$888>>3];
     $890 = (($lp2) + ($i$3495<<3)|0);
     HEAPF64[$890>>3] = $889;
     $891 = (12736 + ($i$3495<<3)|0);
     $892 = +HEAPF64[$891>>3];
     $893 = (($hp2) + ($i$3495<<3)|0);
     HEAPF64[$893>>3] = $892;
     $894 = (($i$3495) + 1)|0;
     $exitcond275 = ($894|0)==(30);
     if ($exitcond275) {
      $$0 = 30;
      break;
     } else {
      $i$3495 = $894;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $895 = (_strcmp($name,296)|0);
   $896 = ($895|0)==(0);
   if ($896) {
    HEAPF64[$lp1>>3] = -0.12940952255092145;
    HEAPF64[$hp1>>3] = -0.48296291314469025;
    HEAPF64[$lp2>>3] = 0.48296291314469025;
    HEAPF64[$hp2>>3] = -0.12940952255092145;
    $897 = ((($lp1)) + 8|0);
    HEAPF64[$897>>3] = 0.22414386804185735;
    $898 = ((($hp1)) + 8|0);
    HEAPF64[$898>>3] = 0.83651630373746899;
    $899 = ((($lp2)) + 8|0);
    HEAPF64[$899>>3] = 0.83651630373746899;
    $900 = ((($hp2)) + 8|0);
    HEAPF64[$900>>3] = -0.22414386804185735;
    $901 = ((($lp1)) + 16|0);
    HEAPF64[$901>>3] = 0.83651630373746899;
    $902 = ((($hp1)) + 16|0);
    HEAPF64[$902>>3] = -0.22414386804185735;
    $903 = ((($lp2)) + 16|0);
    HEAPF64[$903>>3] = 0.22414386804185735;
    $904 = ((($hp2)) + 16|0);
    HEAPF64[$904>>3] = 0.83651630373746899;
    $905 = ((($lp1)) + 24|0);
    HEAPF64[$905>>3] = 0.48296291314469025;
    $906 = ((($hp1)) + 24|0);
    HEAPF64[$906>>3] = -0.12940952255092145;
    $907 = ((($lp2)) + 24|0);
    HEAPF64[$907>>3] = -0.12940952255092145;
    $908 = ((($hp2)) + 24|0);
    HEAPF64[$908>>3] = -0.48296291314469025;
    $$0 = 4;
    STACKTOP = sp;return ($$0|0);
   }
   $909 = (_strcmp($name,304)|0);
   $910 = ($909|0)==(0);
   if ($910) {
    dest=$lp1_a5; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$lp1_a5>>3] = 0.035226291882100656;
    $911 = ((($lp1_a5)) + 8|0);
    HEAPF64[$911>>3] = -0.085441273882241486;
    $912 = ((($lp1_a5)) + 16|0);
    HEAPF64[$912>>3] = -0.13501102001039084;
    $913 = ((($lp1_a5)) + 24|0);
    HEAPF64[$913>>3] = 0.45987750211933132;
    $914 = ((($lp1_a5)) + 32|0);
    HEAPF64[$914>>3] = 0.80689150931333875;
    $915 = ((($lp1_a5)) + 40|0);
    HEAPF64[$915>>3] = 0.33267055295095688;
    dest=$hp1_a6; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$hp1_a6>>3] = -0.33267055295095688;
    $916 = ((($hp1_a6)) + 8|0);
    HEAPF64[$916>>3] = 0.80689150931333875;
    $917 = ((($hp1_a6)) + 16|0);
    HEAPF64[$917>>3] = -0.45987750211933132;
    $918 = ((($hp1_a6)) + 24|0);
    HEAPF64[$918>>3] = -0.13501102001039084;
    $919 = ((($hp1_a6)) + 32|0);
    HEAPF64[$919>>3] = 0.085441273882241486;
    $920 = ((($hp1_a6)) + 40|0);
    HEAPF64[$920>>3] = 0.035226291882100656;
    dest=$lp2_a7; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$lp2_a7>>3] = 0.33267055295095688;
    $921 = ((($lp2_a7)) + 8|0);
    HEAPF64[$921>>3] = 0.80689150931333875;
    $922 = ((($lp2_a7)) + 16|0);
    HEAPF64[$922>>3] = 0.45987750211933132;
    $923 = ((($lp2_a7)) + 24|0);
    HEAPF64[$923>>3] = -0.13501102001039084;
    $924 = ((($lp2_a7)) + 32|0);
    HEAPF64[$924>>3] = -0.085441273882241486;
    $925 = ((($lp2_a7)) + 40|0);
    HEAPF64[$925>>3] = 0.035226291882100656;
    dest=$hp2_a8; stop=dest+40|0; do { HEAP32[dest>>2]=0|0; dest=dest+4|0; } while ((dest|0) < (stop|0));
    HEAPF64[$hp2_a8>>3] = 0.035226291882100656;
    $926 = ((($hp2_a8)) + 8|0);
    HEAPF64[$926>>3] = 0.085441273882241486;
    $927 = ((($hp2_a8)) + 16|0);
    HEAPF64[$927>>3] = -0.13501102001039084;
    $928 = ((($hp2_a8)) + 24|0);
    HEAPF64[$928>>3] = -0.45987750211933132;
    $929 = ((($hp2_a8)) + 32|0);
    HEAPF64[$929>>3] = 0.80689150931333875;
    $930 = ((($hp2_a8)) + 40|0);
    HEAPF64[$930>>3] = -0.33267055295095688;
    $931 = +HEAPF64[$lp1_a5>>3];
    HEAPF64[$lp1>>3] = $931;
    $932 = +HEAPF64[$hp1_a6>>3];
    HEAPF64[$hp1>>3] = $932;
    $933 = +HEAPF64[$lp2_a7>>3];
    HEAPF64[$lp2>>3] = $933;
    $934 = +HEAPF64[$hp2_a8>>3];
    HEAPF64[$hp2>>3] = $934;
    $935 = ((($lp1_a5)) + 8|0);
    $936 = +HEAPF64[$935>>3];
    $937 = ((($lp1)) + 8|0);
    HEAPF64[$937>>3] = $936;
    $938 = ((($hp1_a6)) + 8|0);
    $939 = +HEAPF64[$938>>3];
    $940 = ((($hp1)) + 8|0);
    HEAPF64[$940>>3] = $939;
    $941 = ((($lp2_a7)) + 8|0);
    $942 = +HEAPF64[$941>>3];
    $943 = ((($lp2)) + 8|0);
    HEAPF64[$943>>3] = $942;
    $944 = ((($hp2_a8)) + 8|0);
    $945 = +HEAPF64[$944>>3];
    $946 = ((($hp2)) + 8|0);
    HEAPF64[$946>>3] = $945;
    $947 = ((($lp1_a5)) + 16|0);
    $948 = +HEAPF64[$947>>3];
    $949 = ((($lp1)) + 16|0);
    HEAPF64[$949>>3] = $948;
    $950 = ((($hp1_a6)) + 16|0);
    $951 = +HEAPF64[$950>>3];
    $952 = ((($hp1)) + 16|0);
    HEAPF64[$952>>3] = $951;
    $953 = ((($lp2_a7)) + 16|0);
    $954 = +HEAPF64[$953>>3];
    $955 = ((($lp2)) + 16|0);
    HEAPF64[$955>>3] = $954;
    $956 = ((($hp2_a8)) + 16|0);
    $957 = +HEAPF64[$956>>3];
    $958 = ((($hp2)) + 16|0);
    HEAPF64[$958>>3] = $957;
    $959 = ((($lp1_a5)) + 24|0);
    $960 = +HEAPF64[$959>>3];
    $961 = ((($lp1)) + 24|0);
    HEAPF64[$961>>3] = $960;
    $962 = ((($hp1_a6)) + 24|0);
    $963 = +HEAPF64[$962>>3];
    $964 = ((($hp1)) + 24|0);
    HEAPF64[$964>>3] = $963;
    $965 = ((($lp2_a7)) + 24|0);
    $966 = +HEAPF64[$965>>3];
    $967 = ((($lp2)) + 24|0);
    HEAPF64[$967>>3] = $966;
    $968 = ((($hp2_a8)) + 24|0);
    $969 = +HEAPF64[$968>>3];
    $970 = ((($hp2)) + 24|0);
    HEAPF64[$970>>3] = $969;
    $971 = ((($lp1_a5)) + 32|0);
    $972 = +HEAPF64[$971>>3];
    $973 = ((($lp1)) + 32|0);
    HEAPF64[$973>>3] = $972;
    $974 = ((($hp1_a6)) + 32|0);
    $975 = +HEAPF64[$974>>3];
    $976 = ((($hp1)) + 32|0);
    HEAPF64[$976>>3] = $975;
    $977 = ((($lp2_a7)) + 32|0);
    $978 = +HEAPF64[$977>>3];
    $979 = ((($lp2)) + 32|0);
    HEAPF64[$979>>3] = $978;
    $980 = ((($hp2_a8)) + 32|0);
    $981 = +HEAPF64[$980>>3];
    $982 = ((($hp2)) + 32|0);
    HEAPF64[$982>>3] = $981;
    $983 = ((($lp1_a5)) + 40|0);
    $984 = +HEAPF64[$983>>3];
    $985 = ((($lp1)) + 40|0);
    HEAPF64[$985>>3] = $984;
    $986 = ((($hp1_a6)) + 40|0);
    $987 = +HEAPF64[$986>>3];
    $988 = ((($hp1)) + 40|0);
    HEAPF64[$988>>3] = $987;
    $989 = ((($lp2_a7)) + 40|0);
    $990 = +HEAPF64[$989>>3];
    $991 = ((($lp2)) + 40|0);
    HEAPF64[$991>>3] = $990;
    $992 = ((($hp2_a8)) + 40|0);
    $993 = +HEAPF64[$992>>3];
    $994 = ((($hp2)) + 40|0);
    HEAPF64[$994>>3] = $993;
    $$0 = 6;
    STACKTOP = sp;return ($$0|0);
   }
   $995 = (_strcmp($name,312)|0);
   $996 = ($995|0)==(0);
   if ($996) {
    HEAPF64[$lp1>>3] = -0.075765714789273325;
    HEAPF64[$hp1>>3] = -0.032223100604042702;
    HEAPF64[$lp2>>3] = 0.032223100604042702;
    HEAPF64[$hp2>>3] = -0.075765714789273325;
    $997 = ((($lp1)) + 8|0);
    HEAPF64[$997>>3] = -0.02963552764599851;
    $998 = ((($hp1)) + 8|0);
    HEAPF64[$998>>3] = -0.012603967262037833;
    $999 = ((($lp2)) + 8|0);
    HEAPF64[$999>>3] = -0.012603967262037833;
    $1000 = ((($hp2)) + 8|0);
    HEAPF64[$1000>>3] = 0.02963552764599851;
    $1001 = ((($lp1)) + 16|0);
    HEAPF64[$1001>>3] = 0.49761866763201545;
    $1002 = ((($hp1)) + 16|0);
    HEAPF64[$1002>>3] = 0.099219543576847216;
    $1003 = ((($lp2)) + 16|0);
    HEAPF64[$1003>>3] = -0.099219543576847216;
    $1004 = ((($hp2)) + 16|0);
    HEAPF64[$1004>>3] = 0.49761866763201545;
    $1005 = ((($lp1)) + 24|0);
    HEAPF64[$1005>>3] = 0.80373875180591614;
    $1006 = ((($hp1)) + 24|0);
    HEAPF64[$1006>>3] = 0.29785779560527736;
    $1007 = ((($lp2)) + 24|0);
    HEAPF64[$1007>>3] = 0.29785779560527736;
    $1008 = ((($hp2)) + 24|0);
    HEAPF64[$1008>>3] = -0.80373875180591614;
    $1009 = ((($lp1)) + 32|0);
    HEAPF64[$1009>>3] = 0.29785779560527736;
    $1010 = ((($hp1)) + 32|0);
    HEAPF64[$1010>>3] = -0.80373875180591614;
    $1011 = ((($lp2)) + 32|0);
    HEAPF64[$1011>>3] = 0.80373875180591614;
    $1012 = ((($hp2)) + 32|0);
    HEAPF64[$1012>>3] = 0.29785779560527736;
    $1013 = ((($lp1)) + 40|0);
    HEAPF64[$1013>>3] = -0.099219543576847216;
    $1014 = ((($hp1)) + 40|0);
    HEAPF64[$1014>>3] = 0.49761866763201545;
    $1015 = ((($lp2)) + 40|0);
    HEAPF64[$1015>>3] = 0.49761866763201545;
    $1016 = ((($hp2)) + 40|0);
    HEAPF64[$1016>>3] = 0.099219543576847216;
    $1017 = ((($lp1)) + 48|0);
    HEAPF64[$1017>>3] = -0.012603967262037833;
    $1018 = ((($hp1)) + 48|0);
    HEAPF64[$1018>>3] = 0.02963552764599851;
    $1019 = ((($lp2)) + 48|0);
    HEAPF64[$1019>>3] = -0.02963552764599851;
    $1020 = ((($hp2)) + 48|0);
    HEAPF64[$1020>>3] = -0.012603967262037833;
    $1021 = ((($lp1)) + 56|0);
    HEAPF64[$1021>>3] = 0.032223100604042702;
    $1022 = ((($hp1)) + 56|0);
    HEAPF64[$1022>>3] = -0.075765714789273325;
    $1023 = ((($lp2)) + 56|0);
    HEAPF64[$1023>>3] = -0.075765714789273325;
    $1024 = ((($hp2)) + 56|0);
    HEAPF64[$1024>>3] = -0.032223100604042702;
    $$0 = 8;
    STACKTOP = sp;return ($$0|0);
   }
   $1025 = (_strcmp($name,320)|0);
   $1026 = ($1025|0)==(0);
   if ($1026) {
    $i$3899 = 0;
    while(1) {
     $1027 = (12976 + ($i$3899<<3)|0);
     $1028 = +HEAPF64[$1027>>3];
     $1029 = (($lp1) + ($i$3899<<3)|0);
     HEAPF64[$1029>>3] = $1028;
     $1030 = (13056 + ($i$3899<<3)|0);
     $1031 = +HEAPF64[$1030>>3];
     $1032 = (($hp1) + ($i$3899<<3)|0);
     HEAPF64[$1032>>3] = $1031;
     $1033 = (13136 + ($i$3899<<3)|0);
     $1034 = +HEAPF64[$1033>>3];
     $1035 = (($lp2) + ($i$3899<<3)|0);
     HEAPF64[$1035>>3] = $1034;
     $1036 = (13216 + ($i$3899<<3)|0);
     $1037 = +HEAPF64[$1036>>3];
     $1038 = (($hp2) + ($i$3899<<3)|0);
     HEAPF64[$1038>>3] = $1037;
     $1039 = (($i$3899) + 1)|0;
     $exitcond297 = ($1039|0)==(10);
     if ($exitcond297) {
      $$0 = 10;
      break;
     } else {
      $i$3899 = $1039;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $1040 = (_strcmp($name,328)|0);
   $1041 = ($1040|0)==(0);
   if ($1041) {
    $i$39100 = 0;
    while(1) {
     $1042 = (13296 + ($i$39100<<3)|0);
     $1043 = +HEAPF64[$1042>>3];
     $1044 = (($lp1) + ($i$39100<<3)|0);
     HEAPF64[$1044>>3] = $1043;
     $1045 = (13392 + ($i$39100<<3)|0);
     $1046 = +HEAPF64[$1045>>3];
     $1047 = (($hp1) + ($i$39100<<3)|0);
     HEAPF64[$1047>>3] = $1046;
     $1048 = (13488 + ($i$39100<<3)|0);
     $1049 = +HEAPF64[$1048>>3];
     $1050 = (($lp2) + ($i$39100<<3)|0);
     HEAPF64[$1050>>3] = $1049;
     $1051 = (13584 + ($i$39100<<3)|0);
     $1052 = +HEAPF64[$1051>>3];
     $1053 = (($hp2) + ($i$39100<<3)|0);
     HEAPF64[$1053>>3] = $1052;
     $1054 = (($i$39100) + 1)|0;
     $exitcond302 = ($1054|0)==(12);
     if ($exitcond302) {
      $$0 = 12;
      break;
     } else {
      $i$39100 = $1054;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $1055 = (_strcmp($name,336)|0);
   $1056 = ($1055|0)==(0);
   if ($1056) {
    $i$40101 = 0;
    while(1) {
     $1057 = (13680 + ($i$40101<<3)|0);
     $1058 = +HEAPF64[$1057>>3];
     $1059 = (($lp1) + ($i$40101<<3)|0);
     HEAPF64[$1059>>3] = $1058;
     $1060 = (13792 + ($i$40101<<3)|0);
     $1061 = +HEAPF64[$1060>>3];
     $1062 = (($hp1) + ($i$40101<<3)|0);
     HEAPF64[$1062>>3] = $1061;
     $1063 = (13904 + ($i$40101<<3)|0);
     $1064 = +HEAPF64[$1063>>3];
     $1065 = (($lp2) + ($i$40101<<3)|0);
     HEAPF64[$1065>>3] = $1064;
     $1066 = (14016 + ($i$40101<<3)|0);
     $1067 = +HEAPF64[$1066>>3];
     $1068 = (($hp2) + ($i$40101<<3)|0);
     HEAPF64[$1068>>3] = $1067;
     $1069 = (($i$40101) + 1)|0;
     $exitcond307 = ($1069|0)==(14);
     if ($exitcond307) {
      $$0 = 14;
      break;
     } else {
      $i$40101 = $1069;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $1070 = (_strcmp($name,344)|0);
   $1071 = ($1070|0)==(0);
   if ($1071) {
    $i$41102 = 0;
    while(1) {
     $1072 = (14128 + ($i$41102<<3)|0);
     $1073 = +HEAPF64[$1072>>3];
     $1074 = (($lp1) + ($i$41102<<3)|0);
     HEAPF64[$1074>>3] = $1073;
     $1075 = (14256 + ($i$41102<<3)|0);
     $1076 = +HEAPF64[$1075>>3];
     $1077 = (($hp1) + ($i$41102<<3)|0);
     HEAPF64[$1077>>3] = $1076;
     $1078 = (14384 + ($i$41102<<3)|0);
     $1079 = +HEAPF64[$1078>>3];
     $1080 = (($lp2) + ($i$41102<<3)|0);
     HEAPF64[$1080>>3] = $1079;
     $1081 = (14512 + ($i$41102<<3)|0);
     $1082 = +HEAPF64[$1081>>3];
     $1083 = (($hp2) + ($i$41102<<3)|0);
     HEAPF64[$1083>>3] = $1082;
     $1084 = (($i$41102) + 1)|0;
     $exitcond312 = ($1084|0)==(16);
     if ($exitcond312) {
      $$0 = 16;
      break;
     } else {
      $i$41102 = $1084;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $1085 = (_strcmp($name,352)|0);
   $1086 = ($1085|0)==(0);
   if ($1086) {
    $i$42103 = 0;
    while(1) {
     $1087 = (14640 + ($i$42103<<3)|0);
     $1088 = +HEAPF64[$1087>>3];
     $1089 = (($lp1) + ($i$42103<<3)|0);
     HEAPF64[$1089>>3] = $1088;
     $1090 = (14784 + ($i$42103<<3)|0);
     $1091 = +HEAPF64[$1090>>3];
     $1092 = (($hp1) + ($i$42103<<3)|0);
     HEAPF64[$1092>>3] = $1091;
     $1093 = (14928 + ($i$42103<<3)|0);
     $1094 = +HEAPF64[$1093>>3];
     $1095 = (($lp2) + ($i$42103<<3)|0);
     HEAPF64[$1095>>3] = $1094;
     $1096 = (15072 + ($i$42103<<3)|0);
     $1097 = +HEAPF64[$1096>>3];
     $1098 = (($hp2) + ($i$42103<<3)|0);
     HEAPF64[$1098>>3] = $1097;
     $1099 = (($i$42103) + 1)|0;
     $exitcond317 = ($1099|0)==(18);
     if ($exitcond317) {
      $$0 = 18;
      break;
     } else {
      $i$42103 = $1099;
     }
    }
    STACKTOP = sp;return ($$0|0);
   }
   $1100 = (_strcmp($name,360)|0);
   $1101 = ($1100|0)==(0);
   if ($1101) {
    $i$43104 = 0;
    while(1) {
     $1102 = (15216 + ($i$43104<<3)|0);
     $1103 = +HEAPF64[$1102>>3];
     $1104 = (($lp1) + ($i$43104<<3)|0);
     HEAPF64[$1104>>3] = $1103;
     $1105 = (15376 + ($i$43104<<3)|0);
     $1106 = +HEAPF64[$1105>>3];
     $1107 = (($hp1) + ($i$43104<<3)|0);
     HEAPF64[$1107>>3] = $1106;
     $1108 = (15536 + ($i$43104<<3)|0);
     $1109 = +HEAPF64[$1108>>3];
     $1110 = (($lp2) + ($i$43104<<3)|0);
     HEAPF64[$1110>>3] = $1109;
     $1111 = (15696 + ($i$43104<<3)|0);
     $1112 = +HEAPF64[$1111>>3];
     $1113 = (($hp2) + ($i$43104<<3)|0);
     HEAPF64[$1113>>3] = $1112;
     $1114 = (($i$43104) + 1)|0;
     $exitcond322 = ($1114|0)==(20);
     if ($exitcond322) {
      $$0 = 20;
      break;
     } else {
      $i$43104 = $1114;
     }
    }
    STACKTOP = sp;return ($$0|0);
   } else {
    (_puts((15856|0))|0);
    $$0 = -1;
    STACKTOP = sp;return ($$0|0);
   }
  }
 }
 HEAPF64[$lp1>>3] = 0.70709999999999995;
 HEAPF64[$hp1>>3] = -0.70709999999999995;
 HEAPF64[$lp2>>3] = 0.70709999999999995;
 HEAPF64[$hp2>>3] = 0.70709999999999995;
 $4 = ((($lp1)) + 8|0);
 HEAPF64[$4>>3] = 0.70709999999999995;
 $5 = ((($hp1)) + 8|0);
 HEAPF64[$5>>3] = 0.70709999999999995;
 $6 = ((($lp2)) + 8|0);
 HEAPF64[$6>>3] = 0.70709999999999995;
 $7 = ((($hp2)) + 8|0);
 HEAPF64[$7>>3] = -0.70709999999999995;
 $$0 = 2;
 STACKTOP = sp;return ($$0|0);
}
function _upsamp($x,$lenx,$M,$y) {
 $x = $x|0;
 $lenx = $lenx|0;
 $M = $M|0;
 $y = $y|0;
 var $$0 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0.0, $16 = 0, $17 = 0, $18 = 0, $2 = 0, $3 = 0, $4 = 0.0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 var $exitcond = 0, $i$02 = 0, $i$13 = 0, $j$04 = 0, $j$1 = 0, $k$05 = 0, $k$1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($M|0)<(0);
 if ($0) {
  $$0 = -1;
  return ($$0|0);
 }
 $1 = ($M|0)==(0);
 if ($1) {
  $2 = ($lenx|0)>(0);
  if ($2) {
   $i$02 = 0;
  } else {
   $$0 = $lenx;
   return ($$0|0);
  }
  while(1) {
   $3 = (($x) + ($i$02<<3)|0);
   $4 = +HEAPF64[$3>>3];
   $5 = (($y) + ($i$02<<3)|0);
   HEAPF64[$5>>3] = $4;
   $6 = (($i$02) + 1)|0;
   $exitcond = ($6|0)==($lenx|0);
   if ($exitcond) {
    $$0 = $lenx;
    break;
   } else {
    $i$02 = $6;
   }
  }
  return ($$0|0);
 }
 $7 = (($lenx) + -1)|0;
 $8 = Math_imul($7, $M)|0;
 $9 = (($8) + 1)|0;
 $10 = ($8|0)>(-1);
 if ($10) {
  $i$13 = 0;$j$04 = 1;$k$05 = 0;
 } else {
  $$0 = $9;
  return ($$0|0);
 }
 while(1) {
  $11 = (($j$04) + -1)|0;
  $12 = (($y) + ($i$13<<3)|0);
  HEAPF64[$12>>3] = 0.0;
  $13 = ($11|0)==(0);
  if ($13) {
   $14 = (($x) + ($k$05<<3)|0);
   $15 = +HEAPF64[$14>>3];
   HEAPF64[$12>>3] = $15;
   $16 = (($k$05) + 1)|0;
   $j$1 = $M;$k$1 = $16;
  } else {
   $j$1 = $11;$k$1 = $k$05;
  }
  $17 = (($i$13) + 1)|0;
  $18 = ($17|0)<($9|0);
  if ($18) {
   $i$13 = $17;$j$04 = $j$1;$k$05 = $k$1;
  } else {
   $$0 = $9;
   break;
  }
 }
 return ($$0|0);
}
function _upsamp2($x,$lenx,$M,$y) {
 $x = $x|0;
 $lenx = $lenx|0;
 $M = $M|0;
 $y = $y|0;
 var $$0 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0.0, $15 = 0, $16 = 0, $2 = 0, $3 = 0, $4 = 0.0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $exitcond = 0, $exitcond8 = 0;
 var $i$02 = 0, $i$13 = 0, $j$04 = 0, $j$1 = 0, $k$05 = 0, $k$1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($M|0)<(0);
 if ($0) {
  $$0 = -1;
  return ($$0|0);
 }
 $1 = ($M|0)==(0);
 if ($1) {
  $2 = ($lenx|0)>(0);
  if ($2) {
   $i$02 = 0;
  } else {
   $$0 = $lenx;
   return ($$0|0);
  }
  while(1) {
   $3 = (($x) + ($i$02<<3)|0);
   $4 = +HEAPF64[$3>>3];
   $5 = (($y) + ($i$02<<3)|0);
   HEAPF64[$5>>3] = $4;
   $6 = (($i$02) + 1)|0;
   $exitcond = ($6|0)==($lenx|0);
   if ($exitcond) {
    $$0 = $lenx;
    break;
   } else {
    $i$02 = $6;
   }
  }
  return ($$0|0);
 }
 $7 = Math_imul($M, $lenx)|0;
 $8 = ($7|0)>(0);
 if (!($8)) {
  $$0 = $7;
  return ($$0|0);
 }
 $9 = Math_imul($M, $lenx)|0;
 $i$13 = 0;$j$04 = 1;$k$05 = 0;
 while(1) {
  $10 = (($j$04) + -1)|0;
  $11 = (($y) + ($i$13<<3)|0);
  HEAPF64[$11>>3] = 0.0;
  $12 = ($10|0)==(0);
  if ($12) {
   $13 = (($x) + ($k$05<<3)|0);
   $14 = +HEAPF64[$13>>3];
   HEAPF64[$11>>3] = $14;
   $15 = (($k$05) + 1)|0;
   $j$1 = $M;$k$1 = $15;
  } else {
   $j$1 = $10;$k$1 = $k$05;
  }
  $16 = (($i$13) + 1)|0;
  $exitcond8 = ($16|0)==($9|0);
  if ($exitcond8) {
   $$0 = $7;
   break;
  } else {
   $i$13 = $16;$j$04 = $j$1;$k$05 = $k$1;
  }
 }
 return ($$0|0);
}
function _downsamp($x,$lenx,$M,$y) {
 $x = $x|0;
 $lenx = $lenx|0;
 $M = $M|0;
 $y = $y|0;
 var $$0 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0.0, $14 = 0, $15 = 0, $16 = 0, $2 = 0, $3 = 0, $4 = 0.0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $exitcond = 0, $i$02 = 0;
 var $i$13 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($M|0)<(0);
 if ($0) {
  $$0 = -1;
  return ($$0|0);
 }
 $1 = ($M|0)==(0);
 if ($1) {
  $2 = ($lenx|0)>(0);
  if ($2) {
   $i$02 = 0;
  } else {
   $$0 = $lenx;
   return ($$0|0);
  }
  while(1) {
   $3 = (($x) + ($i$02<<3)|0);
   $4 = +HEAPF64[$3>>3];
   $5 = (($y) + ($i$02<<3)|0);
   HEAPF64[$5>>3] = $4;
   $6 = (($i$02) + 1)|0;
   $exitcond = ($6|0)==($lenx|0);
   if ($exitcond) {
    $$0 = $lenx;
    break;
   } else {
    $i$02 = $6;
   }
  }
  return ($$0|0);
 } else {
  $7 = (($lenx) + -1)|0;
  $8 = (($7|0) / ($M|0))&-1;
  $9 = (($8) + 1)|0;
  $10 = ($8|0)>(-1);
  if ($10) {
   $i$13 = 0;
  } else {
   $$0 = $9;
   return ($$0|0);
  }
  while(1) {
   $11 = Math_imul($i$13, $M)|0;
   $12 = (($x) + ($11<<3)|0);
   $13 = +HEAPF64[$12>>3];
   $14 = (($y) + ($i$13<<3)|0);
   HEAPF64[$14>>3] = $13;
   $15 = (($i$13) + 1)|0;
   $16 = ($15|0)<($9|0);
   if ($16) {
    $i$13 = $15;
   } else {
    $$0 = $9;
    break;
   }
  }
  return ($$0|0);
 }
 return (0)|0;
}
function _per_ext($sig,$len,$a,$oup) {
 $sig = $sig|0;
 $len = $len|0;
 $a = $a|0;
 $oup = $oup|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0.0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0.0, $20 = 0.0, $21 = 0, $22 = 0, $23 = 0.0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $exitcond = 0, $exitcond8 = 0, $i$03 = 0, $i$1$neg = 0, $i$1$neg2 = 0, $i$11 = 0, $len2$0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($len|0)>(0);
 if ($0) {
  $i$03 = 0;
  while(1) {
   $1 = (($sig) + ($i$03<<3)|0);
   $2 = +HEAPF64[$1>>3];
   $3 = (($i$03) + ($a))|0;
   $4 = (($oup) + ($3<<3)|0);
   HEAPF64[$4>>3] = $2;
   $5 = (($i$03) + 1)|0;
   $exitcond8 = ($5|0)==($len|0);
   if ($exitcond8) {
    break;
   } else {
    $i$03 = $5;
   }
  }
 }
 $6 = $len & 1;
 $7 = ($6|0)==(0);
 if ($7) {
  $len2$0 = $len;
 } else {
  $8 = (($len) + 1)|0;
  $9 = (($len) + -1)|0;
  $10 = (($sig) + ($9<<3)|0);
  $11 = +HEAPF64[$10>>3];
  $12 = (($a) + ($len))|0;
  $13 = (($oup) + ($12<<3)|0);
  HEAPF64[$13>>3] = $11;
  $len2$0 = $8;
 }
 $14 = ($a|0)>(0);
 if (!($14)) {
  return ($len2$0|0);
 }
 $15 = (($len2$0) + ($a))|0;
 $16 = (($15) + -1)|0;
 $17 = (($a) + -1)|0;
 $i$1$neg2 = 0;$i$11 = 0;
 while(1) {
  $18 = (($i$11) + ($a))|0;
  $19 = (($oup) + ($18<<3)|0);
  $20 = +HEAPF64[$19>>3];
  $21 = (($16) + ($i$1$neg2))|0;
  $22 = (($oup) + ($21<<3)|0);
  $23 = +HEAPF64[$22>>3];
  $24 = (($17) + ($i$1$neg2))|0;
  $25 = (($oup) + ($24<<3)|0);
  HEAPF64[$25>>3] = $23;
  $26 = (($i$11) + ($15))|0;
  $27 = (($oup) + ($26<<3)|0);
  HEAPF64[$27>>3] = $20;
  $28 = (($i$11) + 1)|0;
  $i$1$neg = $i$11 ^ -1;
  $exitcond = ($28|0)==($a|0);
  if ($exitcond) {
   break;
  } else {
   $i$1$neg2 = $i$1$neg;$i$11 = $28;
  }
 }
 return ($len2$0|0);
}
function _symm_ext($sig,$len,$a,$oup) {
 $sig = $sig|0;
 $len = $len|0;
 $a = $a|0;
 $oup = $oup|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0.0, $13 = 0, $14 = 0, $15 = 0.0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0.0, $7 = 0, $8 = 0;
 var $9 = 0, $exitcond = 0, $exitcond6 = 0, $i$03 = 0, $i$1$neg = 0, $i$1$neg2 = 0, $i$11 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($len|0)>(0);
 if ($0) {
  $i$03 = 0;
  while(1) {
   $5 = (($sig) + ($i$03<<3)|0);
   $6 = +HEAPF64[$5>>3];
   $7 = (($i$03) + ($a))|0;
   $8 = (($oup) + ($7<<3)|0);
   HEAPF64[$8>>3] = $6;
   $9 = (($i$03) + 1)|0;
   $exitcond6 = ($9|0)==($len|0);
   if ($exitcond6) {
    break;
   } else {
    $i$03 = $9;
   }
  }
 }
 $1 = ($a|0)>(0);
 if (!($1)) {
  return ($len|0);
 }
 $2 = (($a) + ($len))|0;
 $3 = (($2) + -1)|0;
 $4 = (($a) + -1)|0;
 $i$1$neg2 = 0;$i$11 = 0;
 while(1) {
  $10 = (($i$11) + ($a))|0;
  $11 = (($oup) + ($10<<3)|0);
  $12 = +HEAPF64[$11>>3];
  $13 = (($3) + ($i$1$neg2))|0;
  $14 = (($oup) + ($13<<3)|0);
  $15 = +HEAPF64[$14>>3];
  $16 = (($4) + ($i$1$neg2))|0;
  $17 = (($oup) + ($16<<3)|0);
  HEAPF64[$17>>3] = $12;
  $18 = (($i$11) + ($2))|0;
  $19 = (($oup) + ($18<<3)|0);
  HEAPF64[$19>>3] = $15;
  $20 = (($i$11) + 1)|0;
  $i$1$neg = $i$11 ^ -1;
  $exitcond = ($20|0)==($a|0);
  if ($exitcond) {
   break;
  } else {
   $i$1$neg2 = $i$1$neg;$i$11 = $20;
  }
 }
 return ($len|0);
}
function _circshift($array,$N,$L) {
 $array = $array|0;
 $N = $N|0;
 $L = $L|0;
 var $$0 = 0, $$1 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0.0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0;
 var $7 = 0, $8 = 0, $9 = 0, $exitcond = 0, $i$13 = 0, $scevgep = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (_iabs($L)|0);
 $1 = ($0|0)>($N|0);
 if ($1) {
  $2 = (_isign($L)|0);
  $3 = (($0|0) % ($N|0))&-1;
  $4 = Math_imul($3, $2)|0;
  $$0 = $4;
 } else {
  $$0 = $L;
 }
 $5 = ($$0|0)<(0);
 if ($5) {
  $6 = (($$0) + ($N))|0;
  $7 = (($6|0) % ($N|0))&-1;
  $$1 = $7;
 } else {
  $$1 = $$0;
 }
 $8 = $$1 << 3;
 $9 = (_malloc($8)|0);
 $10 = ($$1|0)>(0);
 if ($10) {
  _memcpy(($9|0),($array|0),($8|0))|0;
 }
 $11 = ($$1|0)<($N|0);
 if ($11) {
  $12 = (($N) - ($$1))|0;
  $i$13 = 0;
  while(1) {
   $14 = (($i$13) + ($$1))|0;
   $15 = (($array) + ($14<<3)|0);
   $16 = +HEAPF64[$15>>3];
   $17 = (($array) + ($i$13<<3)|0);
   HEAPF64[$17>>3] = $16;
   $18 = (($i$13) + 1)|0;
   $exitcond = ($18|0)==($12|0);
   if ($exitcond) {
    break;
   } else {
    $i$13 = $18;
   }
  }
 }
 $13 = ($$1|0)>(0);
 if (!($13)) {
  _free($9);
  return;
 }
 $19 = (($N) - ($$1))|0;
 $scevgep = (($array) + ($19<<3)|0);
 $20 = $$1 << 3;
 _memcpy(($scevgep|0),($9|0),($20|0))|0;
 _free($9);
 return;
}
function _testSWTlength($N,$J) {
 $N = $N|0;
 $J = $J|0;
 var $$ = 0, $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $div$0$lcssa = 0, $div$01 = 0, $exitcond = 0, $i$02 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($J|0)>(0);
 if ($0) {
  $div$01 = 1;$i$02 = 0;
  while(1) {
   $1 = $div$01 << 1;
   $2 = (($i$02) + 1)|0;
   $exitcond = ($2|0)==($J|0);
   if ($exitcond) {
    $div$0$lcssa = $1;
    break;
   } else {
    $div$01 = $1;$i$02 = $2;
   }
  }
 } else {
  $div$0$lcssa = 1;
 }
 $3 = (($N|0) % ($div$0$lcssa|0))&-1;
 $4 = ($3|0)==(0);
 $$ = $4&1;
 return ($$|0);
}
function _wmaxiter($sig_len,$filt_len) {
 $sig_len = $sig_len|0;
 $filt_len = $filt_len|0;
 var $0 = 0.0, $1 = 0.0, $2 = 0.0, $3 = 0.0, $4 = 0.0, $5 = 0.0, $6 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (+($sig_len|0));
 $1 = (+($filt_len|0));
 $2 = $1 + -1.0;
 $3 = $0 / $2;
 $4 = (+Math_log((+$3)));
 $5 = $4 / 0.69314718055994529;
 $6 = (~~(($5)));
 return ($6|0);
}
function _iabs($N) {
 $N = $N|0;
 var $$0 = 0, $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($N|0)>(-1);
 $1 = (0 - ($N))|0;
 $$0 = $0 ? $N : $1;
 return ($$0|0);
}
function _isign($N) {
 $N = $N|0;
 var $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = $N >> 31;
 $1 = $0 | 1;
 return ($1|0);
}
function _wave_init($wname) {
 $wname = $wname|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $3 = 0;
 var $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $retval$0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($wname|0)!=(0|0);
 if ($0) {
  $1 = (_filtlength($wname)|0);
  $retval$0 = $1;
 } else {
  $retval$0 = 0;
 }
 $2 = $retval$0 << 5;
 $3 = (($2) + 88)|0;
 $4 = (_malloc($3)|0);
 $5 = ((($4)) + 52|0);
 HEAP32[$5>>2] = $retval$0;
 $6 = ((($4)) + 68|0);
 HEAP32[$6>>2] = $retval$0;
 $7 = ((($4)) + 64|0);
 HEAP32[$7>>2] = $retval$0;
 $8 = ((($4)) + 60|0);
 HEAP32[$8>>2] = $retval$0;
 $9 = ((($4)) + 56|0);
 HEAP32[$9>>2] = $retval$0;
 (_strcpy(($4|0),($wname|0))|0);
 if ($0) {
  $10 = ((($4)) + 88|0);
  $11 = (($10) + ($retval$0<<3)|0);
  $12 = $retval$0 << 1;
  $13 = (($10) + ($12<<3)|0);
  $14 = ($retval$0*3)|0;
  $15 = (($10) + ($14<<3)|0);
  (_filtcoef($wname,$10,$11,$13,$15)|0);
 }
 $16 = ((($4)) + 88|0);
 $17 = ((($4)) + 72|0);
 HEAP32[$17>>2] = $16;
 $18 = (($16) + ($retval$0<<3)|0);
 $19 = ((($4)) + 76|0);
 HEAP32[$19>>2] = $18;
 $20 = $retval$0 << 1;
 $21 = (($16) + ($20<<3)|0);
 $22 = ((($4)) + 80|0);
 HEAP32[$22>>2] = $21;
 $23 = ($retval$0*3)|0;
 $24 = (($16) + ($23<<3)|0);
 $25 = ((($4)) + 84|0);
 HEAP32[$25>>2] = $24;
 return ($4|0);
}
function _wt_init($wave,$method,$siglength,$J) {
 $wave = $wave|0;
 $method = $method|0;
 $siglength = $siglength|0;
 $J = $J|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0;
 var $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0;
 var $obj$0 = 0, $scevgep = 0, $scevgep9 = 0, $vararg_buffer = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0;
 $vararg_buffer = sp;
 $0 = ((($wave)) + 52|0);
 $1 = HEAP32[$0>>2]|0;
 $2 = ($J|0)>(100);
 if ($2) {
  (_puts((15888|0))|0);
  _exit(-1);
  // unreachable;
 }
 $3 = (_wmaxiter($siglength,$1)|0);
 $4 = ($3|0)<($J|0);
 if ($4) {
  HEAP32[$vararg_buffer>>2] = $3;
  (_printf((15952|0),($vararg_buffer|0))|0);
  _exit(-1);
  // unreachable;
 }
 $5 = ($method|0)==(0|0);
 do {
  if ($5) {
   $6 = $J << 1;
   $7 = (($1) + 1)|0;
   $8 = Math_imul($6, $7)|0;
   $9 = (($8) + ($siglength))|0;
   $10 = $9 << 3;
   $11 = (($10) + 488)|0;
   $12 = (_malloc($11)|0);
   $13 = ((($12)) + 24|0);
   HEAP32[$13>>2] = $9;
   $14 = ((($12)) + 44|0);
   HEAP8[$14>>0]=7174515&255;HEAP8[$14+1>>0]=(7174515>>8)&255;HEAP8[$14+2>>0]=(7174515>>16)&255;HEAP8[$14+3>>0]=7174515>>24;
   $obj$0 = $12;
  } else {
   $15 = (_strcmp($method,16416)|0);
   $16 = ($15|0)==(0);
   if (!($16)) {
    $17 = (_strcmp($method,16424)|0);
    $18 = ($17|0)==(0);
    if (!($18)) {
     $28 = (_strcmp($method,16448)|0);
     $29 = ($28|0)==(0);
     if (!($29)) {
      $30 = (_strcmp($method,16456)|0);
      $31 = ($30|0)==(0);
      if (!($31)) {
       $41 = (_strcmp($method,16464)|0);
       $42 = ($41|0)==(0);
       if (!($42)) {
        $43 = (_strcmp($method,16472)|0);
        $44 = ($43|0)==(0);
        if (!($44)) {
         $obj$0 = 0;
         break;
        }
       }
       $45 = (_strstr($wave,16088)|0);
       $46 = ($45|0)==(0|0);
       if ($46) {
        $47 = (_strstr($wave,16440)|0);
        $48 = ($47|0)==(0|0);
        if ($48) {
         $49 = (_strstr($wave,16096)|0);
         $50 = ($49|0)==(0|0);
         if ($50) {
          (_puts((16104|0))|0);
          _exit(-1);
          // unreachable;
         }
        }
       }
       $51 = (($J) + 1)|0;
       $52 = Math_imul($51, $siglength)|0;
       $53 = $52 << 3;
       $54 = (($53) + 488)|0;
       $55 = (_malloc($54)|0);
       $56 = ((($55)) + 24|0);
       HEAP32[$56>>2] = $52;
       $57 = ((($55)) + 44|0);
       HEAP8[$57>>0]=7497072&255;HEAP8[$57+1>>0]=(7497072>>8)&255;HEAP8[$57+2>>0]=(7497072>>16)&255;HEAP8[$57+3>>0]=7497072>>24;
       $obj$0 = $55;
       break;
      }
     }
     $32 = (_testSWTlength($siglength,$J)|0);
     $33 = ($32|0)==(0);
     if ($33) {
      (_puts((16032|0))|0);
      _exit(-1);
      // unreachable;
     } else {
      $34 = (($J) + 1)|0;
      $35 = Math_imul($34, $siglength)|0;
      $36 = $35 << 3;
      $37 = (($36) + 488)|0;
      $38 = (_malloc($37)|0);
      $39 = ((($38)) + 24|0);
      HEAP32[$39>>2] = $35;
      $40 = ((($38)) + 44|0);
      HEAP8[$40>>0]=7497072&255;HEAP8[$40+1>>0]=(7497072>>8)&255;HEAP8[$40+2>>0]=(7497072>>16)&255;HEAP8[$40+3>>0]=7497072>>24;
      $obj$0 = $38;
      break;
     }
    }
   }
   $19 = $J << 1;
   $20 = (($1) + 1)|0;
   $21 = Math_imul($19, $20)|0;
   $22 = (($21) + ($siglength))|0;
   $23 = $22 << 3;
   $24 = (($23) + 488)|0;
   $25 = (_malloc($24)|0);
   $26 = ((($25)) + 24|0);
   HEAP32[$26>>2] = $22;
   $27 = ((($25)) + 44|0);
   HEAP8[$27>>0]=7174515&255;HEAP8[$27+1>>0]=(7174515>>8)&255;HEAP8[$27+2>>0]=(7174515>>16)&255;HEAP8[$27+3>>0]=7174515>>24;
   $obj$0 = $25;
  }
 } while(0);
 HEAP32[$obj$0>>2] = $wave;
 $58 = ((($obj$0)) + 20|0);
 HEAP32[$58>>2] = $siglength;
 $59 = ((($obj$0)) + 32|0);
 HEAP32[$59>>2] = $J;
 $60 = ((($obj$0)) + 36|0);
 HEAP32[$60>>2] = $3;
 $61 = ((($obj$0)) + 8|0);
 (_strcpy(($61|0),($method|0))|0);
 $62 = $siglength & 1;
 $63 = ((($obj$0)) + 40|0);
 $64 = $62 ^ 1;
 HEAP32[$63>>2] = $64;
 $65 = ((($obj$0)) + 4|0);
 HEAP32[$65>>2] = 0;
 $66 = ((($obj$0)) + 54|0);
 ;HEAP8[$66>>0]=HEAP8[16184>>0]|0;HEAP8[$66+1>>0]=HEAP8[16184+1>>0]|0;HEAP8[$66+2>>0]=HEAP8[16184+2>>0]|0;HEAP8[$66+3>>0]=HEAP8[16184+3>>0]|0;HEAP8[$66+4>>0]=HEAP8[16184+4>>0]|0;HEAP8[$66+5>>0]=HEAP8[16184+5>>0]|0;HEAP8[$66+6>>0]=HEAP8[16184+6>>0]|0;
 $67 = ((($obj$0)) + 68|0);
 HEAP32[$67>>2] = 0;
 $68 = (($J) + 2)|0;
 $69 = ((($obj$0)) + 28|0);
 HEAP32[$69>>2] = $68;
 $70 = ((($obj$0)) + 488|0);
 $71 = ((($obj$0)) + 484|0);
 HEAP32[$71>>2] = $70;
 $72 = (_strcmp($method,16416)|0);
 $73 = ($72|0)==(0);
 if (!($73)) {
  $74 = (_strcmp($method,16424)|0);
  $75 = ($74|0)==(0);
  if (!($75)) {
   $81 = (_strcmp($method,16448)|0);
   $82 = ($81|0)==(0);
   if (!($82)) {
    $83 = (_strcmp($method,16456)|0);
    $84 = ($83|0)==(0);
    if (!($84)) {
     $85 = (_strcmp($method,16464)|0);
     $86 = ($85|0)==(0);
     if (!($86)) {
      $87 = (_strcmp($method,16472)|0);
      $88 = ($87|0)==(0);
      if (!($88)) {
       STACKTOP = sp;return ($obj$0|0);
      }
     }
    }
   }
   $89 = (($J) + 1)|0;
   $90 = Math_imul($89, $siglength)|0;
   $91 = ($90|0)>(0);
   if (!($91)) {
    STACKTOP = sp;return ($obj$0|0);
   }
   $97 = (($J) + 1)|0;
   $98 = Math_imul($97, $siglength)|0;
   $scevgep9 = ((($obj$0)) + 488|0);
   $99 = $98 << 3;
   _memset(($scevgep9|0),0,($99|0))|0;
   STACKTOP = sp;return ($obj$0|0);
  }
 }
 $76 = $J << 1;
 $77 = (($1) + 1)|0;
 $78 = Math_imul($76, $77)|0;
 $79 = (($78) + ($siglength))|0;
 $80 = ($79|0)>(0);
 if (!($80)) {
  STACKTOP = sp;return ($obj$0|0);
 }
 $92 = (($1) + 1)|0;
 $93 = Math_imul($92, $J)|0;
 $scevgep = ((($obj$0)) + 488|0);
 $94 = $93 << 4;
 $95 = $siglength << 3;
 $96 = (($94) + ($95))|0;
 _memset(($scevgep|0),0,($96|0))|0;
 STACKTOP = sp;return ($obj$0|0);
}
function _dwt($wt,$inp) {
 $wt = $wt|0;
 $inp = $inp|0;
 var $$op = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0;
 var $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0.0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0.0, $38 = 0.0;
 var $39 = 0.0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0;
 var $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0.0;
 var $75 = 0.0, $76 = 0.0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0;
 var $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $N$018 = 0, $N$116 = 0, $N$230 = 0, $N$327 = 0, $exitcond = 0, $exitcond43 = 0, $i$119 = 0, $i$431 = 0, $iter$015 = 0, $iter$126 = 0, $orig$0$in = 0, $orig2$0$in = 0, $scevgep = 0;
 var $scevgep41 = 0, $temp_len$0 = 0, $temp_len$114 = 0, $temp_len$225 = 0, $vararg_buffer = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0;
 $vararg_buffer = sp;
 $0 = ((($wt)) + 20|0);
 $1 = HEAP32[$0>>2]|0;
 $2 = ((($wt)) + 32|0);
 $3 = HEAP32[$2>>2]|0;
 $4 = (($3) + 1)|0;
 $5 = ((($wt)) + 76|0);
 $6 = (((($wt)) + 76|0) + ($4<<2)|0);
 HEAP32[$6>>2] = $1;
 $7 = ((($wt)) + 24|0);
 HEAP32[$7>>2] = 0;
 $8 = $1 & 1;
 $9 = ($8|0)==(0);
 $10 = ((($wt)) + 72|0);
 if ($9) {
  HEAP32[$10>>2] = 0;
  $11 = $1 << 3;
  $12 = (_malloc($11)|0);
  $13 = (_malloc($11)|0);
  $orig$0$in = $12;$orig2$0$in = $13;$temp_len$0 = $1;
 } else {
  HEAP32[$10>>2] = 1;
  $14 = (($1) + 1)|0;
  $15 = $14 << 3;
  $16 = (_malloc($15)|0);
  $17 = (_malloc($15)|0);
  $orig$0$in = $16;$orig2$0$in = $17;$temp_len$0 = $14;
 }
 $18 = HEAP32[$0>>2]|0;
 $19 = ($18|0)>(0);
 if ($19) {
  $20 = HEAP32[$0>>2]|0;
  $21 = ($20|0)>(1);
  $$op = $20 << 3;
  $22 = $21 ? $$op : 8;
  _memcpy(($orig$0$in|0),($inp|0),($22|0))|0;
 }
 $23 = HEAP32[$10>>2]|0;
 $24 = ($23|0)==(1);
 if ($24) {
  $25 = (($temp_len$0) + -2)|0;
  $26 = (($orig$0$in) + ($25<<3)|0);
  $27 = +HEAPF64[$26>>3];
  $28 = (($temp_len$0) + -1)|0;
  $29 = (($orig$0$in) + ($28<<3)|0);
  HEAPF64[$29>>3] = $27;
 }
 $30 = HEAP32[$wt>>2]|0;
 $31 = ((($30)) + 56|0);
 $32 = HEAP32[$31>>2]|0;
 $33 = ((($wt)) + 44|0);
 $34 = (_strcmp($33,16432)|0);
 $35 = ($34|0)==(0);
 if ($35) {
  $36 = ($3|0)>(0);
  if ($36) {
   $N$018 = $temp_len$0;$i$119 = $3;
   while(1) {
    $37 = (+($N$018|0));
    $38 = $37 * 0.5;
    $39 = (+Math_ceil((+$38)));
    $40 = (~~(($39)));
    $41 = (((($wt)) + 76|0) + ($i$119<<2)|0);
    HEAP32[$41>>2] = $40;
    $42 = HEAP32[$7>>2]|0;
    $43 = (($42) + ($40))|0;
    HEAP32[$7>>2] = $43;
    $44 = (($i$119) + -1)|0;
    $45 = ($i$119|0)>(1);
    if ($45) {
     $N$018 = $40;$i$119 = $44;
    } else {
     break;
    }
   }
  }
  $46 = ((($wt)) + 80|0);
  $47 = HEAP32[$46>>2]|0;
  HEAP32[$5>>2] = $47;
  $48 = HEAP32[$7>>2]|0;
  $49 = (($48) + ($47))|0;
  HEAP32[$7>>2] = $49;
  $50 = ($3|0)>(0);
  if (!($50)) {
   _free($orig$0$in);
   _free($orig2$0$in);
   STACKTOP = sp;return;
  }
  $51 = ((($wt)) + 54|0);
  $52 = (($3) + -1)|0;
  $scevgep = ((($wt)) + 488|0);
  $N$116 = $49;$iter$015 = 0;$temp_len$114 = $temp_len$0;
  while(1) {
   $53 = (($3) - ($iter$015))|0;
   $54 = (((($wt)) + 76|0) + ($53<<2)|0);
   $55 = HEAP32[$54>>2]|0;
   $56 = (($N$116) - ($55))|0;
   $57 = (_strcmp($51,16192)|0);
   $58 = ($57|0)==(0);
   if ($58) {
    label = 15;
   } else {
    $59 = (_strcmp($51,16200)|0);
    $60 = ($59|0)==(0);
    if ($60) {
     label = 15;
    } else {
     $62 = (((($wt)) + 488|0) + ($56<<3)|0);
     _dwt_per($wt,$orig$0$in,$temp_len$114,$orig2$0$in,$55,$62);
    }
   }
   if ((label|0) == 15) {
    label = 0;
    $61 = (((($wt)) + 488|0) + ($56<<3)|0);
    _dwt1($wt,$orig$0$in,$temp_len$114,$orig2$0$in,$61);
   }
   $63 = HEAP32[$54>>2]|0;
   $64 = ($iter$015|0)==($52|0);
   $65 = ($55|0)>(0);
   if ($64) {
    if ($65) {
     $66 = $55 << 3;
     _memcpy(($scevgep|0),($orig2$0$in|0),($66|0))|0;
    }
   } else {
    if ($65) {
     $67 = $55 << 3;
     _memcpy(($orig$0$in|0),($orig2$0$in|0),($67|0))|0;
    }
   }
   $68 = (($iter$015) + 1)|0;
   $exitcond = ($68|0)==($3|0);
   if ($exitcond) {
    break;
   } else {
    $N$116 = $56;$iter$015 = $68;$temp_len$114 = $63;
   }
  }
  _free($orig$0$in);
  _free($orig2$0$in);
  STACKTOP = sp;return;
 }
 $69 = (_strcmp($33,16440)|0);
 $70 = ($69|0)==(0);
 if (!($70)) {
  (_printf((16208|0),($vararg_buffer|0))|0);
  _exit(-1);
  // unreachable;
 }
 $71 = ($3|0)>(0);
 if ($71) {
  $72 = (($32) + -2)|0;
  $N$230 = $temp_len$0;$i$431 = $3;
  while(1) {
   $73 = (($72) + ($N$230))|0;
   $74 = (+($73|0));
   $75 = $74 * 0.5;
   $76 = (+Math_ceil((+$75)));
   $77 = (~~(($76)));
   $78 = (((($wt)) + 76|0) + ($i$431<<2)|0);
   HEAP32[$78>>2] = $77;
   $79 = HEAP32[$7>>2]|0;
   $80 = (($79) + ($77))|0;
   HEAP32[$7>>2] = $80;
   $81 = (($i$431) + -1)|0;
   $82 = ($i$431|0)>(1);
   if ($82) {
    $N$230 = $77;$i$431 = $81;
   } else {
    break;
   }
  }
 }
 $83 = ((($wt)) + 80|0);
 $84 = HEAP32[$83>>2]|0;
 HEAP32[$5>>2] = $84;
 $85 = HEAP32[$7>>2]|0;
 $86 = (($85) + ($84))|0;
 HEAP32[$7>>2] = $86;
 $87 = ($3|0)>(0);
 if (!($87)) {
  _free($orig$0$in);
  _free($orig2$0$in);
  STACKTOP = sp;return;
 }
 $88 = ((($wt)) + 54|0);
 $89 = (($3) + -1)|0;
 $scevgep41 = ((($wt)) + 488|0);
 $N$327 = $86;$iter$126 = 0;$temp_len$225 = $temp_len$0;
 while(1) {
  $90 = (($3) - ($iter$126))|0;
  $91 = (((($wt)) + 76|0) + ($90<<2)|0);
  $92 = HEAP32[$91>>2]|0;
  $93 = (($N$327) - ($92))|0;
  $94 = (_strcmp($88,16192)|0);
  $95 = ($94|0)==(0);
  if ($95) {
   label = 31;
  } else {
   $96 = (_strcmp($88,16200)|0);
   $97 = ($96|0)==(0);
   if ($97) {
    label = 31;
   } else {
    $99 = (((($wt)) + 488|0) + ($93<<3)|0);
    _dwt_sym($wt,$orig$0$in,$temp_len$225,$orig2$0$in,$92,$99);
   }
  }
  if ((label|0) == 31) {
   label = 0;
   $98 = (((($wt)) + 488|0) + ($93<<3)|0);
   _dwt1($wt,$orig$0$in,$temp_len$225,$orig2$0$in,$98);
  }
  $100 = HEAP32[$91>>2]|0;
  $101 = ($iter$126|0)==($89|0);
  $102 = ($92|0)>(0);
  if ($101) {
   if ($102) {
    $103 = $92 << 3;
    _memcpy(($scevgep41|0),($orig2$0$in|0),($103|0))|0;
   }
  } else {
   if ($102) {
    $104 = $92 << 3;
    _memcpy(($orig$0$in|0),($orig2$0$in|0),($104|0))|0;
   }
  }
  $105 = (($iter$126) + 1)|0;
  $exitcond43 = ($105|0)==($3|0);
  if ($exitcond43) {
   break;
  } else {
   $N$327 = $93;$iter$126 = $105;$temp_len$225 = $100;
  }
 }
 _free($orig$0$in);
 _free($orig2$0$in);
 STACKTOP = sp;return;
}
function _idwt($wt,$dwtop) {
 $wt = $wt|0;
 $dwtop = $dwtop|0;
 var $$op = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0;
 var $115 = 0, $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0;
 var $133 = 0.0, $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0;
 var $151 = 0, $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0;
 var $17 = 0, $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0.0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0;
 var $188 = 0, $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0;
 var $205 = 0, $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0.0, $219 = 0, $22 = 0, $220 = 0.0, $221 = 0.0, $222 = 0;
 var $223 = 0, $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0;
 var $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0;
 var $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0.0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0;
 var $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0;
 var $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0.0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0;
 var $98 = 0, $99 = 0, $det_len$010 = 0, $det_len$122 = 0, $det_len$234 = 0, $exitcond = 0, $exitcond53 = 0, $exitcond55 = 0, $exitcond56 = 0, $exitcond59 = 0, $exitcond60 = 0, $exitcond61 = 0, $exitcond62 = 0, $i$014 = 0, $i$111 = 0, $i$226 = 0, $i$323 = 0, $i$438 = 0, $i$535 = 0, $i$649 = 0;
 var $i$746 = 0, $iter$09 = 0, $iter$121 = 0, $iter$233 = 0, $iter$345 = 0, $k$241 = 0, $scevgep = 0, $scevgep58 = 0, $tmp = 0, $tmp1 = 0, $vararg_buffer = 0, $vararg_buffer1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0;
 $vararg_buffer1 = sp + 8|0;
 $vararg_buffer = sp;
 $0 = ((($wt)) + 32|0);
 $1 = HEAP32[$0>>2]|0;
 $2 = ((($wt)) + 76|0);
 $3 = HEAP32[$2>>2]|0;
 $4 = ((($wt)) + 20|0);
 $5 = HEAP32[$4>>2]|0;
 $6 = $5 << 3;
 $7 = (($6) + 8)|0;
 $8 = (_malloc($7)|0);
 $9 = ((($wt)) + 44|0);
 $10 = (_strcmp($9,16432)|0);
 $11 = ($10|0)==(0);
 do {
  if ($11) {
   $12 = ((($wt)) + 54|0);
   $13 = (_strcmp($12,16192)|0);
   $14 = ($13|0)==(0);
   if (!($14)) {
    $15 = (_strcmp($12,16200)|0);
    $16 = ($15|0)==(0);
    if (!($16)) {
     label = 12;
     break;
    }
   }
   $17 = HEAP32[$2>>2]|0;
   $18 = ((($wt)) + 80|0);
   $19 = HEAP32[$18>>2]|0;
   $20 = (((($wt)) + 76|0) + ($1<<2)|0);
   $21 = HEAP32[$20>>2]|0;
   $22 = $21 << 1;
   $23 = HEAP32[$wt>>2]|0;
   $24 = ((($23)) + 64|0);
   $25 = HEAP32[$24>>2]|0;
   $26 = ((($23)) + 68|0);
   $27 = HEAP32[$26>>2]|0;
   $28 = (($27) + ($25))|0;
   $29 = (($28|0) / 2)&-1;
   $30 = $21 << 4;
   $31 = (_malloc($30)|0);
   $32 = (($29) + ($22))|0;
   $33 = $32 << 3;
   $34 = (_malloc($33)|0);
   $35 = $29 << 1;
   $36 = (($35) + ($22))|0;
   $37 = $36 << 3;
   $38 = (($37) + -8)|0;
   $39 = (_malloc($38)|0);
   $40 = (_malloc($38)|0);
   $41 = ($17|0)>(0);
   if ($41) {
    $42 = ((($wt)) + 484|0);
    $43 = HEAP32[$42>>2]|0;
    $i$014 = 0;
    while(1) {
     $46 = (($43) + ($i$014<<3)|0);
     $47 = +HEAPF64[$46>>3];
     $48 = (($8) + ($i$014<<3)|0);
     HEAPF64[$48>>3] = $47;
     $49 = (($i$014) + 1)|0;
     $exitcond53 = ($49|0)==($17|0);
     if ($exitcond53) {
      break;
     } else {
      $i$014 = $49;
     }
    }
   }
   $44 = ($1|0)>(0);
   if ($44) {
    $45 = ((($wt)) + 484|0);
    $det_len$010 = $19;$i$111 = 0;$iter$09 = $17;
    while(1) {
     $50 = HEAP32[$45>>2]|0;
     $51 = (($50) + ($iter$09<<3)|0);
     _idwt1($wt,$34,$31,$8,$det_len$010,$51,$det_len$010,$39,$40,$8);
     $52 = (($det_len$010) + ($iter$09))|0;
     $53 = (($i$111) + 2)|0;
     $54 = (((($wt)) + 76|0) + ($53<<2)|0);
     $55 = HEAP32[$54>>2]|0;
     $56 = (($i$111) + 1)|0;
     $exitcond = ($56|0)==($1|0);
     if ($exitcond) {
      break;
     } else {
      $det_len$010 = $55;$i$111 = $56;$iter$09 = $52;
     }
    }
   }
   _free($31);
   _free($39);
   _free($40);
   _free($34);
  } else {
   label = 12;
  }
 } while(0);
 do {
  if ((label|0) == 12) {
   $59 = (_strcmp($9,16432)|0);
   $60 = ($59|0)==(0);
   if ($60) {
    $61 = ((($wt)) + 54|0);
    $62 = (_strcmp($61,16184)|0);
    $63 = ($62|0)==(0);
    if ($63) {
     $64 = HEAP32[$2>>2]|0;
     $65 = ((($wt)) + 80|0);
     $66 = HEAP32[$65>>2]|0;
     $67 = (((($wt)) + 76|0) + ($1<<2)|0);
     $68 = HEAP32[$67>>2]|0;
     $69 = HEAP32[$wt>>2]|0;
     $70 = ((($69)) + 64|0);
     $71 = HEAP32[$70>>2]|0;
     $72 = ((($69)) + 68|0);
     $73 = HEAP32[$72>>2]|0;
     $74 = (($73) + ($71))|0;
     $75 = (($74|0) / 2)&-1;
     $76 = (($75) + ($68))|0;
     $77 = $76 << 4;
     $78 = (($77) + -8)|0;
     $79 = (_malloc($78)|0);
     $80 = ($64|0)>(0);
     if ($80) {
      $81 = ((($wt)) + 484|0);
      $82 = HEAP32[$81>>2]|0;
      $i$226 = 0;
      while(1) {
       $88 = (($82) + ($i$226<<3)|0);
       $89 = +HEAPF64[$88>>3];
       $90 = (($8) + ($i$226<<3)|0);
       HEAPF64[$90>>3] = $89;
       $91 = (($i$226) + 1)|0;
       $exitcond56 = ($91|0)==($64|0);
       if ($exitcond56) {
        break;
       } else {
        $i$226 = $91;
       }
      }
     }
     $83 = ($1|0)>(0);
     if ($83) {
      $84 = ((($wt)) + 484|0);
      $85 = (($74|0) / 4)&-1;
      $86 = $85 << 3;
      $87 = (($86) + -8)|0;
      $scevgep = (($79) + ($87)|0);
      $det_len$122 = $66;$i$323 = 0;$iter$121 = $64;
      while(1) {
       $92 = HEAP32[$84>>2]|0;
       $93 = (($92) + ($iter$121<<3)|0);
       _idwt_per($wt,$8,$det_len$122,$93,$79);
       $94 = $det_len$122 << 1;
       $95 = (($94) + -1)|0;
       $96 = (($95) + ($85))|0;
       $97 = ($85|0)>($96|0);
       if (!($97)) {
        $98 = $det_len$122 << 4;
        _memcpy(($8|0),($scevgep|0),($98|0))|0;
       }
       $99 = (($det_len$122) + ($iter$121))|0;
       $100 = (($i$323) + 2)|0;
       $101 = (((($wt)) + 76|0) + ($100<<2)|0);
       $102 = HEAP32[$101>>2]|0;
       $103 = (($i$323) + 1)|0;
       $exitcond55 = ($103|0)==($1|0);
       if ($exitcond55) {
        break;
       } else {
        $det_len$122 = $102;$i$323 = $103;$iter$121 = $99;
       }
      }
     }
     _free($79);
     break;
    }
   }
   $104 = (_strcmp($9,16440)|0);
   $105 = ($104|0)==(0);
   if ($105) {
    $106 = ((($wt)) + 54|0);
    $107 = (_strcmp($106,16184)|0);
    $108 = ($107|0)==(0);
    if ($108) {
     $109 = HEAP32[$2>>2]|0;
     $110 = ((($wt)) + 80|0);
     $111 = HEAP32[$110>>2]|0;
     $112 = (((($wt)) + 76|0) + ($1<<2)|0);
     $113 = HEAP32[$112>>2]|0;
     $114 = HEAP32[$wt>>2]|0;
     $115 = ((($114)) + 64|0);
     $116 = HEAP32[$115>>2]|0;
     $117 = ((($114)) + 68|0);
     $118 = HEAP32[$117>>2]|0;
     $119 = (($118) + ($116))|0;
     $120 = (($119|0) / 2)&-1;
     $tmp = (($120) + ($113))|0;
     $tmp1 = $tmp << 4;
     $121 = (($tmp1) + -16)|0;
     $122 = (_malloc($121)|0);
     $123 = ($109|0)>(0);
     if ($123) {
      $124 = ((($wt)) + 484|0);
      $125 = HEAP32[$124>>2]|0;
      $i$438 = 0;
      while(1) {
       $132 = (($125) + ($i$438<<3)|0);
       $133 = +HEAPF64[$132>>3];
       $134 = (($8) + ($i$438<<3)|0);
       HEAPF64[$134>>3] = $133;
       $135 = (($i$438) + 1)|0;
       $exitcond60 = ($135|0)==($109|0);
       if ($exitcond60) {
        break;
       } else {
        $i$438 = $135;
       }
      }
     }
     $126 = ($1|0)>(0);
     if ($126) {
      $127 = ((($wt)) + 484|0);
      $128 = (($120) + -2)|0;
      $129 = $120 << 3;
      $130 = (($129) + -16)|0;
      $scevgep58 = (($122) + ($130)|0);
      $131 = (2 - ($120))|0;
      $det_len$234 = $111;$i$535 = 0;$iter$233 = $109;
      while(1) {
       $136 = HEAP32[$127>>2]|0;
       $137 = (($136) + ($iter$233<<3)|0);
       _idwt_sym($wt,$8,$det_len$234,$137,$122);
       $138 = $det_len$234 << 1;
       $139 = ($128|0)<($138|0);
       if ($139) {
        $140 = $det_len$234 << 1;
        $141 = (($131) + ($140))|0;
        $142 = $141 << 3;
        _memcpy(($8|0),($scevgep58|0),($142|0))|0;
       }
       $143 = (($det_len$234) + ($iter$233))|0;
       $144 = (($i$535) + 2)|0;
       $145 = (((($wt)) + 76|0) + ($144<<2)|0);
       $146 = HEAP32[$145>>2]|0;
       $147 = (($i$535) + 1)|0;
       $exitcond59 = ($147|0)==($1|0);
       if ($exitcond59) {
        break;
       } else {
        $det_len$234 = $146;$i$535 = $147;$iter$233 = $143;
       }
      }
     }
     _free($122);
     break;
    }
   }
   $148 = (_strcmp($9,16440)|0);
   $149 = ($148|0)==(0);
   if (!($149)) {
    (_printf((16208|0),($vararg_buffer1|0))|0);
    _exit(-1);
    // unreachable;
   }
   $150 = ((($wt)) + 54|0);
   $151 = (_strcmp($150,16192)|0);
   $152 = ($151|0)==(0);
   if (!($152)) {
    $153 = (_strcmp($150,16200)|0);
    $154 = ($153|0)==(0);
    if (!($154)) {
     (_printf((16208|0),($vararg_buffer1|0))|0);
     _exit(-1);
     // unreachable;
    }
   }
   $155 = HEAP32[$wt>>2]|0;
   $156 = ((($155)) + 56|0);
   $157 = HEAP32[$156>>2]|0;
   $158 = (((($wt)) + 76|0) + ($1<<2)|0);
   $159 = HEAP32[$158>>2]|0;
   $160 = $159 << 1;
   $161 = (($160) + -1)|0;
   $162 = $161 << 3;
   $163 = (_malloc($162)|0);
   $164 = (($161) + ($157))|0;
   $165 = $164 << 3;
   $166 = (($165) + -8)|0;
   $167 = (_malloc($166)|0);
   $168 = (_malloc($166)|0);
   $169 = ($3|0)>(0);
   if ($169) {
    $170 = ((($wt)) + 484|0);
    $171 = HEAP32[$170>>2]|0;
    $i$649 = 0;
    while(1) {
     $180 = (($171) + ($i$649<<3)|0);
     $181 = +HEAPF64[$180>>3];
     $182 = (($8) + ($i$649<<3)|0);
     HEAPF64[$182>>3] = $181;
     $183 = (($i$649) + 1)|0;
     $exitcond62 = ($183|0)==($3|0);
     if ($exitcond62) {
      break;
     } else {
      $i$649 = $183;
     }
    }
   }
   $172 = ($1|0)>(0);
   L64: do {
    if ($172) {
     $173 = ((($wt)) + 4|0);
     $174 = ((($wt)) + 68|0);
     $175 = ((($wt)) + 484|0);
     $176 = (($157) + -2)|0;
     $177 = ((($wt)) + 4|0);
     $178 = ((($wt)) + 68|0);
     $179 = (2 - ($157))|0;
     $i$746 = 0;$iter$345 = $3;
     while(1) {
      $184 = (($i$746) + 1)|0;
      $185 = (((($wt)) + 76|0) + ($184<<2)|0);
      $186 = HEAP32[$185>>2]|0;
      (_upsamp($8,$186,2,$163)|0);
      $187 = HEAP32[$185>>2]|0;
      $188 = $187 << 1;
      $189 = (($188) + -1)|0;
      $190 = HEAP32[$wt>>2]|0;
      $191 = ((($190)) + 64|0);
      $192 = HEAP32[$191>>2]|0;
      $193 = ((($190)) + 68|0);
      $194 = HEAP32[$193>>2]|0;
      $195 = ($192|0)==($194|0);
      do {
       if ($195) {
        $196 = (_strcmp($150,16192)|0);
        $197 = ($196|0)==(0);
        if (!($197)) {
         $198 = (_strcmp($150,16200)|0);
         $199 = ($198|0)==(0);
         if (!($199)) {
          label = 46;
          break;
         }
        }
        $200 = (_conv_init($189,$157)|0);
        HEAP32[$173>>2] = $200;
        HEAP32[$174>>2] = 1;
       } else {
        label = 46;
       }
      } while(0);
      if ((label|0) == 46) {
       label = 0;
       $201 = HEAP32[$wt>>2]|0;
       $202 = ((($201)) + 64|0);
       $203 = HEAP32[$202>>2]|0;
       $204 = ((($201)) + 68|0);
       $205 = HEAP32[$204>>2]|0;
       $206 = ($203|0)==($205|0);
       if (!($206)) {
        break;
       }
      }
      $207 = HEAP32[$wt>>2]|0;
      $208 = ((($207)) + 80|0);
      $209 = HEAP32[$208>>2]|0;
      _wconv($wt,$163,$189,$209,$157,$167);
      $210 = HEAP32[$175>>2]|0;
      $211 = (($210) + ($iter$345<<3)|0);
      (_upsamp($211,$186,2,$163)|0);
      $212 = HEAP32[$wt>>2]|0;
      $213 = ((($212)) + 84|0);
      $214 = HEAP32[$213>>2]|0;
      _wconv($wt,$163,$189,$214,$157,$168);
      $215 = ($176|0)<($188|0);
      if ($215) {
       $216 = $187 << 1;
       $k$241 = $176;
       while(1) {
        $217 = (($167) + ($k$241<<3)|0);
        $218 = +HEAPF64[$217>>3];
        $219 = (($168) + ($k$241<<3)|0);
        $220 = +HEAPF64[$219>>3];
        $221 = $218 + $220;
        $222 = (($179) + ($k$241))|0;
        $223 = (($8) + ($222<<3)|0);
        HEAPF64[$223>>3] = $221;
        $224 = (($k$241) + 1)|0;
        $exitcond61 = ($224|0)==($216|0);
        if ($exitcond61) {
         break;
        } else {
         $k$241 = $224;
        }
       }
      }
      $225 = (($186) + ($iter$345))|0;
      $226 = HEAP32[$wt>>2]|0;
      $227 = ((($226)) + 64|0);
      $228 = HEAP32[$227>>2]|0;
      $229 = ((($226)) + 68|0);
      $230 = HEAP32[$229>>2]|0;
      $231 = ($228|0)==($230|0);
      do {
       if ($231) {
        $232 = (_strcmp($150,16192)|0);
        $233 = ($232|0)==(0);
        if (!($233)) {
         $234 = (_strcmp($150,16200)|0);
         $235 = ($234|0)==(0);
         if (!($235)) {
          break;
         }
        }
        $237 = HEAP32[$177>>2]|0;
        _free_conv($237);
        HEAP32[$178>>2] = 0;
       }
      } while(0);
      $236 = ($184|0)<($1|0);
      if ($236) {
       $i$746 = $184;$iter$345 = $225;
      } else {
       break L64;
      }
     }
     (_printf((16256|0),($vararg_buffer|0))|0);
     _exit(-1);
     // unreachable;
    }
   } while(0);
   _free($163);
   _free($167);
   _free($168);
  }
 } while(0);
 $57 = HEAP32[$4>>2]|0;
 $58 = ($57|0)>(0);
 if (!($58)) {
  _free($8);
  STACKTOP = sp;return;
 }
 $238 = HEAP32[$4>>2]|0;
 $239 = ($238|0)>(1);
 $$op = $238 << 3;
 $240 = $239 ? $$op : 8;
 _memcpy(($dwtop|0),($8|0),($240|0))|0;
 _free($8);
 STACKTOP = sp;return;
}
function _swt($wt,$inp) {
 $wt = $wt|0;
 $inp = $inp|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $vararg_buffer = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0;
 $vararg_buffer = sp;
 $0 = ((($wt)) + 8|0);
 $1 = (_strcmp($0,16448)|0);
 $2 = ($1|0)==(0);
 if ($2) {
  $3 = ((($wt)) + 54|0);
  $4 = (_strcmp($3,16184)|0);
  $5 = ($4|0)==(0);
  if ($5) {
   _swt_direct($wt,$inp);
   STACKTOP = sp;return;
  }
 }
 $6 = (_strcmp($0,16448)|0);
 $7 = ($6|0)==(0);
 if (!($7)) {
  (_printf((16312|0),($vararg_buffer|0))|0);
  _exit(-1);
  // unreachable;
 }
 $8 = ((($wt)) + 54|0);
 $9 = (_strcmp($8,16192)|0);
 $10 = ($9|0)==(0);
 if (!($10)) {
  $11 = (_strcmp($8,16200)|0);
  $12 = ($11|0)==(0);
  if (!($12)) {
   (_printf((16312|0),($vararg_buffer|0))|0);
   _exit(-1);
   // unreachable;
  }
 }
 _swt_fft($wt,$inp);
 STACKTOP = sp;return;
}
function _iswt($wt,$swtop) {
 $wt = $wt|0;
 $swtop = $swtop|0;
 var $$lcssa = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0.0, $107 = 0, $108 = 0.0, $109 = 0.0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0;
 var $115 = 0.0, $116 = 0, $117 = 0, $118 = 0.0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0;
 var $133 = 0.0, $134 = 0, $135 = 0.0, $136 = 0.0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0.0, $144 = 0, $145 = 0.0, $146 = 0.0, $147 = 0.0, $148 = 0, $149 = 0, $15 = 0, $150 = 0;
 var $151 = 0, $152 = 0, $153 = 0, $154 = 0, $155 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0;
 var $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0;
 var $48 = 0, $49 = 0, $5 = 0, $50 = 0.0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0.0, $59 = 0, $6 = 0, $60 = 0, $61 = 0.0, $62 = 0, $63 = 0, $64 = 0, $65 = 0;
 var $66 = 0, $67 = 0.0, $68 = 0, $69 = 0, $7 = 0, $70 = 0.0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0;
 var $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $count$038 = 0, $exitcond = 0, $exitcond50 = 0;
 var $i$321 = 0, $i$429 = 0, $index$014 = 0, $index$134 = 0, $index2$033 = 0, $index_shift$017 = 0, $index_shift$125 = 0, $iter$042 = 0, $len$0$lcssa52 = 0, $len$013 = 0, $len$013$lcssa = 0, $len0$0$lcssa = 0, $len0$016 = 0, $len0$1$lcssa = 0, $len0$124 = 0, $scevgep = 0, $scevgep48 = 0, $vararg_buffer = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0;
 $vararg_buffer = sp;
 $0 = ((($wt)) + 20|0);
 $1 = HEAP32[$0>>2]|0;
 $2 = ((($wt)) + 32|0);
 $3 = HEAP32[$2>>2]|0;
 $4 = HEAP32[$wt>>2]|0;
 $5 = ((($4)) + 64|0);
 $6 = HEAP32[$5>>2]|0;
 $7 = $1 << 3;
 $8 = (_malloc($7)|0);
 $9 = (_malloc($7)|0);
 $10 = (_malloc($7)|0);
 $11 = (_malloc($7)|0);
 $12 = (_malloc($7)|0);
 $13 = (_malloc($7)|0);
 $14 = (_malloc($7)|0);
 $15 = (($1|0) % 2)&-1;
 $16 = (($6) + ($1))|0;
 $17 = (($16) + ($15))|0;
 $18 = $17 << 3;
 $19 = (_malloc($18)|0);
 $20 = (_malloc($18)|0);
 $21 = $6 << 1;
 $22 = (($21) + ($1))|0;
 $23 = $22 << 3;
 $24 = (_malloc($23)|0);
 $25 = (_malloc($23)|0);
 $26 = (_malloc($7)|0);
 $27 = (_malloc($7)|0);
 $28 = ($3|0)>(0);
 if (!($28)) {
  _free($8);
  _free($9);
  _free($10);
  _free($11);
  _free($14);
  _free($19);
  _free($20);
  _free($24);
  _free($25);
  _free($26);
  _free($27);
  _free($12);
  _free($13);
  STACKTOP = sp;return;
 }
 $29 = ($1|0)>(0);
 $30 = (($3) + -1)|0;
 $31 = ($1|0)>(0);
 $32 = (($6|0) / 2)&-1;
 $33 = ((($wt)) + 54|0);
 $34 = ((($wt)) + 4|0);
 $35 = ((($wt)) + 68|0);
 $36 = (($6) + -1)|0;
 $37 = (1 - ($6))|0;
 $38 = (1 - ($6))|0;
 $39 = ($1|0)>(0);
 $40 = ((($wt)) + 484|0);
 $41 = ($1|0)>(0);
 $42 = ((($wt)) + 484|0);
 $43 = $1 << 3;
 $iter$042 = 0;
 L4: while(1) {
  $44 = Math_imul($1, $iter$042)|0;
  $45 = (($1) + ($44))|0;
  if ($29) {
   _memset(($swtop|0),0,($43|0))|0;
  }
  $46 = ($iter$042|0)==(0);
  if ($46) {
   if ($39) {
    $47 = HEAP32[$40>>2]|0;
    _memcpy(($8|0),($47|0),($43|0))|0;
    $scevgep48 = (($47) + ($1<<3)|0);
    _memcpy(($9|0),($scevgep48|0),($43|0))|0;
   }
  } else {
   if ($41) {
    $48 = HEAP32[$42>>2]|0;
    $scevgep = (($48) + ($45<<3)|0);
    _memcpy(($9|0),($scevgep|0),($43|0))|0;
   }
  }
  $49 = (($30) - ($iter$042))|0;
  $50 = (+_ldexp(1.0,$49));
  $51 = (~~(($50)));
  $52 = ($51|0)>(0);
  if ($52) {
   $count$038 = 0;
   while(1) {
    $53 = ($count$038|0)<($1|0);
    if ($53) {
     $index$014 = $count$038;$len$013 = 0;
     while(1) {
      $57 = (($8) + ($index$014<<3)|0);
      $58 = +HEAPF64[$57>>3];
      $59 = (($10) + ($len$013<<3)|0);
      HEAPF64[$59>>3] = $58;
      $60 = (($9) + ($index$014<<3)|0);
      $61 = +HEAPF64[$60>>3];
      $62 = (($11) + ($len$013<<3)|0);
      HEAPF64[$62>>3] = $61;
      $63 = (($len$013) + 1)|0;
      $64 = (($index$014) + ($51))|0;
      $65 = ($64|0)<($1|0);
      if ($65) {
       $index$014 = $64;$len$013 = $63;
      } else {
       $$lcssa = $63;$len$013$lcssa = $len$013;
       break;
      }
     }
     $54 = ($len$013$lcssa|0)>(-1);
     if ($54) {
      $55 = $len$013$lcssa >>> 1;
      $56 = (($55) + 1)|0;
      $index_shift$017 = 0;$len0$016 = 0;
      while(1) {
       $66 = (($10) + ($index_shift$017<<3)|0);
       $67 = +HEAPF64[$66>>3];
       $68 = (($12) + ($len0$016<<3)|0);
       HEAPF64[$68>>3] = $67;
       $69 = (($11) + ($index_shift$017<<3)|0);
       $70 = +HEAPF64[$69>>3];
       $71 = (($13) + ($len0$016<<3)|0);
       HEAPF64[$71>>3] = $70;
       $72 = (($len0$016) + 1)|0;
       $73 = (($index_shift$017) + 2)|0;
       $exitcond = ($72|0)==($56|0);
       if ($exitcond) {
        $len$0$lcssa52 = $$lcssa;$len0$0$lcssa = $56;
        break;
       } else {
        $index_shift$017 = $73;$len0$016 = $72;
       }
      }
     } else {
      $len$0$lcssa52 = $$lcssa;$len0$0$lcssa = 0;
     }
    } else {
     $len$0$lcssa52 = 0;$len0$0$lcssa = 0;
    }
    (_upsamp2($12,$len0$0$lcssa,2,$14)|0);
    $74 = $len0$0$lcssa << 1;
    (_per_ext($14,$74,$32,$19)|0);
    (_upsamp2($13,$len0$0$lcssa,2,$14)|0);
    (_per_ext($14,$74,$32,$20)|0);
    $75 = (($74) + ($6))|0;
    $76 = HEAP32[$wt>>2]|0;
    $77 = ((($76)) + 64|0);
    $78 = HEAP32[$77>>2]|0;
    $79 = ((($76)) + 68|0);
    $80 = HEAP32[$79>>2]|0;
    $81 = ($78|0)==($80|0);
    do {
     if ($81) {
      $82 = (_strcmp($33,16192)|0);
      $83 = ($82|0)==(0);
      if (!($83)) {
       $84 = (_strcmp($33,16200)|0);
       $85 = ($84|0)==(0);
       if (!($85)) {
        label = 21;
        break;
       }
      }
      $86 = (_conv_init($75,$6)|0);
      HEAP32[$34>>2] = $86;
      HEAP32[$35>>2] = 1;
     } else {
      label = 21;
     }
    } while(0);
    if ((label|0) == 21) {
     label = 0;
     $87 = HEAP32[$wt>>2]|0;
     $88 = ((($87)) + 56|0);
     $89 = HEAP32[$88>>2]|0;
     $90 = ((($87)) + 60|0);
     $91 = HEAP32[$90>>2]|0;
     $92 = ($89|0)==($91|0);
     if (!($92)) {
      label = 22;
      break L4;
     }
    }
    $93 = HEAP32[$wt>>2]|0;
    $94 = ((($93)) + 80|0);
    $95 = HEAP32[$94>>2]|0;
    _wconv($wt,$19,$75,$95,$6,$24);
    $96 = HEAP32[$wt>>2]|0;
    $97 = ((($96)) + 84|0);
    $98 = HEAP32[$97>>2]|0;
    _wconv($wt,$20,$75,$98,$6,$25);
    $99 = (($75) + -1)|0;
    $100 = ($6|0)>($99|0);
    if (!($100)) {
     $i$321 = $36;
     while(1) {
      $105 = (($24) + ($i$321<<3)|0);
      $106 = +HEAPF64[$105>>3];
      $107 = (($25) + ($i$321<<3)|0);
      $108 = +HEAPF64[$107>>3];
      $109 = $106 + $108;
      $110 = (($38) + ($i$321))|0;
      $111 = (($26) + ($110<<3)|0);
      HEAPF64[$111>>3] = $109;
      $112 = (($i$321) + 1)|0;
      $113 = ($112|0)<($99|0);
      if ($113) {
       $i$321 = $112;
      } else {
       break;
      }
     }
    }
    $101 = ($len$0$lcssa52|0)>(1);
    if ($101) {
     $102 = (($len$0$lcssa52) + -2)|0;
     $103 = $102 >>> 1;
     $104 = (($103) + 1)|0;
     $index_shift$125 = 1;$len0$124 = 0;
     while(1) {
      $114 = (($10) + ($index_shift$125<<3)|0);
      $115 = +HEAPF64[$114>>3];
      $116 = (($12) + ($len0$124<<3)|0);
      HEAPF64[$116>>3] = $115;
      $117 = (($11) + ($index_shift$125<<3)|0);
      $118 = +HEAPF64[$117>>3];
      $119 = (($13) + ($len0$124<<3)|0);
      HEAPF64[$119>>3] = $118;
      $120 = (($len0$124) + 1)|0;
      $121 = (($index_shift$125) + 2)|0;
      $exitcond50 = ($120|0)==($104|0);
      if ($exitcond50) {
       $len0$1$lcssa = $104;
       break;
      } else {
       $index_shift$125 = $121;$len0$124 = $120;
      }
     }
    } else {
     $len0$1$lcssa = 0;
    }
    (_upsamp2($12,$len0$1$lcssa,2,$14)|0);
    $122 = $len0$1$lcssa << 1;
    (_per_ext($14,$122,$32,$19)|0);
    (_upsamp2($13,$len0$1$lcssa,2,$14)|0);
    (_per_ext($14,$122,$32,$20)|0);
    $123 = (($122) + ($6))|0;
    $124 = HEAP32[$wt>>2]|0;
    $125 = ((($124)) + 80|0);
    $126 = HEAP32[$125>>2]|0;
    _wconv($wt,$19,$123,$126,$6,$24);
    $127 = HEAP32[$wt>>2]|0;
    $128 = ((($127)) + 84|0);
    $129 = HEAP32[$128>>2]|0;
    _wconv($wt,$20,$123,$129,$6,$25);
    $130 = (($123) + -1)|0;
    $131 = ($6|0)>($130|0);
    if (!($131)) {
     $i$429 = $36;
     while(1) {
      $132 = (($24) + ($i$429<<3)|0);
      $133 = +HEAPF64[$132>>3];
      $134 = (($25) + ($i$429<<3)|0);
      $135 = +HEAPF64[$134>>3];
      $136 = $133 + $135;
      $137 = (($37) + ($i$429))|0;
      $138 = (($27) + ($137<<3)|0);
      HEAPF64[$138>>3] = $136;
      $139 = (($i$429) + 1)|0;
      $140 = ($139|0)<($130|0);
      if ($140) {
       $i$429 = $139;
      } else {
       break;
      }
     }
    }
    _circshift($27,$122,-1);
    $141 = ($count$038|0)<($1|0);
    if ($141) {
     $index$134 = $count$038;$index2$033 = 0;
     while(1) {
      $142 = (($26) + ($index2$033<<3)|0);
      $143 = +HEAPF64[$142>>3];
      $144 = (($27) + ($index2$033<<3)|0);
      $145 = +HEAPF64[$144>>3];
      $146 = $143 + $145;
      $147 = $146 * 0.5;
      $148 = (($swtop) + ($index$134<<3)|0);
      HEAPF64[$148>>3] = $147;
      $149 = (($index2$033) + 1)|0;
      $150 = (($index$134) + ($51))|0;
      $151 = ($150|0)<($1|0);
      if ($151) {
       $index$134 = $150;$index2$033 = $149;
      } else {
       break;
      }
     }
    }
    $152 = (($count$038) + 1)|0;
    $153 = ($152|0)<($51|0);
    if ($153) {
     $count$038 = $152;
    } else {
     break;
    }
   }
  }
  if ($31) {
   _memcpy(($8|0),($swtop|0),($43|0))|0;
  }
  $154 = (($iter$042) + 1)|0;
  $155 = ($154|0)<($3|0);
  if ($155) {
   $iter$042 = $154;
  } else {
   label = 35;
   break;
  }
 }
 if ((label|0) == 22) {
  (_printf((16256|0),($vararg_buffer|0))|0);
  _exit(-1);
  // unreachable;
 }
 else if ((label|0) == 35) {
  _free($8);
  _free($9);
  _free($10);
  _free($11);
  _free($14);
  _free($19);
  _free($20);
  _free($24);
  _free($25);
  _free($26);
  _free($27);
  _free($12);
  _free($13);
  STACKTOP = sp;return;
 }
}
function _modwt($wt,$inp) {
 $wt = $wt|0;
 $inp = $inp|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0.0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0.0, $3 = 0, $30 = 0, $31 = 0, $32 = 0.0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $M$13 = 0, $M$2 = 0, $exitcond = 0;
 var $exitcond19 = 0, $exitcond20 = 0, $exitcond22 = 0, $i$08 = 0, $i$11 = 0, $iter$012 = 0, $iter$14 = 0, $lenacc$02 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ((($wt)) + 20|0);
 $1 = HEAP32[$0>>2]|0;
 $2 = ((($wt)) + 32|0);
 $3 = HEAP32[$2>>2]|0;
 $4 = (((($wt)) + 76|0) + ($3<<2)|0);
 HEAP32[$4>>2] = $1;
 $5 = ((($wt)) + 76|0);
 HEAP32[$5>>2] = $1;
 $6 = (($3) + 1)|0;
 $7 = Math_imul($6, $1)|0;
 $8 = (((($wt)) + 76|0) + ($6<<2)|0);
 HEAP32[$8>>2] = $7;
 $9 = ((($wt)) + 24|0);
 HEAP32[$9>>2] = $7;
 $10 = ($3|0)>(1);
 if ($10) {
  $iter$012 = 1;
  while(1) {
   $11 = (((($wt)) + 76|0) + ($iter$012<<2)|0);
   HEAP32[$11>>2] = $1;
   $12 = (($iter$012) + 1)|0;
   $exitcond22 = ($12|0)==($3|0);
   if ($exitcond22) {
    break;
   } else {
    $iter$012 = $12;
   }
  }
 }
 $13 = $1 << 3;
 $14 = (_malloc($13)|0);
 $15 = (_malloc($13)|0);
 $16 = ($1|0)>(0);
 if ($16) {
  $i$08 = 0;
  while(1) {
   $17 = (($inp) + ($i$08<<3)|0);
   $18 = +HEAPF64[$17>>3];
   $19 = (((($wt)) + 488|0) + ($i$08<<3)|0);
   HEAPF64[$19>>3] = $18;
   $20 = (($i$08) + 1)|0;
   $exitcond20 = ($20|0)==($1|0);
   if ($exitcond20) {
    break;
   } else {
    $i$08 = $20;
   }
  }
 }
 $21 = ($3|0)>(0);
 if (!($21)) {
  _free($14);
  _free($15);
  return;
 }
 $22 = HEAP32[$9>>2]|0;
 $23 = ((($wt)) + 488|0);
 $24 = ($1|0)>(0);
 $M$13 = 1;$iter$14 = 0;$lenacc$02 = $22;
 while(1) {
  $25 = (($lenacc$02) - ($1))|0;
  $26 = ($iter$14|0)>(0);
  $27 = $26&1;
  $M$2 = $M$13 << $27;
  _modwt_per($wt,$M$2,$23,$14,$1,$15);
  if ($24) {
   $i$11 = 0;
   while(1) {
    $28 = (($14) + ($i$11<<3)|0);
    $29 = +HEAPF64[$28>>3];
    $30 = (((($wt)) + 488|0) + ($i$11<<3)|0);
    HEAPF64[$30>>3] = $29;
    $31 = (($15) + ($i$11<<3)|0);
    $32 = +HEAPF64[$31>>3];
    $33 = (($i$11) + ($25))|0;
    $34 = (((($wt)) + 488|0) + ($33<<3)|0);
    HEAPF64[$34>>3] = $32;
    $35 = (($i$11) + 1)|0;
    $exitcond = ($35|0)==($1|0);
    if ($exitcond) {
     break;
    } else {
     $i$11 = $35;
    }
   }
  }
  $36 = (($iter$14) + 1)|0;
  $exitcond19 = ($36|0)==($3|0);
  if ($exitcond19) {
   break;
  } else {
   $M$13 = $M$2;$iter$14 = $36;$lenacc$02 = $25;
  }
 }
 _free($14);
 _free($15);
 return;
}
function _imodwt($wt,$dwtop) {
 $wt = $wt|0;
 $dwtop = $dwtop|0;
 var $$M$0 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0.0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $3 = 0, $4 = 0.0;
 var $5 = 0.0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $M$03 = 0, $exitcond = 0, $exitcond11 = 0, $exp2 = 0.0, $i$07 = 0, $iter$04 = 0, $lenacc$02 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ((($wt)) + 20|0);
 $1 = HEAP32[$0>>2]|0;
 $2 = ((($wt)) + 32|0);
 $3 = HEAP32[$2>>2]|0;
 $4 = (+($3|0));
 $5 = $4 + -1.0;
 $exp2 = (+_exp2($5));
 $6 = (~~(($exp2)));
 $7 = $1 << 3;
 $8 = (_malloc($7)|0);
 $9 = ($1|0)>(0);
 if ($9) {
  $10 = ((($wt)) + 484|0);
  $11 = HEAP32[$10>>2]|0;
  $i$07 = 0;
  while(1) {
   $15 = (($11) + ($i$07<<3)|0);
   $16 = +HEAPF64[$15>>3];
   $17 = (($dwtop) + ($i$07<<3)|0);
   HEAPF64[$17>>3] = $16;
   $18 = (($i$07) + 1)|0;
   $exitcond11 = ($18|0)==($1|0);
   if ($exitcond11) {
    break;
   } else {
    $i$07 = $18;
   }
  }
 }
 $12 = ($3|0)>(0);
 if (!($12)) {
  _free($8);
  return;
 }
 $13 = ($1|0)>(0);
 $14 = $1 << 3;
 $M$03 = $6;$iter$04 = 0;$lenacc$02 = $1;
 while(1) {
  $19 = ($iter$04|0)>(0);
  $20 = (($M$03|0) / 2)&-1;
  $$M$0 = $19 ? $20 : $M$03;
  $21 = (((($wt)) + 488|0) + ($lenacc$02<<3)|0);
  _imodwt_per($wt,$$M$0,$dwtop,$1,$21,$8);
  if ($13) {
   _memcpy(($dwtop|0),($8|0),($14|0))|0;
  }
  $22 = (($lenacc$02) + ($1))|0;
  $23 = (($iter$04) + 1)|0;
  $exitcond = ($23|0)==($3|0);
  if ($exitcond) {
   break;
  } else {
   $M$03 = $$M$0;$iter$04 = $23;$lenacc$02 = $22;
  }
 }
 _free($8);
 return;
}
function _setDWTExtension($wt,$extension) {
 $wt = $wt|0;
 $extension = $extension|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $vararg_buffer = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0;
 $vararg_buffer = sp;
 $0 = (_strcmp($extension,16440)|0);
 $1 = ($0|0)==(0);
 if ($1) {
  $2 = ((($wt)) + 44|0);
  HEAP8[$2>>0]=7174515&255;HEAP8[$2+1>>0]=(7174515>>8)&255;HEAP8[$2+2>>0]=(7174515>>16)&255;HEAP8[$2+3>>0]=7174515>>24;
  STACKTOP = sp;return;
 }
 $3 = (_strcmp($extension,16432)|0);
 $4 = ($3|0)==(0);
 if (!($4)) {
  (_printf((16208|0),($vararg_buffer|0))|0);
  _exit(-1);
  // unreachable;
 }
 $5 = ((($wt)) + 44|0);
 HEAP8[$5>>0]=7497072&255;HEAP8[$5+1>>0]=(7497072>>8)&255;HEAP8[$5+2>>0]=(7497072>>16)&255;HEAP8[$5+3>>0]=7497072>>24;
 STACKTOP = sp;return;
}
function _wave_free($object) {
 $object = $object|0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 _free($object);
 return;
}
function _wt_free($object) {
 $object = $object|0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 _free($object);
 return;
}
function _dwt1($wt,$sig,$len_sig,$cA,$cD) {
 $wt = $wt|0;
 $sig = $sig|0;
 $len_sig = $len_sig|0;
 $cA = $cA|0;
 $cD = $cD|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0;
 var $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0;
 var $cA_undec$0 = 0, $signal$0 = 0, $vararg_buffer = 0, $vararg_buffer1 = 0, $vararg_buffer3 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0;
 $vararg_buffer3 = sp + 16|0;
 $vararg_buffer1 = sp;
 $vararg_buffer = sp + 8|0;
 $0 = HEAP32[$wt>>2]|0;
 $1 = ((($0)) + 56|0);
 $2 = HEAP32[$1>>2]|0;
 $3 = ((($0)) + 60|0);
 $4 = HEAP32[$3>>2]|0;
 $5 = (($4) + ($2))|0;
 $6 = (($5|0) / 2)&-1;
 $7 = ((($wt)) + 44|0);
 $8 = (_strcmp($7,16432)|0);
 $9 = ($8|0)==(0);
 if ($9) {
  $10 = (($6) + ($len_sig))|0;
  $11 = (($len_sig|0) % 2)&-1;
  $12 = (($10) + ($11))|0;
  $13 = $12 << 3;
  $14 = (_malloc($13)|0);
  $15 = (($5|0) / 4)&-1;
  $16 = (_per_ext($sig,$len_sig,$15,$14)|0);
  $17 = (($16) + ($6))|0;
  $18 = HEAP32[$wt>>2]|0;
  $19 = ((($18)) + 56|0);
  $20 = HEAP32[$19>>2]|0;
  $21 = (($20) + ($17))|0;
  $22 = $21 << 3;
  $23 = (($22) + -8)|0;
  $24 = (_malloc($23)|0);
  $25 = HEAP32[$wt>>2]|0;
  $26 = ((($25)) + 56|0);
  $27 = HEAP32[$26>>2]|0;
  $28 = ((($25)) + 60|0);
  $29 = HEAP32[$28>>2]|0;
  $30 = ($27|0)==($29|0);
  do {
   if ($30) {
    $31 = ((($wt)) + 54|0);
    $32 = (_strcmp($31,16192)|0);
    $33 = ($32|0)==(0);
    if (!($33)) {
     $34 = (_strcmp($31,16200)|0);
     $35 = ($34|0)==(0);
     if (!($35)) {
      label = 6;
      break;
     }
    }
    $36 = HEAP32[$wt>>2]|0;
    $37 = ((($36)) + 56|0);
    $38 = HEAP32[$37>>2]|0;
    $39 = (_conv_init($17,$38)|0);
    $40 = ((($wt)) + 4|0);
    HEAP32[$40>>2] = $39;
    $41 = ((($wt)) + 68|0);
    HEAP32[$41>>2] = 1;
   } else {
    label = 6;
   }
  } while(0);
  if ((label|0) == 6) {
   $42 = HEAP32[$wt>>2]|0;
   $43 = ((($42)) + 56|0);
   $44 = HEAP32[$43>>2]|0;
   $45 = ((($42)) + 60|0);
   $46 = HEAP32[$45>>2]|0;
   $47 = ($44|0)==($46|0);
   if (!($47)) {
    (_printf((16256|0),($vararg_buffer|0))|0);
    _exit(-1);
    // unreachable;
   }
  }
  $48 = HEAP32[$wt>>2]|0;
  $49 = ((($48)) + 72|0);
  $50 = HEAP32[$49>>2]|0;
  $51 = ((($48)) + 56|0);
  $52 = HEAP32[$51>>2]|0;
  _wconv($wt,$14,$17,$50,$52,$24);
  $53 = (($24) + ($6<<3)|0);
  (_downsamp($53,$16,2,$cA)|0);
  $54 = HEAP32[$wt>>2]|0;
  $55 = ((($54)) + 76|0);
  $56 = HEAP32[$55>>2]|0;
  $57 = ((($54)) + 60|0);
  $58 = HEAP32[$57>>2]|0;
  _wconv($wt,$14,$17,$56,$58,$24);
  (_downsamp($53,$16,2,$cD)|0);
  $cA_undec$0 = $24;$signal$0 = $14;
 } else {
  $59 = (_strcmp($7,16440)|0);
  $60 = ($59|0)==(0);
  if (!($60)) {
   (_printf((16208|0),($vararg_buffer3|0))|0);
   _exit(-1);
   // unreachable;
  }
  $61 = (($2) + -1)|0;
  $62 = $61 << 1;
  $63 = (($62) + ($len_sig))|0;
  $64 = $63 << 3;
  $65 = (_malloc($64)|0);
  $66 = (_symm_ext($sig,$len_sig,$61,$65)|0);
  $67 = ($61*3)|0;
  $68 = (($66) + ($67))|0;
  $69 = $68 << 3;
  $70 = (_malloc($69)|0);
  $71 = HEAP32[$wt>>2]|0;
  $72 = ((($71)) + 56|0);
  $73 = HEAP32[$72>>2]|0;
  $74 = ((($71)) + 60|0);
  $75 = HEAP32[$74>>2]|0;
  $76 = ($73|0)==($75|0);
  do {
   if ($76) {
    $77 = ((($wt)) + 54|0);
    $78 = (_strcmp($77,16192)|0);
    $79 = ($78|0)==(0);
    if (!($79)) {
     $80 = (_strcmp($77,16200)|0);
     $81 = ($80|0)==(0);
     if (!($81)) {
      label = 14;
      break;
     }
    }
    $82 = (($66) + ($62))|0;
    $83 = (_conv_init($82,$2)|0);
    $84 = ((($wt)) + 4|0);
    HEAP32[$84>>2] = $83;
    $85 = ((($wt)) + 68|0);
    HEAP32[$85>>2] = 1;
   } else {
    label = 14;
   }
  } while(0);
  if ((label|0) == 14) {
   $86 = HEAP32[$wt>>2]|0;
   $87 = ((($86)) + 56|0);
   $88 = HEAP32[$87>>2]|0;
   $89 = ((($86)) + 60|0);
   $90 = HEAP32[$89>>2]|0;
   $91 = ($88|0)==($90|0);
   if (!($91)) {
    (_printf((16256|0),($vararg_buffer1|0))|0);
    _exit(-1);
    // unreachable;
   }
  }
  $92 = (($66) + ($62))|0;
  $93 = HEAP32[$wt>>2]|0;
  $94 = ((($93)) + 72|0);
  $95 = HEAP32[$94>>2]|0;
  $96 = ((($93)) + 56|0);
  $97 = HEAP32[$96>>2]|0;
  _wconv($wt,$65,$92,$95,$97,$70);
  $98 = (($70) + ($2<<3)|0);
  $99 = (($2) + -2)|0;
  $100 = (($99) + ($66))|0;
  (_downsamp($98,$100,2,$cA)|0);
  $101 = HEAP32[$wt>>2]|0;
  $102 = ((($101)) + 76|0);
  $103 = HEAP32[$102>>2]|0;
  $104 = ((($101)) + 60|0);
  $105 = HEAP32[$104>>2]|0;
  _wconv($wt,$65,$92,$103,$105,$70);
  (_downsamp($98,$100,2,$cD)|0);
  $cA_undec$0 = $70;$signal$0 = $65;
 }
 $106 = HEAP32[$wt>>2]|0;
 $107 = ((($106)) + 56|0);
 $108 = HEAP32[$107>>2]|0;
 $109 = ((($106)) + 60|0);
 $110 = HEAP32[$109>>2]|0;
 $111 = ($108|0)==($110|0);
 if (!($111)) {
  _free($signal$0);
  _free($cA_undec$0);
  STACKTOP = sp;return;
 }
 $112 = ((($wt)) + 54|0);
 $113 = (_strcmp($112,16192)|0);
 $114 = ($113|0)==(0);
 if (!($114)) {
  $115 = (_strcmp($112,16200)|0);
  $116 = ($115|0)==(0);
  if (!($116)) {
   _free($signal$0);
   _free($cA_undec$0);
   STACKTOP = sp;return;
  }
 }
 $117 = ((($wt)) + 4|0);
 $118 = HEAP32[$117>>2]|0;
 _free_conv($118);
 $119 = ((($wt)) + 68|0);
 HEAP32[$119>>2] = 0;
 _free($signal$0);
 _free($cA_undec$0);
 STACKTOP = sp;return;
}
function _dwt_per($wt,$inp,$N,$cA,$len_cA,$cD) {
 $wt = $wt|0;
 $inp = $inp|0;
 $N = $N|0;
 $cA = $cA|0;
 $len_cA = $len_cA|0;
 $cD = $cD|0;
 var $$neg = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0.0, $106 = 0, $107 = 0, $108 = 0.0, $109 = 0.0, $11 = 0, $110 = 0.0, $111 = 0.0, $112 = 0, $113 = 0, $114 = 0;
 var $115 = 0, $116 = 0.0, $117 = 0.0, $118 = 0.0, $119 = 0.0, $12 = 0, $120 = 0.0, $121 = 0.0, $122 = 0.0, $123 = 0.0, $124 = 0.0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0.0, $13 = 0, $130 = 0.0, $131 = 0.0, $132 = 0.0;
 var $133 = 0.0, $134 = 0, $135 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0.0, $23 = 0, $24 = 0.0, $25 = 0.0, $26 = 0.0, $27 = 0.0, $28 = 0, $29 = 0;
 var $3 = 0, $30 = 0, $31 = 0, $32 = 0.0, $33 = 0.0, $34 = 0.0, $35 = 0.0, $36 = 0.0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0.0, $44 = 0, $45 = 0.0, $46 = 0.0, $47 = 0.0;
 var $48 = 0.0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0.0, $54 = 0.0, $55 = 0.0, $56 = 0.0, $57 = 0.0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0.0, $64 = 0, $65 = 0;
 var $66 = 0.0, $67 = 0.0, $68 = 0.0, $69 = 0.0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0.0, $75 = 0.0, $76 = 0.0, $77 = 0.0, $78 = 0.0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0;
 var $84 = 0.0, $85 = 0, $86 = 0, $87 = 0.0, $88 = 0.0, $89 = 0.0, $9 = 0, $90 = 0.0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0.0, $96 = 0.0, $97 = 0.0, $98 = 0.0, $99 = 0.0, $exitcond = 0, $exitcond8 = 0, $i$04 = 0;
 var $l$03 = 0, $or$cond = 0, $or$cond1 = 0, $or$cond2 = 0, $or$cond3 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[$wt>>2]|0;
 $1 = ((($0)) + 56|0);
 $2 = HEAP32[$1>>2]|0;
 $3 = (($2|0) / 2)&-1;
 $4 = (($N|0) % 2)&-1;
 $5 = ($len_cA|0)>(0);
 if (!($5)) {
  return;
 }
 $6 = ($2|0)>(0);
 $7 = ($4|0)==(0);
 $8 = ($4|0)==(1);
 $9 = (($N) + -1)|0;
 $10 = (($inp) + ($9<<3)|0);
 $$neg = $N ^ -1;
 $i$04 = 0;
 while(1) {
  $11 = $i$04 << 1;
  $12 = (($11) + ($3))|0;
  $13 = (($cA) + ($i$04<<3)|0);
  HEAPF64[$13>>3] = 0.0;
  $14 = (($cD) + ($i$04<<3)|0);
  HEAPF64[$14>>3] = 0.0;
  if ($6) {
   $l$03 = 0;
   while(1) {
    $15 = (($12) - ($l$03))|0;
    $16 = ($15|0)>=($3|0);
    $17 = ($15|0)<($N|0);
    $or$cond1 = $16 & $17;
    do {
     if ($or$cond1) {
      $18 = HEAP32[$wt>>2]|0;
      $19 = ((($18)) + 72|0);
      $20 = HEAP32[$19>>2]|0;
      $21 = (($20) + ($l$03<<3)|0);
      $22 = +HEAPF64[$21>>3];
      $23 = (($inp) + ($15<<3)|0);
      $24 = +HEAPF64[$23>>3];
      $25 = $22 * $24;
      $26 = +HEAPF64[$13>>3];
      $27 = $26 + $25;
      HEAPF64[$13>>3] = $27;
      $28 = HEAP32[$wt>>2]|0;
      $29 = ((($28)) + 76|0);
      $30 = HEAP32[$29>>2]|0;
      $31 = (($30) + ($l$03<<3)|0);
      $32 = +HEAPF64[$31>>3];
      $33 = +HEAPF64[$23>>3];
      $34 = $32 * $33;
      $35 = +HEAPF64[$14>>3];
      $36 = $35 + $34;
      HEAPF64[$14>>3] = $36;
     } else {
      $37 = ($15|0)<($3|0);
      $38 = ($15|0)>(-1);
      $or$cond2 = $37 & $38;
      if ($or$cond2) {
       $39 = HEAP32[$wt>>2]|0;
       $40 = ((($39)) + 72|0);
       $41 = HEAP32[$40>>2]|0;
       $42 = (($41) + ($l$03<<3)|0);
       $43 = +HEAPF64[$42>>3];
       $44 = (($inp) + ($15<<3)|0);
       $45 = +HEAPF64[$44>>3];
       $46 = $43 * $45;
       $47 = +HEAPF64[$13>>3];
       $48 = $47 + $46;
       HEAPF64[$13>>3] = $48;
       $49 = HEAP32[$wt>>2]|0;
       $50 = ((($49)) + 76|0);
       $51 = HEAP32[$50>>2]|0;
       $52 = (($51) + ($l$03<<3)|0);
       $53 = +HEAPF64[$52>>3];
       $54 = +HEAPF64[$44>>3];
       $55 = $53 * $54;
       $56 = +HEAPF64[$14>>3];
       $57 = $56 + $55;
       HEAPF64[$14>>3] = $57;
       break;
      }
      $58 = ($15|0)<(0);
      if ($58) {
       $59 = HEAP32[$wt>>2]|0;
       $60 = ((($59)) + 72|0);
       $61 = HEAP32[$60>>2]|0;
       $62 = (($61) + ($l$03<<3)|0);
       $63 = +HEAPF64[$62>>3];
       $64 = (($15) + ($N))|0;
       $65 = (($inp) + ($64<<3)|0);
       $66 = +HEAPF64[$65>>3];
       $67 = $63 * $66;
       $68 = +HEAPF64[$13>>3];
       $69 = $68 + $67;
       HEAPF64[$13>>3] = $69;
       $70 = HEAP32[$wt>>2]|0;
       $71 = ((($70)) + 76|0);
       $72 = HEAP32[$71>>2]|0;
       $73 = (($72) + ($l$03<<3)|0);
       $74 = +HEAPF64[$73>>3];
       $75 = +HEAPF64[$65>>3];
       $76 = $74 * $75;
       $77 = +HEAPF64[$14>>3];
       $78 = $77 + $76;
       HEAPF64[$14>>3] = $78;
       break;
      }
      $79 = ($15|0)>=($N|0);
      $or$cond = $7 & $79;
      if ($or$cond) {
       $80 = HEAP32[$wt>>2]|0;
       $81 = ((($80)) + 72|0);
       $82 = HEAP32[$81>>2]|0;
       $83 = (($82) + ($l$03<<3)|0);
       $84 = +HEAPF64[$83>>3];
       $85 = (($15) - ($N))|0;
       $86 = (($inp) + ($85<<3)|0);
       $87 = +HEAPF64[$86>>3];
       $88 = $84 * $87;
       $89 = +HEAPF64[$13>>3];
       $90 = $89 + $88;
       HEAPF64[$13>>3] = $90;
       $91 = HEAP32[$wt>>2]|0;
       $92 = ((($91)) + 76|0);
       $93 = HEAP32[$92>>2]|0;
       $94 = (($93) + ($l$03<<3)|0);
       $95 = +HEAPF64[$94>>3];
       $96 = +HEAPF64[$86>>3];
       $97 = $95 * $96;
       $98 = +HEAPF64[$14>>3];
       $99 = $98 + $97;
       HEAPF64[$14>>3] = $99;
       break;
      }
      $or$cond3 = $8 & $79;
      if ($or$cond3) {
       $100 = ($15|0)==($N|0);
       $101 = HEAP32[$wt>>2]|0;
       $102 = ((($101)) + 72|0);
       $103 = HEAP32[$102>>2]|0;
       $104 = (($103) + ($l$03<<3)|0);
       $105 = +HEAPF64[$104>>3];
       if ($100) {
        $121 = +HEAPF64[$10>>3];
        $122 = $105 * $121;
        $123 = +HEAPF64[$13>>3];
        $124 = $123 + $122;
        HEAPF64[$13>>3] = $124;
        $125 = HEAP32[$wt>>2]|0;
        $126 = ((($125)) + 76|0);
        $127 = HEAP32[$126>>2]|0;
        $128 = (($127) + ($l$03<<3)|0);
        $129 = +HEAPF64[$128>>3];
        $130 = +HEAPF64[$10>>3];
        $131 = $129 * $130;
        $132 = +HEAPF64[$14>>3];
        $133 = $132 + $131;
        HEAPF64[$14>>3] = $133;
        break;
       } else {
        $106 = (($15) + ($$neg))|0;
        $107 = (($inp) + ($106<<3)|0);
        $108 = +HEAPF64[$107>>3];
        $109 = $105 * $108;
        $110 = +HEAPF64[$13>>3];
        $111 = $110 + $109;
        HEAPF64[$13>>3] = $111;
        $112 = HEAP32[$wt>>2]|0;
        $113 = ((($112)) + 76|0);
        $114 = HEAP32[$113>>2]|0;
        $115 = (($114) + ($l$03<<3)|0);
        $116 = +HEAPF64[$115>>3];
        $117 = +HEAPF64[$107>>3];
        $118 = $116 * $117;
        $119 = +HEAPF64[$14>>3];
        $120 = $119 + $118;
        HEAPF64[$14>>3] = $120;
        break;
       }
      }
     }
    } while(0);
    $134 = (($l$03) + 1)|0;
    $exitcond = ($134|0)==($2|0);
    if ($exitcond) {
     break;
    } else {
     $l$03 = $134;
    }
   }
  }
  $135 = (($i$04) + 1)|0;
  $exitcond8 = ($135|0)==($len_cA|0);
  if ($exitcond8) {
   break;
  } else {
   $i$04 = $135;
  }
 }
 return;
}
function _dwt_sym($wt,$inp,$N,$cA,$len_cA,$cD) {
 $wt = $wt|0;
 $inp = $inp|0;
 $N = $N|0;
 $cA = $cA|0;
 $len_cA = $len_cA|0;
 $cD = $cD|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0.0, $21 = 0, $22 = 0.0, $23 = 0.0, $24 = 0.0, $25 = 0.0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0.0, $31 = 0.0, $32 = 0.0, $33 = 0.0, $34 = 0.0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0.0, $41 = 0, $42 = 0, $43 = 0.0, $44 = 0.0;
 var $45 = 0.0, $46 = 0.0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0.0, $52 = 0.0, $53 = 0.0, $54 = 0.0, $55 = 0.0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0.0, $61 = 0, $62 = 0;
 var $63 = 0.0, $64 = 0.0, $65 = 0.0, $66 = 0.0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0.0, $72 = 0.0, $73 = 0.0, $74 = 0.0, $75 = 0.0, $76 = 0, $77 = 0, $8 = 0, $9 = 0, $exitcond = 0, $exitcond6 = 0;
 var $i$02 = 0, $l$01 = 0, $or$cond = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[$wt>>2]|0;
 $1 = ((($0)) + 56|0);
 $2 = HEAP32[$1>>2]|0;
 $3 = ($len_cA|0)>(0);
 if (!($3)) {
  return;
 }
 $4 = ($2|0)>(0);
 $5 = $N << 1;
 $6 = (($5) + -1)|0;
 $i$02 = 0;
 while(1) {
  $7 = $i$02 << 1;
  $8 = $7 | 1;
  $9 = (($cA) + ($i$02<<3)|0);
  HEAPF64[$9>>3] = 0.0;
  $10 = (($cD) + ($i$02<<3)|0);
  HEAPF64[$10>>3] = 0.0;
  if ($4) {
   $11 = $7 ^ -2;
   $12 = (($6) - ($8))|0;
   $l$01 = 0;
   while(1) {
    $13 = (($8) - ($l$01))|0;
    $14 = ($13|0)>(-1);
    $15 = ($13|0)<($N|0);
    $or$cond = $14 & $15;
    do {
     if ($or$cond) {
      $16 = HEAP32[$wt>>2]|0;
      $17 = ((($16)) + 72|0);
      $18 = HEAP32[$17>>2]|0;
      $19 = (($18) + ($l$01<<3)|0);
      $20 = +HEAPF64[$19>>3];
      $21 = (($inp) + ($13<<3)|0);
      $22 = +HEAPF64[$21>>3];
      $23 = $20 * $22;
      $24 = +HEAPF64[$9>>3];
      $25 = $24 + $23;
      HEAPF64[$9>>3] = $25;
      $26 = HEAP32[$wt>>2]|0;
      $27 = ((($26)) + 76|0);
      $28 = HEAP32[$27>>2]|0;
      $29 = (($28) + ($l$01<<3)|0);
      $30 = +HEAPF64[$29>>3];
      $31 = +HEAPF64[$21>>3];
      $32 = $30 * $31;
      $33 = +HEAPF64[$10>>3];
      $34 = $33 + $32;
      HEAPF64[$10>>3] = $34;
     } else {
      $35 = ($13|0)<(0);
      if ($35) {
       $36 = HEAP32[$wt>>2]|0;
       $37 = ((($36)) + 72|0);
       $38 = HEAP32[$37>>2]|0;
       $39 = (($38) + ($l$01<<3)|0);
       $40 = +HEAPF64[$39>>3];
       $41 = (($11) + ($l$01))|0;
       $42 = (($inp) + ($41<<3)|0);
       $43 = +HEAPF64[$42>>3];
       $44 = $40 * $43;
       $45 = +HEAPF64[$9>>3];
       $46 = $45 + $44;
       HEAPF64[$9>>3] = $46;
       $47 = HEAP32[$wt>>2]|0;
       $48 = ((($47)) + 76|0);
       $49 = HEAP32[$48>>2]|0;
       $50 = (($49) + ($l$01<<3)|0);
       $51 = +HEAPF64[$50>>3];
       $52 = +HEAPF64[$42>>3];
       $53 = $51 * $52;
       $54 = +HEAPF64[$10>>3];
       $55 = $54 + $53;
       HEAPF64[$10>>3] = $55;
       break;
      }
      if (!($15)) {
       $56 = HEAP32[$wt>>2]|0;
       $57 = ((($56)) + 72|0);
       $58 = HEAP32[$57>>2]|0;
       $59 = (($58) + ($l$01<<3)|0);
       $60 = +HEAPF64[$59>>3];
       $61 = (($12) + ($l$01))|0;
       $62 = (($inp) + ($61<<3)|0);
       $63 = +HEAPF64[$62>>3];
       $64 = $60 * $63;
       $65 = +HEAPF64[$9>>3];
       $66 = $65 + $64;
       HEAPF64[$9>>3] = $66;
       $67 = HEAP32[$wt>>2]|0;
       $68 = ((($67)) + 76|0);
       $69 = HEAP32[$68>>2]|0;
       $70 = (($69) + ($l$01<<3)|0);
       $71 = +HEAPF64[$70>>3];
       $72 = +HEAPF64[$62>>3];
       $73 = $71 * $72;
       $74 = +HEAPF64[$10>>3];
       $75 = $74 + $73;
       HEAPF64[$10>>3] = $75;
      }
     }
    } while(0);
    $76 = (($l$01) + 1)|0;
    $exitcond = ($76|0)==($2|0);
    if ($exitcond) {
     break;
    } else {
     $l$01 = $76;
    }
   }
  }
  $77 = (($i$02) + 1)|0;
  $exitcond6 = ($77|0)==($len_cA|0);
  if ($exitcond6) {
   break;
  } else {
   $i$02 = $77;
  }
 }
 return;
}
function _idwt1($wt,$temp,$cA_up,$cA,$len_cA,$cD,$len_cD,$X_lp,$X_hp,$X) {
 $wt = $wt|0;
 $temp = $temp|0;
 $cA_up = $cA_up|0;
 $cA = $cA|0;
 $len_cA = $len_cA|0;
 $cD = $cD|0;
 $len_cD = $len_cD|0;
 $X_lp = $X_lp|0;
 $X_hp = $X_hp|0;
 $X = $X|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0.0, $47 = 0, $48 = 0.0, $49 = 0.0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $7 = 0, $8 = 0, $9 = 0, $exitcond = 0, $i$01 = 0, $vararg_buffer = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0;
 $vararg_buffer = sp;
 $0 = HEAP32[$wt>>2]|0;
 $1 = ((($0)) + 64|0);
 $2 = HEAP32[$1>>2]|0;
 $3 = ((($0)) + 68|0);
 $4 = HEAP32[$3>>2]|0;
 $5 = (($4) + ($2))|0;
 $6 = (($5|0) / 2)&-1;
 $7 = $len_cD << 1;
 (_upsamp2($cA,$len_cA,2,$cA_up)|0);
 $8 = $len_cA << 1;
 $9 = (($5|0) / 4)&-1;
 (_per_ext($cA_up,$8,$9,$temp)|0);
 $10 = (($6) + ($8))|0;
 $11 = HEAP32[$wt>>2]|0;
 $12 = ((($11)) + 64|0);
 $13 = HEAP32[$12>>2]|0;
 $14 = ((($11)) + 68|0);
 $15 = HEAP32[$14>>2]|0;
 $16 = ($13|0)==($15|0);
 do {
  if ($16) {
   $17 = ((($wt)) + 54|0);
   $18 = (_strcmp($17,16192)|0);
   $19 = ($18|0)==(0);
   if (!($19)) {
    $20 = (_strcmp($17,16200)|0);
    $21 = ($20|0)==(0);
    if (!($21)) {
     label = 5;
     break;
    }
   }
   $22 = (_conv_init($10,$6)|0);
   $23 = ((($wt)) + 4|0);
   HEAP32[$23>>2] = $22;
   $24 = ((($wt)) + 68|0);
   HEAP32[$24>>2] = 1;
  } else {
   label = 5;
  }
 } while(0);
 if ((label|0) == 5) {
  $25 = HEAP32[$wt>>2]|0;
  $26 = ((($25)) + 64|0);
  $27 = HEAP32[$26>>2]|0;
  $28 = ((($25)) + 68|0);
  $29 = HEAP32[$28>>2]|0;
  $30 = ($27|0)==($29|0);
  if (!($30)) {
   (_printf((16256|0),($vararg_buffer|0))|0);
   _exit(-1);
   // unreachable;
  }
 }
 $31 = HEAP32[$wt>>2]|0;
 $32 = ((($31)) + 80|0);
 $33 = HEAP32[$32>>2]|0;
 _wconv($wt,$temp,$10,$33,$6,$X_lp);
 (_upsamp2($cD,$len_cD,2,$cA_up)|0);
 (_per_ext($cA_up,$7,$9,$temp)|0);
 $34 = (($6) + ($7))|0;
 $35 = HEAP32[$wt>>2]|0;
 $36 = ((($35)) + 84|0);
 $37 = HEAP32[$36>>2]|0;
 _wconv($wt,$temp,$34,$37,$6,$X_hp);
 $38 = (($34) + -1)|0;
 $39 = ($6|0)>($38|0);
 if (!($39)) {
  $40 = (($6) + -1)|0;
  $41 = (1 - ($6))|0;
  $42 = $len_cD << 1;
  $43 = (($6) + ($42))|0;
  $44 = (($43) + -1)|0;
  $i$01 = $40;
  while(1) {
   $45 = (($X_lp) + ($i$01<<3)|0);
   $46 = +HEAPF64[$45>>3];
   $47 = (($X_hp) + ($i$01<<3)|0);
   $48 = +HEAPF64[$47>>3];
   $49 = $46 + $48;
   $50 = (($41) + ($i$01))|0;
   $51 = (($X) + ($50<<3)|0);
   HEAPF64[$51>>3] = $49;
   $52 = (($i$01) + 1)|0;
   $exitcond = ($52|0)==($44|0);
   if ($exitcond) {
    break;
   } else {
    $i$01 = $52;
   }
  }
 }
 $53 = HEAP32[$wt>>2]|0;
 $54 = ((($53)) + 64|0);
 $55 = HEAP32[$54>>2]|0;
 $56 = ((($53)) + 68|0);
 $57 = HEAP32[$56>>2]|0;
 $58 = ($55|0)==($57|0);
 if (!($58)) {
  STACKTOP = sp;return;
 }
 $59 = ((($wt)) + 54|0);
 $60 = (_strcmp($59,16192)|0);
 $61 = ($60|0)==(0);
 if (!($61)) {
  $62 = (_strcmp($59,16200)|0);
  $63 = ($62|0)==(0);
  if (!($63)) {
   STACKTOP = sp;return;
  }
 }
 $64 = ((($wt)) + 4|0);
 $65 = HEAP32[$64>>2]|0;
 _free_conv($65);
 $66 = ((($wt)) + 68|0);
 HEAP32[$66>>2] = 0;
 STACKTOP = sp;return;
}
function _idwt_per($wt,$cA,$len_cA,$cD,$X) {
 $wt = $wt|0;
 $cA = $cA|0;
 $len_cA = $len_cA|0;
 $cD = $cD|0;
 $X = $X|0;
 var $$not = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0.0, $105 = 0, $106 = 0, $107 = 0.0, $108 = 0.0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0.0, $113 = 0, $114 = 0.0;
 var $115 = 0.0, $116 = 0.0, $117 = 0.0, $118 = 0.0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0.0, $125 = 0.0, $126 = 0.0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0.0, $131 = 0.0, $132 = 0.0;
 var $133 = 0.0, $134 = 0.0, $135 = 0.0, $136 = 0, $137 = 0, $138 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0.0, $31 = 0, $32 = 0.0, $33 = 0.0, $34 = 0, $35 = 0, $36 = 0, $37 = 0.0, $38 = 0, $39 = 0.0, $4 = 0, $40 = 0.0, $41 = 0.0, $42 = 0.0, $43 = 0.0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0.0, $5 = 0, $50 = 0.0, $51 = 0.0, $52 = 0, $53 = 0, $54 = 0, $55 = 0.0, $56 = 0.0, $57 = 0.0, $58 = 0.0, $59 = 0.0, $6 = 0, $60 = 0.0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0.0, $67 = 0, $68 = 0, $69 = 0.0, $7 = 0, $70 = 0.0, $71 = 0, $72 = 0, $73 = 0, $74 = 0.0, $75 = 0, $76 = 0.0, $77 = 0.0, $78 = 0.0, $79 = 0.0, $8 = 0, $80 = 0.0;
 var $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0.0, $87 = 0.0, $88 = 0.0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0.0, $93 = 0.0, $94 = 0.0, $95 = 0.0, $96 = 0.0, $97 = 0.0, $98 = 0, $99 = 0;
 var $exitcond = 0, $i$07 = 0, $l$03 = 0, $m$05 = 0, $n$06 = 0, $or$cond = 0, $or$cond11 = 0, $or$cond2 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[$wt>>2]|0;
 $1 = ((($0)) + 64|0);
 $2 = HEAP32[$1>>2]|0;
 $3 = ((($0)) + 68|0);
 $4 = HEAP32[$3>>2]|0;
 $5 = (($4) + ($2))|0;
 $6 = (($5|0) / 4)&-1;
 $7 = (($len_cA) + -1)|0;
 $8 = (($7) + ($6))|0;
 $9 = ($8|0)>(0);
 if (!($9)) {
  return;
 }
 $10 = (($5|0) / 2)&-1;
 $11 = ($5|0)>(3);
 $12 = (0 - ($6))|0;
 $13 = (($len_cA) + -1)|0;
 $14 = (($13) + ($10))|0;
 $15 = (($6) + ($len_cA))|0;
 $16 = (($15) + -1)|0;
 $i$07 = 0;$m$05 = -2;$n$06 = -1;
 while(1) {
  $17 = (($m$05) + 2)|0;
  $18 = (($n$06) + 2)|0;
  $19 = (($X) + ($17<<3)|0);
  HEAPF64[$19>>3] = 0.0;
  $20 = (($X) + ($18<<3)|0);
  HEAPF64[$20>>3] = 0.0;
  if ($11) {
   $21 = (($i$07) + ($len_cA))|0;
   $l$03 = 0;
   while(1) {
    $22 = $l$03 << 1;
    $23 = (($i$07) - ($l$03))|0;
    $24 = ($23|0)>(-1);
    $25 = ($23|0)<($len_cA|0);
    $or$cond = $24 & $25;
    do {
     if ($or$cond) {
      $26 = HEAP32[$wt>>2]|0;
      $27 = ((($26)) + 80|0);
      $28 = HEAP32[$27>>2]|0;
      $29 = (($28) + ($22<<3)|0);
      $30 = +HEAPF64[$29>>3];
      $31 = (($cA) + ($23<<3)|0);
      $32 = +HEAPF64[$31>>3];
      $33 = $30 * $32;
      $34 = ((($26)) + 84|0);
      $35 = HEAP32[$34>>2]|0;
      $36 = (($35) + ($22<<3)|0);
      $37 = +HEAPF64[$36>>3];
      $38 = (($cD) + ($23<<3)|0);
      $39 = +HEAPF64[$38>>3];
      $40 = $37 * $39;
      $41 = $33 + $40;
      $42 = +HEAPF64[$19>>3];
      $43 = $42 + $41;
      HEAPF64[$19>>3] = $43;
      $44 = $22 | 1;
      $45 = HEAP32[$wt>>2]|0;
      $46 = ((($45)) + 80|0);
      $47 = HEAP32[$46>>2]|0;
      $48 = (($47) + ($44<<3)|0);
      $49 = +HEAPF64[$48>>3];
      $50 = +HEAPF64[$31>>3];
      $51 = $49 * $50;
      $52 = ((($45)) + 84|0);
      $53 = HEAP32[$52>>2]|0;
      $54 = (($53) + ($44<<3)|0);
      $55 = +HEAPF64[$54>>3];
      $56 = +HEAPF64[$38>>3];
      $57 = $55 * $56;
      $58 = $51 + $57;
      $59 = +HEAPF64[$20>>3];
      $60 = $59 + $58;
      HEAPF64[$20>>3] = $60;
     } else {
      $$not = $25 ^ 1;
      $61 = ($23|0)<($14|0);
      $or$cond11 = $61 & $$not;
      if ($or$cond11) {
       $62 = HEAP32[$wt>>2]|0;
       $63 = ((($62)) + 80|0);
       $64 = HEAP32[$63>>2]|0;
       $65 = (($64) + ($22<<3)|0);
       $66 = +HEAPF64[$65>>3];
       $67 = (($23) - ($len_cA))|0;
       $68 = (($cA) + ($67<<3)|0);
       $69 = +HEAPF64[$68>>3];
       $70 = $66 * $69;
       $71 = ((($62)) + 84|0);
       $72 = HEAP32[$71>>2]|0;
       $73 = (($72) + ($22<<3)|0);
       $74 = +HEAPF64[$73>>3];
       $75 = (($cD) + ($67<<3)|0);
       $76 = +HEAPF64[$75>>3];
       $77 = $74 * $76;
       $78 = $70 + $77;
       $79 = +HEAPF64[$19>>3];
       $80 = $79 + $78;
       HEAPF64[$19>>3] = $80;
       $81 = $22 | 1;
       $82 = HEAP32[$wt>>2]|0;
       $83 = ((($82)) + 80|0);
       $84 = HEAP32[$83>>2]|0;
       $85 = (($84) + ($81<<3)|0);
       $86 = +HEAPF64[$85>>3];
       $87 = +HEAPF64[$68>>3];
       $88 = $86 * $87;
       $89 = ((($82)) + 84|0);
       $90 = HEAP32[$89>>2]|0;
       $91 = (($90) + ($81<<3)|0);
       $92 = +HEAPF64[$91>>3];
       $93 = +HEAPF64[$75>>3];
       $94 = $92 * $93;
       $95 = $88 + $94;
       $96 = +HEAPF64[$20>>3];
       $97 = $96 + $95;
       HEAPF64[$20>>3] = $97;
       break;
      }
      $98 = ($23|0)<(0);
      $99 = ($23|0)>($12|0);
      $or$cond2 = $98 & $99;
      if ($or$cond2) {
       $100 = HEAP32[$wt>>2]|0;
       $101 = ((($100)) + 80|0);
       $102 = HEAP32[$101>>2]|0;
       $103 = (($102) + ($22<<3)|0);
       $104 = +HEAPF64[$103>>3];
       $105 = (($21) - ($l$03))|0;
       $106 = (($cA) + ($105<<3)|0);
       $107 = +HEAPF64[$106>>3];
       $108 = $104 * $107;
       $109 = ((($100)) + 84|0);
       $110 = HEAP32[$109>>2]|0;
       $111 = (($110) + ($22<<3)|0);
       $112 = +HEAPF64[$111>>3];
       $113 = (($cD) + ($105<<3)|0);
       $114 = +HEAPF64[$113>>3];
       $115 = $112 * $114;
       $116 = $108 + $115;
       $117 = +HEAPF64[$19>>3];
       $118 = $117 + $116;
       HEAPF64[$19>>3] = $118;
       $119 = $22 | 1;
       $120 = HEAP32[$wt>>2]|0;
       $121 = ((($120)) + 80|0);
       $122 = HEAP32[$121>>2]|0;
       $123 = (($122) + ($119<<3)|0);
       $124 = +HEAPF64[$123>>3];
       $125 = +HEAPF64[$106>>3];
       $126 = $124 * $125;
       $127 = ((($120)) + 84|0);
       $128 = HEAP32[$127>>2]|0;
       $129 = (($128) + ($119<<3)|0);
       $130 = +HEAPF64[$129>>3];
       $131 = +HEAPF64[$113>>3];
       $132 = $130 * $131;
       $133 = $126 + $132;
       $134 = +HEAPF64[$20>>3];
       $135 = $134 + $133;
       HEAPF64[$20>>3] = $135;
      }
     }
    } while(0);
    $136 = (($l$03) + 1)|0;
    $137 = ($136|0)<($6|0);
    if ($137) {
     $l$03 = $136;
    } else {
     break;
    }
   }
  }
  $138 = (($i$07) + 1)|0;
  $exitcond = ($138|0)==($16|0);
  if ($exitcond) {
   break;
  } else {
   $i$07 = $138;$m$05 = $17;$n$06 = $18;
  }
 }
 return;
}
function _idwt_sym($wt,$cA,$len_cA,$cD,$X) {
 $wt = $wt|0;
 $cA = $cA|0;
 $len_cA = $len_cA|0;
 $cD = $cD|0;
 $X = $X|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0.0, $22 = 0, $23 = 0.0, $24 = 0.0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0.0, $29 = 0, $3 = 0, $30 = 0.0, $31 = 0.0, $32 = 0.0, $33 = 0.0, $34 = 0.0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0.0, $41 = 0.0, $42 = 0.0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0.0, $47 = 0.0, $48 = 0.0, $49 = 0.0, $5 = 0, $50 = 0.0, $51 = 0.0, $52 = 0, $53 = 0, $54 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $exitcond = 0, $l$01 = 0, $m$03 = 0, $n$02 = 0, $or$cond = 0;
 var $v$04 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[$wt>>2]|0;
 $1 = ($len_cA|0)>(0);
 if (!($1)) {
  return;
 }
 $2 = ((($0)) + 68|0);
 $3 = HEAP32[$2>>2]|0;
 $4 = ((($0)) + 64|0);
 $5 = HEAP32[$4>>2]|0;
 $6 = (($3) + ($5))|0;
 $7 = (($6|0) / 4)&-1;
 $8 = ($6|0)>(3);
 $m$03 = -2;$n$02 = -1;$v$04 = 0;
 while(1) {
  $9 = (($m$03) + 2)|0;
  $10 = (($n$02) + 2)|0;
  $11 = (($X) + ($9<<3)|0);
  HEAPF64[$11>>3] = 0.0;
  $12 = (($X) + ($10<<3)|0);
  HEAPF64[$12>>3] = 0.0;
  if ($8) {
   $l$01 = 0;
   while(1) {
    $13 = $l$01 << 1;
    $14 = (($v$04) - ($l$01))|0;
    $15 = ($14|0)>(-1);
    $16 = ($14|0)<($len_cA|0);
    $or$cond = $15 & $16;
    if ($or$cond) {
     $17 = HEAP32[$wt>>2]|0;
     $18 = ((($17)) + 80|0);
     $19 = HEAP32[$18>>2]|0;
     $20 = (($19) + ($13<<3)|0);
     $21 = +HEAPF64[$20>>3];
     $22 = (($cA) + ($14<<3)|0);
     $23 = +HEAPF64[$22>>3];
     $24 = $21 * $23;
     $25 = ((($17)) + 84|0);
     $26 = HEAP32[$25>>2]|0;
     $27 = (($26) + ($13<<3)|0);
     $28 = +HEAPF64[$27>>3];
     $29 = (($cD) + ($14<<3)|0);
     $30 = +HEAPF64[$29>>3];
     $31 = $28 * $30;
     $32 = $24 + $31;
     $33 = +HEAPF64[$11>>3];
     $34 = $33 + $32;
     HEAPF64[$11>>3] = $34;
     $35 = $13 | 1;
     $36 = HEAP32[$wt>>2]|0;
     $37 = ((($36)) + 80|0);
     $38 = HEAP32[$37>>2]|0;
     $39 = (($38) + ($35<<3)|0);
     $40 = +HEAPF64[$39>>3];
     $41 = +HEAPF64[$22>>3];
     $42 = $40 * $41;
     $43 = ((($36)) + 84|0);
     $44 = HEAP32[$43>>2]|0;
     $45 = (($44) + ($35<<3)|0);
     $46 = +HEAPF64[$45>>3];
     $47 = +HEAPF64[$29>>3];
     $48 = $46 * $47;
     $49 = $42 + $48;
     $50 = +HEAPF64[$12>>3];
     $51 = $50 + $49;
     HEAPF64[$12>>3] = $51;
    }
    $52 = (($l$01) + 1)|0;
    $53 = ($52|0)<($7|0);
    if ($53) {
     $l$01 = $52;
    } else {
     break;
    }
   }
  }
  $54 = (($v$04) + 1)|0;
  $exitcond = ($54|0)==($len_cA|0);
  if ($exitcond) {
   break;
  } else {
   $m$03 = $9;$n$02 = $10;$v$04 = $54;
  }
 }
 return;
}
function _wconv($wt,$sig,$N,$filt,$L,$oup) {
 $wt = $wt|0;
 $sig = $sig|0;
 $N = $N|0;
 $filt = $filt|0;
 $L = $L|0;
 $oup = $oup|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $vararg_buffer = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0;
 $vararg_buffer = sp;
 $0 = ((($wt)) + 54|0);
 $1 = (_strcmp($0,16184)|0);
 $2 = ($1|0)==(0);
 if ($2) {
  _conv_direct($sig,$N,$filt,$L,$oup);
  STACKTOP = sp;return;
 }
 $3 = (_strcmp($0,16192)|0);
 $4 = ($3|0)==(0);
 if (!($4)) {
  $5 = (_strcmp($0,16200)|0);
  $6 = ($5|0)==(0);
  if (!($6)) {
   (_printf((16360|0),($vararg_buffer|0))|0);
   _exit(-1);
   // unreachable;
  }
 }
 $7 = ((($wt)) + 68|0);
 $8 = HEAP32[$7>>2]|0;
 $9 = ($8|0)==(0);
 if ($9) {
  $10 = (_conv_init($N,$L)|0);
  $11 = ((($wt)) + 4|0);
  HEAP32[$11>>2] = $10;
  _conv_fft($10,$sig,$filt,$oup);
  $12 = HEAP32[$11>>2]|0;
  _free_conv($12);
  STACKTOP = sp;return;
 } else {
  $13 = ((($wt)) + 4|0);
  $14 = HEAP32[$13>>2]|0;
  _conv_fft($14,$sig,$filt,$oup);
  STACKTOP = sp;return;
 }
}
function _swt_direct($wt,$inp) {
 $wt = $wt|0;
 $inp = $inp|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0.0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0.0, $3 = 0, $30 = 0, $31 = 0, $32 = 0.0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $M$13 = 0, $M$2 = 0, $exitcond = 0;
 var $exitcond19 = 0, $exitcond20 = 0, $exitcond22 = 0, $i$08 = 0, $i$11 = 0, $iter$012 = 0, $iter$14 = 0, $lenacc$02 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ((($wt)) + 20|0);
 $1 = HEAP32[$0>>2]|0;
 $2 = ((($wt)) + 32|0);
 $3 = HEAP32[$2>>2]|0;
 $4 = (((($wt)) + 76|0) + ($3<<2)|0);
 HEAP32[$4>>2] = $1;
 $5 = ((($wt)) + 76|0);
 HEAP32[$5>>2] = $1;
 $6 = (($3) + 1)|0;
 $7 = Math_imul($6, $1)|0;
 $8 = (((($wt)) + 76|0) + ($6<<2)|0);
 HEAP32[$8>>2] = $7;
 $9 = ((($wt)) + 24|0);
 HEAP32[$9>>2] = $7;
 $10 = ($3|0)>(1);
 if ($10) {
  $iter$012 = 1;
  while(1) {
   $11 = (((($wt)) + 76|0) + ($iter$012<<2)|0);
   HEAP32[$11>>2] = $1;
   $12 = (($iter$012) + 1)|0;
   $exitcond22 = ($12|0)==($3|0);
   if ($exitcond22) {
    break;
   } else {
    $iter$012 = $12;
   }
  }
 }
 $13 = $1 << 3;
 $14 = (_malloc($13)|0);
 $15 = (_malloc($13)|0);
 $16 = ($1|0)>(0);
 if ($16) {
  $i$08 = 0;
  while(1) {
   $17 = (($inp) + ($i$08<<3)|0);
   $18 = +HEAPF64[$17>>3];
   $19 = (((($wt)) + 488|0) + ($i$08<<3)|0);
   HEAPF64[$19>>3] = $18;
   $20 = (($i$08) + 1)|0;
   $exitcond20 = ($20|0)==($1|0);
   if ($exitcond20) {
    break;
   } else {
    $i$08 = $20;
   }
  }
 }
 $21 = ($3|0)>(0);
 if (!($21)) {
  _free($14);
  _free($15);
  return;
 }
 $22 = HEAP32[$9>>2]|0;
 $23 = ((($wt)) + 488|0);
 $24 = ($1|0)>(0);
 $M$13 = 1;$iter$14 = 0;$lenacc$02 = $22;
 while(1) {
  $25 = (($lenacc$02) - ($1))|0;
  $26 = ($iter$14|0)>(0);
  $27 = $26&1;
  $M$2 = $M$13 << $27;
  _swt_per($wt,$M$2,$23,$1,$14,$1,$15);
  if ($24) {
   $i$11 = 0;
   while(1) {
    $28 = (($14) + ($i$11<<3)|0);
    $29 = +HEAPF64[$28>>3];
    $30 = (((($wt)) + 488|0) + ($i$11<<3)|0);
    HEAPF64[$30>>3] = $29;
    $31 = (($15) + ($i$11<<3)|0);
    $32 = +HEAPF64[$31>>3];
    $33 = (($i$11) + ($25))|0;
    $34 = (((($wt)) + 488|0) + ($33<<3)|0);
    HEAPF64[$34>>3] = $32;
    $35 = (($i$11) + 1)|0;
    $exitcond = ($35|0)==($1|0);
    if ($exitcond) {
     break;
    } else {
     $i$11 = $35;
    }
   }
  }
  $36 = (($iter$14) + 1)|0;
  $exitcond19 = ($36|0)==($3|0);
  if ($exitcond19) {
   break;
  } else {
   $M$13 = $M$2;$iter$14 = $36;$lenacc$02 = $25;
  }
 }
 _free($14);
 _free($15);
 return;
}
function _swt_fft($wt,$inp) {
 $wt = $wt|0;
 $inp = $inp|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0.0, $113 = 0, $114 = 0, $115 = 0.0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0;
 var $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0.0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0;
 var $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0;
 var $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0.0, $74 = 0, $75 = 0, $76 = 0.0, $77 = 0, $78 = 0, $79 = 0, $8 = 0;
 var $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0;
 var $99 = 0, $M$0$lcssa = 0, $M$015 = 0, $M$16 = 0, $M$2 = 0, $N$0 = 0, $exitcond = 0, $exitcond20 = 0, $exitcond24 = 0, $exitcond26 = 0, $i$011 = 0, $i$12 = 0, $i$23 = 0, $iter$016 = 0, $iter$17 = 0, $lenacc$05 = 0, $vararg_buffer = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0;
 $vararg_buffer = sp;
 $0 = ((($wt)) + 20|0);
 $1 = HEAP32[$0>>2]|0;
 $2 = ((($wt)) + 32|0);
 $3 = HEAP32[$2>>2]|0;
 $4 = (((($wt)) + 76|0) + ($3<<2)|0);
 HEAP32[$4>>2] = $1;
 $5 = ((($wt)) + 76|0);
 HEAP32[$5>>2] = $1;
 $6 = (($3) + 1)|0;
 $7 = Math_imul($6, $1)|0;
 $8 = (((($wt)) + 76|0) + ($6<<2)|0);
 HEAP32[$8>>2] = $7;
 $9 = ((($wt)) + 24|0);
 HEAP32[$9>>2] = $7;
 $10 = ($3|0)>(1);
 if ($10) {
  $M$015 = 1;$iter$016 = 1;
  while(1) {
   $11 = $M$015 << 1;
   $12 = (((($wt)) + 76|0) + ($iter$016<<2)|0);
   HEAP32[$12>>2] = $1;
   $13 = (($iter$016) + 1)|0;
   $exitcond26 = ($13|0)==($3|0);
   if ($exitcond26) {
    $M$0$lcssa = $11;
    break;
   } else {
    $M$015 = $11;$iter$016 = $13;
   }
  }
 } else {
  $M$0$lcssa = 1;
 }
 $14 = HEAP32[$wt>>2]|0;
 $15 = ((($14)) + 52|0);
 $16 = HEAP32[$15>>2]|0;
 $17 = $M$0$lcssa << 3;
 $18 = Math_imul($16, $17)|0;
 $19 = (_malloc($18)|0);
 $20 = (_malloc($18)|0);
 $21 = Math_imul($16, $M$0$lcssa)|0;
 $22 = (($21) + ($1))|0;
 $23 = (($1|0) % 2)&-1;
 $24 = (($22) + ($23))|0;
 $25 = $24 << 3;
 $26 = (_malloc($25)|0);
 $27 = $M$0$lcssa << 1;
 $28 = Math_imul($27, $16)|0;
 $29 = (($28) + ($1))|0;
 $30 = (($29) + ($23))|0;
 $31 = $30 << 3;
 $32 = (($31) + -1)|0;
 $33 = (_malloc($32)|0);
 $34 = (_malloc($32)|0);
 $35 = ($1|0)>(0);
 if ($35) {
  $i$011 = 0;
  while(1) {
   $36 = (($inp) + ($i$011<<3)|0);
   $37 = +HEAPF64[$36>>3];
   $38 = (((($wt)) + 488|0) + ($i$011<<3)|0);
   HEAPF64[$38>>3] = $37;
   $39 = (($i$011) + 1)|0;
   $exitcond24 = ($39|0)==($1|0);
   if ($exitcond24) {
    break;
   } else {
    $i$011 = $39;
   }
  }
 }
 $40 = ($3|0)>(0);
 if (!($40)) {
  _free($19);
  _free($20);
  _free($26);
  _free($33);
  _free($34);
  STACKTOP = sp;return;
 }
 $41 = HEAP32[$9>>2]|0;
 $42 = ((($wt)) + 488|0);
 $43 = ((($wt)) + 54|0);
 $44 = (($23) + ($1))|0;
 $45 = ((($wt)) + 4|0);
 $46 = ((($wt)) + 68|0);
 $47 = (($23) + ($1))|0;
 $48 = ((($wt)) + 54|0);
 $49 = ((($wt)) + 4|0);
 $50 = ((($wt)) + 68|0);
 $51 = ($1|0)>(0);
 $52 = ($16|0)>(0);
 $M$16 = 1;$iter$17 = 0;$lenacc$05 = $41;
 while(1) {
  $53 = (($lenacc$05) - ($1))|0;
  $54 = ($iter$17|0)>(0);
  if ($54) {
   $60 = $M$16 << 1;
   $61 = Math_imul($60, $16)|0;
   $62 = HEAP32[$wt>>2]|0;
   $63 = ((($62)) + 72|0);
   $64 = HEAP32[$63>>2]|0;
   $65 = ((($62)) + 56|0);
   $66 = HEAP32[$65>>2]|0;
   (_upsamp2($64,$66,$60,$19)|0);
   $67 = HEAP32[$wt>>2]|0;
   $68 = ((($67)) + 76|0);
   $69 = HEAP32[$68>>2]|0;
   $70 = ((($67)) + 60|0);
   $71 = HEAP32[$70>>2]|0;
   (_upsamp2($69,$71,$60,$20)|0);
   $M$2 = $60;$N$0 = $61;
  } else {
   if ($52) {
    $55 = HEAP32[$wt>>2]|0;
    $56 = ((($55)) + 72|0);
    $57 = HEAP32[$56>>2]|0;
    $58 = ((($55)) + 76|0);
    $59 = HEAP32[$58>>2]|0;
    $i$12 = 0;
    while(1) {
     $72 = (($57) + ($i$12<<3)|0);
     $73 = +HEAPF64[$72>>3];
     $74 = (($19) + ($i$12<<3)|0);
     HEAPF64[$74>>3] = $73;
     $75 = (($59) + ($i$12<<3)|0);
     $76 = +HEAPF64[$75>>3];
     $77 = (($20) + ($i$12<<3)|0);
     HEAPF64[$77>>3] = $76;
     $78 = (($i$12) + 1)|0;
     $exitcond = ($78|0)==($16|0);
     if ($exitcond) {
      $M$2 = $M$16;$N$0 = $16;
      break;
     } else {
      $i$12 = $78;
     }
    }
   } else {
    $M$2 = $M$16;$N$0 = $16;
   }
  }
  $79 = (($N$0|0) / 2)&-1;
  (_per_ext($42,$1,$79,$26)|0);
  $80 = HEAP32[$wt>>2]|0;
  $81 = ((($80)) + 56|0);
  $82 = HEAP32[$81>>2]|0;
  $83 = ((($80)) + 60|0);
  $84 = HEAP32[$83>>2]|0;
  $85 = ($82|0)==($84|0);
  do {
   if ($85) {
    $86 = (_strcmp($43,16192)|0);
    $87 = ($86|0)==(0);
    if (!($87)) {
     $88 = (_strcmp($43,16200)|0);
     $89 = ($88|0)==(0);
     if (!($89)) {
      label = 16;
      break;
     }
    }
    $90 = (($44) + ($N$0))|0;
    $91 = (_conv_init($90,$N$0)|0);
    HEAP32[$45>>2] = $91;
    HEAP32[$46>>2] = 1;
   } else {
    label = 16;
   }
  } while(0);
  if ((label|0) == 16) {
   label = 0;
   $92 = HEAP32[$wt>>2]|0;
   $93 = ((($92)) + 56|0);
   $94 = HEAP32[$93>>2]|0;
   $95 = ((($92)) + 60|0);
   $96 = HEAP32[$95>>2]|0;
   $97 = ($94|0)==($96|0);
   if (!($97)) {
    label = 17;
    break;
   }
  }
  $98 = (($47) + ($N$0))|0;
  _wconv($wt,$26,$98,$19,$N$0,$33);
  _wconv($wt,$26,$98,$20,$N$0,$34);
  $99 = HEAP32[$wt>>2]|0;
  $100 = ((($99)) + 56|0);
  $101 = HEAP32[$100>>2]|0;
  $102 = ((($99)) + 60|0);
  $103 = HEAP32[$102>>2]|0;
  $104 = ($101|0)==($103|0);
  do {
   if ($104) {
    $105 = (_strcmp($48,16192)|0);
    $106 = ($105|0)==(0);
    if (!($106)) {
     $107 = (_strcmp($48,16200)|0);
     $108 = ($107|0)==(0);
     if (!($108)) {
      break;
     }
    }
    $109 = HEAP32[$49>>2]|0;
    _free_conv($109);
    HEAP32[$50>>2] = 0;
   }
  } while(0);
  if ($51) {
   $i$23 = 0;
   while(1) {
    $110 = (($i$23) + ($N$0))|0;
    $111 = (($33) + ($110<<3)|0);
    $112 = +HEAPF64[$111>>3];
    $113 = (((($wt)) + 488|0) + ($i$23<<3)|0);
    HEAPF64[$113>>3] = $112;
    $114 = (($34) + ($110<<3)|0);
    $115 = +HEAPF64[$114>>3];
    $116 = (($i$23) + ($53))|0;
    $117 = (((($wt)) + 488|0) + ($116<<3)|0);
    HEAPF64[$117>>3] = $115;
    $118 = (($i$23) + 1)|0;
    $exitcond20 = ($118|0)==($1|0);
    if ($exitcond20) {
     break;
    } else {
     $i$23 = $118;
    }
   }
  }
  $119 = (($iter$17) + 1)|0;
  $120 = ($119|0)<($3|0);
  if ($120) {
   $M$16 = $M$2;$iter$17 = $119;$lenacc$05 = $53;
  } else {
   label = 25;
   break;
  }
 }
 if ((label|0) == 17) {
  (_printf((16256|0),($vararg_buffer|0))|0);
  _exit(-1);
  // unreachable;
 }
 else if ((label|0) == 25) {
  _free($19);
  _free($20);
  _free($26);
  _free($33);
  _free($34);
  STACKTOP = sp;return;
 }
}
function _modwt_per($wt,$M,$inp,$cA,$len_cA,$cD) {
 $wt = $wt|0;
 $M = $M|0;
 $inp = $inp|0;
 $cA = $cA|0;
 $len_cA = $len_cA|0;
 $cD = $cD|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0.0, $13 = 0, $14 = 0.0, $15 = 0, $16 = 0, $17 = 0.0, $18 = 0.0, $19 = 0, $2 = 0, $20 = 0, $21 = 0.0, $22 = 0.0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0.0, $28 = 0.0, $29 = 0, $3 = 0, $30 = 0.0, $31 = 0.0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0.0, $4 = 0, $40 = 0, $41 = 0.0, $42 = 0.0, $43 = 0.0, $44 = 0.0;
 var $45 = 0, $46 = 0, $47 = 0.0, $48 = 0.0, $49 = 0.0, $5 = 0, $50 = 0.0, $51 = 0.0, $52 = 0, $53 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $exitcond = 0, $exitcond10 = 0, $exitcond11 = 0, $i$07 = 0, $i$14 = 0, $l$03 = 0;
 var $t$02 = 0, $t$1 = 0, $t$2 = 0, $t$2$lcssa = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[$wt>>2]|0;
 $1 = ((($0)) + 56|0);
 $2 = HEAP32[$1>>2]|0;
 $3 = $2 << 4;
 $4 = (_malloc($3)|0);
 $5 = ($2|0)>(0);
 if ($5) {
  $6 = HEAP32[$wt>>2]|0;
  $7 = ((($6)) + 72|0);
  $8 = HEAP32[$7>>2]|0;
  $9 = ((($6)) + 76|0);
  $10 = HEAP32[$9>>2]|0;
  $i$07 = 0;
  while(1) {
   $16 = (($8) + ($i$07<<3)|0);
   $17 = +HEAPF64[$16>>3];
   $18 = $17 / 1.4142135623730951;
   $19 = (($4) + ($i$07<<3)|0);
   HEAPF64[$19>>3] = $18;
   $20 = (($10) + ($i$07<<3)|0);
   $21 = +HEAPF64[$20>>3];
   $22 = $21 / 1.4142135623730951;
   $23 = (($i$07) + ($2))|0;
   $24 = (($4) + ($23<<3)|0);
   HEAPF64[$24>>3] = $22;
   $25 = (($i$07) + 1)|0;
   $exitcond11 = ($25|0)==($2|0);
   if ($exitcond11) {
    break;
   } else {
    $i$07 = $25;
   }
  }
 }
 $11 = ($len_cA|0)>(0);
 if (!($11)) {
  _free($4);
  return;
 }
 $12 = +HEAPF64[$4>>3];
 $13 = (($4) + ($2<<3)|0);
 $14 = +HEAPF64[$13>>3];
 $15 = ($2|0)>(1);
 $i$14 = 0;
 while(1) {
  $26 = (($inp) + ($i$14<<3)|0);
  $27 = +HEAPF64[$26>>3];
  $28 = $12 * $27;
  $29 = (($cA) + ($i$14<<3)|0);
  HEAPF64[$29>>3] = $28;
  $30 = +HEAPF64[$26>>3];
  $31 = $14 * $30;
  $32 = (($cD) + ($i$14<<3)|0);
  HEAPF64[$32>>3] = $31;
  if ($15) {
   $l$03 = 1;$t$02 = $i$14;
   while(1) {
    $33 = (($t$02) - ($M))|0;
    $t$1 = $33;
    while(1) {
     $34 = ($t$1|0)<($len_cA|0);
     $35 = (($t$1) - ($len_cA))|0;
     if ($34) {
      $t$2 = $t$1;
      break;
     } else {
      $t$1 = $35;
     }
    }
    while(1) {
     $36 = ($t$2|0)<(0);
     $37 = (($t$2) + ($len_cA))|0;
     if ($36) {
      $t$2 = $37;
     } else {
      $t$2$lcssa = $t$2;
      break;
     }
    }
    $38 = (($4) + ($l$03<<3)|0);
    $39 = +HEAPF64[$38>>3];
    $40 = (($inp) + ($t$2$lcssa<<3)|0);
    $41 = +HEAPF64[$40>>3];
    $42 = $39 * $41;
    $43 = +HEAPF64[$29>>3];
    $44 = $43 + $42;
    HEAPF64[$29>>3] = $44;
    $45 = (($l$03) + ($2))|0;
    $46 = (($4) + ($45<<3)|0);
    $47 = +HEAPF64[$46>>3];
    $48 = +HEAPF64[$40>>3];
    $49 = $47 * $48;
    $50 = +HEAPF64[$32>>3];
    $51 = $50 + $49;
    HEAPF64[$32>>3] = $51;
    $52 = (($l$03) + 1)|0;
    $exitcond = ($52|0)==($2|0);
    if ($exitcond) {
     break;
    } else {
     $l$03 = $52;$t$02 = $t$2$lcssa;
    }
   }
  }
  $53 = (($i$14) + 1)|0;
  $exitcond10 = ($53|0)==($len_cA|0);
  if ($exitcond10) {
   break;
  } else {
   $i$14 = $53;
  }
 }
 _free($4);
 return;
}
function _imodwt_per($wt,$M,$cA,$len_cA,$cD,$X) {
 $wt = $wt|0;
 $M = $M|0;
 $cA = $cA|0;
 $len_cA = $len_cA|0;
 $cD = $cD|0;
 $X = $X|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0.0, $13 = 0, $14 = 0.0, $15 = 0, $16 = 0, $17 = 0.0, $18 = 0.0, $19 = 0, $2 = 0, $20 = 0, $21 = 0.0, $22 = 0.0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0.0, $28 = 0.0, $29 = 0, $3 = 0, $30 = 0.0, $31 = 0.0, $32 = 0.0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0.0, $41 = 0, $42 = 0.0, $43 = 0.0, $44 = 0;
 var $45 = 0, $46 = 0.0, $47 = 0, $48 = 0.0, $49 = 0.0, $5 = 0, $50 = 0.0, $51 = 0.0, $52 = 0.0, $53 = 0, $54 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $exitcond = 0, $exitcond10 = 0, $exitcond11 = 0, $i$07 = 0, $i$14 = 0;
 var $l$03 = 0, $t$02 = 0, $t$1 = 0, $t$2 = 0, $t$2$lcssa = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[$wt>>2]|0;
 $1 = ((($0)) + 56|0);
 $2 = HEAP32[$1>>2]|0;
 $3 = $2 << 4;
 $4 = (_malloc($3)|0);
 $5 = ($2|0)>(0);
 if ($5) {
  $6 = HEAP32[$wt>>2]|0;
  $7 = ((($6)) + 72|0);
  $8 = HEAP32[$7>>2]|0;
  $9 = ((($6)) + 76|0);
  $10 = HEAP32[$9>>2]|0;
  $i$07 = 0;
  while(1) {
   $16 = (($8) + ($i$07<<3)|0);
   $17 = +HEAPF64[$16>>3];
   $18 = $17 / 1.4142135623730951;
   $19 = (($4) + ($i$07<<3)|0);
   HEAPF64[$19>>3] = $18;
   $20 = (($10) + ($i$07<<3)|0);
   $21 = +HEAPF64[$20>>3];
   $22 = $21 / 1.4142135623730951;
   $23 = (($i$07) + ($2))|0;
   $24 = (($4) + ($23<<3)|0);
   HEAPF64[$24>>3] = $22;
   $25 = (($i$07) + 1)|0;
   $exitcond11 = ($25|0)==($2|0);
   if ($exitcond11) {
    break;
   } else {
    $i$07 = $25;
   }
  }
 }
 $11 = ($len_cA|0)>(0);
 if (!($11)) {
  _free($4);
  return;
 }
 $12 = +HEAPF64[$4>>3];
 $13 = (($4) + ($2<<3)|0);
 $14 = +HEAPF64[$13>>3];
 $15 = ($2|0)>(1);
 $i$14 = 0;
 while(1) {
  $26 = (($cA) + ($i$14<<3)|0);
  $27 = +HEAPF64[$26>>3];
  $28 = $12 * $27;
  $29 = (($cD) + ($i$14<<3)|0);
  $30 = +HEAPF64[$29>>3];
  $31 = $14 * $30;
  $32 = $28 + $31;
  $33 = (($X) + ($i$14<<3)|0);
  HEAPF64[$33>>3] = $32;
  if ($15) {
   $l$03 = 1;$t$02 = $i$14;
   while(1) {
    $34 = (($t$02) + ($M))|0;
    $t$1 = $34;
    while(1) {
     $35 = ($t$1|0)<($len_cA|0);
     $36 = (($t$1) - ($len_cA))|0;
     if ($35) {
      $t$2 = $t$1;
      break;
     } else {
      $t$1 = $36;
     }
    }
    while(1) {
     $37 = ($t$2|0)<(0);
     $38 = (($t$2) + ($len_cA))|0;
     if ($37) {
      $t$2 = $38;
     } else {
      $t$2$lcssa = $t$2;
      break;
     }
    }
    $39 = (($4) + ($l$03<<3)|0);
    $40 = +HEAPF64[$39>>3];
    $41 = (($cA) + ($t$2$lcssa<<3)|0);
    $42 = +HEAPF64[$41>>3];
    $43 = $40 * $42;
    $44 = (($l$03) + ($2))|0;
    $45 = (($4) + ($44<<3)|0);
    $46 = +HEAPF64[$45>>3];
    $47 = (($cD) + ($t$2$lcssa<<3)|0);
    $48 = +HEAPF64[$47>>3];
    $49 = $46 * $48;
    $50 = $43 + $49;
    $51 = +HEAPF64[$33>>3];
    $52 = $51 + $50;
    HEAPF64[$33>>3] = $52;
    $53 = (($l$03) + 1)|0;
    $exitcond = ($53|0)==($2|0);
    if ($exitcond) {
     break;
    } else {
     $l$03 = $53;$t$02 = $t$2$lcssa;
    }
   }
  }
  $54 = (($i$14) + 1)|0;
  $exitcond10 = ($54|0)==($len_cA|0);
  if ($exitcond10) {
   break;
  } else {
   $i$14 = $54;
  }
 }
 _free($4);
 return;
}
function _swt_per($wt,$M,$inp,$N,$cA,$len_cA,$cD) {
 $wt = $wt|0;
 $M = $M|0;
 $inp = $inp|0;
 $N = $N|0;
 $cA = $cA|0;
 $len_cA = $len_cA|0;
 $cD = $cD|0;
 var $$neg = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0.0, $101 = 0.0, $102 = 0.0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0.0, $11 = 0, $110 = 0, $111 = 0, $112 = 0.0, $113 = 0.0, $114 = 0.0;
 var $115 = 0.0, $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0.0, $121 = 0.0, $122 = 0.0, $123 = 0.0, $124 = 0.0, $125 = 0.0, $126 = 0.0, $127 = 0.0, $128 = 0.0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0;
 var $133 = 0.0, $134 = 0.0, $135 = 0.0, $136 = 0.0, $137 = 0.0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0;
 var $25 = 0.0, $26 = 0, $27 = 0.0, $28 = 0.0, $29 = 0.0, $3 = 0, $30 = 0.0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0.0, $36 = 0.0, $37 = 0.0, $38 = 0.0, $39 = 0.0, $4 = 0, $40 = 0, $41 = 0, $42 = 0;
 var $43 = 0, $44 = 0, $45 = 0, $46 = 0.0, $47 = 0, $48 = 0.0, $49 = 0.0, $5 = 0, $50 = 0.0, $51 = 0.0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0.0, $57 = 0.0, $58 = 0.0, $59 = 0.0, $6 = 0, $60 = 0.0;
 var $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0.0, $67 = 0, $68 = 0, $69 = 0.0, $7 = 0, $70 = 0.0, $71 = 0.0, $72 = 0.0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0.0, $78 = 0.0, $79 = 0.0;
 var $8 = 0, $80 = 0.0, $81 = 0.0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0.0, $88 = 0, $89 = 0, $9 = 0, $90 = 0.0, $91 = 0.0, $92 = 0.0, $93 = 0.0, $94 = 0, $95 = 0, $96 = 0, $97 = 0;
 var $98 = 0.0, $99 = 0.0, $exitcond = 0, $i$05 = 0, $j$04 = 0, $j$1 = 0, $j$1$lcssa = 0, $l$03 = 0, $or$cond = 0, $or$cond1 = 0, $or$cond2 = 0, $or$cond3 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[$wt>>2]|0;
 $1 = ((($0)) + 56|0);
 $2 = HEAP32[$1>>2]|0;
 $3 = Math_imul($2, $M)|0;
 $4 = (($3|0) / 2)&-1;
 $5 = (($N|0) % 2)&-1;
 $6 = ($len_cA|0)>(0);
 if (!($6)) {
  return;
 }
 $7 = ($3|0)>(0);
 $8 = ($5|0)==(0);
 $9 = ($5|0)==(1);
 $10 = (($N) + -1)|0;
 $11 = (($inp) + ($10<<3)|0);
 $$neg = $N ^ -1;
 $i$05 = 0;
 while(1) {
  $12 = (($i$05) + ($4))|0;
  $13 = (($cA) + ($i$05<<3)|0);
  HEAPF64[$13>>3] = 0.0;
  $14 = (($cD) + ($i$05<<3)|0);
  HEAPF64[$14>>3] = 0.0;
  if ($7) {
   $j$04 = 0;$l$03 = -1;
   while(1) {
    $j$1 = $j$04;
    while(1) {
     $15 = ($j$1|0)<($len_cA|0);
     $16 = (($j$1) - ($len_cA))|0;
     if ($15) {
      $j$1$lcssa = $j$1;
      break;
     } else {
      $j$1 = $16;
     }
    }
    $17 = (($l$03) + 1)|0;
    $18 = (($12) - ($j$1$lcssa))|0;
    $19 = ($18|0)>=($4|0);
    $20 = ($18|0)<($N|0);
    $or$cond1 = $19 & $20;
    do {
     if ($or$cond1) {
      $21 = HEAP32[$wt>>2]|0;
      $22 = ((($21)) + 72|0);
      $23 = HEAP32[$22>>2]|0;
      $24 = (($23) + ($17<<3)|0);
      $25 = +HEAPF64[$24>>3];
      $26 = (($inp) + ($18<<3)|0);
      $27 = +HEAPF64[$26>>3];
      $28 = $25 * $27;
      $29 = +HEAPF64[$13>>3];
      $30 = $29 + $28;
      HEAPF64[$13>>3] = $30;
      $31 = HEAP32[$wt>>2]|0;
      $32 = ((($31)) + 76|0);
      $33 = HEAP32[$32>>2]|0;
      $34 = (($33) + ($17<<3)|0);
      $35 = +HEAPF64[$34>>3];
      $36 = +HEAPF64[$26>>3];
      $37 = $35 * $36;
      $38 = +HEAPF64[$14>>3];
      $39 = $38 + $37;
      HEAPF64[$14>>3] = $39;
     } else {
      $40 = ($18|0)<($4|0);
      $41 = ($18|0)>(-1);
      $or$cond2 = $40 & $41;
      if ($or$cond2) {
       $42 = HEAP32[$wt>>2]|0;
       $43 = ((($42)) + 72|0);
       $44 = HEAP32[$43>>2]|0;
       $45 = (($44) + ($17<<3)|0);
       $46 = +HEAPF64[$45>>3];
       $47 = (($inp) + ($18<<3)|0);
       $48 = +HEAPF64[$47>>3];
       $49 = $46 * $48;
       $50 = +HEAPF64[$13>>3];
       $51 = $50 + $49;
       HEAPF64[$13>>3] = $51;
       $52 = HEAP32[$wt>>2]|0;
       $53 = ((($52)) + 76|0);
       $54 = HEAP32[$53>>2]|0;
       $55 = (($54) + ($17<<3)|0);
       $56 = +HEAPF64[$55>>3];
       $57 = +HEAPF64[$47>>3];
       $58 = $56 * $57;
       $59 = +HEAPF64[$14>>3];
       $60 = $59 + $58;
       HEAPF64[$14>>3] = $60;
       break;
      }
      $61 = ($18|0)<(0);
      if ($61) {
       $62 = HEAP32[$wt>>2]|0;
       $63 = ((($62)) + 72|0);
       $64 = HEAP32[$63>>2]|0;
       $65 = (($64) + ($17<<3)|0);
       $66 = +HEAPF64[$65>>3];
       $67 = (($18) + ($N))|0;
       $68 = (($inp) + ($67<<3)|0);
       $69 = +HEAPF64[$68>>3];
       $70 = $66 * $69;
       $71 = +HEAPF64[$13>>3];
       $72 = $71 + $70;
       HEAPF64[$13>>3] = $72;
       $73 = HEAP32[$wt>>2]|0;
       $74 = ((($73)) + 76|0);
       $75 = HEAP32[$74>>2]|0;
       $76 = (($75) + ($17<<3)|0);
       $77 = +HEAPF64[$76>>3];
       $78 = +HEAPF64[$68>>3];
       $79 = $77 * $78;
       $80 = +HEAPF64[$14>>3];
       $81 = $80 + $79;
       HEAPF64[$14>>3] = $81;
       break;
      }
      $82 = ($18|0)>=($N|0);
      $or$cond = $8 & $82;
      if ($or$cond) {
       $83 = HEAP32[$wt>>2]|0;
       $84 = ((($83)) + 72|0);
       $85 = HEAP32[$84>>2]|0;
       $86 = (($85) + ($17<<3)|0);
       $87 = +HEAPF64[$86>>3];
       $88 = (($18) - ($N))|0;
       $89 = (($inp) + ($88<<3)|0);
       $90 = +HEAPF64[$89>>3];
       $91 = $87 * $90;
       $92 = +HEAPF64[$13>>3];
       $93 = $92 + $91;
       HEAPF64[$13>>3] = $93;
       $94 = HEAP32[$wt>>2]|0;
       $95 = ((($94)) + 76|0);
       $96 = HEAP32[$95>>2]|0;
       $97 = (($96) + ($17<<3)|0);
       $98 = +HEAPF64[$97>>3];
       $99 = +HEAPF64[$89>>3];
       $100 = $98 * $99;
       $101 = +HEAPF64[$14>>3];
       $102 = $101 + $100;
       HEAPF64[$14>>3] = $102;
       break;
      }
      $or$cond3 = $9 & $82;
      if ($or$cond3) {
       $103 = (($12) - ($17))|0;
       $104 = ($103|0)==($N|0);
       $105 = HEAP32[$wt>>2]|0;
       $106 = ((($105)) + 72|0);
       $107 = HEAP32[$106>>2]|0;
       $108 = (($107) + ($17<<3)|0);
       $109 = +HEAPF64[$108>>3];
       if ($104) {
        $125 = +HEAPF64[$11>>3];
        $126 = $109 * $125;
        $127 = +HEAPF64[$13>>3];
        $128 = $127 + $126;
        HEAPF64[$13>>3] = $128;
        $129 = HEAP32[$wt>>2]|0;
        $130 = ((($129)) + 76|0);
        $131 = HEAP32[$130>>2]|0;
        $132 = (($131) + ($17<<3)|0);
        $133 = +HEAPF64[$132>>3];
        $134 = +HEAPF64[$11>>3];
        $135 = $133 * $134;
        $136 = +HEAPF64[$14>>3];
        $137 = $136 + $135;
        HEAPF64[$14>>3] = $137;
        break;
       } else {
        $110 = (($18) + ($$neg))|0;
        $111 = (($inp) + ($110<<3)|0);
        $112 = +HEAPF64[$111>>3];
        $113 = $109 * $112;
        $114 = +HEAPF64[$13>>3];
        $115 = $114 + $113;
        HEAPF64[$13>>3] = $115;
        $116 = HEAP32[$wt>>2]|0;
        $117 = ((($116)) + 76|0);
        $118 = HEAP32[$117>>2]|0;
        $119 = (($118) + ($17<<3)|0);
        $120 = +HEAPF64[$119>>3];
        $121 = +HEAPF64[$111>>3];
        $122 = $120 * $121;
        $123 = +HEAPF64[$14>>3];
        $124 = $123 + $122;
        HEAPF64[$14>>3] = $124;
        break;
       }
      }
     }
    } while(0);
    $138 = (($j$1$lcssa) + ($M))|0;
    $139 = ($138|0)<($3|0);
    if ($139) {
     $j$04 = $138;$l$03 = $17;
    } else {
     break;
    }
   }
  }
  $140 = (($i$05) + 1)|0;
  $exitcond = ($140|0)==($len_cA|0);
  if ($exitcond) {
   break;
  } else {
   $i$05 = $140;
  }
 }
 return;
}
function _wtoutputlength($N,$method,$lp,$J,$ext) {
 $N = $N|0;
 $method = $method|0;
 $lp = $lp|0;
 $J = $J|0;
 $ext = $ext|0;
 var $$0$lcssa = 0, $$02 = 0, $$1$lcssa = 0, $$16 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0.0, $21 = 0.0, $22 = 0.0;
 var $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $5 = 0;
 var $6 = 0, $7 = 0.0, $8 = 0.0, $9 = 0.0, $M$0$lcssa = 0, $M$04 = 0, $M$1$lcssa = 0, $M$18 = 0, $M$2 = 0, $i$03 = 0, $i$17 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (_strcmp($method,16416)|0);
 $1 = ($0|0)==(0);
 if (!($1)) {
  $2 = (_strcmp($method,16424)|0);
  $3 = ($2|0)==(0);
  if (!($3)) {
   $28 = (_strcmp($method,16448)|0);
   $29 = ($28|0)==(0);
   if (!($29)) {
    $30 = (_strcmp($method,16456)|0);
    $31 = ($30|0)==(0);
    if (!($31)) {
     $34 = (_strcmp($method,16464)|0);
     $35 = ($34|0)==(0);
     if (!($35)) {
      $36 = (_strcmp($method,16472)|0);
      $37 = ($36|0)==(0);
      if (!($37)) {
       $M$2 = 0;
       return ($M$2|0);
      }
     }
     $38 = (($J) + 1)|0;
     $39 = Math_imul($38, $N)|0;
     $M$2 = $39;
     return ($M$2|0);
    }
   }
   $32 = (($J) + 1)|0;
   $33 = Math_imul($32, $N)|0;
   $M$2 = $33;
   return ($M$2|0);
  }
 }
 $4 = (_strcmp($ext,16432)|0);
 $5 = ($4|0)==(0);
 if ($5) {
  $6 = ($J|0)>(0);
  if ($6) {
   $$02 = $N;$M$04 = 0;$i$03 = $J;
   while(1) {
    $7 = (+($$02|0));
    $8 = $7 * 0.5;
    $9 = (+Math_ceil((+$8)));
    $10 = (~~(($9)));
    $11 = (($10) + ($M$04))|0;
    $12 = (($i$03) + -1)|0;
    $13 = ($i$03|0)>(1);
    if ($13) {
     $$02 = $10;$M$04 = $11;$i$03 = $12;
    } else {
     $$0$lcssa = $10;$M$0$lcssa = $11;
     break;
    }
   }
  } else {
   $$0$lcssa = $N;$M$0$lcssa = 0;
  }
  $14 = (($M$0$lcssa) + ($$0$lcssa))|0;
  $M$2 = $14;
  return ($M$2|0);
 }
 $15 = (_strcmp($ext,16440)|0);
 $16 = ($15|0)==(0);
 if (!($16)) {
  $M$2 = 0;
  return ($M$2|0);
 }
 $17 = ($J|0)>(0);
 if ($17) {
  $18 = (($lp) + -2)|0;
  $$16 = $N;$M$18 = 0;$i$17 = $J;
  while(1) {
   $19 = (($18) + ($$16))|0;
   $20 = (+($19|0));
   $21 = $20 * 0.5;
   $22 = (+Math_ceil((+$21)));
   $23 = (~~(($22)));
   $24 = (($23) + ($M$18))|0;
   $25 = (($i$17) + -1)|0;
   $26 = ($i$17|0)>(1);
   if ($26) {
    $$16 = $23;$M$18 = $24;$i$17 = $25;
   } else {
    $$1$lcssa = $23;$M$1$lcssa = $24;
    break;
   }
  }
 } else {
  $$1$lcssa = $N;$M$1$lcssa = 0;
 }
 $27 = (($M$1$lcssa) + ($$1$lcssa))|0;
 $M$2 = $27;
 return ($M$2|0);
}
function _wave_transform($inp,$N,$wname,$method,$J,$ext,$out,$length,$filters) {
 $inp = $inp|0;
 $N = $N|0;
 $wname = $wname|0;
 $method = $method|0;
 $J = $J|0;
 $ext = $ext|0;
 $out = $out|0;
 $length = $length|0;
 $filters = $filters|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0.0, $18 = 0, $19 = 0, $2 = 0, $20 = 0.0, $21 = 0, $22 = 0, $23 = 0, $24 = 0.0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0.0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0.0;
 var $63 = 0, $64 = 0, $65 = 0, $7 = 0, $8 = 0, $9 = 0, $i$05 = 0, $i$13 = 0, $i$22 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (_wave_init($wname)|0);
 $1 = ((($0)) + 52|0);
 $2 = HEAP32[$1>>2]|0;
 $3 = ($2|0)>(0);
 if ($3) {
  $4 = ((($0)) + 72|0);
  $5 = HEAP32[$4>>2]|0;
  $6 = ((($0)) + 76|0);
  $7 = HEAP32[$6>>2]|0;
  $8 = HEAP32[$1>>2]|0;
  $9 = ((($0)) + 80|0);
  $10 = HEAP32[$9>>2]|0;
  $11 = $8 << 1;
  $12 = ((($0)) + 84|0);
  $13 = HEAP32[$12>>2]|0;
  $14 = ($8*3)|0;
  $15 = HEAP32[$1>>2]|0;
  $i$05 = 0;
  while(1) {
   $16 = (($5) + ($i$05<<3)|0);
   $17 = +HEAPF64[$16>>3];
   $18 = (($filters) + ($i$05<<3)|0);
   HEAPF64[$18>>3] = $17;
   $19 = (($7) + ($i$05<<3)|0);
   $20 = +HEAPF64[$19>>3];
   $21 = (($8) + ($i$05))|0;
   $22 = (($filters) + ($21<<3)|0);
   HEAPF64[$22>>3] = $20;
   $23 = (($10) + ($i$05<<3)|0);
   $24 = +HEAPF64[$23>>3];
   $25 = (($11) + ($i$05))|0;
   $26 = (($filters) + ($25<<3)|0);
   HEAPF64[$26>>3] = $24;
   $27 = (($13) + ($i$05<<3)|0);
   $28 = +HEAPF64[$27>>3];
   $29 = (($14) + ($i$05))|0;
   $30 = (($filters) + ($29<<3)|0);
   HEAPF64[$30>>3] = $28;
   $31 = (($i$05) + 1)|0;
   $32 = ($31|0)<($15|0);
   if ($32) {
    $i$05 = $31;
   } else {
    break;
   }
  }
 }
 $33 = (_wt_init($0,$method,$N,$J)|0);
 $34 = (_strcmp($method,16416)|0);
 $35 = ($34|0)==(0);
 do {
  if ($35) {
   label = 6;
  } else {
   $36 = (_strcmp($method,16424)|0);
   $37 = ($36|0)==(0);
   if ($37) {
    label = 6;
   } else {
    $38 = (_strcmp($method,16448)|0);
    $39 = ($38|0)==(0);
    if (!($39)) {
     $40 = (_strcmp($method,16456)|0);
     $41 = ($40|0)==(0);
     if (!($41)) {
      $42 = (_strcmp($method,16464)|0);
      $43 = ($42|0)==(0);
      if (!($43)) {
       $44 = (_strcmp($method,16472)|0);
       $45 = ($44|0)==(0);
       if (!($45)) {
        break;
       }
      }
      _modwt($33,$inp);
      break;
     }
    }
    _swt($33,$inp);
   }
  }
 } while(0);
 if ((label|0) == 6) {
  _setDWTExtension($33,$ext);
  _dwt($33,$inp);
 }
 $46 = ((($33)) + 28|0);
 $47 = HEAP32[$46>>2]|0;
 $48 = ($47|0)>(0);
 if ($48) {
  $i$13 = 0;
  while(1) {
   $55 = (((($33)) + 76|0) + ($i$13<<2)|0);
   $56 = HEAP32[$55>>2]|0;
   $57 = (($length) + ($i$13<<2)|0);
   HEAP32[$57>>2] = $56;
   $58 = (($i$13) + 1)|0;
   $59 = HEAP32[$46>>2]|0;
   $60 = ($58|0)<($59|0);
   if ($60) {
    $i$13 = $58;
   } else {
    break;
   }
  }
 }
 $49 = ((($33)) + 24|0);
 $50 = HEAP32[$49>>2]|0;
 $51 = ($50|0)>(0);
 if (!($51)) {
  _wave_free($0);
  _wt_free($33);
  return;
 }
 $52 = ((($33)) + 484|0);
 $53 = HEAP32[$52>>2]|0;
 $54 = HEAP32[$49>>2]|0;
 $i$22 = 0;
 while(1) {
  $61 = (($53) + ($i$22<<3)|0);
  $62 = +HEAPF64[$61>>3];
  $63 = (($out) + ($i$22<<3)|0);
  HEAPF64[$63>>3] = $62;
  $64 = (($i$22) + 1)|0;
  $65 = ($64|0)<($54|0);
  if ($65) {
   $i$22 = $64;
  } else {
   break;
  }
 }
 _wave_free($0);
 _wt_free($33);
 return;
}
function _inv_wave_transform($inp,$N,$wname,$method,$J,$ext,$out,$outlength,$length,$lenlength) {
 $inp = $inp|0;
 $N = $N|0;
 $wname = $wname|0;
 $method = $method|0;
 $J = $J|0;
 $ext = $ext|0;
 $out = $out|0;
 $outlength = $outlength|0;
 $length = $length|0;
 $lenlength = $lenlength|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $3 = 0, $4 = 0, $5 = 0, $6 = 0.0, $7 = 0, $8 = 0, $9 = 0, $exitcond = 0, $exitcond7 = 0, $i$02 = 0, $i$11 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (_wave_init($wname)|0);
 $1 = (_wt_init($0,$method,$outlength,$J)|0);
 $2 = ($N|0)>(0);
 if ($2) {
  $3 = ((($1)) + 484|0);
  $4 = HEAP32[$3>>2]|0;
  $i$02 = 0;
  while(1) {
   $5 = (($inp) + ($i$02<<3)|0);
   $6 = +HEAPF64[$5>>3];
   $7 = (($4) + ($i$02<<3)|0);
   HEAPF64[$7>>3] = $6;
   $8 = (($i$02) + 1)|0;
   $exitcond7 = ($8|0)==($N|0);
   if ($exitcond7) {
    break;
   } else {
    $i$02 = $8;
   }
  }
 }
 $9 = ((($1)) + 28|0);
 HEAP32[$9>>2] = $lenlength;
 $10 = ($lenlength|0)>(0);
 if ($10) {
  $i$11 = 0;
  while(1) {
   $11 = (($length) + ($i$11<<2)|0);
   $12 = HEAP32[$11>>2]|0;
   $13 = (((($1)) + 76|0) + ($i$11<<2)|0);
   HEAP32[$13>>2] = $12;
   $14 = (($i$11) + 1)|0;
   $exitcond = ($14|0)==($lenlength|0);
   if ($exitcond) {
    break;
   } else {
    $i$11 = $14;
   }
  }
 }
 $15 = (_strcmp($method,16416)|0);
 $16 = ($15|0)==(0);
 if (!($16)) {
  $17 = (_strcmp($method,16424)|0);
  $18 = ($17|0)==(0);
  if (!($18)) {
   $19 = (_strcmp($method,16448)|0);
   $20 = ($19|0)==(0);
   if (!($20)) {
    $21 = (_strcmp($method,16456)|0);
    $22 = ($21|0)==(0);
    if (!($22)) {
     $23 = (_strcmp($method,16464)|0);
     $24 = ($23|0)==(0);
     if (!($24)) {
      $25 = (_strcmp($method,16472)|0);
      $26 = ($25|0)==(0);
      if (!($26)) {
       _wave_free($0);
       _wt_free($1);
       return;
      }
     }
     _imodwt($1,$out);
     _wave_free($0);
     _wt_free($1);
     return;
    }
   }
   _iswt($1,$out);
   _wave_free($0);
   _wt_free($1);
   return;
  }
 }
 _setDWTExtension($1,$ext);
 _idwt($1,$out);
 _wave_free($0);
 _wt_free($1);
 return;
}
function _ldexp($x,$n) {
 $x = +$x;
 $n = $n|0;
 var $0 = 0.0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (+_scalbn($x,$n));
 return (+$0);
}
function _strchr($s,$c) {
 $s = $s|0;
 $c = $c|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (___strchrnul($s,$c)|0);
 $1 = HEAP8[$0>>0]|0;
 $2 = $c&255;
 $3 = ($1<<24>>24)==($2<<24>>24);
 $4 = $3 ? $0 : 0;
 return ($4|0);
}
function ___strchrnul($s,$c) {
 $s = $s|0;
 $c = $c|0;
 var $$0 = 0, $$02$lcssa = 0, $$0211 = 0, $$1 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0;
 var $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0;
 var $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $or$cond = 0, $or$cond5 = 0, $w$0$lcssa = 0, $w$08 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = $c & 255;
 $1 = ($0|0)==(0);
 if ($1) {
  $6 = (_strlen(($s|0))|0);
  $7 = (($s) + ($6)|0);
  $$0 = $7;
  return ($$0|0);
 }
 $2 = $s;
 $3 = $2 & 3;
 $4 = ($3|0)==(0);
 L5: do {
  if ($4) {
   $$02$lcssa = $s;
  } else {
   $5 = $c&255;
   $$0211 = $s;
   while(1) {
    $8 = HEAP8[$$0211>>0]|0;
    $9 = ($8<<24>>24)==(0);
    $10 = ($8<<24>>24)==($5<<24>>24);
    $or$cond = $9 | $10;
    if ($or$cond) {
     $$0 = $$0211;
     break;
    }
    $11 = ((($$0211)) + 1|0);
    $12 = $11;
    $13 = $12 & 3;
    $14 = ($13|0)==(0);
    if ($14) {
     $$02$lcssa = $11;
     break L5;
    } else {
     $$0211 = $11;
    }
   }
   return ($$0|0);
  }
 } while(0);
 $15 = Math_imul($0, 16843009)|0;
 $16 = HEAP32[$$02$lcssa>>2]|0;
 $17 = (($16) + -16843009)|0;
 $18 = $16 & -2139062144;
 $19 = $18 ^ -2139062144;
 $20 = $19 & $17;
 $21 = ($20|0)==(0);
 L12: do {
  if ($21) {
   $23 = $16;$w$08 = $$02$lcssa;
   while(1) {
    $22 = $23 ^ $15;
    $24 = (($22) + -16843009)|0;
    $25 = $22 & -2139062144;
    $26 = $25 ^ -2139062144;
    $27 = $26 & $24;
    $28 = ($27|0)==(0);
    if (!($28)) {
     $w$0$lcssa = $w$08;
     break L12;
    }
    $29 = ((($w$08)) + 4|0);
    $30 = HEAP32[$29>>2]|0;
    $31 = (($30) + -16843009)|0;
    $32 = $30 & -2139062144;
    $33 = $32 ^ -2139062144;
    $34 = $33 & $31;
    $35 = ($34|0)==(0);
    if ($35) {
     $23 = $30;$w$08 = $29;
    } else {
     $w$0$lcssa = $29;
     break;
    }
   }
  } else {
   $w$0$lcssa = $$02$lcssa;
  }
 } while(0);
 $36 = $c&255;
 $$1 = $w$0$lcssa;
 while(1) {
  $37 = HEAP8[$$1>>0]|0;
  $38 = ($37<<24>>24)==(0);
  $39 = ($37<<24>>24)==($36<<24>>24);
  $or$cond5 = $38 | $39;
  $40 = ((($$1)) + 1|0);
  if ($or$cond5) {
   $$0 = $$1;
   break;
  } else {
   $$1 = $40;
  }
 }
 return ($$0|0);
}
function _strstr($h,$n) {
 $h = $h|0;
 $n = $n|0;
 var $$0 = 0, $$0$i = 0, $$0$lcssa$i = 0, $$0$lcssa$i13 = 0, $$01$i = 0, $$02$i = 0, $$02$i9 = 0, $$03$i = 0, $$03$us$i = 0, $$lcssa$i = 0, $$lcssa$i12 = 0, $$lcssa$i6 = 0, $$lcssa303 = 0, $$lcssa306 = 0, $$lcssa309 = 0, $$lcssa323 = 0, $$lcssa326 = 0, $$lcssa329 = 0, $$lcssa344 = 0, $$pr$i = 0;
 var $$pr$us$i = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0;
 var $115 = 0, $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0;
 var $133 = 0, $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0;
 var $151 = 0, $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0;
 var $17 = 0, $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0;
 var $188 = 0, $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0;
 var $205 = 0, $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0, $220 = 0, $221 = 0, $222 = 0;
 var $223 = 0, $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0;
 var $241 = 0, $242 = 0, $243 = 0, $244 = 0, $245 = 0, $246 = 0, $247 = 0, $248 = 0, $249 = 0, $25 = 0, $250 = 0, $251 = 0, $252 = 0, $253 = 0, $254 = 0, $255 = 0, $256 = 0, $257 = 0, $258 = 0, $259 = 0;
 var $26 = 0, $260 = 0, $261 = 0, $262 = 0, $263 = 0, $264 = 0, $265 = 0, $266 = 0, $267 = 0, $268 = 0, $269 = 0, $27 = 0, $270 = 0, $271 = 0, $272 = 0, $273 = 0, $274 = 0, $275 = 0, $276 = 0, $277 = 0;
 var $278 = 0, $279 = 0, $28 = 0, $280 = 0, $281 = 0, $281$phi = 0, $282 = 0, $283 = 0, $284 = 0, $285 = 0, $286 = 0, $287 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0;
 var $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0;
 var $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0;
 var $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0;
 var $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $byteset$i = 0, $div$i = 0, $div$us$i = 0, $div4$i = 0, $hw$0$in2$i = 0, $hw$03$i = 0, $hw$03$i8 = 0, $ip$0$ph$lcssa$i = 0, $ip$0$ph$lcssa147$i = 0, $ip$0$ph76$i = 0;
 var $ip$1$ip$0$$i = 0, $ip$1$ip$0$i = 0, $ip$1$ph$lcssa$i = 0, $ip$1$ph55$i = 0, $jp$0$ph13$ph70$i = 0, $jp$0$ph1365$i = 0, $jp$0$ph1365$i$lcssa = 0, $jp$0$ph1365$i$lcssa$lcssa = 0, $jp$0$ph77$i = 0, $jp$1$ph56$i = 0, $jp$1$ph9$ph49$i = 0, $jp$1$ph944$i = 0, $jp$1$ph944$i$lcssa = 0, $jp$1$ph944$i$lcssa$lcssa = 0, $k$059$i = 0, $k$139$i = 0, $k$2$us$i = 0, $k$338$i = 0, $k$338$i$lcssa = 0, $k$338$us$i = 0;
 var $k$338$us$i$lcssa = 0, $k$4$i = 0, $k$4$us$i = 0, $l$080$i = 0, $l$080$i$lcssa343 = 0, $mem$0$us$i = 0, $or$cond$i = 0, $or$cond$i10 = 0, $or$cond5$us$i = 0, $p$0$ph$ph$lcssa32$i = 0, $p$0$ph$ph$lcssa32151$i = 0, $p$0$ph$ph71$i = 0, $p$1$p$0$i = 0, $p$1$ph$ph$lcssa23$i = 0, $p$1$ph$ph50$i = 0, $p$3155$i = 0, $shift$i = 0, $z$0$i = 0, $z$0$us$i = 0, $z$1$i = 0;
 var $z$1$us$i = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 1056|0;
 $byteset$i = sp + 1024|0;
 $shift$i = sp;
 $0 = HEAP8[$n>>0]|0;
 $1 = ($0<<24>>24)==(0);
 if ($1) {
  $$0 = $h;
  STACKTOP = sp;return ($$0|0);
 }
 $2 = $0 << 24 >> 24;
 $3 = (_strchr($h,$2)|0);
 $4 = ($3|0)==(0|0);
 if ($4) {
  $$0 = 0;
  STACKTOP = sp;return ($$0|0);
 }
 $5 = ((($n)) + 1|0);
 $6 = HEAP8[$5>>0]|0;
 $7 = ($6<<24>>24)==(0);
 if ($7) {
  $$0 = $3;
  STACKTOP = sp;return ($$0|0);
 }
 $8 = ((($3)) + 1|0);
 $9 = HEAP8[$8>>0]|0;
 $10 = ($9<<24>>24)==(0);
 if ($10) {
  $$0 = 0;
  STACKTOP = sp;return ($$0|0);
 }
 $11 = ((($n)) + 2|0);
 $12 = HEAP8[$11>>0]|0;
 $13 = ($12<<24>>24)==(0);
 if ($13) {
  $14 = $0&255;
  $15 = $14 << 8;
  $16 = $6&255;
  $17 = $16 | $15;
  $18 = HEAP8[$3>>0]|0;
  $19 = $18&255;
  $20 = $19 << 8;
  $21 = $9&255;
  $22 = $20 | $21;
  $$01$i = $8;$280 = $9;$281 = $3;$hw$0$in2$i = $22;
  while(1) {
   $23 = $hw$0$in2$i & 65535;
   $24 = ($23|0)==($17|0);
   if ($24) {
    $$lcssa$i = $281;$31 = $280;
    break;
   }
   $25 = $23 << 8;
   $26 = ((($$01$i)) + 1|0);
   $27 = HEAP8[$26>>0]|0;
   $28 = $27&255;
   $29 = $28 | $25;
   $30 = ($27<<24>>24)==(0);
   if ($30) {
    $$lcssa$i = $$01$i;$31 = 0;
    break;
   } else {
    $281$phi = $$01$i;$$01$i = $26;$280 = $27;$hw$0$in2$i = $29;$281 = $281$phi;
   }
  }
  $32 = ($31<<24>>24)!=(0);
  $33 = $32 ? $$lcssa$i : 0;
  $$0 = $33;
  STACKTOP = sp;return ($$0|0);
 }
 $34 = ((($3)) + 2|0);
 $35 = HEAP8[$34>>0]|0;
 $36 = ($35<<24>>24)==(0);
 if ($36) {
  $$0 = 0;
  STACKTOP = sp;return ($$0|0);
 }
 $37 = ((($n)) + 3|0);
 $38 = HEAP8[$37>>0]|0;
 $39 = ($38<<24>>24)==(0);
 if ($39) {
  $40 = $0&255;
  $41 = $40 << 24;
  $42 = $6&255;
  $43 = $42 << 16;
  $44 = $43 | $41;
  $45 = $12&255;
  $46 = $45 << 8;
  $47 = $44 | $46;
  $48 = HEAP8[$3>>0]|0;
  $49 = $48&255;
  $50 = $49 << 24;
  $51 = $9&255;
  $52 = $51 << 16;
  $53 = $35&255;
  $54 = $53 << 8;
  $55 = $54 | $52;
  $56 = $55 | $50;
  $57 = ($56|0)==($47|0);
  if ($57) {
   $$0$lcssa$i = $34;$$lcssa$i6 = $35;
  } else {
   $$02$i = $34;$hw$03$i = $56;
   while(1) {
    $58 = ((($$02$i)) + 1|0);
    $59 = HEAP8[$58>>0]|0;
    $60 = $59&255;
    $61 = $60 | $hw$03$i;
    $62 = $61 << 8;
    $63 = ($59<<24>>24)==(0);
    $64 = ($62|0)==($47|0);
    $or$cond$i = $63 | $64;
    if ($or$cond$i) {
     $$0$lcssa$i = $58;$$lcssa$i6 = $59;
     break;
    } else {
     $$02$i = $58;$hw$03$i = $62;
    }
   }
  }
  $65 = ($$lcssa$i6<<24>>24)!=(0);
  $66 = ((($$0$lcssa$i)) + -2|0);
  $67 = $65 ? $66 : 0;
  $$0 = $67;
  STACKTOP = sp;return ($$0|0);
 }
 $68 = ((($3)) + 3|0);
 $69 = HEAP8[$68>>0]|0;
 $70 = ($69<<24>>24)==(0);
 if ($70) {
  $$0 = 0;
  STACKTOP = sp;return ($$0|0);
 }
 $71 = ((($n)) + 4|0);
 $72 = HEAP8[$71>>0]|0;
 $73 = ($72<<24>>24)==(0);
 if ($73) {
  $74 = $0&255;
  $75 = $74 << 24;
  $76 = $6&255;
  $77 = $76 << 16;
  $78 = $77 | $75;
  $79 = $12&255;
  $80 = $79 << 8;
  $81 = $78 | $80;
  $82 = $38&255;
  $83 = $81 | $82;
  $84 = HEAP8[$3>>0]|0;
  $85 = $84&255;
  $86 = $85 << 24;
  $87 = $9&255;
  $88 = $87 << 16;
  $89 = $35&255;
  $90 = $89 << 8;
  $91 = $69&255;
  $92 = $90 | $88;
  $93 = $92 | $91;
  $94 = $93 | $86;
  $95 = ($94|0)==($83|0);
  if ($95) {
   $$0$lcssa$i13 = $68;$$lcssa$i12 = $69;
  } else {
   $$02$i9 = $68;$hw$03$i8 = $94;
   while(1) {
    $96 = $hw$03$i8 << 8;
    $97 = ((($$02$i9)) + 1|0);
    $98 = HEAP8[$97>>0]|0;
    $99 = $98&255;
    $100 = $99 | $96;
    $101 = ($98<<24>>24)==(0);
    $102 = ($100|0)==($83|0);
    $or$cond$i10 = $101 | $102;
    if ($or$cond$i10) {
     $$0$lcssa$i13 = $97;$$lcssa$i12 = $98;
     break;
    } else {
     $$02$i9 = $97;$hw$03$i8 = $100;
    }
   }
  }
  $103 = ($$lcssa$i12<<24>>24)!=(0);
  $104 = ((($$0$lcssa$i13)) + -3|0);
  $105 = $103 ? $104 : 0;
  $$0 = $105;
  STACKTOP = sp;return ($$0|0);
 }
 ;HEAP32[$byteset$i>>2]=0|0;HEAP32[$byteset$i+4>>2]=0|0;HEAP32[$byteset$i+8>>2]=0|0;HEAP32[$byteset$i+12>>2]=0|0;HEAP32[$byteset$i+16>>2]=0|0;HEAP32[$byteset$i+20>>2]=0|0;HEAP32[$byteset$i+24>>2]=0|0;HEAP32[$byteset$i+28>>2]=0|0;
 $110 = $0;$l$080$i = 0;
 while(1) {
  $106 = (($3) + ($l$080$i)|0);
  $107 = HEAP8[$106>>0]|0;
  $108 = ($107<<24>>24)==(0);
  if ($108) {
   $$0$i = 0;
   break;
  }
  $109 = $110 & 31;
  $111 = $109&255;
  $112 = 1 << $111;
  $div4$i = ($110&255) >>> 5;
  $113 = $div4$i&255;
  $114 = (($byteset$i) + ($113<<2)|0);
  $115 = HEAP32[$114>>2]|0;
  $116 = $115 | $112;
  HEAP32[$114>>2] = $116;
  $117 = (($l$080$i) + 1)|0;
  $118 = $110&255;
  $119 = (($shift$i) + ($118<<2)|0);
  HEAP32[$119>>2] = $117;
  $120 = (($n) + ($117)|0);
  $121 = HEAP8[$120>>0]|0;
  $122 = ($121<<24>>24)==(0);
  if ($122) {
   $$lcssa344 = $117;$l$080$i$lcssa343 = $l$080$i;
   label = 23;
   break;
  } else {
   $110 = $121;$l$080$i = $117;
  }
 }
 L46: do {
  if ((label|0) == 23) {
   $123 = ($$lcssa344>>>0)>(1);
   L48: do {
    if ($123) {
     $282 = 1;$ip$0$ph76$i = -1;$jp$0$ph77$i = 0;
     L49: while(1) {
      $283 = $282;$jp$0$ph13$ph70$i = $jp$0$ph77$i;$p$0$ph$ph71$i = 1;
      while(1) {
       $284 = $283;$jp$0$ph1365$i = $jp$0$ph13$ph70$i;
       L53: while(1) {
        $133 = $284;$k$059$i = 1;
        while(1) {
         $129 = (($k$059$i) + ($ip$0$ph76$i))|0;
         $130 = (($n) + ($129)|0);
         $131 = HEAP8[$130>>0]|0;
         $132 = (($n) + ($133)|0);
         $134 = HEAP8[$132>>0]|0;
         $135 = ($131<<24>>24)==($134<<24>>24);
         if (!($135)) {
          $$lcssa323 = $133;$$lcssa326 = $131;$$lcssa329 = $134;$jp$0$ph1365$i$lcssa = $jp$0$ph1365$i;
          break L53;
         }
         $136 = ($k$059$i|0)==($p$0$ph$ph71$i|0);
         $127 = (($k$059$i) + 1)|0;
         if ($136) {
          break;
         }
         $126 = (($127) + ($jp$0$ph1365$i))|0;
         $128 = ($126>>>0)<($$lcssa344>>>0);
         if ($128) {
          $133 = $126;$k$059$i = $127;
         } else {
          $ip$0$ph$lcssa$i = $ip$0$ph76$i;$p$0$ph$ph$lcssa32$i = $p$0$ph$ph71$i;
          break L49;
         }
        }
        $137 = (($jp$0$ph1365$i) + ($p$0$ph$ph71$i))|0;
        $138 = (($137) + 1)|0;
        $139 = ($138>>>0)<($$lcssa344>>>0);
        if ($139) {
         $284 = $138;$jp$0$ph1365$i = $137;
        } else {
         $ip$0$ph$lcssa$i = $ip$0$ph76$i;$p$0$ph$ph$lcssa32$i = $p$0$ph$ph71$i;
         break L49;
        }
       }
       $140 = ($$lcssa326&255)>($$lcssa329&255);
       $141 = (($$lcssa323) - ($ip$0$ph76$i))|0;
       if (!($140)) {
        $jp$0$ph1365$i$lcssa$lcssa = $jp$0$ph1365$i$lcssa;
        break;
       }
       $124 = (($$lcssa323) + 1)|0;
       $125 = ($124>>>0)<($$lcssa344>>>0);
       if ($125) {
        $283 = $124;$jp$0$ph13$ph70$i = $$lcssa323;$p$0$ph$ph71$i = $141;
       } else {
        $ip$0$ph$lcssa$i = $ip$0$ph76$i;$p$0$ph$ph$lcssa32$i = $141;
        break L49;
       }
      }
      $142 = (($jp$0$ph1365$i$lcssa$lcssa) + 1)|0;
      $143 = (($jp$0$ph1365$i$lcssa$lcssa) + 2)|0;
      $144 = ($143>>>0)<($$lcssa344>>>0);
      if ($144) {
       $282 = $143;$ip$0$ph76$i = $jp$0$ph1365$i$lcssa$lcssa;$jp$0$ph77$i = $142;
      } else {
       $ip$0$ph$lcssa$i = $jp$0$ph1365$i$lcssa$lcssa;$p$0$ph$ph$lcssa32$i = 1;
       break;
      }
     }
     $285 = 1;$ip$1$ph55$i = -1;$jp$1$ph56$i = 0;
     while(1) {
      $287 = $285;$jp$1$ph9$ph49$i = $jp$1$ph56$i;$p$1$ph$ph50$i = 1;
      while(1) {
       $286 = $287;$jp$1$ph944$i = $jp$1$ph9$ph49$i;
       L68: while(1) {
        $152 = $286;$k$139$i = 1;
        while(1) {
         $148 = (($k$139$i) + ($ip$1$ph55$i))|0;
         $149 = (($n) + ($148)|0);
         $150 = HEAP8[$149>>0]|0;
         $151 = (($n) + ($152)|0);
         $153 = HEAP8[$151>>0]|0;
         $154 = ($150<<24>>24)==($153<<24>>24);
         if (!($154)) {
          $$lcssa303 = $152;$$lcssa306 = $150;$$lcssa309 = $153;$jp$1$ph944$i$lcssa = $jp$1$ph944$i;
          break L68;
         }
         $155 = ($k$139$i|0)==($p$1$ph$ph50$i|0);
         $146 = (($k$139$i) + 1)|0;
         if ($155) {
          break;
         }
         $145 = (($146) + ($jp$1$ph944$i))|0;
         $147 = ($145>>>0)<($$lcssa344>>>0);
         if ($147) {
          $152 = $145;$k$139$i = $146;
         } else {
          $ip$0$ph$lcssa147$i = $ip$0$ph$lcssa$i;$ip$1$ph$lcssa$i = $ip$1$ph55$i;$p$0$ph$ph$lcssa32151$i = $p$0$ph$ph$lcssa32$i;$p$1$ph$ph$lcssa23$i = $p$1$ph$ph50$i;
          break L48;
         }
        }
        $156 = (($jp$1$ph944$i) + ($p$1$ph$ph50$i))|0;
        $157 = (($156) + 1)|0;
        $158 = ($157>>>0)<($$lcssa344>>>0);
        if ($158) {
         $286 = $157;$jp$1$ph944$i = $156;
        } else {
         $ip$0$ph$lcssa147$i = $ip$0$ph$lcssa$i;$ip$1$ph$lcssa$i = $ip$1$ph55$i;$p$0$ph$ph$lcssa32151$i = $p$0$ph$ph$lcssa32$i;$p$1$ph$ph$lcssa23$i = $p$1$ph$ph50$i;
         break L48;
        }
       }
       $159 = ($$lcssa306&255)<($$lcssa309&255);
       $160 = (($$lcssa303) - ($ip$1$ph55$i))|0;
       if (!($159)) {
        $jp$1$ph944$i$lcssa$lcssa = $jp$1$ph944$i$lcssa;
        break;
       }
       $164 = (($$lcssa303) + 1)|0;
       $165 = ($164>>>0)<($$lcssa344>>>0);
       if ($165) {
        $287 = $164;$jp$1$ph9$ph49$i = $$lcssa303;$p$1$ph$ph50$i = $160;
       } else {
        $ip$0$ph$lcssa147$i = $ip$0$ph$lcssa$i;$ip$1$ph$lcssa$i = $ip$1$ph55$i;$p$0$ph$ph$lcssa32151$i = $p$0$ph$ph$lcssa32$i;$p$1$ph$ph$lcssa23$i = $160;
        break L48;
       }
      }
      $161 = (($jp$1$ph944$i$lcssa$lcssa) + 1)|0;
      $162 = (($jp$1$ph944$i$lcssa$lcssa) + 2)|0;
      $163 = ($162>>>0)<($$lcssa344>>>0);
      if ($163) {
       $285 = $162;$ip$1$ph55$i = $jp$1$ph944$i$lcssa$lcssa;$jp$1$ph56$i = $161;
      } else {
       $ip$0$ph$lcssa147$i = $ip$0$ph$lcssa$i;$ip$1$ph$lcssa$i = $jp$1$ph944$i$lcssa$lcssa;$p$0$ph$ph$lcssa32151$i = $p$0$ph$ph$lcssa32$i;$p$1$ph$ph$lcssa23$i = 1;
       break;
      }
     }
    } else {
     $ip$0$ph$lcssa147$i = -1;$ip$1$ph$lcssa$i = -1;$p$0$ph$ph$lcssa32151$i = 1;$p$1$ph$ph$lcssa23$i = 1;
    }
   } while(0);
   $166 = (($ip$1$ph$lcssa$i) + 1)|0;
   $167 = (($ip$0$ph$lcssa147$i) + 1)|0;
   $168 = ($166>>>0)>($167>>>0);
   $p$1$p$0$i = $168 ? $p$1$ph$ph$lcssa23$i : $p$0$ph$ph$lcssa32151$i;
   $ip$1$ip$0$i = $168 ? $ip$1$ph$lcssa$i : $ip$0$ph$lcssa147$i;
   $169 = (($n) + ($p$1$p$0$i)|0);
   $170 = (($ip$1$ip$0$i) + 1)|0;
   $171 = (_memcmp($n,$169,$170)|0);
   $172 = ($171|0)==(0);
   if ($172) {
    $178 = (($$lcssa344) - ($p$1$p$0$i))|0;
    $179 = $$lcssa344 | 63;
    $180 = ($$lcssa344|0)==($p$1$p$0$i|0);
    if ($180) {
     $237 = $179;$p$3155$i = $$lcssa344;
    } else {
     $$03$us$i = $3;$mem$0$us$i = 0;$z$0$us$i = $3;
     L82: while(1) {
      $182 = $z$0$us$i;
      $183 = $$03$us$i;
      $184 = (($182) - ($183))|0;
      $185 = ($184>>>0)<($$lcssa344>>>0);
      do {
       if ($185) {
        $186 = (_memchr($z$0$us$i,0,$179)|0);
        $187 = ($186|0)==(0|0);
        if ($187) {
         $191 = (($z$0$us$i) + ($179)|0);
         $z$1$us$i = $191;
         break;
        } else {
         $188 = $186;
         $189 = (($188) - ($183))|0;
         $190 = ($189>>>0)<($$lcssa344>>>0);
         if ($190) {
          $$0$i = 0;
          break L46;
         } else {
          $z$1$us$i = $186;
          break;
         }
        }
       } else {
        $z$1$us$i = $z$0$us$i;
       }
      } while(0);
      $192 = (($$03$us$i) + ($l$080$i$lcssa343)|0);
      $193 = HEAP8[$192>>0]|0;
      $div$us$i = ($193&255) >>> 5;
      $194 = $div$us$i&255;
      $195 = (($byteset$i) + ($194<<2)|0);
      $196 = HEAP32[$195>>2]|0;
      $197 = $193 & 31;
      $198 = $197&255;
      $199 = 1 << $198;
      $200 = $199 & $196;
      $201 = ($200|0)==(0);
      if ($201) {
       $232 = (($$03$us$i) + ($$lcssa344)|0);
       $$03$us$i = $232;$mem$0$us$i = 0;$z$0$us$i = $z$1$us$i;
       continue;
      }
      $202 = $193&255;
      $203 = (($shift$i) + ($202<<2)|0);
      $204 = HEAP32[$203>>2]|0;
      $205 = (($$lcssa344) - ($204))|0;
      $206 = ($$lcssa344|0)==($204|0);
      if (!($206)) {
       $207 = ($mem$0$us$i|0)!=(0);
       $208 = ($205>>>0)<($p$1$p$0$i>>>0);
       $or$cond5$us$i = $207 & $208;
       $k$2$us$i = $or$cond5$us$i ? $178 : $205;
       $209 = (($$03$us$i) + ($k$2$us$i)|0);
       $$03$us$i = $209;$mem$0$us$i = 0;$z$0$us$i = $z$1$us$i;
       continue;
      }
      $210 = ($170>>>0)>($mem$0$us$i>>>0);
      $211 = $210 ? $170 : $mem$0$us$i;
      $212 = (($n) + ($211)|0);
      $213 = HEAP8[$212>>0]|0;
      $214 = ($213<<24>>24)==(0);
      L96: do {
       if ($214) {
        $k$4$us$i = $170;
       } else {
        $$pr$us$i = $213;$k$338$us$i = $211;
        while(1) {
         $215 = (($$03$us$i) + ($k$338$us$i)|0);
         $216 = HEAP8[$215>>0]|0;
         $217 = ($$pr$us$i<<24>>24)==($216<<24>>24);
         if (!($217)) {
          $k$338$us$i$lcssa = $k$338$us$i;
          break;
         }
         $220 = (($k$338$us$i) + 1)|0;
         $221 = (($n) + ($220)|0);
         $222 = HEAP8[$221>>0]|0;
         $223 = ($222<<24>>24)==(0);
         if ($223) {
          $k$4$us$i = $170;
          break L96;
         } else {
          $$pr$us$i = $222;$k$338$us$i = $220;
         }
        }
        $218 = (($k$338$us$i$lcssa) - ($ip$1$ip$0$i))|0;
        $219 = (($$03$us$i) + ($218)|0);
        $$03$us$i = $219;$mem$0$us$i = 0;$z$0$us$i = $z$1$us$i;
        continue L82;
       }
      } while(0);
      while(1) {
       $224 = ($k$4$us$i>>>0)>($mem$0$us$i>>>0);
       if (!($224)) {
        $$0$i = $$03$us$i;
        break L46;
       }
       $225 = (($k$4$us$i) + -1)|0;
       $226 = (($n) + ($225)|0);
       $227 = HEAP8[$226>>0]|0;
       $228 = (($$03$us$i) + ($225)|0);
       $229 = HEAP8[$228>>0]|0;
       $230 = ($227<<24>>24)==($229<<24>>24);
       if ($230) {
        $k$4$us$i = $225;
       } else {
        break;
       }
      }
      $231 = (($$03$us$i) + ($p$1$p$0$i)|0);
      $$03$us$i = $231;$mem$0$us$i = $178;$z$0$us$i = $z$1$us$i;
     }
    }
   } else {
    $173 = (($$lcssa344) - ($ip$1$ip$0$i))|0;
    $174 = (($173) + -1)|0;
    $175 = ($ip$1$ip$0$i>>>0)>($174>>>0);
    $ip$1$ip$0$$i = $175 ? $ip$1$ip$0$i : $174;
    $176 = (($ip$1$ip$0$$i) + 1)|0;
    $177 = $$lcssa344 | 63;
    $237 = $177;$p$3155$i = $176;
   }
   $181 = (($n) + ($170)|0);
   $$03$i = $3;$z$0$i = $3;
   L106: while(1) {
    $233 = $z$0$i;
    $234 = $$03$i;
    $235 = (($233) - ($234))|0;
    $236 = ($235>>>0)<($$lcssa344>>>0);
    do {
     if ($236) {
      $238 = (_memchr($z$0$i,0,$237)|0);
      $239 = ($238|0)==(0|0);
      if ($239) {
       $243 = (($z$0$i) + ($237)|0);
       $z$1$i = $243;
       break;
      } else {
       $240 = $238;
       $241 = (($240) - ($234))|0;
       $242 = ($241>>>0)<($$lcssa344>>>0);
       if ($242) {
        $$0$i = 0;
        break L46;
       } else {
        $z$1$i = $238;
        break;
       }
      }
     } else {
      $z$1$i = $z$0$i;
     }
    } while(0);
    $244 = (($$03$i) + ($l$080$i$lcssa343)|0);
    $245 = HEAP8[$244>>0]|0;
    $div$i = ($245&255) >>> 5;
    $246 = $div$i&255;
    $247 = (($byteset$i) + ($246<<2)|0);
    $248 = HEAP32[$247>>2]|0;
    $249 = $245 & 31;
    $250 = $249&255;
    $251 = 1 << $250;
    $252 = $251 & $248;
    $253 = ($252|0)==(0);
    if ($253) {
     $260 = (($$03$i) + ($$lcssa344)|0);
     $$03$i = $260;$z$0$i = $z$1$i;
     continue;
    }
    $254 = $245&255;
    $255 = (($shift$i) + ($254<<2)|0);
    $256 = HEAP32[$255>>2]|0;
    $257 = ($$lcssa344|0)==($256|0);
    if (!($257)) {
     $258 = (($$lcssa344) - ($256))|0;
     $259 = (($$03$i) + ($258)|0);
     $$03$i = $259;$z$0$i = $z$1$i;
     continue;
    }
    $261 = HEAP8[$181>>0]|0;
    $262 = ($261<<24>>24)==(0);
    L120: do {
     if ($262) {
      $k$4$i = $170;
     } else {
      $$pr$i = $261;$k$338$i = $170;
      while(1) {
       $263 = (($$03$i) + ($k$338$i)|0);
       $264 = HEAP8[$263>>0]|0;
       $265 = ($$pr$i<<24>>24)==($264<<24>>24);
       if (!($265)) {
        $k$338$i$lcssa = $k$338$i;
        break;
       }
       $266 = (($k$338$i) + 1)|0;
       $267 = (($n) + ($266)|0);
       $268 = HEAP8[$267>>0]|0;
       $269 = ($268<<24>>24)==(0);
       if ($269) {
        $k$4$i = $170;
        break L120;
       } else {
        $$pr$i = $268;$k$338$i = $266;
       }
      }
      $270 = (($k$338$i$lcssa) - ($ip$1$ip$0$i))|0;
      $271 = (($$03$i) + ($270)|0);
      $$03$i = $271;$z$0$i = $z$1$i;
      continue L106;
     }
    } while(0);
    while(1) {
     $272 = ($k$4$i|0)==(0);
     if ($272) {
      $$0$i = $$03$i;
      break L46;
     }
     $273 = (($k$4$i) + -1)|0;
     $274 = (($n) + ($273)|0);
     $275 = HEAP8[$274>>0]|0;
     $276 = (($$03$i) + ($273)|0);
     $277 = HEAP8[$276>>0]|0;
     $278 = ($275<<24>>24)==($277<<24>>24);
     if ($278) {
      $k$4$i = $273;
     } else {
      break;
     }
    }
    $279 = (($$03$i) + ($p$3155$i)|0);
    $$03$i = $279;$z$0$i = $z$1$i;
   }
  }
 } while(0);
 $$0 = $$0$i;
 STACKTOP = sp;return ($$0|0);
}
function _exp2($x) {
 $x = +$x;
 var $$0 = 0.0, $0 = 0, $1 = 0, $10 = 0.0, $11 = 0, $12 = 0.0, $13 = 0, $14 = 0, $15 = 0.0, $16 = 0.0, $17 = 0.0, $18 = 0.0, $19 = 0, $2 = 0, $20 = 0.0, $21 = 0.0, $22 = 0, $23 = 0.0, $24 = 0.0, $25 = 0;
 var $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0.0, $31 = 0.0, $32 = 0, $33 = 0, $34 = 0, $35 = 0.0, $36 = 0, $37 = 0, $38 = 0.0, $39 = 0.0, $4 = 0, $40 = 0.0, $41 = 0.0, $42 = 0.0, $43 = 0.0;
 var $44 = 0.0, $45 = 0.0, $46 = 0.0, $47 = 0.0, $48 = 0.0, $49 = 0.0, $5 = 0, $50 = 0.0, $51 = 0.0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $__x = 0, $or$cond = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0;
 $__x = sp;
 HEAPF64[tempDoublePtr>>3] = $x;$0 = HEAP32[tempDoublePtr>>2]|0;
 $1 = HEAP32[tempDoublePtr+4>>2]|0;
 $2 = $1 & 2147483647;
 $3 = ($2>>>0)>(1083174911);
 do {
  if ($3) {
   $4 = ($2>>>0)>(1083179007);
   $5 = ($1|0)>(-1);
   $6 = ($0>>>0)>(4294967295);
   $7 = ($1|0)==(-1);
   $8 = $7 & $6;
   $9 = $5 | $8;
   $or$cond = $9 & $4;
   if ($or$cond) {
    $10 = $x * 8.9884656743115795E+307;
    $$0 = $10;
    STACKTOP = sp;return (+$$0);
   }
   $11 = ($2>>>0)>(2146435071);
   if ($11) {
    $12 = -1.0 / $x;
    $$0 = $12;
    STACKTOP = sp;return (+$$0);
   }
   $13 = ($1|0)<(0);
   if ($13) {
    $14 = !($x <= -1075.0);
    if ($14) {
     $17 = $x + -4503599627370496.0;
     $18 = $17 + 4503599627370496.0;
     $19 = $18 != $x;
     if (!($19)) {
      break;
     }
     $20 = -1.4012984643248171E-45 / $x;
     $21 = $20;
     HEAPF32[$__x>>2] = $21;
     break;
    } else {
     $15 = -1.4012984643248171E-45 / $x;
     $16 = $15;
     HEAPF32[$__x>>2] = $16;
     $$0 = 0.0;
     STACKTOP = sp;return (+$$0);
    }
   }
  } else {
   $22 = ($2>>>0)<(1016070144);
   if ($22) {
    $23 = $x + 1.0;
    $$0 = $23;
    STACKTOP = sp;return (+$$0);
   }
  }
 } while(0);
 $24 = $x + 26388279066624.0;
 HEAPF64[tempDoublePtr>>3] = $24;$25 = HEAP32[tempDoublePtr>>2]|0;
 $26 = HEAP32[tempDoublePtr+4>>2]|0;
 $27 = (($25) + 128)|0;
 $28 = $27 & -256;
 $29 = (($28|0) / 256)&-1;
 $30 = $24 + -26388279066624.0;
 $31 = $x - $30;
 $32 = $27 << 1;
 $33 = $32 & 510;
 $34 = (16480 + ($33<<3)|0);
 $35 = +HEAPF64[$34>>3];
 $36 = $33 | 1;
 $37 = (16480 + ($36<<3)|0);
 $38 = +HEAPF64[$37>>3];
 $39 = $31 - $38;
 $40 = $35 * $39;
 $41 = $39 * 0.0013333559164630223;
 $42 = $41 + 0.0096181298421260663;
 $43 = $39 * $42;
 $44 = $43 + 0.055504108664821403;
 $45 = $39 * $44;
 $46 = $45 + 0.2402265069591;
 $47 = $39 * $46;
 $48 = $47 + 0.69314718055994529;
 $49 = $40 * $48;
 $50 = $35 + $49;
 $51 = (+_scalbn($50,$29));
 $$0 = $51;
 STACKTOP = sp;return (+$$0);
}
function _log10($x) {
 $x = +$x;
 var $$0 = 0.0, $0 = 0, $1 = 0, $10 = 0.0, $11 = 0.0, $12 = 0.0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0.0;
 var $26 = 0, $27 = 0.0, $28 = 0.0, $29 = 0.0, $3 = 0, $30 = 0.0, $31 = 0.0, $32 = 0.0, $33 = 0.0, $34 = 0.0, $35 = 0.0, $36 = 0.0, $37 = 0.0, $38 = 0.0, $39 = 0.0, $4 = 0, $40 = 0.0, $41 = 0.0, $42 = 0.0, $43 = 0.0;
 var $44 = 0.0, $45 = 0.0, $46 = 0.0, $47 = 0.0, $48 = 0, $49 = 0, $5 = 0, $50 = 0.0, $51 = 0.0, $52 = 0.0, $53 = 0.0, $54 = 0.0, $55 = 0.0, $56 = 0.0, $57 = 0.0, $58 = 0.0, $59 = 0.0, $6 = 0, $60 = 0.0, $61 = 0.0;
 var $62 = 0.0, $63 = 0.0, $64 = 0.0, $65 = 0.0, $66 = 0.0, $67 = 0.0, $68 = 0.0, $69 = 0.0, $7 = 0, $70 = 0, $8 = 0.0, $9 = 0.0, $hx$0 = 0, $k$0 = 0, $or$cond = 0, $or$cond4 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 HEAPF64[tempDoublePtr>>3] = $x;$0 = HEAP32[tempDoublePtr>>2]|0;
 $1 = HEAP32[tempDoublePtr+4>>2]|0;
 $2 = ($1>>>0)<(1048576);
 $3 = ($1|0)<(0);
 $or$cond = $3 | $2;
 do {
  if ($or$cond) {
   $4 = $1 & 2147483647;
   $5 = ($0|0)==(0);
   $6 = ($4|0)==(0);
   $7 = $5 & $6;
   if ($7) {
    $8 = $x * $x;
    $9 = -1.0 / $8;
    $$0 = $9;
    return (+$$0);
   }
   if (!($3)) {
    $12 = $x * 18014398509481984.0;
    HEAPF64[tempDoublePtr>>3] = $12;$13 = HEAP32[tempDoublePtr>>2]|0;
    $14 = HEAP32[tempDoublePtr+4>>2]|0;
    $26 = $13;$70 = $14;$hx$0 = $14;$k$0 = -1077;
    break;
   }
   $10 = $x - $x;
   $11 = $10 / 0.0;
   $$0 = $11;
   return (+$$0);
  } else {
   $15 = ($1>>>0)>(2146435071);
   if ($15) {
    $$0 = $x;
    return (+$$0);
   }
   $16 = ($1|0)==(1072693248);
   $17 = ($0|0)==(0);
   $18 = (0)==(0);
   $19 = $17 & $18;
   $or$cond4 = $19 & $16;
   if ($or$cond4) {
    $$0 = 0.0;
    return (+$$0);
   } else {
    $26 = $0;$70 = $1;$hx$0 = $1;$k$0 = -1023;
   }
  }
 } while(0);
 $20 = (($hx$0) + 614242)|0;
 $21 = $20 >>> 20;
 $22 = (($k$0) + ($21))|0;
 $23 = $20 & 1048575;
 $24 = (($23) + 1072079006)|0;
 HEAP32[tempDoublePtr>>2] = $26;HEAP32[tempDoublePtr+4>>2] = $24;$25 = +HEAPF64[tempDoublePtr>>3];
 $27 = $25 + -1.0;
 $28 = $27 * 0.5;
 $29 = $27 * $28;
 $30 = $27 + 2.0;
 $31 = $27 / $30;
 $32 = $31 * $31;
 $33 = $32 * $32;
 $34 = $33 * 0.15313837699209373;
 $35 = $34 + 0.22222198432149784;
 $36 = $33 * $35;
 $37 = $36 + 0.39999999999409419;
 $38 = $33 * $37;
 $39 = $33 * 0.14798198605116586;
 $40 = $39 + 0.1818357216161805;
 $41 = $33 * $40;
 $42 = $41 + 0.28571428743662391;
 $43 = $33 * $42;
 $44 = $43 + 0.66666666666667351;
 $45 = $32 * $44;
 $46 = $38 + $45;
 $47 = $27 - $29;
 HEAPF64[tempDoublePtr>>3] = $47;$48 = HEAP32[tempDoublePtr>>2]|0;
 $49 = HEAP32[tempDoublePtr+4>>2]|0;
 HEAP32[tempDoublePtr>>2] = 0;HEAP32[tempDoublePtr+4>>2] = $49;$50 = +HEAPF64[tempDoublePtr>>3];
 $51 = $27 - $50;
 $52 = $51 - $29;
 $53 = $29 + $46;
 $54 = $31 * $53;
 $55 = $54 + $52;
 $56 = $50 * 0.43429448187816888;
 $57 = (+($22|0));
 $58 = $57 * 0.30102999566361177;
 $59 = $57 * 3.6942390771589308E-13;
 $60 = $50 + $55;
 $61 = $60 * 2.5082946711645275E-11;
 $62 = $59 + $61;
 $63 = $55 * 0.43429448187816888;
 $64 = $63 + $62;
 $65 = $58 + $56;
 $66 = $58 - $65;
 $67 = $56 + $66;
 $68 = $67 + $64;
 $69 = $65 + $68;
 $$0 = $69;
 return (+$$0);
}
function _scalbn($x,$n) {
 $x = +$x;
 $n = $n|0;
 var $$ = 0, $$0 = 0, $$1 = 0, $0 = 0, $1 = 0.0, $10 = 0, $11 = 0.0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0.0, $18 = 0.0, $2 = 0, $3 = 0, $4 = 0.0, $5 = 0, $6 = 0, $7 = 0;
 var $8 = 0.0, $9 = 0, $y$0 = 0.0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($n|0)>(1023);
 if ($0) {
  $1 = $x * 8.9884656743115795E+307;
  $2 = (($n) + -1023)|0;
  $3 = ($2|0)>(1023);
  if ($3) {
   $4 = $1 * 8.9884656743115795E+307;
   $5 = (($n) + -2046)|0;
   $6 = ($5|0)>(1023);
   $$ = $6 ? 1023 : $5;
   $$0 = $$;$y$0 = $4;
  } else {
   $$0 = $2;$y$0 = $1;
  }
 } else {
  $7 = ($n|0)<(-1022);
  if ($7) {
   $8 = $x * 2.2250738585072014E-308;
   $9 = (($n) + 1022)|0;
   $10 = ($9|0)<(-1022);
   if ($10) {
    $11 = $8 * 2.2250738585072014E-308;
    $12 = (($n) + 2044)|0;
    $13 = ($12|0)<(-1022);
    $$1 = $13 ? -1022 : $12;
    $$0 = $$1;$y$0 = $11;
   } else {
    $$0 = $9;$y$0 = $8;
   }
  } else {
   $$0 = $n;$y$0 = $x;
  }
 }
 $14 = (($$0) + 1023)|0;
 $15 = (_bitshift64Shl(($14|0),0,52)|0);
 $16 = tempRet0;
 HEAP32[tempDoublePtr>>2] = $15;HEAP32[tempDoublePtr+4>>2] = $16;$17 = +HEAPF64[tempDoublePtr>>3];
 $18 = $y$0 * $17;
 return (+$18);
}
function _memchr($src,$c,$n) {
 $src = $src|0;
 $c = $c|0;
 $n = $n|0;
 var $$0$lcssa = 0, $$0$lcssa44 = 0, $$019 = 0, $$1$lcssa = 0, $$110 = 0, $$110$lcssa = 0, $$24 = 0, $$3 = 0, $$lcssa = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0;
 var $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0;
 var $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $or$cond = 0, $or$cond18 = 0, $s$0$lcssa = 0, $s$0$lcssa43 = 0, $s$020 = 0, $s$15 = 0, $s$2 = 0, $w$0$lcssa = 0, $w$011 = 0, $w$011$lcssa = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = $c & 255;
 $1 = $src;
 $2 = $1 & 3;
 $3 = ($2|0)!=(0);
 $4 = ($n|0)!=(0);
 $or$cond18 = $4 & $3;
 L1: do {
  if ($or$cond18) {
   $5 = $c&255;
   $$019 = $n;$s$020 = $src;
   while(1) {
    $6 = HEAP8[$s$020>>0]|0;
    $7 = ($6<<24>>24)==($5<<24>>24);
    if ($7) {
     $$0$lcssa44 = $$019;$s$0$lcssa43 = $s$020;
     label = 6;
     break L1;
    }
    $8 = ((($s$020)) + 1|0);
    $9 = (($$019) + -1)|0;
    $10 = $8;
    $11 = $10 & 3;
    $12 = ($11|0)!=(0);
    $13 = ($9|0)!=(0);
    $or$cond = $13 & $12;
    if ($or$cond) {
     $$019 = $9;$s$020 = $8;
    } else {
     $$0$lcssa = $9;$$lcssa = $13;$s$0$lcssa = $8;
     label = 5;
     break;
    }
   }
  } else {
   $$0$lcssa = $n;$$lcssa = $4;$s$0$lcssa = $src;
   label = 5;
  }
 } while(0);
 if ((label|0) == 5) {
  if ($$lcssa) {
   $$0$lcssa44 = $$0$lcssa;$s$0$lcssa43 = $s$0$lcssa;
   label = 6;
  } else {
   $$3 = 0;$s$2 = $s$0$lcssa;
  }
 }
 L8: do {
  if ((label|0) == 6) {
   $14 = HEAP8[$s$0$lcssa43>>0]|0;
   $15 = $c&255;
   $16 = ($14<<24>>24)==($15<<24>>24);
   if ($16) {
    $$3 = $$0$lcssa44;$s$2 = $s$0$lcssa43;
   } else {
    $17 = Math_imul($0, 16843009)|0;
    $18 = ($$0$lcssa44>>>0)>(3);
    L11: do {
     if ($18) {
      $$110 = $$0$lcssa44;$w$011 = $s$0$lcssa43;
      while(1) {
       $19 = HEAP32[$w$011>>2]|0;
       $20 = $19 ^ $17;
       $21 = (($20) + -16843009)|0;
       $22 = $20 & -2139062144;
       $23 = $22 ^ -2139062144;
       $24 = $23 & $21;
       $25 = ($24|0)==(0);
       if (!($25)) {
        $$110$lcssa = $$110;$w$011$lcssa = $w$011;
        break;
       }
       $26 = ((($w$011)) + 4|0);
       $27 = (($$110) + -4)|0;
       $28 = ($27>>>0)>(3);
       if ($28) {
        $$110 = $27;$w$011 = $26;
       } else {
        $$1$lcssa = $27;$w$0$lcssa = $26;
        label = 11;
        break L11;
       }
      }
      $$24 = $$110$lcssa;$s$15 = $w$011$lcssa;
     } else {
      $$1$lcssa = $$0$lcssa44;$w$0$lcssa = $s$0$lcssa43;
      label = 11;
     }
    } while(0);
    if ((label|0) == 11) {
     $29 = ($$1$lcssa|0)==(0);
     if ($29) {
      $$3 = 0;$s$2 = $w$0$lcssa;
      break;
     } else {
      $$24 = $$1$lcssa;$s$15 = $w$0$lcssa;
     }
    }
    while(1) {
     $30 = HEAP8[$s$15>>0]|0;
     $31 = ($30<<24>>24)==($15<<24>>24);
     if ($31) {
      $$3 = $$24;$s$2 = $s$15;
      break L8;
     }
     $32 = ((($s$15)) + 1|0);
     $33 = (($$24) + -1)|0;
     $34 = ($33|0)==(0);
     if ($34) {
      $$3 = 0;$s$2 = $32;
      break;
     } else {
      $$24 = $33;$s$15 = $32;
     }
    }
   }
  }
 } while(0);
 $35 = ($$3|0)!=(0);
 $36 = $35 ? $s$2 : 0;
 return ($36|0);
}
function _memcmp($vl,$vr,$n) {
 $vl = $vl|0;
 $vr = $vr|0;
 $n = $n|0;
 var $$03 = 0, $$lcssa = 0, $$lcssa19 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $l$04 = 0, $r$05 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($n|0)==(0);
 if ($0) {
  $11 = 0;
  return ($11|0);
 } else {
  $$03 = $n;$l$04 = $vl;$r$05 = $vr;
 }
 while(1) {
  $1 = HEAP8[$l$04>>0]|0;
  $2 = HEAP8[$r$05>>0]|0;
  $3 = ($1<<24>>24)==($2<<24>>24);
  if (!($3)) {
   $$lcssa = $1;$$lcssa19 = $2;
   break;
  }
  $4 = (($$03) + -1)|0;
  $5 = ((($l$04)) + 1|0);
  $6 = ((($r$05)) + 1|0);
  $7 = ($4|0)==(0);
  if ($7) {
   $11 = 0;
   label = 5;
   break;
  } else {
   $$03 = $4;$l$04 = $5;$r$05 = $6;
  }
 }
 if ((label|0) == 5) {
  return ($11|0);
 }
 $8 = $$lcssa&255;
 $9 = $$lcssa19&255;
 $10 = (($8) - ($9))|0;
 $11 = $10;
 return ($11|0);
}
function _strcmp($l,$r) {
 $l = $l|0;
 $r = $r|0;
 var $$014 = 0, $$05 = 0, $$lcssa = 0, $$lcssa2 = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $or$cond = 0, $or$cond3 = 0, label = 0;
 var sp = 0;
 sp = STACKTOP;
 $0 = HEAP8[$l>>0]|0;
 $1 = HEAP8[$r>>0]|0;
 $2 = ($0<<24>>24)!=($1<<24>>24);
 $3 = ($0<<24>>24)==(0);
 $or$cond3 = $3 | $2;
 if ($or$cond3) {
  $$lcssa = $0;$$lcssa2 = $1;
 } else {
  $$014 = $l;$$05 = $r;
  while(1) {
   $4 = ((($$014)) + 1|0);
   $5 = ((($$05)) + 1|0);
   $6 = HEAP8[$4>>0]|0;
   $7 = HEAP8[$5>>0]|0;
   $8 = ($6<<24>>24)!=($7<<24>>24);
   $9 = ($6<<24>>24)==(0);
   $or$cond = $9 | $8;
   if ($or$cond) {
    $$lcssa = $6;$$lcssa2 = $7;
    break;
   } else {
    $$014 = $4;$$05 = $5;
   }
  }
 }
 $10 = $$lcssa&255;
 $11 = $$lcssa2&255;
 $12 = (($10) - ($11))|0;
 return ($12|0);
}
function _malloc($bytes) {
 $bytes = $bytes|0;
 var $$3$i = 0, $$lcssa = 0, $$lcssa211 = 0, $$lcssa215 = 0, $$lcssa216 = 0, $$lcssa217 = 0, $$lcssa219 = 0, $$lcssa222 = 0, $$lcssa224 = 0, $$lcssa226 = 0, $$lcssa228 = 0, $$lcssa230 = 0, $$lcssa232 = 0, $$pre = 0, $$pre$i = 0, $$pre$i$i = 0, $$pre$i22$i = 0, $$pre$i25 = 0, $$pre$phi$i$iZ2D = 0, $$pre$phi$i23$iZ2D = 0;
 var $$pre$phi$i26Z2D = 0, $$pre$phi$iZ2D = 0, $$pre$phi58$i$iZ2D = 0, $$pre$phiZ2D = 0, $$pre105 = 0, $$pre106 = 0, $$pre14$i$i = 0, $$pre43$i = 0, $$pre56$i$i = 0, $$pre57$i$i = 0, $$pre8$i = 0, $$rsize$0$i = 0, $$rsize$3$i = 0, $$sum = 0, $$sum$i$i = 0, $$sum$i$i$i = 0, $$sum$i13$i = 0, $$sum$i14$i = 0, $$sum$i17$i = 0, $$sum$i19$i = 0;
 var $$sum$i2334 = 0, $$sum$i32 = 0, $$sum$i35 = 0, $$sum1 = 0, $$sum1$i = 0, $$sum1$i$i = 0, $$sum1$i15$i = 0, $$sum1$i20$i = 0, $$sum1$i24 = 0, $$sum10 = 0, $$sum10$i = 0, $$sum10$i$i = 0, $$sum11$i = 0, $$sum11$i$i = 0, $$sum1112 = 0, $$sum112$i = 0, $$sum113$i = 0, $$sum114$i = 0, $$sum115$i = 0, $$sum116$i = 0;
 var $$sum117$i = 0, $$sum118$i = 0, $$sum119$i = 0, $$sum12$i = 0, $$sum12$i$i = 0, $$sum120$i = 0, $$sum121$i = 0, $$sum122$i = 0, $$sum123$i = 0, $$sum124$i = 0, $$sum125$i = 0, $$sum13$i = 0, $$sum13$i$i = 0, $$sum14$i$i = 0, $$sum15$i = 0, $$sum15$i$i = 0, $$sum16$i = 0, $$sum16$i$i = 0, $$sum17$i = 0, $$sum17$i$i = 0;
 var $$sum18$i = 0, $$sum1819$i$i = 0, $$sum2 = 0, $$sum2$i = 0, $$sum2$i$i = 0, $$sum2$i$i$i = 0, $$sum2$i16$i = 0, $$sum2$i18$i = 0, $$sum2$i21$i = 0, $$sum20$i$i = 0, $$sum21$i$i = 0, $$sum22$i$i = 0, $$sum23$i$i = 0, $$sum24$i$i = 0, $$sum25$i$i = 0, $$sum27$i$i = 0, $$sum28$i$i = 0, $$sum29$i$i = 0, $$sum3$i = 0, $$sum3$i27 = 0;
 var $$sum30$i$i = 0, $$sum3132$i$i = 0, $$sum34$i$i = 0, $$sum3536$i$i = 0, $$sum3738$i$i = 0, $$sum39$i$i = 0, $$sum4 = 0, $$sum4$i = 0, $$sum4$i$i = 0, $$sum4$i28 = 0, $$sum40$i$i = 0, $$sum41$i$i = 0, $$sum42$i$i = 0, $$sum5$i = 0, $$sum5$i$i = 0, $$sum56 = 0, $$sum6$i = 0, $$sum67$i$i = 0, $$sum7$i = 0, $$sum8$i = 0;
 var $$sum9 = 0, $$sum9$i = 0, $$sum9$i$i = 0, $$tsize$1$i = 0, $$v$0$i = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $1000 = 0, $1001 = 0, $1002 = 0, $1003 = 0, $1004 = 0, $1005 = 0, $1006 = 0, $1007 = 0, $1008 = 0, $1009 = 0, $101 = 0;
 var $1010 = 0, $1011 = 0, $1012 = 0, $1013 = 0, $1014 = 0, $1015 = 0, $1016 = 0, $1017 = 0, $1018 = 0, $1019 = 0, $102 = 0, $1020 = 0, $1021 = 0, $1022 = 0, $1023 = 0, $1024 = 0, $1025 = 0, $1026 = 0, $1027 = 0, $1028 = 0;
 var $1029 = 0, $103 = 0, $1030 = 0, $1031 = 0, $1032 = 0, $1033 = 0, $1034 = 0, $1035 = 0, $1036 = 0, $1037 = 0, $1038 = 0, $1039 = 0, $104 = 0, $1040 = 0, $1041 = 0, $1042 = 0, $1043 = 0, $1044 = 0, $1045 = 0, $1046 = 0;
 var $1047 = 0, $1048 = 0, $1049 = 0, $105 = 0, $1050 = 0, $1051 = 0, $1052 = 0, $1053 = 0, $1054 = 0, $1055 = 0, $1056 = 0, $1057 = 0, $1058 = 0, $1059 = 0, $106 = 0, $1060 = 0, $1061 = 0, $1062 = 0, $1063 = 0, $1064 = 0;
 var $1065 = 0, $1066 = 0, $1067 = 0, $1068 = 0, $1069 = 0, $107 = 0, $1070 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0, $116 = 0, $117 = 0, $118 = 0, $119 = 0;
 var $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0, $134 = 0, $135 = 0, $136 = 0, $137 = 0;
 var $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0, $152 = 0, $153 = 0, $154 = 0, $155 = 0;
 var $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0, $170 = 0, $171 = 0, $172 = 0, $173 = 0;
 var $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0, $189 = 0, $19 = 0, $190 = 0, $191 = 0;
 var $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0, $205 = 0, $206 = 0, $207 = 0, $208 = 0, $209 = 0;
 var $21 = 0, $210 = 0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0, $220 = 0, $221 = 0, $222 = 0, $223 = 0, $224 = 0, $225 = 0, $226 = 0, $227 = 0;
 var $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0, $241 = 0, $242 = 0, $243 = 0, $244 = 0, $245 = 0;
 var $246 = 0, $247 = 0, $248 = 0, $249 = 0, $25 = 0, $250 = 0, $251 = 0, $252 = 0, $253 = 0, $254 = 0, $255 = 0, $256 = 0, $257 = 0, $258 = 0, $259 = 0, $26 = 0, $260 = 0, $261 = 0, $262 = 0, $263 = 0;
 var $264 = 0, $265 = 0, $266 = 0, $267 = 0, $268 = 0, $269 = 0, $27 = 0, $270 = 0, $271 = 0, $272 = 0, $273 = 0, $274 = 0, $275 = 0, $276 = 0, $277 = 0, $278 = 0, $279 = 0, $28 = 0, $280 = 0, $281 = 0;
 var $282 = 0, $283 = 0, $284 = 0, $285 = 0, $286 = 0, $287 = 0, $288 = 0, $289 = 0, $29 = 0, $290 = 0, $291 = 0, $292 = 0, $293 = 0, $294 = 0, $295 = 0, $296 = 0, $297 = 0, $298 = 0, $299 = 0, $3 = 0;
 var $30 = 0, $300 = 0, $301 = 0, $302 = 0, $303 = 0, $304 = 0, $305 = 0, $306 = 0, $307 = 0, $308 = 0, $309 = 0, $31 = 0, $310 = 0, $311 = 0, $312 = 0, $313 = 0, $314 = 0, $315 = 0, $316 = 0, $317 = 0;
 var $318 = 0, $319 = 0, $32 = 0, $320 = 0, $321 = 0, $322 = 0, $323 = 0, $324 = 0, $325 = 0, $326 = 0, $327 = 0, $328 = 0, $329 = 0, $33 = 0, $330 = 0, $331 = 0, $332 = 0, $333 = 0, $334 = 0, $335 = 0;
 var $336 = 0, $337 = 0, $338 = 0, $339 = 0, $34 = 0, $340 = 0, $341 = 0, $342 = 0, $343 = 0, $344 = 0, $345 = 0, $346 = 0, $347 = 0, $348 = 0, $349 = 0, $35 = 0, $350 = 0, $351 = 0, $352 = 0, $353 = 0;
 var $354 = 0, $355 = 0, $356 = 0, $357 = 0, $358 = 0, $359 = 0, $36 = 0, $360 = 0, $361 = 0, $362 = 0, $363 = 0, $364 = 0, $365 = 0, $366 = 0, $367 = 0, $368 = 0, $369 = 0, $37 = 0, $370 = 0, $371 = 0;
 var $372 = 0, $373 = 0, $374 = 0, $375 = 0, $376 = 0, $377 = 0, $378 = 0, $379 = 0, $38 = 0, $380 = 0, $381 = 0, $382 = 0, $383 = 0, $384 = 0, $385 = 0, $386 = 0, $387 = 0, $388 = 0, $389 = 0, $39 = 0;
 var $390 = 0, $391 = 0, $392 = 0, $393 = 0, $394 = 0, $395 = 0, $396 = 0, $397 = 0, $398 = 0, $399 = 0, $4 = 0, $40 = 0, $400 = 0, $401 = 0, $402 = 0, $403 = 0, $404 = 0, $405 = 0, $406 = 0, $407 = 0;
 var $408 = 0, $409 = 0, $41 = 0, $410 = 0, $411 = 0, $412 = 0, $413 = 0, $414 = 0, $415 = 0, $416 = 0, $417 = 0, $418 = 0, $419 = 0, $42 = 0, $420 = 0, $421 = 0, $422 = 0, $423 = 0, $424 = 0, $425 = 0;
 var $426 = 0, $427 = 0, $428 = 0, $429 = 0, $43 = 0, $430 = 0, $431 = 0, $432 = 0, $433 = 0, $434 = 0, $435 = 0, $436 = 0, $437 = 0, $438 = 0, $439 = 0, $44 = 0, $440 = 0, $441 = 0, $442 = 0, $443 = 0;
 var $444 = 0, $445 = 0, $446 = 0, $447 = 0, $448 = 0, $449 = 0, $45 = 0, $450 = 0, $451 = 0, $452 = 0, $453 = 0, $454 = 0, $455 = 0, $456 = 0, $457 = 0, $458 = 0, $459 = 0, $46 = 0, $460 = 0, $461 = 0;
 var $462 = 0, $463 = 0, $464 = 0, $465 = 0, $466 = 0, $467 = 0, $468 = 0, $469 = 0, $47 = 0, $470 = 0, $471 = 0, $472 = 0, $473 = 0, $474 = 0, $475 = 0, $476 = 0, $477 = 0, $478 = 0, $479 = 0, $48 = 0;
 var $480 = 0, $481 = 0, $482 = 0, $483 = 0, $484 = 0, $485 = 0, $486 = 0, $487 = 0, $488 = 0, $489 = 0, $49 = 0, $490 = 0, $491 = 0, $492 = 0, $493 = 0, $494 = 0, $495 = 0, $496 = 0, $497 = 0, $498 = 0;
 var $499 = 0, $5 = 0, $50 = 0, $500 = 0, $501 = 0, $502 = 0, $503 = 0, $504 = 0, $505 = 0, $506 = 0, $507 = 0, $508 = 0, $509 = 0, $51 = 0, $510 = 0, $511 = 0, $512 = 0, $513 = 0, $514 = 0, $515 = 0;
 var $516 = 0, $517 = 0, $518 = 0, $519 = 0, $52 = 0, $520 = 0, $521 = 0, $522 = 0, $523 = 0, $524 = 0, $525 = 0, $526 = 0, $527 = 0, $528 = 0, $529 = 0, $53 = 0, $530 = 0, $531 = 0, $532 = 0, $533 = 0;
 var $534 = 0, $535 = 0, $536 = 0, $537 = 0, $538 = 0, $539 = 0, $54 = 0, $540 = 0, $541 = 0, $542 = 0, $543 = 0, $544 = 0, $545 = 0, $546 = 0, $547 = 0, $548 = 0, $549 = 0, $55 = 0, $550 = 0, $551 = 0;
 var $552 = 0, $553 = 0, $554 = 0, $555 = 0, $556 = 0, $557 = 0, $558 = 0, $559 = 0, $56 = 0, $560 = 0, $561 = 0, $562 = 0, $563 = 0, $564 = 0, $565 = 0, $566 = 0, $567 = 0, $568 = 0, $569 = 0, $57 = 0;
 var $570 = 0, $571 = 0, $572 = 0, $573 = 0, $574 = 0, $575 = 0, $576 = 0, $577 = 0, $578 = 0, $579 = 0, $58 = 0, $580 = 0, $581 = 0, $582 = 0, $583 = 0, $584 = 0, $585 = 0, $586 = 0, $587 = 0, $588 = 0;
 var $589 = 0, $59 = 0, $590 = 0, $591 = 0, $592 = 0, $593 = 0, $594 = 0, $595 = 0, $596 = 0, $597 = 0, $598 = 0, $599 = 0, $6 = 0, $60 = 0, $600 = 0, $601 = 0, $602 = 0, $603 = 0, $604 = 0, $605 = 0;
 var $606 = 0, $607 = 0, $608 = 0, $609 = 0, $61 = 0, $610 = 0, $611 = 0, $612 = 0, $613 = 0, $614 = 0, $615 = 0, $616 = 0, $617 = 0, $618 = 0, $619 = 0, $62 = 0, $620 = 0, $621 = 0, $622 = 0, $623 = 0;
 var $624 = 0, $625 = 0, $626 = 0, $627 = 0, $628 = 0, $629 = 0, $63 = 0, $630 = 0, $631 = 0, $632 = 0, $633 = 0, $634 = 0, $635 = 0, $636 = 0, $637 = 0, $638 = 0, $639 = 0, $64 = 0, $640 = 0, $641 = 0;
 var $642 = 0, $643 = 0, $644 = 0, $645 = 0, $646 = 0, $647 = 0, $648 = 0, $649 = 0, $65 = 0, $650 = 0, $651 = 0, $652 = 0, $653 = 0, $654 = 0, $655 = 0, $656 = 0, $657 = 0, $658 = 0, $659 = 0, $66 = 0;
 var $660 = 0, $661 = 0, $662 = 0, $663 = 0, $664 = 0, $665 = 0, $666 = 0, $667 = 0, $668 = 0, $669 = 0, $67 = 0, $670 = 0, $671 = 0, $672 = 0, $673 = 0, $674 = 0, $675 = 0, $676 = 0, $677 = 0, $678 = 0;
 var $679 = 0, $68 = 0, $680 = 0, $681 = 0, $682 = 0, $683 = 0, $684 = 0, $685 = 0, $686 = 0, $687 = 0, $688 = 0, $689 = 0, $69 = 0, $690 = 0, $691 = 0, $692 = 0, $693 = 0, $694 = 0, $695 = 0, $696 = 0;
 var $697 = 0, $698 = 0, $699 = 0, $7 = 0, $70 = 0, $700 = 0, $701 = 0, $702 = 0, $703 = 0, $704 = 0, $705 = 0, $706 = 0, $707 = 0, $708 = 0, $709 = 0, $71 = 0, $710 = 0, $711 = 0, $712 = 0, $713 = 0;
 var $714 = 0, $715 = 0, $716 = 0, $717 = 0, $718 = 0, $719 = 0, $72 = 0, $720 = 0, $721 = 0, $722 = 0, $723 = 0, $724 = 0, $725 = 0, $726 = 0, $727 = 0, $728 = 0, $729 = 0, $73 = 0, $730 = 0, $731 = 0;
 var $732 = 0, $733 = 0, $734 = 0, $735 = 0, $736 = 0, $737 = 0, $738 = 0, $739 = 0, $74 = 0, $740 = 0, $741 = 0, $742 = 0, $743 = 0, $744 = 0, $745 = 0, $746 = 0, $747 = 0, $748 = 0, $749 = 0, $75 = 0;
 var $750 = 0, $751 = 0, $752 = 0, $753 = 0, $754 = 0, $755 = 0, $756 = 0, $757 = 0, $758 = 0, $759 = 0, $76 = 0, $760 = 0, $761 = 0, $762 = 0, $763 = 0, $764 = 0, $765 = 0, $766 = 0, $767 = 0, $768 = 0;
 var $769 = 0, $77 = 0, $770 = 0, $771 = 0, $772 = 0, $773 = 0, $774 = 0, $775 = 0, $776 = 0, $777 = 0, $778 = 0, $779 = 0, $78 = 0, $780 = 0, $781 = 0, $782 = 0, $783 = 0, $784 = 0, $785 = 0, $786 = 0;
 var $787 = 0, $788 = 0, $789 = 0, $79 = 0, $790 = 0, $791 = 0, $792 = 0, $793 = 0, $794 = 0, $795 = 0, $796 = 0, $797 = 0, $798 = 0, $799 = 0, $8 = 0, $80 = 0, $800 = 0, $801 = 0, $802 = 0, $803 = 0;
 var $804 = 0, $805 = 0, $806 = 0, $807 = 0, $808 = 0, $809 = 0, $81 = 0, $810 = 0, $811 = 0, $812 = 0, $813 = 0, $814 = 0, $815 = 0, $816 = 0, $817 = 0, $818 = 0, $819 = 0, $82 = 0, $820 = 0, $821 = 0;
 var $822 = 0, $823 = 0, $824 = 0, $825 = 0, $826 = 0, $827 = 0, $828 = 0, $829 = 0, $83 = 0, $830 = 0, $831 = 0, $832 = 0, $833 = 0, $834 = 0, $835 = 0, $836 = 0, $837 = 0, $838 = 0, $839 = 0, $84 = 0;
 var $840 = 0, $841 = 0, $842 = 0, $843 = 0, $844 = 0, $845 = 0, $846 = 0, $847 = 0, $848 = 0, $849 = 0, $85 = 0, $850 = 0, $851 = 0, $852 = 0, $853 = 0, $854 = 0, $855 = 0, $856 = 0, $857 = 0, $858 = 0;
 var $859 = 0, $86 = 0, $860 = 0, $861 = 0, $862 = 0, $863 = 0, $864 = 0, $865 = 0, $866 = 0, $867 = 0, $868 = 0, $869 = 0, $87 = 0, $870 = 0, $871 = 0, $872 = 0, $873 = 0, $874 = 0, $875 = 0, $876 = 0;
 var $877 = 0, $878 = 0, $879 = 0, $88 = 0, $880 = 0, $881 = 0, $882 = 0, $883 = 0, $884 = 0, $885 = 0, $886 = 0, $887 = 0, $888 = 0, $889 = 0, $89 = 0, $890 = 0, $891 = 0, $892 = 0, $893 = 0, $894 = 0;
 var $895 = 0, $896 = 0, $897 = 0, $898 = 0, $899 = 0, $9 = 0, $90 = 0, $900 = 0, $901 = 0, $902 = 0, $903 = 0, $904 = 0, $905 = 0, $906 = 0, $907 = 0, $908 = 0, $909 = 0, $91 = 0, $910 = 0, $911 = 0;
 var $912 = 0, $913 = 0, $914 = 0, $915 = 0, $916 = 0, $917 = 0, $918 = 0, $919 = 0, $92 = 0, $920 = 0, $921 = 0, $922 = 0, $923 = 0, $924 = 0, $925 = 0, $926 = 0, $927 = 0, $928 = 0, $929 = 0, $93 = 0;
 var $930 = 0, $931 = 0, $932 = 0, $933 = 0, $934 = 0, $935 = 0, $936 = 0, $937 = 0, $938 = 0, $939 = 0, $94 = 0, $940 = 0, $941 = 0, $942 = 0, $943 = 0, $944 = 0, $945 = 0, $946 = 0, $947 = 0, $948 = 0;
 var $949 = 0, $95 = 0, $950 = 0, $951 = 0, $952 = 0, $953 = 0, $954 = 0, $955 = 0, $956 = 0, $957 = 0, $958 = 0, $959 = 0, $96 = 0, $960 = 0, $961 = 0, $962 = 0, $963 = 0, $964 = 0, $965 = 0, $966 = 0;
 var $967 = 0, $968 = 0, $969 = 0, $97 = 0, $970 = 0, $971 = 0, $972 = 0, $973 = 0, $974 = 0, $975 = 0, $976 = 0, $977 = 0, $978 = 0, $979 = 0, $98 = 0, $980 = 0, $981 = 0, $982 = 0, $983 = 0, $984 = 0;
 var $985 = 0, $986 = 0, $987 = 0, $988 = 0, $989 = 0, $99 = 0, $990 = 0, $991 = 0, $992 = 0, $993 = 0, $994 = 0, $995 = 0, $996 = 0, $997 = 0, $998 = 0, $999 = 0, $F$0$i$i = 0, $F1$0$i = 0, $F4$0 = 0, $F4$0$i$i = 0;
 var $F5$0$i = 0, $I1$0$i$i = 0, $I7$0$i = 0, $I7$0$i$i = 0, $K12$029$i = 0, $K2$07$i$i = 0, $K8$051$i$i = 0, $R$0$i = 0, $R$0$i$i = 0, $R$0$i$i$lcssa = 0, $R$0$i$lcssa = 0, $R$0$i18 = 0, $R$0$i18$lcssa = 0, $R$1$i = 0, $R$1$i$i = 0, $R$1$i20 = 0, $RP$0$i = 0, $RP$0$i$i = 0, $RP$0$i$i$lcssa = 0, $RP$0$i$lcssa = 0;
 var $RP$0$i17 = 0, $RP$0$i17$lcssa = 0, $T$0$lcssa$i = 0, $T$0$lcssa$i$i = 0, $T$0$lcssa$i25$i = 0, $T$028$i = 0, $T$028$i$lcssa = 0, $T$050$i$i = 0, $T$050$i$i$lcssa = 0, $T$06$i$i = 0, $T$06$i$i$lcssa = 0, $br$0$ph$i = 0, $cond$i = 0, $cond$i$i = 0, $cond$i21 = 0, $exitcond$i$i = 0, $i$02$i$i = 0, $idx$0$i = 0, $mem$0 = 0, $nb$0 = 0;
 var $not$$i = 0, $not$$i$i = 0, $not$$i26$i = 0, $oldfirst$0$i$i = 0, $or$cond$i = 0, $or$cond$i30 = 0, $or$cond1$i = 0, $or$cond19$i = 0, $or$cond2$i = 0, $or$cond3$i = 0, $or$cond5$i = 0, $or$cond57$i = 0, $or$cond6$i = 0, $or$cond8$i = 0, $or$cond9$i = 0, $qsize$0$i$i = 0, $rsize$0$i = 0, $rsize$0$i$lcssa = 0, $rsize$0$i15 = 0, $rsize$1$i = 0;
 var $rsize$2$i = 0, $rsize$3$lcssa$i = 0, $rsize$331$i = 0, $rst$0$i = 0, $rst$1$i = 0, $sizebits$0$i = 0, $sp$0$i$i = 0, $sp$0$i$i$i = 0, $sp$084$i = 0, $sp$084$i$lcssa = 0, $sp$183$i = 0, $sp$183$i$lcssa = 0, $ssize$0$$i = 0, $ssize$0$i = 0, $ssize$1$ph$i = 0, $ssize$2$i = 0, $t$0$i = 0, $t$0$i14 = 0, $t$1$i = 0, $t$2$ph$i = 0;
 var $t$2$v$3$i = 0, $t$230$i = 0, $tbase$255$i = 0, $tsize$0$ph$i = 0, $tsize$0323944$i = 0, $tsize$1$i = 0, $tsize$254$i = 0, $v$0$i = 0, $v$0$i$lcssa = 0, $v$0$i16 = 0, $v$1$i = 0, $v$2$i = 0, $v$3$lcssa$i = 0, $v$3$ph$i = 0, $v$332$i = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($bytes>>>0)<(245);
 do {
  if ($0) {
   $1 = ($bytes>>>0)<(11);
   $2 = (($bytes) + 11)|0;
   $3 = $2 & -8;
   $4 = $1 ? 16 : $3;
   $5 = $4 >>> 3;
   $6 = HEAP32[20576>>2]|0;
   $7 = $6 >>> $5;
   $8 = $7 & 3;
   $9 = ($8|0)==(0);
   if (!($9)) {
    $10 = $7 & 1;
    $11 = $10 ^ 1;
    $12 = (($11) + ($5))|0;
    $13 = $12 << 1;
    $14 = (20616 + ($13<<2)|0);
    $$sum10 = (($13) + 2)|0;
    $15 = (20616 + ($$sum10<<2)|0);
    $16 = HEAP32[$15>>2]|0;
    $17 = ((($16)) + 8|0);
    $18 = HEAP32[$17>>2]|0;
    $19 = ($14|0)==($18|0);
    do {
     if ($19) {
      $20 = 1 << $12;
      $21 = $20 ^ -1;
      $22 = $6 & $21;
      HEAP32[20576>>2] = $22;
     } else {
      $23 = HEAP32[(20592)>>2]|0;
      $24 = ($18>>>0)<($23>>>0);
      if ($24) {
       _abort();
       // unreachable;
      }
      $25 = ((($18)) + 12|0);
      $26 = HEAP32[$25>>2]|0;
      $27 = ($26|0)==($16|0);
      if ($27) {
       HEAP32[$25>>2] = $14;
       HEAP32[$15>>2] = $18;
       break;
      } else {
       _abort();
       // unreachable;
      }
     }
    } while(0);
    $28 = $12 << 3;
    $29 = $28 | 3;
    $30 = ((($16)) + 4|0);
    HEAP32[$30>>2] = $29;
    $$sum1112 = $28 | 4;
    $31 = (($16) + ($$sum1112)|0);
    $32 = HEAP32[$31>>2]|0;
    $33 = $32 | 1;
    HEAP32[$31>>2] = $33;
    $mem$0 = $17;
    return ($mem$0|0);
   }
   $34 = HEAP32[(20584)>>2]|0;
   $35 = ($4>>>0)>($34>>>0);
   if ($35) {
    $36 = ($7|0)==(0);
    if (!($36)) {
     $37 = $7 << $5;
     $38 = 2 << $5;
     $39 = (0 - ($38))|0;
     $40 = $38 | $39;
     $41 = $37 & $40;
     $42 = (0 - ($41))|0;
     $43 = $41 & $42;
     $44 = (($43) + -1)|0;
     $45 = $44 >>> 12;
     $46 = $45 & 16;
     $47 = $44 >>> $46;
     $48 = $47 >>> 5;
     $49 = $48 & 8;
     $50 = $49 | $46;
     $51 = $47 >>> $49;
     $52 = $51 >>> 2;
     $53 = $52 & 4;
     $54 = $50 | $53;
     $55 = $51 >>> $53;
     $56 = $55 >>> 1;
     $57 = $56 & 2;
     $58 = $54 | $57;
     $59 = $55 >>> $57;
     $60 = $59 >>> 1;
     $61 = $60 & 1;
     $62 = $58 | $61;
     $63 = $59 >>> $61;
     $64 = (($62) + ($63))|0;
     $65 = $64 << 1;
     $66 = (20616 + ($65<<2)|0);
     $$sum4 = (($65) + 2)|0;
     $67 = (20616 + ($$sum4<<2)|0);
     $68 = HEAP32[$67>>2]|0;
     $69 = ((($68)) + 8|0);
     $70 = HEAP32[$69>>2]|0;
     $71 = ($66|0)==($70|0);
     do {
      if ($71) {
       $72 = 1 << $64;
       $73 = $72 ^ -1;
       $74 = $6 & $73;
       HEAP32[20576>>2] = $74;
       $88 = $34;
      } else {
       $75 = HEAP32[(20592)>>2]|0;
       $76 = ($70>>>0)<($75>>>0);
       if ($76) {
        _abort();
        // unreachable;
       }
       $77 = ((($70)) + 12|0);
       $78 = HEAP32[$77>>2]|0;
       $79 = ($78|0)==($68|0);
       if ($79) {
        HEAP32[$77>>2] = $66;
        HEAP32[$67>>2] = $70;
        $$pre = HEAP32[(20584)>>2]|0;
        $88 = $$pre;
        break;
       } else {
        _abort();
        // unreachable;
       }
      }
     } while(0);
     $80 = $64 << 3;
     $81 = (($80) - ($4))|0;
     $82 = $4 | 3;
     $83 = ((($68)) + 4|0);
     HEAP32[$83>>2] = $82;
     $84 = (($68) + ($4)|0);
     $85 = $81 | 1;
     $$sum56 = $4 | 4;
     $86 = (($68) + ($$sum56)|0);
     HEAP32[$86>>2] = $85;
     $87 = (($68) + ($80)|0);
     HEAP32[$87>>2] = $81;
     $89 = ($88|0)==(0);
     if (!($89)) {
      $90 = HEAP32[(20596)>>2]|0;
      $91 = $88 >>> 3;
      $92 = $91 << 1;
      $93 = (20616 + ($92<<2)|0);
      $94 = HEAP32[20576>>2]|0;
      $95 = 1 << $91;
      $96 = $94 & $95;
      $97 = ($96|0)==(0);
      if ($97) {
       $98 = $94 | $95;
       HEAP32[20576>>2] = $98;
       $$pre105 = (($92) + 2)|0;
       $$pre106 = (20616 + ($$pre105<<2)|0);
       $$pre$phiZ2D = $$pre106;$F4$0 = $93;
      } else {
       $$sum9 = (($92) + 2)|0;
       $99 = (20616 + ($$sum9<<2)|0);
       $100 = HEAP32[$99>>2]|0;
       $101 = HEAP32[(20592)>>2]|0;
       $102 = ($100>>>0)<($101>>>0);
       if ($102) {
        _abort();
        // unreachable;
       } else {
        $$pre$phiZ2D = $99;$F4$0 = $100;
       }
      }
      HEAP32[$$pre$phiZ2D>>2] = $90;
      $103 = ((($F4$0)) + 12|0);
      HEAP32[$103>>2] = $90;
      $104 = ((($90)) + 8|0);
      HEAP32[$104>>2] = $F4$0;
      $105 = ((($90)) + 12|0);
      HEAP32[$105>>2] = $93;
     }
     HEAP32[(20584)>>2] = $81;
     HEAP32[(20596)>>2] = $84;
     $mem$0 = $69;
     return ($mem$0|0);
    }
    $106 = HEAP32[(20580)>>2]|0;
    $107 = ($106|0)==(0);
    if ($107) {
     $nb$0 = $4;
    } else {
     $108 = (0 - ($106))|0;
     $109 = $106 & $108;
     $110 = (($109) + -1)|0;
     $111 = $110 >>> 12;
     $112 = $111 & 16;
     $113 = $110 >>> $112;
     $114 = $113 >>> 5;
     $115 = $114 & 8;
     $116 = $115 | $112;
     $117 = $113 >>> $115;
     $118 = $117 >>> 2;
     $119 = $118 & 4;
     $120 = $116 | $119;
     $121 = $117 >>> $119;
     $122 = $121 >>> 1;
     $123 = $122 & 2;
     $124 = $120 | $123;
     $125 = $121 >>> $123;
     $126 = $125 >>> 1;
     $127 = $126 & 1;
     $128 = $124 | $127;
     $129 = $125 >>> $127;
     $130 = (($128) + ($129))|0;
     $131 = (20880 + ($130<<2)|0);
     $132 = HEAP32[$131>>2]|0;
     $133 = ((($132)) + 4|0);
     $134 = HEAP32[$133>>2]|0;
     $135 = $134 & -8;
     $136 = (($135) - ($4))|0;
     $rsize$0$i = $136;$t$0$i = $132;$v$0$i = $132;
     while(1) {
      $137 = ((($t$0$i)) + 16|0);
      $138 = HEAP32[$137>>2]|0;
      $139 = ($138|0)==(0|0);
      if ($139) {
       $140 = ((($t$0$i)) + 20|0);
       $141 = HEAP32[$140>>2]|0;
       $142 = ($141|0)==(0|0);
       if ($142) {
        $rsize$0$i$lcssa = $rsize$0$i;$v$0$i$lcssa = $v$0$i;
        break;
       } else {
        $144 = $141;
       }
      } else {
       $144 = $138;
      }
      $143 = ((($144)) + 4|0);
      $145 = HEAP32[$143>>2]|0;
      $146 = $145 & -8;
      $147 = (($146) - ($4))|0;
      $148 = ($147>>>0)<($rsize$0$i>>>0);
      $$rsize$0$i = $148 ? $147 : $rsize$0$i;
      $$v$0$i = $148 ? $144 : $v$0$i;
      $rsize$0$i = $$rsize$0$i;$t$0$i = $144;$v$0$i = $$v$0$i;
     }
     $149 = HEAP32[(20592)>>2]|0;
     $150 = ($v$0$i$lcssa>>>0)<($149>>>0);
     if ($150) {
      _abort();
      // unreachable;
     }
     $151 = (($v$0$i$lcssa) + ($4)|0);
     $152 = ($v$0$i$lcssa>>>0)<($151>>>0);
     if (!($152)) {
      _abort();
      // unreachable;
     }
     $153 = ((($v$0$i$lcssa)) + 24|0);
     $154 = HEAP32[$153>>2]|0;
     $155 = ((($v$0$i$lcssa)) + 12|0);
     $156 = HEAP32[$155>>2]|0;
     $157 = ($156|0)==($v$0$i$lcssa|0);
     do {
      if ($157) {
       $167 = ((($v$0$i$lcssa)) + 20|0);
       $168 = HEAP32[$167>>2]|0;
       $169 = ($168|0)==(0|0);
       if ($169) {
        $170 = ((($v$0$i$lcssa)) + 16|0);
        $171 = HEAP32[$170>>2]|0;
        $172 = ($171|0)==(0|0);
        if ($172) {
         $R$1$i = 0;
         break;
        } else {
         $R$0$i = $171;$RP$0$i = $170;
        }
       } else {
        $R$0$i = $168;$RP$0$i = $167;
       }
       while(1) {
        $173 = ((($R$0$i)) + 20|0);
        $174 = HEAP32[$173>>2]|0;
        $175 = ($174|0)==(0|0);
        if (!($175)) {
         $R$0$i = $174;$RP$0$i = $173;
         continue;
        }
        $176 = ((($R$0$i)) + 16|0);
        $177 = HEAP32[$176>>2]|0;
        $178 = ($177|0)==(0|0);
        if ($178) {
         $R$0$i$lcssa = $R$0$i;$RP$0$i$lcssa = $RP$0$i;
         break;
        } else {
         $R$0$i = $177;$RP$0$i = $176;
        }
       }
       $179 = ($RP$0$i$lcssa>>>0)<($149>>>0);
       if ($179) {
        _abort();
        // unreachable;
       } else {
        HEAP32[$RP$0$i$lcssa>>2] = 0;
        $R$1$i = $R$0$i$lcssa;
        break;
       }
      } else {
       $158 = ((($v$0$i$lcssa)) + 8|0);
       $159 = HEAP32[$158>>2]|0;
       $160 = ($159>>>0)<($149>>>0);
       if ($160) {
        _abort();
        // unreachable;
       }
       $161 = ((($159)) + 12|0);
       $162 = HEAP32[$161>>2]|0;
       $163 = ($162|0)==($v$0$i$lcssa|0);
       if (!($163)) {
        _abort();
        // unreachable;
       }
       $164 = ((($156)) + 8|0);
       $165 = HEAP32[$164>>2]|0;
       $166 = ($165|0)==($v$0$i$lcssa|0);
       if ($166) {
        HEAP32[$161>>2] = $156;
        HEAP32[$164>>2] = $159;
        $R$1$i = $156;
        break;
       } else {
        _abort();
        // unreachable;
       }
      }
     } while(0);
     $180 = ($154|0)==(0|0);
     do {
      if (!($180)) {
       $181 = ((($v$0$i$lcssa)) + 28|0);
       $182 = HEAP32[$181>>2]|0;
       $183 = (20880 + ($182<<2)|0);
       $184 = HEAP32[$183>>2]|0;
       $185 = ($v$0$i$lcssa|0)==($184|0);
       if ($185) {
        HEAP32[$183>>2] = $R$1$i;
        $cond$i = ($R$1$i|0)==(0|0);
        if ($cond$i) {
         $186 = 1 << $182;
         $187 = $186 ^ -1;
         $188 = HEAP32[(20580)>>2]|0;
         $189 = $188 & $187;
         HEAP32[(20580)>>2] = $189;
         break;
        }
       } else {
        $190 = HEAP32[(20592)>>2]|0;
        $191 = ($154>>>0)<($190>>>0);
        if ($191) {
         _abort();
         // unreachable;
        }
        $192 = ((($154)) + 16|0);
        $193 = HEAP32[$192>>2]|0;
        $194 = ($193|0)==($v$0$i$lcssa|0);
        if ($194) {
         HEAP32[$192>>2] = $R$1$i;
        } else {
         $195 = ((($154)) + 20|0);
         HEAP32[$195>>2] = $R$1$i;
        }
        $196 = ($R$1$i|0)==(0|0);
        if ($196) {
         break;
        }
       }
       $197 = HEAP32[(20592)>>2]|0;
       $198 = ($R$1$i>>>0)<($197>>>0);
       if ($198) {
        _abort();
        // unreachable;
       }
       $199 = ((($R$1$i)) + 24|0);
       HEAP32[$199>>2] = $154;
       $200 = ((($v$0$i$lcssa)) + 16|0);
       $201 = HEAP32[$200>>2]|0;
       $202 = ($201|0)==(0|0);
       do {
        if (!($202)) {
         $203 = ($201>>>0)<($197>>>0);
         if ($203) {
          _abort();
          // unreachable;
         } else {
          $204 = ((($R$1$i)) + 16|0);
          HEAP32[$204>>2] = $201;
          $205 = ((($201)) + 24|0);
          HEAP32[$205>>2] = $R$1$i;
          break;
         }
        }
       } while(0);
       $206 = ((($v$0$i$lcssa)) + 20|0);
       $207 = HEAP32[$206>>2]|0;
       $208 = ($207|0)==(0|0);
       if (!($208)) {
        $209 = HEAP32[(20592)>>2]|0;
        $210 = ($207>>>0)<($209>>>0);
        if ($210) {
         _abort();
         // unreachable;
        } else {
         $211 = ((($R$1$i)) + 20|0);
         HEAP32[$211>>2] = $207;
         $212 = ((($207)) + 24|0);
         HEAP32[$212>>2] = $R$1$i;
         break;
        }
       }
      }
     } while(0);
     $213 = ($rsize$0$i$lcssa>>>0)<(16);
     if ($213) {
      $214 = (($rsize$0$i$lcssa) + ($4))|0;
      $215 = $214 | 3;
      $216 = ((($v$0$i$lcssa)) + 4|0);
      HEAP32[$216>>2] = $215;
      $$sum4$i = (($214) + 4)|0;
      $217 = (($v$0$i$lcssa) + ($$sum4$i)|0);
      $218 = HEAP32[$217>>2]|0;
      $219 = $218 | 1;
      HEAP32[$217>>2] = $219;
     } else {
      $220 = $4 | 3;
      $221 = ((($v$0$i$lcssa)) + 4|0);
      HEAP32[$221>>2] = $220;
      $222 = $rsize$0$i$lcssa | 1;
      $$sum$i35 = $4 | 4;
      $223 = (($v$0$i$lcssa) + ($$sum$i35)|0);
      HEAP32[$223>>2] = $222;
      $$sum1$i = (($rsize$0$i$lcssa) + ($4))|0;
      $224 = (($v$0$i$lcssa) + ($$sum1$i)|0);
      HEAP32[$224>>2] = $rsize$0$i$lcssa;
      $225 = HEAP32[(20584)>>2]|0;
      $226 = ($225|0)==(0);
      if (!($226)) {
       $227 = HEAP32[(20596)>>2]|0;
       $228 = $225 >>> 3;
       $229 = $228 << 1;
       $230 = (20616 + ($229<<2)|0);
       $231 = HEAP32[20576>>2]|0;
       $232 = 1 << $228;
       $233 = $231 & $232;
       $234 = ($233|0)==(0);
       if ($234) {
        $235 = $231 | $232;
        HEAP32[20576>>2] = $235;
        $$pre$i = (($229) + 2)|0;
        $$pre8$i = (20616 + ($$pre$i<<2)|0);
        $$pre$phi$iZ2D = $$pre8$i;$F1$0$i = $230;
       } else {
        $$sum3$i = (($229) + 2)|0;
        $236 = (20616 + ($$sum3$i<<2)|0);
        $237 = HEAP32[$236>>2]|0;
        $238 = HEAP32[(20592)>>2]|0;
        $239 = ($237>>>0)<($238>>>0);
        if ($239) {
         _abort();
         // unreachable;
        } else {
         $$pre$phi$iZ2D = $236;$F1$0$i = $237;
        }
       }
       HEAP32[$$pre$phi$iZ2D>>2] = $227;
       $240 = ((($F1$0$i)) + 12|0);
       HEAP32[$240>>2] = $227;
       $241 = ((($227)) + 8|0);
       HEAP32[$241>>2] = $F1$0$i;
       $242 = ((($227)) + 12|0);
       HEAP32[$242>>2] = $230;
      }
      HEAP32[(20584)>>2] = $rsize$0$i$lcssa;
      HEAP32[(20596)>>2] = $151;
     }
     $243 = ((($v$0$i$lcssa)) + 8|0);
     $mem$0 = $243;
     return ($mem$0|0);
    }
   } else {
    $nb$0 = $4;
   }
  } else {
   $244 = ($bytes>>>0)>(4294967231);
   if ($244) {
    $nb$0 = -1;
   } else {
    $245 = (($bytes) + 11)|0;
    $246 = $245 & -8;
    $247 = HEAP32[(20580)>>2]|0;
    $248 = ($247|0)==(0);
    if ($248) {
     $nb$0 = $246;
    } else {
     $249 = (0 - ($246))|0;
     $250 = $245 >>> 8;
     $251 = ($250|0)==(0);
     if ($251) {
      $idx$0$i = 0;
     } else {
      $252 = ($246>>>0)>(16777215);
      if ($252) {
       $idx$0$i = 31;
      } else {
       $253 = (($250) + 1048320)|0;
       $254 = $253 >>> 16;
       $255 = $254 & 8;
       $256 = $250 << $255;
       $257 = (($256) + 520192)|0;
       $258 = $257 >>> 16;
       $259 = $258 & 4;
       $260 = $259 | $255;
       $261 = $256 << $259;
       $262 = (($261) + 245760)|0;
       $263 = $262 >>> 16;
       $264 = $263 & 2;
       $265 = $260 | $264;
       $266 = (14 - ($265))|0;
       $267 = $261 << $264;
       $268 = $267 >>> 15;
       $269 = (($266) + ($268))|0;
       $270 = $269 << 1;
       $271 = (($269) + 7)|0;
       $272 = $246 >>> $271;
       $273 = $272 & 1;
       $274 = $273 | $270;
       $idx$0$i = $274;
      }
     }
     $275 = (20880 + ($idx$0$i<<2)|0);
     $276 = HEAP32[$275>>2]|0;
     $277 = ($276|0)==(0|0);
     L123: do {
      if ($277) {
       $rsize$2$i = $249;$t$1$i = 0;$v$2$i = 0;
       label = 86;
      } else {
       $278 = ($idx$0$i|0)==(31);
       $279 = $idx$0$i >>> 1;
       $280 = (25 - ($279))|0;
       $281 = $278 ? 0 : $280;
       $282 = $246 << $281;
       $rsize$0$i15 = $249;$rst$0$i = 0;$sizebits$0$i = $282;$t$0$i14 = $276;$v$0$i16 = 0;
       while(1) {
        $283 = ((($t$0$i14)) + 4|0);
        $284 = HEAP32[$283>>2]|0;
        $285 = $284 & -8;
        $286 = (($285) - ($246))|0;
        $287 = ($286>>>0)<($rsize$0$i15>>>0);
        if ($287) {
         $288 = ($285|0)==($246|0);
         if ($288) {
          $rsize$331$i = $286;$t$230$i = $t$0$i14;$v$332$i = $t$0$i14;
          label = 90;
          break L123;
         } else {
          $rsize$1$i = $286;$v$1$i = $t$0$i14;
         }
        } else {
         $rsize$1$i = $rsize$0$i15;$v$1$i = $v$0$i16;
        }
        $289 = ((($t$0$i14)) + 20|0);
        $290 = HEAP32[$289>>2]|0;
        $291 = $sizebits$0$i >>> 31;
        $292 = (((($t$0$i14)) + 16|0) + ($291<<2)|0);
        $293 = HEAP32[$292>>2]|0;
        $294 = ($290|0)==(0|0);
        $295 = ($290|0)==($293|0);
        $or$cond19$i = $294 | $295;
        $rst$1$i = $or$cond19$i ? $rst$0$i : $290;
        $296 = ($293|0)==(0|0);
        $297 = $sizebits$0$i << 1;
        if ($296) {
         $rsize$2$i = $rsize$1$i;$t$1$i = $rst$1$i;$v$2$i = $v$1$i;
         label = 86;
         break;
        } else {
         $rsize$0$i15 = $rsize$1$i;$rst$0$i = $rst$1$i;$sizebits$0$i = $297;$t$0$i14 = $293;$v$0$i16 = $v$1$i;
        }
       }
      }
     } while(0);
     if ((label|0) == 86) {
      $298 = ($t$1$i|0)==(0|0);
      $299 = ($v$2$i|0)==(0|0);
      $or$cond$i = $298 & $299;
      if ($or$cond$i) {
       $300 = 2 << $idx$0$i;
       $301 = (0 - ($300))|0;
       $302 = $300 | $301;
       $303 = $247 & $302;
       $304 = ($303|0)==(0);
       if ($304) {
        $nb$0 = $246;
        break;
       }
       $305 = (0 - ($303))|0;
       $306 = $303 & $305;
       $307 = (($306) + -1)|0;
       $308 = $307 >>> 12;
       $309 = $308 & 16;
       $310 = $307 >>> $309;
       $311 = $310 >>> 5;
       $312 = $311 & 8;
       $313 = $312 | $309;
       $314 = $310 >>> $312;
       $315 = $314 >>> 2;
       $316 = $315 & 4;
       $317 = $313 | $316;
       $318 = $314 >>> $316;
       $319 = $318 >>> 1;
       $320 = $319 & 2;
       $321 = $317 | $320;
       $322 = $318 >>> $320;
       $323 = $322 >>> 1;
       $324 = $323 & 1;
       $325 = $321 | $324;
       $326 = $322 >>> $324;
       $327 = (($325) + ($326))|0;
       $328 = (20880 + ($327<<2)|0);
       $329 = HEAP32[$328>>2]|0;
       $t$2$ph$i = $329;$v$3$ph$i = 0;
      } else {
       $t$2$ph$i = $t$1$i;$v$3$ph$i = $v$2$i;
      }
      $330 = ($t$2$ph$i|0)==(0|0);
      if ($330) {
       $rsize$3$lcssa$i = $rsize$2$i;$v$3$lcssa$i = $v$3$ph$i;
      } else {
       $rsize$331$i = $rsize$2$i;$t$230$i = $t$2$ph$i;$v$332$i = $v$3$ph$i;
       label = 90;
      }
     }
     if ((label|0) == 90) {
      while(1) {
       label = 0;
       $331 = ((($t$230$i)) + 4|0);
       $332 = HEAP32[$331>>2]|0;
       $333 = $332 & -8;
       $334 = (($333) - ($246))|0;
       $335 = ($334>>>0)<($rsize$331$i>>>0);
       $$rsize$3$i = $335 ? $334 : $rsize$331$i;
       $t$2$v$3$i = $335 ? $t$230$i : $v$332$i;
       $336 = ((($t$230$i)) + 16|0);
       $337 = HEAP32[$336>>2]|0;
       $338 = ($337|0)==(0|0);
       if (!($338)) {
        $rsize$331$i = $$rsize$3$i;$t$230$i = $337;$v$332$i = $t$2$v$3$i;
        label = 90;
        continue;
       }
       $339 = ((($t$230$i)) + 20|0);
       $340 = HEAP32[$339>>2]|0;
       $341 = ($340|0)==(0|0);
       if ($341) {
        $rsize$3$lcssa$i = $$rsize$3$i;$v$3$lcssa$i = $t$2$v$3$i;
        break;
       } else {
        $rsize$331$i = $$rsize$3$i;$t$230$i = $340;$v$332$i = $t$2$v$3$i;
        label = 90;
       }
      }
     }
     $342 = ($v$3$lcssa$i|0)==(0|0);
     if ($342) {
      $nb$0 = $246;
     } else {
      $343 = HEAP32[(20584)>>2]|0;
      $344 = (($343) - ($246))|0;
      $345 = ($rsize$3$lcssa$i>>>0)<($344>>>0);
      if ($345) {
       $346 = HEAP32[(20592)>>2]|0;
       $347 = ($v$3$lcssa$i>>>0)<($346>>>0);
       if ($347) {
        _abort();
        // unreachable;
       }
       $348 = (($v$3$lcssa$i) + ($246)|0);
       $349 = ($v$3$lcssa$i>>>0)<($348>>>0);
       if (!($349)) {
        _abort();
        // unreachable;
       }
       $350 = ((($v$3$lcssa$i)) + 24|0);
       $351 = HEAP32[$350>>2]|0;
       $352 = ((($v$3$lcssa$i)) + 12|0);
       $353 = HEAP32[$352>>2]|0;
       $354 = ($353|0)==($v$3$lcssa$i|0);
       do {
        if ($354) {
         $364 = ((($v$3$lcssa$i)) + 20|0);
         $365 = HEAP32[$364>>2]|0;
         $366 = ($365|0)==(0|0);
         if ($366) {
          $367 = ((($v$3$lcssa$i)) + 16|0);
          $368 = HEAP32[$367>>2]|0;
          $369 = ($368|0)==(0|0);
          if ($369) {
           $R$1$i20 = 0;
           break;
          } else {
           $R$0$i18 = $368;$RP$0$i17 = $367;
          }
         } else {
          $R$0$i18 = $365;$RP$0$i17 = $364;
         }
         while(1) {
          $370 = ((($R$0$i18)) + 20|0);
          $371 = HEAP32[$370>>2]|0;
          $372 = ($371|0)==(0|0);
          if (!($372)) {
           $R$0$i18 = $371;$RP$0$i17 = $370;
           continue;
          }
          $373 = ((($R$0$i18)) + 16|0);
          $374 = HEAP32[$373>>2]|0;
          $375 = ($374|0)==(0|0);
          if ($375) {
           $R$0$i18$lcssa = $R$0$i18;$RP$0$i17$lcssa = $RP$0$i17;
           break;
          } else {
           $R$0$i18 = $374;$RP$0$i17 = $373;
          }
         }
         $376 = ($RP$0$i17$lcssa>>>0)<($346>>>0);
         if ($376) {
          _abort();
          // unreachable;
         } else {
          HEAP32[$RP$0$i17$lcssa>>2] = 0;
          $R$1$i20 = $R$0$i18$lcssa;
          break;
         }
        } else {
         $355 = ((($v$3$lcssa$i)) + 8|0);
         $356 = HEAP32[$355>>2]|0;
         $357 = ($356>>>0)<($346>>>0);
         if ($357) {
          _abort();
          // unreachable;
         }
         $358 = ((($356)) + 12|0);
         $359 = HEAP32[$358>>2]|0;
         $360 = ($359|0)==($v$3$lcssa$i|0);
         if (!($360)) {
          _abort();
          // unreachable;
         }
         $361 = ((($353)) + 8|0);
         $362 = HEAP32[$361>>2]|0;
         $363 = ($362|0)==($v$3$lcssa$i|0);
         if ($363) {
          HEAP32[$358>>2] = $353;
          HEAP32[$361>>2] = $356;
          $R$1$i20 = $353;
          break;
         } else {
          _abort();
          // unreachable;
         }
        }
       } while(0);
       $377 = ($351|0)==(0|0);
       do {
        if (!($377)) {
         $378 = ((($v$3$lcssa$i)) + 28|0);
         $379 = HEAP32[$378>>2]|0;
         $380 = (20880 + ($379<<2)|0);
         $381 = HEAP32[$380>>2]|0;
         $382 = ($v$3$lcssa$i|0)==($381|0);
         if ($382) {
          HEAP32[$380>>2] = $R$1$i20;
          $cond$i21 = ($R$1$i20|0)==(0|0);
          if ($cond$i21) {
           $383 = 1 << $379;
           $384 = $383 ^ -1;
           $385 = HEAP32[(20580)>>2]|0;
           $386 = $385 & $384;
           HEAP32[(20580)>>2] = $386;
           break;
          }
         } else {
          $387 = HEAP32[(20592)>>2]|0;
          $388 = ($351>>>0)<($387>>>0);
          if ($388) {
           _abort();
           // unreachable;
          }
          $389 = ((($351)) + 16|0);
          $390 = HEAP32[$389>>2]|0;
          $391 = ($390|0)==($v$3$lcssa$i|0);
          if ($391) {
           HEAP32[$389>>2] = $R$1$i20;
          } else {
           $392 = ((($351)) + 20|0);
           HEAP32[$392>>2] = $R$1$i20;
          }
          $393 = ($R$1$i20|0)==(0|0);
          if ($393) {
           break;
          }
         }
         $394 = HEAP32[(20592)>>2]|0;
         $395 = ($R$1$i20>>>0)<($394>>>0);
         if ($395) {
          _abort();
          // unreachable;
         }
         $396 = ((($R$1$i20)) + 24|0);
         HEAP32[$396>>2] = $351;
         $397 = ((($v$3$lcssa$i)) + 16|0);
         $398 = HEAP32[$397>>2]|0;
         $399 = ($398|0)==(0|0);
         do {
          if (!($399)) {
           $400 = ($398>>>0)<($394>>>0);
           if ($400) {
            _abort();
            // unreachable;
           } else {
            $401 = ((($R$1$i20)) + 16|0);
            HEAP32[$401>>2] = $398;
            $402 = ((($398)) + 24|0);
            HEAP32[$402>>2] = $R$1$i20;
            break;
           }
          }
         } while(0);
         $403 = ((($v$3$lcssa$i)) + 20|0);
         $404 = HEAP32[$403>>2]|0;
         $405 = ($404|0)==(0|0);
         if (!($405)) {
          $406 = HEAP32[(20592)>>2]|0;
          $407 = ($404>>>0)<($406>>>0);
          if ($407) {
           _abort();
           // unreachable;
          } else {
           $408 = ((($R$1$i20)) + 20|0);
           HEAP32[$408>>2] = $404;
           $409 = ((($404)) + 24|0);
           HEAP32[$409>>2] = $R$1$i20;
           break;
          }
         }
        }
       } while(0);
       $410 = ($rsize$3$lcssa$i>>>0)<(16);
       L199: do {
        if ($410) {
         $411 = (($rsize$3$lcssa$i) + ($246))|0;
         $412 = $411 | 3;
         $413 = ((($v$3$lcssa$i)) + 4|0);
         HEAP32[$413>>2] = $412;
         $$sum18$i = (($411) + 4)|0;
         $414 = (($v$3$lcssa$i) + ($$sum18$i)|0);
         $415 = HEAP32[$414>>2]|0;
         $416 = $415 | 1;
         HEAP32[$414>>2] = $416;
        } else {
         $417 = $246 | 3;
         $418 = ((($v$3$lcssa$i)) + 4|0);
         HEAP32[$418>>2] = $417;
         $419 = $rsize$3$lcssa$i | 1;
         $$sum$i2334 = $246 | 4;
         $420 = (($v$3$lcssa$i) + ($$sum$i2334)|0);
         HEAP32[$420>>2] = $419;
         $$sum1$i24 = (($rsize$3$lcssa$i) + ($246))|0;
         $421 = (($v$3$lcssa$i) + ($$sum1$i24)|0);
         HEAP32[$421>>2] = $rsize$3$lcssa$i;
         $422 = $rsize$3$lcssa$i >>> 3;
         $423 = ($rsize$3$lcssa$i>>>0)<(256);
         if ($423) {
          $424 = $422 << 1;
          $425 = (20616 + ($424<<2)|0);
          $426 = HEAP32[20576>>2]|0;
          $427 = 1 << $422;
          $428 = $426 & $427;
          $429 = ($428|0)==(0);
          if ($429) {
           $430 = $426 | $427;
           HEAP32[20576>>2] = $430;
           $$pre$i25 = (($424) + 2)|0;
           $$pre43$i = (20616 + ($$pre$i25<<2)|0);
           $$pre$phi$i26Z2D = $$pre43$i;$F5$0$i = $425;
          } else {
           $$sum17$i = (($424) + 2)|0;
           $431 = (20616 + ($$sum17$i<<2)|0);
           $432 = HEAP32[$431>>2]|0;
           $433 = HEAP32[(20592)>>2]|0;
           $434 = ($432>>>0)<($433>>>0);
           if ($434) {
            _abort();
            // unreachable;
           } else {
            $$pre$phi$i26Z2D = $431;$F5$0$i = $432;
           }
          }
          HEAP32[$$pre$phi$i26Z2D>>2] = $348;
          $435 = ((($F5$0$i)) + 12|0);
          HEAP32[$435>>2] = $348;
          $$sum15$i = (($246) + 8)|0;
          $436 = (($v$3$lcssa$i) + ($$sum15$i)|0);
          HEAP32[$436>>2] = $F5$0$i;
          $$sum16$i = (($246) + 12)|0;
          $437 = (($v$3$lcssa$i) + ($$sum16$i)|0);
          HEAP32[$437>>2] = $425;
          break;
         }
         $438 = $rsize$3$lcssa$i >>> 8;
         $439 = ($438|0)==(0);
         if ($439) {
          $I7$0$i = 0;
         } else {
          $440 = ($rsize$3$lcssa$i>>>0)>(16777215);
          if ($440) {
           $I7$0$i = 31;
          } else {
           $441 = (($438) + 1048320)|0;
           $442 = $441 >>> 16;
           $443 = $442 & 8;
           $444 = $438 << $443;
           $445 = (($444) + 520192)|0;
           $446 = $445 >>> 16;
           $447 = $446 & 4;
           $448 = $447 | $443;
           $449 = $444 << $447;
           $450 = (($449) + 245760)|0;
           $451 = $450 >>> 16;
           $452 = $451 & 2;
           $453 = $448 | $452;
           $454 = (14 - ($453))|0;
           $455 = $449 << $452;
           $456 = $455 >>> 15;
           $457 = (($454) + ($456))|0;
           $458 = $457 << 1;
           $459 = (($457) + 7)|0;
           $460 = $rsize$3$lcssa$i >>> $459;
           $461 = $460 & 1;
           $462 = $461 | $458;
           $I7$0$i = $462;
          }
         }
         $463 = (20880 + ($I7$0$i<<2)|0);
         $$sum2$i = (($246) + 28)|0;
         $464 = (($v$3$lcssa$i) + ($$sum2$i)|0);
         HEAP32[$464>>2] = $I7$0$i;
         $$sum3$i27 = (($246) + 16)|0;
         $465 = (($v$3$lcssa$i) + ($$sum3$i27)|0);
         $$sum4$i28 = (($246) + 20)|0;
         $466 = (($v$3$lcssa$i) + ($$sum4$i28)|0);
         HEAP32[$466>>2] = 0;
         HEAP32[$465>>2] = 0;
         $467 = HEAP32[(20580)>>2]|0;
         $468 = 1 << $I7$0$i;
         $469 = $467 & $468;
         $470 = ($469|0)==(0);
         if ($470) {
          $471 = $467 | $468;
          HEAP32[(20580)>>2] = $471;
          HEAP32[$463>>2] = $348;
          $$sum5$i = (($246) + 24)|0;
          $472 = (($v$3$lcssa$i) + ($$sum5$i)|0);
          HEAP32[$472>>2] = $463;
          $$sum6$i = (($246) + 12)|0;
          $473 = (($v$3$lcssa$i) + ($$sum6$i)|0);
          HEAP32[$473>>2] = $348;
          $$sum7$i = (($246) + 8)|0;
          $474 = (($v$3$lcssa$i) + ($$sum7$i)|0);
          HEAP32[$474>>2] = $348;
          break;
         }
         $475 = HEAP32[$463>>2]|0;
         $476 = ((($475)) + 4|0);
         $477 = HEAP32[$476>>2]|0;
         $478 = $477 & -8;
         $479 = ($478|0)==($rsize$3$lcssa$i|0);
         L217: do {
          if ($479) {
           $T$0$lcssa$i = $475;
          } else {
           $480 = ($I7$0$i|0)==(31);
           $481 = $I7$0$i >>> 1;
           $482 = (25 - ($481))|0;
           $483 = $480 ? 0 : $482;
           $484 = $rsize$3$lcssa$i << $483;
           $K12$029$i = $484;$T$028$i = $475;
           while(1) {
            $491 = $K12$029$i >>> 31;
            $492 = (((($T$028$i)) + 16|0) + ($491<<2)|0);
            $487 = HEAP32[$492>>2]|0;
            $493 = ($487|0)==(0|0);
            if ($493) {
             $$lcssa232 = $492;$T$028$i$lcssa = $T$028$i;
             break;
            }
            $485 = $K12$029$i << 1;
            $486 = ((($487)) + 4|0);
            $488 = HEAP32[$486>>2]|0;
            $489 = $488 & -8;
            $490 = ($489|0)==($rsize$3$lcssa$i|0);
            if ($490) {
             $T$0$lcssa$i = $487;
             break L217;
            } else {
             $K12$029$i = $485;$T$028$i = $487;
            }
           }
           $494 = HEAP32[(20592)>>2]|0;
           $495 = ($$lcssa232>>>0)<($494>>>0);
           if ($495) {
            _abort();
            // unreachable;
           } else {
            HEAP32[$$lcssa232>>2] = $348;
            $$sum11$i = (($246) + 24)|0;
            $496 = (($v$3$lcssa$i) + ($$sum11$i)|0);
            HEAP32[$496>>2] = $T$028$i$lcssa;
            $$sum12$i = (($246) + 12)|0;
            $497 = (($v$3$lcssa$i) + ($$sum12$i)|0);
            HEAP32[$497>>2] = $348;
            $$sum13$i = (($246) + 8)|0;
            $498 = (($v$3$lcssa$i) + ($$sum13$i)|0);
            HEAP32[$498>>2] = $348;
            break L199;
           }
          }
         } while(0);
         $499 = ((($T$0$lcssa$i)) + 8|0);
         $500 = HEAP32[$499>>2]|0;
         $501 = HEAP32[(20592)>>2]|0;
         $502 = ($500>>>0)>=($501>>>0);
         $not$$i = ($T$0$lcssa$i>>>0)>=($501>>>0);
         $503 = $502 & $not$$i;
         if ($503) {
          $504 = ((($500)) + 12|0);
          HEAP32[$504>>2] = $348;
          HEAP32[$499>>2] = $348;
          $$sum8$i = (($246) + 8)|0;
          $505 = (($v$3$lcssa$i) + ($$sum8$i)|0);
          HEAP32[$505>>2] = $500;
          $$sum9$i = (($246) + 12)|0;
          $506 = (($v$3$lcssa$i) + ($$sum9$i)|0);
          HEAP32[$506>>2] = $T$0$lcssa$i;
          $$sum10$i = (($246) + 24)|0;
          $507 = (($v$3$lcssa$i) + ($$sum10$i)|0);
          HEAP32[$507>>2] = 0;
          break;
         } else {
          _abort();
          // unreachable;
         }
        }
       } while(0);
       $508 = ((($v$3$lcssa$i)) + 8|0);
       $mem$0 = $508;
       return ($mem$0|0);
      } else {
       $nb$0 = $246;
      }
     }
    }
   }
  }
 } while(0);
 $509 = HEAP32[(20584)>>2]|0;
 $510 = ($509>>>0)<($nb$0>>>0);
 if (!($510)) {
  $511 = (($509) - ($nb$0))|0;
  $512 = HEAP32[(20596)>>2]|0;
  $513 = ($511>>>0)>(15);
  if ($513) {
   $514 = (($512) + ($nb$0)|0);
   HEAP32[(20596)>>2] = $514;
   HEAP32[(20584)>>2] = $511;
   $515 = $511 | 1;
   $$sum2 = (($nb$0) + 4)|0;
   $516 = (($512) + ($$sum2)|0);
   HEAP32[$516>>2] = $515;
   $517 = (($512) + ($509)|0);
   HEAP32[$517>>2] = $511;
   $518 = $nb$0 | 3;
   $519 = ((($512)) + 4|0);
   HEAP32[$519>>2] = $518;
  } else {
   HEAP32[(20584)>>2] = 0;
   HEAP32[(20596)>>2] = 0;
   $520 = $509 | 3;
   $521 = ((($512)) + 4|0);
   HEAP32[$521>>2] = $520;
   $$sum1 = (($509) + 4)|0;
   $522 = (($512) + ($$sum1)|0);
   $523 = HEAP32[$522>>2]|0;
   $524 = $523 | 1;
   HEAP32[$522>>2] = $524;
  }
  $525 = ((($512)) + 8|0);
  $mem$0 = $525;
  return ($mem$0|0);
 }
 $526 = HEAP32[(20588)>>2]|0;
 $527 = ($526>>>0)>($nb$0>>>0);
 if ($527) {
  $528 = (($526) - ($nb$0))|0;
  HEAP32[(20588)>>2] = $528;
  $529 = HEAP32[(20600)>>2]|0;
  $530 = (($529) + ($nb$0)|0);
  HEAP32[(20600)>>2] = $530;
  $531 = $528 | 1;
  $$sum = (($nb$0) + 4)|0;
  $532 = (($529) + ($$sum)|0);
  HEAP32[$532>>2] = $531;
  $533 = $nb$0 | 3;
  $534 = ((($529)) + 4|0);
  HEAP32[$534>>2] = $533;
  $535 = ((($529)) + 8|0);
  $mem$0 = $535;
  return ($mem$0|0);
 }
 $536 = HEAP32[21048>>2]|0;
 $537 = ($536|0)==(0);
 do {
  if ($537) {
   $538 = (_sysconf(30)|0);
   $539 = (($538) + -1)|0;
   $540 = $539 & $538;
   $541 = ($540|0)==(0);
   if ($541) {
    HEAP32[(21056)>>2] = $538;
    HEAP32[(21052)>>2] = $538;
    HEAP32[(21060)>>2] = -1;
    HEAP32[(21064)>>2] = -1;
    HEAP32[(21068)>>2] = 0;
    HEAP32[(21020)>>2] = 0;
    $542 = (_time((0|0))|0);
    $543 = $542 & -16;
    $544 = $543 ^ 1431655768;
    HEAP32[21048>>2] = $544;
    break;
   } else {
    _abort();
    // unreachable;
   }
  }
 } while(0);
 $545 = (($nb$0) + 48)|0;
 $546 = HEAP32[(21056)>>2]|0;
 $547 = (($nb$0) + 47)|0;
 $548 = (($546) + ($547))|0;
 $549 = (0 - ($546))|0;
 $550 = $548 & $549;
 $551 = ($550>>>0)>($nb$0>>>0);
 if (!($551)) {
  $mem$0 = 0;
  return ($mem$0|0);
 }
 $552 = HEAP32[(21016)>>2]|0;
 $553 = ($552|0)==(0);
 if (!($553)) {
  $554 = HEAP32[(21008)>>2]|0;
  $555 = (($554) + ($550))|0;
  $556 = ($555>>>0)<=($554>>>0);
  $557 = ($555>>>0)>($552>>>0);
  $or$cond1$i = $556 | $557;
  if ($or$cond1$i) {
   $mem$0 = 0;
   return ($mem$0|0);
  }
 }
 $558 = HEAP32[(21020)>>2]|0;
 $559 = $558 & 4;
 $560 = ($559|0)==(0);
 L258: do {
  if ($560) {
   $561 = HEAP32[(20600)>>2]|0;
   $562 = ($561|0)==(0|0);
   L260: do {
    if ($562) {
     label = 174;
    } else {
     $sp$0$i$i = (21024);
     while(1) {
      $563 = HEAP32[$sp$0$i$i>>2]|0;
      $564 = ($563>>>0)>($561>>>0);
      if (!($564)) {
       $565 = ((($sp$0$i$i)) + 4|0);
       $566 = HEAP32[$565>>2]|0;
       $567 = (($563) + ($566)|0);
       $568 = ($567>>>0)>($561>>>0);
       if ($568) {
        $$lcssa228 = $sp$0$i$i;$$lcssa230 = $565;
        break;
       }
      }
      $569 = ((($sp$0$i$i)) + 8|0);
      $570 = HEAP32[$569>>2]|0;
      $571 = ($570|0)==(0|0);
      if ($571) {
       label = 174;
       break L260;
      } else {
       $sp$0$i$i = $570;
      }
     }
     $594 = HEAP32[(20588)>>2]|0;
     $595 = (($548) - ($594))|0;
     $596 = $595 & $549;
     $597 = ($596>>>0)<(2147483647);
     if ($597) {
      $598 = (_sbrk(($596|0))|0);
      $599 = HEAP32[$$lcssa228>>2]|0;
      $600 = HEAP32[$$lcssa230>>2]|0;
      $601 = (($599) + ($600)|0);
      $602 = ($598|0)==($601|0);
      $$3$i = $602 ? $596 : 0;
      if ($602) {
       $603 = ($598|0)==((-1)|0);
       if ($603) {
        $tsize$0323944$i = $$3$i;
       } else {
        $tbase$255$i = $598;$tsize$254$i = $$3$i;
        label = 194;
        break L258;
       }
      } else {
       $br$0$ph$i = $598;$ssize$1$ph$i = $596;$tsize$0$ph$i = $$3$i;
       label = 184;
      }
     } else {
      $tsize$0323944$i = 0;
     }
    }
   } while(0);
   do {
    if ((label|0) == 174) {
     $572 = (_sbrk(0)|0);
     $573 = ($572|0)==((-1)|0);
     if ($573) {
      $tsize$0323944$i = 0;
     } else {
      $574 = $572;
      $575 = HEAP32[(21052)>>2]|0;
      $576 = (($575) + -1)|0;
      $577 = $576 & $574;
      $578 = ($577|0)==(0);
      if ($578) {
       $ssize$0$i = $550;
      } else {
       $579 = (($576) + ($574))|0;
       $580 = (0 - ($575))|0;
       $581 = $579 & $580;
       $582 = (($550) - ($574))|0;
       $583 = (($582) + ($581))|0;
       $ssize$0$i = $583;
      }
      $584 = HEAP32[(21008)>>2]|0;
      $585 = (($584) + ($ssize$0$i))|0;
      $586 = ($ssize$0$i>>>0)>($nb$0>>>0);
      $587 = ($ssize$0$i>>>0)<(2147483647);
      $or$cond$i30 = $586 & $587;
      if ($or$cond$i30) {
       $588 = HEAP32[(21016)>>2]|0;
       $589 = ($588|0)==(0);
       if (!($589)) {
        $590 = ($585>>>0)<=($584>>>0);
        $591 = ($585>>>0)>($588>>>0);
        $or$cond2$i = $590 | $591;
        if ($or$cond2$i) {
         $tsize$0323944$i = 0;
         break;
        }
       }
       $592 = (_sbrk(($ssize$0$i|0))|0);
       $593 = ($592|0)==($572|0);
       $ssize$0$$i = $593 ? $ssize$0$i : 0;
       if ($593) {
        $tbase$255$i = $572;$tsize$254$i = $ssize$0$$i;
        label = 194;
        break L258;
       } else {
        $br$0$ph$i = $592;$ssize$1$ph$i = $ssize$0$i;$tsize$0$ph$i = $ssize$0$$i;
        label = 184;
       }
      } else {
       $tsize$0323944$i = 0;
      }
     }
    }
   } while(0);
   L280: do {
    if ((label|0) == 184) {
     $604 = (0 - ($ssize$1$ph$i))|0;
     $605 = ($br$0$ph$i|0)!=((-1)|0);
     $606 = ($ssize$1$ph$i>>>0)<(2147483647);
     $or$cond5$i = $606 & $605;
     $607 = ($545>>>0)>($ssize$1$ph$i>>>0);
     $or$cond6$i = $607 & $or$cond5$i;
     do {
      if ($or$cond6$i) {
       $608 = HEAP32[(21056)>>2]|0;
       $609 = (($547) - ($ssize$1$ph$i))|0;
       $610 = (($609) + ($608))|0;
       $611 = (0 - ($608))|0;
       $612 = $610 & $611;
       $613 = ($612>>>0)<(2147483647);
       if ($613) {
        $614 = (_sbrk(($612|0))|0);
        $615 = ($614|0)==((-1)|0);
        if ($615) {
         (_sbrk(($604|0))|0);
         $tsize$0323944$i = $tsize$0$ph$i;
         break L280;
        } else {
         $616 = (($612) + ($ssize$1$ph$i))|0;
         $ssize$2$i = $616;
         break;
        }
       } else {
        $ssize$2$i = $ssize$1$ph$i;
       }
      } else {
       $ssize$2$i = $ssize$1$ph$i;
      }
     } while(0);
     $617 = ($br$0$ph$i|0)==((-1)|0);
     if ($617) {
      $tsize$0323944$i = $tsize$0$ph$i;
     } else {
      $tbase$255$i = $br$0$ph$i;$tsize$254$i = $ssize$2$i;
      label = 194;
      break L258;
     }
    }
   } while(0);
   $618 = HEAP32[(21020)>>2]|0;
   $619 = $618 | 4;
   HEAP32[(21020)>>2] = $619;
   $tsize$1$i = $tsize$0323944$i;
   label = 191;
  } else {
   $tsize$1$i = 0;
   label = 191;
  }
 } while(0);
 if ((label|0) == 191) {
  $620 = ($550>>>0)<(2147483647);
  if ($620) {
   $621 = (_sbrk(($550|0))|0);
   $622 = (_sbrk(0)|0);
   $623 = ($621|0)!=((-1)|0);
   $624 = ($622|0)!=((-1)|0);
   $or$cond3$i = $623 & $624;
   $625 = ($621>>>0)<($622>>>0);
   $or$cond8$i = $625 & $or$cond3$i;
   if ($or$cond8$i) {
    $626 = $622;
    $627 = $621;
    $628 = (($626) - ($627))|0;
    $629 = (($nb$0) + 40)|0;
    $630 = ($628>>>0)>($629>>>0);
    $$tsize$1$i = $630 ? $628 : $tsize$1$i;
    if ($630) {
     $tbase$255$i = $621;$tsize$254$i = $$tsize$1$i;
     label = 194;
    }
   }
  }
 }
 if ((label|0) == 194) {
  $631 = HEAP32[(21008)>>2]|0;
  $632 = (($631) + ($tsize$254$i))|0;
  HEAP32[(21008)>>2] = $632;
  $633 = HEAP32[(21012)>>2]|0;
  $634 = ($632>>>0)>($633>>>0);
  if ($634) {
   HEAP32[(21012)>>2] = $632;
  }
  $635 = HEAP32[(20600)>>2]|0;
  $636 = ($635|0)==(0|0);
  L299: do {
   if ($636) {
    $637 = HEAP32[(20592)>>2]|0;
    $638 = ($637|0)==(0|0);
    $639 = ($tbase$255$i>>>0)<($637>>>0);
    $or$cond9$i = $638 | $639;
    if ($or$cond9$i) {
     HEAP32[(20592)>>2] = $tbase$255$i;
    }
    HEAP32[(21024)>>2] = $tbase$255$i;
    HEAP32[(21028)>>2] = $tsize$254$i;
    HEAP32[(21036)>>2] = 0;
    $640 = HEAP32[21048>>2]|0;
    HEAP32[(20612)>>2] = $640;
    HEAP32[(20608)>>2] = -1;
    $i$02$i$i = 0;
    while(1) {
     $641 = $i$02$i$i << 1;
     $642 = (20616 + ($641<<2)|0);
     $$sum$i$i = (($641) + 3)|0;
     $643 = (20616 + ($$sum$i$i<<2)|0);
     HEAP32[$643>>2] = $642;
     $$sum1$i$i = (($641) + 2)|0;
     $644 = (20616 + ($$sum1$i$i<<2)|0);
     HEAP32[$644>>2] = $642;
     $645 = (($i$02$i$i) + 1)|0;
     $exitcond$i$i = ($645|0)==(32);
     if ($exitcond$i$i) {
      break;
     } else {
      $i$02$i$i = $645;
     }
    }
    $646 = (($tsize$254$i) + -40)|0;
    $647 = ((($tbase$255$i)) + 8|0);
    $648 = $647;
    $649 = $648 & 7;
    $650 = ($649|0)==(0);
    $651 = (0 - ($648))|0;
    $652 = $651 & 7;
    $653 = $650 ? 0 : $652;
    $654 = (($tbase$255$i) + ($653)|0);
    $655 = (($646) - ($653))|0;
    HEAP32[(20600)>>2] = $654;
    HEAP32[(20588)>>2] = $655;
    $656 = $655 | 1;
    $$sum$i13$i = (($653) + 4)|0;
    $657 = (($tbase$255$i) + ($$sum$i13$i)|0);
    HEAP32[$657>>2] = $656;
    $$sum2$i$i = (($tsize$254$i) + -36)|0;
    $658 = (($tbase$255$i) + ($$sum2$i$i)|0);
    HEAP32[$658>>2] = 40;
    $659 = HEAP32[(21064)>>2]|0;
    HEAP32[(20604)>>2] = $659;
   } else {
    $sp$084$i = (21024);
    while(1) {
     $660 = HEAP32[$sp$084$i>>2]|0;
     $661 = ((($sp$084$i)) + 4|0);
     $662 = HEAP32[$661>>2]|0;
     $663 = (($660) + ($662)|0);
     $664 = ($tbase$255$i|0)==($663|0);
     if ($664) {
      $$lcssa222 = $660;$$lcssa224 = $661;$$lcssa226 = $662;$sp$084$i$lcssa = $sp$084$i;
      label = 204;
      break;
     }
     $665 = ((($sp$084$i)) + 8|0);
     $666 = HEAP32[$665>>2]|0;
     $667 = ($666|0)==(0|0);
     if ($667) {
      break;
     } else {
      $sp$084$i = $666;
     }
    }
    if ((label|0) == 204) {
     $668 = ((($sp$084$i$lcssa)) + 12|0);
     $669 = HEAP32[$668>>2]|0;
     $670 = $669 & 8;
     $671 = ($670|0)==(0);
     if ($671) {
      $672 = ($635>>>0)>=($$lcssa222>>>0);
      $673 = ($635>>>0)<($tbase$255$i>>>0);
      $or$cond57$i = $673 & $672;
      if ($or$cond57$i) {
       $674 = (($$lcssa226) + ($tsize$254$i))|0;
       HEAP32[$$lcssa224>>2] = $674;
       $675 = HEAP32[(20588)>>2]|0;
       $676 = (($675) + ($tsize$254$i))|0;
       $677 = ((($635)) + 8|0);
       $678 = $677;
       $679 = $678 & 7;
       $680 = ($679|0)==(0);
       $681 = (0 - ($678))|0;
       $682 = $681 & 7;
       $683 = $680 ? 0 : $682;
       $684 = (($635) + ($683)|0);
       $685 = (($676) - ($683))|0;
       HEAP32[(20600)>>2] = $684;
       HEAP32[(20588)>>2] = $685;
       $686 = $685 | 1;
       $$sum$i17$i = (($683) + 4)|0;
       $687 = (($635) + ($$sum$i17$i)|0);
       HEAP32[$687>>2] = $686;
       $$sum2$i18$i = (($676) + 4)|0;
       $688 = (($635) + ($$sum2$i18$i)|0);
       HEAP32[$688>>2] = 40;
       $689 = HEAP32[(21064)>>2]|0;
       HEAP32[(20604)>>2] = $689;
       break;
      }
     }
    }
    $690 = HEAP32[(20592)>>2]|0;
    $691 = ($tbase$255$i>>>0)<($690>>>0);
    if ($691) {
     HEAP32[(20592)>>2] = $tbase$255$i;
     $755 = $tbase$255$i;
    } else {
     $755 = $690;
    }
    $692 = (($tbase$255$i) + ($tsize$254$i)|0);
    $sp$183$i = (21024);
    while(1) {
     $693 = HEAP32[$sp$183$i>>2]|0;
     $694 = ($693|0)==($692|0);
     if ($694) {
      $$lcssa219 = $sp$183$i;$sp$183$i$lcssa = $sp$183$i;
      label = 212;
      break;
     }
     $695 = ((($sp$183$i)) + 8|0);
     $696 = HEAP32[$695>>2]|0;
     $697 = ($696|0)==(0|0);
     if ($697) {
      $sp$0$i$i$i = (21024);
      break;
     } else {
      $sp$183$i = $696;
     }
    }
    if ((label|0) == 212) {
     $698 = ((($sp$183$i$lcssa)) + 12|0);
     $699 = HEAP32[$698>>2]|0;
     $700 = $699 & 8;
     $701 = ($700|0)==(0);
     if ($701) {
      HEAP32[$$lcssa219>>2] = $tbase$255$i;
      $702 = ((($sp$183$i$lcssa)) + 4|0);
      $703 = HEAP32[$702>>2]|0;
      $704 = (($703) + ($tsize$254$i))|0;
      HEAP32[$702>>2] = $704;
      $705 = ((($tbase$255$i)) + 8|0);
      $706 = $705;
      $707 = $706 & 7;
      $708 = ($707|0)==(0);
      $709 = (0 - ($706))|0;
      $710 = $709 & 7;
      $711 = $708 ? 0 : $710;
      $712 = (($tbase$255$i) + ($711)|0);
      $$sum112$i = (($tsize$254$i) + 8)|0;
      $713 = (($tbase$255$i) + ($$sum112$i)|0);
      $714 = $713;
      $715 = $714 & 7;
      $716 = ($715|0)==(0);
      $717 = (0 - ($714))|0;
      $718 = $717 & 7;
      $719 = $716 ? 0 : $718;
      $$sum113$i = (($719) + ($tsize$254$i))|0;
      $720 = (($tbase$255$i) + ($$sum113$i)|0);
      $721 = $720;
      $722 = $712;
      $723 = (($721) - ($722))|0;
      $$sum$i19$i = (($711) + ($nb$0))|0;
      $724 = (($tbase$255$i) + ($$sum$i19$i)|0);
      $725 = (($723) - ($nb$0))|0;
      $726 = $nb$0 | 3;
      $$sum1$i20$i = (($711) + 4)|0;
      $727 = (($tbase$255$i) + ($$sum1$i20$i)|0);
      HEAP32[$727>>2] = $726;
      $728 = ($720|0)==($635|0);
      L324: do {
       if ($728) {
        $729 = HEAP32[(20588)>>2]|0;
        $730 = (($729) + ($725))|0;
        HEAP32[(20588)>>2] = $730;
        HEAP32[(20600)>>2] = $724;
        $731 = $730 | 1;
        $$sum42$i$i = (($$sum$i19$i) + 4)|0;
        $732 = (($tbase$255$i) + ($$sum42$i$i)|0);
        HEAP32[$732>>2] = $731;
       } else {
        $733 = HEAP32[(20596)>>2]|0;
        $734 = ($720|0)==($733|0);
        if ($734) {
         $735 = HEAP32[(20584)>>2]|0;
         $736 = (($735) + ($725))|0;
         HEAP32[(20584)>>2] = $736;
         HEAP32[(20596)>>2] = $724;
         $737 = $736 | 1;
         $$sum40$i$i = (($$sum$i19$i) + 4)|0;
         $738 = (($tbase$255$i) + ($$sum40$i$i)|0);
         HEAP32[$738>>2] = $737;
         $$sum41$i$i = (($736) + ($$sum$i19$i))|0;
         $739 = (($tbase$255$i) + ($$sum41$i$i)|0);
         HEAP32[$739>>2] = $736;
         break;
        }
        $$sum2$i21$i = (($tsize$254$i) + 4)|0;
        $$sum114$i = (($$sum2$i21$i) + ($719))|0;
        $740 = (($tbase$255$i) + ($$sum114$i)|0);
        $741 = HEAP32[$740>>2]|0;
        $742 = $741 & 3;
        $743 = ($742|0)==(1);
        if ($743) {
         $744 = $741 & -8;
         $745 = $741 >>> 3;
         $746 = ($741>>>0)<(256);
         L332: do {
          if ($746) {
           $$sum3738$i$i = $719 | 8;
           $$sum124$i = (($$sum3738$i$i) + ($tsize$254$i))|0;
           $747 = (($tbase$255$i) + ($$sum124$i)|0);
           $748 = HEAP32[$747>>2]|0;
           $$sum39$i$i = (($tsize$254$i) + 12)|0;
           $$sum125$i = (($$sum39$i$i) + ($719))|0;
           $749 = (($tbase$255$i) + ($$sum125$i)|0);
           $750 = HEAP32[$749>>2]|0;
           $751 = $745 << 1;
           $752 = (20616 + ($751<<2)|0);
           $753 = ($748|0)==($752|0);
           do {
            if (!($753)) {
             $754 = ($748>>>0)<($755>>>0);
             if ($754) {
              _abort();
              // unreachable;
             }
             $756 = ((($748)) + 12|0);
             $757 = HEAP32[$756>>2]|0;
             $758 = ($757|0)==($720|0);
             if ($758) {
              break;
             }
             _abort();
             // unreachable;
            }
           } while(0);
           $759 = ($750|0)==($748|0);
           if ($759) {
            $760 = 1 << $745;
            $761 = $760 ^ -1;
            $762 = HEAP32[20576>>2]|0;
            $763 = $762 & $761;
            HEAP32[20576>>2] = $763;
            break;
           }
           $764 = ($750|0)==($752|0);
           do {
            if ($764) {
             $$pre57$i$i = ((($750)) + 8|0);
             $$pre$phi58$i$iZ2D = $$pre57$i$i;
            } else {
             $765 = ($750>>>0)<($755>>>0);
             if ($765) {
              _abort();
              // unreachable;
             }
             $766 = ((($750)) + 8|0);
             $767 = HEAP32[$766>>2]|0;
             $768 = ($767|0)==($720|0);
             if ($768) {
              $$pre$phi58$i$iZ2D = $766;
              break;
             }
             _abort();
             // unreachable;
            }
           } while(0);
           $769 = ((($748)) + 12|0);
           HEAP32[$769>>2] = $750;
           HEAP32[$$pre$phi58$i$iZ2D>>2] = $748;
          } else {
           $$sum34$i$i = $719 | 24;
           $$sum115$i = (($$sum34$i$i) + ($tsize$254$i))|0;
           $770 = (($tbase$255$i) + ($$sum115$i)|0);
           $771 = HEAP32[$770>>2]|0;
           $$sum5$i$i = (($tsize$254$i) + 12)|0;
           $$sum116$i = (($$sum5$i$i) + ($719))|0;
           $772 = (($tbase$255$i) + ($$sum116$i)|0);
           $773 = HEAP32[$772>>2]|0;
           $774 = ($773|0)==($720|0);
           do {
            if ($774) {
             $$sum67$i$i = $719 | 16;
             $$sum122$i = (($$sum2$i21$i) + ($$sum67$i$i))|0;
             $784 = (($tbase$255$i) + ($$sum122$i)|0);
             $785 = HEAP32[$784>>2]|0;
             $786 = ($785|0)==(0|0);
             if ($786) {
              $$sum123$i = (($$sum67$i$i) + ($tsize$254$i))|0;
              $787 = (($tbase$255$i) + ($$sum123$i)|0);
              $788 = HEAP32[$787>>2]|0;
              $789 = ($788|0)==(0|0);
              if ($789) {
               $R$1$i$i = 0;
               break;
              } else {
               $R$0$i$i = $788;$RP$0$i$i = $787;
              }
             } else {
              $R$0$i$i = $785;$RP$0$i$i = $784;
             }
             while(1) {
              $790 = ((($R$0$i$i)) + 20|0);
              $791 = HEAP32[$790>>2]|0;
              $792 = ($791|0)==(0|0);
              if (!($792)) {
               $R$0$i$i = $791;$RP$0$i$i = $790;
               continue;
              }
              $793 = ((($R$0$i$i)) + 16|0);
              $794 = HEAP32[$793>>2]|0;
              $795 = ($794|0)==(0|0);
              if ($795) {
               $R$0$i$i$lcssa = $R$0$i$i;$RP$0$i$i$lcssa = $RP$0$i$i;
               break;
              } else {
               $R$0$i$i = $794;$RP$0$i$i = $793;
              }
             }
             $796 = ($RP$0$i$i$lcssa>>>0)<($755>>>0);
             if ($796) {
              _abort();
              // unreachable;
             } else {
              HEAP32[$RP$0$i$i$lcssa>>2] = 0;
              $R$1$i$i = $R$0$i$i$lcssa;
              break;
             }
            } else {
             $$sum3536$i$i = $719 | 8;
             $$sum117$i = (($$sum3536$i$i) + ($tsize$254$i))|0;
             $775 = (($tbase$255$i) + ($$sum117$i)|0);
             $776 = HEAP32[$775>>2]|0;
             $777 = ($776>>>0)<($755>>>0);
             if ($777) {
              _abort();
              // unreachable;
             }
             $778 = ((($776)) + 12|0);
             $779 = HEAP32[$778>>2]|0;
             $780 = ($779|0)==($720|0);
             if (!($780)) {
              _abort();
              // unreachable;
             }
             $781 = ((($773)) + 8|0);
             $782 = HEAP32[$781>>2]|0;
             $783 = ($782|0)==($720|0);
             if ($783) {
              HEAP32[$778>>2] = $773;
              HEAP32[$781>>2] = $776;
              $R$1$i$i = $773;
              break;
             } else {
              _abort();
              // unreachable;
             }
            }
           } while(0);
           $797 = ($771|0)==(0|0);
           if ($797) {
            break;
           }
           $$sum30$i$i = (($tsize$254$i) + 28)|0;
           $$sum118$i = (($$sum30$i$i) + ($719))|0;
           $798 = (($tbase$255$i) + ($$sum118$i)|0);
           $799 = HEAP32[$798>>2]|0;
           $800 = (20880 + ($799<<2)|0);
           $801 = HEAP32[$800>>2]|0;
           $802 = ($720|0)==($801|0);
           do {
            if ($802) {
             HEAP32[$800>>2] = $R$1$i$i;
             $cond$i$i = ($R$1$i$i|0)==(0|0);
             if (!($cond$i$i)) {
              break;
             }
             $803 = 1 << $799;
             $804 = $803 ^ -1;
             $805 = HEAP32[(20580)>>2]|0;
             $806 = $805 & $804;
             HEAP32[(20580)>>2] = $806;
             break L332;
            } else {
             $807 = HEAP32[(20592)>>2]|0;
             $808 = ($771>>>0)<($807>>>0);
             if ($808) {
              _abort();
              // unreachable;
             }
             $809 = ((($771)) + 16|0);
             $810 = HEAP32[$809>>2]|0;
             $811 = ($810|0)==($720|0);
             if ($811) {
              HEAP32[$809>>2] = $R$1$i$i;
             } else {
              $812 = ((($771)) + 20|0);
              HEAP32[$812>>2] = $R$1$i$i;
             }
             $813 = ($R$1$i$i|0)==(0|0);
             if ($813) {
              break L332;
             }
            }
           } while(0);
           $814 = HEAP32[(20592)>>2]|0;
           $815 = ($R$1$i$i>>>0)<($814>>>0);
           if ($815) {
            _abort();
            // unreachable;
           }
           $816 = ((($R$1$i$i)) + 24|0);
           HEAP32[$816>>2] = $771;
           $$sum3132$i$i = $719 | 16;
           $$sum119$i = (($$sum3132$i$i) + ($tsize$254$i))|0;
           $817 = (($tbase$255$i) + ($$sum119$i)|0);
           $818 = HEAP32[$817>>2]|0;
           $819 = ($818|0)==(0|0);
           do {
            if (!($819)) {
             $820 = ($818>>>0)<($814>>>0);
             if ($820) {
              _abort();
              // unreachable;
             } else {
              $821 = ((($R$1$i$i)) + 16|0);
              HEAP32[$821>>2] = $818;
              $822 = ((($818)) + 24|0);
              HEAP32[$822>>2] = $R$1$i$i;
              break;
             }
            }
           } while(0);
           $$sum120$i = (($$sum2$i21$i) + ($$sum3132$i$i))|0;
           $823 = (($tbase$255$i) + ($$sum120$i)|0);
           $824 = HEAP32[$823>>2]|0;
           $825 = ($824|0)==(0|0);
           if ($825) {
            break;
           }
           $826 = HEAP32[(20592)>>2]|0;
           $827 = ($824>>>0)<($826>>>0);
           if ($827) {
            _abort();
            // unreachable;
           } else {
            $828 = ((($R$1$i$i)) + 20|0);
            HEAP32[$828>>2] = $824;
            $829 = ((($824)) + 24|0);
            HEAP32[$829>>2] = $R$1$i$i;
            break;
           }
          }
         } while(0);
         $$sum9$i$i = $744 | $719;
         $$sum121$i = (($$sum9$i$i) + ($tsize$254$i))|0;
         $830 = (($tbase$255$i) + ($$sum121$i)|0);
         $831 = (($744) + ($725))|0;
         $oldfirst$0$i$i = $830;$qsize$0$i$i = $831;
        } else {
         $oldfirst$0$i$i = $720;$qsize$0$i$i = $725;
        }
        $832 = ((($oldfirst$0$i$i)) + 4|0);
        $833 = HEAP32[$832>>2]|0;
        $834 = $833 & -2;
        HEAP32[$832>>2] = $834;
        $835 = $qsize$0$i$i | 1;
        $$sum10$i$i = (($$sum$i19$i) + 4)|0;
        $836 = (($tbase$255$i) + ($$sum10$i$i)|0);
        HEAP32[$836>>2] = $835;
        $$sum11$i$i = (($qsize$0$i$i) + ($$sum$i19$i))|0;
        $837 = (($tbase$255$i) + ($$sum11$i$i)|0);
        HEAP32[$837>>2] = $qsize$0$i$i;
        $838 = $qsize$0$i$i >>> 3;
        $839 = ($qsize$0$i$i>>>0)<(256);
        if ($839) {
         $840 = $838 << 1;
         $841 = (20616 + ($840<<2)|0);
         $842 = HEAP32[20576>>2]|0;
         $843 = 1 << $838;
         $844 = $842 & $843;
         $845 = ($844|0)==(0);
         do {
          if ($845) {
           $846 = $842 | $843;
           HEAP32[20576>>2] = $846;
           $$pre$i22$i = (($840) + 2)|0;
           $$pre56$i$i = (20616 + ($$pre$i22$i<<2)|0);
           $$pre$phi$i23$iZ2D = $$pre56$i$i;$F4$0$i$i = $841;
          } else {
           $$sum29$i$i = (($840) + 2)|0;
           $847 = (20616 + ($$sum29$i$i<<2)|0);
           $848 = HEAP32[$847>>2]|0;
           $849 = HEAP32[(20592)>>2]|0;
           $850 = ($848>>>0)<($849>>>0);
           if (!($850)) {
            $$pre$phi$i23$iZ2D = $847;$F4$0$i$i = $848;
            break;
           }
           _abort();
           // unreachable;
          }
         } while(0);
         HEAP32[$$pre$phi$i23$iZ2D>>2] = $724;
         $851 = ((($F4$0$i$i)) + 12|0);
         HEAP32[$851>>2] = $724;
         $$sum27$i$i = (($$sum$i19$i) + 8)|0;
         $852 = (($tbase$255$i) + ($$sum27$i$i)|0);
         HEAP32[$852>>2] = $F4$0$i$i;
         $$sum28$i$i = (($$sum$i19$i) + 12)|0;
         $853 = (($tbase$255$i) + ($$sum28$i$i)|0);
         HEAP32[$853>>2] = $841;
         break;
        }
        $854 = $qsize$0$i$i >>> 8;
        $855 = ($854|0)==(0);
        do {
         if ($855) {
          $I7$0$i$i = 0;
         } else {
          $856 = ($qsize$0$i$i>>>0)>(16777215);
          if ($856) {
           $I7$0$i$i = 31;
           break;
          }
          $857 = (($854) + 1048320)|0;
          $858 = $857 >>> 16;
          $859 = $858 & 8;
          $860 = $854 << $859;
          $861 = (($860) + 520192)|0;
          $862 = $861 >>> 16;
          $863 = $862 & 4;
          $864 = $863 | $859;
          $865 = $860 << $863;
          $866 = (($865) + 245760)|0;
          $867 = $866 >>> 16;
          $868 = $867 & 2;
          $869 = $864 | $868;
          $870 = (14 - ($869))|0;
          $871 = $865 << $868;
          $872 = $871 >>> 15;
          $873 = (($870) + ($872))|0;
          $874 = $873 << 1;
          $875 = (($873) + 7)|0;
          $876 = $qsize$0$i$i >>> $875;
          $877 = $876 & 1;
          $878 = $877 | $874;
          $I7$0$i$i = $878;
         }
        } while(0);
        $879 = (20880 + ($I7$0$i$i<<2)|0);
        $$sum12$i$i = (($$sum$i19$i) + 28)|0;
        $880 = (($tbase$255$i) + ($$sum12$i$i)|0);
        HEAP32[$880>>2] = $I7$0$i$i;
        $$sum13$i$i = (($$sum$i19$i) + 16)|0;
        $881 = (($tbase$255$i) + ($$sum13$i$i)|0);
        $$sum14$i$i = (($$sum$i19$i) + 20)|0;
        $882 = (($tbase$255$i) + ($$sum14$i$i)|0);
        HEAP32[$882>>2] = 0;
        HEAP32[$881>>2] = 0;
        $883 = HEAP32[(20580)>>2]|0;
        $884 = 1 << $I7$0$i$i;
        $885 = $883 & $884;
        $886 = ($885|0)==(0);
        if ($886) {
         $887 = $883 | $884;
         HEAP32[(20580)>>2] = $887;
         HEAP32[$879>>2] = $724;
         $$sum15$i$i = (($$sum$i19$i) + 24)|0;
         $888 = (($tbase$255$i) + ($$sum15$i$i)|0);
         HEAP32[$888>>2] = $879;
         $$sum16$i$i = (($$sum$i19$i) + 12)|0;
         $889 = (($tbase$255$i) + ($$sum16$i$i)|0);
         HEAP32[$889>>2] = $724;
         $$sum17$i$i = (($$sum$i19$i) + 8)|0;
         $890 = (($tbase$255$i) + ($$sum17$i$i)|0);
         HEAP32[$890>>2] = $724;
         break;
        }
        $891 = HEAP32[$879>>2]|0;
        $892 = ((($891)) + 4|0);
        $893 = HEAP32[$892>>2]|0;
        $894 = $893 & -8;
        $895 = ($894|0)==($qsize$0$i$i|0);
        L418: do {
         if ($895) {
          $T$0$lcssa$i25$i = $891;
         } else {
          $896 = ($I7$0$i$i|0)==(31);
          $897 = $I7$0$i$i >>> 1;
          $898 = (25 - ($897))|0;
          $899 = $896 ? 0 : $898;
          $900 = $qsize$0$i$i << $899;
          $K8$051$i$i = $900;$T$050$i$i = $891;
          while(1) {
           $907 = $K8$051$i$i >>> 31;
           $908 = (((($T$050$i$i)) + 16|0) + ($907<<2)|0);
           $903 = HEAP32[$908>>2]|0;
           $909 = ($903|0)==(0|0);
           if ($909) {
            $$lcssa = $908;$T$050$i$i$lcssa = $T$050$i$i;
            break;
           }
           $901 = $K8$051$i$i << 1;
           $902 = ((($903)) + 4|0);
           $904 = HEAP32[$902>>2]|0;
           $905 = $904 & -8;
           $906 = ($905|0)==($qsize$0$i$i|0);
           if ($906) {
            $T$0$lcssa$i25$i = $903;
            break L418;
           } else {
            $K8$051$i$i = $901;$T$050$i$i = $903;
           }
          }
          $910 = HEAP32[(20592)>>2]|0;
          $911 = ($$lcssa>>>0)<($910>>>0);
          if ($911) {
           _abort();
           // unreachable;
          } else {
           HEAP32[$$lcssa>>2] = $724;
           $$sum23$i$i = (($$sum$i19$i) + 24)|0;
           $912 = (($tbase$255$i) + ($$sum23$i$i)|0);
           HEAP32[$912>>2] = $T$050$i$i$lcssa;
           $$sum24$i$i = (($$sum$i19$i) + 12)|0;
           $913 = (($tbase$255$i) + ($$sum24$i$i)|0);
           HEAP32[$913>>2] = $724;
           $$sum25$i$i = (($$sum$i19$i) + 8)|0;
           $914 = (($tbase$255$i) + ($$sum25$i$i)|0);
           HEAP32[$914>>2] = $724;
           break L324;
          }
         }
        } while(0);
        $915 = ((($T$0$lcssa$i25$i)) + 8|0);
        $916 = HEAP32[$915>>2]|0;
        $917 = HEAP32[(20592)>>2]|0;
        $918 = ($916>>>0)>=($917>>>0);
        $not$$i26$i = ($T$0$lcssa$i25$i>>>0)>=($917>>>0);
        $919 = $918 & $not$$i26$i;
        if ($919) {
         $920 = ((($916)) + 12|0);
         HEAP32[$920>>2] = $724;
         HEAP32[$915>>2] = $724;
         $$sum20$i$i = (($$sum$i19$i) + 8)|0;
         $921 = (($tbase$255$i) + ($$sum20$i$i)|0);
         HEAP32[$921>>2] = $916;
         $$sum21$i$i = (($$sum$i19$i) + 12)|0;
         $922 = (($tbase$255$i) + ($$sum21$i$i)|0);
         HEAP32[$922>>2] = $T$0$lcssa$i25$i;
         $$sum22$i$i = (($$sum$i19$i) + 24)|0;
         $923 = (($tbase$255$i) + ($$sum22$i$i)|0);
         HEAP32[$923>>2] = 0;
         break;
        } else {
         _abort();
         // unreachable;
        }
       }
      } while(0);
      $$sum1819$i$i = $711 | 8;
      $924 = (($tbase$255$i) + ($$sum1819$i$i)|0);
      $mem$0 = $924;
      return ($mem$0|0);
     } else {
      $sp$0$i$i$i = (21024);
     }
    }
    while(1) {
     $925 = HEAP32[$sp$0$i$i$i>>2]|0;
     $926 = ($925>>>0)>($635>>>0);
     if (!($926)) {
      $927 = ((($sp$0$i$i$i)) + 4|0);
      $928 = HEAP32[$927>>2]|0;
      $929 = (($925) + ($928)|0);
      $930 = ($929>>>0)>($635>>>0);
      if ($930) {
       $$lcssa215 = $925;$$lcssa216 = $928;$$lcssa217 = $929;
       break;
      }
     }
     $931 = ((($sp$0$i$i$i)) + 8|0);
     $932 = HEAP32[$931>>2]|0;
     $sp$0$i$i$i = $932;
    }
    $$sum$i14$i = (($$lcssa216) + -47)|0;
    $$sum1$i15$i = (($$lcssa216) + -39)|0;
    $933 = (($$lcssa215) + ($$sum1$i15$i)|0);
    $934 = $933;
    $935 = $934 & 7;
    $936 = ($935|0)==(0);
    $937 = (0 - ($934))|0;
    $938 = $937 & 7;
    $939 = $936 ? 0 : $938;
    $$sum2$i16$i = (($$sum$i14$i) + ($939))|0;
    $940 = (($$lcssa215) + ($$sum2$i16$i)|0);
    $941 = ((($635)) + 16|0);
    $942 = ($940>>>0)<($941>>>0);
    $943 = $942 ? $635 : $940;
    $944 = ((($943)) + 8|0);
    $945 = (($tsize$254$i) + -40)|0;
    $946 = ((($tbase$255$i)) + 8|0);
    $947 = $946;
    $948 = $947 & 7;
    $949 = ($948|0)==(0);
    $950 = (0 - ($947))|0;
    $951 = $950 & 7;
    $952 = $949 ? 0 : $951;
    $953 = (($tbase$255$i) + ($952)|0);
    $954 = (($945) - ($952))|0;
    HEAP32[(20600)>>2] = $953;
    HEAP32[(20588)>>2] = $954;
    $955 = $954 | 1;
    $$sum$i$i$i = (($952) + 4)|0;
    $956 = (($tbase$255$i) + ($$sum$i$i$i)|0);
    HEAP32[$956>>2] = $955;
    $$sum2$i$i$i = (($tsize$254$i) + -36)|0;
    $957 = (($tbase$255$i) + ($$sum2$i$i$i)|0);
    HEAP32[$957>>2] = 40;
    $958 = HEAP32[(21064)>>2]|0;
    HEAP32[(20604)>>2] = $958;
    $959 = ((($943)) + 4|0);
    HEAP32[$959>>2] = 27;
    ;HEAP32[$944>>2]=HEAP32[(21024)>>2]|0;HEAP32[$944+4>>2]=HEAP32[(21024)+4>>2]|0;HEAP32[$944+8>>2]=HEAP32[(21024)+8>>2]|0;HEAP32[$944+12>>2]=HEAP32[(21024)+12>>2]|0;
    HEAP32[(21024)>>2] = $tbase$255$i;
    HEAP32[(21028)>>2] = $tsize$254$i;
    HEAP32[(21036)>>2] = 0;
    HEAP32[(21032)>>2] = $944;
    $960 = ((($943)) + 28|0);
    HEAP32[$960>>2] = 7;
    $961 = ((($943)) + 32|0);
    $962 = ($961>>>0)<($$lcssa217>>>0);
    if ($962) {
     $964 = $960;
     while(1) {
      $963 = ((($964)) + 4|0);
      HEAP32[$963>>2] = 7;
      $965 = ((($964)) + 8|0);
      $966 = ($965>>>0)<($$lcssa217>>>0);
      if ($966) {
       $964 = $963;
      } else {
       break;
      }
     }
    }
    $967 = ($943|0)==($635|0);
    if (!($967)) {
     $968 = $943;
     $969 = $635;
     $970 = (($968) - ($969))|0;
     $971 = HEAP32[$959>>2]|0;
     $972 = $971 & -2;
     HEAP32[$959>>2] = $972;
     $973 = $970 | 1;
     $974 = ((($635)) + 4|0);
     HEAP32[$974>>2] = $973;
     HEAP32[$943>>2] = $970;
     $975 = $970 >>> 3;
     $976 = ($970>>>0)<(256);
     if ($976) {
      $977 = $975 << 1;
      $978 = (20616 + ($977<<2)|0);
      $979 = HEAP32[20576>>2]|0;
      $980 = 1 << $975;
      $981 = $979 & $980;
      $982 = ($981|0)==(0);
      if ($982) {
       $983 = $979 | $980;
       HEAP32[20576>>2] = $983;
       $$pre$i$i = (($977) + 2)|0;
       $$pre14$i$i = (20616 + ($$pre$i$i<<2)|0);
       $$pre$phi$i$iZ2D = $$pre14$i$i;$F$0$i$i = $978;
      } else {
       $$sum4$i$i = (($977) + 2)|0;
       $984 = (20616 + ($$sum4$i$i<<2)|0);
       $985 = HEAP32[$984>>2]|0;
       $986 = HEAP32[(20592)>>2]|0;
       $987 = ($985>>>0)<($986>>>0);
       if ($987) {
        _abort();
        // unreachable;
       } else {
        $$pre$phi$i$iZ2D = $984;$F$0$i$i = $985;
       }
      }
      HEAP32[$$pre$phi$i$iZ2D>>2] = $635;
      $988 = ((($F$0$i$i)) + 12|0);
      HEAP32[$988>>2] = $635;
      $989 = ((($635)) + 8|0);
      HEAP32[$989>>2] = $F$0$i$i;
      $990 = ((($635)) + 12|0);
      HEAP32[$990>>2] = $978;
      break;
     }
     $991 = $970 >>> 8;
     $992 = ($991|0)==(0);
     if ($992) {
      $I1$0$i$i = 0;
     } else {
      $993 = ($970>>>0)>(16777215);
      if ($993) {
       $I1$0$i$i = 31;
      } else {
       $994 = (($991) + 1048320)|0;
       $995 = $994 >>> 16;
       $996 = $995 & 8;
       $997 = $991 << $996;
       $998 = (($997) + 520192)|0;
       $999 = $998 >>> 16;
       $1000 = $999 & 4;
       $1001 = $1000 | $996;
       $1002 = $997 << $1000;
       $1003 = (($1002) + 245760)|0;
       $1004 = $1003 >>> 16;
       $1005 = $1004 & 2;
       $1006 = $1001 | $1005;
       $1007 = (14 - ($1006))|0;
       $1008 = $1002 << $1005;
       $1009 = $1008 >>> 15;
       $1010 = (($1007) + ($1009))|0;
       $1011 = $1010 << 1;
       $1012 = (($1010) + 7)|0;
       $1013 = $970 >>> $1012;
       $1014 = $1013 & 1;
       $1015 = $1014 | $1011;
       $I1$0$i$i = $1015;
      }
     }
     $1016 = (20880 + ($I1$0$i$i<<2)|0);
     $1017 = ((($635)) + 28|0);
     HEAP32[$1017>>2] = $I1$0$i$i;
     $1018 = ((($635)) + 20|0);
     HEAP32[$1018>>2] = 0;
     HEAP32[$941>>2] = 0;
     $1019 = HEAP32[(20580)>>2]|0;
     $1020 = 1 << $I1$0$i$i;
     $1021 = $1019 & $1020;
     $1022 = ($1021|0)==(0);
     if ($1022) {
      $1023 = $1019 | $1020;
      HEAP32[(20580)>>2] = $1023;
      HEAP32[$1016>>2] = $635;
      $1024 = ((($635)) + 24|0);
      HEAP32[$1024>>2] = $1016;
      $1025 = ((($635)) + 12|0);
      HEAP32[$1025>>2] = $635;
      $1026 = ((($635)) + 8|0);
      HEAP32[$1026>>2] = $635;
      break;
     }
     $1027 = HEAP32[$1016>>2]|0;
     $1028 = ((($1027)) + 4|0);
     $1029 = HEAP32[$1028>>2]|0;
     $1030 = $1029 & -8;
     $1031 = ($1030|0)==($970|0);
     L459: do {
      if ($1031) {
       $T$0$lcssa$i$i = $1027;
      } else {
       $1032 = ($I1$0$i$i|0)==(31);
       $1033 = $I1$0$i$i >>> 1;
       $1034 = (25 - ($1033))|0;
       $1035 = $1032 ? 0 : $1034;
       $1036 = $970 << $1035;
       $K2$07$i$i = $1036;$T$06$i$i = $1027;
       while(1) {
        $1043 = $K2$07$i$i >>> 31;
        $1044 = (((($T$06$i$i)) + 16|0) + ($1043<<2)|0);
        $1039 = HEAP32[$1044>>2]|0;
        $1045 = ($1039|0)==(0|0);
        if ($1045) {
         $$lcssa211 = $1044;$T$06$i$i$lcssa = $T$06$i$i;
         break;
        }
        $1037 = $K2$07$i$i << 1;
        $1038 = ((($1039)) + 4|0);
        $1040 = HEAP32[$1038>>2]|0;
        $1041 = $1040 & -8;
        $1042 = ($1041|0)==($970|0);
        if ($1042) {
         $T$0$lcssa$i$i = $1039;
         break L459;
        } else {
         $K2$07$i$i = $1037;$T$06$i$i = $1039;
        }
       }
       $1046 = HEAP32[(20592)>>2]|0;
       $1047 = ($$lcssa211>>>0)<($1046>>>0);
       if ($1047) {
        _abort();
        // unreachable;
       } else {
        HEAP32[$$lcssa211>>2] = $635;
        $1048 = ((($635)) + 24|0);
        HEAP32[$1048>>2] = $T$06$i$i$lcssa;
        $1049 = ((($635)) + 12|0);
        HEAP32[$1049>>2] = $635;
        $1050 = ((($635)) + 8|0);
        HEAP32[$1050>>2] = $635;
        break L299;
       }
      }
     } while(0);
     $1051 = ((($T$0$lcssa$i$i)) + 8|0);
     $1052 = HEAP32[$1051>>2]|0;
     $1053 = HEAP32[(20592)>>2]|0;
     $1054 = ($1052>>>0)>=($1053>>>0);
     $not$$i$i = ($T$0$lcssa$i$i>>>0)>=($1053>>>0);
     $1055 = $1054 & $not$$i$i;
     if ($1055) {
      $1056 = ((($1052)) + 12|0);
      HEAP32[$1056>>2] = $635;
      HEAP32[$1051>>2] = $635;
      $1057 = ((($635)) + 8|0);
      HEAP32[$1057>>2] = $1052;
      $1058 = ((($635)) + 12|0);
      HEAP32[$1058>>2] = $T$0$lcssa$i$i;
      $1059 = ((($635)) + 24|0);
      HEAP32[$1059>>2] = 0;
      break;
     } else {
      _abort();
      // unreachable;
     }
    }
   }
  } while(0);
  $1060 = HEAP32[(20588)>>2]|0;
  $1061 = ($1060>>>0)>($nb$0>>>0);
  if ($1061) {
   $1062 = (($1060) - ($nb$0))|0;
   HEAP32[(20588)>>2] = $1062;
   $1063 = HEAP32[(20600)>>2]|0;
   $1064 = (($1063) + ($nb$0)|0);
   HEAP32[(20600)>>2] = $1064;
   $1065 = $1062 | 1;
   $$sum$i32 = (($nb$0) + 4)|0;
   $1066 = (($1063) + ($$sum$i32)|0);
   HEAP32[$1066>>2] = $1065;
   $1067 = $nb$0 | 3;
   $1068 = ((($1063)) + 4|0);
   HEAP32[$1068>>2] = $1067;
   $1069 = ((($1063)) + 8|0);
   $mem$0 = $1069;
   return ($mem$0|0);
  }
 }
 $1070 = (___errno_location()|0);
 HEAP32[$1070>>2] = 12;
 $mem$0 = 0;
 return ($mem$0|0);
}
function _free($mem) {
 $mem = $mem|0;
 var $$lcssa = 0, $$pre = 0, $$pre$phi59Z2D = 0, $$pre$phi61Z2D = 0, $$pre$phiZ2D = 0, $$pre57 = 0, $$pre58 = 0, $$pre60 = 0, $$sum = 0, $$sum11 = 0, $$sum12 = 0, $$sum13 = 0, $$sum14 = 0, $$sum1718 = 0, $$sum19 = 0, $$sum2 = 0, $$sum20 = 0, $$sum22 = 0, $$sum23 = 0, $$sum24 = 0;
 var $$sum25 = 0, $$sum26 = 0, $$sum27 = 0, $$sum28 = 0, $$sum29 = 0, $$sum3 = 0, $$sum30 = 0, $$sum31 = 0, $$sum5 = 0, $$sum67 = 0, $$sum8 = 0, $$sum9 = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0;
 var $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0, $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0;
 var $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0, $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0;
 var $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0, $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0;
 var $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0, $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0;
 var $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0, $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0;
 var $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0, $205 = 0, $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0, $212 = 0;
 var $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0, $220 = 0, $221 = 0, $222 = 0, $223 = 0, $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0, $230 = 0;
 var $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0, $241 = 0, $242 = 0, $243 = 0, $244 = 0, $245 = 0, $246 = 0, $247 = 0, $248 = 0, $249 = 0;
 var $25 = 0, $250 = 0, $251 = 0, $252 = 0, $253 = 0, $254 = 0, $255 = 0, $256 = 0, $257 = 0, $258 = 0, $259 = 0, $26 = 0, $260 = 0, $261 = 0, $262 = 0, $263 = 0, $264 = 0, $265 = 0, $266 = 0, $267 = 0;
 var $268 = 0, $269 = 0, $27 = 0, $270 = 0, $271 = 0, $272 = 0, $273 = 0, $274 = 0, $275 = 0, $276 = 0, $277 = 0, $278 = 0, $279 = 0, $28 = 0, $280 = 0, $281 = 0, $282 = 0, $283 = 0, $284 = 0, $285 = 0;
 var $286 = 0, $287 = 0, $288 = 0, $289 = 0, $29 = 0, $290 = 0, $291 = 0, $292 = 0, $293 = 0, $294 = 0, $295 = 0, $296 = 0, $297 = 0, $298 = 0, $299 = 0, $3 = 0, $30 = 0, $300 = 0, $301 = 0, $302 = 0;
 var $303 = 0, $304 = 0, $305 = 0, $306 = 0, $307 = 0, $308 = 0, $309 = 0, $31 = 0, $310 = 0, $311 = 0, $312 = 0, $313 = 0, $314 = 0, $315 = 0, $316 = 0, $317 = 0, $318 = 0, $319 = 0, $32 = 0, $320 = 0;
 var $321 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0;
 var $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0;
 var $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0;
 var $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $F16$0 = 0, $I18$0 = 0, $K19$052 = 0, $R$0 = 0, $R$0$lcssa = 0, $R$1 = 0;
 var $R7$0 = 0, $R7$0$lcssa = 0, $R7$1 = 0, $RP$0 = 0, $RP$0$lcssa = 0, $RP9$0 = 0, $RP9$0$lcssa = 0, $T$0$lcssa = 0, $T$051 = 0, $T$051$lcssa = 0, $cond = 0, $cond47 = 0, $not$ = 0, $p$0 = 0, $psize$0 = 0, $psize$1 = 0, $sp$0$i = 0, $sp$0$in$i = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($mem|0)==(0|0);
 if ($0) {
  return;
 }
 $1 = ((($mem)) + -8|0);
 $2 = HEAP32[(20592)>>2]|0;
 $3 = ($1>>>0)<($2>>>0);
 if ($3) {
  _abort();
  // unreachable;
 }
 $4 = ((($mem)) + -4|0);
 $5 = HEAP32[$4>>2]|0;
 $6 = $5 & 3;
 $7 = ($6|0)==(1);
 if ($7) {
  _abort();
  // unreachable;
 }
 $8 = $5 & -8;
 $$sum = (($8) + -8)|0;
 $9 = (($mem) + ($$sum)|0);
 $10 = $5 & 1;
 $11 = ($10|0)==(0);
 do {
  if ($11) {
   $12 = HEAP32[$1>>2]|0;
   $13 = ($6|0)==(0);
   if ($13) {
    return;
   }
   $$sum2 = (-8 - ($12))|0;
   $14 = (($mem) + ($$sum2)|0);
   $15 = (($12) + ($8))|0;
   $16 = ($14>>>0)<($2>>>0);
   if ($16) {
    _abort();
    // unreachable;
   }
   $17 = HEAP32[(20596)>>2]|0;
   $18 = ($14|0)==($17|0);
   if ($18) {
    $$sum3 = (($8) + -4)|0;
    $103 = (($mem) + ($$sum3)|0);
    $104 = HEAP32[$103>>2]|0;
    $105 = $104 & 3;
    $106 = ($105|0)==(3);
    if (!($106)) {
     $p$0 = $14;$psize$0 = $15;
     break;
    }
    HEAP32[(20584)>>2] = $15;
    $107 = $104 & -2;
    HEAP32[$103>>2] = $107;
    $108 = $15 | 1;
    $$sum20 = (($$sum2) + 4)|0;
    $109 = (($mem) + ($$sum20)|0);
    HEAP32[$109>>2] = $108;
    HEAP32[$9>>2] = $15;
    return;
   }
   $19 = $12 >>> 3;
   $20 = ($12>>>0)<(256);
   if ($20) {
    $$sum30 = (($$sum2) + 8)|0;
    $21 = (($mem) + ($$sum30)|0);
    $22 = HEAP32[$21>>2]|0;
    $$sum31 = (($$sum2) + 12)|0;
    $23 = (($mem) + ($$sum31)|0);
    $24 = HEAP32[$23>>2]|0;
    $25 = $19 << 1;
    $26 = (20616 + ($25<<2)|0);
    $27 = ($22|0)==($26|0);
    if (!($27)) {
     $28 = ($22>>>0)<($2>>>0);
     if ($28) {
      _abort();
      // unreachable;
     }
     $29 = ((($22)) + 12|0);
     $30 = HEAP32[$29>>2]|0;
     $31 = ($30|0)==($14|0);
     if (!($31)) {
      _abort();
      // unreachable;
     }
    }
    $32 = ($24|0)==($22|0);
    if ($32) {
     $33 = 1 << $19;
     $34 = $33 ^ -1;
     $35 = HEAP32[20576>>2]|0;
     $36 = $35 & $34;
     HEAP32[20576>>2] = $36;
     $p$0 = $14;$psize$0 = $15;
     break;
    }
    $37 = ($24|0)==($26|0);
    if ($37) {
     $$pre60 = ((($24)) + 8|0);
     $$pre$phi61Z2D = $$pre60;
    } else {
     $38 = ($24>>>0)<($2>>>0);
     if ($38) {
      _abort();
      // unreachable;
     }
     $39 = ((($24)) + 8|0);
     $40 = HEAP32[$39>>2]|0;
     $41 = ($40|0)==($14|0);
     if ($41) {
      $$pre$phi61Z2D = $39;
     } else {
      _abort();
      // unreachable;
     }
    }
    $42 = ((($22)) + 12|0);
    HEAP32[$42>>2] = $24;
    HEAP32[$$pre$phi61Z2D>>2] = $22;
    $p$0 = $14;$psize$0 = $15;
    break;
   }
   $$sum22 = (($$sum2) + 24)|0;
   $43 = (($mem) + ($$sum22)|0);
   $44 = HEAP32[$43>>2]|0;
   $$sum23 = (($$sum2) + 12)|0;
   $45 = (($mem) + ($$sum23)|0);
   $46 = HEAP32[$45>>2]|0;
   $47 = ($46|0)==($14|0);
   do {
    if ($47) {
     $$sum25 = (($$sum2) + 20)|0;
     $57 = (($mem) + ($$sum25)|0);
     $58 = HEAP32[$57>>2]|0;
     $59 = ($58|0)==(0|0);
     if ($59) {
      $$sum24 = (($$sum2) + 16)|0;
      $60 = (($mem) + ($$sum24)|0);
      $61 = HEAP32[$60>>2]|0;
      $62 = ($61|0)==(0|0);
      if ($62) {
       $R$1 = 0;
       break;
      } else {
       $R$0 = $61;$RP$0 = $60;
      }
     } else {
      $R$0 = $58;$RP$0 = $57;
     }
     while(1) {
      $63 = ((($R$0)) + 20|0);
      $64 = HEAP32[$63>>2]|0;
      $65 = ($64|0)==(0|0);
      if (!($65)) {
       $R$0 = $64;$RP$0 = $63;
       continue;
      }
      $66 = ((($R$0)) + 16|0);
      $67 = HEAP32[$66>>2]|0;
      $68 = ($67|0)==(0|0);
      if ($68) {
       $R$0$lcssa = $R$0;$RP$0$lcssa = $RP$0;
       break;
      } else {
       $R$0 = $67;$RP$0 = $66;
      }
     }
     $69 = ($RP$0$lcssa>>>0)<($2>>>0);
     if ($69) {
      _abort();
      // unreachable;
     } else {
      HEAP32[$RP$0$lcssa>>2] = 0;
      $R$1 = $R$0$lcssa;
      break;
     }
    } else {
     $$sum29 = (($$sum2) + 8)|0;
     $48 = (($mem) + ($$sum29)|0);
     $49 = HEAP32[$48>>2]|0;
     $50 = ($49>>>0)<($2>>>0);
     if ($50) {
      _abort();
      // unreachable;
     }
     $51 = ((($49)) + 12|0);
     $52 = HEAP32[$51>>2]|0;
     $53 = ($52|0)==($14|0);
     if (!($53)) {
      _abort();
      // unreachable;
     }
     $54 = ((($46)) + 8|0);
     $55 = HEAP32[$54>>2]|0;
     $56 = ($55|0)==($14|0);
     if ($56) {
      HEAP32[$51>>2] = $46;
      HEAP32[$54>>2] = $49;
      $R$1 = $46;
      break;
     } else {
      _abort();
      // unreachable;
     }
    }
   } while(0);
   $70 = ($44|0)==(0|0);
   if ($70) {
    $p$0 = $14;$psize$0 = $15;
   } else {
    $$sum26 = (($$sum2) + 28)|0;
    $71 = (($mem) + ($$sum26)|0);
    $72 = HEAP32[$71>>2]|0;
    $73 = (20880 + ($72<<2)|0);
    $74 = HEAP32[$73>>2]|0;
    $75 = ($14|0)==($74|0);
    if ($75) {
     HEAP32[$73>>2] = $R$1;
     $cond = ($R$1|0)==(0|0);
     if ($cond) {
      $76 = 1 << $72;
      $77 = $76 ^ -1;
      $78 = HEAP32[(20580)>>2]|0;
      $79 = $78 & $77;
      HEAP32[(20580)>>2] = $79;
      $p$0 = $14;$psize$0 = $15;
      break;
     }
    } else {
     $80 = HEAP32[(20592)>>2]|0;
     $81 = ($44>>>0)<($80>>>0);
     if ($81) {
      _abort();
      // unreachable;
     }
     $82 = ((($44)) + 16|0);
     $83 = HEAP32[$82>>2]|0;
     $84 = ($83|0)==($14|0);
     if ($84) {
      HEAP32[$82>>2] = $R$1;
     } else {
      $85 = ((($44)) + 20|0);
      HEAP32[$85>>2] = $R$1;
     }
     $86 = ($R$1|0)==(0|0);
     if ($86) {
      $p$0 = $14;$psize$0 = $15;
      break;
     }
    }
    $87 = HEAP32[(20592)>>2]|0;
    $88 = ($R$1>>>0)<($87>>>0);
    if ($88) {
     _abort();
     // unreachable;
    }
    $89 = ((($R$1)) + 24|0);
    HEAP32[$89>>2] = $44;
    $$sum27 = (($$sum2) + 16)|0;
    $90 = (($mem) + ($$sum27)|0);
    $91 = HEAP32[$90>>2]|0;
    $92 = ($91|0)==(0|0);
    do {
     if (!($92)) {
      $93 = ($91>>>0)<($87>>>0);
      if ($93) {
       _abort();
       // unreachable;
      } else {
       $94 = ((($R$1)) + 16|0);
       HEAP32[$94>>2] = $91;
       $95 = ((($91)) + 24|0);
       HEAP32[$95>>2] = $R$1;
       break;
      }
     }
    } while(0);
    $$sum28 = (($$sum2) + 20)|0;
    $96 = (($mem) + ($$sum28)|0);
    $97 = HEAP32[$96>>2]|0;
    $98 = ($97|0)==(0|0);
    if ($98) {
     $p$0 = $14;$psize$0 = $15;
    } else {
     $99 = HEAP32[(20592)>>2]|0;
     $100 = ($97>>>0)<($99>>>0);
     if ($100) {
      _abort();
      // unreachable;
     } else {
      $101 = ((($R$1)) + 20|0);
      HEAP32[$101>>2] = $97;
      $102 = ((($97)) + 24|0);
      HEAP32[$102>>2] = $R$1;
      $p$0 = $14;$psize$0 = $15;
      break;
     }
    }
   }
  } else {
   $p$0 = $1;$psize$0 = $8;
  }
 } while(0);
 $110 = ($p$0>>>0)<($9>>>0);
 if (!($110)) {
  _abort();
  // unreachable;
 }
 $$sum19 = (($8) + -4)|0;
 $111 = (($mem) + ($$sum19)|0);
 $112 = HEAP32[$111>>2]|0;
 $113 = $112 & 1;
 $114 = ($113|0)==(0);
 if ($114) {
  _abort();
  // unreachable;
 }
 $115 = $112 & 2;
 $116 = ($115|0)==(0);
 if ($116) {
  $117 = HEAP32[(20600)>>2]|0;
  $118 = ($9|0)==($117|0);
  if ($118) {
   $119 = HEAP32[(20588)>>2]|0;
   $120 = (($119) + ($psize$0))|0;
   HEAP32[(20588)>>2] = $120;
   HEAP32[(20600)>>2] = $p$0;
   $121 = $120 | 1;
   $122 = ((($p$0)) + 4|0);
   HEAP32[$122>>2] = $121;
   $123 = HEAP32[(20596)>>2]|0;
   $124 = ($p$0|0)==($123|0);
   if (!($124)) {
    return;
   }
   HEAP32[(20596)>>2] = 0;
   HEAP32[(20584)>>2] = 0;
   return;
  }
  $125 = HEAP32[(20596)>>2]|0;
  $126 = ($9|0)==($125|0);
  if ($126) {
   $127 = HEAP32[(20584)>>2]|0;
   $128 = (($127) + ($psize$0))|0;
   HEAP32[(20584)>>2] = $128;
   HEAP32[(20596)>>2] = $p$0;
   $129 = $128 | 1;
   $130 = ((($p$0)) + 4|0);
   HEAP32[$130>>2] = $129;
   $131 = (($p$0) + ($128)|0);
   HEAP32[$131>>2] = $128;
   return;
  }
  $132 = $112 & -8;
  $133 = (($132) + ($psize$0))|0;
  $134 = $112 >>> 3;
  $135 = ($112>>>0)<(256);
  do {
   if ($135) {
    $136 = (($mem) + ($8)|0);
    $137 = HEAP32[$136>>2]|0;
    $$sum1718 = $8 | 4;
    $138 = (($mem) + ($$sum1718)|0);
    $139 = HEAP32[$138>>2]|0;
    $140 = $134 << 1;
    $141 = (20616 + ($140<<2)|0);
    $142 = ($137|0)==($141|0);
    if (!($142)) {
     $143 = HEAP32[(20592)>>2]|0;
     $144 = ($137>>>0)<($143>>>0);
     if ($144) {
      _abort();
      // unreachable;
     }
     $145 = ((($137)) + 12|0);
     $146 = HEAP32[$145>>2]|0;
     $147 = ($146|0)==($9|0);
     if (!($147)) {
      _abort();
      // unreachable;
     }
    }
    $148 = ($139|0)==($137|0);
    if ($148) {
     $149 = 1 << $134;
     $150 = $149 ^ -1;
     $151 = HEAP32[20576>>2]|0;
     $152 = $151 & $150;
     HEAP32[20576>>2] = $152;
     break;
    }
    $153 = ($139|0)==($141|0);
    if ($153) {
     $$pre58 = ((($139)) + 8|0);
     $$pre$phi59Z2D = $$pre58;
    } else {
     $154 = HEAP32[(20592)>>2]|0;
     $155 = ($139>>>0)<($154>>>0);
     if ($155) {
      _abort();
      // unreachable;
     }
     $156 = ((($139)) + 8|0);
     $157 = HEAP32[$156>>2]|0;
     $158 = ($157|0)==($9|0);
     if ($158) {
      $$pre$phi59Z2D = $156;
     } else {
      _abort();
      // unreachable;
     }
    }
    $159 = ((($137)) + 12|0);
    HEAP32[$159>>2] = $139;
    HEAP32[$$pre$phi59Z2D>>2] = $137;
   } else {
    $$sum5 = (($8) + 16)|0;
    $160 = (($mem) + ($$sum5)|0);
    $161 = HEAP32[$160>>2]|0;
    $$sum67 = $8 | 4;
    $162 = (($mem) + ($$sum67)|0);
    $163 = HEAP32[$162>>2]|0;
    $164 = ($163|0)==($9|0);
    do {
     if ($164) {
      $$sum9 = (($8) + 12)|0;
      $175 = (($mem) + ($$sum9)|0);
      $176 = HEAP32[$175>>2]|0;
      $177 = ($176|0)==(0|0);
      if ($177) {
       $$sum8 = (($8) + 8)|0;
       $178 = (($mem) + ($$sum8)|0);
       $179 = HEAP32[$178>>2]|0;
       $180 = ($179|0)==(0|0);
       if ($180) {
        $R7$1 = 0;
        break;
       } else {
        $R7$0 = $179;$RP9$0 = $178;
       }
      } else {
       $R7$0 = $176;$RP9$0 = $175;
      }
      while(1) {
       $181 = ((($R7$0)) + 20|0);
       $182 = HEAP32[$181>>2]|0;
       $183 = ($182|0)==(0|0);
       if (!($183)) {
        $R7$0 = $182;$RP9$0 = $181;
        continue;
       }
       $184 = ((($R7$0)) + 16|0);
       $185 = HEAP32[$184>>2]|0;
       $186 = ($185|0)==(0|0);
       if ($186) {
        $R7$0$lcssa = $R7$0;$RP9$0$lcssa = $RP9$0;
        break;
       } else {
        $R7$0 = $185;$RP9$0 = $184;
       }
      }
      $187 = HEAP32[(20592)>>2]|0;
      $188 = ($RP9$0$lcssa>>>0)<($187>>>0);
      if ($188) {
       _abort();
       // unreachable;
      } else {
       HEAP32[$RP9$0$lcssa>>2] = 0;
       $R7$1 = $R7$0$lcssa;
       break;
      }
     } else {
      $165 = (($mem) + ($8)|0);
      $166 = HEAP32[$165>>2]|0;
      $167 = HEAP32[(20592)>>2]|0;
      $168 = ($166>>>0)<($167>>>0);
      if ($168) {
       _abort();
       // unreachable;
      }
      $169 = ((($166)) + 12|0);
      $170 = HEAP32[$169>>2]|0;
      $171 = ($170|0)==($9|0);
      if (!($171)) {
       _abort();
       // unreachable;
      }
      $172 = ((($163)) + 8|0);
      $173 = HEAP32[$172>>2]|0;
      $174 = ($173|0)==($9|0);
      if ($174) {
       HEAP32[$169>>2] = $163;
       HEAP32[$172>>2] = $166;
       $R7$1 = $163;
       break;
      } else {
       _abort();
       // unreachable;
      }
     }
    } while(0);
    $189 = ($161|0)==(0|0);
    if (!($189)) {
     $$sum12 = (($8) + 20)|0;
     $190 = (($mem) + ($$sum12)|0);
     $191 = HEAP32[$190>>2]|0;
     $192 = (20880 + ($191<<2)|0);
     $193 = HEAP32[$192>>2]|0;
     $194 = ($9|0)==($193|0);
     if ($194) {
      HEAP32[$192>>2] = $R7$1;
      $cond47 = ($R7$1|0)==(0|0);
      if ($cond47) {
       $195 = 1 << $191;
       $196 = $195 ^ -1;
       $197 = HEAP32[(20580)>>2]|0;
       $198 = $197 & $196;
       HEAP32[(20580)>>2] = $198;
       break;
      }
     } else {
      $199 = HEAP32[(20592)>>2]|0;
      $200 = ($161>>>0)<($199>>>0);
      if ($200) {
       _abort();
       // unreachable;
      }
      $201 = ((($161)) + 16|0);
      $202 = HEAP32[$201>>2]|0;
      $203 = ($202|0)==($9|0);
      if ($203) {
       HEAP32[$201>>2] = $R7$1;
      } else {
       $204 = ((($161)) + 20|0);
       HEAP32[$204>>2] = $R7$1;
      }
      $205 = ($R7$1|0)==(0|0);
      if ($205) {
       break;
      }
     }
     $206 = HEAP32[(20592)>>2]|0;
     $207 = ($R7$1>>>0)<($206>>>0);
     if ($207) {
      _abort();
      // unreachable;
     }
     $208 = ((($R7$1)) + 24|0);
     HEAP32[$208>>2] = $161;
     $$sum13 = (($8) + 8)|0;
     $209 = (($mem) + ($$sum13)|0);
     $210 = HEAP32[$209>>2]|0;
     $211 = ($210|0)==(0|0);
     do {
      if (!($211)) {
       $212 = ($210>>>0)<($206>>>0);
       if ($212) {
        _abort();
        // unreachable;
       } else {
        $213 = ((($R7$1)) + 16|0);
        HEAP32[$213>>2] = $210;
        $214 = ((($210)) + 24|0);
        HEAP32[$214>>2] = $R7$1;
        break;
       }
      }
     } while(0);
     $$sum14 = (($8) + 12)|0;
     $215 = (($mem) + ($$sum14)|0);
     $216 = HEAP32[$215>>2]|0;
     $217 = ($216|0)==(0|0);
     if (!($217)) {
      $218 = HEAP32[(20592)>>2]|0;
      $219 = ($216>>>0)<($218>>>0);
      if ($219) {
       _abort();
       // unreachable;
      } else {
       $220 = ((($R7$1)) + 20|0);
       HEAP32[$220>>2] = $216;
       $221 = ((($216)) + 24|0);
       HEAP32[$221>>2] = $R7$1;
       break;
      }
     }
    }
   }
  } while(0);
  $222 = $133 | 1;
  $223 = ((($p$0)) + 4|0);
  HEAP32[$223>>2] = $222;
  $224 = (($p$0) + ($133)|0);
  HEAP32[$224>>2] = $133;
  $225 = HEAP32[(20596)>>2]|0;
  $226 = ($p$0|0)==($225|0);
  if ($226) {
   HEAP32[(20584)>>2] = $133;
   return;
  } else {
   $psize$1 = $133;
  }
 } else {
  $227 = $112 & -2;
  HEAP32[$111>>2] = $227;
  $228 = $psize$0 | 1;
  $229 = ((($p$0)) + 4|0);
  HEAP32[$229>>2] = $228;
  $230 = (($p$0) + ($psize$0)|0);
  HEAP32[$230>>2] = $psize$0;
  $psize$1 = $psize$0;
 }
 $231 = $psize$1 >>> 3;
 $232 = ($psize$1>>>0)<(256);
 if ($232) {
  $233 = $231 << 1;
  $234 = (20616 + ($233<<2)|0);
  $235 = HEAP32[20576>>2]|0;
  $236 = 1 << $231;
  $237 = $235 & $236;
  $238 = ($237|0)==(0);
  if ($238) {
   $239 = $235 | $236;
   HEAP32[20576>>2] = $239;
   $$pre = (($233) + 2)|0;
   $$pre57 = (20616 + ($$pre<<2)|0);
   $$pre$phiZ2D = $$pre57;$F16$0 = $234;
  } else {
   $$sum11 = (($233) + 2)|0;
   $240 = (20616 + ($$sum11<<2)|0);
   $241 = HEAP32[$240>>2]|0;
   $242 = HEAP32[(20592)>>2]|0;
   $243 = ($241>>>0)<($242>>>0);
   if ($243) {
    _abort();
    // unreachable;
   } else {
    $$pre$phiZ2D = $240;$F16$0 = $241;
   }
  }
  HEAP32[$$pre$phiZ2D>>2] = $p$0;
  $244 = ((($F16$0)) + 12|0);
  HEAP32[$244>>2] = $p$0;
  $245 = ((($p$0)) + 8|0);
  HEAP32[$245>>2] = $F16$0;
  $246 = ((($p$0)) + 12|0);
  HEAP32[$246>>2] = $234;
  return;
 }
 $247 = $psize$1 >>> 8;
 $248 = ($247|0)==(0);
 if ($248) {
  $I18$0 = 0;
 } else {
  $249 = ($psize$1>>>0)>(16777215);
  if ($249) {
   $I18$0 = 31;
  } else {
   $250 = (($247) + 1048320)|0;
   $251 = $250 >>> 16;
   $252 = $251 & 8;
   $253 = $247 << $252;
   $254 = (($253) + 520192)|0;
   $255 = $254 >>> 16;
   $256 = $255 & 4;
   $257 = $256 | $252;
   $258 = $253 << $256;
   $259 = (($258) + 245760)|0;
   $260 = $259 >>> 16;
   $261 = $260 & 2;
   $262 = $257 | $261;
   $263 = (14 - ($262))|0;
   $264 = $258 << $261;
   $265 = $264 >>> 15;
   $266 = (($263) + ($265))|0;
   $267 = $266 << 1;
   $268 = (($266) + 7)|0;
   $269 = $psize$1 >>> $268;
   $270 = $269 & 1;
   $271 = $270 | $267;
   $I18$0 = $271;
  }
 }
 $272 = (20880 + ($I18$0<<2)|0);
 $273 = ((($p$0)) + 28|0);
 HEAP32[$273>>2] = $I18$0;
 $274 = ((($p$0)) + 16|0);
 $275 = ((($p$0)) + 20|0);
 HEAP32[$275>>2] = 0;
 HEAP32[$274>>2] = 0;
 $276 = HEAP32[(20580)>>2]|0;
 $277 = 1 << $I18$0;
 $278 = $276 & $277;
 $279 = ($278|0)==(0);
 L199: do {
  if ($279) {
   $280 = $276 | $277;
   HEAP32[(20580)>>2] = $280;
   HEAP32[$272>>2] = $p$0;
   $281 = ((($p$0)) + 24|0);
   HEAP32[$281>>2] = $272;
   $282 = ((($p$0)) + 12|0);
   HEAP32[$282>>2] = $p$0;
   $283 = ((($p$0)) + 8|0);
   HEAP32[$283>>2] = $p$0;
  } else {
   $284 = HEAP32[$272>>2]|0;
   $285 = ((($284)) + 4|0);
   $286 = HEAP32[$285>>2]|0;
   $287 = $286 & -8;
   $288 = ($287|0)==($psize$1|0);
   L202: do {
    if ($288) {
     $T$0$lcssa = $284;
    } else {
     $289 = ($I18$0|0)==(31);
     $290 = $I18$0 >>> 1;
     $291 = (25 - ($290))|0;
     $292 = $289 ? 0 : $291;
     $293 = $psize$1 << $292;
     $K19$052 = $293;$T$051 = $284;
     while(1) {
      $300 = $K19$052 >>> 31;
      $301 = (((($T$051)) + 16|0) + ($300<<2)|0);
      $296 = HEAP32[$301>>2]|0;
      $302 = ($296|0)==(0|0);
      if ($302) {
       $$lcssa = $301;$T$051$lcssa = $T$051;
       break;
      }
      $294 = $K19$052 << 1;
      $295 = ((($296)) + 4|0);
      $297 = HEAP32[$295>>2]|0;
      $298 = $297 & -8;
      $299 = ($298|0)==($psize$1|0);
      if ($299) {
       $T$0$lcssa = $296;
       break L202;
      } else {
       $K19$052 = $294;$T$051 = $296;
      }
     }
     $303 = HEAP32[(20592)>>2]|0;
     $304 = ($$lcssa>>>0)<($303>>>0);
     if ($304) {
      _abort();
      // unreachable;
     } else {
      HEAP32[$$lcssa>>2] = $p$0;
      $305 = ((($p$0)) + 24|0);
      HEAP32[$305>>2] = $T$051$lcssa;
      $306 = ((($p$0)) + 12|0);
      HEAP32[$306>>2] = $p$0;
      $307 = ((($p$0)) + 8|0);
      HEAP32[$307>>2] = $p$0;
      break L199;
     }
    }
   } while(0);
   $308 = ((($T$0$lcssa)) + 8|0);
   $309 = HEAP32[$308>>2]|0;
   $310 = HEAP32[(20592)>>2]|0;
   $311 = ($309>>>0)>=($310>>>0);
   $not$ = ($T$0$lcssa>>>0)>=($310>>>0);
   $312 = $311 & $not$;
   if ($312) {
    $313 = ((($309)) + 12|0);
    HEAP32[$313>>2] = $p$0;
    HEAP32[$308>>2] = $p$0;
    $314 = ((($p$0)) + 8|0);
    HEAP32[$314>>2] = $309;
    $315 = ((($p$0)) + 12|0);
    HEAP32[$315>>2] = $T$0$lcssa;
    $316 = ((($p$0)) + 24|0);
    HEAP32[$316>>2] = 0;
    break;
   } else {
    _abort();
    // unreachable;
   }
  }
 } while(0);
 $317 = HEAP32[(20608)>>2]|0;
 $318 = (($317) + -1)|0;
 HEAP32[(20608)>>2] = $318;
 $319 = ($318|0)==(0);
 if ($319) {
  $sp$0$in$i = (21032);
 } else {
  return;
 }
 while(1) {
  $sp$0$i = HEAP32[$sp$0$in$i>>2]|0;
  $320 = ($sp$0$i|0)==(0|0);
  $321 = ((($sp$0$i)) + 8|0);
  if ($320) {
   break;
  } else {
   $sp$0$in$i = $321;
  }
 }
 HEAP32[(20608)>>2] = -1;
 return;
}
function runPostSets() {

}
function _memset(ptr, value, num) {
    ptr = ptr|0; value = value|0; num = num|0;
    var stop = 0, value4 = 0, stop4 = 0, unaligned = 0;
    stop = (ptr + num)|0;
    if ((num|0) >= 20) {
      // This is unaligned, but quite large, so work hard to get to aligned settings
      value = value & 0xff;
      unaligned = ptr & 3;
      value4 = value | (value << 8) | (value << 16) | (value << 24);
      stop4 = stop & ~3;
      if (unaligned) {
        unaligned = (ptr + 4 - unaligned)|0;
        while ((ptr|0) < (unaligned|0)) { // no need to check for stop, since we have large num
          HEAP8[((ptr)>>0)]=value;
          ptr = (ptr+1)|0;
        }
      }
      while ((ptr|0) < (stop4|0)) {
        HEAP32[((ptr)>>2)]=value4;
        ptr = (ptr+4)|0;
      }
    }
    while ((ptr|0) < (stop|0)) {
      HEAP8[((ptr)>>0)]=value;
      ptr = (ptr+1)|0;
    }
    return (ptr-num)|0;
}
function _bitshift64Shl(low, high, bits) {
    low = low|0; high = high|0; bits = bits|0;
    var ander = 0;
    if ((bits|0) < 32) {
      ander = ((1 << bits) - 1)|0;
      tempRet0 = (high << bits) | ((low&(ander << (32 - bits))) >>> (32 - bits));
      return low << bits;
    }
    tempRet0 = low << (bits - 32);
    return 0;
}
function _strlen(ptr) {
    ptr = ptr|0;
    var curr = 0;
    curr = ptr;
    while (((HEAP8[((curr)>>0)])|0)) {
      curr = (curr + 1)|0;
    }
    return (curr - ptr)|0;
}
function _memcpy(dest, src, num) {
    dest = dest|0; src = src|0; num = num|0;
    var ret = 0;
    if ((num|0) >= 4096) return _emscripten_memcpy_big(dest|0, src|0, num|0)|0;
    ret = dest|0;
    if ((dest&3) == (src&3)) {
      while (dest & 3) {
        if ((num|0) == 0) return ret|0;
        HEAP8[((dest)>>0)]=((HEAP8[((src)>>0)])|0);
        dest = (dest+1)|0;
        src = (src+1)|0;
        num = (num-1)|0;
      }
      while ((num|0) >= 4) {
        HEAP32[((dest)>>2)]=((HEAP32[((src)>>2)])|0);
        dest = (dest+4)|0;
        src = (src+4)|0;
        num = (num-4)|0;
      }
    }
    while ((num|0) > 0) {
      HEAP8[((dest)>>0)]=((HEAP8[((src)>>0)])|0);
      dest = (dest+1)|0;
      src = (src+1)|0;
      num = (num-1)|0;
    }
    return ret|0;
}
function _strcpy(pdest, psrc) {
    pdest = pdest|0; psrc = psrc|0;
    var i = 0;
    do {
      HEAP8[(((pdest+i)|0)>>0)]=HEAP8[(((psrc+i)|0)>>0)];
      i = (i+1)|0;
    } while (((HEAP8[(((psrc)+(i-1))>>0)])|0));
    return pdest|0;
}

  


// EMSCRIPTEN_END_FUNCS


  return { _filtcoef: _filtcoef, _free: _free, _wave_transform: _wave_transform, _wtoutputlength: _wtoutputlength, _memset: _memset, _malloc: _malloc, _memcpy: _memcpy, _strlen: _strlen, _inv_wave_transform: _inv_wave_transform, _filtlength: _filtlength, _strcpy: _strcpy, _bitshift64Shl: _bitshift64Shl, runPostSets: runPostSets, stackAlloc: stackAlloc, stackSave: stackSave, stackRestore: stackRestore, establishStackSpace: establishStackSpace, setThrew: setThrew, setTempRet0: setTempRet0, getTempRet0: getTempRet0 };
})
// EMSCRIPTEN_END_ASM
(Module.asmGlobalArg, Module.asmLibraryArg, buffer);
var _wtoutputlength = Module["_wtoutputlength"] = asm["_wtoutputlength"];
var _free = Module["_free"] = asm["_free"];
var runPostSets = Module["runPostSets"] = asm["runPostSets"];
var _wave_transform = Module["_wave_transform"] = asm["_wave_transform"];
var _filtcoef = Module["_filtcoef"] = asm["_filtcoef"];
var _memset = Module["_memset"] = asm["_memset"];
var _malloc = Module["_malloc"] = asm["_malloc"];
var _memcpy = Module["_memcpy"] = asm["_memcpy"];
var _strlen = Module["_strlen"] = asm["_strlen"];
var _inv_wave_transform = Module["_inv_wave_transform"] = asm["_inv_wave_transform"];
var _filtlength = Module["_filtlength"] = asm["_filtlength"];
var _strcpy = Module["_strcpy"] = asm["_strcpy"];
var _bitshift64Shl = Module["_bitshift64Shl"] = asm["_bitshift64Shl"];
;

Runtime.stackAlloc = asm['stackAlloc'];
Runtime.stackSave = asm['stackSave'];
Runtime.stackRestore = asm['stackRestore'];
Runtime.establishStackSpace = asm['establishStackSpace'];

Runtime.setTempRet0 = asm['setTempRet0'];
Runtime.getTempRet0 = asm['getTempRet0'];


// Warning: printing of i64 values may be slightly rounded! No deep i64 math used, so precise i64 code not included
var i64Math = null;

// === Auto-generated postamble setup entry stuff ===


function ExitStatus(status) {
  this.name = "ExitStatus";
  this.message = "Program terminated with exit(" + status + ")";
  this.status = status;
};
ExitStatus.prototype = new Error();
ExitStatus.prototype.constructor = ExitStatus;

var initialStackTop;
var preloadStartTime = null;
var calledMain = false;

dependenciesFulfilled = function runCaller() {
  // If run has never been called, and we should call run (INVOKE_RUN is true, and Module.noInitialRun is not false)
  if (!Module['calledRun']) run();
  if (!Module['calledRun']) dependenciesFulfilled = runCaller; // try this again later, after new deps are fulfilled
}

Module['callMain'] = Module.callMain = function callMain(args) {
  assert(runDependencies == 0, 'cannot call main when async dependencies remain! (listen on __ATMAIN__)');
  assert(__ATPRERUN__.length == 0, 'cannot call main when preRun functions remain to be called');

  args = args || [];

  ensureInitRuntime();

  var argc = args.length+1;
  function pad() {
    for (var i = 0; i < 4-1; i++) {
      argv.push(0);
    }
  }
  var argv = [allocate(intArrayFromString(Module['thisProgram']), 'i8', ALLOC_NORMAL) ];
  pad();
  for (var i = 0; i < argc-1; i = i + 1) {
    argv.push(allocate(intArrayFromString(args[i]), 'i8', ALLOC_NORMAL));
    pad();
  }
  argv.push(0);
  argv = allocate(argv, 'i32', ALLOC_NORMAL);

  initialStackTop = STACKTOP;

  try {

    var ret = Module['_main'](argc, argv, 0);


    // if we're not running an evented main loop, it's time to exit
    exit(ret, /* implicit = */ true);
  }
  catch(e) {
    if (e instanceof ExitStatus) {
      // exit() throws this once it's done to make sure execution
      // has been stopped completely
      return;
    } else if (e == 'SimulateInfiniteLoop') {
      // running an evented main loop, don't immediately exit
      Module['noExitRuntime'] = true;
      return;
    } else {
      if (e && typeof e === 'object' && e.stack) Module.printErr('exception thrown: ' + [e, e.stack]);
      throw e;
    }
  } finally {
    calledMain = true;
  }
}




function run(args) {
  args = args || Module['arguments'];

  if (preloadStartTime === null) preloadStartTime = Date.now();

  if (runDependencies > 0) {
    return;
  }

  preRun();

  if (runDependencies > 0) return; // a preRun added a dependency, run will be called later
  if (Module['calledRun']) return; // run may have just been called through dependencies being fulfilled just in this very frame

  function doRun() {
    if (Module['calledRun']) return; // run may have just been called while the async setStatus time below was happening
    Module['calledRun'] = true;

    if (ABORT) return; 

    ensureInitRuntime();

    preMain();

    if (ENVIRONMENT_IS_WEB && preloadStartTime !== null) {
      Module.printErr('pre-main prep time: ' + (Date.now() - preloadStartTime) + ' ms');
    }

    if (Module['onRuntimeInitialized']) Module['onRuntimeInitialized']();

    if (Module['_main'] && shouldRunNow) Module['callMain'](args);

    postRun();
  }

  if (Module['setStatus']) {
    Module['setStatus']('Running...');
    setTimeout(function() {
      setTimeout(function() {
        Module['setStatus']('');
      }, 1);
      doRun();
    }, 1);
  } else {
    doRun();
  }
}
Module['run'] = Module.run = run;

function exit(status, implicit) {
  if (implicit && Module['noExitRuntime']) {
    return;
  }

  if (Module['noExitRuntime']) {
  } else {

    ABORT = true;
    EXITSTATUS = status;
    STACKTOP = initialStackTop;

    exitRuntime();

    if (Module['onExit']) Module['onExit'](status);
  }

  if (ENVIRONMENT_IS_NODE) {
    // Work around a node.js bug where stdout buffer is not flushed at process exit:
    // Instead of process.exit() directly, wait for stdout flush event.
    // See https://github.com/joyent/node/issues/1669 and https://github.com/kripken/emscripten/issues/2582
    // Workaround is based on https://github.com/RReverser/acorn/commit/50ab143cecc9ed71a2d66f78b4aec3bb2e9844f6
    process['stdout']['once']('drain', function () {
      process['exit'](status);
    });
    console.log(' '); // Make sure to print something to force the drain event to occur, in case the stdout buffer was empty.
    // Work around another node bug where sometimes 'drain' is never fired - make another effort
    // to emit the exit status, after a significant delay (if node hasn't fired drain by then, give up)
    setTimeout(function() {
      process['exit'](status);
    }, 500);
  } else
  if (ENVIRONMENT_IS_SHELL && typeof quit === 'function') {
    quit(status);
  }
  // if we reach here, we must throw an exception to halt the current execution
  throw new ExitStatus(status);
}
Module['exit'] = Module.exit = exit;

var abortDecorators = [];

function abort(what) {
  if (what !== undefined) {
    Module.print(what);
    Module.printErr(what);
    what = JSON.stringify(what)
  } else {
    what = '';
  }

  ABORT = true;
  EXITSTATUS = 1;

  var extra = '\nIf this abort() is unexpected, build with -s ASSERTIONS=1 which can give more information.';

  var output = 'abort(' + what + ') at ' + stackTrace() + extra;
  if (abortDecorators) {
    abortDecorators.forEach(function(decorator) {
      output = decorator(output, what);
    });
  }
  throw output;
}
Module['abort'] = Module.abort = abort;

// {{PRE_RUN_ADDITIONS}}

if (Module['preInit']) {
  if (typeof Module['preInit'] == 'function') Module['preInit'] = [Module['preInit']];
  while (Module['preInit'].length > 0) {
    Module['preInit'].pop()();
  }
}

// shouldRunNow refers to calling main(), not run().
var shouldRunNow = true;
if (Module['noInitialRun']) {
  shouldRunNow = false;
}


run();

// {{POST_RUN_ADDITIONS}}






// {{MODULE_ADDITIONS}}



