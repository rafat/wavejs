#include "wave_wrapper.h"

int wtoutputlength(int N,char *method,int lp, int J, char *ext) {
        int i, M;
        M = 0;

        if (!strcmp(method, "dwt") || !strcmp(method, "DWT")) {
                if (!strcmp(ext,"per")) {
                        i = J;
                        while (i > 0) {
                                N = (int)ceil((double)N / 2.0);
                                M += N;
                                i--;
                        }
                        M += N;
                }
                else if (!strcmp(ext,"sym")) {
                        i = J;
                        while (i > 0) {
                                N = N + lp - 2;
                                N = (int) ceil((double)N / 2.0);
                                M += N;
                                i--;
                        }
                        M += N;
                }
        }
        else if (!strcmp(method, "swt") || !strcmp(method, "SWT")) {
                M = (J+1) * N;
        }
        else if (!strcmp(method, "modwt") || !strcmp(method, "MODWT")) {
                M = (J+1) * N;
        }


        return M;
}

void wave_transform(double *inp, int N, char *wname, char *method, int J, char *ext, double *out, int *length, double *filters) {

        wave_object obj;
        wt_object wt;
        int zpad,i;

        zpad = 0;

        if ((N % 2) == 0) {
        	zpad = 1;
        }

        obj = wave_init(wname);

        for(i = 0; i < obj->filtlength;++i) {
                filters[i] = obj->lpd[i];
                filters[obj->filtlength + i] = obj->hpd[i];
                filters[obj->filtlength * 2 + i] = obj->lpr[i];
                filters[obj->filtlength * 3 + i] = obj->hpr[i];
        }

        wt = wt_init(obj, method, N, J);

        if (!strcmp(method, "dwt") || !strcmp(method, "DWT")) {
                setDWTExtension(wt,ext);
                dwt(wt,inp);
        }
        else if (!strcmp(method, "swt") || !strcmp(method, "SWT")) {
                swt(wt,inp);
        }
        else if (!strcmp(method, "modwt") || !strcmp(method, "MODWT")) {
                modwt(wt,inp);
        }


        for(i = 0; i < wt->lenlength;++i) {
                length[i] = wt->length[i];
        }

        for(i = 0; i < wt->outlength;++i) {
                out[i] = wt->output[i];
        }

        wave_free(obj);
        wt_free(wt);
}

void inv_wave_transform(double *inp, int N, char *wname, char *method, int J, char *ext, double *out, int outlength, int *length, int lenlength) {

        wave_object obj;
        wt_object wt;
        int i;

        obj = wave_init(wname);

        wt = wt_init(obj, method, outlength, J);

        for(i = 0; i < N;++i) {
                wt->output[i] = inp[i];
        }

        wt->lenlength = lenlength;

        for(i = 0; i < lenlength;++i) {
                wt->length[i] = length[i];
        }

        if (!strcmp(method, "dwt") || !strcmp(method, "DWT")) {
                setDWTExtension(wt,ext);
                idwt(wt,out);
        }
        else if (!strcmp(method, "swt") || !strcmp(method, "SWT")) {
                iswt(wt,out);
        }
        else if (!strcmp(method, "modwt") || !strcmp(method, "MODWT")) {
                imodwt(wt,out);
        }


        wave_free(obj);
        wt_free(wt);
}

void cwave_transform(char* wave, double param, double *inp, int N,double dt, int J,double s0, double dj, char *type, int power,
  int npflag, double *oupmag,double *scale, double *period, double *coi) {
    double t1;
    int ibase2,npad,mother,i;
    double *oup;

    if (npflag == 1) {
      t1 = 0.499999 + log((double)N) / log(2.0);
    	ibase2 = 1 + (int)t1;

    	npad = (int)pow(2.0, (double)ibase2);
    } else {
      npad = N;
    }

    oup = (double*)malloc(sizeof(double)* 2 * N * J);

    if (!strcmp(wave, "morlet")) {
            mother = 0;
    }
    else if (!strcmp(wave, "paul")) {
            mother = 1;
    }
    else if (!strcmp(wave, "dgauss")) {
            mother = 2;
    }

    if (!strcmp(type, "pow") || !strcmp(type, "power")) {
  		for (i = 0; i < J; ++i) {
  			scale[i] = s0*pow((double) power, (double)(i)*dj);
  		}

  	}
  	else if (!strcmp(type, "lin") || !strcmp(type, "linear")) {
  		for (i = 0; i < J; ++i) {
  			scale[i] = s0 + (double)i * dj;
  		}
  	}
  	else {
  		printf("\n Type accepts only two values : pow and lin\n");
  		exit(-1);
  	}

    cwavelet(inp, N, dt, mother, param, s0, dj, J, npad, oup, scale, period, coi);

    for(i = 0; i < N * J;++i) {
      oupmag[i] = sqrt(oup[2*i]*oup[2*i] + oup[2*i+1]*oup[2*i+1]);
    }
    free(oup);
}

void wdenoise(double *signal,int N,int J,char *denoise_method,char *wname,char *method,char *ext,char *thresh,double *denoised) {
        if(!strcmp(denoise_method,"Visushrink")) {
                visushrink(signal,N,J,wname,method,ext,thresh,denoised);
        } if(!strcmp(denoise_method,"Sureshrink")) {
                sureshrink(signal,N,J,wname,method,ext,thresh,denoised);
        }
}

int wreccoeff(char *method,double *coeff,int *length,char *ctype,char *ext, int level, int J,double *lpr,
		double *hpr,int lf,int siglength,double *reccoeff) {
	//void getDWTRecCoeff(double *coeff,int *length,char *ctype,char *ext, int level, int J,double *lpr,
	//	double *hpr,int lf,int siglength,double *reccoeff)

	if (!strcmp(method, "dwt")) {
		getDWTRecCoeff(coeff,length,ctype,ext, level, J,lpr, hpr,lf,siglength,reccoeff);
		return 0;
	} else {
		printf("This function gets reconstruction coefficients only for DWT decomposition\n");
		exit(-1);
	}
}
