/*
Copyright (c) 2017, Rafat Hussain
*/
#ifndef WAUX_H_
#define WAUX_H_

#include <float.h>
#include "wavelib.h"

#ifdef __cplusplus
extern "C" {
#endif


void visushrink(double *signal,int N,int J,char *wname,char *method,char *ext,char *thresh,double *denoised);

void sureshrink(double *signal,int N,int J,char *wname,char *method,char *ext,char *thresh,double *denoised);

void getDWTRecCoeff(double *coeff,int *length,char *ctype,char *ext, int level, int J,double *lpr, double *hpr,int lf,int siglength,double *reccoeff);


#ifdef __cplusplus
}
#endif


#endif /* WAUX_H_ */
